# [v2.4.7](https://gitlab.com/opencpi/osp/ocpi.osp.n3xx/-/compare/v2.4.6...v2.4.7) (2024-01-10)

Changes/additions since [OpenCPI Release v2.4.6](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.6)

### Bug Fixes
- **devops**: prevent commits from automatically launching a CI/CD pipeline. (!1)(27104d6a)

# [v2.4.6](https://gitlab.com/opencpi/osp/ocpi.osp.n3xx/-/compare/v2.4.5...v2.4.6) (2023-03-29)

No Changes/additions since [OpenCPI Release v2.4.5](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.5)

# [v2.4.5](https://gitlab.com/opencpi/osp/ocpi.osp.n3xx/-/compare/v2.4.4...v2.4.5) (2023-03-16)

No Changes/additions since [OpenCPI Release v2.4.4](https://gitlab.com/opencpi/opencpi/-/releases/v2.4.4)

# [v2.4.4](https://gitlab.com/opencpi/osp/ocpi.osp.n3xx/-/compare/dd0f02e1...v2.4.4) (2023-02-09)

Initial release of n3xx OSP for OpenCPI
