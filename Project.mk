# This Makefile fragment is for the n3xx project

# Package identifier is used in a hierarchical fashion from Project to Libraries....
# The PackageName, PackagePrefix and Package variables can optionally be set here:
# PackageName defaults to the name of the directory
# PackagePrefix defaults to local
# Package defaults to PackagePrefix.PackageName
#
# ***************** WARNING ********************
# When changing the PackageName or PackagePrefix of an existing project the
# project needs to be both unregistered and re-registered then cleaned and
# rebuilt. This also includes cleaning and rebuilding any projects that
# depend on this project.
# ***************** WARNING ********************
#
PackageName=osp.n3xx
PackagePrefix=ocpi
ProjectDependencies=ocpi.platform ocpi.core ocpi.assets
ComponentLibraries=util_comps base_comps misc_comps devices components
