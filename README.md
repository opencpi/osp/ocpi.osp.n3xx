# N3xx OpenCPI Support System Project (OSP)

<br>

OSP for Ettus N3xx line of products.  Instructions are based on the CentOS 7 distribution.

<br>

***
## Supported Platforms
| Name | Status |
| ---- | ------ |
| n310 | v1.0.0 |

<br>

***
## Dependencies

| Project                         | Tested Commit/Tag  |
| ------------------------------- | ------------------ |
| opencpi                         | v2.4.3             |
| ettus_n310_v4 (ocpi.osp.ettus)  | v2.4.3             | 
| Xilinx Vivado                   | 2019.2             |

<br>

***
## OSP Setup Guide

### OpenCPI
To install [OpenCPI](https://opencpi.gitlab.io):

```bash
$ git clone https:://gitlab.com/opencpi/opencpi.git && cd opencpi
$ git fetch --all --tags
$ git checkout tags/<ocpi-version>
$ ./scripts/install_opencpi.sh
```
Make sure to source the OpenCPI environment script with `source <opencpi-install-dir>/cdk/opencpi-setup.sh -s`.

### `ettus_n310_v4` RCC Platform

CentOS prerequisites:

```bash
$ sudo yum install chrpath diffstat texinfo
$ build tar from source  #CentOS default version is too old
$ unset LD_LIBRARY_PATH
```

To install rcc platform:

```bash
$ cd <opencpi-install-dir>
$ ocpiadmin install platform ettus_n310_v4 -p ocpi.osp.ettus --minimal
```

<br>

### `n310` HDL Platform

1. To install the hdl platform:

```bash
$ cd <git-repo-dir>
$ ocpidev register project ocpi.osp.n3xx
$ cd <git-repo-dir>/ocpi.osp.n3xx
$ git fetch --all --tags
$ git checkout <ocpi.osp.n3xx-version>
$ ocpiadmin install platform n310 --minimal
```

This command will build the primitives, device workers, platform worker, and platform configurations.

<br>

2. To build the `n310` DRC:

```bash
$ cd <git-repo-dir>/ocpi.osp.n3xx/hdl/devices/drc_n310.rcc
$  ocpidev build --rcc-plaform ettus_n310_v4
```

<br>

3. To build the documentation:

```bash
$ cd <git-repo-dir>/ocpi.osp.n3xx
$ ocpidoc build -b
```
The HTML documentation is stored in `<git-repo-dir>/ocpi.osp.n3xx/gen/doc` with the top-level HTML file being `index.html`.


<br>

4. To build the hdl assemblies:

```bash
$ cd <git-repo-dir>/ocpi.osp.n3xx/hdl/assemblies
$ ocpidev build --hdl-plaform n310 --workers-as-needed
```

Individual assemblies can be built using this command within each assemblies sub-directory.


<br>

5. To all the applications:

```bash
$ cd <git-repo-dir>/ocpi.osp.n3xx/applications
$ ocpidev build --rcc-plaform ettus_n310_v4
```

Indiviual applications can be built using this command within each application sub-directory.


### SD Card

The following is the process for the creation of Ettus Research N310 microSD and modifications for installing OpenCPI and gdbserver.  The versions are:

- Linux Kernel: Ettus Research OpenEmbedded (OE) Linux 5.2.28-yocto-standard armv7<br>
- OpenCPI: OpenCPI v2.4.3<br>
- gdbserver: GNU gdbserver (GDB) 11.1 - Needed for debugging only!<br>

The SD CARD files are located at `<git-repo-dir>/ocpi.osp.n3xx/hdl/platform/n310/sdcard`. <br>

<br>

The following instructions were created using CentOS 7 with username 'opencpi'.  Other distributions may require different commands.

<br>

1. Insert microSD into the host computer and located the N310 microSD device node location using lsblk:

```bash
$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0   275G  0 disk 
├─sda1   8:1    0   300M  0 part /boot
├─sda2   8:2    0   7.9G  0 part [SWAP]
└─sda3   8:3    0 266.9G  0 part /
sdb      8:16   1  59.5G  0 disk 
├─sdb1   8:17   1    16M  0 part /run/media/opencpi/boot
├─sdb2   8:18   1   7.3G  0 part /run/media/opencpi/primary
├─sdb3   8:19   1   7.3G  0 part /run/media/opencpi/secondary
└─sdb4   8:20   1  44.8G  0 part /run/media/opencpi/data
``` 

The device node is sdb consisting of the following partitions: sdb1, sdb2, sdb3, sdb4.

<br>

2. Unmount the N310 partitions:

```bash
$ sudo umount /run/media/opencpi/boot
$ sudo umount /run/media/opencpi/primary
$ sudo umount /run/media/opencpi/secondary
$ sudo umount /run/media/opencpi/data
```

Verify the partitions are umounted:

```bash
$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0   275G  0 disk 
├─sda1   8:1    0   300M  0 part /boot
├─sda2   8:2    0   7.9G  0 part [SWAP]
└─sda3   8:3    0 266.9G  0 part /
sdb      8:16   1  14.9G  0 disk 
├─sdb1   8:17   1    16M  0 part 
├─sdb2   8:18   1   7.3G  0 part 
├─sdb3   8:19   1   7.3G  0 part 
└─sdb4   8:20   1   128M  0 part
```

<br>

3. Untar the Linux disk image located at `<git-repo-dir>/ocpi.osp.n3xx/hdl/platform/n310/sdcard/common` into a working directory: 

```bash
$ tar xvzf gnuradio-image-ni-sulfur-rev11-mender.sdimg.tar.gz
```
NOTE: The file size is 16GB.  Make sure there is enough disk space in the working directory.

<br>

4. Write the disk image onto the microSD using `dd if=<IMAGE> of=<SD_CARD_DEV_NAME> bs=1M`:

```bash
% sudo dd if=./gnuradio-image-ni-sulfur-rev11-mender.sdimg of=/dev/sdb bs=1M
15144+0 records in
15144+0 records out
15879634944 bytes (16 GB) copied, 1834.25 s, 8.7 MB/s
```

<br>

5. To ensure the disk is synchronized, run the sync command:

```bash
$ sync
```

<br>

6. Remove and re-insert the microSD into the host computer and verify the device node and partitions using lsblk:

```bash
$ lsblk
sda      8:0    0   275G  0 disk 
├─sda1   8:1    0   300M  0 part /boot
├─sda2   8:2    0   7.9G  0 part [SWAP]
└─sda3   8:3    0 266.9G  0 part /
sdb      8:16   1  14.9G  0 disk 
├─sdb1   8:17   1    16M  0 part /run/media/opencpi/boot
├─sdb2   8:18   1   7.3G  0 part /run/media/opencpi/5dbec412-f495-4000-ba55-d3d71fc9712f
├─sdb3   8:19   1   7.3G  0 part /run/media/opencpi/5dbec412-f495-4000-ba55-d3d71fc9712f1
└─sdb4   8:20   1   264M  0 part /run/media/opencpi/data
```

<br>

7.  Untar OpenCPI Directory located at `<git-repo-dir>/ocpi.osp.n3xx/hdl/platform/n310/sdcard/<ocpi-version>` into a working directory and copy the OpenCPI Directory to the second partition:

```bash
$ tar xvzf opencpi.tar.gz
$ sudo cp -r opencpi /run/media/opencpi/5dbec412-f495-4000-ba55-d3d71fc9712f/home/root
```

<br>

8. Optional for debugging: Untar gdbserver located at `<git-repo-dir>/ocpi.osp.n3xx/hdl/platform/n310/sdcard/common` into a working directory and copy gdbserver to the second partition:

```bash
$ tar xvzf gdbserver.tar.gz
$ sudo cp gdbserver /run/media/opencpi/5dbec412-f495-4000-ba55-d3d71fc9712f/usr/bin
```

<br>

9. Umount partitions and remove microSD from host computer.  Insert microSD into the N310 platform and power on the system.


### OpenCPI Environment Script

To set up the OpenCPI environment on the platform, source `mysetup` script located at `/home/root/opencpi`.  Using ssh or console, source the script:

```bash
root@ni-n3xx-322C646:~# ls
opencpi

root@ni-n3xx-322C646:~# cd opencpi

root@ni-n3xx-322C646:~/opencpi# source ./mysetup.sh 

Attempting to set time from time.nist.gov
rdate: time.nist.gov: short read
====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
Running login script.
OCPI_CDK_DIR is now /home/root/opencpi.
OCPI_ROOT_DIR is now /home/root/opencpi/...
Executing /home/root/.ocpiprofile.
The driver module is already loaded. No action was taken.
OpenCPI ready for zynq.
Loading bitstream
Bitstream loaded successfully
Discovering available containers...
Available containers:
 #  Model Platform            OS     OS-Version  Arch     Name
 0  hdl   n310                                            PL:0
 1  rcc   ettus_n310_v4       linux  ettus_n310_v4 arm      rcc0
```

***NOTE: THE MYSETUP SCRIPT CAN CAUSE A REBOOT OF THE PLATFORM DUE TO A CONFLICT WITH ETTUS MODULE PERIPHERAL MANAGER (MPM).  AFTER REBOOT, PLEASE TRY TO SOURCE THE SCRIPT AGAIN WAITING FOR 30 SECONDS AFTER BOOT UP.  DEVELOPERS ARE WORKING ON REPLACING THE ETTUS MPM.***

<br>

Verify that the `testbias` application runs:

```bash
% cd applications/ocpi

% ocpirun -v -d testbias.xml -m bias=hdl
Available containers are:  0: PL:0 [model: hdl os:  platform: n310], 1: rcc0 [model: rcc os: linux platform: ettus_n310_v4]
Actual deployment is:
  Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in /home/root/opencpi/ettus_n310_v4/artifacts/ocpi.core.file_read.rcc.0.ettus_n310_v4.so dated Fri Jan 14 13:23:03 2022
  Instance  1 bias (spec ocpi.core.bias) on hdl container 0: PL:0, using bias_vhdl/a/bias_vhdl in /home/root/opencpi/artifacts/testbias_n310_base.bitz dated Fri Jan 14 13:52:38 2022
  Instance  2 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in /home/root/opencpi/ettus_n310_v4/artifacts/ocpi.core.file_write.rcc.0.ettus_n310_v4.so dated Fri Jan 14 13:23:03 2022
Application XML parsed and deployments (containers and artifacts) chosen [0 s 69 ms]
Application established: containers, workers, connections all created [0 s 43 ms]
Dump of all initial property values:
Property   0: file_read.fileName = "test.input" (cached)
Property   1: file_read.messagesInFile = "false" (cached)
Property   2: file_read.opcode = "0" (cached)
Property   3: file_read.messageSize = "16"
Property   4: file_read.granularity = "4" (cached)
Property   5: file_read.repeat = "false"
Property   6: file_read.bytesRead = "0"
Property   7: file_read.messagesWritten = "0"
Property   8: file_read.suppressEOF = "false"
Property   9: file_read.badMessage = "false"
Property  16: bias.biasValue = "16909060" (cached)
Property  20: bias.test64 = "0"
Property  31: file_write.fileName = "test.output" (cached)
Property  32: file_write.messagesInFile = "false" (cached)
Property  33: file_write.bytesWritten = "0"
Property  34: file_write.messagesWritten = "0"
Property  35: file_write.stopOnEOF = "true" (cached)
Property  39: file_write.suppressWrites = "false"
Property  40: file_write.countData = "false"
Property  41: file_write.bytesPerSecond = "0"
Application started/running [0 s 5 ms]
Waiting for application to finish (no time limit)
Application finished [0 s 51 ms]
Dump of all final property values:
Property   0: file_read.fileName = "test.input" (cached)
Property   1: file_read.messagesInFile = "false" (cached)
Property   2: file_read.opcode = "0" (cached)
Property   3: file_read.messageSize = "16"
Property   4: file_read.granularity = "4" (cached)
Property   5: file_read.repeat = "false" (cached)
Property   6: file_read.bytesRead = "4000"
Property   7: file_read.messagesWritten = "250"
Property   8: file_read.suppressEOF = "false" (cached)
Property   9: file_read.badMessage = "false"
Property  16: bias.biasValue = "16909060" (cached)
Property  20: bias.test64 = "0" (cached)
Property  31: file_write.fileName = "test.output" (cached)
Property  32: file_write.messagesInFile = "false" (cached)
Property  33: file_write.bytesWritten = "4000"
Property  34: file_write.messagesWritten = "250"
Property  35: file_write.stopOnEOF = "true" (cached)
Property  39: file_write.suppressWrites = "false" (cached)
Property  40: file_write.countData = "false" (cached)
Property  41: file_write.bytesPerSecond = "80008"
```

### Known Issues

* The OpenCPI environment script can cause a reboot of the platform due to a conflict with Ettus Module Peripheral Manager (MPM).
