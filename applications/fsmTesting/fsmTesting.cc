#include <iostream>
#include <string>
#include "OcpiApi.hh"

// Version 2.4.1
#include "OcpiDebugApi.hh"

// Version 2.2.1
//#include "OcpiOsDebugApi.hh"

namespace OA = OCPI::API;

#include <map>
#include <set>
#include <vector>
#include <bitset>
#include <iomanip> // std::setfill
#include <sstream> // std::stringstream
#include <stdint.h>
#include <array>
#include <regex>
#include <pthread.h>
#include <stdint.h>
#include <unistd.h>


namespace OA = OCPI::API;

#define releaseBuild 1

typedef uint8_t register_address;
typedef uint16_t register_value;


const std::array<register_value, 16> thebitarray{
{1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,16384,32768,}
};

const std::array<register_value, 16> thebitarray_hightolow{
{32768, 16384, 8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1}
};


void print_uint16_register_in_binary(uint16_t the_input)
{
    std::stringstream ss;
    
    ss << std::hex << the_input;
    unsigned n;
    ss >> n;
    std::bitset<16> b(n);
    std::cout << b.to_string();
}

void print_uint16_register_in_binary_dontcares(uint16_t the_input, uint16_t the_care)
{
    std::stringstream ss;
    
    ss << std::hex << the_input;
    unsigned n;
    ss >> n;
    std::bitset<16> b(n);

    std::string tempbits = b.to_string();
             
    volatile uint16_t temp_care = the_care;
    for(int i = 0; i < 16; i++)
    {
        // This is inverted because the string at position 0
        // is the highest value in the string
        // however, before, when doing the math, we were 
        // using the regular bit array.
        if(thebitarray[i] == (temp_care & thebitarray[i])) // valid 
        {
            // do nothing
        }
        else
        {
            tempbits[15 - i] = 'X';
        }

        temp_care = the_care;
    }

    std::cout << tempbits << std::endl;
}


std::string convert_uint8_to_hex_string(const uint8_t the_number)
{
  std::stringstream ss;
  ss << std::hex << std::setfill('0');
  ss << std::hex << std::setw(2) << static_cast<int>(the_number);
  return ss.str();
}

std::string logLMK(OA::Application & app, std::uint64_t masterClockRate_Hz = 125000000){ // @TODO: if want to make modular, add argument for LUT
  std::stringstream lmkLogOut;
  lmkLogOut << "Testing LMK04828 device on daughterboard A" << std::endl;
  // lmkLogOut << "       software  hardware" << std::endl;
  std::string columnNames = "Address || Software & MASK | Hardware & MASK | MASK | Software | Hardware";

  static const std::map<std::uint64_t,std::uint16_t> lmk04828_MemMapLUT_MUX = {
    {122.88e6,0},
    {125e6,1},
    {153.6e6,2}
  };

  // use masterClockRate to choose closest  index for the LUT
  static const std::array<std::array<std::pair<std::uint16_t,std::uint8_t>,126>,3> lmk04828_MemMapLUT = {{
    {{ // 122.88 MHz
      // LMK Register outputs by running 122.88
      {0x0000,0x10},
      {0x0002,0x00},
      {0x0003,0x06},
      {0x0004,0xD0},
      {0x0005,0x5B},
      {0x0006,0x20},
      {0x000C,0x51},
      {0x000D,0x04},
      {0x0100,0x78},
      {0x0101,0xCC},
      {0x0103,0x00},
      {0x0104,0x20},
      {0x0105,0x00},
      {0x0106,0x72},
      {0x0107,0x15},
      {0x0108,0x79},
      {0x0109,0xDC},
      {0x010B,0x00},
      {0x010C,0x00},
      {0x010D,0x00},
      {0x010E,0x70},
      {0x010F,0x55},
      {0x0110,0x78},
      {0x0111,0xCC},
      {0x0113,0x00},
      {0x0114,0x00},
      {0x0115,0x00},
      {0x0116,0xF9},
      {0x0117,0x00},
      {0x0118,0x78},
      {0x0119,0xCC},
      {0x011B,0x00},
      {0x011C,0x20},
      {0x011D,0x00},
      {0x011E,0xF1},
      {0x011F,0x00},
      {0x0120,0x79},
      {0x0121,0xDC},
      {0x0123,0x00},
      {0x0124,0x20},
      {0x0125,0x00},
      {0x0126,0x72},
      {0x0127,0x55},
      {0x0128,0x79},
      {0x0129,0xDC},
      {0x012B,0x00},
      {0x012C,0x00},
      {0x012D,0x00},
      {0x012E,0x72},
      {0x012F,0xD0},
      {0x0130,0x79},
      {0x0131,0xDC},
      {0x0133,0x00},
      {0x0134,0x20},
      {0x0135,0x00},
      {0x0136,0xF1},
      {0x0137,0x05},
      {0x0138,0x30},
      {0x0139,0x02},
      {0x013A,0x01},
      {0x013B,0xF4},
      {0x013C,0x00},
      {0x013D,0x08},
      {0x013E,0x00},
      {0x013F,0x09},
      {0x0140,0x00},
      {0x0141,0x00},
      {0x0142,0x00},
      {0x0143,0x52},
      {0x0144,0xFF},
      {0x0145,0x7F},
      {0x0146,0x10},
      {0x0147,0x1A},
      {0x0148,0x33},
      {0x0149,0x01},
      {0x014A,0x02},
      {0x014B,0x01},
      {0x014C,0xF6},
      {0x014D,0x00},
      {0x014E,0x00},
      {0x014F,0x7F},
      {0x0150,0x00},
      {0x0151,0x02},
      {0x0152,0x00},
      {0x0153,0x00},
      {0x0154,0x78},
      {0x0155,0x00},
      {0x0156,0x19},
      {0x0157,0x00},
      {0x0158,0x01},
      {0x0159,0x00},
      {0x015A,0x80},
      {0x015B,0xCF},
      {0x015C,0x2},
      {0x015D,0x10},
      {0x015E,0x00},
      {0x015F,0x0B},
      {0x0160,0x00},
      {0x0161,0x04},
      {0x0162,0x44},
      {0x0163,0x00},
      {0x0164,0x00},
      {0x0165,0x19},
      {0x0171,0xAA},
      {0x0172,0x02},
      {0x017C,0x15},
      {0x017D,0x33},
      {0x0166,0x00},
      {0x0167,0x00},
      {0x0168,0x40},
      {0x0169,0x51},
      {0x016A,0x27},
      {0x016B,0x10},
      {0x016C,0x00},
      {0x016D,0x00},
      {0x016E,0x13},
      {0x0173,0x00},
      {0x0174,0x00},
      {0x0182,0x04},
      {0x0183,0x02},
      {0x0184,0x90},
      {0x0185,0x00},
      {0x0188,0x00},
      {0x1FFD,0x00},
      {0x1FFE,0x00},
      {0x1FFF,0x53}
    }},
    {{ // 125 MHz
      {0x0000,0x10},
      {0x0002,0x00},
      {0x0003,0x06},
      {0x0004,0xD0},
      {0x0005,0x5B},
      {0x0006,0x20},
      {0x000C,0x51},
      {0x000D,0x04},
      {0x0100,0x78},
      {0x0101,0xCC},
      {0x0103,0x00},
      {0x0104,0x20},
      {0x0105,0x00},
      {0x0106,0x72},
      {0x0107,0x15},
      {0x0108,0x78},
      {0x0109,0xCC},
      {0x010B,0x00},
      {0x010C,0x00},
      {0x010D,0x00},
      {0x010E,0x70},
      {0x010F,0x55},
      {0x0110,0x78},
      {0x0111,0xCC},
      {0x0113,0x00},
      {0x0114,0x00},
      {0x0115,0x00},
      {0x0116,0xF9},
      {0x0117,0x00},
      {0x0118,0x78},
      {0x0119,0xCC},
      {0x011B,0x00},
      {0x011C,0x20},
      {0x011D,0x00},
      {0x011E,0xF1},
      {0x011F,0x00},
      {0x0120,0x78},
      {0x0121,0xCC},
      {0x0123,0x00},
      {0x0124,0x20},
      {0x0125,0x00},
      {0x0126,0x72},
      {0x0127,0x55},
      {0x0128,0x78},
      {0x0129,0xCC},
      {0x012B,0x00},
      {0x012C,0x00},
      {0x012D,0x00},
      {0x012E,0x72},
      {0x012F,0xD0},
      {0x0130,0x78},
      {0x0131,0xCC},
      {0x0133,0x00},
      {0x0134,0x20},
      {0x0135,0x00},
      {0x0136,0xF1},
      {0x0137,0x05},
      {0x0138,0x30},
      {0x0139,0x02},
      {0x013A,0x01},
      {0x013B,0xE0},
      {0x013C,0x00},
      {0x013D,0x08},
      {0x013E,0x00},
      {0x013F,0x09},
      {0x0140,0x00},
      {0x0141,0x00},
      {0x0142,0x00},
      {0x0143,0x52},
      {0x0144,0xFF},
      {0x0145,0x7F},
      {0x0146,0x10},
      {0x0147,0x1A},
      {0x0148,0x33},
      {0x0149,0x01},
      {0x014A,0x02},
      {0x014B,0x01},
      {0x014C,0xF6},
      {0x014D,0x00},
      {0x014E,0x00},
      {0x014F,0x7F},
      {0x0150,0x00},
      {0x0151,0x02},
      {0x0152,0x00},
      {0x0153,0x00},
      {0x0154,0x78},
      {0x0155,0x00},
      {0x0156,0x19},
      {0x0157,0x00},
      {0x0158,0x01},
      {0x0159,0x00},
      {0x015A,0x7D},
      {0x015B,0xCF},
      {0x015C,0x27},
      {0x015D,0x10},
      {0x015E,0x00},
      {0x015F,0x0B},
      {0x0160,0x00},
      {0x0161,0x04},
      {0x0162,0xA4},
      {0x0163,0x00},
      {0x0164,0x00},
      {0x0165,0x19},
      {0x0171,0xAA},
      {0x0172,0x02},
      {0x017C,0x15},
      {0x017D,0x33},
      {0x0166,0x00},
      {0x0167,0x00},
      {0x0168,0x19},
      {0x0169,0x51},
      {0x016A,0x27},
      {0x016B,0x10},
      {0x016C,0x00},
      {0x016D,0x00},
      {0x016E,0x13},
      {0x0173,0x00},
      {0x0174,0x00},
      {0x0182,0x00},
      {0x0183,0x00},
      {0x0184,0x80},
      {0x0185,0x00},
      {0x0188,0x00},
      {0x1FFD,0x00},
      {0x1FFE,0x00},
      {0x1FFF,0x53}
    }},
    {{ // 153.6 MHz
      // Register outputs by running them
      {0x0000,0x10},
      {0x0002,0x00},
      {0x0003,0x06},
      {0x0004,0xD0},
      {0x0005,0x5B},
      {0x0006,0x20},
      {0x000C,0x51},
      {0x000D,0x04},
      {0x0100,0x74},
      {0x0101,0xAA},
      {0x0103,0x00},
      {0x0104,0x20},
      {0x0105,0x00},
      {0x0106,0x72},
      {0x0107,0x15},
      {0x0108,0x74},
      {0x0109,0xAA},
      {0x010B,0x00},
      {0x010C,0x00},
      {0x010D,0x00},
      {0x010E,0x70},
      {0x010F,0x55},
      {0x0110,0x74},
      {0x0111,0xAA},
      {0x0113,0x00},
      {0x0114,0x00},
      {0x0115,0x00},
      {0x0116,0xF9},
      {0x0117,0x00},
      {0x0118,0x78},
      {0x0119,0xAC},
      {0x011B,0x00},
      {0x011C,0x20},
      {0x011D,0x00},
      {0x011E,0xF1},
      {0x011F,0x00},
      {0x0120,0x74},
      {0x0121,0xAA},
      {0x0123,0x00},
      {0x0124,0x20},
      {0x0125,0x00},
      {0x0126,0x72},
      {0x0127,0x55},
      {0x0128,0x74},
      {0x0129,0xAA},
      {0x012B,0x00},
      {0x012C,0x00},
      {0x012D,0x00},
      {0x012E,0x72},
      {0x012F,0xD0},
      {0x0130,0x74},
      {0x0131,0xAA},
      {0x0133,0x00},
      {0x0134,0x20},
      {0x0135,0x00},
      {0x0136,0xF1},
      {0x0137,0x05},
      {0x0138,0x30},
      {0x0139,0x02},
      {0x013A,0x01},
      {0x013B,0x90},
      {0x013C,0x00},
      {0x013D,0x08},
      {0x013E,0x00},
      {0x013F,0x09},
      {0x0140,0x00},
      {0x0141,0x00},
      {0x0142,0x00},
      {0x0143,0x52},
      {0x0144,0xFF},
      {0x0145,0x7F},
      {0x0146,0x10},
      {0x0147,0x1A},
      {0x0148,0x33},
      {0x0149,0x01},
      {0x014A,0x02},
      {0x014B,0x01},
      {0x014C,0xF6},
      {0x014D,0x00},
      {0x014E,0x00},
      {0x014F,0x7F},
      {0x0150,0x00},
      {0x0151,0x02},
      {0x0152,0x00},
      {0x0153,0x00},
      {0x0154,0x78},
      {0x0155,0x00},
      {0x0156,0x19},
      {0x0157,0x00},
      {0x0158,0x01},
      {0x0159,0x00},
      {0x015A,0x80},
      {0x015B,0xCF},
      {0x015C,0x02},
      {0x015D,0x10},
      {0x015E,0x00},
      {0x015F,0x0B},
      {0x0160,0x00},
      {0x0161,0x04},
      {0x0162,0x44},
      {0x0163,0x00},
      {0x0164,0x00},
      {0x0165,0x19},
      {0x0171,0xAA},
      {0x0172,0x02},
      {0x017C,0x15},
      {0x017D,0x33},
      {0x0166,0x00},
      {0x0167,0x00},
      {0x0168,0x19},
      {0x0169,0x51},
      {0x016A,0x27},
      {0x016B,0x10},
      {0x016C,0x00},
      {0x016D,0x00},
      {0x016E,0x13},
      {0x0173,0x00},
      {0x0174,0x00},
      {0x0182,0x04},
      {0x0183,0x02},
      {0x0184,0x90},
      {0x0185,0x00},
      {0x0188,0x00},
      {0x1FFD,0x00},
      {0x1FFE,0x00},
      {0x1FFF,0x53}
    }}
  }};

  static const std::array<std::pair<std::uint16_t,std::uint8_t>,126> lmk04828_MemMapLUT_MASK = {{
    {0x0000,0xFF},
    {0x0002,0xFF},
    {0x0003,0xFF},
    {0x0004,0xFF},
    {0x0005,0xFF},
    {0x0006,0xFF},
    {0x000C,0xFF},
    {0x000D,0xFF},
    {0x0100,0xFF},
    {0x0101,0xFF},
    {0x0103,0xFF},
    {0x0104,0xFF},
    {0x0105,0xFF},
    {0x0106,0xFF},
    {0x0107,0xFF},
    {0x0108,0xFF},
    {0x0109,0xFF},
    {0x010B,0xFF},
    {0x010C,0xFF},
    {0x010D,0xFF},
    {0x010E,0xFF},
    {0x010F,0xFF},
    {0x0110,0xFF},
    {0x0111,0xFF},
    {0x0113,0xFF},
    {0x0114,0xFF},
    {0x0115,0xFF},
    {0x0116,0xFF},
    {0x0117,0xFF},
    {0x0118,0xFF},
    {0x0119,0xFF},
    {0x011B,0xFF},
    {0x011C,0xFF},
    {0x011D,0xFF},
    {0x011E,0xFF},
    {0x011F,0xFF},
    {0x0120,0xFF},
    {0x0121,0xFF},
    {0x0123,0xFF},
    {0x0124,0xFF},
    {0x0125,0xFF},
    {0x0126,0xFF},
    {0x0127,0xFF},
    {0x0128,0xFF},
    {0x0129,0xFF},
    {0x012B,0xFF},
    {0x012C,0xFF},
    {0x012D,0xFF},
    {0x012E,0xFF},
    {0x012F,0xFF},
    {0x0130,0xFF},
    {0x0131,0xFF},
    {0x0133,0xFF},
    {0x0134,0xFF},
    {0x0135,0xFF},
    {0x0136,0xFF},
    {0x0137,0xFF},
    {0x0138,0xFF},
    {0x0139,0xFF},
    {0x013A,0xFF},
    {0x013B,0xFF},
    {0x013C,0xFF},
    {0x013D,0xFF},
    {0x013E,0xFF},
    {0x013F,0xFF},
    {0x0140,0xFF},
    {0x0141,0xFF},
    {0x0142,0xFF},
    {0x0143,0xFF},
    {0x0144,0xFF},
    {0x0145,0xFF},
    {0x0146,0xFF},
    {0x0147,0xFF},
    {0x0148,0xFF},
    {0x0149,0xFF},
    {0x014A,0xFF},
    {0x014B,0xFF},
    {0x014C,0xFF},
    {0x014D,0xFF},
    {0x014E,0xFF},
    {0x014F,0xFF},
    {0x0150,0xFF},
    {0x0151,0xFF},
    {0x0152,0xFF},
    {0x0153,0xFF},
    {0x0154,0xFF},
    {0x0155,0xFF},
    {0x0156,0xFF},
    {0x0157,0xFF},
    {0x0158,0xFF},
    {0x0159,0xFF},
    {0x015A,0xFF},
    {0x015B,0xFF},
    {0x015C,0xFF},
    {0x015D,0xFF},
    {0x015E,0xFF},
    {0x015F,0xFF},
    {0x0160,0xFF},
    {0x0161,0xFF},
    {0x0162,0xFF},
    {0x0163,0xFF},
    {0x0164,0xFF},
    {0x0165,0xFF},
    {0x0171,0xFF},
    {0x0172,0xFF},
    {0x017C,0xFF},
    {0x017D,0xFF},
    {0x0166,0xFF},
    {0x0167,0xFF},
    {0x0168,0xFF},
    {0x0169,0xFF},
    {0x016A,0xFF},
    {0x016B,0xFF},
    {0x016C,0xFF},
    {0x016D,0xFF},
    {0x016E,0xFF},
    {0x0173,0xFF},
    {0x0174,0xFF},
    {0x0182,0x00}, // readback values only
    {0x0183,0x00}, // readback values only
    {0x0184,0x00}, // readback values only
    {0x0185,0x00}, // readback values only
    {0x0188,0x00}, // readback values only
    {0x1FFD,0x00}, // cannot be read back
    {0x1FFE,0x00}, // cannot be read back
    {0x1FFF,0x00} // cannot be read back
  }};

  for(std::uint32_t index = 0; index < lmk04828_MemMapLUT.at(lmk04828_MemMapLUT_MUX.at(masterClockRate_Hz)).size(); index++){
    if(not(index % 30)){ // arbitrary count for when to place the column titles again
      lmkLogOut << "\n" << columnNames << "\n" << std::endl;
    }
    auto lmkReg = lmk04828_MemMapLUT.at(lmk04828_MemMapLUT_MUX.at(masterClockRate_Hz)).at(index);
    std::uint8_t softwareReg = lmkReg.second; // softwareRegisterValue
    std::uint16_t address = lmkReg.first;
    auto regMask = lmk04828_MemMapLUT_MASK.at(index).second;
    // prepare SPI command address field
    app.setPropertyValue<std::uint64_t>("drc_n310","spiControlStruct",address,{"address"});
    // app.setPropertyValue<>("drc_n310","spiControlStruct",,{"readback"});
    // execute SPI command
    app.setProperty("drc_n310.lmk04828ControllerOrdinal","spiReadWrite");
    // store lmk hardware register value
    std::uint8_t hardwareReg = app.getPropertyValue<std::uint64_t>("drc_n310","spiReadback");
    // Address
    lmkLogOut << "0x" << std::setfill('0') << std::setw(4) << std::hex << address;
    lmkLogOut << "  || "; // aligned column = 6 0xXXXX + 5 spaces
    // Software & MASK
    lmkLogOut << "0x" << std::setfill('0') << std::setw(2) << std::hex << static_cast<std::uint16_t>((softwareReg) & (regMask)); // software
    lmkLogOut << "            | "; // aligned column = 4 0xXX + 14 spaces
    // Hardware & MASK
    lmkLogOut << "0x" << std::setfill('0') << std::setw(2) << std::hex << static_cast<std::uint16_t>((hardwareReg) & (regMask)); // hardware
    lmkLogOut << "            | "; // aligned column = 4 0xXXXX + 5 spaces
    // MASK
    lmkLogOut << "0x" << std::setfill('0') << std::setw(2) << std::hex << static_cast<std::uint16_t>(regMask);
    lmkLogOut << " | "; // aligned column = 4 0xXX + 3 spaces
    // Software
    lmkLogOut << "0x" << std::setfill('0') << std::setw(2) << std::hex << static_cast<std::uint16_t>(softwareReg);
    lmkLogOut << "     | "; // aligned column = 4 0xXX + 7 spaces
    // Hardware
    lmkLogOut << "0x" << std::setfill('0') << std::setw(2) << std::hex << static_cast<std::uint16_t>(hardwareReg);
    if((softwareReg & regMask) != (hardwareReg & regMask)){ // unexpected value recorded 
      lmkLogOut << " -"; // denote that there's a difference
    }
    lmkLogOut << "\n";
  }
  lmkLogOut << "\n";
  return lmkLogOut.str();
}

std::string logCPLD(OA::Application & app, std::uint16_t LUT_Select){ // @TODO: if want to make modular, add argument for LUT
  std::stringstream cpldLogOut;
  cpldLogOut << "Testing CPLD device on daughterboard A" << std::endl;
  cpldLogOut << "Address || Test LUT & MASK | Hardware & MASK | MASK   | Test LUT | Hardware | Register Descriptions\n";
  cpldLogOut << "--------++-----------------+-----------------+--------+----------+----------+----------------------\n";


  volatile register_address cpldRegisterAddress;
  volatile register_value cpldRegisterValue;


  static const std::array<std::array<std::pair<std::uint8_t,std::uint16_t>,24>,16> cpld_MemMapLUT = {{
    {{  // RX -- Lowband
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x10f3}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x55,0x05b9}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x10f3}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x05b9}, // RxCh2_1_RxOn
    }},
    {{  // RX -- 440
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x04f3}, // RxCh1_0_Idle
      {0x52,0x0001}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x04c7}, // RxCh1_0_RxOn
      {0x55,0x05a1}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x04f3}, // RxCh2_0_Idle
      {0x62,0x0001}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x04c7}, // RxCh2_0_RxOn
      {0x65,0x05a1}  // RxCh2_1_RxOn 
    }},
    {{  // RX -- 650
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x20f3}, // RxCh1_0_Idle
      {0x52,0x0001}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x20d7}, // RxCh1_0_RxOn
      {0x55,0x05a1}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x20f3}, // RxCh2_0_Idle
      {0x62,0x0001}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x20d7}, // RxCh2_0_RxOn
      {0x65,0x05a1}, // RxCh2_1_RxOn
    }},
    {{  // RX -- 1100
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0x19ab}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x08f3}, // RxCh1_0_Idle
      {0x52,0x0001}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x08e7}, // RxCh1_0_RxOn
      {0x55,0x05a1}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x08f3}, // RxCh2_0_Idle
      {0x62,0x0001}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x08e7}, // RxCh2_0_RxOn
      {0x65,0x05a1}, // RxCh2_1_RxOn
    }},
    {{  // RX -- 1600
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x1173}, // RxCh1_0_Idle
      {0x52,0x0002}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x1127}, // RxCh1_0_RxOn
      {0x55,0x0562}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x0173}, // RxCh2_0_Idle
      {0x62,0x0002}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x1127}, // RxCh2_0_RxOn
      {0x65,0x0562}, // RxCh2_1_RxOn
    }},
    {{  // RX -- 2100
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x10f3}, // RxCh1_0_Idle
      {0x52,0x0002}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x1087}, // RxCh1_0_RxOn
      {0x55,0x0562}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x10f3}, // RxCh2_0_Idle
      {0x62,0x0002}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x1087}, // RxCh2_0_RxOn
      {0x65,0x0562}, // RxCh2_1_RxOn
    }},
    {{  // RX -- 2750
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x1273}, // RxCh1_0_Idle
      {0x52,0x0002}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x127f}, // RxCh1_0_RxOn
      {0x55,0x0562}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x1273}, // RxCh2_0_Idle
      {0x62,0x0002}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x127f}, // RxCh2_0_RxOn
      {0x65,0x0562}, // RxCh2_1_RxOn
    }},
    {{  // RX -- Wideband
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x10f3}, // RxCh1_0_Idle
      {0x52,0x0004}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x10fb}, // RxCh1_0_RxOn
      {0x55,0x0524}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x10f3}, // RxCh2_0_Idle
      {0x62,0x0004}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x10fb}, // RxCh2_0_RxOn
      {0x65,0x0524}, // RxCh2_1_RxOn
    }},
    {{  // RX -- Wideband superheterodyne
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0093}, // Tx1Ch1_Idle
      {0x51,0x10f3}, // RxCh1_0_Idle
      {0x52,0x000c}, // RxCh1_1_Idle
      {0x53,0x0693}, // TxCh1_TxOn
      {0x54,0x10fb}, // RxCh1_0_RxOn
      {0x55,0x053c}, // RxCh1_1_RxOn
      {0x60,0x00cf}, // TxCh2_Idle
      {0x61,0x10f3}, // RxCh2_0_Idle
      {0x62,0x000c}, // RxCh2_1_Idle
      {0x63,0x0693}, // TxCh2_TxOn
      {0x64,0x10fb}, // RxCh2_0_RxOn
      {0x65,0x053c}, // RxCh2_1_RxOn
    }},
    {{  // TX -- Lowband
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0090}, // Tx1Ch1_Idle
      {0x51,0x1097}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x6793}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x55,0x07b9}, // RxCh1_1_RxOn
      {0x60,0x0090}, // TxCh2_Idle
      {0x61,0x1097}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x6793}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x07b9}, // RxCh2_1_RxOn
    }},
    {{  // TX -- 440
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0010}, // Tx1Ch1_Idle
      {0x51,0x1097}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x6613}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x60,0x0010}, // TxCh2_Idle
      {0x55,0x07b9}, // RxCh1_1_RxOn
      {0x61,0x1097}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x6613}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x07b9}, // RxCh2_1_RxOn
    }},
    {{  // TX -- 750
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0008}, // Tx1Ch1_Idle
      {0x51,0x1097}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x6609}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x55,0x07b9}, // RxCh1_1_RxOn
      {0x60,0x0008}, // TxCh2_Idle
      {0x61,0x1097}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x6609}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x07b9}, // RxCh2_1_RxOn
    }},
    {{  // TX -- 1500
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0004}, // Tx1Ch1_Idle
      {0x51,0x1097}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x6606}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x55,0x07b9}, // RxCh1_1_RxOn
      {0x60,0x0004}, // TxCh2_Idle
      {0x61,0x1097}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x6606}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x07b9}, // RxCh2_1_RxOn
    }},
    {{  // TX -- 3000
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x0820}, // Tx1Ch1_Idle
      {0x51,0x1097}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x6e20}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x55,0x07b9}, // RxCh1_1_RxOn
      {0x60,0x0820}, // TxCh2_Idle
      {0x61,0x1097}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x6e20}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x07b9}, // RxCh2_1_RxOn
    }},
    {{  // TX -- Wideband
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x1850}, // Tx1Ch1_Idle
      {0x51,0x1097}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x7a50}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x55,0x07b9}, // RxCh1_1_RxOn
      {0x60,0x1850}, // TxCh2_Idle
      {0x61,0x1097}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x7a50}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x07b9}, // RxCh2_1_RxOn
    }},
    {{  // TX -- Wideband superheterodyne
      {0x00,0xcafe}, // PS_SignatureReg
      {0x01,0x0000}, // PS_MinorRevReg
      {0x02,0x0005}, // PS_MajorRevReg 
      {0x03,0x0408}, // PS_BuildCodeLSB
      {0x04,0x1801}, // PS_BuildCodeMSB
      {0x05,0x0000}, // PS_Scratch
      {0x0a,0x0000}, // PS_PS_CpldControl - WritableOnly
      {0x0b,0x0000}, // PS_LmkControl | RxLoLockDetect
      {0x0c,0x0000}, // PS_LoStatus | TxLoLockDetect 
      {0x0d,0x0000}, // PS_MykonosControl | MykonosReset
      {0x40,0xbae0}, // PlScratch
      {0x41,0x0000}, // PLCpldControl | cpld_reset
      {0x50,0x18d0}, // Tx1Ch1_Idle
      {0x51,0x1097}, // RxCh1_0_Idle
      {0x52,0x0009}, // RxCh1_1_Idle
      {0x53,0x7bd0}, // TxCh1_TxOn
      {0x54,0x1097}, // RxCh1_0_RxOn
      {0x55,0x07b9}, // RxCh1_1_RxOn
      {0x60,0x18d0}, // TxCh2_Idle
      {0x61,0x1097}, // RxCh2_0_Idle
      {0x62,0x0009}, // RxCh2_1_Idle
      {0x63,0x7bd0}, // TxCh2_TxOn
      {0x64,0x1097}, // RxCh2_0_RxOn
      {0x65,0x07b9}, // RxCh2_1_RxOn
    }},
  }};

  static const std::array<std::string,24> regDescriptionLUT = {{
    "PS_SignatureReg",
    "PS_MinorRevReg",
    "PS_MajorRevReg ",
    "PS_BuildCodeLSB",
    "PS_BuildCodeMSB",
    "PS_Scratch",
    "PS_PS_CpldControl - WritableOnly",
    "PS_LmkControl | RxLoLockDetect",
    "PS_LoStatus | TxLoLockDetect ",
    "PS_MykonosControl | MykonosReset",
    "PlScratch",
    "PLCpldControl | cpld_reset",
    "Tx1Ch1_Idle",
    "RxCh1_0_Idle",
    "RxCh1_1_Idle",
    "TxCh1_TxOn",
    "RxCh1_0_RxOn",
    "RxCh1_1_RxOn",
    "TxCh2_Idle",
    "RxCh2_0_Idle",
    "RxCh2_1_Idle",
    "TxCh2_TxOn",
    "RxCh2_0_RxOn",
    "RxCh2_1_RxOn",
  }};
  // only bits set that don't care about
  // Note: Maybe better to make this one an array of std::maps such that can be referenced simply by address key
  static const std::array<std::pair<std::uint8_t,std::uint16_t>,24> rx_reg_MASK_upperBank = {{
    {0x00,0x0000},
    {0x01,0x0000},
    {0x02,0x0000},
    {0x03,0x0000},
    {0x04,0x0000},
    {0x05,0x0000},
    {0x0a,0x0000},
    {0x0b,0x0000},
    {0x0c,0x0000},
    {0x0d,0x0000},
    {0x40,0xFFFF},
    {0x41,0x0000},
    {0x50,0xFFFF}, // Tx1Ch1_Idle
    {0x51,0xFC00}, // RxCh1_0_Idle
    {0x52,0xF800}, // RxCh1_1_Idle
    {0x53,0xFFFF}, // TxCh1_TxOn
    {0x54,0xFC00}, // RxCh1_0_RxOn
    {0x55,0xF800}, // RxCh1_1_RxOn
    {0x60,0xFFFF}, // TxCh2_Idle
    {0x61,0xFC00}, // RxCh2_0_Idle
    {0x62,0xF800}, // RxCh2_1_Idle
    {0x63,0xFFFF}, // TxCh2_TxOn
    {0x64,0xFC00}, // RxCh2_0_RxOn
    {0x65,0xF800}  // RxCh2_1_RxOn
  }};
  static const std::array<std::pair<std::uint8_t,std::uint16_t>,24> rx_reg_MASK_lowerBank = {{
    {0x00,0x0000},
    {0x01,0x0000},
    {0x02,0x0000},
    {0x03,0x0000},
    {0x04,0x0000},
    {0x05,0x0000},
    {0x0a,0x0000},
    {0x0b,0x0000},
    {0x0c,0x0000},
    {0x0d,0x0000},
    {0x40,0xFFFF},
    {0x41,0x0000},
    {0x50,0xFFFF}, // Tx1Ch1_Idle
    {0x51,0xC380}, // RxCh1_0_Idle
    {0x52,0xF800}, // RxCh1_1_Idle
    {0x53,0xFFFF}, // TxCh1_TxOn
    {0x54,0xC380}, // RxCh1_0_RxOn
    {0x55,0xF800}, // RxCh1_1_RxOn
    {0x60,0xFFFF}, // TxCh2_Idle
    {0x61,0xC380}, // RxCh2_0_Idle
    {0x62,0xF800}, // RxCh2_1_Idle
    {0x63,0xFFFF}, // TxCh2_TxOn
    {0x64,0xC380}, // RxCh2_0_RxOn
    {0x65,0xF800}  // RxCh2_1_RxOn
  }};
  static const std::array<std::pair<std::uint8_t,std::uint16_t>,24> rx_reg_MASK_bypass = {{
    {0x00,0x0000},
    {0x01,0x0000},
    {0x02,0x0000},
    {0x03,0x0000},
    {0x04,0x0000},
    {0x05,0x0000},
    {0x0a,0x0000},
    {0x0b,0x0000},
    {0x0c,0x0000},
    {0x0d,0x0000},
    {0x40,0xFFFF},
    {0x41,0x0000},
    {0x50,0xFFFF}, // Tx1Ch1_Idle
    {0x51,0xFF80}, // RxCh1_0_Idle
    {0x52,0xF800}, // RxCh1_1_Idle
    {0x53,0xFFFF}, // TxCh1_TxOn
    {0x54,0xFF80}, // RxCh1_0_RxOn
    {0x55,0xF800}, // RxCh1_1_RxOn
    {0x60,0xFFFF}, // TxCh2_Idle
    {0x61,0xFF80}, // RxCh2_0_Idle
    {0x62,0xF800}, // RxCh2_1_Idle
    {0x63,0xFFFF}, // TxCh2_TxOn
    {0x64,0xFF80}, // RxCh2_0_RxOn
    {0x65,0xF800}  // RxCh2_1_RxOn
  }};
  static const std::array<std::pair<std::uint8_t,std::uint16_t>,24> tx_reg_MASK = {{
    {0x00,0x0000},
    {0x01,0x0000},
    {0x02,0x0000},
    {0x03,0x0000},
    {0x04,0x0000},
    {0x05,0x0000},
    {0x0a,0x0000},
    {0x0b,0x0000},
    {0x0c,0x0000},
    {0x0d,0x0000},
    {0x40,0xFFFF},
    {0x41,0x0000},
    {0x50,0x8000}, // Tx1Ch1_Idle
    {0x51,0xFFFF}, // RxCh1_0_Idle
    {0x52,0xFFFF}, // RxCh1_1_Idle
    {0x53,0x8000}, // TxCh1_TxOn
    {0x54,0xFFFF}, // RxCh1_0_RxOn
    {0x55,0xFFFF}, // RxCh1_1_RxOn
    {0x60,0x8000}, // TxCh2_Idle
    {0x61,0xFFFF}, // RxCh2_0_Idle
    {0x62,0xFFFF}, // RxCh2_1_Idle
    {0x63,0x8000}, // TxCh2_TxOn
    {0x64,0xFFFF}, // RxCh2_0_RxOn
    {0x65,0xFFFF}, // RxCh2_1_RxOn
  }};
  static const std::array<std::array<std::pair<std::uint8_t,std::uint16_t>,24>,16> cpld_MemMapLUT_MASK = {{
    rx_reg_MASK_lowerBank,
    rx_reg_MASK_lowerBank,
    rx_reg_MASK_lowerBank,
    rx_reg_MASK_lowerBank,
    rx_reg_MASK_upperBank,
    rx_reg_MASK_upperBank,
    rx_reg_MASK_upperBank,
    rx_reg_MASK_bypass,
    rx_reg_MASK_bypass,
    tx_reg_MASK,
    tx_reg_MASK,
    tx_reg_MASK,
    tx_reg_MASK,
    tx_reg_MASK,
    tx_reg_MASK,
    tx_reg_MASK
  }};

  for(std::uint16_t regIndex = 0; regIndex < cpld_MemMapLUT.at(LUT_Select).size(); regIndex++){
    auto cpldNode = cpld_MemMapLUT.at(LUT_Select).at(regIndex);
    cpldRegisterAddress = static_cast<register_address>(cpldNode.first);
    // prepare SPI command address field
    //std::cout << cpldNode.first << std::endl;
    app.setPropertyValue<std::uint64_t>("drc_n310", "spiControlStruct", cpldRegisterAddress, {"address"});
    // execute SPI command
    app.setProperty("drc_n310.cpldControllerOrdinal", "spiReadWrite");
    // store hardware address
    cpldRegisterValue = app.getPropertyValue<std::uint64_t>("drc_n310","spiReadback");
      
    // Address
    cpldLogOut << "0x" << std::setfill('0') << std::setw(2) << std::hex << convert_uint8_to_hex_string(cpldRegisterAddress);
    cpldLogOut << "    || "; // aligned column = 4 0xXX + 7 spaces
    // Software & MASK
    cpldLogOut << "0x" << std::setfill('0') << std::setw(4) << std::hex << (cpldNode.second & (~cpld_MemMapLUT_MASK.at(LUT_Select).at(regIndex).second));
    cpldLogOut << "          | "; // aligned column = 6 0xXXXX + 12 spaces
    // Hardware & MASK
    std::uint16_t maskedHardwareValue = cpldRegisterValue & cpld_MemMapLUT_MASK.at(LUT_Select).at(regIndex).second;
    cpldLogOut << "0x" << std::setfill('0') << std::setw(4) << std::hex << (cpldRegisterValue & (~cpld_MemMapLUT_MASK.at(LUT_Select).at(regIndex).second));
    cpldLogOut << "          | "; // aligned column = 6 0xXXXX + 12 spaces
    // MASK
    cpldLogOut << "0x" << std::setfill('0') << std::setw(4) << std::hex << static_cast<std::uint16_t>(~cpld_MemMapLUT_MASK.at(LUT_Select).at(regIndex).second);
    cpldLogOut << " | "; // aligned column = 6 0xXXXX + 12 spaces
    // Software
    cpldLogOut << "0x" << std::setfill('0') << std::setw(4) << std::hex << cpldNode.second;
    cpldLogOut << "   | "; // aligned column = 6 0xXXXX + 5 spaces
    // Hardware
    cpldLogOut << "0x" << std::setfill('0') << std::setw(4) << std::hex << cpldRegisterValue;

    // bool deltaFlag = false;
    if(
        (cpldNode.second & (~cpld_MemMapLUT_MASK.at(LUT_Select).at(regIndex).second)) // software & MASK
        !=
        (cpldRegisterValue & (~cpld_MemMapLUT_MASK.at(LUT_Select).at(regIndex).second)) // hardware & MASK
      )
    {
      // deltaFlag = true;
      cpldLogOut << " - | "; // add dash at the end to indicate values don't match
    } else {
      cpldLogOut << "   | ";
    }
    // cpldLogOut << (deltaFlag) ? " | " : "   | ";
    cpldLogOut << regDescriptionLUT.at(regIndex);
    cpldLogOut << "\n";
  }
  cpldLogOut << "\n";
  return cpldLogOut.str();
}

const std::array<std::pair<std::string, std::vector<std::string>>,4> testCases = {{
  {
    "FSM Tests",
    {
      "prepare x2, start x2, stop x2, release x2",
      "start, stop release",
      "stop",
      "rf_port_num vs T/RX port constraint failure",
      "redundant rf_port_num channels configured",
      "Tolerance Failure -- tuning frequency",
      "Tolerance Failure -- RF Bandwidth",
      "Tolerance Failure -- Gain",
      "Dual Channel Type Deployment",
    }
  },
  {
    "Application Tests",
    {
      "DBA AND DBB - TX0 TX1 RX0 RX1  - 96 MHZ",
      "DBA         - TX0 RX1          - 96 MHZ",
      "DBB         - TX1 RX0          - 96 MHZ",
      "DBA         - TX0 TX1 RX0 RX1  - 90 MHZ", 
      "DBB         - TX0 TX1 RX0 RX1  - 90 MHZ",
      "Toggle individual channels across both daughterboards",
      "Library Verification",
      "DBA AND DBB - TX0 TX1 RX0 RX1  - 75 MHZ",
      "Cycle Bandwidth"
    }
  },
  {
    "LMK04828 Tests",
    {
      "125 MHz Master Clock Rate",
      "122.88 MHz Master Clock Rate",
      "153.6 MHz Master Clock Rate"
    }
  },
  {
    "Front End (CPLD) Tests",
    {
      "RX -- Lowband",
      "RX -- 440",
      "RX -- 650",
      "RX -- 1100",
      "RX -- 1600",
      "RX -- 2100",
      "RX -- 2750",
      "RX -- Wideband",
      "RX -- Wideband superheterodyne",
      "TX -- Lowband",
      "TX -- 440",
      "TX -- 750",
      "TX -- 1500",
      "TX -- 3000",
      "TX -- Wideband",
      "TX -- Wideband superheterodyne"
    }
  },
}};

struct testSelection {
  std::uint16_t categorySelect, testSelect; // make these enumerations?
  testSelection(std::uint16_t _categorySelect,std::uint16_t _testSelect)
    :
      categorySelect(_categorySelect),
      testSelect(_testSelect)
    { }
};

enum _stateHelper {
  inactive = 0,
  prepared = 1,
  operating= 2,
  error    = 3
};

std::array<const std::string,4> stateHelper = {{
  "inactive",
  "prepared",
  "operating",
  "error",
}};

auto getConfigState(OA::Application &app, std::uint16_t configOrdinal = 0)->std::string{
  std::string drcStatus_get;
  app.getProperty("drc_n310.status",drcStatus_get,OA::AccessList({configOrdinal,"state"}));
  std::cout << "config " << configOrdinal << " State = " << drcStatus_get << "\n";
  return drcStatus_get;
};


bool drc_n310_testing_suite(OA::Property &configs, OA::Application &app, struct testSelection &_testSelection){
  enum port_literals {
    DBATX0 = 0,
    DBATX1 = 1,
    DBARX0 = 2,
    DBARX1 = 3,
    DBBTX0 = 4,
    DBBTX1 = 5,
    DBBRX0 = 6,
    DBBRX1 = 7,
    OFF = 0xFF
  };
  OA::Property status(app,"drc_n310","status");
  OA::Property start(app, "drc_n310", "start");
  OA::Property stop(app, "drc_n310", "stop");
  OA::Property release(app, "drc_n310", "release");
  OA::Property prepare(app, "drc_n310", "prepare");
  std::string drcLog;
  switch (_testSelection.categorySelect){
    case 0: // FSM 
      // create initial/basic configuration, 1 TX and 1 RX
      // configuraiton 0 = RX0
      configs.setProperty(
          "rx true,"
          "rf_port_num 2," // DBA, RX channel --> xyz = 000
          "tuning_freq_mhz 380,"
          "bandwidth_3db_mhz 70,"
          "sampling_rate_Msps 96,"
          "samples_are_complex true,"
          "gain_db 25,"
          "tolerance_tuning_freq_mhz 0.01,"
          "tolerance_sampling_rate_msps 0.01,"
          "tolerance_gain_db 1,"
          "tolerance_bandwidth_3dB_MHz 10",
          { 0, "channels", 0} 
          // DRC Config 0 , Channels, Channel Configuration #0
        );
      // configuration 1 = TX0
      configs.setProperty( // setting a CHANNEL configuration
          "rx false,"
          "rf_port_num 0," // DBA, TX channel --> xyz = 010
          "tuning_freq_mhz 200,"
          "bandwidth_3db_mhz 70,"
          "sampling_rate_Msps 96,"
          "samples_are_complex true,"
          "gain_db 25,"
          "tolerance_tuning_freq_mhz 0.01,"
          "tolerance_sampling_rate_msps 0.01,"
          "tolerance_gain_db 1,"
          "tolerance_bandwidth_3dB_MHz 10",
          { 0, "channels", 1} 
          // DRC Config 0, Channels, Channel Configuration #1
        );
      switch (_testSelection.testSelect){
        case 0: // prepare, start, stop, start, stop, release
          // Given a configuration 0 that can be satisfied by the n310 hardware,
          // this sequence of FSM operations should be valid and are asserted as such
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 1: // start, stop, release
          // Given a configuration 0 that can be satisfied by the n310 hardware,
          // this sequence of FSM operations should be valid and are asserted as such
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 2: // stop
          std::cout << "App Stop with nothing else causes exception assert.\n";
          // https://gitlab.com/opencpi/opencpi/-/issues/3008
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 3: // rf_port_num vs T/RX port constraint failure
          configs.setValue(false,{0,"channels",1,"rx"}); // set RX=false -- channel type
          configs.setValue(2,{0,"channels",1,"rf_port_num"}); // select rf_port_num = 2, WHICH SHOULD FAIL
          // assignment of RX channel to ODD port number (1 == DBA)
          configs.setValue(true,{0,"channels",0,"rx"}); // set RX = true
          configs.setValue(3,{0,"channels",0,"rf_port_num"}); // set rf_port_num = 3 --> this should be fine
          // this should set the state to ERROR
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          break;
        case 4: // redundant rf_port_num channels configured
          configs.setValue(true,{0,"channels",0,"rx"}); // set RX = true
          configs.setValue(3,{0,"channels",0,"rf_port_num"}); // set rf_port_num = 3 --> this should be fine
          configs.setValue(true,{0,"channels",1,"rx"}); // set RX=false -- channel type
          configs.setValue(3,{0,"channels",1,"rf_port_num"}); // select rf_port_num = 2, which should fail
          // setting channels 0 and 1 to the same rf_port_num (and making sure rx is true)
          // this should place it in the error state
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          break;
        case 5: // Tolerance Failure -- tuning frequency
          configs.setValue(8.0,{0,"channels",1,"tuning_freq_mhz"}); // place tuning frequency below limit
          // tuning frequency will "saturate" to 10 MHz
          configs.setValue(1.0,{0,"channels",1,"tolerance_tuning_freq_mhz"}); // make tolerance 1 MHz, won't work
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 6: // Tolerance Failure -- RF Bandwidth
          configs.setValue(55.0,{0,"channels",1,"bandwidth_3db_mhz"}); // place tuning frequency below limit
          // bandwidth static configuration at 100 MHz for now
          configs.setValue(1.0,{0,"channels",1,"tolerance_bandwidth_3db_mhz"}); // make tolerance 1 MHz, won't work
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 7: // Tolerance Failure -- Gain
          configs.setValue(50.0,{0,"channels",1,"gain_db"}); // place tuning frequency below limit
          // gain saturates to 35.0
          configs.setValue(1.0,{0,"channels",1,"tolerance_gain_db"}); // make tolerance 1 MHz, won't work
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 8: // dual channel fail then pass
          // Update channel 1 to port 3 (RX1) and tunning freq = 670 MHz
          configs.setValue(3,{0,"channels",1,"rf_port_num"});
          configs.setValue(true,{0,"channels",1,"rx"});
          // tolerance for tuning frequency is 1 MHz
          configs.setValue(670,{0,"channels",1,"tuning_freq_mhz"}); 
          // initial value is 1 MHz tolerance, which we expect to fail configuration
          // configs.setValue(1.0,{0,"channels",1,"tolerance_tuning_freq_mhz"});
          prepare.setValue(0);
          // DRC prepare should FAIL -> result in error state, but to evaluate more logic before leaving, asserting inactive for now
          // assert(stateHelper.at(_stateHelper::error)==getConfigState(app,0));
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0)); // @TODO -- wait for OCPI fix for issue #3008
          // update tolerance from 1 MHz to a passable tolerance
          configs.setValue(295,{0,"channels",1,"tolerance_tuning_freq_mhz"});

          // follow through full lifecycle, assert proper state changes
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
        default:
          break;
      }
      break;
    case 1: // Application Tests
      switch (_testSelection.testSelect){
        case 0: // DBA AND DBB - TX0 TX1 RX0 RX1  - 96 MHZ
          configs.setProperty( // setting a CHANNEL configuration
              "rx false,"
              "rf_port_num 0," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 1} // config 0, channel 1
            );
          configs.setProperty(
              "rx true,"
              "rf_port_num 2," // DBA, RX channel --> xyz = 010
              "tuning_freq_mhz 380,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 0} // config 0, channel 0
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 6," // DBB, RX channel --> xyz = 110
              "tuning_freq_mhz 800.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 12.947,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 3} // config 0, channel 3
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 4," // DBB, TX channel --> xyz = 100
              "tuning_freq_mhz 5999.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 1.673,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 2} // config 0, channel 2
            );

          // Below are the additional coupled channels, ONLY GAIN VALUES CAN BE INDEPENDENT
            // @TODO: new tolerance values aren't checked against at all, only requested values with 1st tolerances
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 3," // DBA, RX channel --> xyz = 011
              "tuning_freq_mhz 380.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 28.7,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 4} // config 0, channel 4
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 1," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 14.13,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 5} // config 0, channel 5
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 7," // DBB, RX channel --> xyz = 111
              "tuning_freq_mhz 800.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 25.22,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 6} // config 0, channel 6 
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 5," // DBB, TX channel --> xyz = 101
              "tuning_freq_mhz 5999.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 7.386,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 7} // config 0, channel 7
            );

          // prepare
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          // start
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          std::cout << "\nApplication running and waiting for 5 seconds...\n\n";
          usleep(5 * 1e6);
          // stop
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 1: // DBA - TX0 RX1 - 96MHZ
          configs.setProperty( // setting a CHANNEL configuration
              "rx false,"
              "rf_port_num 0," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 1} // config 0, channel 1
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 3," // DBA, RX channel --> xyz = 011
              "tuning_freq_mhz 380.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 28.7,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 4} // config 0, channel 4
            );
 
          // prepare
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          // start
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          std::cout << "\nApplication running and waiting for 5 seconds...\n\n";
          usleep(5 * 1e6);
          // stop
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 2: // DBB - TX1 RX0 - 96MHZ
          configs.setProperty( // setting a CHANNEL configuration
              "description active,"
              "rx false," // TX
              "rf_port_num 5," // DBB, TX channel --> xyz = 101
              "tuning_freq_mhz 5999.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 1.673,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 2} // config 0, channel 2
            );
          configs.setProperty( // setting a CHANNEL configuration
              "description active,"
              "rx true," //RX
              "rf_port_num 6," // DBB, RX channel --> xyz = 110
              "tuning_freq_mhz 800.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 96,"
              "samples_are_complex true,"
              "gain_db 25.22,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 6} // config 0, channel 6
            );
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          // start
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          std::cout << "\nApplication running and waiting for 5 seconds...\n\n";
          usleep(5 * 1e6);
          // stop
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 3: // DBA TX0 TX1 RX0 RX1 - 90 MHZ 
          configs.setProperty( // setting a CHANNEL configuration
              "description active,"
              "rx false,"
              "rf_port_num 0," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1",
              { 0, "channels", 1} // config 0, channel 1
            );
          configs.setProperty(
              "rx true,"
              "rf_port_num 2," // DBA, RX channel --> xyz = 010
              "tuning_freq_mhz 380,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 0} // config 0, channel 0
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 3," // DBA, RX channel --> xyz = 011
              "tuning_freq_mhz 380.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 28.7,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 4} // config 0, channel 4
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 1," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 14.13,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 5} // config 0, channel 5
            );
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          // start
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          std::cout << "\nApplication running and waiting for 5 seconds...\n\n";
          usleep(5 * 1e6);
          // stop
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 4: // DBB TX0 TX1 RX0 RX1 - 90 MHz
          configs.setProperty( // setting a CHANNEL configuration
              "description active,"
              "rx false,"
              "rf_port_num 4," // DBB, TX channel --> xyz = 001
              "tuning_freq_mhz 200,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1",
              { 0, "channels", 1} // config 0, channel 1
            );
          configs.setProperty(
              "rx true,"
              "rf_port_num 6," // DBB, RX channel --> xyz = 010
              "tuning_freq_mhz 380,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 0} // config 0, channel 0
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 7," // DBB, RX channel --> xyz = 011
              "tuning_freq_mhz 380.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 28.7,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 4} // config 0, channel 4
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 5," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200.0,"
              "bandwidth_3db_mhz 70,"
              "sampling_rate_Msps 90,"
              "samples_are_complex true,"
              "gain_db 14.13,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 5} // config 0, channel 5
            );
            prepare.setValue(0);
            assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
            // start
            start.setValue(0);
            assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
            std::cout << "\nApplication running and waiting for 5 seconds...\n\n";
            usleep(5 * 1e6);
            // stop
            stop.setValue(0);
            assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
            release.setValue(0);
            assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
            break;
        case 5: // Toggle individual channels across both daughterboards
          {
            // re-evaluate this application
            // need to keep track of channel configurations differen
            // rf_port_num == physical mapping == one channel
            // default value for OFF channel = 0xFF
            // place a string
            struct drcConfigurationAppStruct {
              std::string channelTemplateConfiguration;
              std::uint16_t activePort;
              std::string portName;
            };
            // this data must encode the active port number with it as well, inactive might be unnecessary to be stored in the struct itself, though
            std::array<drcConfigurationAppStruct,8> static_channel_configs = {{
              { // DBA, TX channel --> xyz = 010
                "rx false,"
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 200,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                // "gain_db 25,"
                "gain_db 0,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                0,
                "DBA TX0",
              },
              { // DBA, TX channel --> xyz = 011
                "rx false," // TX
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 200.0,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                "gain_db 14.13,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                1,
                "DBA TX1",
              },
              { // DBA, RX channel --> xyz = 000
                "rx true,"
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 380,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                "gain_db 25,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                2,
                "DBA RX0",
              },
              { // DBA, RX channel --> xyz = 001
                "rx true,"
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 380.0,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                "gain_db 28.7,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                3,
                "DBA RX1",
              },
              { // DBB, TX channel --> xyz = 110
                "rx false," // TX
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 5999.0,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                "gain_db 1.673,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                4,
                "DBB TX0",
              },
              { // DBB, TX channel --> xyz = 111
                "rx false," // TX
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 5999.0,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                "gain_db 7.386,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                5,
                "DBB TX1"
              },
              { // DBB, RX channel --> xyz = 100
                "rx true,"
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 800.0,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                "gain_db 12.947,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                6,
                "DBB RX0",
              },
              { // DBB, RX channel --> xyz = 101
                "rx true,"
                "rf_port_num 255,"
                "rf_port_name NO_NAME,"
                "tuning_freq_mhz 800.0,"
                "bandwidth_3db_mhz 70,"
                "sampling_rate_Msps 96,"
                "samples_are_complex true,"
                "gain_db 25.22,"
                "tolerance_tuning_freq_mhz 0.01,"
                "tolerance_sampling_rate_msps 0.01,"
                "tolerance_gain_db 1,"
                "tolerance_bandwidth_3dB_MHz 10",
                7,
                "DBB RX1",
              },
            }};
            bool keepRunning = true;
            while(keepRunning){
              // select which channel to make active (which channels??)
              static std::uint16_t userActivateChannelChoice = 0;
              // make sure configuration in INACTIVE state --> stop AND release
              // std::uint16_t configState = app.getPropertyValue<std::uint32_t>("drc_n310","status",{0,"state"});
              std::uint16_t configState = 0;
              auto tmp = app.getPropertyValue<std::uint32_t>("drc_n310","status",{0,"state"});
              // OA::Property configs(app,"drc_n310","configurations");
              // OA::Property drcStatus(app,"drc_n310","status");
              // drcStatus.getUCharValue();
              std::string drcStatus_get;
              static std::array<const std::string,4> stateHelper = {{
                "inactive",
                "prepared",
                "operating",
                "error",
              }};
              do{ // make sure all configurations are released
                // @TODO: make looping check for all configs, not just 0
                app.getProperty("drc_n310.status",drcStatus_get,OA::AccessList({0,"state"}));
                std::cout << "configState = " << drcStatus_get << "\n";
                if(stateHelper.at(0) != drcStatus_get){ // until status is 'inactive'
                  // stop it
                  release.setValue(0);
                }
                app.getProperty("drc_n310.status",drcStatus_get,OA::AccessList({0,"state"}));
                
                // what if in error state?
              } while(stateHelper.at(0) != drcStatus_get);
              // debugConfig.setValue(0);
              // now configuration is inactive

              // loop through channel LUT structure
              std::array<std::uint32_t,8> channelPortOrder = {{
                5,6,3,1,7,2,0,4, // random assortment of ports vs configurationChannels 
              }};
              assert(8 == static_channel_configs.size()); // for now, DBA and DBB
              for(size_t _idx = 0; _idx < channelPortOrder.size(); _idx++){
                auto currentChannel = channelPortOrder.at(_idx);
                auto currentPortNum = _idx;
                auto &curChannelStruct = static_channel_configs.at(currentPortNum);
                configs.setProperty(curChannelStruct.channelTemplateConfiguration,{ 0, "channels", currentChannel});

                std::cout << "Activate port " << currentPortNum << "? == (" << curChannelStruct.portName << ")\n";
                std::cout << "\nyes = 0\nno = 1\n> ";
                std::cin >> userActivateChannelChoice;
                // based on this decision, replace <RFPORT> with actual value OR 0xFF
                // then setProperty using the access list
                if(userActivateChannelChoice == 0){ // activate the channel
                  configs.setValue<std::uint8_t>(curChannelStruct.activePort,{0,"channels",currentChannel,"rf_port_num"});
                  configs.setValue(curChannelStruct.portName,{0,"channels",currentChannel,"rf_port_name"});
                } else { // deactivate the channel
                  // inactive patterns
                  configs.setValue<std::uint8_t>(0xff,{0,"channels",currentChannel,"rf_port_num"});
                  configs.setValue("NO_NAME",{0,"channels",currentChannel,"rf_port_name"});
                }
              }
              // start
              start.setValue(0);
              std::cout << "\nApplication running and waiting for 5 seconds...\n\n";
              usleep(5 * 1e6);
              // stop
              // continue running?
              std::cout << "Configuration choice active\n";
              std::cout << "Try again? \nyes = 0\nno = 1\n> ";
              std::cin >> userActivateChannelChoice;
              keepRunning = (userActivateChannelChoice == 0)? true: false;
            } // while
            // stop
            release.setValue(0);
          } // case 5
          break;
        case 6: // Library Verification
          {
            const std::vector<std::string> PORT_NAMES = {{
              "DBATX0",
              "DBATX1",
              "DBARX0",
              "DBARX1",
              "DBBTX0",
              "DBBTX1",
              "DBBRX0",
              "DBBRX1",
            }};
            const double __TOLERANCE_TUNING_FREQ_MHZ = 0.01, __TOLERANCE_SAMPLING_RATE_MSPS = 0.01;
            // bandwidth won't be accurately reported/requested right now, check cycle bandwidth for that test
            const double __TOLERANCE_GAIN_DB = 1, __TOLERANCE_BANDWIDTH_3DB_MHZ = 50.0;
            // setting both default initialized channels in configuration to OFF (rf_port_num)
            configs.setValue<std::uint8_t>(OFF, {0, "channels", 1, "rf_port_num"});

            // going to only be using channel 0 to make configurations, but still start in OFF port number
            configs.setValue<std::uint8_t>(OFF, {0, "channels", 0, "rf_port_num"});
            // default tolerances
            configs.setValue(__TOLERANCE_TUNING_FREQ_MHZ, {0, "channels", 0, "tolerance_tuning_freq_mhz"});
            configs.setValue(__TOLERANCE_SAMPLING_RATE_MSPS, {0, "channels", 0, "tolerance_sampling_rate_msps"});
            configs.setValue(__TOLERANCE_GAIN_DB, {0, "channels", 0, "tolerance_gain_db"});
            configs.setValue(__TOLERANCE_BANDWIDTH_3DB_MHZ, {0, "channels", 0, "tolerance_bandwidth_3dB_MHz"});
            configs.setValue(50.0, {0, "channels", 0, "bandwidth_3dB_MHz"});
            // get the status, VERIFICATION TEST
            struct tuningFreqTuple {
              bool rx;
              double tuningFreq_MHz;
              tuningFreqTuple(bool _rx, double _tuningFreq_MHz) : rx(_rx), tuningFreq_MHz(_tuningFreq_MHz) { }
            };
            std::vector<struct tuningFreqTuple> _requestedTuningTuple = {
              {true,200.0},    // Lowband
              {true,380.0},    // band0
              {true,500.0},    // band1
              {true,800.0},    // band2
              {true,1550.0},   // band3
              {true,1800.0},   // band4
              {true,2400.0},   // band5
              {true,2900.0},   // band6
              {true,-5500.0},   // Wideband
              {true,-220.0},   // wideband, superheterodyne
              {false,200.0},    // Lowband
              {false,500.0},    // 440
              {false,800.0},    // 750
              {false,1830.0},   // 1500
              {false,4000.0},   // 3000
              {false,-5500.0},    // Wideband
              {false,-134.0}    // wideband, superheterodyne
            };
            std::vector<double> _requestedDevClk = {
              90,96,122.88,125,153.6 // removed 75 temporarily, too consistently popping up RX framer errors, and not able to conditionally assert on that yet
            };
            // maybe requested gain doesn't need to be stored?
            std::vector<double> _requestedGains = {0,1.5,22,17,30.3};
            std::uint32_t counter = 0; // skip arbitrarily if need to
            std::uint16_t testSize = _requestedTuningTuple.size();
            testSize = (testSize < _requestedDevClk.size()) ? _requestedDevClk.size() : testSize;
            // testSize = (testSize < _requestedDevClk.size()) ? _requestedDevClk.size() : testSize;
            for(std::uint16_t x = 0; x < testSize; x++){
              // initialized static variable to increment for 'current relative port' -- relative to TX/RX direction bit being controlled
              static std::uint16_t curRelPort = 0;
              // grab loop relative values
              auto _tuningFreq_MHz = _requestedTuningTuple.at(x % _requestedTuningTuple.size()).tuningFreq_MHz; // double
              auto __rxBool = _requestedTuningTuple.at(x % _requestedTuningTuple.size()).rx; // bool
              std::uint8_t _portNum = (__rxBool << 1) + ((curRelPort & 0x2) << 1) + (curRelPort & 0x1); // this will cycle through all 8 channels, maybe..
              auto _samplingRate_Msps = _requestedDevClk.at(x % _requestedDevClk.size()); // double

              configs.setValue(_tuningFreq_MHz, {0, "channels", 0, "tuning_freq_mhz"});
              configs.setValue(_samplingRate_Msps, {0, "channels", 0, "sampling_rate_Msps"});
              configs.setValue(__rxBool, {0, "channels", 0, "rx"});
              configs.setValue<std::uint8_t>(_portNum, {0, "channels", 0, "rf_port_num"});
              
              for(auto &_gain : _requestedGains){
                counter++;
                configs.setValue(_gain, {0, "channels", 0, "gain_db"});
                // now run the configuration, and verify it
                std::cout << "Deploying DRC configuration index = " << counter << "\n"; // counter is
                std::int16_t attempt_counter = 0;
                while(attempt_counter++ < 2){
                  std::cout << "Running Configuration attempt #" << attempt_counter << "\n";
                  start.setValue(0);
                  // output the configuration
                  if(stateHelper.at(_stateHelper::operating)==getConfigState(app,0)){
                    std::cout << "Failed configuration attempt #" << attempt_counter << "\n";
                    break;
                  }
                }
                assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
                std::string _value_getter_str;
                app.getProperty("drc_n310.status", _value_getter_str, OA::AccessList({0, "channels", 0}), {OA::UNREADABLE_OK});
                std::cout << "Config 0, channel 0, port " << _portNum << ", " << PORT_NAMES.at(_portNum) << ":\n\t";
                std::cout << _value_getter_str << "\n";
                // print the status
                release.setValue(0);
                assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
              } // end of gain variation
              // manage static loop variable
              curRelPort += 1;
            }
            break;
          }
        case 7: // DBA AND DBB - TX0 TX1 RX0 RX1  - 75 MHZ
          configs.setProperty( // setting a CHANNEL configuration
              "rx false,"
              "rf_port_num 0," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 1} // config 0, channel 1
            );
          configs.setProperty(
              "rx true,"
              "rf_port_num 2," // DBA, RX channel --> xyz = 010
              "tuning_freq_mhz 380,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 0} // config 0, channel 0
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 6," // DBB, RX channel --> xyz = 110
              "tuning_freq_mhz 800.0,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 12.947,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 3} // config 0, channel 3
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 4," // DBB, TX channel --> xyz = 100
              "tuning_freq_mhz 5999.0,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 1.673,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 2} // config 0, channel 2
            );

          // Below are the additional coupled channels, ONLY GAIN VALUES CAN BE INDEPENDENT
            // @TODO: new tolerance values aren't checked against at all, only requested values with 1st tolerances
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 3," // DBA, RX channel --> xyz = 011
              "tuning_freq_mhz 380.0,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 28.7,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 4} // config 0, channel 4
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 1," // DBA, TX channel --> xyz = 001
              "tuning_freq_mhz 200.0,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 14.13,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 5} // config 0, channel 5
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx true,"
              "rf_port_num 7," // DBB, RX channel --> xyz = 111
              "tuning_freq_mhz 800.0,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 25.22,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 6} // config 0, channel 6 
            );
          configs.setProperty( // setting a CHANNEL configuration
              "rx false," // TX
              "rf_port_num 5," // DBB, TX channel --> xyz = 101
              "tuning_freq_mhz 5999.0,"
              "bandwidth_3db_mhz 60,"
              "sampling_rate_Msps 75,"
              "samples_are_complex true,"
              "gain_db 7.386,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3dB_MHz 10",
              { 0, "channels", 7} // config 0, channel 7
            );

          // prepare
          prepare.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          // start
          start.setValue(0);
          assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
          std::cout << "\nApplication running and waiting for 5 seconds...\n\n";
          usleep(5 * 1e6);
          // stop
          stop.setValue(0);
          assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
          release.setValue(0);
          assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
          break;
        case 8: // Cycle Bandwidth
          {
            // configuration 0: RX0 DBA
            configs.setProperty( // setting a CHANNEL configuration
              "description active,"
              "rx true,"
              "rf_port_num 2," // DBA, TX channel --> xyz = 010
              "tuning_freq_mhz 600,"
              "bandwidth_3db_mhz 100.0,"
              "sampling_rate_Msps 125,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3db_mhz 10.0",
              { 0, "channels", 0} 
              // DRC Config 0, Channels, Channel Configuration #1
            );
            // configuration 1: TX0 DBA
            configs.setProperty( // setting a CHANNEL configuration
              "description active,"
              "rx false,"
              "rf_port_num 0," // DBA, TX channel --> xyz = 010
              "tuning_freq_mhz 600,"
              "bandwidth_3db_mhz 100.0,"
              "sampling_rate_Msps 125,"
              "samples_are_complex true,"
              "gain_db 25,"
              "tolerance_tuning_freq_mhz 0.01,"
              "tolerance_sampling_rate_msps 0.01,"
              "tolerance_gain_db 1,"
              "tolerance_bandwidth_3db_mhz 10.0",
              { 0, "channels", 1} 
              // DRC Config 0, Channels, Channel Configuration #1
            );
            const std::map<double,std::set<std::pair<double,double>>> loop_struct = {{
              {75.0,{{30.0, 20.0}, {40.0, 20.0}, {50.0, 20.0}, {60.0, 20.0}, {30.0, 30.0}, {40.0, 30.0}, {50.0, 30.0}, {60.0, 30.0}, {30.0, 40.0}, {40.0, 40.0}, {50.0, 40.0}, {60.0, 40.0}, {30.0, 50.0}, {40.0, 50.0}, {50.0, 50.0}, {60.0, 50.0}, {30.0, 60.0}, {40.0, 60.0}, {50.0, 60.0}, {60.0, 60.0}}},
              {90.0,{{40.0, 20.0}, {50.0, 20.0}, {60.0, 20.0}, {70.0, 20.0}, {40.0, 30.0}, {50.0, 30.0}, {60.0, 30.0}, {70.0, 30.0}, {40.0, 40.0}, {50.0, 40.0}, {60.0, 40.0}, {70.0, 40.0}, {40.0, 50.0}, {50.0, 50.0}, {60.0, 50.0}, {70.0, 50.0}, {40.0, 60.0}, {50.0, 60.0}, {60.0, 60.0}, {70.0, 60.0}, {40.0, 70.0}, {50.0, 70.0}, {60.0, 70.0}, {70.0, 70.0}}},
              {96.0,{{40.0, 20.0}, {50.0, 20.0}, {60.0, 20.0}, {70.0, 20.0}, {40.0, 30.0}, {50.0, 30.0}, {60.0, 30.0}, {70.0, 30.0}, {40.0, 40.0}, {50.0, 40.0}, {60.0, 40.0}, {70.0, 40.0}, {40.0, 50.0}, {50.0, 50.0}, {60.0, 50.0}, {70.0, 50.0}, {40.0, 60.0}, {50.0, 60.0}, {60.0, 60.0}, {70.0, 60.0}, {40.0, 70.0}, {50.0, 70.0}, {60.0, 70.0}, {70.0, 70.0}}},
              {122.88,{{100.0, 100.0}, {50.0, 100.0}, {60.0, 100.0}, {70.0, 100.0}, {80.0, 100.0}, {90.0, 100.0}, {100.0, 20.0}, {50.0, 20.0}, {60.0, 20.0}, {70.0, 20.0}, {80.0, 20.0}, {90.0, 20.0}, {100.0, 30.0}, {50.0, 30.0}, {60.0, 30.0}, {70.0, 30.0}, {80.0, 30.0}, {90.0, 30.0}, {100.0, 40.0}, {50.0, 40.0}, {60.0, 40.0}, {70.0, 40.0}, {80.0, 40.0}, {90.0, 40.0}, {100.0, 50.0}, {50.0, 50.0}, {60.0, 50.0}, {70.0, 50.0}, {80.0, 50.0}, {90.0, 50.0}, {100.0, 60.0}, {50.0, 60.0}, {60.0, 60.0}, {70.0, 60.0}, {80.0, 60.0}, {90.0, 60.0}, {100.0, 70.0}, {50.0, 70.0}, {60.0, 70.0}, {70.0, 70.0}, {80.0, 70.0}, {90.0, 70.0}, {100.0, 80.0}, {50.0, 80.0}, {60.0, 80.0}, {70.0, 80.0}, {80.0, 80.0}, {90.0, 80.0}, {100.0, 90.0}, {50.0, 90.0}, {60.0, 90.0}, {70.0, 90.0}, {80.0, 90.0}, {90.0, 90.0}}},
              {125,{{100.0, 100.0}, {50.0, 100.0}, {60.0, 100.0}, {70.0, 100.0}, {80.0, 100.0}, {90.0, 100.0}, {100.0, 20.0}, {50.0, 20.0}, {60.0, 20.0}, {70.0, 20.0}, {80.0, 20.0}, {90.0, 20.0}, {100.0, 30.0}, {50.0, 30.0}, {60.0, 30.0}, {70.0, 30.0}, {80.0, 30.0}, {90.0, 30.0}, {100.0, 40.0}, {50.0, 40.0}, {60.0, 40.0}, {70.0, 40.0}, {80.0, 40.0}, {90.0, 40.0}, {100.0, 50.0}, {50.0, 50.0}, {60.0, 50.0}, {70.0, 50.0}, {80.0, 50.0}, {90.0, 50.0}, {100.0, 60.0}, {50.0, 60.0}, {60.0, 60.0}, {70.0, 60.0}, {80.0, 60.0}, {90.0, 60.0}, {100.0, 70.0}, {50.0, 70.0}, {60.0, 70.0}, {70.0, 70.0}, {80.0, 70.0}, {90.0, 70.0}, {100.0, 80.0}, {50.0, 80.0}, {60.0, 80.0}, {70.0, 80.0}, {80.0, 80.0}, {90.0, 80.0}, {100.0, 90.0}, {50.0, 90.0}, {60.0, 90.0}, {70.0, 90.0}, {80.0, 90.0}, {90.0, 90.0}}},
              {153.6, {{100.0, 100.0}, {70.0, 100.0}, {80.0, 100.0}, {90.0, 100.0}, {100.0, 20.0}, {70.0, 20.0}, {80.0, 20.0}, {90.0, 20.0}, {100.0, 30.0}, {70.0, 30.0}, {80.0, 30.0}, {90.0, 30.0}, {100.0, 40.0}, {70.0, 40.0}, {80.0, 40.0}, {90.0, 40.0}, {100.0, 50.0}, {70.0, 50.0}, {80.0, 50.0}, {90.0, 50.0}, {100.0, 60.0}, {70.0, 60.0}, {80.0, 60.0}, {90.0, 60.0}, {100.0, 70.0}, {70.0, 70.0}, {80.0, 70.0}, {90.0, 70.0}, {100.0, 80.0}, {70.0, 80.0}, {80.0, 80.0}, {90.0, 80.0}, {100.0, 90.0}, {70.0, 90.0}, {80.0, 90.0}, {90.0, 90.0}} },
            }};
            std::uint32_t counter = 0; // skip arbitrarily if need to
            for(auto &it : loop_struct){
              auto dev_clk = it.first; // sampling rate
              if(dev_clk < 122 or dev_clk > 123){
                // continue;
                ;
              }
              configs.setValue(dev_clk, {0, "channels", 0, "sampling_rate_Msps"});
              configs.setValue(dev_clk, {0, "channels", 1, "sampling_rate_Msps"});
              for(auto &band_pair : loop_struct.at(dev_clk)){
                auto rx_bandwidth = band_pair.first;
                auto tx_bandwidth = band_pair.second;
                // loop for enabling specific channels ? Right now selecting both manually
                configs.setValue(rx_bandwidth+1.0, {0, "channels", 0, "bandwidth_3db_mhz"}); // added +1 to see if saturate upwards
                configs.setValue(tx_bandwidth+1.0, {0, "channels", 1, "bandwidth_3db_mhz"}); // added +1 to see if saturate upwards
                typedef struct {
                  bool rx_enabled, tx_enabled;
                } port_values; // "0xFF","0","2" --> OFF (default value), TX, RX -- anything outside of range is INACTIVE

                // port loop pattern
                const std::vector<port_values> port_table = {{false,true},{true,false},{true,true}};
                for(auto &port_nums : port_table){
                  counter++;
                  configs.setValue((uint32_t)((port_nums.rx_enabled)? DBARX0 : OFF), {0, "channels", 0, "rf_port_num"});
                  configs.setValue((uint32_t)((port_nums.tx_enabled)? DBATX0 : OFF), {0, "channels", 1, "rf_port_num"});
                  // print out request
                  if(stateHelper.at(_stateHelper::operating)==getConfigState(app,0)){
                    std::cout << "Releasing previous DRC configuration\n";
                    // only release (stop -> release) when we are operating
                    release.setValue(0);
                    assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
                  }
                  std::cout << "Deploying DRC configuration index = " << counter << "\n"; // counter is
                  std::int16_t attempt_counter = 0;
                  while(attempt_counter++ < 2){
                    std::cout << "Running Configuration attempt #" << attempt_counter << "\n";
                    start.setValue(0);
                    // output the configuration

                    if(stateHelper.at(_stateHelper::operating)==getConfigState(app,0)){
                      std::cout << "Failed configuration attempt #" << attempt_counter << "\n";
                      break;
                    }
                  }
                  // assume passes by here, framer issue?
                  assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
                  // request a log
                  // Temporarily unavailable
                  // app.setProperty("drc_n310.enhancedSignaler", "log_bandwidth_application");
                  std::string drcStatus_get_channel_zero;
                  std::string drcStatus_get_channel_one;

                  std::cout << "\n\n";
                  if(port_nums.rx_enabled){
                    app.getProperty("drc_n310.status", drcStatus_get_channel_zero, OA::AccessList({0, "channels", 0}), {OA::UNREADABLE_OK});
                    std::cout << "Config 0, channel 0, port 2, rx:\n\t";
                    std::cout << drcStatus_get_channel_zero << "\n";
                  }
                  if(port_nums.tx_enabled){
                    app.getProperty("drc_n310.status", drcStatus_get_channel_one, OA::AccessList({0, "channels", 1}), {OA::UNREADABLE_OK});
                    std::cout << "Config 0, channel 1, port 0, tx:\n\t";
                    std::cout << drcStatus_get_channel_one << "\n";
                  }
                  std::cout << "\n\n";
                  // must be operating at this point
                }
              }
            }
          }
        default:
          break;
      }
      break;
    case 2: // LMK Test(s)
      switch (_testSelection.testSelect){
        // default channel configuration
        configs.setProperty(
          "description active,"
          "rx true,"
          "rf_port_num 2," // DBA, RX channel --> xyz = 000
          "tuning_freq_mhz 380,"
          "bandwidth_3db_mhz 100.0,"
          "sampling_rate_Msps 125,"
          "samples_are_complex true,"
          "gain_db 25,"
          "tolerance_tuning_freq_mhz 0.01,"
          "tolerance_sampling_rate_msps 0.01,"
          "tolerance_gain_db 1",
          { 0, "channels", 0} 
          // DRC Config 0 , Channels, Channel Configuration #0
        );
        configs.setProperty( // setting a CHANNEL configuration
          "description active,"
          "rx false,"
          "rf_port_num 0," // DBA, TX channel --> xyz = 010
          "tuning_freq_mhz 200,"
          "bandwidth_3db_mhz 100.0,"
          "sampling_rate_Msps 125,"
          "samples_are_complex true,"
          "gain_db 25,"
          "tolerance_tuning_freq_mhz 0.01,"
          "tolerance_sampling_rate_msps 0.01,"
          "tolerance_gain_db 1",
          { 0, "channels", 1} 
          // DRC Config 0, Channels, Channel Configuration #1
        );
        case 0:
          app.setPropertyValue<std::uint16_t>("drc_n310","prepare",0);
          app.setPropertyValue<std::uint16_t>("drc_n310","start",0);
          app.setPropertyValue<std::uint16_t>("drc_n310","stop",0);
          // scope defined because nature of dbselect still undetermined, right now hardcoding a constant essentially
          {
            bool dbSelect = true; // @TODO: For now, just hardcoding it to DBB
            app.setPropertyValue<std::uint64_t>("drc_n310","spiControlStruct",dbSelect,{"address"});
          }
          drcLog = logLMK(app);
          OCPI::OS::Log::print(3, "%s", drcLog.c_str()); // HITL output for LMK testing at MCR = 125 MHz (using LUT)
          app.setPropertyValue<std::uint16_t>("drc_n310","release",0);
          break;
        case 1:
          configs.setValue(122.88,{0, "channels", 0, "sampling_rate_Msps"});
          configs.setValue(122.88,{0, "channels", 1, "sampling_rate_Msps"});
          app.setPropertyValue<std::uint16_t>("drc_n310","prepare",0);
          app.setPropertyValue<std::uint16_t>("drc_n310","start",0);
          app.setPropertyValue<std::uint16_t>("drc_n310","stop",0);
          // scope defined because nature of dbselect still undetermined, right now hardcoding a constant essentially
          {
            bool dbSelect = true; // @TODO: For now, just hardcoding it to DBB
            app.setPropertyValue<std::uint64_t>("drc_n310","spiControlStruct",dbSelect,{"address"});
          }
          drcLog = logLMK(app);
          OCPI::OS::Log::print(3, "%s", drcLog.c_str()); // HITL output for LMK testing at MCR = 125 MHz (using LUT)
          app.setPropertyValue<std::uint16_t>("drc_n310","release",0);
          break;
        case 2:
          configs.setValue(153.6,{0, "channels", 0, "sampling_rate_Msps"});
          configs.setValue(153.6,{0, "channels", 1, "sampling_rate_Msps"});
          app.setPropertyValue<std::uint16_t>("drc_n310","prepare",0);
          app.setPropertyValue<std::uint16_t>("drc_n310","start",0);
          app.setPropertyValue<std::uint16_t>("drc_n310","stop",0);
          // scope defined because nature of dbselect still undetermined, right now hardcoding a constant essentially
          {
            bool dbSelect = true; // @TODO: For now, just hardcoding it to DBB
            app.setPropertyValue<std::uint64_t>("drc_n310","spiControlStruct",dbSelect,{"address"});
          }
          drcLog = logLMK(app);
          OCPI::OS::Log::print(3, "%s", drcLog.c_str()); // HITL output for LMK testing at MCR = 125 MHz (using LUT)
          app.setPropertyValue<std::uint16_t>("drc_n310","release",0);
          break;
        default:
          break;
      }
      break;
    case 3: // Frontend tests
      configs.setProperty( // setting a CHANNEL configuration
          "description active,"
          "rx false,"
          "rf_port_num 0," // DBA, TX channel --> xyz = 010
          "tuning_freq_mhz 200,"
          "bandwidth_3db_mhz 100.0,"
          "sampling_rate_Msps 125,"
          "samples_are_complex true,"
          "gain_db 25,"
          "tolerance_tuning_freq_mhz 0.01,"
          "tolerance_sampling_rate_msps 0.01,"
          "tolerance_gain_db 1",
          { 0, "channels", 1} 
          // DRC Config 0, Channels, Channel Configuration #1
        );

      configs.setProperty(
          "description active,"
          "rx true,"
          "rf_port_num 2," // DBA, RX channel --> xyz = 000
          "tuning_freq_mhz 380,"
          "bandwidth_3db_mhz 100.0,"
          "sampling_rate_Msps 125,"
          "samples_are_complex true,"
          "gain_db 25,"
          "tolerance_tuning_freq_mhz 0.01,"
          "tolerance_sampling_rate_msps 0.01,"
          "tolerance_gain_db 1",
          { 0, "channels", 0} 
          // DRC Config 0 , Channels, Channel Configuration #0
        );

      { // scope for the LUT generated
        enum TRX : std::int16_t {RX, TX};
        const static std::array<std::pair<std::int16_t,double>,16> LO_LUT_MHz = {{
          {TRX::RX,300.0},    // Lowband
          {TRX::RX,500.0},    // 440
          {TRX::RX,800.0},    // 650
          {TRX::RX,1550.0},   // 1100
          {TRX::RX,1800.0},   // 1600
          {TRX::RX,2400.0},   // 2100
          {TRX::RX,2900.0},   // 2750
          {TRX::RX,-5500.0},   // Wideband
          {TRX::RX,-220.0},   // wideband, superheterodyne
          {TRX::TX,300.0},    // Lowband
          {TRX::TX,500.0},    // 440
          {TRX::TX,800.0},    // 750
          {TRX::TX,1550.0},   // 1500
          {TRX::TX,4000.0},   // 3000
          {TRX::TX,-5500.0},    // Wideband
          {TRX::TX,-134.0}    // wideband, superheterodyne
        }};
        // test select --> Programs the corresponding Sampling Rate
        // TX/RX --> Selects which channel to program (rf_port_num)
        bool dbSelect = true; // for now, hardcoding DBB
        std::int16_t configurationOrdinal = 0; // also hard coded
        // right now, channels 1 and 3 are arbitrarily programmed in for the DRC config test
        const static std::array<std::int16_t,2> channel_LUT = {{ // TRX is indexor
          0,
          1
        }};
        // update the TX/RX tuning frequencies
        configs.setValue(
            LO_LUT_MHz.at(_testSelection.testSelect).second,
            {
              configurationOrdinal,
              "channels", // indexing based on the channels data array of channel config structures
              channel_LUT.at(LO_LUT_MHz.at(_testSelection.testSelect).first), // Select the correct channel index
              "tuning_freq_mhz" // update the tuning frequency in MHz
            }
          );
        // traverse the FSM
        app.setPropertyValue<std::uint16_t>("drc_n310","prepare",0);
        assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
        std::string drcLog = logCPLD(app,_testSelection.testSelect);
        OCPI::OS::Log::print(3, "%s", drcLog.c_str());
        app.setPropertyValue<std::uint16_t>("drc_n310","start",0);
        assert(stateHelper.at(_stateHelper::operating)==getConfigState(app,0));
        // Think this is where tests need to be done, using all of the LUT's generated
        app.setPropertyValue<std::uint16_t>("drc_n310","stop",0);
        assert(stateHelper.at(_stateHelper::prepared)==getConfigState(app,0));
        app.setPropertyValue<std::uint16_t>("drc_n310","release",0);
        assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));
      }
      break;
    default: // unspecified tests
      break;
    }
  }

int main(/*int argc, char **argv*/) {
  // For an explanation of the ACI, see:
  // https://opencpi.gitlab.io/releases/develop/docs/OpenCPI_Application_Development_Guide.pdf

  try {
    OA::Application app("fsmTesting.xml");
    app.initialize(); // all resources have been allocated
    app.start();      // execution is started

    OA::Property configs(app,"drc_n310","configurations"); // grab ENTIRE configurations data
    // configs.setValue(-23, {2, "channels", 1, "gain_db"}); // configuration 2, channel 1

    std::string drcLog;

    // stream to be passed to stdout for user IO @TODO: safeguard user IO
    std::stringstream appStream;
    appStream << "\n\nSelect test scenario to run for the DRC FSM Application\n";
    std::int16_t categorySelect,testSelect;


    auto testCasesIndex = (releaseBuild == 1) ? 2 : 0;

    for(std::uint16_t index = 0; index < testCases.size() - testCasesIndex; index++){
      appStream << "\t" << index << "\t" << testCases.at(index).first << '\n';
    }
    std::cout << appStream.str();
    appStream.str("> ");
    std::cout << appStream.str();
    std::cin >> categorySelect;

    if ( categorySelect > testCases.size() - testCasesIndex - 1 )
    {
      std::cout << "\nPlease enter a valid selection\n\n";
      app.finish(); 
      exit(-1);
    }

    appStream.str("");


    auto testSelectIndex = (releaseBuild == 1 && categorySelect == 1) ? 3 : 0;

    appStream << "\nSelect specific test to run for requested category (" << testCases.at(categorySelect).first << ")\n";
    for(std::uint16_t index = 0; index < testCases.at(categorySelect).second.size() - testSelectIndex; index++){
      appStream << "\t" << index << "\t" << testCases.at(categorySelect).second.at(index) << '\n';
    }
    std::cout << appStream.str();
    appStream.str("> ");
    std::cout << appStream.str();
    std::cin >> testSelect;

    if ( testSelect > testCases.at(categorySelect).second.size() - testSelectIndex - 1 )
    {
      std::cout << "\nPlease enter a valid selection\n\n";
      app.finish(); 
      exit(-1);
    }

    appStream.str("");

    // appStream(""); // empty the stringstream

    assert(stateHelper.at(_stateHelper::inactive)==getConfigState(app,0));

    auto _testSelection = testSelection(categorySelect,testSelect);
    drc_n310_testing_suite(configs, app,_testSelection);

    // Must use either wait()/finish() or stop(). The finish() method must
    // always be called after wait(). The start() method can be called
    // again after stop().
    // app.wait();       // wait until app is "done"
    app.finish();     // do end-of-run processing like dump properties
    // app.stop();

  } catch (std::string &e) {
    std::cerr << "app failed: " << e << std::endl;
    return 1;
  }
  return 0;
}
