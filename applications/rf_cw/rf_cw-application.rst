.. rf_cw documentation


.. _rf_cw-application:


RF CW Generator (``rf_cw``)
===========================

Description
-----------
Application that generates a RF CW for testing of the RF sub-system using the following OpenCPI Application Specifications (OAS):

* rf_cw_tx.xml - Transmitter that generates a CW at tuning_freq_MHz 
* rf_cw_txrx.xml - Transceiver that generates a CW offset from tuning_freq_MHz
* rf_cw_loopback - Transceiver that generates a CW offset from tuning_freq_MHz (JESD Baseband Mode)


Build
-----
1. Ensure the following ``ocpi.assets`` components are build for the ``n310`` platform:

	- ocpi.assets.misc_comps.csts_to_iqstream.hdl


2. Build the following components using the ``ocpidev build --hdl-plaform n310`` within each component directory (``./components``).

	- ocpi.osp.n3xx.constant_generator.hdl
	- ocpi.osp.n3xx.carrier_generator
	- ocpi.osp.n3xx.data_sink.hdl


3. Build the following assemblies using ``ocpidev build --hdl-plaform n310`` within each assemblies directory (``./hdl/assemblies``):

	- rf_cw_tx
	- rf_cw_txrx
	- rf_cw_loopback


4. Build the application using ``ocpidev build --rcc-platform ettus_n310_v4``  within the application directory.  The application artifact is ``./target-ettus_n310_v4/rf_cw``.

Prerequisites
-------------
1. Copy DRC artifacts into the OpenCPI artifacts directory on the N310 platform:

	- ocpi.osp.n3xx.devices.drc_n310.rcc.10.ettus_n310_v4.so (rf_cw_loopback.xml and rf_cw_txrx)
	- ocpi.osp.n3xx.devices.drc_n310.rcc.11.ettus_n310_v4.so (rf_cw_tx.xml)


2. Make ``bitz`` directory within the application working directory on the ``n310`` platform:

.. code-block:: bash

	% mkdir ./bitz


3. Copy the following the bitz files in the ``./artifacts`` directory into the ``bitz`` directory in the application working directory:

	- ocpi.osp.n3xx.rf_cw_loopback_n310_cfg_loopback_dba_cnt_loopback_nocic_dba.hdl.0.n310.bitz
	- ocpi.osp.n3xx.rf_cw_tx_n310_cfg_1tx_0rx_dba_cnt_1tx_0rx_nocic_dba.hdl.0.n310.bitz
	- ocpi.osp.n3xx.rf_cw_txrx_n310_cfg_1tx_1rx_dba_cnt_1tx_1rx_nocic_dba.hdl.0.n310.bitz


4. Copy the OAS files and ACI program into the application working directory:

	- rf_cw_loopback.xml
	- rf_cw_tx.xml
	- rf_cw_txrx.xml
	- target-ettus_n310_v4/rf_cw


Execution
---------
Copy and run the rf_cw application script in the application working directory on the ``n310`` platform:

.. code-block:: bash 

	% ./n310_rf_cw_app


.. code-block:: bash

	N310 OSP RF Applications
	--------------------------
	[1] RF CW TX
	[2] RF CW TXRX
	[3] RF CW LOOPBACK
	[4] RF CW TX TUNER
	--------------------------
	Enter App Number: 
	2


Output
------

.. code-block:: bash

	OCPI( 3:740.0019): drc_n310: 
	Daughterboard--DBA:
	Power Off Reset Control
	-----------------------
	CHIP            | POR
	-----------------------
	CPLD Pathing    | x
	LMK04828        | x
	ADF4351         | x
	DSA             | x
	CPLD GPIO POR   | x
	Transceiver Obj | x
	-----------------------
	OCPI( 3:743.0200): drc_n310: DBA lmkController: PLLs locked
	OCPI( 3:743.0201): drc_n310: DBA lmkController: SYNC Event
	OCPI( 3:743.0230): drc_n310: DBA Transceiver: rx_clkgen: MMCM-PLL locked
	OCPI( 3:743.0250): drc_n310: DBA Transceiver: tx_clkgen: MMCM-PLL locked
	OCPI( 3:744.0020): drc_n310: DBA Transceiver: MCS successful
	OCPI( 3:744.0021): drc_n310: DBA Transceiver: CLKPLL locked
	OCPI( 3:748.0454): drc_n310: DBA Transceiver: AD9371 ARM version 5.2.2
	OCPI( 3:748.0780): drc_n310: DBA Transceiver: PLLs locked
	OCPI( 3:748.0800): drc_n310: DBA Transceiver: waiting for initCals (or 60 second timer)
	OCPI( 3:754.0204): drc_n310: DBA Transceiver: Calibrations completed successfully
	OCPI( 3:754.0320): drc_n310: DBA Transceiver: tx_adxcvr: OK
	OCPI( 3:754.0440): drc_n310: DBA Transceiver: rx_adxcvr: OK
	OCPI( 3:754.0451): drc_n310: DBA Transceiver: rx_jesd status:
		Link is enabled
		Measured Link Clock: 45.000 MHz
		Reported Link Clock: 45.000 MHz
		Lane rate: 1800.000 MHz
		Lane rate / 40: 45.000 MHz
		Link status: DATA
		SYSREF captured: Yes
		SYSREF alignment error: No
	OCPI( 3:754.0457): drc_n310: DBA Transceiver: rx_jesd lane 0 status:
	Errors: 1
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 1 Multi-frames and 60 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 0, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x47, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1800000 kHz
	OCPI( 3:754.0463): drc_n310: DBA Transceiver: rx_jesd lane 1 status:
	Errors: 1
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 2 Multi-frames and 36 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 1, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x48, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1800000 kHz
	OCPI( 3:754.0465): drc_n310: DBA Transceiver: rx_jesd lane 2 status:
	Errors: 0
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 2 Multi-frames and 52 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 2, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x49, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1800000 kHz
	OCPI( 3:754.0468): drc_n310: DBA Transceiver: rx_jesd lane 3 status:
	Errors: 0
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 2 Multi-frames and 2 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 3, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x4A, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1800000 kHz
	OCPI( 3:754.0580): drc_n310: DBA Transceiver: rx_adc: Successfully initialized
	OCPI( 3:754.0581): drc_n310: DBA Transceiver: tx_jesd status:
		Link is enabled
		Measured Link Clock: 45.000 MHz
		Reported Link Clock: 45.000 MHz
		Lane rate: 1800.000 MHz
		Lane rate / 40: 45.000 MHz
		SYNC~: deasserted
		Link status: DATA
		SYSREF captured: Yes
		SYSREF alignment error: No
	OCPI( 3:754.0690): drc_n310: DBA Transceiver: tx_dac: Successfully initialized
	OCPI( 3:754.0691): drc_n310: DBA Transceiver: Successful configuration of ad9371 chip
	OCPI( 3:754.0842): drc_n310: prepare_config():
	Upgraded config 0 from inactive state to prepared
	OCPI( 3:754.0843): drc_n310: update_tuning_frequency bypassed due to parameter remaining unchanged
	OCPI( 3:754.0844): drc_n310: update_tuning_frequency bypassed due to parameter remaining unchanged
	OCPI( 3:754.0960): drc_n310: start_config():
	Upgraded config 0 from prepared state to OPERATING state
	
	App runs for 10 seconds...
	
	
	OCPI( 3:765.0060): drc_n310: stop_config():
	Downgraded from operating state to PREPARED state
	OCPI( 3:765.0069): drc_n310: DBA Transceiver: rx_jesd status:
		Link is disabled
		Measured Link Clock: 45.000 MHz
		Reported Link Clock: 45.000 MHz
		External reset is deasserted
	OCPI( 3:765.0069): drc_n310: DBA Transceiver: tx_jesd status:
		Link is disabled
		Measured Link Clock: 45.000 MHz
		Reported Link Clock: 45.000 MHz
		External reset is deasserted
	OCPI( 3:765.0069): drc_n310: 
	Daughterboard--DBA:
	Power Off Reset Control
	-----------------------
	CHIP            | POR
	-----------------------
	CPLD Pathing    | x
	LMK04828        | x
	ADF4351         |
	DSA             |
	CPLD GPIO POR   | x
	Transceiver Obj | x
	-----------------------
	OCPI( 3:765.0069): drc_n310: 
	Daughterboard--DBB: INACTIVE
	Failed reset operation
	OCPI( 3:765.0069): drc_n310: release_config():
	Downgraded config 0 from prepared state to inactive state
	App is Finished .... 
	OCPI( 3:765.0182): drc_n310: 
	Daughterboard--DBA:
	Power Off Reset Control
	-----------------------
	CHIP            | POR
	-----------------------
	CPLD Pathing    |
	LMK04828        |
	ADF4351         |
	DSA             |
	CPLD GPIO POR   |
	Transceiver Obj |
	-----------------------
	OCPI( 3:765.0182): drc_n310: 
	Daughterboard--DBB: INACTIVE
	Failed reset operation
	OCPI( 3:765.0182): drc_n310: release_config():
	Downgraded config 0 from inactive state to inactive state




