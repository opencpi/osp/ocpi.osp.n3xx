#include <iostream>
#include <string>
#include <unistd.h> // sleep()
#include "OcpiApi.hh"

#define DBA 0
#define DBB 1

namespace OA = OCPI::API;

int main(int argc, char **argv) {
  
  char appNum;
  std::string appFileName;
  double timer = 10;
  int tunerEnable = 0;

  try {
    appNum = (char) *argv[1];

    switch(appNum){
      case '1':
        appFileName = "rf_cw_tx.xml";
        break;
      case '2':
        appFileName = "rf_cw_txrx.xml";
        break;
      case '3':
        appFileName = "rf_cw_loopback.xml";
        break;
      case '4':
        appFileName = "rf_cw_tx.xml";
        tunerEnable = 1;
        break;
      default:
        std::cout << "Please enter valid app number [1:4]" << std::endl;
        std::cout << std::endl;
        exit(-1);
    } 
  
    OA::Application app(appFileName);
    
    app.initialize(); // all resources have been allocated
    app.start();      // execution is started
    app.wait(100000);

    OA::Property start(app, "drc_n310", "start");
    OA::Property stop(app, "drc_n310", "stop");
    OA::Property prepare(app, "drc_n310", "prepare");
    OA::Property release(app, "drc_n310", "release");
    OA::Property configs(app,"drc_n310","configurations");

    prepare.setValue(0);

    if ((tunerEnable == 0)) {
      start.setValue(0);

      std::cout << std::endl;
      std::cout << "App runs for " << std::dec << timer << " seconds...\n";
      std::cout << std::endl;
      while(timer-- > 0) {
        sleep(1);
      }
      std::cout << std::endl;

      stop.setValue(0);
    }
    else {
      double startFreq;
      double stopFreq;
      double stepSize;
      double currentFreq;

      std::cout << std::endl;
      std::cout << "Enter Start Frequency (MHz): ";
      std::cin >> startFreq;

      std::cout << "Enter Stop Frequency (MHz): ";
      std::cin >> stopFreq;

      std::cout << "Enter Frequency Step Size (MHz): ";
      std::cin >> stepSize;      

      currentFreq = startFreq;

      while (currentFreq <= stopFreq){
        configs.setValue(currentFreq, {0, "channels", 0, "tuning_freq_MHz"});
        start.setValue(0);

        std::cout << std::endl;
        std::cout << "Tx Tuning Freq: " << std::dec << currentFreq << " MHz\n\n";
        app.wait(3000000);
        stop.setValue(0);
        currentFreq = currentFreq + stepSize; 
      }
    } 

    release.setValue(0);

    app.stop();
    app.wait(100000);
    app.finish();
    std::cout << "App is Finished .... " << std::endl; 

  } catch (std::string &e) {
    std::cerr << "app failed: " << e << std::endl;
    return 1;
  }
  return 0;
}
