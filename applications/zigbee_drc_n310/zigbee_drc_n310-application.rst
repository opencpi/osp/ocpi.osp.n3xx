.. zigbee_drc_n310 documentation


.. _zigbee_drc_n310-application:


ZigBee (``zigbee_drc_n310``)
============================

Description
-----------
  **This application will be deprecated within this OSP in future releases.** 

``n310`` implementation for the Zigbee reference design (``ocpi.zigbee``).  This application was used to verify the running of the ZigBee reference design on the ``n310`` platform using the following OpenCPI Application Specifications (OAS):

* zigbee_drc_n310_tx.xml - Transmitter
* zigbee_drc_n310_rx.xml - Receiver
* zigbee_drc_n310_txrx.xml - Transceiver
* zigbee_drc_n310_loopback.xml - Transceiver Baseband Loopback


Build
-----
1. Build the following components using the ``ocpidev build --hdl-plaform n310`` within each component directory (``./components``).

	- ocpi.osp.n3xx.pass_through.hdl
	- ocpi.osp.n3xx.quad_demod.hdl
	- ocpi.osp.n3xx.modified_clock_recovery.hdl
	- ocpi.osp.n3xx.slicer.hdl


2. Build the following assemblies using ``ocpidev build --hdl-plaform n310`` within each assemblies directory (``./hdl/assemblies``):

	- zigbee_tx
	- zigbee_rx
	- zigbee_txrx
	- zigbee_loopback


3. Build the application using ``ocpidev build --rcc-platform ettus_n310_v4``  within the application directory.  The application artifact is ``./target-ettus_n310_v4/zigbee_drc_n310``.


Prerequisites
-------------
1. Copy the rcc artifacts within ``./rcc directory`` of the zigbee_drc_n310 application directory into the OpenCPI artifacts directory (``/home/root/opencpi/artifacts``) on the ``n310`` platform):

	- ocpi.sdr.assets.ocpi-math.multiply_cc.rcc.0.ettus_n310_v4.so
	- ocpi.sdr.assets.ocpi-mod.chunks_to_symbols.rcc.0.ettus_n310_v4.so
	- ocpi.sdr.assets.ocpi-siggen.vector_source_c.rcc.0.ettus_n310_v4.so
	- ocpi.sdr.assets.ocpi-stream.packed_to_unpacked.rcc.0.ettus_n310_v4.so
	- ocpi.sdr.assets.ocpi-stream.repeat_cc.rcc.0.ettus_n310_v4.so
	- ocpi.sdr.assets.ocpi-utilities.delay_cc.rcc.0.ettus_n310_v4.so
	- ocpi.zigbee.ocpi-zigbee.access_code_prefixer.rcc.0.ettus_n310_v4.so
	- ocpi.zigbee.ocpi-zigbee.packet_sync.rcc.0.ettus_n310_v4.so


2. Copy DRC artifacts into the OpenCPI artifacts directory (``/home/root/opencpi/artifacts``) on the ``n310`` platform:

	- ocpi.osp.n3xx.devices.drc_n310.rcc.4.ettus_n310_v4.so (zigbee_drc_n310_tx.xml)
	- ocpi.osp.n3xx.devices.drc_n310.rcc.5.ettus_n310_v4.so (zigbee_drc_n310_rx.xml)
	- ocpi.osp.n3xx.devices.drc_n310.rcc.2.ettus_n310_v4.so (zigbee_drc_n310_txrx.xml and zigbee_drc_n310_loopback.xml)


3. Make ``bitz`` directory within the application working directory on the ``n310`` platform:

.. code-block:: bash

	% mkdir ./bitz


4. Copy the following the bitz files in the ``./artifacts`` directory into the ``bitz`` directory in the application working directory:

	- ocpi.osp.n3xx.zigbee_tx_n310_cfg_1tx_0rx_dba_cnt_1tx_0rx.hdl.0.n310.bitz
	- ocpi.osp.n3xx.zigbee_rx_n310_cfg_0tx_1rx_dba_cnt_0tx_1rx_dba.hdl.0.n310.bitz
	- ocpi.osp.n3xx.zigbee_txrx_n310_cfg_1tx_1rx_dba_cnt_1tx_1rx_dba.hdl.0.n310.bit
	- ocpi.osp.n3xx.zigbee_loopback_n310_cfg_loopback_dba_cnt_loopback_dba.hdl.0.n310.bitz


5. Copy the OAS files and ACI program into the application working directory:

	- zigbee_drc_n310_tx.xml
	- zigbee_drc_n310_rx.xml 
	- zigbee_drc_n310_txrx.xml 
	- zigbee_drc_n310_loopback.xml
	- target-ettus_n310_v4/zigbee_drc_n310


6. Make ``idata`` and ``odata`` directories within the application working directory:

.. code-block:: bash

	% mkdir ./zigbee_data_files
	% mkdir ./odata


7. Copy the source file and symbol table file into ``zigbee_data_files`` directory (files are located in ``./zigbee_data_files`` in zigbee_drc_n310 application directory):

    - zigbee_non_proto_msginfile_true.dat
    - sym_table.txt


Execution
---------
Copy and run the zigbee_drc_n310 application script in the application working directory on the ``n310`` platform:

.. code-block:: bash 

	% ./n310_zigbee_app


.. code-block:: bash

	ZigBee Applications
	----------------------------
	[1] ZIGBEE_DRC_N310_Tx
	[2] ZIGBEE_DRC_N310_Rx
	[3] ZIGBEE_DRC_N310_TxRx
	[4] ZIGBEE_DRC_N310_Loopback
	----------------------------
	Enter App Number: 
	3


Output
------

.. code-block:: bash

	OCPI( 3:215.0161): drc_n310: 
	Daughterboard--DBA:
	Power Off Reset Control
	-----------------------
	CHIP            | POR
	-----------------------
	CPLD Pathing    | x
	LMK04828        | x
	ADF4351         | x
	DSA             | x
	CPLD GPIO POR   | x
	Transceiver Obj | x
	-----------------------
	OCPI( 3:218.0102): drc_n310: DBA lmkController: PLLs locked
	OCPI( 3:218.0103): drc_n310: DBA lmkController: SYNC Event
	OCPI( 3:218.0132): drc_n310: DBA Transceiver: rx_clkgen: MMCM-PLL locked
	OCPI( 3:218.0152): drc_n310: DBA Transceiver: tx_clkgen: MMCM-PLL locked
	OCPI( 3:218.0922): drc_n310: DBA Transceiver: MCS successful
	OCPI( 3:218.0923): drc_n310: DBA Transceiver: CLKPLL locked
	OCPI( 3:223.0162): drc_n310: DBA Transceiver: AD9371 ARM version 5.2.2
	OCPI( 3:223.0483): drc_n310: DBA Transceiver: PLLs locked
	OCPI( 3:223.0503): drc_n310: DBA Transceiver: waiting for initCals (or 60 second timer)
	OCPI( 3:228.0447): drc_n310: DBA Transceiver: Calibrations completed successfully
	OCPI( 3:228.0562): drc_n310: DBA Transceiver: tx_adxcvr: OK
	OCPI( 3:228.0682): drc_n310: DBA Transceiver: rx_adxcvr: OK
	OCPI( 3:228.0693): drc_n310: DBA Transceiver: rx_jesd status:
		Link is enabled
		Measured Link Clock: 48.000 MHz
		Reported Link Clock: 48.000 MHz
		Lane rate: 1920.000 MHz
		Lane rate / 40: 48.000 MHz
		Link status: DATA
		SYSREF captured: Yes
		SYSREF alignment error: No
	OCPI( 3:228.0699): drc_n310: DBA Transceiver: rx_jesd lane 0 status:
	Errors: 0
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 2 Multi-frames and 28 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 0, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x47, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1920000 kHz
	OCPI( 3:228.0704): drc_n310: DBA Transceiver: rx_jesd lane 1 status:
	Errors: 1
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 2 Multi-frames and 22 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 1, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x48, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1920000 kHz
	OCPI( 3:228.0708): drc_n310: DBA Transceiver: rx_jesd lane 2 status:
	Errors: 0
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 2 Multi-frames and 28 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 2, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x49, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1920000 kHz
	OCPI( 3:228.0711): drc_n310: DBA Transceiver: rx_jesd lane 3 status:
	Errors: 1
		CGS state: DATA
		Initial Frame Synchronization: Yes
		Lane Latency: 2 Multi-frames and 4 Octets
		Initial Lane Alignment Sequence: Yes
		DID: 0, BID: 0, LID: 3, L: 4, SCR: 1, F: 2
		K: 32, M: 4, N: 16, CS: 0, N': 16, S: 1, HD: 0
		FCHK: 0x4A, CF: 0
		ADJCNT: 0, PHADJ: 0, ADJDIR: 0, JESDV: 1, SUBCLASS: 1
		FC: 1920000 kHz
	OCPI( 3:228.0823): drc_n310: DBA Transceiver: rx_adc: Successfully initialized
	OCPI( 3:228.0824): drc_n310: DBA Transceiver: tx_jesd status:
		Link is enabled
		Measured Link Clock: 48.000 MHz
		Reported Link Clock: 48.000 MHz
		Lane rate: 1920.000 MHz
		Lane rate / 40: 48.000 MHz
		SYNC~: deasserted
		Link status: DATA
		SYSREF captured: Yes
		SYSREF alignment error: No
	OCPI( 3:228.0933): drc_n310: DBA Transceiver: tx_dac: Successfully initialized
	OCPI( 3:228.0934): drc_n310: DBA Transceiver: Successful configuration of ad9371 chip
	OCPI( 3:229.0535): drc_n310: prepare_config():
	Upgraded config 0 from inactive state to prepared
	OCPI( 3:229.0536): drc_n310: update_tuning_frequency bypassed due to parameter remaining unchanged
	OCPI( 3:229.0537): drc_n310: update_tuning_frequency bypassed due to parameter remaining unchanged
	OCPI( 3:229.0653): drc_n310: start_config():
	Upgraded config 0 from prepared state to OPERATING state
	
	App runs for 60 seconds...
	
	OCPI( 4:229.0659): packet_sync: Decoded frame length: 12
	OCPI( 4:230.0854): packet_sync: Decoded frame length: 12
	OCPI( 4:231.0026): packet_sync: Decoded frame length: 12
	OCPI( 4:231.0294): packet_sync: Decoded frame length: 12
	OCPI( 4:231.0343): packet_sync: Decoded frame length: 12
	OCPI( 4:231.0385): packet_sync: Decoded frame length: 12
	OCPI( 4:231.0450): packet_sync: Decoded frame length: 47
	OCPI( 4:231.0478): packet_sync: Decoded frame length: 5
	OCPI( 4:231.0624): packet_sync: Decoded frame length: 5
	OCPI( 4:231.0832): packet_sync: Decoded frame length: 12
	OCPI( 4:232.0217): packet_sync: Decoded frame length: 10
	OCPI( 4:232.0322): packet_sync: Decoded frame length: 10
	OCPI( 4:232.0370): packet_sync: Decoded frame length: 12
	OCPI( 4:232.0386): packet_sync: Decoded frame length: 12
	OCPI( 4:232.0576): packet_sync: Decoded frame length: 12
	OCPI( 4:232.0595): packet_sync: Decoded frame length: 12
	OCPI( 4:232.0635): packet_sync: Decoded frame length: 57
	OCPI( 4:232.0664): packet_sync: Decoded frame length: 28
	OCPI( 4:232.0891): packet_sync: Decoded frame length: 5
	OCPI( 4:233.0337): packet_sync: Decoded frame length: 10
	OCPI( 4:233.0354): packet_sync: Decoded frame length: 10
	OCPI( 4:233.0456): packet_sync: Decoded frame length: 10
	OCPI( 4:233.0627): packet_sync: Decoded frame length: 10
	OCPI( 4:233.0657): packet_sync: Decoded frame length: 10
	OCPI( 4:233.0759): packet_sync: Decoded frame length: 12
	OCPI( 4:233.0826): packet_sync: Decoded frame length: 12
	OCPI( 4:233.0896): packet_sync: Decoded frame length: 28
	OCPI( 4:234.0035): packet_sync: Decoded frame length: 57
	
	OCPI( 3:290.0253): drc_n310: stop_config():
	Downgraded from operating state to PREPARED state
	OCPI( 3:290.0262): drc_n310: DBA Transceiver: rx_jesd status:
		Link is disabled
		Measured Link Clock: 48.000 MHz
		Reported Link Clock: 48.000 MHz
		External reset is deasserted
	OCPI( 3:290.0262): drc_n310: DBA Transceiver: tx_jesd status:
		Link is disabled
		Measured Link Clock: 48.000 MHz
		Reported Link Clock: 48.000 MHz
		External reset is deasserted
	OCPI( 3:290.0262): drc_n310: 
	Daughterboard--DBA:
	Power Off Reset Control
	-----------------------
	CHIP            | POR
	-----------------------
	CPLD Pathing    | x
	LMK04828        | x
	ADF4351         |
	DSA             |
	CPLD GPIO POR   | x
	Transceiver Obj | x
	-----------------------
	OCPI( 3:290.0262): drc_n310: 
	Daughterboard--DBB: INACTIVE
	Failed reset operation
	OCPI( 3:290.0262): drc_n310: release_config():
	Downgraded config 0 from prepared state to inactive state
	App is Finished .... 
	OCPI( 3:290.0384): drc_n310: 
	Daughterboard--DBA:
	Power Off Reset Control
	-----------------------
	CHIP            | POR
	-----------------------
	CPLD Pathing    |
	LMK04828        |
	ADF4351         |
	DSA             |
	CPLD GPIO POR   |
	Transceiver Obj |
	-----------------------
	OCPI( 3:290.0385): drc_n310: 
	Daughterboard--DBB: INACTIVE
	Failed reset operation
	OCPI( 3:290.0385): drc_n310: release_config():
	Downgraded config 0 from inactive state to inactive state


.. Troubleshooting
	---------------
	Skeleton outline: List any known issues and and potential fixes here.
