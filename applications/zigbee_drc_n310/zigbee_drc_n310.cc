#include <iostream>
#include <string>
#include <unistd.h> // sleep()
#include "OcpiApi.hh"

#define DBA 0
#define DBB 1
 
namespace OA = OCPI::API;

int main(int argc, char **argv) {
  // For an explanation of the ACI, see:
  // https://opencpi.gitlab.io/releases/develop/docs/OpenCPI_Application_Development_Guide.pdf

  char appNum;
  std:: string appFileName;
  unsigned timer = 60, i;
  std::string txIntFactor, txIntScale, rxIntFactor, rxIntScale;


  try {

    appNum = (char) *argv[1];
    
    switch(appNum){
      case '1':
        appFileName = "zigbee_drc_n310_tx.xml";
        break;
      case '2':
        appFileName = "zigbee_drc_n310_rx.xml";
        break;
      case '3':
        appFileName = "zigbee_drc_n310_txrx.xml";
        break;
      case '4':
        appFileName = "zigbee_drc_n310_loopback.xml";
        break;
      default:
        std::cout << "Please enter valid app number [1:4]" << std::endl;
        std::cout << std::endl;
        exit(-1);
    } 
  
    OA::Application app(appFileName);

    app.initialize(); // all resources have been allocated
    app.start();      // execution is started
    app.wait(100000);

    app.setPropertyValue<std::uint16_t>("drc_n310","prepare",0);
    app.setPropertyValue<std::uint16_t>("drc_n310","start",0);

    std::cout << std::endl;
    std::cout << "App runs for " << std::dec << timer << " seconds...\n";
    std::cout << std::endl;
    
    while(timer-- > 0) {
      sleep(1);
    }
    std::cout << std::endl;

    app.setPropertyValue<std::uint16_t>("drc_n310","stop",0);
    app.setPropertyValue<std::uint16_t>("drc_n310","release",0);

    app.wait(100000);
    app.stop();
    //app.dumpProperties();
    app.finish();
    std::cout << "App is Finished .... " << std::endl;

  } catch (std::string &e) {
    std::cerr << "app failed: " << e << std::endl;
    return 1;
  }
  return 0;
}

