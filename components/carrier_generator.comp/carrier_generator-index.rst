.. carrier_generator documentation

.. _carrier_generator:


Carrier Generator (``carrier_generator``)
=========================================
A numerically controlled oscillator (NCO) for generating complex sinusoidal signals.

Design
------
**This component is the equivalent of** ``carrier_generator_xs`` **component within OpenCPI SDR Components Library** (``ocpi.comp.sdr``) **.**

Generates a complex stream of shorts which are the sine and cosine waves, with both the amplitude and frequency of the output wave settable.

The mathematical representation of the implementation is given in :eq:`carrier_generator_xs-equation`.

.. math::
   :label: carrier_generator_xs-equation

   y[n] = A \cos{\left(2 \pi \frac{\delta}{2^{32}} n \right)} + j A \sin{\left(2 \pi \frac{\delta}{2^{32}} n \right)}

.. math::

In :eq:`carrier_generator_xs-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`A` is the output amplitude.

 * :math:`\delta` controls the frequency of the output wave, and is set by the ``step_size`` property.

The relation between the output frequency and :math:`\delta` is given by :eq:`carrier_generator_step_size-equation`.

The mathematical representation of the implementation is given in :eq:`carrier_generator_xs-equation`.

.. math::
   :label: carrier_generator_step_size-equation

    \delta = \frac{2^{32} f}{F_s}

In :eq:`carrier_generator_xs-equation`:

 * :math:`f` is the desired output frequency

 * :math:`F_s` is the sample rate of the system.

 * :math:`\delta` is the value same as set in :eq:`carrier_generator_xs-equation`.

Interface
---------
.. literalinclude:: ../specs/carrier_generator-spec.xml
   :language: xml

Ports
~~~~~
.. ocpi_documentation_ports::

   output: Primary output samples port.


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

 carrier_amplitude: Should not be set to greater than 19,872, as this has a safety margin under the maximum of the short output values - this value maps to near full range output on a 16-bit signed value.

 discontinuity_on_step_size_change: The discontinuity is sent directly after the last output sample generated using the previous step size is sent.

 discontinuity_on_carrier_amplitude_change: The discontinuity is sent directly after the last output sample generated using the previous carrier amplitude values is sent.

Implementations
---------------
.. ocpi_documentation_implementations:: ../carrier_generator.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`CORDIC DDS <cordic_dds-primitive>`

 * :ref:`Protocol interface generator primitive v2 <protocol_interface_generator_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``carrier_generator_xs`` are:

 * Setting of ``carrier_amplitude`` above 19,872 is untested behaviour and will result in unpredictable behaviour. At low carrier amplitude values (100 or less) the error from a true carrier becomes a significant proportion of the output signal magnitude.

 * Setting of ``message_size`` above ``ocpi_max_bytes_output``/``sample_size_bytes`` is unsupported behaviour.

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::