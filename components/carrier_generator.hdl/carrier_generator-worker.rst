.. carrier_generator HDL worker


.. _carrier_generator-HDL-worker:


``carrier_generator`` HDL Worker
================================

Detail
------
Implementation of a numerically controlled oscillator using the (Coordinate Rotation Digital Computer (CORDIC) algorithm.

The carrier generator is implemented as a numerically controlled oscillator.
The ``step_size`` property sets the step size in a phase accumulator unit which generates a sawtooth waveform between :math:`0` and :math:`2^{32}-1`. The frequency of the sawtooth wave determines the frequency of the output waveform.

The sawtooth waveform is input into a phase-to-amplitude converter (PAC) which generates a complex sinusoid at the desired frequency. The PAC is implemented using 16 iterations of the CORDIC algorithm to approximate the sine and cosine of the input phase.

The sine and cosine are combined together to generate a complex short output signal.

.. ocpi_documentation_worker::

.. Utilisation
   -----------
   .. ocpi_documentation_utilization::
