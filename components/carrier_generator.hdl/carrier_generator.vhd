-- THIS FILE WAS ORIGINALLY GENERATED ON Tue Jul 19 13:04:06 2022 EDT
-- BASED ON THE FILE: carrier_generator.xml
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.cordic_dds;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator_v2;

architecture rtl of worker is

  constant opcode_width_c : integer :=
    integer(ceil(log2(real(complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)+1))));

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  ------------------------------------------------------------------------------
  -- DDS Constants
  ------------------------------------------------------------------------------
  -- Sets the output bit width of the sine and cosine signals
  constant dds_output_size_c            : integer := output_out.data'length/2;
  -- Sets the phase accumulator size
  constant dds_phase_accumulator_size_c : integer := props_in.step_size'length;

  ------------------------------------------------------------------------------
  -- Interface Constants
  ------------------------------------------------------------------------------
  -- Number of clock cycles it takes for a change in step size or dds gain to
  -- reach the output.
  constant delay_c : integer := dds_output_size_c + 1;

  ------------------------------------------------------------------------------
  -- DDS Signals
  ------------------------------------------------------------------------------
  signal dds_gain    : std_logic_vector(dds_output_size_c -1 downto 0);
  signal dds_step    : std_logic_vector(dds_phase_accumulator_size_c - 1 downto 0);
  signal dds_sin_out : std_logic_vector(dds_output_size_c -1 downto 0);
  signal dds_cos_out : std_logic_vector(dds_output_size_c -1 downto 0);
  signal data_out    : std_logic_vector(output_out.data'length - 1 downto 0);

  ------------------------------------------------------------------------------
  -- Interface Signals
  ------------------------------------------------------------------------------
  signal enable        : std_logic;
  signal output_hold   : std_logic;
  signal discontinuity : std_logic;
  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data   : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  -- Enable the DDS when enable property is true and output is ready, but not
  -- when the interface module is outputting other message signals.
  enable <= output_in.ready and props_in.enable and not output_hold;

  dds_step <= std_logic_vector(props_in.step_size);
  dds_gain <= std_logic_vector(props_in.carrier_amplitude(dds_output_size_c - 1 downto 0));

  -- Triggers the insertion of a discontinuity message into the output
  -- data interface.
  discontinuity <= '1' when (props_in.step_size_written = '1' and
                             props_in.discontinuity_on_step_size_change = '1') or
                   (props_in.carrier_amplitude_written = '1' and
                    props_in.discontinuity_on_carrier_amplitude_change = '1') else '0';

  ------------------------------------------------------------------------------
  -- Instantiate DDS module
  ------------------------------------------------------------------------------
  dds_inst : cordic_dds
    generic map (
      output_size_g      => dds_output_size_c,
      phase_accum_size_g => dds_phase_accumulator_size_c
      )
    port map (
      clk        => ctl_in.clk,
      rst        => ctl_in.reset,
      clk_en     => enable,
      step_size  => dds_step,
      gain_in    => dds_gain,
      sine_out   => dds_sin_out,
      cosine_out => dds_cos_out
      );

  data_out <= std_logic_vector(signed(dds_sin_out)) &
              std_logic_vector(signed(dds_cos_out));

  interface_generator_i : protocol_interface_generator_v2
    generic map (
      delay_g                 => delay_c,
      data_width_g            => output_out.data'length,
      max_message_size_g      => to_integer(ocpi_max_bytes_output),
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => output_out.byte_enable'length,
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e),
      discontinuity_opcode_g  => opcode_to_slv(complex_short_timed_sample_discontinuity_op_e)
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_in.ready,
      discontinuity_trigger => discontinuity,
      generator_enable      => props_in.enable,
      generator_reset       => '0',
      processed_stream_in   => data_out,
      message_length        => props_in.message_length,
      output_hold           => output_hold,
      output_som            => open,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_opcode,
      output_data           => output_data
      );
  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
