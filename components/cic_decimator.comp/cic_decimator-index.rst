.. cic_decimator documentation

.. _cic_decimator:


CIC Decimator (``cic_decimator``)
=================================
CIC (cascading integrating comb) filter combined with decimator.

Design
------
**This component will be deprecated within this OSP in future releases.  It is the equivalent of** ``cic_decimator_xs`` **component within OpenCPI SDR Components Library** (``ocpi.comp.sdr``) **.**


Interface
---------
.. literalinclude:: ../specs/cic_decimator-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   down_sample_factor: Must not be set to zero.
   scale_output: Must not be set greater than :math:`\texttt{cic_reg_size} - 16`.
   cic_diff_delay: Must not be set to zero.

Implementations
---------------
.. ocpi_documentation_implementations:: ../cic_decimator.hdl

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`CIC decimator primitive <cic_dec-primitive>`

 * :ref:`Rounding (half-up) primitive <rounding_halfup-primitive>`

 * :ref:`Down-sample protocol interface delay primitive v2 <downsample_protocol_interface_delay_v2-primitive>`

 * :ref:`Timestamp recovery primitive <timestamp_recovery-primitive>`

 * :ref:`Flush inserter primitive v2 <flush_inserter_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``cic_decimator_xs`` are:

 * After a timestamp opcode message is received any zero length messages with the sample opcode are discarded until the next valid message with a sample opcode is received. Under all other conditions zero length sample opcode messages are passed directly through.

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::