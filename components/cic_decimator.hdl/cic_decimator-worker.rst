.. cic_decimator HDL worker


.. _cic_decimator-HDL-worker:


``cic_decimator`` HDL Worker
===============================

Detail
------
The worker implements the CIC decimator primitive, with two instances declared one for the real data values and the second for the imaginary data values.

.. ocpi_documentation_worker::

.. Utilisation
   -----------
   .. ocpi_documentation_utilization::
