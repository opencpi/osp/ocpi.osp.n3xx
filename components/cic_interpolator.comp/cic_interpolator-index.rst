.. cic_interpolator documentation

.. _cic_interpolator:


CIC Interpolator (``cic_interpolator``)
=======================================
CIC (cascading integrating comb) filter combined with interpolator.

Design
------
**This component will be deprecated within this OSP in future releases.  It is the equivalent of** ``cic_interpolator_xs`` **component within OpenCPI SDR Components Library** (``ocpi.comp.sdr``) **.**


Interface
---------
.. literalinclude:: ../specs/cic_interpolator-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   up_sample_factor: Must not be set to zero.
   scale_output: Must not be set greater than :math:`\texttt{cic_reg_size} - 16`.
   cic_diff_delay: Must not be set to zero.

Implementations
---------------
.. ocpi_documentation_implementations:: ../cic_interpolator.hdl


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`CIC interpolator primitive <cic_int-primitive>`

 * :ref:`Rounding (half-up) primitive <rounding_halfup-primitive>`

 * :ref:`Flush inserter primitive v2 <flush_inserter_v2-primitive>`

 * :ref:`Upsample Protocol Interface Delay primitive v2 <upsample_protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``cic_interpolator_xs`` are:

 * Up-sample factor must be greater than or equal to 2.

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::