.. cic_interpolator HDL worker


.. _cic_interpolator-HDL-worker:


``cic_interpolator`` HDL Worker
===============================

Detail
------
The worker implements the CIC interpolator primitive, with two instances declared one for the real data values and the second for the imaginary data values.

While the CIC filter stages are completed within the child primitive, the attenuation before the final output is done in the worker.

.. ocpi_documentation_worker::

.. Utilisation
   -----------
   .. ocpi_documentation_utilization::
