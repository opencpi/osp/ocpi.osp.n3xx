-- HDL Implementation of a CIC interpolator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- CIC interpolator
-- Increases the sample rate by a user settable factor

-- The module supports back pressure being asserted on it. During backpressure
-- no input data is taken and no processing occurs.
-- The module will apply back pressure to allow for its processing time.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cic_int;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of worker is
  ----------------------------------------------------------------------------
  -- Constants
  ----------------------------------------------------------------------------
  -- Set input and output sizes to 16 bit
  constant input_word_size_c  : integer := 16;
  constant output_word_size_c : integer := 16;
  -- Sets the size of the internal registers used to store the cic calculations
  constant cic_word_size_c    : integer := to_integer(cic_reg_size);
  -- Sets number of comb and integrator stages
  constant comb_stages_c      : integer := to_integer(cic_order);
  constant int_stages_c       : integer := to_integer(cic_order);
  constant diff_delay_c       : integer := to_integer(cic_diff_delay);

  ----------------------------------------------------------------------------
  -- Signals / Registers
  ----------------------------------------------------------------------------
  -- Data input signals
  signal data_in_i          	: signed(input_word_size_c - 1 downto 0);
  signal data_in_q          	: signed(input_word_size_c - 1 downto 0);
  -- Data output signals
  signal data_valid_out     	: std_logic;
  signal data_out_i         	: signed(cic_word_size_c - 1 downto 0);
  signal data_out_q         	: signed(cic_word_size_c - 1 downto 0);
  -- Output rounding signals
  signal rounded_data_out_i 	: signed(output_word_size_c - 1 downto 0);
  signal rounded_data_out_q 	: signed(output_word_size_c - 1 downto 0);
  signal scale_factor       	: integer range 0 to 127;
  -- Interface  signals
  signal input_hold         	: std_logic;
  signal input_hold_n        	: std_logic;
  signal reset              	: std_logic;
  signal take               	: std_logic;
  signal data_valid_in      	: std_logic;
  signal processed_data     	: std_logic_vector(output_out.data'range);
  signal input_interface_in 	: worker_input_in_t;
  signal input_interface_out	: worker_input_out_t;
  signal output_interface		  : worker_output_out_t; 

begin

  ----------------------------------------------------------------------------
  -- Flush inject
  ----------------------------------------------------------------------------
  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready.
  flush_insert_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      -- disable max message length by setting to the max length of a flush
      max_message_length_g => to_integer(ocpi_max_bytes_output)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => props_in.flush_length,
      input_out       => input_interface_out,
      input_interface => input_interface_in
      );

  input_out <= input_interface_out;

  ----------------------------------------------------------------------------
  -- Interface handling (with eof and back pressure support)
  ----------------------------------------------------------------------------

  input_hold_n <= not input_hold;
  take         <= output_in.ready and input_hold_n;

  -- Split I and Q data
  data_in_i <= signed(input_interface_in.data(input_word_size_c - 1 downto 0));
  data_in_q <= signed(input_interface_in.data((2 * input_word_size_c) - 1 downto input_word_size_c));

  -- Flags to indicate different message types on the input interface
  data_valid_in <= '1' when input_interface_in.valid = '1'
                   and input_interface_in.opcode = complex_short_timed_sample_sample_op_e else '0';

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the CIC interpolator.
  interface_delay_i : entity work.complex_short_protocol_delay
    generic map (
      -- delay due to comb stage
      stage1_delay_g => comb_stages_c,
      -- delay due to integrator stage and rounding
      stage2_delay_g => (int_stages_c + 1)
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => input_hold_n,
      input_in            => input_interface_in,
      up_sample_factor    => props_in.up_sample_factor,
      processed_stream_in => processed_data,
      output_out          => output_interface
      );

    output_out <= output_interface;

  ----------------------------------------------------------------------------
  -- CIC filter
  ----------------------------------------------------------------------------
  -- CIC interpolator reset signal
  reset <= ctl_in.reset or props_in.up_sample_factor_written;

  -- Instantiate two CIC modules for I and Q streams
  cic_module_i : cic_int
    generic map (
      int_stages_g       => int_stages_c,
      comb_stages_g      => comb_stages_c,
      diff_delay_g       => diff_delay_c,
      input_word_size_g  => input_word_size_c,
      output_word_size_g => cic_word_size_c
      )
    port map (
      clk              => ctl_in.clk,
      reset            => reset,
      clk_en           => output_in.ready,
      data_valid_in    => data_valid_in,
      data_in          => data_in_i,
      up_sample_factor => props_in.up_sample_factor,
      input_hold       => input_hold,
      data_valid_out   => data_valid_out,
      data_out         => data_out_i
      );

  cic_module_q : cic_int
    generic map (
      int_stages_g       => int_stages_c,
      comb_stages_g      => comb_stages_c,
      diff_delay_g       => diff_delay_c,
      input_word_size_g  => input_word_size_c,
      output_word_size_g => cic_word_size_c
      )
    port map (
      clk              => ctl_in.clk,
      reset            => reset,
      clk_en           => output_in.ready,
      data_valid_in    => data_valid_in,
      data_in          => data_in_q,
      up_sample_factor => props_in.up_sample_factor,
      input_hold       => open,
      data_valid_out   => open,
      data_out         => data_out_q
      );

  ----------------------------------------------------------------------------
  -- Output rounding and scaling
  ----------------------------------------------------------------------------
  -- Round output using half-up adder

  -- Get scale factor from property
  scale_factor <= to_integer(props_in.scale_output);

  halfup_rounder_i : rounding_halfup
    generic map (
      input_width_g  => cic_word_size_c,
      output_width_g => output_word_size_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => output_in.ready,
      data_in        => data_out_i,
      data_out       => rounded_data_out_i,
      binary_point   => scale_factor,
      data_valid_in  => data_valid_out,
      data_valid_out => open
      );

  halfup_rounder_q : rounding_halfup
    generic map (
      input_width_g  => cic_word_size_c,
      output_width_g => output_word_size_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => output_in.ready,
      data_in        => data_out_q,
      data_out       => rounded_data_out_q,
      binary_point   => scale_factor,
      data_valid_in  => data_valid_out,
      data_valid_out => open
      );

  processed_data <= std_logic_vector(rounded_data_out_q) &
                    std_logic_vector(rounded_data_out_i);


  DEBUG_ILA: if its(VIVADO_ILA_p) generate  

    signal input_data   : std_logic_vector(31 downto 0);
    signal input_vld    : std_logic_vector(0 downto 0);
    signal input_rdy    : std_logic_vector(0 downto 0);
    signal input_take   : std_logic_vector(0 downto 0);
    signal cic_I_in     : std_logic_vector(15 downto 0);
    signal cic_I_out    : std_logic_vector(35 downto 0);
    signal cic_I_vld    : std_logic_vector(0 downto 0);
    signal cic_Q_in     : std_logic_vector(15 downto 0);
    signal cic_Q_vld    : std_logic_vector(0 downto 0);
    signal cic_Q_out    : std_logic_vector(35 downto 0);
    signal output_data  : std_logic_vector(31 downto 0);
    signal output_vld   : std_logic_vector(0 downto 0); 
    signal output_rdy   : std_logic_vector(0 downto 0);
    signal rounded_I    : std_logic_vector(15 downto 0);
    signal rounded_q    : std_logic_vector(15 downto 0);

  begin

   input_data <= input_in.data(31 downto 0);
   input_vld(0) <= input_in.valid;
   input_rdy(0) <= input_in.ready;
   input_take(0) <= input_interface_out.take;
   cic_I_in <= std_logic_vector(data_in_i);
   cic_I_out <= std_logic_vector(data_out_i);
   cic_I_vld(0) <= data_valid_in;
   cic_Q_in <= std_logic_vector(data_in_q);
   cic_Q_out <= std_logic_vector(data_out_q);
   cic_Q_vld(0) <= data_valid_in;
   rounded_I <= std_logic_vector(rounded_data_out_i);
   rounded_Q <= std_logic_vector(rounded_data_out_q);
   output_data <= output_interface.data;
   output_vld(0) <= output_interface.valid;
   output_rdy(0) <= output_in.ready;

    
    
    ila : work.cic_interpolator_xs_pkg.ila_0
      port map ( clk => ctl_in.clk,
        probe0 => input_data,
        probe1 => input_vld,
        probe2 => input_rdy,
        probe3 => input_take,
        probe4 => cic_I_in,
        probe5 => cic_I_vld,
        probe6 => cic_Q_in,
        probe7 => cic_Q_vld,
        probe8 => rounded_I,
        probe9 => rounded_Q,
        probe10 => output_data,
        probe11 => output_vld,
        probe12 => output_rdy,
        probe13 => cic_I_out,
        probe14 => cic_Q_out );
  
  end generate;

end rtl;
