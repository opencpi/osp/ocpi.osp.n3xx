library ieee; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;

package cic_interpolator_xs_pkg is

	component ila_0 is
	  port (
	    clk      : in  std_logic;
	    probe0   : in  std_logic_vector(31 downto 0);
	    probe1   : in  std_logic_vector(0 downto 0);
	    probe2   : in  std_logic_vector(0 downto 0);
	    probe3   : in  std_logic_vector(0 downto 0);
	    probe4   : in  std_logic_vector(15 downto 0);
	    probe5   : in  std_logic_vector(0 downto 0);
	    probe6   : in  std_logic_vector(15 downto 0);
	    probe7   : in  std_logic_vector(0 downto 0);
	    probe8   : in  std_logic_vector(15 downto 0);
	    probe9   : in  std_logic_vector(15 downto 0);
	    probe10  : in  std_logic_vector(31 downto 0);
	    probe11  : in  std_logic_vector(0 downto 0);
	    probe12  : in  std_logic_vector(0 downto 0);
	    probe13  : in  std_logic_vector(35 downto 0);
	    probe14  : in  std_logic_vector(35 downto 0) );
	end component;

end package cic_interpolator_xs_pkg;