.. constant_generator documentation

.. _constant_generator:

Constant Generator (``constant_generator``)
===========================================

Design
------
**This component is based on** ``constant_s`` **component within OpenCPI SDR Components Library** (``ocpi.comp.sdr``).  **It was modified to use** ``complex_short_timed_sample`` **protocol.**

Generates a constant stream of samples whose values are set by the ``value`` property.

The component can be enabled or disabled during run time. When enabled values are produced whenever the output is ready. When the component is disabled no output values are generated.

The generated output values are settable during run time. There is an option to output a discontinuity message after a change in output value.

The size of the generated stream messages can be set during run time and must be between 1 and ``ocpi_max_bytes_output``/``sample_size_bytes``.

Interface
---------
.. literalinclude:: ../specs/constant_generator-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::



Opcode Handling
~~~~~~~~~~~~~~~
Will generate messages with sample opcode. Discontinuity opcode messages will only be generated if ``discontinuity_on_value_change`` is set.


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Implementations
---------------
.. ocpi_documentation_implementations:: ../constant_generator.hdl


Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface generator primitive v2 <protocol_interface_generator_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``


Limitations
-----------
Limitations of ``constant_s`` are:

 * Setting ``message_length`` to 0 or greater than ``ocpi_max_bytes_output``/``sample_size_bytes`` results in unsupported behaviour.

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::