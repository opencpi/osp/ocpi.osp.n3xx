.. constant_generator HDL worker


.. _constant_generator-HDL-worker:


``constant_generator`` HDL Worker
=================================

Detail
------
The ``value`` property sets the I samples.  Q samples is set to zero.


.. ocpi_documentation_worker::


.. Utilization
   -----------
   .. ocpi_documentation_utilization::
