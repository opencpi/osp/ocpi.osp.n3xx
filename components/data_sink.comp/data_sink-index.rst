.. data_sink documentation

.. _data_sink:


Data Sink (``data_sink``)
=============================
Digital Logic Analyzer Sink.  

Design
------
Simple data sink for the implementation of a digital logic analyzer.


Ports
~~~~~
.. ocpi_documentation_ports::

   in: Input Data Port


Implementations
---------------
.. ocpi_documentation_implementations:: ../data_sink.hdl


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * NONE

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``data_sink`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
