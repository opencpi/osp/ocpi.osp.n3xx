.. data_sink HDL worker


.. _data_sink-HDL-worker:


``data_sink`` HDL Worker
========================

Detail
------
HDL implementation of a Xilinx Integrated Logic Analyzer (ILA) for the data port.

.. ocpi_documentation_worker::


.. Utilization
   -----------
   .. ocpi_documentation_utilization::
