-- THIS FILE WAS ORIGINALLY GENERATED ON Mon Nov 22 09:47:41 2021 EST
-- BASED ON THE FILE: data_sink.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all;
library work; use work.ila_pkg.all;


architecture Structural of worker is

	signal valid	: std_logic_vector(0 downto 0);
	signal ready	: std_logic_vector(0 downto 0);
	signal reset	: std_logic_vector(0 downto 0);
	signal som		: std_logic_vector(0 downto 0);
	signal eom		: std_logic_vector(0 downto 0);
	signal eof		: std_logic_vector(0 downto 0);
	signal take 	: std_logic_vector(0 downto 0);

begin

  take(0) <= in_in.valid;
  in_out.take <= to_bool(take);
  
  valid(0) <= in_in.valid;
  ready(0) <= in_in.ready;
  reset(0) <= in_in.reset;
  som(0) <= in_in.som;
  eom(0) <= in_in.eom;
  eof(0) <= in_in.eof;


  jesd_ila: ila_0
      port map (
        clk => ctl_in.clk,
        probe0 => in_in.data,
        probe1 => valid,
        probe2 => ready,
        probe3 => (others => '0'),
        probe4 => reset,
        probe5 => som,
        probe6 => eom,
        probe7 => eof,
        probe8 => take );  

end Structural;
