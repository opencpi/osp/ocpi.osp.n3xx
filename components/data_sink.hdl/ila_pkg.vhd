library IEEE; use IEEE.std_logic_1164.all, IEEE.numeric_std.all;

package ila_pkg is


component ila_0
    port (
      clk      : in  std_logic;
      probe0   : in  std_logic_vector(31 downto 0);
      probe1   : in  std_logic_vector(0 downto 0);
      probe2   : in  std_logic_vector(0 downto 0);
      probe3   : in  std_logic_vector(2 downto 0);
      probe4   : in  std_logic_vector(0 downto 0);
      probe5   : in  std_logic_vector(0 downto 0);
      probe6   : in  std_logic_vector(0 downto 0);
      probe7   : in  std_logic_vector(0 downto 0);
      probe8   : in  std_logic_vector(0 downto 0) );
end component;

end package ila_pkg;