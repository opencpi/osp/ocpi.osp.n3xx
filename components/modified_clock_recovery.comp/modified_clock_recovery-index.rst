.. modified_clock_recovery documentation

.. _modified_clock_recovery:


Clock Recovery(``modified_clock_recovery``)
===========================================
Mueller and Muller dicrete-time error-tacking synchronizer.  

Design
------

**This component will be deprecated within this OSP in future releases.  It is the equivalent of** ``modified_clock_recovery_mm_cc`` **component within OpenCPI SDR Assets Library** (``ocpi.sdr.assets``) **.**


Interface
---------
.. literalinclude:: ../specs/modified_clock_recovery-spec.xml
   :language: xml
   :lines: 1,4-



Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Ports
~~~~~
.. ocpi_documentation_ports::


Implementations
---------------
.. ocpi_documentation_implementations:: ../modified_clock_recovery.hdl


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * NONE

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``modified_clock_recovery`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
