-- THIS FILE WAS ORIGINALLY GENERATED ON Fri Oct 14 18:00:22 2022 EDT
-- BASED ON THE FILE: modified_clock_recovery.xml
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.wci.all;
use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library sdr_assets_timed_sample_prot;
use sdr_assets_timed_sample_prot.timed_sample.all;

architecture rtl of worker is

    constant single_width : positive := in_in.data'length;
    constant double_width : positive := single_width*2;

    constant NTAPS  : integer := 8;
    constant NSTEPS : integer := 128;

    type data_array is array (NTAPS-1 downto 0) of signed(single_width-1 downto 0);
    signal samples : data_array;

    signal current_sample : signed(single_width-1 downto 0) := (others => '0');
    signal last_sample    : signed(single_width-1 downto 0) := (others => '0');

    type taps_t is array (0 to 7) of integer;
    signal taps : taps_t;

    signal in_idx : integer := NTAPS;
    signal ctr1   : integer := 0;
    signal imu    : integer := 0;

    -- Q 2.15 +3.999 to -3.999 
    signal gain_mu     : signed(single_width-1 downto 0) := (others => '0');
    signal gain_omega  : signed(single_width-1 downto 0) := (others => '0');
    signal omega_mid   : signed(double_width-1 downto 0) := (others => '0');
    signal omega       : signed(double_width-1 downto 0) := (others => '0');

    signal mu            : signed(single_width-1 downto 0) := (others => '0');
    signal omega_lim     : signed(double_width-1 downto 0) := (others => '0');
    signal neg_omega_lim : signed(double_width-1 downto 0) := (others => '0');
    signal mm_val        : signed(single_width-1 downto 0) := (others => '0');

    signal gmu_mm   : signed(single_width-1 downto 0) := (others => '0');
    signal gomeg_mm : signed(single_width-1 downto 0) := (others => '0');

    signal Recstar       : std_logic := '1';
    signal interpolating : std_logic := '0';
    signal done_intp     : std_logic := '0';
    signal done_mmval    : std_logic := '0';

    signal miss_data : std_logic := '0';

    signal vld_s, eof_s : std_logic := '0';

    ---------------------------------------
    -- Demarshaller and Marshaller Signals
    ---------------------------------------

    signal i_demarshaller_oprotocol : protocol_t := PROTOCOL_ZERO;
    signal o_marshaller_oprotocol   : protocol_t := PROTOCOL_ZERO;

    signal iclk_opcode : sdr_assets_timed_sample_prot.timed_sample.opcode_t := sdr_assets_timed_sample_prot.timed_sample.SAMPLE;
    signal oclk_opcode : sdr_assets_timed_sample_prot.timed_sample.opcode_t := sdr_assets_timed_sample_prot.timed_sample.SAMPLE;

    signal samps_op            : std_logic := '0';
    signal sm_ready            : std_logic := '0';
    signal i_demarshaller_oeof : std_logic := '0';
    signal i_demarshaller_irdy : std_logic := '0';
    signal oclk_out_irdy       : std_logic := '0';

    signal demarsh_sample : std_logic_vector(single_width-1 downto 0) := (others => '0');
    signal tmp_sample     : std_logic_vector(single_width-1 downto 0) := (others => '0');
    signal oclk_data      : std_logic_vector(single_width-1 downto 0) := (others => '0');

begin

    omega_lim     <=  props_in.omega_limit + omega_mid;
    neg_omega_lim <= -props_in.omega_limit + omega_mid;

    omega_mid  <= '0' & props_in.omega & ("000000000000000");
    gain_omega <= props_in.gain_omega;
    gain_mu    <= props_in.gain_mu;

    mm_val_update : process(ctl_in.clk)
        variable vmm_val : signed(single_width-1 downto 0) := (others => '0');
        variable csf     : std_logic :='0';
        variable lsf     : std_logic :='0';
        variable sel     : std_logic_vector(1 downto 0) := "00";
    begin
        if rising_edge(ctl_in.clk) then
            done_mmval <= '0';
            if samps_op = '1' and done_intp = '1' and ctl_in.state = OPERATING_e  and oclk_out_irdy = '1' then

                csf := not current_sample(current_sample'high);
                lsf := not last_sample(last_sample'high);

                sel := csf & lsf;

                case sel is
                    when "11" =>
                        vmm_val := current_sample - last_sample;
                    when "10" =>
                        vmm_val := -current_sample - last_sample;
                    when "01" =>
                        vmm_val := current_sample + last_sample;
                    when others =>
                        vmm_val := -current_sample + last_sample;
                end case;

                mm_val     <= vmm_val;
                gmu_mm     <= shift_right(gain_mu    * vmm_val, 15)(single_width-1 downto 0);
                gomeg_mm   <= shift_right(gain_omega * vmm_val, 15)(single_width-1 downto 0);
                done_mmval <= '1';
            end if;
        end if;
    end process mm_val_update;

    omega_update : process(ctl_in.clk)
        variable vint_pre_omega : signed(double_width-1 downto 0) := (others => '0');
        variable vint_omega     : signed(double_width-1 downto 0) := (others => '0');

        variable tmp_cal : signed(double_width-1 downto 0) := (others => '0');
        variable imu_pre : signed(single_width-1 downto 0) := (others => '0');
        variable int_mu  : signed(double_width-1 downto 0) := (others => '0');

    begin
        if rising_edge(ctl_in.clk) then
            if ctl_in.reset = '1' then
                int_mu := (others => '0');
                mu    <= (others => '0');
                omega <= (others => '0');
                imu   <= 0;
            elsif ctl_in.state = INITIALIZED_e then
                int_mu := ("0000000000000000") & props_in.mu;
                mu    <= props_in.mu;
                omega <= '0' & props_in.omega & ("000000000000000");
                imu   <= to_integer(NSTEPS - shift_right((('0' & int_mu(single_width-2 downto 0)) * NSTEPS), 15));
            elsif samps_op = '1' and done_mmval = '1' and ctl_in.state = OPERATING_e then

                vint_pre_omega := omega + gomeg_mm;

                if vint_pre_omega > omega_lim then
                    vint_omega := omega_lim;
                elsif vint_pre_omega < neg_omega_lim then
                    vint_omega := neg_omega_lim;
                else
                    vint_omega := vint_pre_omega;
                end if;

                int_mu := mu + vint_omega + gmu_mm;

                omega <= vint_omega;
                -- This should be equivalent to mu = mu - floor(mu)
                mu <= '0' & int_mu(single_width-2 downto 0);

                in_idx <= to_integer(unsigned(int_mu(double_width-2 downto single_width-1)));

                if int_mu >= 0 then 
                    tmp_cal := ('0' & int_mu(single_width-2 downto 0)) * NSTEPS;
                    imu_pre := shift_right(tmp_cal, 15)(single_width-1 downto 0);
                    if tmp_cal(single_width-2) = '1' then
                        imu_pre := imu_pre + 1;
                    end if;
                    imu <= to_integer(NSTEPS - imu_pre);
                else
                    imu <= NSTEPS;
                end if;
            end if;
        end if;
    end process omega_update;

    intp_taps : sdr_assets_fir.fir.interpolator_taps
    generic map (USE_CLOCK => false)
    port map (
    clk   => ctl_in.clk,
    addr  => imu,
    tap_0 => taps(0),
    tap_1 => taps(1),
    tap_2 => taps(2),
    tap_3 => taps(3),
    tap_4 => taps(4),
    tap_5 => taps(5),
    tap_6 => taps(6),
    tap_7 => taps(7));

    interpolate : process(ctl_in.clk)
        variable m0, m1, m2, m3, m4, m5, m6, m7 : signed(single_width-1 downto 0) := (others => '0');
    begin
        if rising_edge(ctl_in.clk) then
            if oclk_out_irdy = '1' then
                done_intp <= '0';
            end if;
            if interpolating = '1' and ctl_in.state = OPERATING_e and samps_op = '1' and oclk_out_irdy = '1' then

                m0 := signed(shift_right(samples(7) * taps(0), 15)(single_width-1 downto 0));
                m1 := signed(shift_right(samples(6) * taps(1), 15)(single_width-1 downto 0));
                m2 := signed(shift_right(samples(5) * taps(2), 15)(single_width-1 downto 0));
                m3 := signed(shift_right(samples(4) * taps(3), 15)(single_width-1 downto 0));
                m4 := signed(shift_right(samples(3) * taps(4), 15)(single_width-1 downto 0));
                m5 := signed(shift_right(samples(2) * taps(5), 15)(single_width-1 downto 0));
                m6 := signed(shift_right(samples(1) * taps(6), 15)(single_width-1 downto 0));
                m7 := signed(shift_right(samples(0) * taps(7), 15)(single_width-1 downto 0));

                last_sample <= current_sample;
                current_sample <= resize(m0 + m1 + m2 + m3 + m4 + m5 + m6 + m7, single_width);
                done_intp <= '1';
            end if;
        end if;
    end process interpolate;

    -- Get the input array
    shiftin : process(ctl_in.clk)
    begin
        if rising_edge(ctl_in.clk) then
            if ctl_in.reset = '1' then
                samples <= (others => (others => '0'));
            elsif ctl_in.state = INITIALIZED_e then
                samples <= (others => (others => '0'));
            elsif i_demarshaller_irdy = '1' and i_demarshaller_oprotocol.sample_vld = '1' then
                samples(NTAPS-1 downto 0) <= samples(NTAPS-2 downto 0) & signed(demarsh_sample);
            end if;
        end if;
    end process shiftin;

    -- Count how many inputs are needed
    Receive : process(ctl_in.clk)
    begin
        if (rising_edge(ctl_in.clk)) then
            if (ctl_in.reset = '1') then
                Recstar         <= '1';
                ctr1            <=  0;
            else
                if i_demarshaller_irdy = '1' and i_demarshaller_oprotocol.sample_vld = '1' then
                    if (ctr1 >= in_idx-1) then
                        ctr1    <= 0;
                        Recstar <= '0';
                    else
                        ctr1    <= ctr1 + 1;
                        Recstar <= '1';
                    end if;
                elsif (miss_data = '1') then
                    Recstar <= '0';
                else
                    Recstar <= '1';
                end if;
            end if;
        end if;
    end process;

    -- Convert input opcode to something the demarshaller can handle
    iclk_opcode <=
        SAMPLE          when in_in.opcode = short_timed_sample_sample_op_e          else
        TIME_TIME       when in_in.opcode = short_timed_sample_time_op_e            else
        SAMPLE_INTERVAL when in_in.opcode = short_timed_sample_sample_interval_op_e else
        FLUSH           when in_in.opcode = short_timed_sample_flush_op_e           else
        DISCONTINUITY   when in_in.opcode = short_timed_sample_discontinuity_op_e   else
        METADATA        when in_in.opcode = short_timed_sample_metadata_op_e        else
        SAMPLE;

        in_demarshaller : timed_sample_demarshaller
        generic map(
            WSI_DATA_WIDTH => single_width)
        port map(
            clk       => ctl_in.clk,
            rst       => ctl_in.reset,
            -- INPUT
            idata     => in_in.data,
            ivalid    => in_in.valid,
            iready    => in_in.ready,
            isom      => in_in.som,
            ieom      => in_in.eom,
            iopcode   => iclk_opcode,
            ieof      => in_in.eof,
            itake     => in_out.take,
            -- OUTPUT
            samp_data => demarsh_sample,
            oprotocol => i_demarshaller_oprotocol,
            oeof      => i_demarshaller_oeof,
            ordy      => i_demarshaller_irdy);

    -- Tells the Demarshaller that it can continue to operate, unless the marshaller isn't ready or the state machine isn't ready
    i_demarshaller_irdy <=  sm_ready when samps_op = '1' else
                            (in_in.ready or in_in.eof) and oclk_out_irdy;

    -- Stay samples opcode until we output a valid
    samps_op <= '1' when iclk_opcode = SAMPLE or interpolating = '1' or done_intp = '1' or done_mmval = '1' else '0';

    sm_ready <= not miss_data and not done_intp and not interpolating and not done_mmval;

    interpolating <= not Recstar and oclk_out_irdy;

    miss_data <= '1' when Recstar = '0' and done_mmval = '0' and oclk_out_irdy = '0' else '0';

    vld_s <= oclk_out_irdy and done_intp;

    tmp_sample <= std_logic_vector(current_sample);

    -- When the loop has a valid output sample, sets the samples valid signal
    o_marshaller_oprotocol.sample_vld <= vld_s;

    -- All other opcodes just need the demarshaller and marshaller to be ready
    o_marshaller_oprotocol.time                <= i_demarshaller_oprotocol.time;
    o_marshaller_oprotocol.time_vld            <= i_demarshaller_oprotocol.time_vld            and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.sample_interval     <= i_demarshaller_oprotocol.sample_interval;
    o_marshaller_oprotocol.sample_interval_vld <= i_demarshaller_oprotocol.sample_interval_vld and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.flush               <= i_demarshaller_oprotocol.flush               and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.discontinuity       <= i_demarshaller_oprotocol.discontinuity       and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.metadata            <= i_demarshaller_oprotocol.metadata;
    o_marshaller_oprotocol.metadata_vld        <= i_demarshaller_oprotocol.metadata_vld        and oclk_out_irdy and i_demarshaller_irdy;

    eof_s <= i_demarshaller_oeof and sm_ready;

    out_marshaller : timed_sample_marshaller
    generic map(
        WSI_DATA_WIDTH    => single_width,
        WSI_MBYTEEN_WIDTH => out_out.byte_enable'length)
    port map(
        clk          => ctl_in.clk,
        rst          => ctl_in.reset,
        -- INPUT
        samp_data    => tmp_sample,
        iprotocol    => o_marshaller_oprotocol,
        ieof         => eof_s,
        irdy         => oclk_out_irdy,
        -- OUTPUT
        odata        => oclk_data,
        ovalid       => out_out.valid,
        obyte_enable => out_out.byte_enable,
        ogive        => out_out.give,
        osom         => out_out.som,
        oeom         => out_out.eom,
        oeof         => out_out.eof,
        oopcode      => oclk_opcode,
        oready       => out_in.ready);

    -- this only needed to avoid build bug for xsim:
    -- ERROR: [XSIM 43-3316] Signal SIGSEGV received.
    out_out.data <= oclk_data;

    -- Set output opcode
    out_out.opcode <=
        short_timed_sample_sample_op_e          when oclk_opcode = SAMPLE          else
        short_timed_sample_time_op_e            when oclk_opcode = TIME_TIME       else
        short_timed_sample_sample_interval_op_e when oclk_opcode = SAMPLE_INTERVAL else
        short_timed_sample_flush_op_e           when oclk_opcode = FLUSH           else
        short_timed_sample_discontinuity_op_e   when oclk_opcode = DISCONTINUITY   else
        short_timed_sample_metadata_op_e        when oclk_opcode = METADATA        else
        short_timed_sample_sample_op_e;

end rtl;
