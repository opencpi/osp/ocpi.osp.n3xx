.. pass_through documentation

.. _pass_through:


Pass Through (``pass_through``)
================================
Simple pass-through from the input port to the output port

Design
------

**This component will be deprecated within this OSP in future releases.  It is the equivalent of** ``csts_pass_through`` **component within OpenCPI SDR Assets Library** (``ocpi.sdr.assets``) **.**



Ports
~~~~~
.. ocpi_documentation_ports::


Implementations
---------------
.. ocpi_documentation_implementations:: ../pass_through.hdl


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * NONE

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``pass_through`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
