-- THIS FILE WAS ORIGINALLY GENERATED ON Mon Oct 17 08:01:34 2022 EDT
-- BASED ON THE FILE: pass_through.xml
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library sdr_assets_timed_sample_prot;
use sdr_assets_timed_sample_prot.timed_sample.all;

library sdr_assets_protocol_util;
use sdr_assets_protocol_util.protocol_util.all;

architecture rtl of worker is

    signal input_sample_data  : std_logic_vector(in_in.data'length-1 downto 0); -- Input Samples Data
    signal input_sample_vld   : std_logic; -- Valid for input samples
    signal input_sample_eom   : std_logic; -- Eom on sample data
    signal input_eof          : std_logic;
    signal input_rdy          : std_logic; -- Tell the input that the worker is ready for data

    signal output_sample_data : std_logic_vector(out_out.data'length-1 downto 0); -- Output Samples Data
    signal output_sample_vld  : std_logic; -- Valid for output samples
    signal output_sample_eom  : std_logic; -- Samples End of Message
    signal output_eof         : std_logic;
    signal output_rdy         : std_logic; -- The output is ready for data

    signal oclk_data          : std_logic_vector(out_out.data'length-1 downto 0);

    ---------------------------------------
    -- Demarshaller and Marshaller Signals
    ---------------------------------------
    signal input_protocol  : protocol_t;
    signal output_protocol : protocol_t;

    signal iclk_opcode : sdr_assets_timed_sample_prot.timed_sample.opcode_t;
    signal oclk_opcode : sdr_assets_timed_sample_prot.timed_sample.opcode_t;

    ---------------------------------------
    -- Passthrough Marshaller Signals
    ---------------------------------------
    signal reset           : std_logic;
    signal iclk_opcode_num : natural;
    signal oclk_opcode_num : natural;

begin

    using_marshallers : if use_marsh = '1' generate
        -- Convert input opcode to something the demarshaller can handle
        iclk_opcode <=
            SAMPLE          when in_in.opcode = complex_short_timed_sample_sample_op_e          else
            TIME_TIME       when in_in.opcode = complex_short_timed_sample_time_op_e            else
            SAMPLE_INTERVAL when in_in.opcode = complex_short_timed_sample_sample_interval_op_e else
            FLUSH           when in_in.opcode = complex_short_timed_sample_flush_op_e           else
            DISCONTINUITY   when in_in.opcode = complex_short_timed_sample_discontinuity_op_e   else
            METADATA        when in_in.opcode = complex_short_timed_sample_metadata_op_e        else
            SAMPLE;

        in_demarshaller : timed_sample_demarshaller
        generic map(
            WSI_DATA_WIDTH => in_in.data'length)
        port map(
            clk       => ctl_in.clk,
            rst       => ctl_in.reset,
            -- INPUT
            idata     => in_in.data,
            ivalid    => in_in.valid,
            iready    => in_in.ready,
            isom      => in_in.som,
            ieom      => in_in.eom,
            iopcode   => iclk_opcode,
            ieof      => in_in.eof,
            itake     => in_out.take,
            -- OUTPUT
            samp_data => input_sample_data,
            oprotocol => input_protocol,
            oeof      => input_eof,
            ordy      => input_rdy);

        out_marshaller : timed_sample_marshaller
        generic map(
            WSI_DATA_WIDTH    => out_out.data'length,
            WSI_MBYTEEN_WIDTH => out_out.byte_enable'length)
        port map(
            clk          => ctl_in.clk,
            rst          => ctl_in.reset,
            -- INPUT
            samp_data    => output_sample_data,
            iprotocol    => output_protocol,
            ieof         => output_eof,
            irdy         => output_rdy,
            -- OUTPUT
            odata        => oclk_data,
            ovalid       => out_out.valid,
            obyte_enable => out_out.byte_enable,
            ogive        => out_out.give,
            osom         => out_out.som,
            oeom         => out_out.eom,
            oeof         => out_out.eof,
            oopcode      => oclk_opcode,
            oready       => out_in.ready);

        -- this only needed to avoid build bug for xsim:
        -- ERROR: [XSIM 43-3316] Signal SIGSEGV received.
        out_out.data <= oclk_data;

        -- Set output opcode
        out_out.opcode <=
            complex_short_timed_sample_sample_op_e          when oclk_opcode = SAMPLE          else
            complex_short_timed_sample_time_op_e            when oclk_opcode = TIME_TIME       else
            complex_short_timed_sample_sample_interval_op_e when oclk_opcode = SAMPLE_INTERVAL else
            complex_short_timed_sample_flush_op_e           when oclk_opcode = FLUSH           else
            complex_short_timed_sample_discontinuity_op_e   when oclk_opcode = DISCONTINUITY   else
            complex_short_timed_sample_metadata_op_e        when oclk_opcode = METADATA        else
            complex_short_timed_sample_sample_op_e;
    end generate using_marshallers;

    using_parser : if use_marsh = '0' generate
        -- Convert input opcode to something the parser can handle
        iclk_opcode_num <=
            0 when in_in.opcode = complex_short_timed_sample_sample_op_e          else
            1 when in_in.opcode = complex_short_timed_sample_time_op_e            else
            2 when in_in.opcode = complex_short_timed_sample_sample_interval_op_e else
            3 when in_in.opcode = complex_short_timed_sample_flush_op_e           else
            4 when in_in.opcode = complex_short_timed_sample_discontinuity_op_e   else
            5 when in_in.opcode = complex_short_timed_sample_metadata_op_e        else
            0;

        output_sample_data <= input_sample_data;
        output_sample_vld  <= input_sample_vld;
        output_sample_eom  <= input_sample_eom;
        output_eof         <= input_eof;

        input_rdy <= output_rdy;

        in_out_parser : sdr_assets_protocol_util.protocol_util.nonsample_passthrough_marshaller
        generic map(
            INPUT_DATA_WIDTH  => in_in.data'length,
            OUTPUT_DATA_WIDTH => out_out.data'length,
            WSI_MBYTEEN_WIDTH => out_out.byte_enable'length)
        port map(
            clk             => ctl_in.clk,
            rst             => ctl_in.reset,
            -- INPUT
            in_data         => in_in.data,
            in_valid        => in_in.valid,
            in_ready        => in_in.ready,
            in_som          => in_in.som,
            in_eom          => in_in.eom,
            in_opcode       => iclk_opcode_num,
            in_eof          => in_in.eof,
            in_take         => in_out.take,
            -- OUTPUT TO WORKER FROM INPUT SIDE
            samp_reset      => reset,
            samp_data_in    => input_sample_data,
            samp_in_vld     => input_sample_vld,
            samp_som_in     => open,
            samp_eom_in     => input_sample_eom,
            oeof            => input_eof,
            take            => input_rdy,
            -- INPUT FROM WORKER TO OUTPUT SIDE
            samp_data_out   => output_sample_data,
            samp_out_vld    => output_sample_vld,
            samp_eom_out    => output_sample_eom,
            ieof            => output_eof,
            enable          => output_rdy,
            -- OUTPUT
            out_data        => oclk_data,
            out_valid       => out_out.valid,
            out_byte_en     => out_out.byte_enable,
            out_give        => out_out.give,
            out_eom         => out_out.eom,
            out_opcode      => oclk_opcode_num,
            out_eof         => out_out.eof,
            out_ready       => out_in.ready,
            -- DEBUG
            passthrough     => props_in.bypass -- Bypass worker
            );

        -- this only needed to avoid build bug for xsim:
        -- ERROR: [XSIM 43-3316] Signal SIGSEGV received.
        out_out.data <= oclk_data;

        -- Set output opcode
        out_out.opcode <=
            complex_short_timed_sample_sample_op_e          when oclk_opcode_num = 0 else
            complex_short_timed_sample_time_op_e            when oclk_opcode_num = 1 else
            complex_short_timed_sample_sample_interval_op_e when oclk_opcode_num = 2 else
            complex_short_timed_sample_flush_op_e           when oclk_opcode_num = 3 else
            complex_short_timed_sample_discontinuity_op_e   when oclk_opcode_num = 4 else
            complex_short_timed_sample_metadata_op_e        when oclk_opcode_num = 5 else
            complex_short_timed_sample_sample_op_e;
    end generate using_parser;

end rtl;