.. portterm_csts documentation


.. _portterm_csts:


Port Termination (``portterm_csts``)
====================================
Source or sink termination for an external port of a OpenCPI HDL assembly.

Design
------
This component can be used to terminate unused external ports of a HDL assembly:


* For a source termination, the output data is set to 0xFACEBEEF.
* For a sink termination, the input data is stored in inBuffer property.  


Interface
---------
.. literalinclude:: ../specs/portterm_csts-spec.xml
   :language: xml
   :lines: 1, 4-



Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Ports
~~~~~
.. ocpi_documentation_ports::


Implementations
---------------

.. ocpi_documentation_implementations:: ../portterm_csts.hdl 



Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * NONE

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``portterm_csts`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
