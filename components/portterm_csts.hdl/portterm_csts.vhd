-- THIS FILE WAS ORIGINALLY GENERATED ON Tue Apr 26 16:55:10 2022 EDT
-- BASED ON THE FILE: portTerm_cswm.xml

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all;

architecture rtl of worker is

	signal regBuf : std_logic_vector(31 downto 0);

begin

	SRC_TERM: if (SRC_TERM_p = '1') generate
    begin

    out_out.data <= x"FACEBEEF";
	out_out.valid <= out_in.ready;

    end generate;


    SINK_TERM : if (SRC_TERM_p = '0') generate
    begin
    	process (in_in.clk) is
		begin
		  if (rising_edge(in_in.clk)) then
		  	if (in_in.valid = '1') then
		  		regBuf <= in_in.data;
		  	end if;
		  end if;
		end process;
	
		props_out.inBuffer <= to_ulong(regBuf);
		in_out.take <= in_in.valid;
	end generate;

end rtl;
