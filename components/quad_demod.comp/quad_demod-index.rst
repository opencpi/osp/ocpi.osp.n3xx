.. quad_demod documentation

.. _quad_demod:


Quadrature Demodulator (``quad_demod``)
=======================================
Calculates the angle between the complex conjugate of the previous input and the current input.
The output is proportional to the input signal's relative frequency..

Design
------
**This component will be deprecated within this OSP in future releases.  It is the equivalent of** ``quadrature_demod`` **component within OpenCPI SDR Assets Library** (``ocpi.sdr.assets``) **.**


Ports
~~~~~
.. ocpi_documentation_ports::


Implementations
---------------
.. ocpi_documentation_implementations:: ../quad_demod.hdl


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ``protocol_util`` primitives in sdr.assets

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``quad_demod`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
