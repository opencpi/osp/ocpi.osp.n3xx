-- THIS FILE WAS ORIGINALLY GENERATED ON Fri Oct 14 14:47:49 2022 EDT
-- BASED ON THE FILE: quad_demod.xml
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
library util_prims;
use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library sdr_assets_timed_sample_prot;
use sdr_assets_timed_sample_prot.timed_sample.all;

architecture rtl of worker is

    -- Normalize Property Parameters
    constant c_data_ext   : positive := to_integer(DATA_EXT);
    constant c_stages     : positive := to_integer(STAGES);
    constant single_width : integer := 16;
    constant double_width : integer := 32;
    --
    -- Breakdown of CORDIC primitive(s) delays:
    --  cordic_rp.vhd : 5
    --  cordic_rp.vhd/cordic.vhd/cordic_stage.vhd : c_stages (build property parameter)
    --  cordic_rp.vhd/round_conv.vhd : 1
    constant c_primitive_delays : positive := c_stages + 7;

    signal p_samp_r : signed(single_width-1 downto 0) := (others => '0');
    signal p_samp_i : signed(single_width-1 downto 0) := (others => '0');

    signal mult_out_re : std_logic_vector(single_width downto 0) := (others => '0');
    signal mult_out_im : std_logic_vector(single_width downto 0) := (others => '0');

    signal s_transient_cnt   : unsigned(integer(ceil(log2(real(c_primitive_delays))))-1 downto 0) := (others => '0');
    signal s_transient_done  : std_logic := '0';
    signal s1_transient_done : std_logic := '0';

    signal vld_input, do_work   : std_logic := '0';
    signal vld_s, eof_s         : std_logic := '0';
    signal cordic_out           : std_logic_vector(single_width downto 0) := (others => '0');

    signal b_r : std_logic_vector(single_width downto 0) := (others => '0');
    signal b_i : std_logic_vector(single_width downto 0) := (others => '0');

    ---------------------------------------
    -- Demarshaller and Marshaller Signals
    ---------------------------------------

    signal i_demarshaller_oprotocol : protocol_t := PROTOCOL_ZERO;
    signal o_marshaller_oprotocol   : protocol_t := PROTOCOL_ZERO;

    signal iclk_opcode          : sdr_assets_timed_sample_prot.timed_sample.opcode_t := SAMPLE;
    signal oclk_opcode          : sdr_assets_timed_sample_prot.timed_sample.opcode_t := SAMPLE;

    signal samps_op             : std_logic := '0';
    signal i_demarshaller_oeof  : std_logic := '0';
    signal i_demarshaller_irdy  : std_logic := '0';
    signal oclk_out_irdy        : std_logic := '0';
    signal oclk_data            : std_logic_vector(single_width-1 downto 0) := (others => '0');
    signal marsh_sample         : std_logic_vector(single_width-1 downto 0) := (others => '0');
    signal demarsh_sample       : std_logic_vector(double_width-1 downto 0) := (others => '0');

begin

    -- Give extra width for rounding in the complex multiplier
    b_r <= std_logic_vector(resize(p_samp_r, single_width+1));
    b_i <= std_logic_vector(resize(p_samp_i, single_width+1));

    -----------------------------------------------------------------------------
    -- Complex Multiplier : Full width is DATA_WIDTH + 1
    -- Multiply Complex Conjugate
    -----------------------------------------------------------------------------
    complex_multiplier : util_prims.util_prims.complex_mult
    generic map (
        A_WIDTH  => single_width,
        B_WIDTH  => single_width+1,
        PROD_WIDTH => single_width+1)
    port map (
        CLK      => ctl_in.clk,
        RST      => ctl_in.reset,
        DIN_A_RE => demarsh_sample(single_width-1 downto 0),
        DIN_A_IM => demarsh_sample(double_width-1 downto single_width),
        DIN_B_RE => b_r,
        DIN_B_IM => b_i,
        DIN_VLD  => do_work,
        DOUT_RE  => mult_out_re,
        DOUT_IM  => mult_out_im,
        DOUT_VLD => open);

    -----------------------------------------------------------------------------
    -- Rectangular-to-Polar CORDIC
    -----------------------------------------------------------------------------
        cord : dsp_prims.dsp_prims.cordic_rp
        generic map (
            DATA_WIDTH => single_width+1,
            DATA_EXT   => c_data_ext,
            STAGES     => c_stages)
        port map (
            CLK     => ctl_in.clk,
            RST     => ctl_in.reset,
            I       => mult_out_re,
            Q       => mult_out_im,
            VLD_IN  => do_work,
            MAG     => open,
            PHASE   => cordic_out,
            VLD_OUT => open);

    -----------------------------------------------------------------------------
    -- Flag to indicate when Start-Up Transient data has been pushed output of
    -- the CORIDC primitive and that the first 'valid' data is available on the
    -- output of the CORIDC primitive.
    -----------------------------------------------------------------------------
    cordic_startup : process (ctl_in.clk)
    begin
        if rising_edge(ctl_in.clk) then
            if (ctl_in.reset = '1') then
                s_transient_cnt   <= (others => '0');
                s_transient_done  <= '0';
                s1_transient_done <= '0';
            elsif (do_work = '1') then
                s1_transient_done <= s_transient_done;
                if (s_transient_cnt = c_primitive_delays-1) then
                    s_transient_done <= '1';
                else
                    s_transient_cnt <= s_transient_cnt + 1;
                end if;
            end if;
        end if;
    end process cordic_startup;

    -----------------------------------------------------------------------------
    -- Take the input data and supply it to the mcc. Check control signal for 
    -- reset and check for eof signal.
    -----------------------------------------------------------------------------
    demod : process(ctl_in.clk)
        variable eof_clk_cnt  : integer := 0;
    begin
        if rising_edge(ctl_in.clk) then
            if (ctl_in.reset = '1') then
                p_samp_r <= (others => '0');
                p_samp_i <= (others => '0');
                eof_clk_cnt := 0;
            elsif i_demarshaller_oeof = '1' and out_in.ready = '1' then
                eof_clk_cnt := eof_clk_cnt + 1;
                if eof_clk_cnt = c_primitive_delays+1 then
                    eof_s <= '1';
                end if;
            elsif vld_input = '1' then
                p_samp_r <= signed(demarsh_sample(single_width-1 downto 0));
                p_samp_i <= -signed(demarsh_sample(double_width-1 downto single_width));
            end if;
        end if;
    end process demod;

    -- Convert input opcode to something the demarshaller can handle
    iclk_opcode <=
        SAMPLE          when in_in.opcode = complex_short_timed_sample_sample_op_e          else
        TIME_TIME       when in_in.opcode = complex_short_timed_sample_time_op_e            else
        SAMPLE_INTERVAL when in_in.opcode = complex_short_timed_sample_sample_interval_op_e else
        FLUSH           when in_in.opcode = complex_short_timed_sample_flush_op_e           else
        DISCONTINUITY   when in_in.opcode = complex_short_timed_sample_discontinuity_op_e   else
        METADATA        when in_in.opcode = complex_short_timed_sample_metadata_op_e        else
        SAMPLE;

    in_demarshaller : timed_sample_demarshaller
    generic map(
        WSI_DATA_WIDTH => double_width)
    port map(
        clk       => ctl_in.clk,
        rst       => ctl_in.reset,
        -- INPUT
        idata     => in_in.data,
        ivalid    => in_in.valid,
        iready    => in_in.ready,
        isom      => in_in.som,
        ieom      => in_in.eom,
        iopcode   => iclk_opcode,
        ieof      => in_in.eof,
        itake     => in_out.take,
        -- OUTPUT
        samp_data => demarsh_sample,
        oprotocol => i_demarshaller_oprotocol,
        oeof      => i_demarshaller_oeof,
        ordy      => i_demarshaller_irdy);

    -- Tell the demarshaller it can operate
    i_demarshaller_irdy <= oclk_out_irdy;

    -- Valid input signal for the samples operations
    vld_input <= i_demarshaller_oprotocol.sample_vld and oclk_out_irdy;

    -- Work signal for sample operations
    do_work <= vld_input or (i_demarshaller_oeof and oclk_out_irdy);

    -- Valid output for sample operations
    vld_s <= '1' when (do_work = '1' and s1_transient_done = '1' and eof_s = '0') else '0';

    marsh_sample <= std_logic_vector(shift_right(signed(cordic_out), 1)(single_width-1 downto 0));

    -- Sets the samples valid signal
    o_marshaller_oprotocol.sample_vld <= vld_s;

    -- All other opcodes just need the demarshaller and marshaller to be ready
    o_marshaller_oprotocol.time                <= i_demarshaller_oprotocol.time;
    o_marshaller_oprotocol.time_vld            <= i_demarshaller_oprotocol.time_vld            and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.sample_interval     <= i_demarshaller_oprotocol.sample_interval;
    o_marshaller_oprotocol.sample_interval_vld <= i_demarshaller_oprotocol.sample_interval_vld and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.flush               <= i_demarshaller_oprotocol.flush               and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.discontinuity       <= i_demarshaller_oprotocol.discontinuity       and oclk_out_irdy and i_demarshaller_irdy;
    o_marshaller_oprotocol.metadata            <= i_demarshaller_oprotocol.metadata;
    o_marshaller_oprotocol.metadata_vld        <= i_demarshaller_oprotocol.metadata_vld        and oclk_out_irdy and i_demarshaller_irdy;

    out_marshaller : timed_sample_marshaller
    generic map(
        WSI_DATA_WIDTH    => single_width,
        WSI_MBYTEEN_WIDTH => out_out.byte_enable'length)
    port map(
        clk          => ctl_in.clk,
        rst          => ctl_in.reset,
        -- INPUT
        samp_data    => marsh_sample,
        iprotocol    => o_marshaller_oprotocol,
        ieof         => eof_s,
        irdy         => oclk_out_irdy,
        -- OUTPUT
        odata        => oclk_data,
        ovalid       => out_out.valid,
        obyte_enable => out_out.byte_enable,
        ogive        => out_out.give,
        osom         => out_out.som,
        oeom         => out_out.eom,
        oeof         => out_out.eof,
        oopcode      => oclk_opcode,
        oready       => out_in.ready);

    -- this only needed to avoid build bug for xsim:
    -- ERROR: [XSIM 43-3316] Signal SIGSEGV received.
    out_out.data <= oclk_data;

    -- Set output opcode
    out_out.opcode <=
        short_timed_sample_sample_op_e          when oclk_opcode = SAMPLE          else
        short_timed_sample_time_op_e            when oclk_opcode = TIME_TIME       else
        short_timed_sample_sample_interval_op_e when oclk_opcode = SAMPLE_INTERVAL else
        short_timed_sample_flush_op_e           when oclk_opcode = FLUSH           else
        short_timed_sample_discontinuity_op_e   when oclk_opcode = DISCONTINUITY   else
        short_timed_sample_metadata_op_e        when oclk_opcode = METADATA        else
        short_timed_sample_sample_op_e;

end rtl;