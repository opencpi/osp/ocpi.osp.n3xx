.. slicer documentation

.. _slicer:

Binary Slicer (``slicer``)
==========================
Slices data into 1 or 0 depending on if the input is greater than 0.

Design
------
**This component will be deprecated within this OSP in future releases.  It is the equivalent of** ``binary_slicer`` **component within OpenCPI SDR Assets Library** (``ocpi.sdr.assets``) **.**


Ports
~~~~~
.. ocpi_documentation_ports::


Implementations
---------------
.. ocpi_documentation_implementations:: ../slicer.hdl


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * NONE

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``slicer`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
