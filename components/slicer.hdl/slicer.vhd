-- THIS FILE WAS ORIGINALLY GENERATED ON Mon Oct 17 07:52:50 2022 EDT
-- BASED ON THE FILE: slicer.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions

architecture rtl of worker is
  --Worker Signals
  signal do_work    : std_logic;

  --Marshaller Signals
  signal reset              : std_logic;
  signal input_sample_data  : std_logic_vector(in_in.data'length-1 downto 0); -- Input Samples Data
  signal input_sample_vld   : std_logic; -- Valid for input samples
  signal in_som             : std_logic;
  signal input_sample_eom   : std_logic; -- Eom on sample data
  signal input_eof          : std_logic;
  signal input_rdy          : std_logic; -- Tell the input that the worker is ready for data

  signal output_sample_data : std_logic_vector(out_out.data'length-1 downto 0); -- Output Samples Data
  signal output_sample_vld  : std_logic; -- Valid for output samples
  signal output_sample_eom  : std_logic; -- Samples End of Message
  signal output_eof         : std_logic;
  signal output_rdy         : std_logic; -- The output is ready for data

  signal oclk_data          : std_logic_vector(out_out.data'length-1 downto 0);

  signal iclk_opcode_num    : natural;
  signal oclk_opcode_num    : natural;

begin
    ----------------------------------------
    -- OPCODE MARSHALLER
    ----------------------------------------
    -- InputOpcode Conversion
    -- Convert input opcode to something the parser can handle
    iclk_opcode_num <=
        0 when in_in.opcode = short_timed_sample_sample_op_e          else
        1 when in_in.opcode = short_timed_sample_time_op_e            else
        2 when in_in.opcode = short_timed_sample_sample_interval_op_e else
        3 when in_in.opcode = short_timed_sample_flush_op_e           else
        4 when in_in.opcode = short_timed_sample_discontinuity_op_e   else
        5 when in_in.opcode = short_timed_sample_metadata_op_e        else
        0;

        passthrough_marshaller : sdr_assets_protocol_util.protocol_util.nonsample_passthrough_marshaller
        generic map(
            INPUT_DATA_WIDTH  => in_in.data'length,
            OUTPUT_DATA_WIDTH => out_out.data'length,
            WSI_MBYTEEN_WIDTH => out_out.byte_enable'length)
        port map(
            clk             => ctl_in.clk,
            rst             => ctl_in.reset,
            -- INPUT
            in_data         => in_in.data,
            in_valid        => in_in.valid,
            in_ready        => in_in.ready,
            in_som          => in_in.som,
            in_eom          => in_in.eom,
            in_opcode       => iclk_opcode_num,
            in_eof          => in_in.eof,
            in_take         => in_out.take,
            -- OUTPUT TO WORKER FROM INPUT SIDE
            samp_reset      => reset,
            samp_data_in    => input_sample_data,
            samp_in_vld     => input_sample_vld,
            samp_som_in     => in_som,
            samp_eom_in     => input_sample_eom,
            oeof            => input_eof,
            take            => input_rdy,
            -- INPUT FROM WORKER TO OUTPUT SIDE
            samp_data_out   => output_sample_data,
            samp_out_vld    => output_sample_vld,
            samp_eom_out    => output_sample_eom,
            ieof            => output_eof,
            enable          => output_rdy,
            -- OUTPUT
            out_data        => oclk_data,
            out_valid       => out_out.valid,
            out_byte_en     => out_out.byte_enable,
            out_give        => out_out.give,
            out_eom         => out_out.eom,
            out_opcode      => oclk_opcode_num,
            out_eof         => out_out.eof,
            out_ready       => out_in.ready);

    -- this only needed to avoid build bug for xsim:
    -- ERROR: [XSIM 43-3316] Signal SIGSEGV received.
    out_out.data <= oclk_data;

    -- OutputOpcode Conversion
    out_out.opcode <=
        uchar_timed_sample_sample_op_e          when oclk_opcode_num = 0 else
        uchar_timed_sample_time_op_e            when oclk_opcode_num = 1 else
        uchar_timed_sample_sample_interval_op_e when oclk_opcode_num = 2 else
        uchar_timed_sample_flush_op_e           when oclk_opcode_num = 3 else
        uchar_timed_sample_discontinuity_op_e   when oclk_opcode_num = 4 else
        uchar_timed_sample_metadata_op_e        when oclk_opcode_num = 5 else
        uchar_timed_sample_sample_op_e;

    ----------------------------------------
    --CONTROL SIGNALS
    ----------------------------------------
    --Enables
    do_work            <= output_rdy and input_sample_vld;
    input_rdy          <= do_work;
  
    --Messages
    output_sample_vld  <= do_work;
    output_sample_data <= ( 0 => not in_in.data(15), others => '0');

    --Message Boundaries
    output_sample_eom  <= input_sample_eom; 
    output_eof         <= input_eof;

end rtl;