.. ad9371_gpio documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _ad9371_gpio:


AD9371 GPIO Interface (``ad9371_gpio``)
=======================================
GPIO interface to the Analog Device AD9371 Tranceiver.

Design
------

Interface
---------
.. literalinclude:: ../specs/ad9371_gpio-spec.xml
   :language: xml


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Implementations
---------------
.. ocpi_documentation_implementations:: ../ad9371_gpio.hdl


Dependencies
------------
There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``ad9371_gpio`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
