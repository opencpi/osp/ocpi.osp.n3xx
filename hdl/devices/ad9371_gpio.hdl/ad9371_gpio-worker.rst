.. ad9371_gpio HDL worker


.. _ad9371_gpio-HDL-worker:


``ad9371_gpio`` HDL Worker
==========================

Detail
------
HDL implementation of ``ad9371_gpio`` device worker in support of the Analog Devices AD9371 Transceiver for the ``n310`` platform. This device worker allows for the debugging of the hardware chip. Based on the ``n310`` board layout, this implementation supports **READ ONLY** operation of GPIO Pins: 0, 1, 3, 4, 12, 13, 14, 15.

.. ocpi_documentation_worker::

   regValue: Readback Register Value


Signal Ports
~~~~~~~~~~~~
.. literalinclude:: ./ad9371_gpio.xml
   :language: xml
   :lines: 4-22

.. Utilization
   -----------
   .. ocpi_documentation_utilization::
