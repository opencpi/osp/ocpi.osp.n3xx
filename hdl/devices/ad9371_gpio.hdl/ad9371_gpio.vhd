-- THIS FILE WAS ORIGINALLY GENERATED ON Tue Mar 22 12:41:59 2022 EDT
-- BASED ON THE FILE: ad9371_gpio.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions

architecture rtl of worker is

  signal readReg : std_logic_vector(7 downto 0);

begin
  
  process (ctl_in.clk)
  begin
    if (rising_edge(ctl_in.clk)) then
      case (props_in.DB_Select) is
        when '0' =>
          readReg(0) <= DBA_MK_GPIO_0;
          readReg(1) <= DBA_MK_GPIO_1;
          readReg(2) <= DBA_MK_GPIO_3;
          readReg(3) <= DBA_MK_GPIO_4;
          readReg(4) <= DBA_MK_GPIO_12;
          readReg(5) <= DBA_MK_GPIO_13;
          readReg(6) <= DBA_MK_GPIO_14;
          readReg(7) <= DBA_MK_GPIO_15;
        when '1' =>
          readReg(0) <= DBB_MK_GPIO_0;
          readReg(1) <= DBB_MK_GPIO_1;
          readReg(2) <= DBB_MK_GPIO_3;
          readReg(3) <= DBB_MK_GPIO_4;
          readReg(4) <= DBB_MK_GPIO_12;
          readReg(5) <= DBB_MK_GPIO_13;
          readReg(6) <= DBB_MK_GPIO_14;
          readReg(7) <= DBB_MK_GPIO_15;
      end case;
    end if;
  end process;

  props_out.regValue <= to_uchar(readReg);
end rtl;