.. ad9371_spi documentation

.. _ad9371_spi:


AD9371 SPI Interface (``ad9371_spi``)
=====================================
SPI communication interface for the Analog Devices AD9371 Transceiver.  This device worker provides service to the Transceiver API Controller, which performs register map programming based on AD9371 channel profiles via Analog Devices Mykonos Library.  

Design
------

Interface
---------
.. literalinclude:: ../specs/ad9371_spi-spec.xml
   :language: xml
   :lines: 2-8,11


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::



Implementations
---------------
.. ocpi_documentation_implementations:: ../ad9371_spi.hdl


Dependencies
------------

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``ad9371_spi`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
