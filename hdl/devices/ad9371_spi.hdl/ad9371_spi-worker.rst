.. ad9371_spi HDL worker


.. _ad9371_spi-HDL-worker:


``ad9371_spi`` HDL Worker
=========================


Detail
------
HDL implementation for ``ad9371_spi`` device worker in support of Analog Devices AD9371 Transceiver chip.

.. ocpi_documentation_worker::

   VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)

Signal Ports
~~~~~~~~~~~~
.. literalinclude:: ./ad9371_spi.xml
   :language: xml
   :lines: 4-14

.. Utilization
   -----------
   .. ocpi_documentation_utilization::
