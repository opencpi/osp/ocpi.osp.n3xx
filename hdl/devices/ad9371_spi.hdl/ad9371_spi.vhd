-- THIS FILE WAS ORIGINALLY GENERATED ON Mon Apr  5 09:54:30 2021 EDT
-- BASED ON THE FILE: ad9371_spi.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; 
library work; use work.adrv9371_spi_ila_pkg.all;
architecture rtl of worker is

  type   state_t is (idle_e, starting_e, busy_e, flush_e, done_e);
  signal state_r      : state_t := idle_e;

  -- Internal signals
  signal fdata          : ulong_t := (others => '0');
  signal rdata          : std_logic_vector(7 downto 0) := (others => '0');
  signal preamble       : integer := 1;
  signal addr_width     : integer := 15;
  signal data_width     : integer := 8;
  signal frame_length   : integer := 24;
  signal clock_divisor  : integer := 0;  

  
  --Component Spec Property Signals
  signal MK_DB_SEL     : bool_t   := bfalse;
  signal MK_RENABLE    : bool_t;
  signal MK_WENABLE    : bool_t;
    
  --Primitive
  signal clk                 : std_logic;
  signal reset               : std_logic := '0';
  signal done           : bool_t := bfalse;


  -- The SPI signals - a 4 wire interface
  signal sdi                 : std_logic := '0';
  signal sclk                : std_logic := '0';
  signal sen                 : std_logic := '0';
  signal sdio                : std_logic := '0';

  signal bit_count           : integer := 0;
  signal clk_count           : integer := 0; 

  signal dba_mk_sclk         : std_logic_vector(0 downto 0);
  signal dba_mk_sdio         : std_logic_vector(0 downto 0);
  signal dba_mk_sdi          : std_logic_vector(0 downto 0);
  signal dba_mk_csb          : std_logic_vector(0 downto 0);
  signal dbb_mk_sclk         : std_logic_vector(0 downto 0);
  signal dbb_mk_sdio         : std_logic_vector(0 downto 0);
  signal dbb_mk_sdi          : std_logic_vector(0 downto 0);
  signal dbb_mk_csb          : std_logic_vector(0 downto 0);
  
begin

  clk                   <= ctl_in.clk;
  reset                 <= ctl_in.reset;
    
  fdata                 <= props_in.MK_FRAME; 
  props_out.MK_DONE     <= done;
  props_out.MK_RDATA    <= unsigned(rdata); 
  MK_DB_SEL             <= props_in.MK_DB_SEL;
  MK_RENABLE            <= props_in.MK_RENABLE and ctl_in.is_operating;
  MK_WENABLE            <= props_in.MK_WENABLE and ctl_in.is_operating; 
  done                  <= to_bool(state_r = done_e);
  

  clock_divisor <= 100;

  --Possible Future Clock Divisor
  --clock_divisor <= to_integer(to_unsigned((from_float(CLK_FREQ_p)*2)/9000000,10)) when caddr = "000" else -- 9 MHz
  --                 to_integer(to_unsigned((from_float(CLK_FREQ_p)*2)/20000000,10)) when caddr = "010" else  -- 20 MHz
  --                 to_integer(to_unsigned(from_float(CLK_FREQ_p)/from_float(SPI_CLK_FREQ_p),16));

  
  sen   <= '0' when ((state_r = starting_e) or (state_r = busy_e) or (state_r = flush_e)) else '1'; -- enable asserted low

  
  sdio  <=  fdata(frame_length - bit_count - 1) when (MK_WENABLE and (state_r = busy_e)) else --Writing
            fdata(frame_length - bit_count - 1) when (MK_RENABLE and (bit_count < (preamble + addr_width)) and (state_r = busy_e)) else --Reading
            '0'; 


  spiRead: process(sclk) is
  begin
    if (rising_edge(sclk)) then
      if (MK_RENABLE = '1' and (bit_count > (preamble + addr_width - 1))) then
            rdata(data_width - (bit_count - (preamble + addr_width - 1))) <= sdi;
      end if;
    end if;
  end process;


  spiSclk: process(clk) is 
  begin
      if (rising_edge(clk)) then
        if (reset = '1') then
          sclk <= '0';
        else
          case (state_r) is
            when idle_e =>
              if (MK_RENABLE or MK_WENABLE) then
                state_r <= starting_e;
              else
                state_r <= idle_e;
              end if;
            when starting_e =>
              if (clk_count = clock_divisor/2 -1) then
                state_r     <= busy_e;
                bit_count   <= 0;
                clk_count   <= 0;
              else
                state_r <= starting_e;
                clk_count <= clk_count + 1;
              end if; 
            when busy_e =>
              if (clk_count = clock_divisor - 1) then -- end of bit cycle
                clk_count <= 0;
                if (bit_count = frame_length - 1) then -- end of access
                    sclk <= '0';
                    state_r <= flush_e;
                else
                  sclk <=  not sclk;
                  bit_count <= bit_count + 1;
                end if;
              else
                clk_count <= clk_count + 1;
                if (clk_count = clock_divisor/2 - 1) then -- mid-cycle, read data is captured
                  sclk <= not sclk;
                else
                  sclk <= sclk;
                end if;
              end if;
            when flush_e =>
              if (clk_count = clock_divisor/2 - 1) then
                state_r <= flush_e;
                clk_count <= clk_count + 1;
              elsif (clk_count = clock_divisor - 1) then
                state_r <= done_e;
                clk_count <= 0;
              else
                state_r <= flush_e;
                clk_count <= clk_count + 1;
              end if;
            when done_e =>
              sclk <= '0';
              if (MK_RENABLE or MK_WENABLE) then
                state_r <= done_e;
              else
                state_r <= idle_e;
              end if;
          end case;
        end if;
      end if;
  end process;


  spiInterface: process(clk) is 
  begin
    if (rising_edge(clk)) then
           
      if (MK_DB_SEL = '0') then
  
        --Daughter Board A Signals
        dba_mk_sclk(0) <= sclk;  
        dba_mk_sdio(0) <= sdio;  
        dba_mk_sdi(0) <= DBA_MK_SPI_SDI;  
        dba_mk_csb(0)  <= sen;  
  
        sdi <= DBA_MK_SPI_SDI;
  
        dbb_mk_sclk(0) <= '0';  
        dbb_mk_sdio(0) <= '0';  
        dbb_mk_sdi(0) <= '0';  
        dbb_mk_csb(0)  <='1';  
  
      else
  
        --Daughter Board B Signals
        dbb_mk_sclk(0) <= sclk;  
        dbb_mk_sdio(0) <= sdio;  
        dbb_mk_sdi(0) <= DBB_MK_SPI_SDI;  
        dbb_mk_csb(0)  <= sen;  
  
        sdi <= DBB_MK_SPI_SDI;
  
        dba_mk_sclk(0) <= '0';  
        dba_mk_sdio(0) <= '0'; 
        dba_mk_sdi(0) <= '0';  
        dba_mk_csb(0)  <='1';  
  
      end if;
    end if;
  end process;


  --Port Definitions
  DBA_MK_SPI_SCLK <= dba_mk_sclk(0);
  DBA_MK_SPI_SDIO <= dba_mk_sdio(0);
  DBA_MK_SPI_CSB <= dba_mk_csb(0);
  DBB_MK_SPI_SCLK <= dbb_mk_sclk(0);
  DBB_MK_SPI_SDIO <= dbb_mk_sdio(0);
  DBB_MK_SPI_CSB <= dbb_mk_csb(0);


  -- Vivadio ILA Intergation
  DEBUG: if its(VIVADO_ILA_p) generate  
  begin
    
    ad9371_spi_ila: ila_0
      port map (
        clk => ctl_in.clk,
        probe0 => dba_mk_sclk,
        probe1 => dba_mk_sdio,
        probe2 => dba_mk_sdi,
        probe3 => dba_mk_csb,
        probe4 => from_bool(MK_WENABLE),
        probe5 => from_bool(MK_RENABLE),
        probe6 => from_bool(done)  );  
  
  end generate;
end rtl;
