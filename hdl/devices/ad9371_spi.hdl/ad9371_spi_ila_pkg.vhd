library IEEE; use IEEE.std_logic_1164.all;

package adrv9371_spi_ila_pkg is

  component ila_0
    port (
      clk      : in  std_logic;
      probe0   : in  std_logic_vector(0 downto 0);
      probe1   : in  std_logic_vector(0 downto 0);
      probe2   : in  std_logic_vector(0 downto 0);
      probe3   : in  std_logic_vector(0 downto 0);
      probe4   : in  std_logic_vector(0 downto 0);
      probe5   : in  std_logic_vector(0 downto 0);
      probe6   : in  std_logic_vector(0 downto 0) );
  end component;


end package adrv9371_spi_ila_pkg;