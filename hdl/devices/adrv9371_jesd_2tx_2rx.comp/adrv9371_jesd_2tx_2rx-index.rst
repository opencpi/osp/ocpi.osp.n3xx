.. adrv9371_jesd_2tx_2rx documentation

.. _adrv9371_jesd_2tx_2rx:


JESD204B Interface (``adrv9371_jesd_2tx_2rx``)
==============================================
JESD204B Interface based on `Analog Devices (ADI) JESD204 Framework <https://wiki.analog.com/resources/fpga/peripherals/jesd204>`_.


Implementations
---------------
.. ocpi_documentation_implementations:: ../adrv9371_jesd_2tx_2rx.hdl


Dependencies
------------
There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``adrv9371_jesd_2tx_2rx`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
