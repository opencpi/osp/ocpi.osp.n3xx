.. adrv9371_jesd_2tx_2rx HDL worker


.. _adrv9371_jesd_2tx_2rx-HDL-worker:


``adrv9371_jesd_2tx_2rx`` HDL Worker
====================================

Detail
------

HDL implementation of a Xilinx block diagram for ``adrv9371_jesd_2tx_2rx`` device worker, which supports 2 Tx and 2 Rx channels per daughterboard. The block diagram consists of several Analog Devices (ADI) IP cores for the JESD204 Physical Layer, JESD204 Link Layer, and JESD Transport Layer. 

* JESD Physical Layer:  Responsible for interfacing and configuring the Xilinx Gigabit Transceivers
   * `ADI AXI_ADXCVR <https://wiki.analog.com/resources/fpga/docs/axi_adxcvr>`_
   * `ADI UTIL_ADXCVR <https://wiki.analog.com/resources/fpga/docs/util_xcvr>`_
* JESD Link Layer: Responsible for protocol handling, including scrambling/descrambling, lane alignment, character replacement and alignment monitoring.
   * `ADI JESD204B/C Link Transmit Peripheral <https://wiki.analog.com/resources/fpga/peripherals/jesd204/axi_jesd204_tx>`_
   * `ADI JESD204B/C Link Receive Peripheral <https://wiki.analog.com/resources/fpga/peripherals/jesd204/axi_jesd204_rx>`_
* JESD Transport Layer:  Responsible for converter specific data framing and de-framing. 
   * `ADI DAC JESD204B/C Transport Peripheral <https://wiki.analog.com/resources/fpga/peripherals/jesd204/jesd204_tpl_dac>`_
   * `ADI ADC JESD204B/C Transport Peripheral <https://wiki.analog.com/resources/fpga/peripherals/jesd204/jesd204_tpl_adc>`_

.. _adrv9371_jesd_2tx_2rx-diagram:

.. figure:: ../adrv9371_jesd_2tx_2rx.comp/adrv9371_jesd_2tx_2rx.svg
   :alt: ADI JESD204B Interface Block Diagram.
   :align: center
   :height: 1200px
   :width: 1200px

   ADI JESD204B Interface Block Diagram


.. ocpi_documentation_worker::

   LOOPBACK_p: Build-time parameter for Baseband Loopback Mode
   VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)

   dac: JESD 4-Lane qDAC Sub-Worker Interface
   adc: JESD 4-Lane qADC Sub-Worker Interface
   axi_tx_adxcvr: AXI-Lite Register Map Interface for JESD Tx Physical Layer
   axi_tx_jesd: AXI-Lite Register Map Interface for JESD Tx Link Layer
   axi_tx_tpl: AXI-Lite Register Map Interface for JESD Tx Transport Layer
   axi_tx_clkgen: AXI-Lite Register Map Interface for JESD Tx Clock Generator
   axi_rx_adxcvr: AXI-Lite Register Map Interface for JESD Rx Physical Layer
   axi_rx_jesd: AXI-Lite Register Map Interface for JESD Rx Link Layer
   axi_rx_tpl: AXI-Lite Register Map Interface for JESD Rx Transport Layer
   axi_rx_clkgen: AXI-Lite Register Map Interface for JESD Rx Clock Generator



Signal Ports
~~~~~~~~~~~~
.. literalinclude:: ./adrv9371_jesd_2tx_2rx.xml
   :language: xml
   :lines: 4-27

.. Utilization
   -----------
   .. ocpi_documentation_utilization::
