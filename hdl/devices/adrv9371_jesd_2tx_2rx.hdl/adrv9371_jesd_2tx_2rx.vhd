-- THIS FILE WAS ORIGINALLY GENERATED ON Wed May 19 13:18:21 2021 PDT
-- BASED ON THE FILE: adrv9371_jesd_2tx_2rx.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library UNISIM; use UNISIM.vcomponents.all;
library ocpi; use ocpi.types.all; 
library work; use work.adrv9371_jesd_2tx_2rx_bd_pkg.all;


architecture Structural of worker is

	signal axi_ap_in 	        : axilite_ap_m2s_array_t(0 to C_M_AXI_AP_COUNT-1);
	signal axi_ap_out	        : axilite_ap_s2m_array_t(0 to C_M_AXI_AP_COUNT-1);
	signal mgtclk	            : std_logic;
    signal devclk               : std_logic;
    signal devclk_in            : std_logic;
	signal sysref 		        : std_logic;
    signal tx_linkclk           : std_logic;
    signal rx_linkclk           : std_logic;
    signal txoutclk             : std_logic;
    signal rxoutclk             : std_logic;
    signal txSync               : std_logic_vector(0 downto 0);
    signal rxSync               : std_logic_vector(0 downto 0);      
        
    signal dac_data0            : std_logic_vector(31 downto 0);
    signal dac_data1            : std_logic_vector(31 downto 0);
    signal dac_data2            : std_logic_vector(31 downto 0);
    signal dac_data3            : std_logic_vector(31 downto 0);
    signal dac_valid0           : std_logic_vector(0 downto 0);
    signal dac_valid1           : std_logic_vector(0 downto 0);
    signal dac_valid2           : std_logic_vector(0 downto 0);
    signal dac_valid3           : std_logic_vector(0 downto 0);
    signal dac_enable0          : std_logic_vector(0 downto 0);
    signal dac_enable1          : std_logic_vector(0 downto 0);
    signal dac_enable2          : std_logic_vector(0 downto 0);
    signal dac_enable3          : std_logic_vector(0 downto 0);
    signal adc_data0            : std_logic_vector(31 downto 0);
    signal adc_data1            : std_logic_vector(31 downto 0);
    signal adc_data2            : std_logic_vector(31 downto 0);
    signal adc_data3            : std_logic_vector(31 downto 0);
    signal adc_valid0           : std_logic_vector(0 downto 0);
    signal adc_valid1           : std_logic_vector(0 downto 0);
    signal adc_valid2           : std_logic_vector(0 downto 0);
    signal adc_valid3           : std_logic_vector(0 downto 0);
    signal adc_enable0          : std_logic_vector(0 downto 0);
    signal adc_enable1          : std_logic_vector(0 downto 0);
    signal adc_enable2          : std_logic_vector(0 downto 0);
    signal adc_enable3          : std_logic_vector(0 downto 0);


begin

    mgtclk_ibuf: IBUFDS_GTE2
        port map (
            CEB => '0',
            I => mgtclk_p,
            IB => mgtclk_n,
            O => mgtclk );
    
    devclk_ibuf: IBUFDS
        port map (
            I => devclk_p,
            IB => devclk_n,
            O => devclk_in );

    devclk_bufg: BUFG
        port map (
            I => devclk_in,
            O => devclk );
    
    sysref_ibuf: IBUFDS
    	port map (
    		I => sysref_p,
    		IB => sysref_n,
    		O => sysref );
    
    txSync  <= tx_sync;
    rx_sync <= rxSync;

    dac_out.devclk <= devclk;
    adc_out.devclk <= devclk;
    

    NO_LOOPBACK_GEN: if (LOOPBACK_p = '0') generate
    begin
        dac_data0 <= dac_in.dac_data_0;
        dac_data1 <= dac_in.dac_data_1;
        dac_data2 <= dac_in.dac_data_2;
        dac_data3 <= dac_in.dac_data_3;
        dac_out.dac_valid_0 <= dac_valid0;
        dac_out.dac_valid_1 <= dac_valid1;
        dac_out.dac_valid_2 <= dac_valid2;
        dac_out.dac_valid_3 <= dac_valid3;
        dac_out.dac_enable_0 <= dac_enable0;
        dac_out.dac_enable_1 <= dac_enable1;
        dac_out.dac_enable_2 <= dac_enable2;
        dac_out.dac_enable_3 <= dac_enable3;
        adc_out.adc_data_0 <= adc_data0;
        adc_out.adc_data_1 <= adc_data1;
        adc_out.adc_data_2 <= adc_data2;
        adc_out.adc_data_3 <= adc_data3;
        adc_out.adc_valid_0 <= adc_valid0;
        adc_out.adc_valid_1 <= adc_valid1;
        adc_out.adc_valid_2 <= adc_valid2;
        adc_out.adc_valid_3 <= adc_valid3;
        adc_out.adc_enable_0 <= adc_enable0;
        adc_out.adc_enable_1 <= adc_enable1;
        adc_out.adc_enable_2 <= adc_enable2;
        adc_out.adc_enable_3 <= adc_enable3;
    end generate;
    

    LOOPBACK_GEN: if (LOOPBACK_P ='1') generate

    signal dac_in_loopback_dac0_vld : std_logic_vector(0 downto 0);
    signal dac_in_loopback_dac1_vld : std_logic_vector(0 downto 0);

    begin 
        dac_in_loopback_dac0_vld(0) <= dac_in.loopback_dac0_vld;
        dac_in_loopback_dac1_vld(0) <= dac_in.loopback_dac1_vld;

    LOOPBACK: jesd_loopback 
        port map( 
            clk                 => tx_linkclk,
            loopback            => to_boolean(LOOPBACK_p),
            loopback_dac0_vld   => dac_in_loopback_dac0_vld,
            loopback_dac1_vld   => dac_in_loopback_dac1_vld,
            dac0_in             => dac_in.dac_data_0,
            dac1_in             => dac_in.dac_data_1,
            dac2_in             => dac_in.dac_data_2,
            dac3_in             => dac_in.dac_data_3,
            dac0_out            => dac_data0,
            dac1_out            => dac_data1, 
            dac2_out            => dac_data2, 
            dac3_out            => dac_data3,
            dac_valid0_out      => dac_out.dac_valid_0,
            dac_valid1_out      => dac_out.dac_valid_1,
            dac_valid2_out      => dac_out.dac_valid_2,
            dac_valid3_out      => dac_out.dac_valid_3,
            dac_valid0_in       => dac_valid0,  
            dac_valid1_in       => dac_valid1,  
            dac_valid2_in       => dac_valid2,  
            dac_valid3_in       => dac_valid3,  
            dac_enable0_out     => dac_out.dac_enable_0,
            dac_enable1_out     => dac_out.dac_enable_1,
            dac_enable2_out     => dac_out.dac_enable_2,
            dac_enable3_out     => dac_out.dac_enable_3,
            dac_enable0_in      => dac_enable0,
            dac_enable1_in      => dac_enable1,
            dac_enable2_in      => dac_enable2,
            dac_enable3_in      => dac_enable3,
            adc0_out            => adc_out.adc_data_0,
            adc1_out            => adc_out.adc_data_1,
            adc2_out            => adc_out.adc_data_2,
            adc3_out            => adc_out.adc_data_3,
            adc0_in             => adc_data0,
            adc1_in             => adc_data1, 
            adc2_in             => adc_data2, 
            adc3_in             => adc_data3,
            adc_valid0_out      => adc_out.adc_valid_0,
            adc_valid1_out      => adc_out.adc_valid_1,
            adc_valid2_out      => adc_out.adc_valid_2,
            adc_valid3_out      => adc_out.adc_valid_3,
            adc_valid0_in       => adc_valid0,  
            adc_valid1_in       => adc_valid1,  
            adc_valid2_in       => adc_valid2,  
            adc_valid3_in       => adc_valid3,  
            adc_enable0_out     => adc_out.adc_enable_0,
            adc_enable1_out     => adc_out.adc_enable_1,
            adc_enable2_out     => adc_out.adc_enable_2,
            adc_enable3_out     => adc_out.adc_enable_3,
            adc_enable0_in      => adc_enable0,
            adc_enable1_in      => adc_enable1,
            adc_enable2_in      => adc_enable2,
            adc_enable3_in      => adc_enable3 );
    end generate;


    JESD: adrv9371_jesd_2tx_2rx_bd
      port map(
        tx_data_0_p => tx_data_0_p,
        tx_data_0_n => tx_data_0_n,
        tx_data_1_p => tx_data_1_p,
        tx_data_1_n => tx_data_1_n,
        tx_data_2_p => tx_data_2_p,
        tx_data_2_n => tx_data_2_n,
        tx_data_3_p => tx_data_3_p,
        tx_data_3_n => tx_data_3_n,
        tx_sync => tx_sync,
        tx_ref_clk_0 => mgtclk,
        tx_sysref_0 => sysref,
        rx_data_0_p => rx_data_0_p,
        rx_data_0_n => rx_data_0_n,
        rx_data_1_p => rx_data_1_p,
        rx_data_1_n => rx_data_1_n,
        rx_data_2_p => rx_data_2_p,
        rx_data_2_n => rx_data_2_n,
        rx_data_3_p => rx_data_3_p,
        rx_data_3_n => rx_data_3_n,
        rx_sync => rxSync, 
        rx_ref_clk_0 => mgtclk,
        rx_sysref_0 => sysref,
        dac_data_0 => dac_data0,
        dac_data_1 => dac_data1,
        dac_data_2 => dac_data2,
        dac_data_3 => dac_data3,
        dac_dunf => dac_in.dac_dunf,
        dac_valid_0 => dac_valid0,
        dac_valid_1 => dac_valid1,
        dac_valid_2 => dac_valid2,
        dac_valid_3 => dac_valid3,
        dac_enable_0 => dac_enable0,
        dac_enable_1 => dac_enable1,
        dac_enable_2 => dac_enable2,
        dac_enable_3 => dac_enable3,
        adc_data_0 => adc_data0,
        adc_data_1 => adc_data1,
        adc_data_2 => adc_data2,
        adc_data_3 => adc_data3,
        adc_dovf => adc_in.adc_dovf,
        adc_valid_0 => adc_valid0,
        adc_valid_1 => adc_valid1,
        adc_valid_2 => adc_valid2,
        adc_valid_3 => adc_valid3,
        adc_enable_0 => adc_enable0,
        adc_enable_1 => adc_enable1,
        adc_enable_2 => adc_enable2,
        adc_enable_3 => adc_enable3,
        axi_ap_in => axi_ap_in,
        axi_ap_out => axi_ap_out,
        s_axi_aclk => axi_tx_adxcvr_in.s_axi_aclk,
        s_axi_aresetn => axi_tx_adxcvr_in.s_axi_aresetn,
        tx_linkclk => tx_linkclk,
        rx_linkclk => rx_linkclk,
        txoutclk => txoutclk,
        rxoutclk => rxoutclk,
        tx_clkgen_ref => devclk,
        rx_clkgen_ref => devclk );

    -- AXI Lite Interface
    axi_ap_in(0).aw.addr <= axi_tx_adxcvr_in.awaddr;
    axi_ap_in(0).aw.prot <= axi_tx_adxcvr_in.awprot;
    axi_ap_in(0).aw.valid <= axi_tx_adxcvr_in.awvalid;
    axi_tx_adxcvr_out.awready <= axi_ap_out(0).aw.ready;
    axi_ap_in(0).w.data <= axi_tx_adxcvr_in.wdata;
    axi_ap_in(0).w.strb <= axi_tx_adxcvr_in.wstrb;
    axi_ap_in(0).w.valid <= axi_tx_adxcvr_in.wvalid;
    axi_tx_adxcvr_out.wready <= axi_ap_out(0).w.ready;
    axi_tx_adxcvr_out.bresp <= axi_ap_out(0).b.resp;
    axi_tx_adxcvr_out.bvalid <= axi_ap_out(0).b.valid;
    axi_ap_in(0).b.ready <= axi_tx_adxcvr_in.bready;
    axi_ap_in(0).ar.addr <= axi_tx_adxcvr_in.araddr; 
    axi_ap_in(0).ar.prot <= axi_tx_adxcvr_in.arprot;
    axi_ap_in(0).ar.valid <= axi_tx_adxcvr_in.arvalid;
    axi_tx_adxcvr_out.arready <= axi_ap_out(0).ar.ready;
    axi_tx_adxcvr_out.rdata <= axi_ap_out(0).r.data;
    axi_tx_adxcvr_out.rresp <= axi_ap_out(0).r.resp;
    axi_tx_adxcvr_out.rvalid <= axi_ap_out(0).r.valid;
    axi_ap_in(0).r.ready <= axi_tx_adxcvr_in.rready;
    
    axi_ap_in(1).aw.addr <= axi_tx_jesd_in.awaddr;
    axi_ap_in(1).aw.prot <= axi_tx_jesd_in.awprot;
    axi_ap_in(1).aw.valid <= axi_tx_jesd_in.awvalid;
    axi_tx_jesd_out.awready <= axi_ap_out(1).aw.ready;
    axi_ap_in(1).w.data <= axi_tx_jesd_in.wdata;
    axi_ap_in(1).w.strb <= axi_tx_jesd_in.wstrb;
    axi_ap_in(1).w.valid <= axi_tx_jesd_in.wvalid;
    axi_tx_jesd_out.wready <= axi_ap_out(1).w.ready;
    axi_tx_jesd_out.bresp <= axi_ap_out(1).b.resp;
    axi_tx_jesd_out.bvalid <= axi_ap_out(1).b.valid;
    axi_ap_in(1).b.ready <= axi_tx_jesd_in.bready;
    axi_ap_in(1).ar.addr <= axi_tx_jesd_in.araddr; 
    axi_ap_in(1).ar.prot <= axi_tx_jesd_in.arprot;
    axi_ap_in(1).ar.valid <= axi_tx_jesd_in.arvalid;
    axi_tx_jesd_out.arready <= axi_ap_out(1).ar.ready;
    axi_tx_jesd_out.rdata <= axi_ap_out(1).r.data;
    axi_tx_jesd_out.rresp <= axi_ap_out(1).r.resp;
    axi_tx_jesd_out.rvalid <= axi_ap_out(1).r.valid;
    axi_ap_in(1).r.ready <= axi_tx_jesd_in.rready;
    
    axi_ap_in(2).aw.addr <= axi_tx_tpl_in.awaddr;
    axi_ap_in(2).aw.prot <= axi_tx_tpl_in.awprot;
    axi_ap_in(2).aw.valid <= axi_tx_tpl_in.awvalid;
    axi_tx_tpl_out.awready <= axi_ap_out(2).aw.ready;
    axi_ap_in(2).w.data <= axi_tx_tpl_in.wdata;
    axi_ap_in(2).w.strb <= axi_tx_tpl_in.wstrb;
    axi_ap_in(2).w.valid <= axi_tx_tpl_in.wvalid;
    axi_tx_tpl_out.wready <= axi_ap_out(2).w.ready;
    axi_tx_tpl_out.bresp <= axi_ap_out(2).b.resp;
    axi_tx_tpl_out.bvalid <= axi_ap_out(2).b.valid;
    axi_ap_in(2).b.ready <= axi_tx_tpl_in.bready;
    axi_ap_in(2).ar.addr <= axi_tx_tpl_in.araddr; 
    axi_ap_in(2).ar.prot <= axi_tx_tpl_in.arprot;
    axi_ap_in(2).ar.valid <= axi_tx_tpl_in.arvalid;
    axi_tx_tpl_out.arready <= axi_ap_out(2).ar.ready;
    axi_tx_tpl_out.rdata <= axi_ap_out(2).r.data;
    axi_tx_tpl_out.rresp <= axi_ap_out(2).r.resp;
    axi_tx_tpl_out.rvalid <= axi_ap_out(2).r.valid;
    axi_ap_in(2).r.ready <= axi_tx_tpl_in.rready;
    
    axi_ap_in(3).aw.addr <= axi_tx_clkgen_in.awaddr;
    axi_ap_in(3).aw.prot <= axi_tx_clkgen_in.awprot;
    axi_ap_in(3).aw.valid <= axi_tx_clkgen_in.awvalid;
    axi_tx_clkgen_out.awready <= axi_ap_out(3).aw.ready;
    axi_ap_in(3).w.data <= axi_tx_clkgen_in.wdata;
    axi_ap_in(3).w.strb <= axi_tx_clkgen_in.wstrb;
    axi_ap_in(3).w.valid <= axi_tx_clkgen_in.wvalid;
    axi_tx_clkgen_out.wready <= axi_ap_out(3).w.ready;
    axi_tx_clkgen_out.bresp <= axi_ap_out(3).b.resp;
    axi_tx_clkgen_out.bvalid <= axi_ap_out(3).b.valid;
    axi_ap_in(3).b.ready <= axi_tx_clkgen_in.bready;
    axi_ap_in(3).ar.addr <= axi_tx_clkgen_in.araddr; 
    axi_ap_in(3).ar.prot <= axi_tx_clkgen_in.arprot;
    axi_ap_in(3).ar.valid <= axi_tx_clkgen_in.arvalid;
    axi_tx_clkgen_out.arready <= axi_ap_out(3).ar.ready;
    axi_tx_clkgen_out.rdata <= axi_ap_out(3).r.data;
    axi_tx_clkgen_out.rresp <= axi_ap_out(3).r.resp;
    axi_tx_clkgen_out.rvalid <= axi_ap_out(3).r.valid;
    axi_ap_in(3).r.ready <= axi_tx_clkgen_in.rready;
    
    axi_ap_in(4).aw.addr <= axi_rx_adxcvr_in.awaddr;
    axi_ap_in(4).aw.prot <= axi_rx_adxcvr_in.awprot;
    axi_ap_in(4).aw.valid <= axi_rx_adxcvr_in.awvalid;
    axi_rx_adxcvr_out.awready <= axi_ap_out(4).aw.ready;
    axi_ap_in(4).w.data <= axi_rx_adxcvr_in.wdata;
    axi_ap_in(4).w.strb <= axi_rx_adxcvr_in.wstrb;
    axi_ap_in(4).w.valid <= axi_rx_adxcvr_in.wvalid;
    axi_rx_adxcvr_out.wready <= axi_ap_out(4).w.ready;
    axi_rx_adxcvr_out.bresp <= axi_ap_out(4).b.resp;
    axi_rx_adxcvr_out.bvalid <= axi_ap_out(4).b.valid;
    axi_ap_in(4).b.ready <= axi_rx_adxcvr_in.bready;
    axi_ap_in(4).ar.addr <= axi_rx_adxcvr_in.araddr; 
    axi_ap_in(4).ar.prot <= axi_rx_adxcvr_in.arprot;
    axi_ap_in(4).ar.valid <= axi_rx_adxcvr_in.arvalid;
    axi_rx_adxcvr_out.arready <= axi_ap_out(4).ar.ready;
    axi_rx_adxcvr_out.rdata <= axi_ap_out(4).r.data;
    axi_rx_adxcvr_out.rresp <= axi_ap_out(4).r.resp;
    axi_rx_adxcvr_out.rvalid <= axi_ap_out(4).r.valid;
    axi_ap_in(4).r.ready <= axi_rx_adxcvr_in.rready;
    
    axi_ap_in(5).aw.addr <= axi_rx_jesd_in.awaddr;
    axi_ap_in(5).aw.prot <= axi_rx_jesd_in.awprot;
    axi_ap_in(5).aw.valid <= axi_rx_jesd_in.awvalid;
    axi_rx_jesd_out.awready <= axi_ap_out(5).aw.ready;
    axi_ap_in(5).w.data <= axi_rx_jesd_in.wdata;
    axi_ap_in(5).w.strb <= axi_rx_jesd_in.wstrb;
    axi_ap_in(5).w.valid <= axi_rx_jesd_in.wvalid;
    axi_rx_jesd_out.wready <= axi_ap_out(5).w.ready;
    axi_rx_jesd_out.bresp <= axi_ap_out(5).b.resp;
    axi_rx_jesd_out.bvalid <= axi_ap_out(5).b.valid;
    axi_ap_in(5).b.ready <= axi_rx_jesd_in.bready;
    axi_ap_in(5).ar.addr <= axi_rx_jesd_in.araddr; 
    axi_ap_in(5).ar.prot <= axi_rx_jesd_in.arprot;
    axi_ap_in(5).ar.valid <= axi_rx_jesd_in.arvalid;
    axi_rx_jesd_out.arready <= axi_ap_out(5).ar.ready;
    axi_rx_jesd_out.rdata <= axi_ap_out(5).r.data;
    axi_rx_jesd_out.rresp <= axi_ap_out(5).r.resp;
    axi_rx_jesd_out.rvalid <= axi_ap_out(5).r.valid;
    axi_ap_in(5).r.ready <= axi_rx_jesd_in.rready;
    
    axi_ap_in(6).aw.addr <= axi_rx_tpl_in.awaddr;
    axi_ap_in(6).aw.prot <= axi_rx_tpl_in.awprot;
    axi_ap_in(6).aw.valid <= axi_rx_tpl_in.awvalid;
    axi_rx_tpl_out.awready <= axi_ap_out(6).aw.ready;
    axi_ap_in(6).w.data <= axi_rx_tpl_in.wdata;
    axi_ap_in(6).w.strb <= axi_rx_tpl_in.wstrb;
    axi_ap_in(6).w.valid <= axi_rx_tpl_in.wvalid;
    axi_rx_tpl_out.wready <= axi_ap_out(6).w.ready;
    axi_rx_tpl_out.bresp <= axi_ap_out(6).b.resp;
    axi_rx_tpl_out.bvalid <= axi_ap_out(6).b.valid;
    axi_ap_in(6).b.ready <= axi_rx_tpl_in.bready;
    axi_ap_in(6).ar.addr <= axi_rx_tpl_in.araddr; 
    axi_ap_in(6).ar.prot <= axi_rx_tpl_in.arprot;
    axi_ap_in(6).ar.valid <= axi_rx_tpl_in.arvalid;
    axi_rx_tpl_out.arready <= axi_ap_out(6).ar.ready;
    axi_rx_tpl_out.rdata <= axi_ap_out(6).r.data;
    axi_rx_tpl_out.rresp <= axi_ap_out(6).r.resp;
    axi_rx_tpl_out.rvalid <= axi_ap_out(6).r.valid;
    axi_ap_in(6).r.ready <= axi_rx_tpl_in.rready;
    
    axi_ap_in(7).aw.addr <= axi_rx_clkgen_in.awaddr;
    axi_ap_in(7).aw.prot <= axi_rx_clkgen_in.awprot;
    axi_ap_in(7).aw.valid <= axi_rx_clkgen_in.awvalid;
    axi_rx_clkgen_out.awready <= axi_ap_out(7).aw.ready;
    axi_ap_in(7).w.data <= axi_rx_clkgen_in.wdata;
    axi_ap_in(7).w.strb <= axi_rx_clkgen_in.wstrb;
    axi_ap_in(7).w.valid <= axi_rx_clkgen_in.wvalid;
    axi_rx_clkgen_out.wready <= axi_ap_out(7).w.ready;
    axi_rx_clkgen_out.bresp <= axi_ap_out(7).b.resp;
    axi_rx_clkgen_out.bvalid <= axi_ap_out(7).b.valid;
    axi_ap_in(7).b.ready <= axi_rx_clkgen_in.bready;
    axi_ap_in(7).ar.addr <= axi_rx_clkgen_in.araddr; 
    axi_ap_in(7).ar.prot <= axi_rx_clkgen_in.arprot;
    axi_ap_in(7).ar.valid <= axi_rx_clkgen_in.arvalid;
    axi_rx_clkgen_out.arready <= axi_ap_out(7).ar.ready;
    axi_rx_clkgen_out.rdata <= axi_ap_out(7).r.data;
    axi_rx_clkgen_out.rresp <= axi_ap_out(7).r.resp;
    axi_rx_clkgen_out.rvalid <= axi_ap_out(7).r.valid;
    axi_ap_in(7).r.ready <= axi_rx_clkgen_in.rready;


-- Vivadio ILA Intergation
  DEBUG: if its(VIVADO_ILA_p) generate  

    signal mgtclk_ila       : std_logic_vector(0 downto 0);
    signal devclk_ila       : std_logic_vector(0 downto 0);
    signal sysref_ila       : std_logic_vector(0 downto 0);
    signal tx_linkclk_ila   : std_logic_vector(0 downto 0);
    signal rx_linkclk_ila   : std_logic_vector(0 downto 0);
    signal txoutclk_ila     : std_logic_vector(0 downto 0);
    signal rxoutclk_ila     : std_logic_vector(0 downto 0);
    

  begin

    mgtclk_ila(0) <= mgtclk;
    devclk_ila(0) <= devclk;
    sysref_ila(0) <= sysref;
    tx_linkclk_ila(0) <= tx_linkclk;
    rx_linkclk_ila(0) <= rx_linkclk;
    txoutclk_ila(0) <= txoutclk;
    rxoutclk_ila(0) <= rxoutclk;


    jesd_ila: ila_0
      port map (
        clk => devclk,
        probe0 => dac_data0,
        probe1 => dac_data1,
        probe2 => dac_data2,
        probe3 => dac_data3,
        probe4 => dac_enable0,
        probe5 => dac_enable1,
        probe6 => dac_enable2,
        probe7 => dac_enable3,
        probe8 => txSync,
        probe9 => tx_linkclk_ila,
        probe10 => adc_data0,
        probe11 => adc_data1,
        probe12 => adc_data2,
        probe13 => adc_data3,
        probe14 => adc_enable0,
        probe15 => adc_enable1,
        probe16 => adc_enable2,
        probe17 => adc_enable3,
        probe18 => rxSync,
        probe19 => rx_linkclk_ila,
        probe20 => sysref_ila );  
  
  end generate;

end Structural;