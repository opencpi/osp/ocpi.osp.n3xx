library IEEE; use IEEE.std_logic_1164.all, ieee.numeric_std.all;
library work; use work.adrv9371_jesd_2tx_2rx_bd_pkg.all;

entity adrv9371_jesd_2tx_2rx_bd is
  port(
    tx_data_0_p : out STD_LOGIC;
    tx_data_0_n : out STD_LOGIC;
    tx_data_1_p : out STD_LOGIC;
    tx_data_1_n : out STD_LOGIC;
    tx_data_2_p : out STD_LOGIC;
    tx_data_2_n : out STD_LOGIC;
    tx_data_3_p : out STD_LOGIC;
    tx_data_3_n : out STD_LOGIC;
    tx_sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_ref_clk_0 : in STD_LOGIC;
    tx_sysref_0 : in STD_LOGIC;
    rx_data_0_p : in STD_LOGIC;
    rx_data_0_n : in STD_LOGIC;
    rx_data_1_p : in STD_LOGIC;
    rx_data_1_n : in STD_LOGIC;
    rx_data_2_p : in STD_LOGIC;
    rx_data_2_n : in STD_LOGIC;
    rx_data_3_p : in STD_LOGIC;
    rx_data_3_n : in STD_LOGIC;
    rx_sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_ref_clk_0 : in STD_LOGIC;
    rx_sysref_0 : in STD_LOGIC;
    dac_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_dunf : in STD_LOGIC;
    dac_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_dovf : in STD_LOGIC;
    adc_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    axi_ap_in : in axilite_ap_m2s_array_t(0 to C_M_AXI_AP_COUNT-1);
    axi_ap_out : out axilite_ap_s2m_array_t(0 to C_M_AXI_AP_COUNT-1);
    s_axi_aclk : in std_logic;
    s_axi_aresetn : in std_logic;
    tx_linkclk : out std_logic;
    rx_linkclk : out std_logic;
    txoutclk : out std_logic;
    rxoutclk : out std_logic;
    tx_clkgen_ref : in std_logic;
    rx_clkgen_ref : in std_logic );
end entity adrv9371_jesd_2tx_2rx_bd;

architecture Structural of adrv9371_jesd_2tx_2rx_bd is

component system is
  port (
    tx_data_0_p : out STD_LOGIC;
    tx_data_0_n : out STD_LOGIC;
    tx_data_1_p : out STD_LOGIC;
    tx_data_1_n : out STD_LOGIC;
    tx_data_2_p : out STD_LOGIC;
    tx_data_2_n : out STD_LOGIC;
    tx_data_3_p : out STD_LOGIC;
    tx_data_3_n : out STD_LOGIC;
    tx_sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_ref_clk_0 : in STD_LOGIC;
    tx_sysref_0 : in STD_LOGIC;
    rx_data_0_p : in STD_LOGIC;
    rx_data_0_n : in STD_LOGIC;
    rx_data_1_p : in STD_LOGIC;
    rx_data_1_n : in STD_LOGIC;
    rx_data_2_p : in STD_LOGIC;
    rx_data_2_n : in STD_LOGIC;
    rx_data_3_p : in STD_LOGIC;
    rx_data_3_n : in STD_LOGIC;
    rx_sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_ref_clk_0 : in STD_LOGIC;
    rx_sysref_0 : in STD_LOGIC;
    dac_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_dunf : in STD_LOGIC;
    dac_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_dovf : in STD_LOGIC;
    adc_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_tx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_awready : out STD_LOGIC;
    s_axi_tx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_wready : out STD_LOGIC;
    s_axi_tx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_bready : in STD_LOGIC;
    s_axi_tx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_arready : out STD_LOGIC;
    s_axi_tx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_rready : in STD_LOGIC;
    s_axi_tx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_awvalid : in STD_LOGIC;
    s_axi_tx_jesd_awready : out STD_LOGIC;
    s_axi_tx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_jesd_wvalid : in STD_LOGIC;
    s_axi_tx_jesd_wready : out STD_LOGIC;
    s_axi_tx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_bvalid : out STD_LOGIC;
    s_axi_tx_jesd_bready : in STD_LOGIC;
    s_axi_tx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_arvalid : in STD_LOGIC;
    s_axi_tx_jesd_arready : out STD_LOGIC;
    s_axi_tx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_rvalid : out STD_LOGIC;
    s_axi_tx_jesd_rready : in STD_LOGIC;
    s_axi_tx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_awvalid : in STD_LOGIC;
    s_axi_tx_tpl_awready : out STD_LOGIC;
    s_axi_tx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_tpl_wvalid : in STD_LOGIC;
    s_axi_tx_tpl_wready : out STD_LOGIC;
    s_axi_tx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_bvalid : out STD_LOGIC;
    s_axi_tx_tpl_bready : in STD_LOGIC;
    s_axi_tx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_arvalid : in STD_LOGIC;
    s_axi_tx_tpl_arready : out STD_LOGIC;
    s_axi_tx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_rvalid : out STD_LOGIC;
    s_axi_tx_tpl_rready : in STD_LOGIC;
    s_axi_tx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_awvalid : in STD_LOGIC;
    s_axi_tx_clkgen_awready : out STD_LOGIC;
    s_axi_tx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_clkgen_wvalid : in STD_LOGIC;
    s_axi_tx_clkgen_wready : out STD_LOGIC;
    s_axi_tx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_bvalid : out STD_LOGIC;
    s_axi_tx_clkgen_bready : in STD_LOGIC;
    s_axi_tx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_arvalid : in STD_LOGIC;
    s_axi_tx_clkgen_arready : out STD_LOGIC;
    s_axi_tx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_rvalid : out STD_LOGIC;
    s_axi_tx_clkgen_rready : in STD_LOGIC;
    s_axi_rx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_awready : out STD_LOGIC;
    s_axi_rx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_wready : out STD_LOGIC;
    s_axi_rx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_bready : in STD_LOGIC;
    s_axi_rx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_arready : out STD_LOGIC;
    s_axi_rx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_rready : in STD_LOGIC;
    s_axi_rx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_awvalid : in STD_LOGIC;
    s_axi_rx_jesd_awready : out STD_LOGIC;
    s_axi_rx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_jesd_wvalid : in STD_LOGIC;
    s_axi_rx_jesd_wready : out STD_LOGIC;
    s_axi_rx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_bvalid : out STD_LOGIC;
    s_axi_rx_jesd_bready : in STD_LOGIC;
    s_axi_rx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_arvalid : in STD_LOGIC;
    s_axi_rx_jesd_arready : out STD_LOGIC;
    s_axi_rx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_rvalid : out STD_LOGIC;
    s_axi_rx_jesd_rready : in STD_LOGIC;
    s_axi_rx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_awvalid : in STD_LOGIC;
    s_axi_rx_tpl_awready : out STD_LOGIC;
    s_axi_rx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_tpl_wvalid : in STD_LOGIC;
    s_axi_rx_tpl_wready : out STD_LOGIC;
    s_axi_rx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_bvalid : out STD_LOGIC;
    s_axi_rx_tpl_bready : in STD_LOGIC;
    s_axi_rx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_arvalid : in STD_LOGIC;
    s_axi_rx_tpl_arready : out STD_LOGIC;
    s_axi_rx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_rvalid : out STD_LOGIC;
    s_axi_rx_tpl_rready : in STD_LOGIC;
    s_axi_rx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_awvalid : in STD_LOGIC;
    s_axi_rx_clkgen_awready : out STD_LOGIC;
    s_axi_rx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_clkgen_wvalid : in STD_LOGIC;
    s_axi_rx_clkgen_wready : out STD_LOGIC;
    s_axi_rx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_bvalid : out STD_LOGIC;
    s_axi_rx_clkgen_bready : in STD_LOGIC;
    s_axi_rx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_arvalid : in STD_LOGIC;
    s_axi_rx_clkgen_arready : out STD_LOGIC;
    s_axi_rx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_rvalid : out STD_LOGIC;
    s_axi_rx_clkgen_rready : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    tx_linkclk : out STD_LOGIC;
    rx_linkclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    tx_clkgen_ref : in STD_LOGIC;
    rx_clkgen_ref : in STD_LOGIC );
end component system;

begin

BD: system
  port map (
    tx_data_0_p => tx_data_0_p,
    tx_data_0_n => tx_data_0_n,
    tx_data_1_p => tx_data_1_p,
    tx_data_1_n => tx_data_1_n,
    tx_data_2_p => tx_data_2_p,
    tx_data_2_n => tx_data_2_n,
    tx_data_3_p => tx_data_3_p,
    tx_data_3_n => tx_data_3_n,
    tx_sync => tx_sync, 
  	tx_ref_clk_0 => tx_ref_clk_0,
    tx_sysref_0 => tx_sysref_0,
    rx_data_0_p => rx_data_0_p,
    rx_data_0_n => rx_data_0_n,
    rx_data_1_p => rx_data_1_p,
    rx_data_1_n => rx_data_1_n,
    rx_data_2_p => rx_data_2_p,
    rx_data_2_n => rx_data_2_n,
    rx_data_3_p => rx_data_3_p,
    rx_data_3_n => rx_data_3_n,
    rx_sync => rx_sync, 
    rx_ref_clk_0 => rx_ref_clk_0,
    rx_sysref_0 => rx_sysref_0,
    dac_data_0 => dac_data_0,
    dac_data_1 => dac_data_1,
    dac_data_2 => dac_data_2,
    dac_data_3 => dac_data_3,
    dac_dunf => dac_dunf,
    dac_valid_0 => dac_valid_0,
    dac_valid_1 => dac_valid_1,
    dac_valid_2 => dac_valid_2,
    dac_valid_3 => dac_valid_3,
    dac_enable_0 => dac_enable_0,
    dac_enable_1 => dac_enable_1,
    dac_enable_2 => dac_enable_2,
    dac_enable_3 => dac_enable_3,
    adc_data_0 => adc_data_0,
    adc_data_1 => adc_data_1,
    adc_data_2 => adc_data_2,
    adc_data_3 => adc_data_3,
    adc_dovf => adc_dovf,
    adc_valid_0 => adc_valid_0,
    adc_valid_1 => adc_valid_1,
    adc_valid_2 => adc_valid_2,
    adc_valid_3 => adc_valid_3,
    adc_enable_0 => adc_enable_0,
    adc_enable_1 => adc_enable_1,
    adc_enable_2 => adc_enable_2,
    adc_enable_3 => adc_enable_3,
    s_axi_tx_adxcvr_awaddr => axi_ap_in(0).aw.addr(15 downto 0),
    s_axi_tx_adxcvr_awprot => axi_ap_in(0).aw.prot,
    s_axi_tx_adxcvr_awvalid => axi_ap_in(0).aw.valid,
    s_axi_tx_adxcvr_awready => axi_ap_out(0).aw.ready,
    s_axi_tx_adxcvr_wdata => axi_ap_in(0).w.data,
    s_axi_tx_adxcvr_wstrb => axi_ap_in(0).w.strb,
    s_axi_tx_adxcvr_wvalid => axi_ap_in(0).w.valid,
    s_axi_tx_adxcvr_wready => axi_ap_out(0).w.ready,
    s_axi_tx_adxcvr_bresp => axi_ap_out(0).b.resp,
    s_axi_tx_adxcvr_bvalid => axi_ap_out(0).b.valid,
    s_axi_tx_adxcvr_bready => axi_ap_in(0).b.ready,
    s_axi_tx_adxcvr_araddr => axi_ap_in(0).ar.addr(15 downto 0),
    s_axi_tx_adxcvr_arprot => axi_ap_in(0).ar.prot,
    s_axi_tx_adxcvr_arvalid => axi_ap_in(0).ar.valid,
    s_axi_tx_adxcvr_arready => axi_ap_out(0).ar.ready,
    s_axi_tx_adxcvr_rdata => axi_ap_out(0).r.data,
    s_axi_tx_adxcvr_rresp => axi_ap_out(0).r.resp,
    s_axi_tx_adxcvr_rvalid => axi_ap_out(0).r.valid,
    s_axi_tx_adxcvr_rready => axi_ap_in(0).r.ready,
    s_axi_tx_jesd_awaddr => axi_ap_in(1).aw.addr(13 downto 0),
    s_axi_tx_jesd_awprot => axi_ap_in(1).aw.prot,
    s_axi_tx_jesd_awvalid => axi_ap_in(1).aw.valid,
    s_axi_tx_jesd_awready => axi_ap_out(1).aw.ready,
    s_axi_tx_jesd_wdata => axi_ap_in(1).w.data,
    s_axi_tx_jesd_wstrb => axi_ap_in(1).w.strb,
    s_axi_tx_jesd_wvalid => axi_ap_in(1).w.valid,
    s_axi_tx_jesd_wready => axi_ap_out(1).w.ready,
    s_axi_tx_jesd_bresp => axi_ap_out(1).b.resp,
    s_axi_tx_jesd_bvalid => axi_ap_out(1).b.valid,
    s_axi_tx_jesd_bready => axi_ap_in(1).b.ready,
    s_axi_tx_jesd_araddr => axi_ap_in(1).ar.addr(13 downto 0),
    s_axi_tx_jesd_arprot => axi_ap_in(1).ar.prot,
    s_axi_tx_jesd_arvalid => axi_ap_in(1).ar.valid,
    s_axi_tx_jesd_arready => axi_ap_out(1).ar.ready,
    s_axi_tx_jesd_rdata => axi_ap_out(1).r.data,
    s_axi_tx_jesd_rresp => axi_ap_out(1).r.resp,
    s_axi_tx_jesd_rvalid => axi_ap_out(1).r.valid,
    s_axi_tx_jesd_rready => axi_ap_in(1).r.ready,
    s_axi_tx_tpl_awaddr => axi_ap_in(2).aw.addr(12 downto 0),
    s_axi_tx_tpl_awprot => axi_ap_in(2).aw.prot,
    s_axi_tx_tpl_awvalid => axi_ap_in(2).aw.valid,
    s_axi_tx_tpl_awready => axi_ap_out(2).aw.ready,
    s_axi_tx_tpl_wdata => axi_ap_in(2).w.data,
    s_axi_tx_tpl_wstrb => axi_ap_in(2).w.strb,
    s_axi_tx_tpl_wvalid => axi_ap_in(2).w.valid,
    s_axi_tx_tpl_wready => axi_ap_out(2).w.ready,
    s_axi_tx_tpl_bresp => axi_ap_out(2).b.resp,
    s_axi_tx_tpl_bvalid => axi_ap_out(2).b.valid,
    s_axi_tx_tpl_bready => axi_ap_in(2).b.ready,
    s_axi_tx_tpl_araddr => axi_ap_in(2).ar.addr(12 downto 0),
    s_axi_tx_tpl_arprot => axi_ap_in(2).ar.prot,
    s_axi_tx_tpl_arvalid => axi_ap_in(2).ar.valid,
    s_axi_tx_tpl_arready => axi_ap_out(2).ar.ready,
    s_axi_tx_tpl_rdata => axi_ap_out(2).r.data,
    s_axi_tx_tpl_rresp => axi_ap_out(2).r.resp,
    s_axi_tx_tpl_rvalid => axi_ap_out(2).r.valid,
    s_axi_tx_tpl_rready => axi_ap_in(2).r.ready,
    s_axi_tx_clkgen_awaddr => axi_ap_in(3).aw.addr(15 downto 0),
    s_axi_tx_clkgen_awprot => axi_ap_in(3).aw.prot,
    s_axi_tx_clkgen_awvalid => axi_ap_in(3).aw.valid,
    s_axi_tx_clkgen_awready => axi_ap_out(3).aw.ready,
    s_axi_tx_clkgen_wdata => axi_ap_in(3).w.data,
    s_axi_tx_clkgen_wstrb => axi_ap_in(3).w.strb,
    s_axi_tx_clkgen_wvalid => axi_ap_in(3).w.valid,
    s_axi_tx_clkgen_wready => axi_ap_out(3).w.ready,
    s_axi_tx_clkgen_bresp => axi_ap_out(3).b.resp,
    s_axi_tx_clkgen_bvalid => axi_ap_out(3).b.valid,
    s_axi_tx_clkgen_bready => axi_ap_in(3).b.ready,
    s_axi_tx_clkgen_araddr => axi_ap_in(3).ar.addr(15 downto 0),
    s_axi_tx_clkgen_arprot => axi_ap_in(3).ar.prot,
    s_axi_tx_clkgen_arvalid => axi_ap_in(3).ar.valid,
    s_axi_tx_clkgen_arready => axi_ap_out(3).ar.ready,
    s_axi_tx_clkgen_rdata => axi_ap_out(3).r.data,
    s_axi_tx_clkgen_rresp => axi_ap_out(3).r.resp,
    s_axi_tx_clkgen_rvalid => axi_ap_out(3).r.valid,
    s_axi_tx_clkgen_rready => axi_ap_in(3).r.ready,
    s_axi_rx_adxcvr_awaddr => axi_ap_in(4).aw.addr(15 downto 0),
    s_axi_rx_adxcvr_awprot => axi_ap_in(4).aw.prot,
    s_axi_rx_adxcvr_awvalid => axi_ap_in(4).aw.valid,
    s_axi_rx_adxcvr_awready => axi_ap_out(4).aw.ready,
    s_axi_rx_adxcvr_wdata => axi_ap_in(4).w.data,
    s_axi_rx_adxcvr_wstrb => axi_ap_in(4).w.strb,
    s_axi_rx_adxcvr_wvalid => axi_ap_in(4).w.valid,
    s_axi_rx_adxcvr_wready => axi_ap_out(4).w.ready,
    s_axi_rx_adxcvr_bresp => axi_ap_out(4).b.resp,
    s_axi_rx_adxcvr_bvalid => axi_ap_out(4).b.valid,
    s_axi_rx_adxcvr_bready => axi_ap_in(4).b.ready,
    s_axi_rx_adxcvr_araddr => axi_ap_in(4).ar.addr(15 downto 0),
    s_axi_rx_adxcvr_arprot => axi_ap_in(4).ar.prot,
    s_axi_rx_adxcvr_arvalid => axi_ap_in(4).ar.valid,
    s_axi_rx_adxcvr_arready => axi_ap_out(4).ar.ready,
    s_axi_rx_adxcvr_rdata => axi_ap_out(4).r.data,
    s_axi_rx_adxcvr_rresp => axi_ap_out(4).r.resp,
    s_axi_rx_adxcvr_rvalid => axi_ap_out(4).r.valid,
    s_axi_rx_adxcvr_rready => axi_ap_in(4).r.ready,
    s_axi_rx_jesd_awaddr => axi_ap_in(5).aw.addr(13 downto 0),
    s_axi_rx_jesd_awprot => axi_ap_in(5).aw.prot,
    s_axi_rx_jesd_awvalid => axi_ap_in(5).aw.valid,
    s_axi_rx_jesd_awready => axi_ap_out(5).aw.ready,
    s_axi_rx_jesd_wdata => axi_ap_in(5).w.data,
    s_axi_rx_jesd_wstrb => axi_ap_in(5).w.strb,
    s_axi_rx_jesd_wvalid => axi_ap_in(5).w.valid,
    s_axi_rx_jesd_wready => axi_ap_out(5).w.ready,
    s_axi_rx_jesd_bresp => axi_ap_out(5).b.resp,
    s_axi_rx_jesd_bvalid => axi_ap_out(5).b.valid,
    s_axi_rx_jesd_bready => axi_ap_in(5).b.ready,
    s_axi_rx_jesd_araddr => axi_ap_in(5).ar.addr(13 downto 0),
    s_axi_rx_jesd_arprot => axi_ap_in(5).ar.prot,
    s_axi_rx_jesd_arvalid => axi_ap_in(5).ar.valid,
    s_axi_rx_jesd_arready => axi_ap_out(5).ar.ready,
    s_axi_rx_jesd_rdata => axi_ap_out(5).r.data,
    s_axi_rx_jesd_rresp => axi_ap_out(5).r.resp,
    s_axi_rx_jesd_rvalid => axi_ap_out(5).r.valid,
    s_axi_rx_jesd_rready => axi_ap_in(5).r.ready,
    s_axi_rx_tpl_awaddr => axi_ap_in(6).aw.addr(12 downto 0),
    s_axi_rx_tpl_awprot =>axi_ap_in(6).aw.prot,
    s_axi_rx_tpl_awvalid => axi_ap_in(6).aw.valid,
    s_axi_rx_tpl_awready => axi_ap_out(6).aw.ready,
    s_axi_rx_tpl_wdata => axi_ap_in(6).w.data,
    s_axi_rx_tpl_wstrb => axi_ap_in(6).w.strb,
    s_axi_rx_tpl_wvalid => axi_ap_in(6).w.valid,
    s_axi_rx_tpl_wready => axi_ap_out(6).w.ready,
    s_axi_rx_tpl_bresp => axi_ap_out(6).b.resp,
    s_axi_rx_tpl_bvalid => axi_ap_out(6).b.valid,
    s_axi_rx_tpl_bready => axi_ap_in(6).b.ready,
    s_axi_rx_tpl_araddr => axi_ap_in(6).ar.addr(12 downto 0),
    s_axi_rx_tpl_arprot => axi_ap_in(6).ar.prot,
    s_axi_rx_tpl_arvalid => axi_ap_in(6).ar.valid,
    s_axi_rx_tpl_arready => axi_ap_out(6).ar.ready,
    s_axi_rx_tpl_rdata => axi_ap_out(6).r.data,
    s_axi_rx_tpl_rresp => axi_ap_out(6).r.resp,
    s_axi_rx_tpl_rvalid => axi_ap_out(6).r.valid,
    s_axi_rx_tpl_rready => axi_ap_in(6).r.ready,
    s_axi_rx_clkgen_awaddr => axi_ap_in(7).aw.addr(15 downto 0),
    s_axi_rx_clkgen_awprot => axi_ap_in(7).aw.prot,
    s_axi_rx_clkgen_awvalid => axi_ap_in(7).aw.valid,
    s_axi_rx_clkgen_awready => axi_ap_out(7).aw.ready,
    s_axi_rx_clkgen_wdata => axi_ap_in(7).w.data,
    s_axi_rx_clkgen_wstrb => axi_ap_in(7).w.strb,
    s_axi_rx_clkgen_wvalid => axi_ap_in(7).w.valid,
    s_axi_rx_clkgen_wready => axi_ap_out(7).w.ready,
    s_axi_rx_clkgen_bresp => axi_ap_out(7).b.resp,
    s_axi_rx_clkgen_bvalid => axi_ap_out(7).b.valid,
    s_axi_rx_clkgen_bready => axi_ap_in(7).b.ready,
    s_axi_rx_clkgen_araddr => axi_ap_in(7).ar.addr(15 downto 0),
    s_axi_rx_clkgen_arprot => axi_ap_in(7).ar.prot,
    s_axi_rx_clkgen_arvalid => axi_ap_in(7).ar.valid,
    s_axi_rx_clkgen_arready => axi_ap_out(7).ar.ready,
    s_axi_rx_clkgen_rdata => axi_ap_out(7).r.data,
    s_axi_rx_clkgen_rresp => axi_ap_out(7).r.resp,
    s_axi_rx_clkgen_rvalid => axi_ap_out(7).r.valid,
    s_axi_rx_clkgen_rready => axi_ap_in(7).r.ready,
    s_axi_aclk => s_axi_aclk,
    s_axi_aresetn => s_axi_aresetn,
    tx_linkclk => tx_linkclk,
    rx_linkclk => rx_linkclk,
    txoutclk => txoutclk,
    rxoutclk => rxoutclk,
    tx_clkgen_ref => tx_clkgen_ref,
    rx_clkgen_ref => rx_clkgen_ref );

end Structural;