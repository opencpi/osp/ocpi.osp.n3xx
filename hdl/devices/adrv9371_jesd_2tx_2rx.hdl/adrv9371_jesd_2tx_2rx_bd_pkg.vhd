library IEEE; use IEEE.std_logic_1164.all, IEEE.numeric_std.all;

package adrv9371_jesd_2tx_2rx_bd_pkg is

  constant C_M_AXI_AP_COUNT	: natural := 8;	

-----------------------------------------------------------
  -- The global signals, which from a prefix point of view is called the A channel
  -- Which of CLK and RESETn is driven by which side varies
  type axilite_ap_a_m2s_t is record
    RESETn : std_logic;
  end record axilite_ap_a_m2s_t;
  --modified above 7/15/2021
  type axilite_ap_a_s2m_t is record
    CLK : std_logic;
  end record axilite_ap_a_s2m_t;
  -- modified above 7/15/2021
------------------------------------------------------------
  -- The write address channel, a.k.a. the AW channel
  type axilite_ap_aw_m2s_t is record
    ADDR : std_logic_vector(32 -1 downto 0);
    PROT : std_logic_vector(2 downto 0);
    VALID : std_logic;
  end record axilite_ap_aw_m2s_t;

  type axilite_ap_aw_s2m_t is record
    READY : std_logic;
  end record axilite_ap_aw_s2m_t;


  -- The write data channel, a.k.a. the W channel
  type axilite_ap_w_m2s_t is record
    DATA : std_logic_vector(32 -1 downto 0);
    STRB : std_logic_vector((32/8)-1 downto 0);
    VALID : std_logic;
  end record axilite_ap_w_m2s_t; 

  type axilite_ap_w_s2m_t is record
    READY : std_logic;
  end record axilite_ap_w_s2m_t;

  -- Th write response channel, a.k.a. the B channel
  type axilite_ap_b_m2s_t is record
    READY : std_logic;
  end record axilite_ap_b_m2s_t;
  
  type axilite_ap_b_s2m_t is record
    RESP : std_logic_vector(1 downto 0);
    VALID : std_logic;
  end record axilite_ap_b_s2m_t;
 
  ------------------------------------------------------------
  -- The read address channel, a.k.a. the AR channel
  type axilite_ap_ar_m2s_t is record
    ADDR : std_logic_vector(32 -1 downto 0);
    PROT : std_logic_vector(2 downto 0);
    VALID : std_logic;
  end record axilite_ap_ar_m2s_t;

  type axilite_ap_ar_s2m_t is record
    READY : std_logic;
  end record axilite_ap_ar_s2m_t;
  

  -- The read data channel, a.k.a. the R channel
  type axilite_ap_r_m2s_t is record
    READY : std_logic;
  end record axilite_ap_r_m2s_t;

  type axilite_ap_r_s2m_t is record
    DATA : std_logic_vector(32 -1 downto 0);
    RESP : std_logic_vector(1 downto 0);
    VALID : std_logic;
  end record axilite_ap_r_s2m_t;


  -- The bundles of signals m2s, and s2m
  type axilite_ap_m2s_t is record
    a   : axilite_ap_a_m2s_t;
    aw  : axilite_ap_aw_m2s_t;
    ar  : axilite_ap_ar_m2s_t;
    w   : axilite_ap_w_m2s_t;
    r   : axilite_ap_r_m2s_t;
    b   : axilite_ap_b_m2s_t;
  end record axilite_ap_m2s_t;
  
  type axilite_ap_s2m_t is record
    a   : axilite_ap_a_s2m_t;
    aw  : axilite_ap_aw_s2m_t;
    ar  : axilite_ap_ar_s2m_t;
    w   : axilite_ap_w_s2m_t;
    r   : axilite_ap_r_s2m_t;
    b   : axilite_ap_b_s2m_t;
  end record axilite_ap_s2m_t;

  type axilite_ap_m2s_array_t is array (natural range <>) of axilite_ap_m2s_t;
  type axilite_ap_s2m_array_t is array (natural range <>) of axilite_ap_s2m_t;

  component adrv9371_jesd_2tx_2rx_bd is
	port (
		  tx_data_0_p : out STD_LOGIC;
    	tx_data_0_n : out STD_LOGIC;
    	tx_data_1_p : out STD_LOGIC;
    	tx_data_1_n : out STD_LOGIC;
    	tx_data_2_p : out STD_LOGIC;
    	tx_data_2_n : out STD_LOGIC;
    	tx_data_3_p : out STD_LOGIC;
    	tx_data_3_n : out STD_LOGIC;
    	tx_sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    	tx_ref_clk_0 : in STD_LOGIC;
    	tx_sysref_0 : in STD_LOGIC;
    	rx_data_0_p : in STD_LOGIC;
    	rx_data_0_n : in STD_LOGIC;
    	rx_data_1_p : in STD_LOGIC;
    	rx_data_1_n : in STD_LOGIC;
    	rx_data_2_p : in STD_LOGIC;
    	rx_data_2_n : in STD_LOGIC;
    	rx_data_3_p : in STD_LOGIC;
    	rx_data_3_n : in STD_LOGIC;
    	rx_sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    	rx_ref_clk_0 : in STD_LOGIC;
    	rx_sysref_0 : in STD_LOGIC;
		  dac_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    	dac_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    	dac_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    	dac_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    	dac_dunf : in STD_LOGIC;
    	dac_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	dac_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	dac_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	dac_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	dac_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	dac_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	dac_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	dac_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    	adc_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    	adc_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    	adc_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    	adc_dovf : in STD_LOGIC;
    	adc_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    	adc_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
		  axi_ap_in : in axilite_ap_m2s_array_t(0 to C_M_AXI_AP_COUNT-1);
      axi_ap_out : out axilite_ap_s2m_array_t(0 to C_M_AXI_AP_COUNT-1);
      s_axi_aclk : in STD_LOGIC;
  		s_axi_aresetn : in STD_LOGIC;
      tx_linkclk : out STD_LOGIC;
      rx_linkclk : out STD_LOGIC;
      txoutclk : out STD_LOGIC;
      rxoutclk : out STD_LOGIC;
      tx_clkgen_ref : in STD_LOGIC;
      rx_clkgen_ref : in STD_LOGIC);
  end component adrv9371_jesd_2tx_2rx_bd;

  component ila_0
  port (
    clk      : in  std_logic;
    probe0   : in  std_logic_vector(31 downto 0);
    probe1   : in  std_logic_vector(31 downto 0);
    probe2   : in  std_logic_vector(31 downto 0);
    probe3   : in  std_logic_vector(31 downto 0);
    probe4   : in  std_logic_vector(0 downto 0);
    probe5   : in  std_logic_vector(0 downto 0);
    probe6   : in  std_logic_vector(0 downto 0);
    probe7   : in  std_logic_vector(0 downto 0);
    probe8   : in  std_logic_vector(0 downto 0);
    probe9   : in  std_logic_vector(0 downto 0);
    probe10   : in  std_logic_vector(31 downto 0);
    probe11   : in  std_logic_vector(31 downto 0);
    probe12   : in  std_logic_vector(31 downto 0);
    probe13   : in  std_logic_vector(31 downto 0);
    probe14   : in  std_logic_vector(0 downto 0);
    probe15   : in  std_logic_vector(0 downto 0);
    probe16   : in  std_logic_vector(0 downto 0);
    probe17   : in  std_logic_vector(0 downto 0);
    probe18   : in  std_logic_vector(0 downto 0);
    probe19   : in  std_logic_vector(0 downto 0);
    probe20   : in  std_logic_vector(0 downto 0) );
  end component;

  component jesd_loopback is
  port(
    clk                 : in std_logic;
    loopback            : in boolean;
    loopback_dac0_vld   : in std_logic_vector(0 downto 0);
    loopback_dac1_vld   : in std_logic_vector(0 downto 0);
    dac0_in             : in std_logic_vector(31 downto 0);
    dac1_in             : in std_logic_vector(31 downto 0);
    dac2_in             : in std_logic_vector(31 downto 0);
    dac3_in             : in std_logic_vector(31 downto 0);
    dac0_out            : out std_logic_vector(31 downto 0);
    dac1_out            : out std_logic_vector(31 downto 0);
    dac2_out            : out std_logic_vector(31 downto 0);
    dac3_out            : out std_logic_vector(31 downto 0);
    dac_valid0_out      : out std_logic_vector(0 downto 0);
    dac_valid1_out      : out std_logic_vector(0 downto 0);
    dac_valid2_out      : out std_logic_vector(0 downto 0);
    dac_valid3_out      : out std_logic_vector(0 downto 0);
    dac_valid0_in       : in std_logic_vector(0 downto 0);
    dac_valid1_in       : in std_logic_vector(0 downto 0);
    dac_valid2_in       : in std_logic_vector(0 downto 0);
    dac_valid3_in       : in std_logic_vector(0 downto 0);
    dac_enable0_out     : out std_logic_vector(0 downto 0);
    dac_enable1_out     : out std_logic_vector(0 downto 0);
    dac_enable2_out     : out std_logic_vector(0 downto 0);
    dac_enable3_out     : out std_logic_vector(0 downto 0);
    dac_enable0_in      : in std_logic_vector(0 downto 0);
    dac_enable1_in      : in std_logic_vector(0 downto 0);
    dac_enable2_in      : in std_logic_vector(0 downto 0);
    dac_enable3_in      : in std_logic_vector(0 downto 0);
    adc0_out            : out std_logic_vector(31 downto 0);
    adc1_out            : out std_logic_vector(31 downto 0);
    adc2_out            : out std_logic_vector(31 downto 0);
    adc3_out            : out std_logic_vector(31 downto 0);
    adc0_in             : in std_logic_vector(31 downto 0);
    adc1_in             : in std_logic_vector(31 downto 0);
    adc2_in             : in std_logic_vector(31 downto 0);
    adc3_in             : in std_logic_vector(31 downto 0);
    adc_valid0_out      : out std_logic_vector(0 downto 0);
    adc_valid1_out      : out std_logic_vector(0 downto 0);
    adc_valid2_out      : out std_logic_vector(0 downto 0);
    adc_valid3_out      : out std_logic_vector(0 downto 0);
    adc_valid0_in       : in std_logic_vector(0 downto 0);
    adc_valid1_in       : in std_logic_vector(0 downto 0);
    adc_valid2_in       : in std_logic_vector(0 downto 0);
    adc_valid3_in       : in std_logic_vector(0 downto 0);
    adc_enable0_out     : out std_logic_vector(0 downto 0);
    adc_enable1_out     : out std_logic_vector(0 downto 0);
    adc_enable2_out     : out std_logic_vector(0 downto 0);
    adc_enable3_out     : out std_logic_vector(0 downto 0);
    adc_enable0_in      : in std_logic_vector(0 downto 0);
    adc_enable1_in      : in std_logic_vector(0 downto 0);
    adc_enable2_in      : in std_logic_vector(0 downto 0);
    adc_enable3_in      : in std_logic_vector(0 downto 0) );
  end component;

end package adrv9371_jesd_2tx_2rx_bd_pkg;
