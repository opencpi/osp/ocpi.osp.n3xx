library IEEE; use IEEE.std_logic_1164.all, ieee.numeric_std.all;
library work; use work.adrv9371_jesd_2tx_2rx_bd_pkg.all;

entity jesd_loopback is
  port(
    clk                 : in std_logic;
    loopback            : in boolean;
    loopback_dac0_vld   : in std_logic_vector(0 downto 0);
    loopback_dac1_vld   : in std_logic_vector(0 downto 0);
    dac0_in             : in std_logic_vector(31 downto 0);
    dac1_in             : in std_logic_vector(31 downto 0);
    dac2_in             : in std_logic_vector(31 downto 0);
    dac3_in             : in std_logic_vector(31 downto 0);
    dac0_out            : out std_logic_vector(31 downto 0);
    dac1_out            : out std_logic_vector(31 downto 0);
    dac2_out            : out std_logic_vector(31 downto 0);
    dac3_out            : out std_logic_vector(31 downto 0);
    dac_valid0_out      : out std_logic_vector(0 downto 0);
    dac_valid1_out      : out std_logic_vector(0 downto 0);
    dac_valid2_out      : out std_logic_vector(0 downto 0);
    dac_valid3_out      : out std_logic_vector(0 downto 0);
    dac_valid0_in       : in std_logic_vector(0 downto 0);
    dac_valid1_in       : in std_logic_vector(0 downto 0);
    dac_valid2_in       : in std_logic_vector(0 downto 0);
    dac_valid3_in       : in std_logic_vector(0 downto 0);
    dac_enable0_out     : out std_logic_vector(0 downto 0);
    dac_enable1_out     : out std_logic_vector(0 downto 0);
    dac_enable2_out     : out std_logic_vector(0 downto 0);
    dac_enable3_out     : out std_logic_vector(0 downto 0);
    dac_enable0_in      : in std_logic_vector(0 downto 0);
    dac_enable1_in      : in std_logic_vector(0 downto 0);
    dac_enable2_in      : in std_logic_vector(0 downto 0);
    dac_enable3_in      : in std_logic_vector(0 downto 0);
    adc0_out            : out std_logic_vector(31 downto 0);
    adc1_out            : out std_logic_vector(31 downto 0);
    adc2_out            : out std_logic_vector(31 downto 0);
    adc3_out            : out std_logic_vector(31 downto 0);
    adc0_in             : in std_logic_vector(31 downto 0);
    adc1_in             : in std_logic_vector(31 downto 0);
    adc2_in             : in std_logic_vector(31 downto 0);
    adc3_in             : in std_logic_vector(31 downto 0);
    adc_valid0_out      : out std_logic_vector(0 downto 0);
    adc_valid1_out      : out std_logic_vector(0 downto 0);
    adc_valid2_out      : out std_logic_vector(0 downto 0);
    adc_valid3_out      : out std_logic_vector(0 downto 0);
    adc_valid0_in       : in std_logic_vector(0 downto 0);
    adc_valid1_in       : in std_logic_vector(0 downto 0);
    adc_valid2_in       : in std_logic_vector(0 downto 0);
    adc_valid3_in       : in std_logic_vector(0 downto 0);
    adc_enable0_out     : out std_logic_vector(0 downto 0);
    adc_enable1_out     : out std_logic_vector(0 downto 0);
    adc_enable2_out     : out std_logic_vector(0 downto 0);
    adc_enable3_out     : out std_logic_vector(0 downto 0);
    adc_enable0_in      : in std_logic_vector(0 downto 0);
    adc_enable1_in      : in std_logic_vector(0 downto 0);
    adc_enable2_in      : in std_logic_vector(0 downto 0);
    adc_enable3_in      : in std_logic_vector(0 downto 0) );
end entity jesd_loopback;

architecture Behavorial of jesd_loopback is

  --signal ctr      : std_logic := '0';
  --signal clk2x    : std_logic := '0';

begin

  --@TODO : May be remove in final implementationation
  --  process (clk) is
  --  begin
  --    if (rising_edge(clk)) then
  --      if (ctr = '0') then
  --        clk2x <= '1';
  --      else
  --        clk2x <= '0';
  --      end if;
  --      ctr <= not ctr;
  --    end if;
  --  end process;

    process (clk) is
    begin
        if (rising_edge(clk)) then
          dac0_out <= (others => '0');
          dac1_out <= (others => '0');
          dac2_out <= (others => '0');
          dac3_out <= (others => '0');
          dac_valid0_out <= "1"; 
          dac_valid1_out <= "1"; 
          dac_valid2_out <= "1";
          dac_valid3_out <= "1";
          dac_enable0_out <= "1";
          dac_enable1_out <= "1";
          dac_enable2_out <= "1";
          dac_enable3_out <= "1";
          adc0_out <= dac0_in;
          adc1_out <= dac1_in;
          adc2_out <= dac2_in;
          adc3_out <= dac3_in;
          adc_valid0_out <= loopback_dac0_vld;
          adc_valid1_out <= loopback_dac0_vld;
          adc_valid2_out <= loopback_dac1_vld;
          adc_valid3_out <= loopback_dac1_vld;
          adc_enable0_out <= "1";
          adc_enable1_out <= "1";
          adc_enable2_out <= "1";
          adc_enable3_out <= "1";
        end if;
    end process;

end Behavorial;