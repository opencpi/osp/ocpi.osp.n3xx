--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Tue Oct 26 15:51:27 2021
--Host        : localhost.localdomain running 64-bit CentOS Linux release 7.9.2009 (Core)
--Command     : generate_target system_wrapper.bd
--Design      : system_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system_wrapper is
  port (
    adc_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_dovf : in STD_LOGIC;
    adc_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_dunf : in STD_LOGIC;
    dac_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_clkgen_ref : in STD_LOGIC;
    rx_data_0_n : in STD_LOGIC;
    rx_data_0_p : in STD_LOGIC;
    rx_data_1_n : in STD_LOGIC;
    rx_data_1_p : in STD_LOGIC;
    rx_data_2_n : in STD_LOGIC;
    rx_data_2_p : in STD_LOGIC;
    rx_data_3_n : in STD_LOGIC;
    rx_data_3_p : in STD_LOGIC;
    rx_linkclk : out STD_LOGIC;
    rx_ref_clk_0 : in STD_LOGIC;
    rx_sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_sysref_0 : in STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_rx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_arready : out STD_LOGIC;
    s_axi_rx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_awready : out STD_LOGIC;
    s_axi_rx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_bready : in STD_LOGIC;
    s_axi_rx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_rready : in STD_LOGIC;
    s_axi_rx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_wready : out STD_LOGIC;
    s_axi_rx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_rx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_arready : out STD_LOGIC;
    s_axi_rx_clkgen_arvalid : in STD_LOGIC;
    s_axi_rx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_awready : out STD_LOGIC;
    s_axi_rx_clkgen_awvalid : in STD_LOGIC;
    s_axi_rx_clkgen_bready : in STD_LOGIC;
    s_axi_rx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_bvalid : out STD_LOGIC;
    s_axi_rx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_rready : in STD_LOGIC;
    s_axi_rx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_rvalid : out STD_LOGIC;
    s_axi_rx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_wready : out STD_LOGIC;
    s_axi_rx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_clkgen_wvalid : in STD_LOGIC;
    s_axi_rx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_arready : out STD_LOGIC;
    s_axi_rx_jesd_arvalid : in STD_LOGIC;
    s_axi_rx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_awready : out STD_LOGIC;
    s_axi_rx_jesd_awvalid : in STD_LOGIC;
    s_axi_rx_jesd_bready : in STD_LOGIC;
    s_axi_rx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_bvalid : out STD_LOGIC;
    s_axi_rx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_rready : in STD_LOGIC;
    s_axi_rx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_rvalid : out STD_LOGIC;
    s_axi_rx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_wready : out STD_LOGIC;
    s_axi_rx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_jesd_wvalid : in STD_LOGIC;
    s_axi_rx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_arready : out STD_LOGIC;
    s_axi_rx_tpl_arvalid : in STD_LOGIC;
    s_axi_rx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_awready : out STD_LOGIC;
    s_axi_rx_tpl_awvalid : in STD_LOGIC;
    s_axi_rx_tpl_bready : in STD_LOGIC;
    s_axi_rx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_bvalid : out STD_LOGIC;
    s_axi_rx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_rready : in STD_LOGIC;
    s_axi_rx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_rvalid : out STD_LOGIC;
    s_axi_rx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_wready : out STD_LOGIC;
    s_axi_rx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_tpl_wvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_arready : out STD_LOGIC;
    s_axi_tx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_awready : out STD_LOGIC;
    s_axi_tx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_bready : in STD_LOGIC;
    s_axi_tx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_rready : in STD_LOGIC;
    s_axi_tx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_wready : out STD_LOGIC;
    s_axi_tx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_tx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_arready : out STD_LOGIC;
    s_axi_tx_clkgen_arvalid : in STD_LOGIC;
    s_axi_tx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_awready : out STD_LOGIC;
    s_axi_tx_clkgen_awvalid : in STD_LOGIC;
    s_axi_tx_clkgen_bready : in STD_LOGIC;
    s_axi_tx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_bvalid : out STD_LOGIC;
    s_axi_tx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_rready : in STD_LOGIC;
    s_axi_tx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_rvalid : out STD_LOGIC;
    s_axi_tx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_wready : out STD_LOGIC;
    s_axi_tx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_clkgen_wvalid : in STD_LOGIC;
    s_axi_tx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_arready : out STD_LOGIC;
    s_axi_tx_jesd_arvalid : in STD_LOGIC;
    s_axi_tx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_awready : out STD_LOGIC;
    s_axi_tx_jesd_awvalid : in STD_LOGIC;
    s_axi_tx_jesd_bready : in STD_LOGIC;
    s_axi_tx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_bvalid : out STD_LOGIC;
    s_axi_tx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_rready : in STD_LOGIC;
    s_axi_tx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_rvalid : out STD_LOGIC;
    s_axi_tx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_wready : out STD_LOGIC;
    s_axi_tx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_jesd_wvalid : in STD_LOGIC;
    s_axi_tx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_arready : out STD_LOGIC;
    s_axi_tx_tpl_arvalid : in STD_LOGIC;
    s_axi_tx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_awready : out STD_LOGIC;
    s_axi_tx_tpl_awvalid : in STD_LOGIC;
    s_axi_tx_tpl_bready : in STD_LOGIC;
    s_axi_tx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_bvalid : out STD_LOGIC;
    s_axi_tx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_rready : in STD_LOGIC;
    s_axi_tx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_rvalid : out STD_LOGIC;
    s_axi_tx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_wready : out STD_LOGIC;
    s_axi_tx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_tpl_wvalid : in STD_LOGIC;
    tx_clkgen_ref : in STD_LOGIC;
    tx_data_0_n : out STD_LOGIC;
    tx_data_0_p : out STD_LOGIC;
    tx_data_1_n : out STD_LOGIC;
    tx_data_1_p : out STD_LOGIC;
    tx_data_2_n : out STD_LOGIC;
    tx_data_2_p : out STD_LOGIC;
    tx_data_3_n : out STD_LOGIC;
    tx_data_3_p : out STD_LOGIC;
    tx_linkclk : out STD_LOGIC;
    tx_ref_clk_0 : in STD_LOGIC;
    tx_sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_sysref_0 : in STD_LOGIC;
    txoutclk : out STD_LOGIC
  );
end system_wrapper;

architecture STRUCTURE of system_wrapper is
  component system is
  port (
    tx_ref_clk_0 : in STD_LOGIC;
    rx_ref_clk_0 : in STD_LOGIC;
    tx_sysref_0 : in STD_LOGIC;
    tx_sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_data_0_p : out STD_LOGIC;
    tx_data_0_n : out STD_LOGIC;
    tx_data_1_p : out STD_LOGIC;
    tx_data_1_n : out STD_LOGIC;
    tx_data_2_p : out STD_LOGIC;
    tx_data_2_n : out STD_LOGIC;
    tx_data_3_p : out STD_LOGIC;
    tx_data_3_n : out STD_LOGIC;
    rx_sysref_0 : in STD_LOGIC;
    rx_sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_data_0_p : in STD_LOGIC;
    rx_data_0_n : in STD_LOGIC;
    rx_data_1_p : in STD_LOGIC;
    rx_data_1_n : in STD_LOGIC;
    rx_data_2_p : in STD_LOGIC;
    rx_data_2_n : in STD_LOGIC;
    rx_data_3_p : in STD_LOGIC;
    rx_data_3_n : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    dac_dunf : in STD_LOGIC;
    dac_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_dovf : in STD_LOGIC;
    adc_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    tx_linkclk : out STD_LOGIC;
    rx_linkclk : out STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    txoutclk : out STD_LOGIC;
    rx_clkgen_ref : in STD_LOGIC;
    tx_clkgen_ref : in STD_LOGIC;
    s_axi_rx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_awvalid : in STD_LOGIC;
    s_axi_rx_tpl_awready : out STD_LOGIC;
    s_axi_rx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_tpl_wvalid : in STD_LOGIC;
    s_axi_rx_tpl_wready : out STD_LOGIC;
    s_axi_rx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_bvalid : out STD_LOGIC;
    s_axi_rx_tpl_bready : in STD_LOGIC;
    s_axi_rx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_arvalid : in STD_LOGIC;
    s_axi_rx_tpl_arready : out STD_LOGIC;
    s_axi_rx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_rvalid : out STD_LOGIC;
    s_axi_rx_tpl_rready : in STD_LOGIC;
    s_axi_tx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_awvalid : in STD_LOGIC;
    s_axi_tx_tpl_awready : out STD_LOGIC;
    s_axi_tx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_tpl_wvalid : in STD_LOGIC;
    s_axi_tx_tpl_wready : out STD_LOGIC;
    s_axi_tx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_bvalid : out STD_LOGIC;
    s_axi_tx_tpl_bready : in STD_LOGIC;
    s_axi_tx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_arvalid : in STD_LOGIC;
    s_axi_tx_tpl_arready : out STD_LOGIC;
    s_axi_tx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_rvalid : out STD_LOGIC;
    s_axi_tx_tpl_rready : in STD_LOGIC;
    s_axi_rx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_awready : out STD_LOGIC;
    s_axi_rx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_wready : out STD_LOGIC;
    s_axi_rx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_bready : in STD_LOGIC;
    s_axi_rx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_arready : out STD_LOGIC;
    s_axi_rx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_rready : in STD_LOGIC;
    s_axi_rx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_awvalid : in STD_LOGIC;
    s_axi_rx_clkgen_awready : out STD_LOGIC;
    s_axi_rx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_clkgen_wvalid : in STD_LOGIC;
    s_axi_rx_clkgen_wready : out STD_LOGIC;
    s_axi_rx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_bvalid : out STD_LOGIC;
    s_axi_rx_clkgen_bready : in STD_LOGIC;
    s_axi_rx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_arvalid : in STD_LOGIC;
    s_axi_rx_clkgen_arready : out STD_LOGIC;
    s_axi_rx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_rvalid : out STD_LOGIC;
    s_axi_rx_clkgen_rready : in STD_LOGIC;
    s_axi_tx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_awvalid : in STD_LOGIC;
    s_axi_tx_jesd_awready : out STD_LOGIC;
    s_axi_tx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_jesd_wvalid : in STD_LOGIC;
    s_axi_tx_jesd_wready : out STD_LOGIC;
    s_axi_tx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_bvalid : out STD_LOGIC;
    s_axi_tx_jesd_bready : in STD_LOGIC;
    s_axi_tx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_arvalid : in STD_LOGIC;
    s_axi_tx_jesd_arready : out STD_LOGIC;
    s_axi_tx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_rvalid : out STD_LOGIC;
    s_axi_tx_jesd_rready : in STD_LOGIC;
    s_axi_tx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_awvalid : in STD_LOGIC;
    s_axi_tx_clkgen_awready : out STD_LOGIC;
    s_axi_tx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_clkgen_wvalid : in STD_LOGIC;
    s_axi_tx_clkgen_wready : out STD_LOGIC;
    s_axi_tx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_bvalid : out STD_LOGIC;
    s_axi_tx_clkgen_bready : in STD_LOGIC;
    s_axi_tx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_arvalid : in STD_LOGIC;
    s_axi_tx_clkgen_arready : out STD_LOGIC;
    s_axi_tx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_rvalid : out STD_LOGIC;
    s_axi_tx_clkgen_rready : in STD_LOGIC;
    s_axi_rx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_awvalid : in STD_LOGIC;
    s_axi_rx_jesd_awready : out STD_LOGIC;
    s_axi_rx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_jesd_wvalid : in STD_LOGIC;
    s_axi_rx_jesd_wready : out STD_LOGIC;
    s_axi_rx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_bvalid : out STD_LOGIC;
    s_axi_rx_jesd_bready : in STD_LOGIC;
    s_axi_rx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_arvalid : in STD_LOGIC;
    s_axi_rx_jesd_arready : out STD_LOGIC;
    s_axi_rx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_rvalid : out STD_LOGIC;
    s_axi_rx_jesd_rready : in STD_LOGIC;
    s_axi_tx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_awready : out STD_LOGIC;
    s_axi_tx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_wready : out STD_LOGIC;
    s_axi_tx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_bready : in STD_LOGIC;
    s_axi_tx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_arready : out STD_LOGIC;
    s_axi_tx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_rready : in STD_LOGIC
  );
  end component system;
begin
system_i: component system
     port map (
      adc_data_0(31 downto 0) => adc_data_0(31 downto 0),
      adc_data_1(31 downto 0) => adc_data_1(31 downto 0),
      adc_data_2(31 downto 0) => adc_data_2(31 downto 0),
      adc_data_3(31 downto 0) => adc_data_3(31 downto 0),
      adc_dovf => adc_dovf,
      adc_enable_0(0) => adc_enable_0(0),
      adc_enable_1(0) => adc_enable_1(0),
      adc_enable_2(0) => adc_enable_2(0),
      adc_enable_3(0) => adc_enable_3(0),
      adc_valid_0(0) => adc_valid_0(0),
      adc_valid_1(0) => adc_valid_1(0),
      adc_valid_2(0) => adc_valid_2(0),
      adc_valid_3(0) => adc_valid_3(0),
      dac_data_0(31 downto 0) => dac_data_0(31 downto 0),
      dac_data_1(31 downto 0) => dac_data_1(31 downto 0),
      dac_data_2(31 downto 0) => dac_data_2(31 downto 0),
      dac_data_3(31 downto 0) => dac_data_3(31 downto 0),
      dac_dunf => dac_dunf,
      dac_enable_0(0) => dac_enable_0(0),
      dac_enable_1(0) => dac_enable_1(0),
      dac_enable_2(0) => dac_enable_2(0),
      dac_enable_3(0) => dac_enable_3(0),
      dac_valid_0(0) => dac_valid_0(0),
      dac_valid_1(0) => dac_valid_1(0),
      dac_valid_2(0) => dac_valid_2(0),
      dac_valid_3(0) => dac_valid_3(0),
      rx_clkgen_ref => rx_clkgen_ref,
      rx_data_0_n => rx_data_0_n,
      rx_data_0_p => rx_data_0_p,
      rx_data_1_n => rx_data_1_n,
      rx_data_1_p => rx_data_1_p,
      rx_data_2_n => rx_data_2_n,
      rx_data_2_p => rx_data_2_p,
      rx_data_3_n => rx_data_3_n,
      rx_data_3_p => rx_data_3_p,
      rx_linkclk => rx_linkclk,
      rx_ref_clk_0 => rx_ref_clk_0,
      rx_sync(0) => rx_sync(0),
      rx_sysref_0 => rx_sysref_0,
      rxoutclk => rxoutclk,
      s_axi_aclk => s_axi_aclk,
      s_axi_aresetn => s_axi_aresetn,
      s_axi_rx_adxcvr_araddr(15 downto 0) => s_axi_rx_adxcvr_araddr(15 downto 0),
      s_axi_rx_adxcvr_arprot(2 downto 0) => s_axi_rx_adxcvr_arprot(2 downto 0),
      s_axi_rx_adxcvr_arready => s_axi_rx_adxcvr_arready,
      s_axi_rx_adxcvr_arvalid => s_axi_rx_adxcvr_arvalid,
      s_axi_rx_adxcvr_awaddr(15 downto 0) => s_axi_rx_adxcvr_awaddr(15 downto 0),
      s_axi_rx_adxcvr_awprot(2 downto 0) => s_axi_rx_adxcvr_awprot(2 downto 0),
      s_axi_rx_adxcvr_awready => s_axi_rx_adxcvr_awready,
      s_axi_rx_adxcvr_awvalid => s_axi_rx_adxcvr_awvalid,
      s_axi_rx_adxcvr_bready => s_axi_rx_adxcvr_bready,
      s_axi_rx_adxcvr_bresp(1 downto 0) => s_axi_rx_adxcvr_bresp(1 downto 0),
      s_axi_rx_adxcvr_bvalid => s_axi_rx_adxcvr_bvalid,
      s_axi_rx_adxcvr_rdata(31 downto 0) => s_axi_rx_adxcvr_rdata(31 downto 0),
      s_axi_rx_adxcvr_rready => s_axi_rx_adxcvr_rready,
      s_axi_rx_adxcvr_rresp(1 downto 0) => s_axi_rx_adxcvr_rresp(1 downto 0),
      s_axi_rx_adxcvr_rvalid => s_axi_rx_adxcvr_rvalid,
      s_axi_rx_adxcvr_wdata(31 downto 0) => s_axi_rx_adxcvr_wdata(31 downto 0),
      s_axi_rx_adxcvr_wready => s_axi_rx_adxcvr_wready,
      s_axi_rx_adxcvr_wstrb(3 downto 0) => s_axi_rx_adxcvr_wstrb(3 downto 0),
      s_axi_rx_adxcvr_wvalid => s_axi_rx_adxcvr_wvalid,
      s_axi_rx_clkgen_araddr(15 downto 0) => s_axi_rx_clkgen_araddr(15 downto 0),
      s_axi_rx_clkgen_arprot(2 downto 0) => s_axi_rx_clkgen_arprot(2 downto 0),
      s_axi_rx_clkgen_arready => s_axi_rx_clkgen_arready,
      s_axi_rx_clkgen_arvalid => s_axi_rx_clkgen_arvalid,
      s_axi_rx_clkgen_awaddr(15 downto 0) => s_axi_rx_clkgen_awaddr(15 downto 0),
      s_axi_rx_clkgen_awprot(2 downto 0) => s_axi_rx_clkgen_awprot(2 downto 0),
      s_axi_rx_clkgen_awready => s_axi_rx_clkgen_awready,
      s_axi_rx_clkgen_awvalid => s_axi_rx_clkgen_awvalid,
      s_axi_rx_clkgen_bready => s_axi_rx_clkgen_bready,
      s_axi_rx_clkgen_bresp(1 downto 0) => s_axi_rx_clkgen_bresp(1 downto 0),
      s_axi_rx_clkgen_bvalid => s_axi_rx_clkgen_bvalid,
      s_axi_rx_clkgen_rdata(31 downto 0) => s_axi_rx_clkgen_rdata(31 downto 0),
      s_axi_rx_clkgen_rready => s_axi_rx_clkgen_rready,
      s_axi_rx_clkgen_rresp(1 downto 0) => s_axi_rx_clkgen_rresp(1 downto 0),
      s_axi_rx_clkgen_rvalid => s_axi_rx_clkgen_rvalid,
      s_axi_rx_clkgen_wdata(31 downto 0) => s_axi_rx_clkgen_wdata(31 downto 0),
      s_axi_rx_clkgen_wready => s_axi_rx_clkgen_wready,
      s_axi_rx_clkgen_wstrb(3 downto 0) => s_axi_rx_clkgen_wstrb(3 downto 0),
      s_axi_rx_clkgen_wvalid => s_axi_rx_clkgen_wvalid,
      s_axi_rx_jesd_araddr(13 downto 0) => s_axi_rx_jesd_araddr(13 downto 0),
      s_axi_rx_jesd_arprot(2 downto 0) => s_axi_rx_jesd_arprot(2 downto 0),
      s_axi_rx_jesd_arready => s_axi_rx_jesd_arready,
      s_axi_rx_jesd_arvalid => s_axi_rx_jesd_arvalid,
      s_axi_rx_jesd_awaddr(13 downto 0) => s_axi_rx_jesd_awaddr(13 downto 0),
      s_axi_rx_jesd_awprot(2 downto 0) => s_axi_rx_jesd_awprot(2 downto 0),
      s_axi_rx_jesd_awready => s_axi_rx_jesd_awready,
      s_axi_rx_jesd_awvalid => s_axi_rx_jesd_awvalid,
      s_axi_rx_jesd_bready => s_axi_rx_jesd_bready,
      s_axi_rx_jesd_bresp(1 downto 0) => s_axi_rx_jesd_bresp(1 downto 0),
      s_axi_rx_jesd_bvalid => s_axi_rx_jesd_bvalid,
      s_axi_rx_jesd_rdata(31 downto 0) => s_axi_rx_jesd_rdata(31 downto 0),
      s_axi_rx_jesd_rready => s_axi_rx_jesd_rready,
      s_axi_rx_jesd_rresp(1 downto 0) => s_axi_rx_jesd_rresp(1 downto 0),
      s_axi_rx_jesd_rvalid => s_axi_rx_jesd_rvalid,
      s_axi_rx_jesd_wdata(31 downto 0) => s_axi_rx_jesd_wdata(31 downto 0),
      s_axi_rx_jesd_wready => s_axi_rx_jesd_wready,
      s_axi_rx_jesd_wstrb(3 downto 0) => s_axi_rx_jesd_wstrb(3 downto 0),
      s_axi_rx_jesd_wvalid => s_axi_rx_jesd_wvalid,
      s_axi_rx_tpl_araddr(12 downto 0) => s_axi_rx_tpl_araddr(12 downto 0),
      s_axi_rx_tpl_arprot(2 downto 0) => s_axi_rx_tpl_arprot(2 downto 0),
      s_axi_rx_tpl_arready => s_axi_rx_tpl_arready,
      s_axi_rx_tpl_arvalid => s_axi_rx_tpl_arvalid,
      s_axi_rx_tpl_awaddr(12 downto 0) => s_axi_rx_tpl_awaddr(12 downto 0),
      s_axi_rx_tpl_awprot(2 downto 0) => s_axi_rx_tpl_awprot(2 downto 0),
      s_axi_rx_tpl_awready => s_axi_rx_tpl_awready,
      s_axi_rx_tpl_awvalid => s_axi_rx_tpl_awvalid,
      s_axi_rx_tpl_bready => s_axi_rx_tpl_bready,
      s_axi_rx_tpl_bresp(1 downto 0) => s_axi_rx_tpl_bresp(1 downto 0),
      s_axi_rx_tpl_bvalid => s_axi_rx_tpl_bvalid,
      s_axi_rx_tpl_rdata(31 downto 0) => s_axi_rx_tpl_rdata(31 downto 0),
      s_axi_rx_tpl_rready => s_axi_rx_tpl_rready,
      s_axi_rx_tpl_rresp(1 downto 0) => s_axi_rx_tpl_rresp(1 downto 0),
      s_axi_rx_tpl_rvalid => s_axi_rx_tpl_rvalid,
      s_axi_rx_tpl_wdata(31 downto 0) => s_axi_rx_tpl_wdata(31 downto 0),
      s_axi_rx_tpl_wready => s_axi_rx_tpl_wready,
      s_axi_rx_tpl_wstrb(3 downto 0) => s_axi_rx_tpl_wstrb(3 downto 0),
      s_axi_rx_tpl_wvalid => s_axi_rx_tpl_wvalid,
      s_axi_tx_adxcvr_araddr(15 downto 0) => s_axi_tx_adxcvr_araddr(15 downto 0),
      s_axi_tx_adxcvr_arprot(2 downto 0) => s_axi_tx_adxcvr_arprot(2 downto 0),
      s_axi_tx_adxcvr_arready => s_axi_tx_adxcvr_arready,
      s_axi_tx_adxcvr_arvalid => s_axi_tx_adxcvr_arvalid,
      s_axi_tx_adxcvr_awaddr(15 downto 0) => s_axi_tx_adxcvr_awaddr(15 downto 0),
      s_axi_tx_adxcvr_awprot(2 downto 0) => s_axi_tx_adxcvr_awprot(2 downto 0),
      s_axi_tx_adxcvr_awready => s_axi_tx_adxcvr_awready,
      s_axi_tx_adxcvr_awvalid => s_axi_tx_adxcvr_awvalid,
      s_axi_tx_adxcvr_bready => s_axi_tx_adxcvr_bready,
      s_axi_tx_adxcvr_bresp(1 downto 0) => s_axi_tx_adxcvr_bresp(1 downto 0),
      s_axi_tx_adxcvr_bvalid => s_axi_tx_adxcvr_bvalid,
      s_axi_tx_adxcvr_rdata(31 downto 0) => s_axi_tx_adxcvr_rdata(31 downto 0),
      s_axi_tx_adxcvr_rready => s_axi_tx_adxcvr_rready,
      s_axi_tx_adxcvr_rresp(1 downto 0) => s_axi_tx_adxcvr_rresp(1 downto 0),
      s_axi_tx_adxcvr_rvalid => s_axi_tx_adxcvr_rvalid,
      s_axi_tx_adxcvr_wdata(31 downto 0) => s_axi_tx_adxcvr_wdata(31 downto 0),
      s_axi_tx_adxcvr_wready => s_axi_tx_adxcvr_wready,
      s_axi_tx_adxcvr_wstrb(3 downto 0) => s_axi_tx_adxcvr_wstrb(3 downto 0),
      s_axi_tx_adxcvr_wvalid => s_axi_tx_adxcvr_wvalid,
      s_axi_tx_clkgen_araddr(15 downto 0) => s_axi_tx_clkgen_araddr(15 downto 0),
      s_axi_tx_clkgen_arprot(2 downto 0) => s_axi_tx_clkgen_arprot(2 downto 0),
      s_axi_tx_clkgen_arready => s_axi_tx_clkgen_arready,
      s_axi_tx_clkgen_arvalid => s_axi_tx_clkgen_arvalid,
      s_axi_tx_clkgen_awaddr(15 downto 0) => s_axi_tx_clkgen_awaddr(15 downto 0),
      s_axi_tx_clkgen_awprot(2 downto 0) => s_axi_tx_clkgen_awprot(2 downto 0),
      s_axi_tx_clkgen_awready => s_axi_tx_clkgen_awready,
      s_axi_tx_clkgen_awvalid => s_axi_tx_clkgen_awvalid,
      s_axi_tx_clkgen_bready => s_axi_tx_clkgen_bready,
      s_axi_tx_clkgen_bresp(1 downto 0) => s_axi_tx_clkgen_bresp(1 downto 0),
      s_axi_tx_clkgen_bvalid => s_axi_tx_clkgen_bvalid,
      s_axi_tx_clkgen_rdata(31 downto 0) => s_axi_tx_clkgen_rdata(31 downto 0),
      s_axi_tx_clkgen_rready => s_axi_tx_clkgen_rready,
      s_axi_tx_clkgen_rresp(1 downto 0) => s_axi_tx_clkgen_rresp(1 downto 0),
      s_axi_tx_clkgen_rvalid => s_axi_tx_clkgen_rvalid,
      s_axi_tx_clkgen_wdata(31 downto 0) => s_axi_tx_clkgen_wdata(31 downto 0),
      s_axi_tx_clkgen_wready => s_axi_tx_clkgen_wready,
      s_axi_tx_clkgen_wstrb(3 downto 0) => s_axi_tx_clkgen_wstrb(3 downto 0),
      s_axi_tx_clkgen_wvalid => s_axi_tx_clkgen_wvalid,
      s_axi_tx_jesd_araddr(13 downto 0) => s_axi_tx_jesd_araddr(13 downto 0),
      s_axi_tx_jesd_arprot(2 downto 0) => s_axi_tx_jesd_arprot(2 downto 0),
      s_axi_tx_jesd_arready => s_axi_tx_jesd_arready,
      s_axi_tx_jesd_arvalid => s_axi_tx_jesd_arvalid,
      s_axi_tx_jesd_awaddr(13 downto 0) => s_axi_tx_jesd_awaddr(13 downto 0),
      s_axi_tx_jesd_awprot(2 downto 0) => s_axi_tx_jesd_awprot(2 downto 0),
      s_axi_tx_jesd_awready => s_axi_tx_jesd_awready,
      s_axi_tx_jesd_awvalid => s_axi_tx_jesd_awvalid,
      s_axi_tx_jesd_bready => s_axi_tx_jesd_bready,
      s_axi_tx_jesd_bresp(1 downto 0) => s_axi_tx_jesd_bresp(1 downto 0),
      s_axi_tx_jesd_bvalid => s_axi_tx_jesd_bvalid,
      s_axi_tx_jesd_rdata(31 downto 0) => s_axi_tx_jesd_rdata(31 downto 0),
      s_axi_tx_jesd_rready => s_axi_tx_jesd_rready,
      s_axi_tx_jesd_rresp(1 downto 0) => s_axi_tx_jesd_rresp(1 downto 0),
      s_axi_tx_jesd_rvalid => s_axi_tx_jesd_rvalid,
      s_axi_tx_jesd_wdata(31 downto 0) => s_axi_tx_jesd_wdata(31 downto 0),
      s_axi_tx_jesd_wready => s_axi_tx_jesd_wready,
      s_axi_tx_jesd_wstrb(3 downto 0) => s_axi_tx_jesd_wstrb(3 downto 0),
      s_axi_tx_jesd_wvalid => s_axi_tx_jesd_wvalid,
      s_axi_tx_tpl_araddr(12 downto 0) => s_axi_tx_tpl_araddr(12 downto 0),
      s_axi_tx_tpl_arprot(2 downto 0) => s_axi_tx_tpl_arprot(2 downto 0),
      s_axi_tx_tpl_arready => s_axi_tx_tpl_arready,
      s_axi_tx_tpl_arvalid => s_axi_tx_tpl_arvalid,
      s_axi_tx_tpl_awaddr(12 downto 0) => s_axi_tx_tpl_awaddr(12 downto 0),
      s_axi_tx_tpl_awprot(2 downto 0) => s_axi_tx_tpl_awprot(2 downto 0),
      s_axi_tx_tpl_awready => s_axi_tx_tpl_awready,
      s_axi_tx_tpl_awvalid => s_axi_tx_tpl_awvalid,
      s_axi_tx_tpl_bready => s_axi_tx_tpl_bready,
      s_axi_tx_tpl_bresp(1 downto 0) => s_axi_tx_tpl_bresp(1 downto 0),
      s_axi_tx_tpl_bvalid => s_axi_tx_tpl_bvalid,
      s_axi_tx_tpl_rdata(31 downto 0) => s_axi_tx_tpl_rdata(31 downto 0),
      s_axi_tx_tpl_rready => s_axi_tx_tpl_rready,
      s_axi_tx_tpl_rresp(1 downto 0) => s_axi_tx_tpl_rresp(1 downto 0),
      s_axi_tx_tpl_rvalid => s_axi_tx_tpl_rvalid,
      s_axi_tx_tpl_wdata(31 downto 0) => s_axi_tx_tpl_wdata(31 downto 0),
      s_axi_tx_tpl_wready => s_axi_tx_tpl_wready,
      s_axi_tx_tpl_wstrb(3 downto 0) => s_axi_tx_tpl_wstrb(3 downto 0),
      s_axi_tx_tpl_wvalid => s_axi_tx_tpl_wvalid,
      tx_clkgen_ref => tx_clkgen_ref,
      tx_data_0_n => tx_data_0_n,
      tx_data_0_p => tx_data_0_p,
      tx_data_1_n => tx_data_1_n,
      tx_data_1_p => tx_data_1_p,
      tx_data_2_n => tx_data_2_n,
      tx_data_2_p => tx_data_2_p,
      tx_data_3_n => tx_data_3_n,
      tx_data_3_p => tx_data_3_p,
      tx_linkclk => tx_linkclk,
      tx_ref_clk_0 => tx_ref_clk_0,
      tx_sync(0) => tx_sync(0),
      tx_sysref_0 => tx_sysref_0,
      txoutclk => txoutclk
    );
end STRUCTURE;
