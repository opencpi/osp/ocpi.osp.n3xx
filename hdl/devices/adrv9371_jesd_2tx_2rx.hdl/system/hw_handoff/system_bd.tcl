
################################################################
# This is a generated script based on design: system
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source system_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z045ffg900-2
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name system

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: tx_ad9371_tpl_core
proc create_hier_cell_tx_ad9371_tpl_core { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_tx_ad9371_tpl_core() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 link

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi


  # Create pins
  create_bd_pin -dir I -from 31 -to 0 dac_data_0
  create_bd_pin -dir I -from 31 -to 0 dac_data_1
  create_bd_pin -dir I -from 31 -to 0 dac_data_2
  create_bd_pin -dir I -from 31 -to 0 dac_data_3
  create_bd_pin -dir I dac_dunf
  create_bd_pin -dir O -from 0 -to 0 dac_enable_0
  create_bd_pin -dir O -from 0 -to 0 dac_enable_1
  create_bd_pin -dir O -from 0 -to 0 dac_enable_2
  create_bd_pin -dir O -from 0 -to 0 dac_enable_3
  create_bd_pin -dir O -from 0 -to 0 dac_valid_0
  create_bd_pin -dir O -from 0 -to 0 dac_valid_1
  create_bd_pin -dir O -from 0 -to 0 dac_valid_2
  create_bd_pin -dir O -from 0 -to 0 dac_valid_3
  create_bd_pin -dir I -type clk link_clk
  create_bd_pin -dir I -type clk s_axi_aclk
  create_bd_pin -dir I -type rst s_axi_aresetn

  # Create instance: dac_tpl_core, and set properties
  set dac_tpl_core [ create_bd_cell -type ip -vlnv analog.com:user:ad_ip_jesd204_tpl_dac:1.0 dac_tpl_core ]
  set_property -dict [ list \
   CONFIG.BITS_PER_SAMPLE {16} \
   CONFIG.CONVERTER_RESOLUTION {16} \
   CONFIG.DATAPATH_DISABLE {1} \
   CONFIG.NUM_CHANNELS {4} \
   CONFIG.NUM_LANES {4} \
   CONFIG.OCTETS_PER_BEAT {4} \
   CONFIG.SAMPLES_PER_FRAME {1} \
 ] $dac_tpl_core

  # Create instance: data_concat0, and set properties
  set data_concat0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 data_concat0 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {4} \
 ] $data_concat0

  # Create instance: enable_slice_0, and set properties
  set enable_slice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_0

  # Create instance: enable_slice_1, and set properties
  set enable_slice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_1

  # Create instance: enable_slice_2, and set properties
  set enable_slice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_2

  # Create instance: enable_slice_3, and set properties
  set enable_slice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_3 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {3} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_3

  # Create instance: valid_slice_0, and set properties
  set valid_slice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_0

  # Create instance: valid_slice_1, and set properties
  set valid_slice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_1

  # Create instance: valid_slice_2, and set properties
  set valid_slice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_2

  # Create instance: valid_slice_3, and set properties
  set valid_slice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_3 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {3} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_3

  # Create interface connections
  connect_bd_intf_net -intf_net dac_tpl_core_link [get_bd_intf_pins link] [get_bd_intf_pins dac_tpl_core/link]
  connect_bd_intf_net -intf_net s_axi_1 [get_bd_intf_pins s_axi] [get_bd_intf_pins dac_tpl_core/s_axi]

  # Create port connections
  connect_bd_net -net dac_data_0_1 [get_bd_pins dac_data_0] [get_bd_pins data_concat0/In0]
  connect_bd_net -net dac_data_1_1 [get_bd_pins dac_data_1] [get_bd_pins data_concat0/In1]
  connect_bd_net -net dac_data_2_1 [get_bd_pins dac_data_2] [get_bd_pins data_concat0/In2]
  connect_bd_net -net dac_data_3_1 [get_bd_pins dac_data_3] [get_bd_pins data_concat0/In3]
  connect_bd_net -net dac_dunf_1 [get_bd_pins dac_dunf] [get_bd_pins dac_tpl_core/dac_dunf]
  connect_bd_net -net dac_tpl_core_dac_valid [get_bd_pins dac_tpl_core/dac_valid] [get_bd_pins valid_slice_0/Din] [get_bd_pins valid_slice_1/Din] [get_bd_pins valid_slice_2/Din] [get_bd_pins valid_slice_3/Din]
  connect_bd_net -net dac_tpl_core_enable [get_bd_pins dac_tpl_core/enable] [get_bd_pins enable_slice_0/Din] [get_bd_pins enable_slice_1/Din] [get_bd_pins enable_slice_2/Din] [get_bd_pins enable_slice_3/Din]
  connect_bd_net -net data_concat0_dout [get_bd_pins dac_tpl_core/dac_ddata] [get_bd_pins data_concat0/dout]
  connect_bd_net -net enable_slice_0_Dout [get_bd_pins dac_enable_0] [get_bd_pins enable_slice_0/Dout]
  connect_bd_net -net enable_slice_1_Dout [get_bd_pins dac_enable_1] [get_bd_pins enable_slice_1/Dout]
  connect_bd_net -net enable_slice_2_Dout [get_bd_pins dac_enable_2] [get_bd_pins enable_slice_2/Dout]
  connect_bd_net -net enable_slice_3_Dout [get_bd_pins dac_enable_3] [get_bd_pins enable_slice_3/Dout]
  connect_bd_net -net link_clk_1 [get_bd_pins link_clk] [get_bd_pins dac_tpl_core/link_clk]
  connect_bd_net -net s_axi_aclk_1 [get_bd_pins s_axi_aclk] [get_bd_pins dac_tpl_core/s_axi_aclk]
  connect_bd_net -net s_axi_aresetn_1 [get_bd_pins s_axi_aresetn] [get_bd_pins dac_tpl_core/s_axi_aresetn]
  connect_bd_net -net valid_slice_0_Dout [get_bd_pins dac_valid_0] [get_bd_pins valid_slice_0/Dout]
  connect_bd_net -net valid_slice_1_Dout [get_bd_pins dac_valid_1] [get_bd_pins valid_slice_1/Dout]
  connect_bd_net -net valid_slice_2_Dout [get_bd_pins dac_valid_2] [get_bd_pins valid_slice_2/Dout]
  connect_bd_net -net valid_slice_3_Dout [get_bd_pins dac_valid_3] [get_bd_pins valid_slice_3/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: rx_ad9371_tpl_core
proc create_hier_cell_rx_ad9371_tpl_core { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_rx_ad9371_tpl_core() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi


  # Create pins
  create_bd_pin -dir O -from 31 -to 0 adc_data_0
  create_bd_pin -dir O -from 31 -to 0 adc_data_1
  create_bd_pin -dir O -from 31 -to 0 adc_data_2
  create_bd_pin -dir O -from 31 -to 0 adc_data_3
  create_bd_pin -dir I adc_dovf
  create_bd_pin -dir O -from 0 -to 0 adc_enable_0
  create_bd_pin -dir O -from 0 -to 0 adc_enable_1
  create_bd_pin -dir O -from 0 -to 0 adc_enable_2
  create_bd_pin -dir O -from 0 -to 0 adc_enable_3
  create_bd_pin -dir O -from 0 -to 0 adc_valid_0
  create_bd_pin -dir O -from 0 -to 0 adc_valid_1
  create_bd_pin -dir O -from 0 -to 0 adc_valid_2
  create_bd_pin -dir O -from 0 -to 0 adc_valid_3
  create_bd_pin -dir I -type clk link_clk
  create_bd_pin -dir I -from 127 -to 0 link_data
  create_bd_pin -dir I -from 3 -to 0 link_sof
  create_bd_pin -dir I link_valid
  create_bd_pin -dir I -type clk s_axi_aclk
  create_bd_pin -dir I -type rst s_axi_aresetn

  # Create instance: adc_tpl_core, and set properties
  set adc_tpl_core [ create_bd_cell -type ip -vlnv analog.com:user:ad_ip_jesd204_tpl_adc:1.0 adc_tpl_core ]
  set_property -dict [ list \
   CONFIG.BITS_PER_SAMPLE {16} \
   CONFIG.CONVERTER_RESOLUTION {16} \
   CONFIG.NUM_CHANNELS {4} \
   CONFIG.NUM_LANES {4} \
   CONFIG.OCTETS_PER_BEAT {4} \
   CONFIG.SAMPLES_PER_FRAME {1} \
 ] $adc_tpl_core

  # Create instance: data_slice_0, and set properties
  set data_slice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 data_slice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {31} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {128} \
   CONFIG.DOUT_WIDTH {32} \
 ] $data_slice_0

  # Create instance: data_slice_1, and set properties
  set data_slice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 data_slice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {63} \
   CONFIG.DIN_TO {32} \
   CONFIG.DIN_WIDTH {128} \
   CONFIG.DOUT_WIDTH {32} \
 ] $data_slice_1

  # Create instance: data_slice_2, and set properties
  set data_slice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 data_slice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {95} \
   CONFIG.DIN_TO {64} \
   CONFIG.DIN_WIDTH {128} \
   CONFIG.DOUT_WIDTH {32} \
 ] $data_slice_2

  # Create instance: data_slice_3, and set properties
  set data_slice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 data_slice_3 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {127} \
   CONFIG.DIN_TO {96} \
   CONFIG.DIN_WIDTH {128} \
   CONFIG.DOUT_WIDTH {32} \
 ] $data_slice_3

  # Create instance: enable_slice_0, and set properties
  set enable_slice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_0

  # Create instance: enable_slice_1, and set properties
  set enable_slice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_1

  # Create instance: enable_slice_2, and set properties
  set enable_slice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_2

  # Create instance: enable_slice_3, and set properties
  set enable_slice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 enable_slice_3 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {3} \
   CONFIG.DIN_WIDTH {4} \
 ] $enable_slice_3

  # Create instance: valid_slice_0, and set properties
  set valid_slice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_0

  # Create instance: valid_slice_1, and set properties
  set valid_slice_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_1 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_1

  # Create instance: valid_slice_2, and set properties
  set valid_slice_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_2 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_2

  # Create instance: valid_slice_3, and set properties
  set valid_slice_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 valid_slice_3 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {3} \
   CONFIG.DIN_WIDTH {4} \
 ] $valid_slice_3

  # Create interface connections
  connect_bd_intf_net -intf_net s_axi_1 [get_bd_intf_pins s_axi] [get_bd_intf_pins adc_tpl_core/s_axi]

  # Create port connections
  connect_bd_net -net adc_dovf_1 [get_bd_pins adc_dovf] [get_bd_pins adc_tpl_core/adc_dovf]
  connect_bd_net -net adc_tpl_core_adc_data [get_bd_pins adc_tpl_core/adc_data] [get_bd_pins data_slice_0/Din] [get_bd_pins data_slice_1/Din] [get_bd_pins data_slice_2/Din] [get_bd_pins data_slice_3/Din]
  connect_bd_net -net adc_tpl_core_adc_valid [get_bd_pins adc_tpl_core/adc_valid] [get_bd_pins valid_slice_0/Din] [get_bd_pins valid_slice_1/Din] [get_bd_pins valid_slice_2/Din] [get_bd_pins valid_slice_3/Din]
  connect_bd_net -net adc_tpl_core_enable [get_bd_pins adc_tpl_core/enable] [get_bd_pins enable_slice_0/Din] [get_bd_pins enable_slice_1/Din] [get_bd_pins enable_slice_2/Din] [get_bd_pins enable_slice_3/Din]
  connect_bd_net -net data_slice_0_Dout [get_bd_pins adc_data_0] [get_bd_pins data_slice_0/Dout]
  connect_bd_net -net data_slice_1_Dout [get_bd_pins adc_data_1] [get_bd_pins data_slice_1/Dout]
  connect_bd_net -net data_slice_2_Dout [get_bd_pins adc_data_2] [get_bd_pins data_slice_2/Dout]
  connect_bd_net -net data_slice_3_Dout [get_bd_pins adc_data_3] [get_bd_pins data_slice_3/Dout]
  connect_bd_net -net enable_slice_0_Dout [get_bd_pins adc_enable_0] [get_bd_pins enable_slice_0/Dout]
  connect_bd_net -net enable_slice_1_Dout [get_bd_pins adc_enable_1] [get_bd_pins enable_slice_1/Dout]
  connect_bd_net -net enable_slice_2_Dout [get_bd_pins adc_enable_2] [get_bd_pins enable_slice_2/Dout]
  connect_bd_net -net enable_slice_3_Dout [get_bd_pins adc_enable_3] [get_bd_pins enable_slice_3/Dout]
  connect_bd_net -net link_clk_1 [get_bd_pins link_clk] [get_bd_pins adc_tpl_core/link_clk]
  connect_bd_net -net link_data_1 [get_bd_pins link_data] [get_bd_pins adc_tpl_core/link_data]
  connect_bd_net -net link_sof_1 [get_bd_pins link_sof] [get_bd_pins adc_tpl_core/link_sof]
  connect_bd_net -net link_valid_1 [get_bd_pins link_valid] [get_bd_pins adc_tpl_core/link_valid]
  connect_bd_net -net s_axi_aclk_1 [get_bd_pins s_axi_aclk] [get_bd_pins adc_tpl_core/s_axi_aclk]
  connect_bd_net -net s_axi_aresetn_1 [get_bd_pins s_axi_aresetn] [get_bd_pins adc_tpl_core/s_axi_aresetn]
  connect_bd_net -net valid_slice_0_Dout [get_bd_pins adc_valid_0] [get_bd_pins valid_slice_0/Dout]
  connect_bd_net -net valid_slice_1_Dout [get_bd_pins adc_valid_1] [get_bd_pins valid_slice_1/Dout]
  connect_bd_net -net valid_slice_2_Dout [get_bd_pins adc_valid_2] [get_bd_pins valid_slice_2/Dout]
  connect_bd_net -net valid_slice_3_Dout [get_bd_pins adc_valid_3] [get_bd_pins valid_slice_3/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: axi_ad9371_tx_jesd
proc create_hier_cell_axi_ad9371_tx_jesd { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_axi_ad9371_tx_jesd() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 tx_data

  create_bd_intf_pin -mode Master -vlnv xilinx.com:display_jesd204:jesd204_tx_bus_rtl:1.0 tx_phy0

  create_bd_intf_pin -mode Master -vlnv xilinx.com:display_jesd204:jesd204_tx_bus_rtl:1.0 tx_phy1

  create_bd_intf_pin -mode Master -vlnv xilinx.com:display_jesd204:jesd204_tx_bus_rtl:1.0 tx_phy2

  create_bd_intf_pin -mode Master -vlnv xilinx.com:display_jesd204:jesd204_tx_bus_rtl:1.0 tx_phy3


  # Create pins
  create_bd_pin -dir I -type clk device_clk
  create_bd_pin -dir O -type intr irq
  create_bd_pin -dir I -type clk s_axi_aclk
  create_bd_pin -dir I -type rst s_axi_aresetn
  create_bd_pin -dir I -from 0 -to 0 sync
  create_bd_pin -dir I sysref

  # Create instance: tx, and set properties
  set tx [ create_bd_cell -type ip -vlnv analog.com:user:jesd204_tx:1.0 tx ]
  set_property -dict [ list \
   CONFIG.LINK_MODE {1} \
   CONFIG.NUM_LANES {4} \
   CONFIG.NUM_LINKS {1} \
 ] $tx

  # Create instance: tx_axi, and set properties
  set tx_axi [ create_bd_cell -type ip -vlnv analog.com:user:axi_jesd204_tx:1.0 tx_axi ]
  set_property -dict [ list \
   CONFIG.LINK_MODE {1} \
   CONFIG.NUM_LANES {4} \
   CONFIG.NUM_LINKS {1} \
 ] $tx_axi

  # Create interface connections
  connect_bd_intf_net -intf_net s_axi_1 [get_bd_intf_pins s_axi] [get_bd_intf_pins tx_axi/s_axi]
  connect_bd_intf_net -intf_net tx_axi_tx_cfg [get_bd_intf_pins tx/tx_cfg] [get_bd_intf_pins tx_axi/tx_cfg]
  connect_bd_intf_net -intf_net tx_axi_tx_ctrl [get_bd_intf_pins tx/tx_ctrl] [get_bd_intf_pins tx_axi/tx_ctrl]
  connect_bd_intf_net -intf_net tx_data_1 [get_bd_intf_pins tx_data] [get_bd_intf_pins tx/tx_data]
  connect_bd_intf_net -intf_net tx_tx_event [get_bd_intf_pins tx/tx_event] [get_bd_intf_pins tx_axi/tx_event]
  connect_bd_intf_net -intf_net tx_tx_ilas_config [get_bd_intf_pins tx/tx_ilas_config] [get_bd_intf_pins tx_axi/tx_ilas_config]
  connect_bd_intf_net -intf_net tx_tx_phy0 [get_bd_intf_pins tx_phy0] [get_bd_intf_pins tx/tx_phy0]
  connect_bd_intf_net -intf_net tx_tx_phy1 [get_bd_intf_pins tx_phy1] [get_bd_intf_pins tx/tx_phy1]
  connect_bd_intf_net -intf_net tx_tx_phy2 [get_bd_intf_pins tx_phy2] [get_bd_intf_pins tx/tx_phy2]
  connect_bd_intf_net -intf_net tx_tx_phy3 [get_bd_intf_pins tx_phy3] [get_bd_intf_pins tx/tx_phy3]
  connect_bd_intf_net -intf_net tx_tx_status [get_bd_intf_pins tx/tx_status] [get_bd_intf_pins tx_axi/tx_status]

  # Create port connections
  connect_bd_net -net device_clk_1 [get_bd_pins device_clk] [get_bd_pins tx/clk] [get_bd_pins tx_axi/core_clk]
  connect_bd_net -net s_axi_aclk_1 [get_bd_pins s_axi_aclk] [get_bd_pins tx_axi/s_axi_aclk]
  connect_bd_net -net s_axi_aresetn_1 [get_bd_pins s_axi_aresetn] [get_bd_pins tx_axi/s_axi_aresetn]
  connect_bd_net -net sync_1 [get_bd_pins sync] [get_bd_pins tx/sync]
  connect_bd_net -net sysref_1 [get_bd_pins sysref] [get_bd_pins tx/sysref]
  connect_bd_net -net tx_axi_core_reset [get_bd_pins tx/reset] [get_bd_pins tx_axi/core_reset]
  connect_bd_net -net tx_axi_irq [get_bd_pins irq] [get_bd_pins tx_axi/irq]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: axi_ad9371_rx_jesd
proc create_hier_cell_axi_ad9371_rx_jesd { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_axi_ad9371_rx_jesd() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:display_jesd204:jesd204_rx_bus_rtl:1.0 rx_phy0

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:display_jesd204:jesd204_rx_bus_rtl:1.0 rx_phy1

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:display_jesd204:jesd204_rx_bus_rtl:1.0 rx_phy2

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:display_jesd204:jesd204_rx_bus_rtl:1.0 rx_phy3

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi


  # Create pins
  create_bd_pin -dir I -type clk device_clk
  create_bd_pin -dir O -type intr irq
  create_bd_pin -dir O phy_en_char_align
  create_bd_pin -dir O -from 127 -to 0 rx_data_tdata
  create_bd_pin -dir O rx_data_tvalid
  create_bd_pin -dir O -from 3 -to 0 rx_eof
  create_bd_pin -dir O -from 3 -to 0 rx_sof
  create_bd_pin -dir I -type clk s_axi_aclk
  create_bd_pin -dir I -type rst s_axi_aresetn
  create_bd_pin -dir O -from 0 -to 0 sync
  create_bd_pin -dir I sysref

  # Create instance: rx, and set properties
  set rx [ create_bd_cell -type ip -vlnv analog.com:user:jesd204_rx:1.0 rx ]
  set_property -dict [ list \
   CONFIG.LINK_MODE {1} \
   CONFIG.NUM_LANES {4} \
   CONFIG.NUM_LINKS {1} \
 ] $rx

  # Create instance: rx_axi, and set properties
  set rx_axi [ create_bd_cell -type ip -vlnv analog.com:user:axi_jesd204_rx:1.0 rx_axi ]
  set_property -dict [ list \
   CONFIG.LINK_MODE {1} \
   CONFIG.NUM_LANES {4} \
   CONFIG.NUM_LINKS {1} \
 ] $rx_axi

  # Create interface connections
  connect_bd_intf_net -intf_net rx_axi_rx_cfg [get_bd_intf_pins rx/rx_cfg] [get_bd_intf_pins rx_axi/rx_cfg]
  connect_bd_intf_net -intf_net rx_phy0_1 [get_bd_intf_pins rx_phy0] [get_bd_intf_pins rx/rx_phy0]
  connect_bd_intf_net -intf_net rx_phy1_1 [get_bd_intf_pins rx_phy1] [get_bd_intf_pins rx/rx_phy1]
  connect_bd_intf_net -intf_net rx_phy2_1 [get_bd_intf_pins rx_phy2] [get_bd_intf_pins rx/rx_phy2]
  connect_bd_intf_net -intf_net rx_phy3_1 [get_bd_intf_pins rx_phy3] [get_bd_intf_pins rx/rx_phy3]
  connect_bd_intf_net -intf_net rx_rx_event [get_bd_intf_pins rx/rx_event] [get_bd_intf_pins rx_axi/rx_event]
  connect_bd_intf_net -intf_net rx_rx_ilas_config [get_bd_intf_pins rx/rx_ilas_config] [get_bd_intf_pins rx_axi/rx_ilas_config]
  connect_bd_intf_net -intf_net rx_rx_status [get_bd_intf_pins rx/rx_status] [get_bd_intf_pins rx_axi/rx_status]
  connect_bd_intf_net -intf_net s_axi_1 [get_bd_intf_pins s_axi] [get_bd_intf_pins rx_axi/s_axi]

  # Create port connections
  connect_bd_net -net device_clk_1 [get_bd_pins device_clk] [get_bd_pins rx/clk] [get_bd_pins rx_axi/core_clk]
  connect_bd_net -net rx_axi_core_reset [get_bd_pins rx/reset] [get_bd_pins rx_axi/core_reset]
  connect_bd_net -net rx_axi_irq [get_bd_pins irq] [get_bd_pins rx_axi/irq]
  connect_bd_net -net rx_phy_en_char_align [get_bd_pins phy_en_char_align] [get_bd_pins rx/phy_en_char_align]
  connect_bd_net -net rx_rx_data [get_bd_pins rx_data_tdata] [get_bd_pins rx/rx_data]
  connect_bd_net -net rx_rx_eof [get_bd_pins rx_eof] [get_bd_pins rx/rx_eof]
  connect_bd_net -net rx_rx_sof [get_bd_pins rx_sof] [get_bd_pins rx/rx_sof]
  connect_bd_net -net rx_rx_valid [get_bd_pins rx_data_tvalid] [get_bd_pins rx/rx_valid]
  connect_bd_net -net rx_sync [get_bd_pins sync] [get_bd_pins rx/sync]
  connect_bd_net -net s_axi_aclk_1 [get_bd_pins s_axi_aclk] [get_bd_pins rx_axi/s_axi_aclk]
  connect_bd_net -net s_axi_aresetn_1 [get_bd_pins s_axi_aresetn] [get_bd_pins rx_axi/s_axi_aresetn]
  connect_bd_net -net sysref_1 [get_bd_pins sysref] [get_bd_pins rx/sysref]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set s_axi_rx_adxcvr [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_rx_adxcvr ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {16} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_rx_adxcvr

  set s_axi_rx_clkgen [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_rx_clkgen ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {16} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_rx_clkgen

  set s_axi_rx_jesd [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_rx_jesd ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {14} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_rx_jesd

  set s_axi_rx_tpl [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_rx_tpl ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {13} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_rx_tpl

  set s_axi_tx_adxcvr [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_tx_adxcvr ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {16} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_tx_adxcvr

  set s_axi_tx_clkgen [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_tx_clkgen ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {16} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_tx_clkgen

  set s_axi_tx_jesd [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_tx_jesd ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {14} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_tx_jesd

  set s_axi_tx_tpl [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_tx_tpl ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {13} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $s_axi_tx_tpl


  # Create ports
  set adc_data_0 [ create_bd_port -dir O -from 31 -to 0 adc_data_0 ]
  set adc_data_1 [ create_bd_port -dir O -from 31 -to 0 adc_data_1 ]
  set adc_data_2 [ create_bd_port -dir O -from 31 -to 0 adc_data_2 ]
  set adc_data_3 [ create_bd_port -dir O -from 31 -to 0 adc_data_3 ]
  set adc_dovf [ create_bd_port -dir I adc_dovf ]
  set adc_enable_0 [ create_bd_port -dir O -from 0 -to 0 adc_enable_0 ]
  set adc_enable_1 [ create_bd_port -dir O -from 0 -to 0 adc_enable_1 ]
  set adc_enable_2 [ create_bd_port -dir O -from 0 -to 0 adc_enable_2 ]
  set adc_enable_3 [ create_bd_port -dir O -from 0 -to 0 adc_enable_3 ]
  set adc_valid_0 [ create_bd_port -dir O -from 0 -to 0 adc_valid_0 ]
  set adc_valid_1 [ create_bd_port -dir O -from 0 -to 0 adc_valid_1 ]
  set adc_valid_2 [ create_bd_port -dir O -from 0 -to 0 adc_valid_2 ]
  set adc_valid_3 [ create_bd_port -dir O -from 0 -to 0 adc_valid_3 ]
  set dac_data_0 [ create_bd_port -dir I -from 31 -to 0 dac_data_0 ]
  set dac_data_1 [ create_bd_port -dir I -from 31 -to 0 dac_data_1 ]
  set dac_data_2 [ create_bd_port -dir I -from 31 -to 0 dac_data_2 ]
  set dac_data_3 [ create_bd_port -dir I -from 31 -to 0 dac_data_3 ]
  set dac_dunf [ create_bd_port -dir I dac_dunf ]
  set dac_enable_0 [ create_bd_port -dir O -from 0 -to 0 dac_enable_0 ]
  set dac_enable_1 [ create_bd_port -dir O -from 0 -to 0 dac_enable_1 ]
  set dac_enable_2 [ create_bd_port -dir O -from 0 -to 0 dac_enable_2 ]
  set dac_enable_3 [ create_bd_port -dir O -from 0 -to 0 dac_enable_3 ]
  set dac_valid_0 [ create_bd_port -dir O -from 0 -to 0 dac_valid_0 ]
  set dac_valid_1 [ create_bd_port -dir O -from 0 -to 0 dac_valid_1 ]
  set dac_valid_2 [ create_bd_port -dir O -from 0 -to 0 dac_valid_2 ]
  set dac_valid_3 [ create_bd_port -dir O -from 0 -to 0 dac_valid_3 ]
  set rx_clkgen_ref [ create_bd_port -dir I -type clk rx_clkgen_ref ]
  set rx_data_0_n [ create_bd_port -dir I rx_data_0_n ]
  set rx_data_0_p [ create_bd_port -dir I rx_data_0_p ]
  set rx_data_1_n [ create_bd_port -dir I rx_data_1_n ]
  set rx_data_1_p [ create_bd_port -dir I rx_data_1_p ]
  set rx_data_2_n [ create_bd_port -dir I rx_data_2_n ]
  set rx_data_2_p [ create_bd_port -dir I rx_data_2_p ]
  set rx_data_3_n [ create_bd_port -dir I rx_data_3_n ]
  set rx_data_3_p [ create_bd_port -dir I rx_data_3_p ]
  set rx_linkclk [ create_bd_port -dir O rx_linkclk ]
  set rx_ref_clk_0 [ create_bd_port -dir I rx_ref_clk_0 ]
  set rx_sync [ create_bd_port -dir O -from 0 -to 0 rx_sync ]
  set rx_sysref_0 [ create_bd_port -dir I rx_sysref_0 ]
  set rxoutclk [ create_bd_port -dir O rxoutclk ]
  set s_axi_aclk [ create_bd_port -dir I -type clk s_axi_aclk ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_RESET {s_axi_aresetn} \
 ] $s_axi_aclk
  set s_axi_aresetn [ create_bd_port -dir I -type rst s_axi_aresetn ]
  set tx_clkgen_ref [ create_bd_port -dir I -type clk tx_clkgen_ref ]
  set tx_data_0_n [ create_bd_port -dir O tx_data_0_n ]
  set tx_data_0_p [ create_bd_port -dir O tx_data_0_p ]
  set tx_data_1_n [ create_bd_port -dir O tx_data_1_n ]
  set tx_data_1_p [ create_bd_port -dir O tx_data_1_p ]
  set tx_data_2_n [ create_bd_port -dir O tx_data_2_n ]
  set tx_data_2_p [ create_bd_port -dir O tx_data_2_p ]
  set tx_data_3_n [ create_bd_port -dir O tx_data_3_n ]
  set tx_data_3_p [ create_bd_port -dir O tx_data_3_p ]
  set tx_linkclk [ create_bd_port -dir O tx_linkclk ]
  set tx_ref_clk_0 [ create_bd_port -dir I tx_ref_clk_0 ]
  set tx_sync [ create_bd_port -dir I -from 0 -to 0 tx_sync ]
  set tx_sysref_0 [ create_bd_port -dir I tx_sysref_0 ]
  set txoutclk [ create_bd_port -dir O txoutclk ]

  # Create instance: axi_ad9371_rx_clkgen, and set properties
  set axi_ad9371_rx_clkgen [ create_bd_cell -type ip -vlnv analog.com:user:axi_clkgen:1.0 axi_ad9371_rx_clkgen ]
  set_property -dict [ list \
   CONFIG.CLK0_DIV {8} \
   CONFIG.CLKIN_PERIOD {8} \
   CONFIG.ID {2} \
   CONFIG.VCO_DIV {1} \
   CONFIG.VCO_MUL {8} \
 ] $axi_ad9371_rx_clkgen

  # Create instance: axi_ad9371_rx_jesd
  create_hier_cell_axi_ad9371_rx_jesd [current_bd_instance .] axi_ad9371_rx_jesd

  # Create instance: axi_ad9371_rx_xcvr, and set properties
  set axi_ad9371_rx_xcvr [ create_bd_cell -type ip -vlnv analog.com:user:axi_adxcvr:1.0 axi_ad9371_rx_xcvr ]
  set_property -dict [ list \
   CONFIG.LPM_OR_DFE_N {1} \
   CONFIG.NUM_OF_LANES {4} \
   CONFIG.OUT_CLK_SEL {"011"} \
   CONFIG.QPLL_ENABLE {0} \
   CONFIG.SYS_CLK_SEL {00} \
   CONFIG.TX_OR_RX_N {0} \
 ] $axi_ad9371_rx_xcvr

  # Create instance: axi_ad9371_tx_clkgen, and set properties
  set axi_ad9371_tx_clkgen [ create_bd_cell -type ip -vlnv analog.com:user:axi_clkgen:1.0 axi_ad9371_tx_clkgen ]
  set_property -dict [ list \
   CONFIG.CLK0_DIV {8} \
   CONFIG.CLKIN_PERIOD {8} \
   CONFIG.ID {2} \
   CONFIG.VCO_DIV {1} \
   CONFIG.VCO_MUL {8} \
 ] $axi_ad9371_tx_clkgen

  # Create instance: axi_ad9371_tx_jesd
  create_hier_cell_axi_ad9371_tx_jesd [current_bd_instance .] axi_ad9371_tx_jesd

  # Create instance: axi_ad9371_tx_xcvr, and set properties
  set axi_ad9371_tx_xcvr [ create_bd_cell -type ip -vlnv analog.com:user:axi_adxcvr:1.0 axi_ad9371_tx_xcvr ]
  set_property -dict [ list \
   CONFIG.LPM_OR_DFE_N {0} \
   CONFIG.NUM_OF_LANES {4} \
   CONFIG.OUT_CLK_SEL {"011"} \
   CONFIG.QPLL_ENABLE {1} \
   CONFIG.SYS_CLK_SEL {"11"} \
   CONFIG.TX_OR_RX_N {1} \
 ] $axi_ad9371_tx_xcvr

  # Create instance: rx_ad9371_tpl_core
  create_hier_cell_rx_ad9371_tpl_core [current_bd_instance .] rx_ad9371_tpl_core

  # Create instance: tx_ad9371_tpl_core
  create_hier_cell_tx_ad9371_tpl_core [current_bd_instance .] tx_ad9371_tpl_core

  # Create instance: util_ad9371_xcvr, and set properties
  set util_ad9371_xcvr [ create_bd_cell -type ip -vlnv analog.com:user:util_adxcvr:1.0 util_ad9371_xcvr ]
  set_property -dict [ list \
   CONFIG.CPLL_FBDIV {4} \
   CONFIG.QPLL_FBDIV {0x120} \
   CONFIG.RX_CDR_CFG {0x03000023ff20400020} \
   CONFIG.RX_CLK25_DIV {5} \
   CONFIG.RX_NUM_OF_LANES {4} \
   CONFIG.RX_PMA_CFG {0x00018480} \
   CONFIG.TX_CLK25_DIV {5} \
   CONFIG.TX_NUM_OF_LANES {4} \
   CONFIG.TX_OUT_DIV {2} \
 ] $util_ad9371_xcvr

  # Create interface connections
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_ch_0 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_ch_0] [get_bd_intf_pins util_ad9371_xcvr/up_rx_0]
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_ch_1 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_ch_1] [get_bd_intf_pins util_ad9371_xcvr/up_rx_1]
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_ch_2 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_ch_2] [get_bd_intf_pins util_ad9371_xcvr/up_rx_2]
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_ch_3 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_ch_3] [get_bd_intf_pins util_ad9371_xcvr/up_rx_3]
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_es_0 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_es_0] [get_bd_intf_pins util_ad9371_xcvr/up_es_0]
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_es_1 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_es_1] [get_bd_intf_pins util_ad9371_xcvr/up_es_1]
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_es_2 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_es_2] [get_bd_intf_pins util_ad9371_xcvr/up_es_2]
  connect_bd_intf_net -intf_net axi_ad9371_rx_xcvr_up_es_3 [get_bd_intf_pins axi_ad9371_rx_xcvr/up_es_3] [get_bd_intf_pins util_ad9371_xcvr/up_es_3]
  connect_bd_intf_net -intf_net axi_ad9371_tx_jesd_tx_phy0 [get_bd_intf_pins axi_ad9371_tx_jesd/tx_phy0] [get_bd_intf_pins util_ad9371_xcvr/tx_0]
  connect_bd_intf_net -intf_net axi_ad9371_tx_jesd_tx_phy1 [get_bd_intf_pins axi_ad9371_tx_jesd/tx_phy1] [get_bd_intf_pins util_ad9371_xcvr/tx_1]
  connect_bd_intf_net -intf_net axi_ad9371_tx_jesd_tx_phy2 [get_bd_intf_pins axi_ad9371_tx_jesd/tx_phy2] [get_bd_intf_pins util_ad9371_xcvr/tx_2]
  connect_bd_intf_net -intf_net axi_ad9371_tx_jesd_tx_phy3 [get_bd_intf_pins axi_ad9371_tx_jesd/tx_phy3] [get_bd_intf_pins util_ad9371_xcvr/tx_3]
  connect_bd_intf_net -intf_net axi_ad9371_tx_xcvr_up_ch_0 [get_bd_intf_pins axi_ad9371_tx_xcvr/up_ch_0] [get_bd_intf_pins util_ad9371_xcvr/up_tx_1]
  connect_bd_intf_net -intf_net axi_ad9371_tx_xcvr_up_ch_1 [get_bd_intf_pins axi_ad9371_tx_xcvr/up_ch_1] [get_bd_intf_pins util_ad9371_xcvr/up_tx_2]
  connect_bd_intf_net -intf_net axi_ad9371_tx_xcvr_up_ch_2 [get_bd_intf_pins axi_ad9371_tx_xcvr/up_ch_2] [get_bd_intf_pins util_ad9371_xcvr/up_tx_3]
  connect_bd_intf_net -intf_net axi_ad9371_tx_xcvr_up_ch_3 [get_bd_intf_pins axi_ad9371_tx_xcvr/up_ch_3] [get_bd_intf_pins util_ad9371_xcvr/up_tx_0]
  connect_bd_intf_net -intf_net axi_ad9371_tx_xcvr_up_cm_0 [get_bd_intf_pins axi_ad9371_tx_xcvr/up_cm_0] [get_bd_intf_pins util_ad9371_xcvr/up_cm_0]
  connect_bd_intf_net -intf_net s_axi_0_1 [get_bd_intf_ports s_axi_rx_adxcvr] [get_bd_intf_pins axi_ad9371_rx_xcvr/s_axi]
  connect_bd_intf_net -intf_net s_axi_0_2 [get_bd_intf_ports s_axi_tx_adxcvr] [get_bd_intf_pins axi_ad9371_tx_xcvr/s_axi]
  connect_bd_intf_net -intf_net s_axi_0_3 [get_bd_intf_ports s_axi_tx_tpl] [get_bd_intf_pins tx_ad9371_tpl_core/s_axi]
  connect_bd_intf_net -intf_net s_axi_0_4 [get_bd_intf_ports s_axi_rx_tpl] [get_bd_intf_pins rx_ad9371_tpl_core/s_axi]
  connect_bd_intf_net -intf_net s_axi_0_5 [get_bd_intf_ports s_axi_tx_jesd] [get_bd_intf_pins axi_ad9371_tx_jesd/s_axi]
  connect_bd_intf_net -intf_net s_axi_0_6 [get_bd_intf_ports s_axi_rx_jesd] [get_bd_intf_pins axi_ad9371_rx_jesd/s_axi]
  connect_bd_intf_net -intf_net s_axi_0_7 [get_bd_intf_ports s_axi_tx_clkgen] [get_bd_intf_pins axi_ad9371_tx_clkgen/s_axi]
  connect_bd_intf_net -intf_net s_axi_0_8 [get_bd_intf_ports s_axi_rx_clkgen] [get_bd_intf_pins axi_ad9371_rx_clkgen/s_axi]
  connect_bd_intf_net -intf_net tx_data_1 [get_bd_intf_pins axi_ad9371_tx_jesd/tx_data] [get_bd_intf_pins tx_ad9371_tpl_core/link]
  connect_bd_intf_net -intf_net util_ad9371_xcvr_rx_0 [get_bd_intf_pins axi_ad9371_rx_jesd/rx_phy0] [get_bd_intf_pins util_ad9371_xcvr/rx_0]
  connect_bd_intf_net -intf_net util_ad9371_xcvr_rx_1 [get_bd_intf_pins axi_ad9371_rx_jesd/rx_phy1] [get_bd_intf_pins util_ad9371_xcvr/rx_1]
  connect_bd_intf_net -intf_net util_ad9371_xcvr_rx_2 [get_bd_intf_pins axi_ad9371_rx_jesd/rx_phy2] [get_bd_intf_pins util_ad9371_xcvr/rx_2]
  connect_bd_intf_net -intf_net util_ad9371_xcvr_rx_3 [get_bd_intf_pins axi_ad9371_rx_jesd/rx_phy3] [get_bd_intf_pins util_ad9371_xcvr/rx_3]

  # Create port connections
  connect_bd_net -net ad9371_rx_device_clk [get_bd_ports rx_linkclk] [get_bd_pins axi_ad9371_rx_clkgen/clk_0] [get_bd_pins axi_ad9371_rx_jesd/device_clk] [get_bd_pins rx_ad9371_tpl_core/link_clk] [get_bd_pins util_ad9371_xcvr/rx_clk_0] [get_bd_pins util_ad9371_xcvr/rx_clk_1] [get_bd_pins util_ad9371_xcvr/rx_clk_2] [get_bd_pins util_ad9371_xcvr/rx_clk_3]
  connect_bd_net -net ad9371_tx_device_clk [get_bd_ports tx_linkclk] [get_bd_pins axi_ad9371_tx_clkgen/clk_0] [get_bd_pins axi_ad9371_tx_jesd/device_clk] [get_bd_pins tx_ad9371_tpl_core/link_clk] [get_bd_pins util_ad9371_xcvr/tx_clk_0] [get_bd_pins util_ad9371_xcvr/tx_clk_1] [get_bd_pins util_ad9371_xcvr/tx_clk_2] [get_bd_pins util_ad9371_xcvr/tx_clk_3]
  connect_bd_net -net adc_dovf_0_1 [get_bd_ports adc_dovf] [get_bd_pins rx_ad9371_tpl_core/adc_dovf]
  connect_bd_net -net axi_ad9371_rx_jesd_phy_en_char_align [get_bd_pins axi_ad9371_rx_jesd/phy_en_char_align] [get_bd_pins util_ad9371_xcvr/rx_calign_0] [get_bd_pins util_ad9371_xcvr/rx_calign_1] [get_bd_pins util_ad9371_xcvr/rx_calign_2] [get_bd_pins util_ad9371_xcvr/rx_calign_3]
  connect_bd_net -net axi_ad9371_rx_jesd_rx_data_tdata [get_bd_pins axi_ad9371_rx_jesd/rx_data_tdata] [get_bd_pins rx_ad9371_tpl_core/link_data]
  connect_bd_net -net axi_ad9371_rx_jesd_rx_data_tvalid [get_bd_pins axi_ad9371_rx_jesd/rx_data_tvalid] [get_bd_pins rx_ad9371_tpl_core/link_valid]
  connect_bd_net -net axi_ad9371_rx_jesd_rx_sof [get_bd_pins axi_ad9371_rx_jesd/rx_sof] [get_bd_pins rx_ad9371_tpl_core/link_sof]
  connect_bd_net -net axi_ad9371_rx_jesd_sync [get_bd_ports rx_sync] [get_bd_pins axi_ad9371_rx_jesd/sync]
  connect_bd_net -net axi_ad9371_rx_xcvr_up_pll_rst [get_bd_pins axi_ad9371_rx_xcvr/up_pll_rst] [get_bd_pins util_ad9371_xcvr/up_cpll_rst_0] [get_bd_pins util_ad9371_xcvr/up_cpll_rst_1] [get_bd_pins util_ad9371_xcvr/up_cpll_rst_2] [get_bd_pins util_ad9371_xcvr/up_cpll_rst_3]
  connect_bd_net -net axi_ad9371_tx_xcvr_up_pll_rst [get_bd_pins axi_ad9371_tx_xcvr/up_pll_rst] [get_bd_pins util_ad9371_xcvr/up_qpll_rst_0]
  connect_bd_net -net clk_0_1 [get_bd_ports rx_clkgen_ref] [get_bd_pins axi_ad9371_rx_clkgen/clk]
  connect_bd_net -net clk_0_2 [get_bd_ports tx_clkgen_ref] [get_bd_pins axi_ad9371_tx_clkgen/clk]
  connect_bd_net -net dac_data_0_0_1 [get_bd_ports dac_data_0] [get_bd_pins tx_ad9371_tpl_core/dac_data_0]
  connect_bd_net -net dac_data_1_0_1 [get_bd_ports dac_data_1] [get_bd_pins tx_ad9371_tpl_core/dac_data_1]
  connect_bd_net -net dac_data_2_0_1 [get_bd_ports dac_data_2] [get_bd_pins tx_ad9371_tpl_core/dac_data_2]
  connect_bd_net -net dac_data_3_0_1 [get_bd_ports dac_data_3] [get_bd_pins tx_ad9371_tpl_core/dac_data_3]
  connect_bd_net -net dac_dunf_0_1 [get_bd_ports dac_dunf] [get_bd_pins tx_ad9371_tpl_core/dac_dunf]
  connect_bd_net -net rx_ad9371_tpl_core_adc_data_0 [get_bd_ports adc_data_0] [get_bd_pins rx_ad9371_tpl_core/adc_data_0]
  connect_bd_net -net rx_ad9371_tpl_core_adc_data_1 [get_bd_ports adc_data_1] [get_bd_pins rx_ad9371_tpl_core/adc_data_1]
  connect_bd_net -net rx_ad9371_tpl_core_adc_data_2 [get_bd_ports adc_data_2] [get_bd_pins rx_ad9371_tpl_core/adc_data_2]
  connect_bd_net -net rx_ad9371_tpl_core_adc_data_3 [get_bd_ports adc_data_3] [get_bd_pins rx_ad9371_tpl_core/adc_data_3]
  connect_bd_net -net rx_ad9371_tpl_core_adc_enable_0 [get_bd_ports adc_enable_0] [get_bd_pins rx_ad9371_tpl_core/adc_enable_0]
  connect_bd_net -net rx_ad9371_tpl_core_adc_enable_1 [get_bd_ports adc_enable_1] [get_bd_pins rx_ad9371_tpl_core/adc_enable_1]
  connect_bd_net -net rx_ad9371_tpl_core_adc_enable_2 [get_bd_ports adc_enable_2] [get_bd_pins rx_ad9371_tpl_core/adc_enable_2]
  connect_bd_net -net rx_ad9371_tpl_core_adc_enable_3 [get_bd_ports adc_enable_3] [get_bd_pins rx_ad9371_tpl_core/adc_enable_3]
  connect_bd_net -net rx_ad9371_tpl_core_adc_valid_0 [get_bd_ports adc_valid_0] [get_bd_pins rx_ad9371_tpl_core/adc_valid_0]
  connect_bd_net -net rx_ad9371_tpl_core_adc_valid_1 [get_bd_ports adc_valid_1] [get_bd_pins rx_ad9371_tpl_core/adc_valid_1]
  connect_bd_net -net rx_ad9371_tpl_core_adc_valid_2 [get_bd_ports adc_valid_2] [get_bd_pins rx_ad9371_tpl_core/adc_valid_2]
  connect_bd_net -net rx_ad9371_tpl_core_adc_valid_3 [get_bd_ports adc_valid_3] [get_bd_pins rx_ad9371_tpl_core/adc_valid_3]
  connect_bd_net -net rx_data_0_n_1 [get_bd_ports rx_data_0_n] [get_bd_pins util_ad9371_xcvr/rx_0_n]
  connect_bd_net -net rx_data_0_p_1 [get_bd_ports rx_data_0_p] [get_bd_pins util_ad9371_xcvr/rx_0_p]
  connect_bd_net -net rx_data_1_n_1 [get_bd_ports rx_data_1_n] [get_bd_pins util_ad9371_xcvr/rx_1_n]
  connect_bd_net -net rx_data_1_p_1 [get_bd_ports rx_data_1_p] [get_bd_pins util_ad9371_xcvr/rx_1_p]
  connect_bd_net -net rx_data_2_n_1 [get_bd_ports rx_data_2_n] [get_bd_pins util_ad9371_xcvr/rx_2_n]
  connect_bd_net -net rx_data_2_p_1 [get_bd_ports rx_data_2_p] [get_bd_pins util_ad9371_xcvr/rx_2_p]
  connect_bd_net -net rx_data_3_n_1 [get_bd_ports rx_data_3_n] [get_bd_pins util_ad9371_xcvr/rx_3_n]
  connect_bd_net -net rx_data_3_p_1 [get_bd_ports rx_data_3_p] [get_bd_pins util_ad9371_xcvr/rx_3_p]
  connect_bd_net -net rx_ref_clk_0_1 [get_bd_ports rx_ref_clk_0] [get_bd_pins util_ad9371_xcvr/cpll_ref_clk_0] [get_bd_pins util_ad9371_xcvr/cpll_ref_clk_1] [get_bd_pins util_ad9371_xcvr/cpll_ref_clk_2] [get_bd_pins util_ad9371_xcvr/cpll_ref_clk_3]
  connect_bd_net -net s_axi_aclk_0_1 [get_bd_ports s_axi_aclk] [get_bd_pins axi_ad9371_rx_clkgen/s_axi_aclk] [get_bd_pins axi_ad9371_rx_jesd/s_axi_aclk] [get_bd_pins axi_ad9371_rx_xcvr/s_axi_aclk] [get_bd_pins axi_ad9371_tx_clkgen/s_axi_aclk] [get_bd_pins axi_ad9371_tx_jesd/s_axi_aclk] [get_bd_pins axi_ad9371_tx_xcvr/s_axi_aclk] [get_bd_pins rx_ad9371_tpl_core/s_axi_aclk] [get_bd_pins tx_ad9371_tpl_core/s_axi_aclk] [get_bd_pins util_ad9371_xcvr/up_clk]
  connect_bd_net -net s_axi_aresetn_0_1 [get_bd_ports s_axi_aresetn] [get_bd_pins axi_ad9371_rx_clkgen/s_axi_aresetn] [get_bd_pins axi_ad9371_rx_jesd/s_axi_aresetn] [get_bd_pins axi_ad9371_rx_xcvr/s_axi_aresetn] [get_bd_pins axi_ad9371_tx_clkgen/s_axi_aresetn] [get_bd_pins axi_ad9371_tx_jesd/s_axi_aresetn] [get_bd_pins axi_ad9371_tx_xcvr/s_axi_aresetn] [get_bd_pins rx_ad9371_tpl_core/s_axi_aresetn] [get_bd_pins tx_ad9371_tpl_core/s_axi_aresetn] [get_bd_pins util_ad9371_xcvr/up_rstn]
  connect_bd_net -net sync_1 [get_bd_ports tx_sync] [get_bd_pins axi_ad9371_tx_jesd/sync]
  connect_bd_net -net sysref_1 [get_bd_ports tx_sysref_0] [get_bd_pins axi_ad9371_tx_jesd/sysref]
  connect_bd_net -net sysref_2 [get_bd_ports rx_sysref_0] [get_bd_pins axi_ad9371_rx_jesd/sysref]
  connect_bd_net -net tx_ad9371_tpl_core_dac_enable_0 [get_bd_ports dac_enable_0] [get_bd_pins tx_ad9371_tpl_core/dac_enable_0]
  connect_bd_net -net tx_ad9371_tpl_core_dac_enable_1 [get_bd_ports dac_enable_1] [get_bd_pins tx_ad9371_tpl_core/dac_enable_1]
  connect_bd_net -net tx_ad9371_tpl_core_dac_enable_2 [get_bd_ports dac_enable_2] [get_bd_pins tx_ad9371_tpl_core/dac_enable_2]
  connect_bd_net -net tx_ad9371_tpl_core_dac_enable_3 [get_bd_ports dac_enable_3] [get_bd_pins tx_ad9371_tpl_core/dac_enable_3]
  connect_bd_net -net tx_ad9371_tpl_core_dac_valid_0 [get_bd_ports dac_valid_0] [get_bd_pins tx_ad9371_tpl_core/dac_valid_0]
  connect_bd_net -net tx_ad9371_tpl_core_dac_valid_1 [get_bd_ports dac_valid_1] [get_bd_pins tx_ad9371_tpl_core/dac_valid_1]
  connect_bd_net -net tx_ad9371_tpl_core_dac_valid_2 [get_bd_ports dac_valid_2] [get_bd_pins tx_ad9371_tpl_core/dac_valid_2]
  connect_bd_net -net tx_ad9371_tpl_core_dac_valid_3 [get_bd_ports dac_valid_3] [get_bd_pins tx_ad9371_tpl_core/dac_valid_3]
  connect_bd_net -net tx_ref_clk_0_1 [get_bd_ports tx_ref_clk_0] [get_bd_pins util_ad9371_xcvr/qpll_ref_clk_0]
  connect_bd_net -net util_ad9371_xcvr_rx_out_clk_0 [get_bd_ports rxoutclk] [get_bd_pins util_ad9371_xcvr/rx_out_clk_0]
  connect_bd_net -net util_ad9371_xcvr_tx_0_n [get_bd_ports tx_data_0_n] [get_bd_pins util_ad9371_xcvr/tx_0_n]
  connect_bd_net -net util_ad9371_xcvr_tx_0_p [get_bd_ports tx_data_0_p] [get_bd_pins util_ad9371_xcvr/tx_0_p]
  connect_bd_net -net util_ad9371_xcvr_tx_1_n [get_bd_ports tx_data_1_n] [get_bd_pins util_ad9371_xcvr/tx_1_n]
  connect_bd_net -net util_ad9371_xcvr_tx_1_p [get_bd_ports tx_data_1_p] [get_bd_pins util_ad9371_xcvr/tx_1_p]
  connect_bd_net -net util_ad9371_xcvr_tx_2_n [get_bd_ports tx_data_2_n] [get_bd_pins util_ad9371_xcvr/tx_2_n]
  connect_bd_net -net util_ad9371_xcvr_tx_2_p [get_bd_ports tx_data_2_p] [get_bd_pins util_ad9371_xcvr/tx_2_p]
  connect_bd_net -net util_ad9371_xcvr_tx_3_n [get_bd_ports tx_data_3_n] [get_bd_pins util_ad9371_xcvr/tx_3_n]
  connect_bd_net -net util_ad9371_xcvr_tx_3_p [get_bd_ports tx_data_3_p] [get_bd_pins util_ad9371_xcvr/tx_3_p]
  connect_bd_net -net util_ad9371_xcvr_tx_out_clk_0 [get_bd_ports txoutclk] [get_bd_pins util_ad9371_xcvr/tx_out_clk_0]

  # Create address segments
  assign_bd_address -offset 0x00000000 -range 0x00002000 -target_address_space [get_bd_addr_spaces s_axi_rx_tpl] [get_bd_addr_segs rx_ad9371_tpl_core/adc_tpl_core/s_axi/axi_lite] -force
  assign_bd_address -offset 0x00000000 -range 0x00010000 -target_address_space [get_bd_addr_spaces s_axi_rx_clkgen] [get_bd_addr_segs axi_ad9371_rx_clkgen/s_axi/axi_lite] -force
  assign_bd_address -offset 0x00000000 -range 0x00010000 -target_address_space [get_bd_addr_spaces s_axi_rx_adxcvr] [get_bd_addr_segs axi_ad9371_rx_xcvr/s_axi/axi_lite] -force
  assign_bd_address -offset 0x00000000 -range 0x00010000 -target_address_space [get_bd_addr_spaces s_axi_tx_clkgen] [get_bd_addr_segs axi_ad9371_tx_clkgen/s_axi/axi_lite] -force
  assign_bd_address -offset 0x00000000 -range 0x00010000 -target_address_space [get_bd_addr_spaces s_axi_tx_adxcvr] [get_bd_addr_segs axi_ad9371_tx_xcvr/s_axi/axi_lite] -force
  assign_bd_address -offset 0x00000000 -range 0x00002000 -target_address_space [get_bd_addr_spaces s_axi_tx_tpl] [get_bd_addr_segs tx_ad9371_tpl_core/dac_tpl_core/s_axi/axi_lite] -force
  assign_bd_address -offset 0x00000000 -range 0x00004000 -target_address_space [get_bd_addr_spaces s_axi_rx_jesd] [get_bd_addr_segs axi_ad9371_rx_jesd/rx_axi/s_axi/axi_lite] -force
  assign_bd_address -offset 0x00000000 -range 0x00004000 -target_address_space [get_bd_addr_spaces s_axi_tx_jesd] [get_bd_addr_segs axi_ad9371_tx_jesd/tx_axi/s_axi/axi_lite] -force


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


