//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
//Date        : Wed May 19 11:01:39 2021
//Host        : localhost.localdomain running 64-bit CentOS Linux release 7.9.2009 (Core)
//Command     : generate_target system.bd
//Design      : system
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module axi_ad9371_rx_jesd_imp_GUTUBY
   (device_clk,
    irq,
    phy_en_char_align,
    rx_data_tdata,
    rx_data_tvalid,
    rx_eof,
    rx_phy0_rxcharisk,
    rx_phy0_rxdata,
    rx_phy0_rxdisperr,
    rx_phy0_rxnotintable,
    rx_phy1_rxcharisk,
    rx_phy1_rxdata,
    rx_phy1_rxdisperr,
    rx_phy1_rxnotintable,
    rx_phy2_rxcharisk,
    rx_phy2_rxdata,
    rx_phy2_rxdisperr,
    rx_phy2_rxnotintable,
    rx_phy3_rxcharisk,
    rx_phy3_rxdata,
    rx_phy3_rxdisperr,
    rx_phy3_rxnotintable,
    rx_sof,
    s_axi_aclk,
    s_axi_araddr,
    s_axi_aresetn,
    s_axi_arprot,
    s_axi_arready,
    s_axi_arvalid,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awready,
    s_axi_awvalid,
    s_axi_bready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_rdata,
    s_axi_rready,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_wdata,
    s_axi_wready,
    s_axi_wstrb,
    s_axi_wvalid,
    sync,
    sysref);
  input device_clk;
  output irq;
  output phy_en_char_align;
  output [127:0]rx_data_tdata;
  output rx_data_tvalid;
  output [3:0]rx_eof;
  input [3:0]rx_phy0_rxcharisk;
  input [31:0]rx_phy0_rxdata;
  input [3:0]rx_phy0_rxdisperr;
  input [3:0]rx_phy0_rxnotintable;
  input [3:0]rx_phy1_rxcharisk;
  input [31:0]rx_phy1_rxdata;
  input [3:0]rx_phy1_rxdisperr;
  input [3:0]rx_phy1_rxnotintable;
  input [3:0]rx_phy2_rxcharisk;
  input [31:0]rx_phy2_rxdata;
  input [3:0]rx_phy2_rxdisperr;
  input [3:0]rx_phy2_rxnotintable;
  input [3:0]rx_phy3_rxcharisk;
  input [31:0]rx_phy3_rxdata;
  input [3:0]rx_phy3_rxdisperr;
  input [3:0]rx_phy3_rxnotintable;
  output [3:0]rx_sof;
  input s_axi_aclk;
  input [13:0]s_axi_araddr;
  input s_axi_aresetn;
  input [2:0]s_axi_arprot;
  output s_axi_arready;
  input s_axi_arvalid;
  input [13:0]s_axi_awaddr;
  input [2:0]s_axi_awprot;
  output s_axi_awready;
  input s_axi_awvalid;
  input s_axi_bready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  output [31:0]s_axi_rdata;
  input s_axi_rready;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input [31:0]s_axi_wdata;
  output s_axi_wready;
  input [3:0]s_axi_wstrb;
  input s_axi_wvalid;
  output [0:0]sync;
  input sysref;

  wire device_clk_1;
  wire rx_axi_core_reset;
  wire rx_axi_irq;
  wire [7:0]rx_axi_rx_cfg_beats_per_multiframe;
  wire [7:0]rx_axi_rx_cfg_buffer_delay;
  wire rx_axi_rx_cfg_buffer_early_release;
  wire rx_axi_rx_cfg_disable_char_replacement;
  wire rx_axi_rx_cfg_disable_scrambler;
  wire [6:0]rx_axi_rx_cfg_err_statistics_mask;
  wire rx_axi_rx_cfg_err_statistics_reset;
  wire [7:0]rx_axi_rx_cfg_frame_align_err_threshold;
  wire [1:0]rx_axi_rx_cfg_lanes_disable;
  wire [0:0]rx_axi_rx_cfg_links_disable;
  wire [7:0]rx_axi_rx_cfg_lmfc_offset;
  wire [7:0]rx_axi_rx_cfg_octets_per_frame;
  wire rx_axi_rx_cfg_sysref_disable;
  wire rx_axi_rx_cfg_sysref_oneshot;
  wire [3:0]rx_phy0_1_rxcharisk;
  wire [31:0]rx_phy0_1_rxdata;
  wire [3:0]rx_phy0_1_rxdisperr;
  wire [3:0]rx_phy0_1_rxnotintable;
  wire [3:0]rx_phy1_1_rxcharisk;
  wire [31:0]rx_phy1_1_rxdata;
  wire [3:0]rx_phy1_1_rxdisperr;
  wire [3:0]rx_phy1_1_rxnotintable;
  wire [3:0]rx_phy2_1_rxcharisk;
  wire [31:0]rx_phy2_1_rxdata;
  wire [3:0]rx_phy2_1_rxdisperr;
  wire [3:0]rx_phy2_1_rxnotintable;
  wire [3:0]rx_phy3_1_rxcharisk;
  wire [31:0]rx_phy3_1_rxdata;
  wire [3:0]rx_phy3_1_rxdisperr;
  wire [3:0]rx_phy3_1_rxnotintable;
  wire rx_phy_en_char_align;
  wire [127:0]rx_rx_data;
  wire [3:0]rx_rx_eof;
  wire rx_rx_event_frame_alignment_error;
  wire rx_rx_event_sysref_alignment_error;
  wire rx_rx_event_sysref_edge;
  wire rx_rx_event_unexpected_lane_state_error;
  wire [7:0]rx_rx_ilas_config_addr;
  wire [127:0]rx_rx_ilas_config_data;
  wire [3:0]rx_rx_ilas_config_valid;
  wire [3:0]rx_rx_sof;
  wire [1:0]rx_rx_status_ctrl_state;
  wire [127:0]rx_rx_status_err_statistics_cnt;
  wire [7:0]rx_rx_status_lane_cgs_state;
  wire [11:0]rx_rx_status_lane_emb_state;
  wire [31:0]rx_rx_status_lane_frame_align_err_cnt;
  wire [3:0]rx_rx_status_lane_ifs_ready;
  wire [55:0]rx_rx_status_lane_latency;
  wire rx_rx_valid;
  wire [0:0]rx_sync;
  wire [13:0]s_axi_1_ARADDR;
  wire [2:0]s_axi_1_ARPROT;
  wire s_axi_1_ARREADY;
  wire s_axi_1_ARVALID;
  wire [13:0]s_axi_1_AWADDR;
  wire [2:0]s_axi_1_AWPROT;
  wire s_axi_1_AWREADY;
  wire s_axi_1_AWVALID;
  wire s_axi_1_BREADY;
  wire [1:0]s_axi_1_BRESP;
  wire s_axi_1_BVALID;
  wire [31:0]s_axi_1_RDATA;
  wire s_axi_1_RREADY;
  wire [1:0]s_axi_1_RRESP;
  wire s_axi_1_RVALID;
  wire [31:0]s_axi_1_WDATA;
  wire s_axi_1_WREADY;
  wire [3:0]s_axi_1_WSTRB;
  wire s_axi_1_WVALID;
  wire s_axi_aclk_1;
  wire s_axi_aresetn_1;
  wire sysref_1;

  assign device_clk_1 = device_clk;
  assign irq = rx_axi_irq;
  assign phy_en_char_align = rx_phy_en_char_align;
  assign rx_data_tdata[127:0] = rx_rx_data;
  assign rx_data_tvalid = rx_rx_valid;
  assign rx_eof[3:0] = rx_rx_eof;
  assign rx_phy0_1_rxcharisk = rx_phy0_rxcharisk[3:0];
  assign rx_phy0_1_rxdata = rx_phy0_rxdata[31:0];
  assign rx_phy0_1_rxdisperr = rx_phy0_rxdisperr[3:0];
  assign rx_phy0_1_rxnotintable = rx_phy0_rxnotintable[3:0];
  assign rx_phy1_1_rxcharisk = rx_phy1_rxcharisk[3:0];
  assign rx_phy1_1_rxdata = rx_phy1_rxdata[31:0];
  assign rx_phy1_1_rxdisperr = rx_phy1_rxdisperr[3:0];
  assign rx_phy1_1_rxnotintable = rx_phy1_rxnotintable[3:0];
  assign rx_phy2_1_rxcharisk = rx_phy2_rxcharisk[3:0];
  assign rx_phy2_1_rxdata = rx_phy2_rxdata[31:0];
  assign rx_phy2_1_rxdisperr = rx_phy2_rxdisperr[3:0];
  assign rx_phy2_1_rxnotintable = rx_phy2_rxnotintable[3:0];
  assign rx_phy3_1_rxcharisk = rx_phy3_rxcharisk[3:0];
  assign rx_phy3_1_rxdata = rx_phy3_rxdata[31:0];
  assign rx_phy3_1_rxdisperr = rx_phy3_rxdisperr[3:0];
  assign rx_phy3_1_rxnotintable = rx_phy3_rxnotintable[3:0];
  assign rx_sof[3:0] = rx_rx_sof;
  assign s_axi_1_ARADDR = s_axi_araddr[13:0];
  assign s_axi_1_ARPROT = s_axi_arprot[2:0];
  assign s_axi_1_ARVALID = s_axi_arvalid;
  assign s_axi_1_AWADDR = s_axi_awaddr[13:0];
  assign s_axi_1_AWPROT = s_axi_awprot[2:0];
  assign s_axi_1_AWVALID = s_axi_awvalid;
  assign s_axi_1_BREADY = s_axi_bready;
  assign s_axi_1_RREADY = s_axi_rready;
  assign s_axi_1_WDATA = s_axi_wdata[31:0];
  assign s_axi_1_WSTRB = s_axi_wstrb[3:0];
  assign s_axi_1_WVALID = s_axi_wvalid;
  assign s_axi_aclk_1 = s_axi_aclk;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  assign s_axi_arready = s_axi_1_ARREADY;
  assign s_axi_awready = s_axi_1_AWREADY;
  assign s_axi_bresp[1:0] = s_axi_1_BRESP;
  assign s_axi_bvalid = s_axi_1_BVALID;
  assign s_axi_rdata[31:0] = s_axi_1_RDATA;
  assign s_axi_rresp[1:0] = s_axi_1_RRESP;
  assign s_axi_rvalid = s_axi_1_RVALID;
  assign s_axi_wready = s_axi_1_WREADY;
  assign sync[0] = rx_sync;
  assign sysref_1 = sysref;
  system_rx_0 rx
       (.cfg_beats_per_multiframe(rx_axi_rx_cfg_beats_per_multiframe),
        .cfg_buffer_delay(rx_axi_rx_cfg_buffer_delay),
        .cfg_buffer_early_release(rx_axi_rx_cfg_buffer_early_release),
        .cfg_disable_char_replacement(rx_axi_rx_cfg_disable_char_replacement),
        .cfg_disable_scrambler(rx_axi_rx_cfg_disable_scrambler),
        .cfg_frame_align_err_threshold(rx_axi_rx_cfg_frame_align_err_threshold),
        .cfg_lanes_disable({1'b0,1'b0,rx_axi_rx_cfg_lanes_disable}),
        .cfg_links_disable(rx_axi_rx_cfg_links_disable),
        .cfg_lmfc_offset(rx_axi_rx_cfg_lmfc_offset),
        .cfg_octets_per_frame(rx_axi_rx_cfg_octets_per_frame),
        .cfg_sysref_disable(rx_axi_rx_cfg_sysref_disable),
        .cfg_sysref_oneshot(rx_axi_rx_cfg_sysref_oneshot),
        .clk(device_clk_1),
        .ctrl_err_statistics_mask(rx_axi_rx_cfg_err_statistics_mask),
        .ctrl_err_statistics_reset(rx_axi_rx_cfg_err_statistics_reset),
        .event_frame_alignment_error(rx_rx_event_frame_alignment_error),
        .event_sysref_alignment_error(rx_rx_event_sysref_alignment_error),
        .event_sysref_edge(rx_rx_event_sysref_edge),
        .event_unexpected_lane_state_error(rx_rx_event_unexpected_lane_state_error),
        .ilas_config_addr(rx_rx_ilas_config_addr),
        .ilas_config_data(rx_rx_ilas_config_data),
        .ilas_config_valid(rx_rx_ilas_config_valid),
        .phy_block_sync({1'b0,1'b0,1'b0,1'b0}),
        .phy_charisk({rx_phy3_1_rxcharisk,rx_phy2_1_rxcharisk,rx_phy1_1_rxcharisk,rx_phy0_1_rxcharisk}),
        .phy_data({rx_phy3_1_rxdata,rx_phy2_1_rxdata,rx_phy1_1_rxdata,rx_phy0_1_rxdata}),
        .phy_disperr({rx_phy3_1_rxdisperr,rx_phy2_1_rxdisperr,rx_phy1_1_rxdisperr,rx_phy0_1_rxdisperr}),
        .phy_en_char_align(rx_phy_en_char_align),
        .phy_header({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .phy_notintable({rx_phy3_1_rxnotintable,rx_phy2_1_rxnotintable,rx_phy1_1_rxnotintable,rx_phy0_1_rxnotintable}),
        .reset(rx_axi_core_reset),
        .rx_data(rx_rx_data),
        .rx_eof(rx_rx_eof),
        .rx_sof(rx_rx_sof),
        .rx_valid(rx_rx_valid),
        .status_ctrl_state(rx_rx_status_ctrl_state),
        .status_err_statistics_cnt(rx_rx_status_err_statistics_cnt),
        .status_lane_cgs_state(rx_rx_status_lane_cgs_state),
        .status_lane_emb_state(rx_rx_status_lane_emb_state),
        .status_lane_frame_align_err_cnt(rx_rx_status_lane_frame_align_err_cnt),
        .status_lane_ifs_ready(rx_rx_status_lane_ifs_ready),
        .status_lane_latency(rx_rx_status_lane_latency),
        .sync(rx_sync),
        .sysref(sysref_1));
  system_rx_axi_0 rx_axi
       (.core_cfg_beats_per_multiframe(rx_axi_rx_cfg_beats_per_multiframe),
        .core_cfg_buffer_delay(rx_axi_rx_cfg_buffer_delay),
        .core_cfg_buffer_early_release(rx_axi_rx_cfg_buffer_early_release),
        .core_cfg_disable_char_replacement(rx_axi_rx_cfg_disable_char_replacement),
        .core_cfg_disable_scrambler(rx_axi_rx_cfg_disable_scrambler),
        .core_cfg_frame_align_err_threshold(rx_axi_rx_cfg_frame_align_err_threshold),
        .core_cfg_lanes_disable(rx_axi_rx_cfg_lanes_disable),
        .core_cfg_links_disable(rx_axi_rx_cfg_links_disable),
        .core_cfg_lmfc_offset(rx_axi_rx_cfg_lmfc_offset),
        .core_cfg_octets_per_frame(rx_axi_rx_cfg_octets_per_frame),
        .core_cfg_sysref_disable(rx_axi_rx_cfg_sysref_disable),
        .core_cfg_sysref_oneshot(rx_axi_rx_cfg_sysref_oneshot),
        .core_clk(device_clk_1),
        .core_ctrl_err_statistics_mask(rx_axi_rx_cfg_err_statistics_mask),
        .core_ctrl_err_statistics_reset(rx_axi_rx_cfg_err_statistics_reset),
        .core_event_frame_alignment_error(rx_rx_event_frame_alignment_error),
        .core_event_sysref_alignment_error(rx_rx_event_sysref_alignment_error),
        .core_event_sysref_edge(rx_rx_event_sysref_edge),
        .core_event_unexpected_lane_state_error(rx_rx_event_unexpected_lane_state_error),
        .core_ilas_config_addr(rx_rx_ilas_config_addr[3:0]),
        .core_ilas_config_data(rx_rx_ilas_config_data[63:0]),
        .core_ilas_config_valid(rx_rx_ilas_config_valid[1:0]),
        .core_reset(rx_axi_core_reset),
        .core_reset_ext(1'b0),
        .core_status_ctrl_state(rx_rx_status_ctrl_state),
        .core_status_err_statistics_cnt(rx_rx_status_err_statistics_cnt[63:0]),
        .core_status_lane_cgs_state(rx_rx_status_lane_cgs_state[3:0]),
        .core_status_lane_emb_state(rx_rx_status_lane_emb_state[5:0]),
        .core_status_lane_frame_align_err_cnt(rx_rx_status_lane_frame_align_err_cnt[15:0]),
        .core_status_lane_ifs_ready(rx_rx_status_lane_ifs_ready[1:0]),
        .core_status_lane_latency(rx_rx_status_lane_latency[27:0]),
        .irq(rx_axi_irq),
        .s_axi_aclk(s_axi_aclk_1),
        .s_axi_araddr(s_axi_1_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(s_axi_1_ARPROT),
        .s_axi_arready(s_axi_1_ARREADY),
        .s_axi_arvalid(s_axi_1_ARVALID),
        .s_axi_awaddr(s_axi_1_AWADDR),
        .s_axi_awprot(s_axi_1_AWPROT),
        .s_axi_awready(s_axi_1_AWREADY),
        .s_axi_awvalid(s_axi_1_AWVALID),
        .s_axi_bready(s_axi_1_BREADY),
        .s_axi_bresp(s_axi_1_BRESP),
        .s_axi_bvalid(s_axi_1_BVALID),
        .s_axi_rdata(s_axi_1_RDATA),
        .s_axi_rready(s_axi_1_RREADY),
        .s_axi_rresp(s_axi_1_RRESP),
        .s_axi_rvalid(s_axi_1_RVALID),
        .s_axi_wdata(s_axi_1_WDATA),
        .s_axi_wready(s_axi_1_WREADY),
        .s_axi_wstrb(s_axi_1_WSTRB),
        .s_axi_wvalid(s_axi_1_WVALID));
endmodule

module axi_ad9371_tx_jesd_imp_17BPCLV
   (device_clk,
    irq,
    s_axi_aclk,
    s_axi_araddr,
    s_axi_aresetn,
    s_axi_arprot,
    s_axi_arready,
    s_axi_arvalid,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awready,
    s_axi_awvalid,
    s_axi_bready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_rdata,
    s_axi_rready,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_wdata,
    s_axi_wready,
    s_axi_wstrb,
    s_axi_wvalid,
    sync,
    sysref,
    tx_data_tdata,
    tx_data_tready,
    tx_data_tvalid,
    tx_phy0_txcharisk,
    tx_phy0_txdata,
    tx_phy1_txcharisk,
    tx_phy1_txdata,
    tx_phy2_txcharisk,
    tx_phy2_txdata,
    tx_phy3_txcharisk,
    tx_phy3_txdata);
  input device_clk;
  output irq;
  input s_axi_aclk;
  input [13:0]s_axi_araddr;
  input s_axi_aresetn;
  input [2:0]s_axi_arprot;
  output s_axi_arready;
  input s_axi_arvalid;
  input [13:0]s_axi_awaddr;
  input [2:0]s_axi_awprot;
  output s_axi_awready;
  input s_axi_awvalid;
  input s_axi_bready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  output [31:0]s_axi_rdata;
  input s_axi_rready;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input [31:0]s_axi_wdata;
  output s_axi_wready;
  input [3:0]s_axi_wstrb;
  input s_axi_wvalid;
  input [0:0]sync;
  input sysref;
  input [127:0]tx_data_tdata;
  output tx_data_tready;
  input tx_data_tvalid;
  output [3:0]tx_phy0_txcharisk;
  output [31:0]tx_phy0_txdata;
  output [3:0]tx_phy1_txcharisk;
  output [31:0]tx_phy1_txdata;
  output [3:0]tx_phy2_txcharisk;
  output [31:0]tx_phy2_txdata;
  output [3:0]tx_phy3_txcharisk;
  output [31:0]tx_phy3_txdata;

  wire device_clk_1;
  wire [13:0]s_axi_1_ARADDR;
  wire [2:0]s_axi_1_ARPROT;
  wire s_axi_1_ARREADY;
  wire s_axi_1_ARVALID;
  wire [13:0]s_axi_1_AWADDR;
  wire [2:0]s_axi_1_AWPROT;
  wire s_axi_1_AWREADY;
  wire s_axi_1_AWVALID;
  wire s_axi_1_BREADY;
  wire [1:0]s_axi_1_BRESP;
  wire s_axi_1_BVALID;
  wire [31:0]s_axi_1_RDATA;
  wire s_axi_1_RREADY;
  wire [1:0]s_axi_1_RRESP;
  wire s_axi_1_RVALID;
  wire [31:0]s_axi_1_WDATA;
  wire s_axi_1_WREADY;
  wire [3:0]s_axi_1_WSTRB;
  wire s_axi_1_WVALID;
  wire s_axi_aclk_1;
  wire s_axi_aresetn_1;
  wire [0:0]sync_1;
  wire sysref_1;
  wire tx_axi_core_reset;
  wire tx_axi_irq;
  wire [7:0]tx_axi_tx_cfg_beats_per_multiframe;
  wire tx_axi_tx_cfg_continuous_cgs;
  wire tx_axi_tx_cfg_continuous_ilas;
  wire tx_axi_tx_cfg_disable_char_replacement;
  wire tx_axi_tx_cfg_disable_scrambler;
  wire [3:0]tx_axi_tx_cfg_lanes_disable;
  wire [0:0]tx_axi_tx_cfg_links_disable;
  wire [7:0]tx_axi_tx_cfg_lmfc_offset;
  wire [7:0]tx_axi_tx_cfg_mframes_per_ilas;
  wire [7:0]tx_axi_tx_cfg_octets_per_frame;
  wire tx_axi_tx_cfg_skip_ilas;
  wire tx_axi_tx_cfg_sysref_disable;
  wire tx_axi_tx_cfg_sysref_oneshot;
  wire tx_axi_tx_ctrl_manual_sync_request;
  wire [127:0]tx_data_1_TDATA;
  wire tx_data_1_TREADY;
  wire tx_data_1_TVALID;
  wire tx_tx_event_sysref_alignment_error;
  wire tx_tx_event_sysref_edge;
  wire [1:0]tx_tx_ilas_config_addr;
  wire [127:0]tx_tx_ilas_config_data;
  wire tx_tx_ilas_config_rd;
  wire [3:0]tx_tx_phy0_txcharisk;
  wire [31:0]tx_tx_phy0_txdata;
  wire [7:4]tx_tx_phy1_txcharisk;
  wire [63:32]tx_tx_phy1_txdata;
  wire [11:8]tx_tx_phy2_txcharisk;
  wire [95:64]tx_tx_phy2_txdata;
  wire [15:12]tx_tx_phy3_txcharisk;
  wire [127:96]tx_tx_phy3_txdata;
  wire [1:0]tx_tx_status_state;
  wire [0:0]tx_tx_status_sync;

  assign device_clk_1 = device_clk;
  assign irq = tx_axi_irq;
  assign s_axi_1_ARADDR = s_axi_araddr[13:0];
  assign s_axi_1_ARPROT = s_axi_arprot[2:0];
  assign s_axi_1_ARVALID = s_axi_arvalid;
  assign s_axi_1_AWADDR = s_axi_awaddr[13:0];
  assign s_axi_1_AWPROT = s_axi_awprot[2:0];
  assign s_axi_1_AWVALID = s_axi_awvalid;
  assign s_axi_1_BREADY = s_axi_bready;
  assign s_axi_1_RREADY = s_axi_rready;
  assign s_axi_1_WDATA = s_axi_wdata[31:0];
  assign s_axi_1_WSTRB = s_axi_wstrb[3:0];
  assign s_axi_1_WVALID = s_axi_wvalid;
  assign s_axi_aclk_1 = s_axi_aclk;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  assign s_axi_arready = s_axi_1_ARREADY;
  assign s_axi_awready = s_axi_1_AWREADY;
  assign s_axi_bresp[1:0] = s_axi_1_BRESP;
  assign s_axi_bvalid = s_axi_1_BVALID;
  assign s_axi_rdata[31:0] = s_axi_1_RDATA;
  assign s_axi_rresp[1:0] = s_axi_1_RRESP;
  assign s_axi_rvalid = s_axi_1_RVALID;
  assign s_axi_wready = s_axi_1_WREADY;
  assign sync_1 = sync[0];
  assign sysref_1 = sysref;
  assign tx_data_1_TDATA = tx_data_tdata[127:0];
  assign tx_data_1_TVALID = tx_data_tvalid;
  assign tx_data_tready = tx_data_1_TREADY;
  assign tx_phy0_txcharisk[3:0] = tx_tx_phy0_txcharisk;
  assign tx_phy0_txdata[31:0] = tx_tx_phy0_txdata;
  assign tx_phy1_txcharisk[3:0] = tx_tx_phy1_txcharisk;
  assign tx_phy1_txdata[31:0] = tx_tx_phy1_txdata;
  assign tx_phy2_txcharisk[3:0] = tx_tx_phy2_txcharisk;
  assign tx_phy2_txdata[31:0] = tx_tx_phy2_txdata;
  assign tx_phy3_txcharisk[3:0] = tx_tx_phy3_txcharisk;
  assign tx_phy3_txdata[31:0] = tx_tx_phy3_txdata;
  system_tx_0 tx
       (.cfg_beats_per_multiframe(tx_axi_tx_cfg_beats_per_multiframe),
        .cfg_continuous_cgs(tx_axi_tx_cfg_continuous_cgs),
        .cfg_continuous_ilas(tx_axi_tx_cfg_continuous_ilas),
        .cfg_disable_char_replacement(tx_axi_tx_cfg_disable_char_replacement),
        .cfg_disable_scrambler(tx_axi_tx_cfg_disable_scrambler),
        .cfg_lanes_disable(tx_axi_tx_cfg_lanes_disable),
        .cfg_links_disable(tx_axi_tx_cfg_links_disable),
        .cfg_lmfc_offset(tx_axi_tx_cfg_lmfc_offset),
        .cfg_mframes_per_ilas(tx_axi_tx_cfg_mframes_per_ilas),
        .cfg_octets_per_frame(tx_axi_tx_cfg_octets_per_frame),
        .cfg_skip_ilas(tx_axi_tx_cfg_skip_ilas),
        .cfg_sysref_disable(tx_axi_tx_cfg_sysref_disable),
        .cfg_sysref_oneshot(tx_axi_tx_cfg_sysref_oneshot),
        .clk(device_clk_1),
        .ctrl_manual_sync_request(tx_axi_tx_ctrl_manual_sync_request),
        .event_sysref_alignment_error(tx_tx_event_sysref_alignment_error),
        .event_sysref_edge(tx_tx_event_sysref_edge),
        .ilas_config_addr(tx_tx_ilas_config_addr),
        .ilas_config_data(tx_tx_ilas_config_data),
        .ilas_config_rd(tx_tx_ilas_config_rd),
        .phy_charisk({tx_tx_phy3_txcharisk,tx_tx_phy2_txcharisk,tx_tx_phy1_txcharisk,tx_tx_phy0_txcharisk}),
        .phy_data({tx_tx_phy3_txdata,tx_tx_phy2_txdata,tx_tx_phy1_txdata,tx_tx_phy0_txdata}),
        .reset(tx_axi_core_reset),
        .status_state(tx_tx_status_state),
        .status_sync(tx_tx_status_sync),
        .sync(sync_1),
        .sysref(sysref_1),
        .tx_data(tx_data_1_TDATA),
        .tx_ready(tx_data_1_TREADY),
        .tx_valid(tx_data_1_TVALID));
  system_tx_axi_0 tx_axi
       (.core_cfg_beats_per_multiframe(tx_axi_tx_cfg_beats_per_multiframe),
        .core_cfg_continuous_cgs(tx_axi_tx_cfg_continuous_cgs),
        .core_cfg_continuous_ilas(tx_axi_tx_cfg_continuous_ilas),
        .core_cfg_disable_char_replacement(tx_axi_tx_cfg_disable_char_replacement),
        .core_cfg_disable_scrambler(tx_axi_tx_cfg_disable_scrambler),
        .core_cfg_lanes_disable(tx_axi_tx_cfg_lanes_disable),
        .core_cfg_links_disable(tx_axi_tx_cfg_links_disable),
        .core_cfg_lmfc_offset(tx_axi_tx_cfg_lmfc_offset),
        .core_cfg_mframes_per_ilas(tx_axi_tx_cfg_mframes_per_ilas),
        .core_cfg_octets_per_frame(tx_axi_tx_cfg_octets_per_frame),
        .core_cfg_skip_ilas(tx_axi_tx_cfg_skip_ilas),
        .core_cfg_sysref_disable(tx_axi_tx_cfg_sysref_disable),
        .core_cfg_sysref_oneshot(tx_axi_tx_cfg_sysref_oneshot),
        .core_clk(device_clk_1),
        .core_ctrl_manual_sync_request(tx_axi_tx_ctrl_manual_sync_request),
        .core_event_sysref_alignment_error(tx_tx_event_sysref_alignment_error),
        .core_event_sysref_edge(tx_tx_event_sysref_edge),
        .core_ilas_config_addr(tx_tx_ilas_config_addr),
        .core_ilas_config_data(tx_tx_ilas_config_data),
        .core_ilas_config_rd(tx_tx_ilas_config_rd),
        .core_reset(tx_axi_core_reset),
        .core_reset_ext(1'b0),
        .core_status_state(tx_tx_status_state),
        .core_status_sync(tx_tx_status_sync),
        .irq(tx_axi_irq),
        .s_axi_aclk(s_axi_aclk_1),
        .s_axi_araddr(s_axi_1_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(s_axi_1_ARPROT),
        .s_axi_arready(s_axi_1_ARREADY),
        .s_axi_arvalid(s_axi_1_ARVALID),
        .s_axi_awaddr(s_axi_1_AWADDR),
        .s_axi_awprot(s_axi_1_AWPROT),
        .s_axi_awready(s_axi_1_AWREADY),
        .s_axi_awvalid(s_axi_1_AWVALID),
        .s_axi_bready(s_axi_1_BREADY),
        .s_axi_bresp(s_axi_1_BRESP),
        .s_axi_bvalid(s_axi_1_BVALID),
        .s_axi_rdata(s_axi_1_RDATA),
        .s_axi_rready(s_axi_1_RREADY),
        .s_axi_rresp(s_axi_1_RRESP),
        .s_axi_rvalid(s_axi_1_RVALID),
        .s_axi_wdata(s_axi_1_WDATA),
        .s_axi_wready(s_axi_1_WREADY),
        .s_axi_wstrb(s_axi_1_WSTRB),
        .s_axi_wvalid(s_axi_1_WVALID));
endmodule

module rx_ad9371_tpl_core_imp_WW7IEM
   (adc_data_0,
    adc_data_1,
    adc_data_2,
    adc_data_3,
    adc_dovf,
    adc_enable_0,
    adc_enable_1,
    adc_enable_2,
    adc_enable_3,
    adc_valid_0,
    adc_valid_1,
    adc_valid_2,
    adc_valid_3,
    link_clk,
    link_data,
    link_sof,
    link_valid,
    s_axi_aclk,
    s_axi_araddr,
    s_axi_aresetn,
    s_axi_arprot,
    s_axi_arready,
    s_axi_arvalid,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awready,
    s_axi_awvalid,
    s_axi_bready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_rdata,
    s_axi_rready,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_wdata,
    s_axi_wready,
    s_axi_wstrb,
    s_axi_wvalid);
  output [31:0]adc_data_0;
  output [31:0]adc_data_1;
  output [31:0]adc_data_2;
  output [31:0]adc_data_3;
  input adc_dovf;
  output [0:0]adc_enable_0;
  output [0:0]adc_enable_1;
  output [0:0]adc_enable_2;
  output [0:0]adc_enable_3;
  output [0:0]adc_valid_0;
  output [0:0]adc_valid_1;
  output [0:0]adc_valid_2;
  output [0:0]adc_valid_3;
  input link_clk;
  input [127:0]link_data;
  input [3:0]link_sof;
  input link_valid;
  input s_axi_aclk;
  input [12:0]s_axi_araddr;
  input s_axi_aresetn;
  input [2:0]s_axi_arprot;
  output s_axi_arready;
  input s_axi_arvalid;
  input [12:0]s_axi_awaddr;
  input [2:0]s_axi_awprot;
  output s_axi_awready;
  input s_axi_awvalid;
  input s_axi_bready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  output [31:0]s_axi_rdata;
  input s_axi_rready;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input [31:0]s_axi_wdata;
  output s_axi_wready;
  input [3:0]s_axi_wstrb;
  input s_axi_wvalid;

  wire adc_dovf_1;
  wire [127:0]adc_tpl_core_adc_data;
  wire [3:0]adc_tpl_core_adc_valid;
  wire [3:0]adc_tpl_core_enable;
  wire [31:0]data_slice_0_Dout;
  wire [31:0]data_slice_1_Dout;
  wire [31:0]data_slice_2_Dout;
  wire [31:0]data_slice_3_Dout;
  wire [0:0]enable_slice_0_Dout;
  wire [0:0]enable_slice_1_Dout;
  wire [0:0]enable_slice_2_Dout;
  wire [0:0]enable_slice_3_Dout;
  wire link_clk_1;
  wire [127:0]link_data_1;
  wire [3:0]link_sof_1;
  wire link_valid_1;
  wire [12:0]s_axi_1_ARADDR;
  wire [2:0]s_axi_1_ARPROT;
  wire s_axi_1_ARREADY;
  wire s_axi_1_ARVALID;
  wire [12:0]s_axi_1_AWADDR;
  wire [2:0]s_axi_1_AWPROT;
  wire s_axi_1_AWREADY;
  wire s_axi_1_AWVALID;
  wire s_axi_1_BREADY;
  wire [1:0]s_axi_1_BRESP;
  wire s_axi_1_BVALID;
  wire [31:0]s_axi_1_RDATA;
  wire s_axi_1_RREADY;
  wire [1:0]s_axi_1_RRESP;
  wire s_axi_1_RVALID;
  wire [31:0]s_axi_1_WDATA;
  wire s_axi_1_WREADY;
  wire [3:0]s_axi_1_WSTRB;
  wire s_axi_1_WVALID;
  wire s_axi_aclk_1;
  wire s_axi_aresetn_1;
  wire [0:0]valid_slice_0_Dout;
  wire [0:0]valid_slice_1_Dout;
  wire [0:0]valid_slice_2_Dout;
  wire [0:0]valid_slice_3_Dout;

  assign adc_data_0[31:0] = data_slice_0_Dout;
  assign adc_data_1[31:0] = data_slice_1_Dout;
  assign adc_data_2[31:0] = data_slice_2_Dout;
  assign adc_data_3[31:0] = data_slice_3_Dout;
  assign adc_dovf_1 = adc_dovf;
  assign adc_enable_0[0] = enable_slice_0_Dout;
  assign adc_enable_1[0] = enable_slice_1_Dout;
  assign adc_enable_2[0] = enable_slice_2_Dout;
  assign adc_enable_3[0] = enable_slice_3_Dout;
  assign adc_valid_0[0] = valid_slice_0_Dout;
  assign adc_valid_1[0] = valid_slice_1_Dout;
  assign adc_valid_2[0] = valid_slice_2_Dout;
  assign adc_valid_3[0] = valid_slice_3_Dout;
  assign link_clk_1 = link_clk;
  assign link_data_1 = link_data[127:0];
  assign link_sof_1 = link_sof[3:0];
  assign link_valid_1 = link_valid;
  assign s_axi_1_ARADDR = s_axi_araddr[12:0];
  assign s_axi_1_ARPROT = s_axi_arprot[2:0];
  assign s_axi_1_ARVALID = s_axi_arvalid;
  assign s_axi_1_AWADDR = s_axi_awaddr[12:0];
  assign s_axi_1_AWPROT = s_axi_awprot[2:0];
  assign s_axi_1_AWVALID = s_axi_awvalid;
  assign s_axi_1_BREADY = s_axi_bready;
  assign s_axi_1_RREADY = s_axi_rready;
  assign s_axi_1_WDATA = s_axi_wdata[31:0];
  assign s_axi_1_WSTRB = s_axi_wstrb[3:0];
  assign s_axi_1_WVALID = s_axi_wvalid;
  assign s_axi_aclk_1 = s_axi_aclk;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  assign s_axi_arready = s_axi_1_ARREADY;
  assign s_axi_awready = s_axi_1_AWREADY;
  assign s_axi_bresp[1:0] = s_axi_1_BRESP;
  assign s_axi_bvalid = s_axi_1_BVALID;
  assign s_axi_rdata[31:0] = s_axi_1_RDATA;
  assign s_axi_rresp[1:0] = s_axi_1_RRESP;
  assign s_axi_rvalid = s_axi_1_RVALID;
  assign s_axi_wready = s_axi_1_WREADY;
  system_adc_tpl_core_0 adc_tpl_core
       (.adc_data(adc_tpl_core_adc_data),
        .adc_dovf(adc_dovf_1),
        .adc_sync_in(1'b0),
        .adc_valid(adc_tpl_core_adc_valid),
        .enable(adc_tpl_core_enable),
        .link_clk(link_clk_1),
        .link_data(link_data_1),
        .link_sof(link_sof_1),
        .link_valid(link_valid_1),
        .s_axi_aclk(s_axi_aclk_1),
        .s_axi_araddr(s_axi_1_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(s_axi_1_ARPROT),
        .s_axi_arready(s_axi_1_ARREADY),
        .s_axi_arvalid(s_axi_1_ARVALID),
        .s_axi_awaddr(s_axi_1_AWADDR),
        .s_axi_awprot(s_axi_1_AWPROT),
        .s_axi_awready(s_axi_1_AWREADY),
        .s_axi_awvalid(s_axi_1_AWVALID),
        .s_axi_bready(s_axi_1_BREADY),
        .s_axi_bresp(s_axi_1_BRESP),
        .s_axi_bvalid(s_axi_1_BVALID),
        .s_axi_rdata(s_axi_1_RDATA),
        .s_axi_rready(s_axi_1_RREADY),
        .s_axi_rresp(s_axi_1_RRESP),
        .s_axi_rvalid(s_axi_1_RVALID),
        .s_axi_wdata(s_axi_1_WDATA),
        .s_axi_wready(s_axi_1_WREADY),
        .s_axi_wstrb(s_axi_1_WSTRB),
        .s_axi_wvalid(s_axi_1_WVALID));
  system_data_slice_0_0 data_slice_0
       (.Din(adc_tpl_core_adc_data),
        .Dout(data_slice_0_Dout));
  system_data_slice_1_0 data_slice_1
       (.Din(adc_tpl_core_adc_data),
        .Dout(data_slice_1_Dout));
  system_data_slice_2_0 data_slice_2
       (.Din(adc_tpl_core_adc_data),
        .Dout(data_slice_2_Dout));
  system_data_slice_3_0 data_slice_3
       (.Din(adc_tpl_core_adc_data),
        .Dout(data_slice_3_Dout));
  system_enable_slice_0_1 enable_slice_0
       (.Din(adc_tpl_core_enable),
        .Dout(enable_slice_0_Dout));
  system_enable_slice_1_1 enable_slice_1
       (.Din(adc_tpl_core_enable),
        .Dout(enable_slice_1_Dout));
  system_enable_slice_2_1 enable_slice_2
       (.Din(adc_tpl_core_enable),
        .Dout(enable_slice_2_Dout));
  system_enable_slice_3_1 enable_slice_3
       (.Din(adc_tpl_core_enable),
        .Dout(enable_slice_3_Dout));
  system_valid_slice_0_1 valid_slice_0
       (.Din(adc_tpl_core_adc_valid),
        .Dout(valid_slice_0_Dout));
  system_valid_slice_1_1 valid_slice_1
       (.Din(adc_tpl_core_adc_valid),
        .Dout(valid_slice_1_Dout));
  system_valid_slice_2_1 valid_slice_2
       (.Din(adc_tpl_core_adc_valid),
        .Dout(valid_slice_2_Dout));
  system_valid_slice_3_1 valid_slice_3
       (.Din(adc_tpl_core_adc_valid),
        .Dout(valid_slice_3_Dout));
endmodule

(* CORE_GENERATION_INFO = "system,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=system,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=36,numReposBlks=32,numNonXlnxBlks=11,numHierBlks=4,maxHierDepth=1,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=Global}" *) (* HW_HANDOFF = "system.hwdef" *) 
module system
   (adc_data_0,
    adc_data_1,
    adc_data_2,
    adc_data_3,
    adc_dovf,
    adc_enable_0,
    adc_enable_1,
    adc_enable_2,
    adc_enable_3,
    adc_valid_0,
    adc_valid_1,
    adc_valid_2,
    adc_valid_3,
    dac_data_0,
    dac_data_1,
    dac_data_2,
    dac_data_3,
    dac_dunf,
    dac_enable_0,
    dac_enable_1,
    dac_enable_2,
    dac_enable_3,
    dac_valid_0,
    dac_valid_1,
    dac_valid_2,
    dac_valid_3,
    rx_data_0_n,
    rx_data_0_p,
    rx_data_1_n,
    rx_data_1_p,
    rx_data_2_n,
    rx_data_2_p,
    rx_data_3_n,
    rx_data_3_p,
    rx_ref_clk_0,
    rx_sync,
    rx_sysref_0,
    s_axi_aclk,
    s_axi_aresetn,
    s_axi_rx_adxcvr_araddr,
    s_axi_rx_adxcvr_arprot,
    s_axi_rx_adxcvr_arready,
    s_axi_rx_adxcvr_arvalid,
    s_axi_rx_adxcvr_awaddr,
    s_axi_rx_adxcvr_awprot,
    s_axi_rx_adxcvr_awready,
    s_axi_rx_adxcvr_awvalid,
    s_axi_rx_adxcvr_bready,
    s_axi_rx_adxcvr_bresp,
    s_axi_rx_adxcvr_bvalid,
    s_axi_rx_adxcvr_rdata,
    s_axi_rx_adxcvr_rready,
    s_axi_rx_adxcvr_rresp,
    s_axi_rx_adxcvr_rvalid,
    s_axi_rx_adxcvr_wdata,
    s_axi_rx_adxcvr_wready,
    s_axi_rx_adxcvr_wstrb,
    s_axi_rx_adxcvr_wvalid,
    s_axi_rx_clkgen_araddr,
    s_axi_rx_clkgen_arprot,
    s_axi_rx_clkgen_arready,
    s_axi_rx_clkgen_arvalid,
    s_axi_rx_clkgen_awaddr,
    s_axi_rx_clkgen_awprot,
    s_axi_rx_clkgen_awready,
    s_axi_rx_clkgen_awvalid,
    s_axi_rx_clkgen_bready,
    s_axi_rx_clkgen_bresp,
    s_axi_rx_clkgen_bvalid,
    s_axi_rx_clkgen_rdata,
    s_axi_rx_clkgen_rready,
    s_axi_rx_clkgen_rresp,
    s_axi_rx_clkgen_rvalid,
    s_axi_rx_clkgen_wdata,
    s_axi_rx_clkgen_wready,
    s_axi_rx_clkgen_wstrb,
    s_axi_rx_clkgen_wvalid,
    s_axi_rx_jesd_araddr,
    s_axi_rx_jesd_arprot,
    s_axi_rx_jesd_arready,
    s_axi_rx_jesd_arvalid,
    s_axi_rx_jesd_awaddr,
    s_axi_rx_jesd_awprot,
    s_axi_rx_jesd_awready,
    s_axi_rx_jesd_awvalid,
    s_axi_rx_jesd_bready,
    s_axi_rx_jesd_bresp,
    s_axi_rx_jesd_bvalid,
    s_axi_rx_jesd_rdata,
    s_axi_rx_jesd_rready,
    s_axi_rx_jesd_rresp,
    s_axi_rx_jesd_rvalid,
    s_axi_rx_jesd_wdata,
    s_axi_rx_jesd_wready,
    s_axi_rx_jesd_wstrb,
    s_axi_rx_jesd_wvalid,
    s_axi_rx_tpl_araddr,
    s_axi_rx_tpl_arprot,
    s_axi_rx_tpl_arready,
    s_axi_rx_tpl_arvalid,
    s_axi_rx_tpl_awaddr,
    s_axi_rx_tpl_awprot,
    s_axi_rx_tpl_awready,
    s_axi_rx_tpl_awvalid,
    s_axi_rx_tpl_bready,
    s_axi_rx_tpl_bresp,
    s_axi_rx_tpl_bvalid,
    s_axi_rx_tpl_rdata,
    s_axi_rx_tpl_rready,
    s_axi_rx_tpl_rresp,
    s_axi_rx_tpl_rvalid,
    s_axi_rx_tpl_wdata,
    s_axi_rx_tpl_wready,
    s_axi_rx_tpl_wstrb,
    s_axi_rx_tpl_wvalid,
    s_axi_tx_adxcvr_araddr,
    s_axi_tx_adxcvr_arprot,
    s_axi_tx_adxcvr_arready,
    s_axi_tx_adxcvr_arvalid,
    s_axi_tx_adxcvr_awaddr,
    s_axi_tx_adxcvr_awprot,
    s_axi_tx_adxcvr_awready,
    s_axi_tx_adxcvr_awvalid,
    s_axi_tx_adxcvr_bready,
    s_axi_tx_adxcvr_bresp,
    s_axi_tx_adxcvr_bvalid,
    s_axi_tx_adxcvr_rdata,
    s_axi_tx_adxcvr_rready,
    s_axi_tx_adxcvr_rresp,
    s_axi_tx_adxcvr_rvalid,
    s_axi_tx_adxcvr_wdata,
    s_axi_tx_adxcvr_wready,
    s_axi_tx_adxcvr_wstrb,
    s_axi_tx_adxcvr_wvalid,
    s_axi_tx_clkgen_araddr,
    s_axi_tx_clkgen_arprot,
    s_axi_tx_clkgen_arready,
    s_axi_tx_clkgen_arvalid,
    s_axi_tx_clkgen_awaddr,
    s_axi_tx_clkgen_awprot,
    s_axi_tx_clkgen_awready,
    s_axi_tx_clkgen_awvalid,
    s_axi_tx_clkgen_bready,
    s_axi_tx_clkgen_bresp,
    s_axi_tx_clkgen_bvalid,
    s_axi_tx_clkgen_rdata,
    s_axi_tx_clkgen_rready,
    s_axi_tx_clkgen_rresp,
    s_axi_tx_clkgen_rvalid,
    s_axi_tx_clkgen_wdata,
    s_axi_tx_clkgen_wready,
    s_axi_tx_clkgen_wstrb,
    s_axi_tx_clkgen_wvalid,
    s_axi_tx_jesd_araddr,
    s_axi_tx_jesd_arprot,
    s_axi_tx_jesd_arready,
    s_axi_tx_jesd_arvalid,
    s_axi_tx_jesd_awaddr,
    s_axi_tx_jesd_awprot,
    s_axi_tx_jesd_awready,
    s_axi_tx_jesd_awvalid,
    s_axi_tx_jesd_bready,
    s_axi_tx_jesd_bresp,
    s_axi_tx_jesd_bvalid,
    s_axi_tx_jesd_rdata,
    s_axi_tx_jesd_rready,
    s_axi_tx_jesd_rresp,
    s_axi_tx_jesd_rvalid,
    s_axi_tx_jesd_wdata,
    s_axi_tx_jesd_wready,
    s_axi_tx_jesd_wstrb,
    s_axi_tx_jesd_wvalid,
    s_axi_tx_tpl_araddr,
    s_axi_tx_tpl_arprot,
    s_axi_tx_tpl_arready,
    s_axi_tx_tpl_arvalid,
    s_axi_tx_tpl_awaddr,
    s_axi_tx_tpl_awprot,
    s_axi_tx_tpl_awready,
    s_axi_tx_tpl_awvalid,
    s_axi_tx_tpl_bready,
    s_axi_tx_tpl_bresp,
    s_axi_tx_tpl_bvalid,
    s_axi_tx_tpl_rdata,
    s_axi_tx_tpl_rready,
    s_axi_tx_tpl_rresp,
    s_axi_tx_tpl_rvalid,
    s_axi_tx_tpl_wdata,
    s_axi_tx_tpl_wready,
    s_axi_tx_tpl_wstrb,
    s_axi_tx_tpl_wvalid,
    tx_data_0_n,
    tx_data_0_p,
    tx_data_1_n,
    tx_data_1_p,
    tx_data_2_n,
    tx_data_2_p,
    tx_data_3_n,
    tx_data_3_p,
    tx_ref_clk_0,
    tx_sync,
    tx_sysref_0);
  output [31:0]adc_data_0;
  output [31:0]adc_data_1;
  output [31:0]adc_data_2;
  output [31:0]adc_data_3;
  input adc_dovf;
  output [0:0]adc_enable_0;
  output [0:0]adc_enable_1;
  output [0:0]adc_enable_2;
  output [0:0]adc_enable_3;
  output [0:0]adc_valid_0;
  output [0:0]adc_valid_1;
  output [0:0]adc_valid_2;
  output [0:0]adc_valid_3;
  input [31:0]dac_data_0;
  input [31:0]dac_data_1;
  input [31:0]dac_data_2;
  input [31:0]dac_data_3;
  input dac_dunf;
  output [0:0]dac_enable_0;
  output [0:0]dac_enable_1;
  output [0:0]dac_enable_2;
  output [0:0]dac_enable_3;
  output [0:0]dac_valid_0;
  output [0:0]dac_valid_1;
  output [0:0]dac_valid_2;
  output [0:0]dac_valid_3;
  input rx_data_0_n;
  input rx_data_0_p;
  input rx_data_1_n;
  input rx_data_1_p;
  input rx_data_2_n;
  input rx_data_2_p;
  input rx_data_3_n;
  input rx_data_3_p;
  input rx_ref_clk_0;
  output [0:0]rx_sync;
  input rx_sysref_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 CLK.S_AXI_ACLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME CLK.S_AXI_ACLK, ASSOCIATED_BUSIF s_axi_rx_adxcvr:s_axi_tx_adxcvr:s_axi_tx_tpl:s_axi_rx_tpl:s_axi_tx_jesd:s_axi_rx_jesd:s_axi_tx_clkgen:s_axi_rx_clkgen, ASSOCIATED_RESET s_axi_aresetn, CLK_DOMAIN system_s_axi_aclk_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000" *) input s_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.S_AXI_ARESETN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.S_AXI_ARESETN, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input s_axi_aresetn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_rx_adxcvr, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [15:0]s_axi_rx_adxcvr_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input [2:0]s_axi_rx_adxcvr_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output s_axi_rx_adxcvr_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input s_axi_rx_adxcvr_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input [15:0]s_axi_rx_adxcvr_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input [2:0]s_axi_rx_adxcvr_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output s_axi_rx_adxcvr_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input s_axi_rx_adxcvr_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input s_axi_rx_adxcvr_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output [1:0]s_axi_rx_adxcvr_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output s_axi_rx_adxcvr_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output [31:0]s_axi_rx_adxcvr_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input s_axi_rx_adxcvr_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output [1:0]s_axi_rx_adxcvr_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output s_axi_rx_adxcvr_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input [31:0]s_axi_rx_adxcvr_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) output s_axi_rx_adxcvr_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input [3:0]s_axi_rx_adxcvr_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr " *) input s_axi_rx_adxcvr_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_rx_clkgen, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [15:0]s_axi_rx_clkgen_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input [2:0]s_axi_rx_clkgen_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output s_axi_rx_clkgen_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input s_axi_rx_clkgen_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input [15:0]s_axi_rx_clkgen_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input [2:0]s_axi_rx_clkgen_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output s_axi_rx_clkgen_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input s_axi_rx_clkgen_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input s_axi_rx_clkgen_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output [1:0]s_axi_rx_clkgen_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output s_axi_rx_clkgen_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output [31:0]s_axi_rx_clkgen_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input s_axi_rx_clkgen_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output [1:0]s_axi_rx_clkgen_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output s_axi_rx_clkgen_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input [31:0]s_axi_rx_clkgen_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) output s_axi_rx_clkgen_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input [3:0]s_axi_rx_clkgen_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen " *) input s_axi_rx_clkgen_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_rx_jesd, ADDR_WIDTH 14, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [13:0]s_axi_rx_jesd_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input [2:0]s_axi_rx_jesd_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output s_axi_rx_jesd_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input s_axi_rx_jesd_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input [13:0]s_axi_rx_jesd_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input [2:0]s_axi_rx_jesd_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output s_axi_rx_jesd_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input s_axi_rx_jesd_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input s_axi_rx_jesd_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output [1:0]s_axi_rx_jesd_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output s_axi_rx_jesd_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output [31:0]s_axi_rx_jesd_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input s_axi_rx_jesd_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output [1:0]s_axi_rx_jesd_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output s_axi_rx_jesd_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input [31:0]s_axi_rx_jesd_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) output s_axi_rx_jesd_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input [3:0]s_axi_rx_jesd_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd " *) input s_axi_rx_jesd_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_rx_tpl, ADDR_WIDTH 13, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [12:0]s_axi_rx_tpl_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input [2:0]s_axi_rx_tpl_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output s_axi_rx_tpl_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input s_axi_rx_tpl_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input [12:0]s_axi_rx_tpl_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input [2:0]s_axi_rx_tpl_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output s_axi_rx_tpl_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input s_axi_rx_tpl_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input s_axi_rx_tpl_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output [1:0]s_axi_rx_tpl_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output s_axi_rx_tpl_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output [31:0]s_axi_rx_tpl_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input s_axi_rx_tpl_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output [1:0]s_axi_rx_tpl_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output s_axi_rx_tpl_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input [31:0]s_axi_rx_tpl_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) output s_axi_rx_tpl_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input [3:0]s_axi_rx_tpl_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl " *) input s_axi_rx_tpl_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_tx_adxcvr, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [15:0]s_axi_tx_adxcvr_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input [2:0]s_axi_tx_adxcvr_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output s_axi_tx_adxcvr_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input s_axi_tx_adxcvr_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input [15:0]s_axi_tx_adxcvr_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input [2:0]s_axi_tx_adxcvr_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output s_axi_tx_adxcvr_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input s_axi_tx_adxcvr_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input s_axi_tx_adxcvr_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output [1:0]s_axi_tx_adxcvr_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output s_axi_tx_adxcvr_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output [31:0]s_axi_tx_adxcvr_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input s_axi_tx_adxcvr_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output [1:0]s_axi_tx_adxcvr_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output s_axi_tx_adxcvr_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input [31:0]s_axi_tx_adxcvr_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) output s_axi_tx_adxcvr_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input [3:0]s_axi_tx_adxcvr_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr " *) input s_axi_tx_adxcvr_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_tx_clkgen, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [15:0]s_axi_tx_clkgen_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input [2:0]s_axi_tx_clkgen_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output s_axi_tx_clkgen_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input s_axi_tx_clkgen_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input [15:0]s_axi_tx_clkgen_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input [2:0]s_axi_tx_clkgen_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output s_axi_tx_clkgen_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input s_axi_tx_clkgen_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input s_axi_tx_clkgen_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output [1:0]s_axi_tx_clkgen_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output s_axi_tx_clkgen_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output [31:0]s_axi_tx_clkgen_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input s_axi_tx_clkgen_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output [1:0]s_axi_tx_clkgen_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output s_axi_tx_clkgen_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input [31:0]s_axi_tx_clkgen_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) output s_axi_tx_clkgen_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input [3:0]s_axi_tx_clkgen_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen " *) input s_axi_tx_clkgen_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_tx_jesd, ADDR_WIDTH 14, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [13:0]s_axi_tx_jesd_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input [2:0]s_axi_tx_jesd_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output s_axi_tx_jesd_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input s_axi_tx_jesd_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input [13:0]s_axi_tx_jesd_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input [2:0]s_axi_tx_jesd_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output s_axi_tx_jesd_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input s_axi_tx_jesd_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input s_axi_tx_jesd_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output [1:0]s_axi_tx_jesd_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output s_axi_tx_jesd_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output [31:0]s_axi_tx_jesd_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input s_axi_tx_jesd_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output [1:0]s_axi_tx_jesd_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output s_axi_tx_jesd_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input [31:0]s_axi_tx_jesd_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) output s_axi_tx_jesd_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input [3:0]s_axi_tx_jesd_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd " *) input s_axi_tx_jesd_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_tx_tpl, ADDR_WIDTH 13, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0" *) input [12:0]s_axi_tx_tpl_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input [2:0]s_axi_tx_tpl_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output s_axi_tx_tpl_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input s_axi_tx_tpl_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input [12:0]s_axi_tx_tpl_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input [2:0]s_axi_tx_tpl_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output s_axi_tx_tpl_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input s_axi_tx_tpl_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input s_axi_tx_tpl_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output [1:0]s_axi_tx_tpl_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output s_axi_tx_tpl_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output [31:0]s_axi_tx_tpl_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input s_axi_tx_tpl_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output [1:0]s_axi_tx_tpl_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output s_axi_tx_tpl_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input [31:0]s_axi_tx_tpl_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) output s_axi_tx_tpl_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input [3:0]s_axi_tx_tpl_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl " *) input s_axi_tx_tpl_wvalid;
  output tx_data_0_n;
  output tx_data_0_p;
  output tx_data_1_n;
  output tx_data_1_p;
  output tx_data_2_n;
  output tx_data_2_p;
  output tx_data_3_n;
  output tx_data_3_p;
  input tx_ref_clk_0;
  input [0:0]tx_sync;
  input tx_sysref_0;

  wire ad9371_rx_device_clk;
  wire ad9371_tx_device_clk;
  wire adc_dovf_0_1;
  wire axi_ad9371_rx_jesd_phy_en_char_align;
  wire [127:0]axi_ad9371_rx_jesd_rx_data_tdata;
  wire axi_ad9371_rx_jesd_rx_data_tvalid;
  wire [3:0]axi_ad9371_rx_jesd_rx_sof;
  wire [0:0]axi_ad9371_rx_jesd_sync;
  wire [11:0]axi_ad9371_rx_xcvr_up_ch_0_addr;
  wire axi_ad9371_rx_xcvr_up_ch_0_enb;
  wire axi_ad9371_rx_xcvr_up_ch_0_lpm_dfe_n;
  wire [2:0]axi_ad9371_rx_xcvr_up_ch_0_out_clk_sel;
  wire axi_ad9371_rx_xcvr_up_ch_0_pll_locked;
  wire [2:0]axi_ad9371_rx_xcvr_up_ch_0_rate;
  wire [15:0]axi_ad9371_rx_xcvr_up_ch_0_rdata;
  wire axi_ad9371_rx_xcvr_up_ch_0_ready;
  wire axi_ad9371_rx_xcvr_up_ch_0_rst;
  wire axi_ad9371_rx_xcvr_up_ch_0_rst_done;
  wire [1:0]axi_ad9371_rx_xcvr_up_ch_0_sys_clk_sel;
  wire axi_ad9371_rx_xcvr_up_ch_0_user_ready;
  wire [15:0]axi_ad9371_rx_xcvr_up_ch_0_wdata;
  wire axi_ad9371_rx_xcvr_up_ch_0_wr;
  wire [11:0]axi_ad9371_rx_xcvr_up_ch_1_addr;
  wire axi_ad9371_rx_xcvr_up_ch_1_enb;
  wire axi_ad9371_rx_xcvr_up_ch_1_lpm_dfe_n;
  wire [2:0]axi_ad9371_rx_xcvr_up_ch_1_out_clk_sel;
  wire axi_ad9371_rx_xcvr_up_ch_1_pll_locked;
  wire [2:0]axi_ad9371_rx_xcvr_up_ch_1_rate;
  wire [15:0]axi_ad9371_rx_xcvr_up_ch_1_rdata;
  wire axi_ad9371_rx_xcvr_up_ch_1_ready;
  wire axi_ad9371_rx_xcvr_up_ch_1_rst;
  wire axi_ad9371_rx_xcvr_up_ch_1_rst_done;
  wire [1:0]axi_ad9371_rx_xcvr_up_ch_1_sys_clk_sel;
  wire axi_ad9371_rx_xcvr_up_ch_1_user_ready;
  wire [15:0]axi_ad9371_rx_xcvr_up_ch_1_wdata;
  wire axi_ad9371_rx_xcvr_up_ch_1_wr;
  wire [11:0]axi_ad9371_rx_xcvr_up_es_0_addr;
  wire axi_ad9371_rx_xcvr_up_es_0_enb;
  wire [15:0]axi_ad9371_rx_xcvr_up_es_0_rdata;
  wire axi_ad9371_rx_xcvr_up_es_0_ready;
  wire axi_ad9371_rx_xcvr_up_es_0_reset;
  wire [15:0]axi_ad9371_rx_xcvr_up_es_0_wdata;
  wire axi_ad9371_rx_xcvr_up_es_0_wr;
  wire [11:0]axi_ad9371_rx_xcvr_up_es_1_addr;
  wire axi_ad9371_rx_xcvr_up_es_1_enb;
  wire [15:0]axi_ad9371_rx_xcvr_up_es_1_rdata;
  wire axi_ad9371_rx_xcvr_up_es_1_ready;
  wire axi_ad9371_rx_xcvr_up_es_1_reset;
  wire [15:0]axi_ad9371_rx_xcvr_up_es_1_wdata;
  wire axi_ad9371_rx_xcvr_up_es_1_wr;
  wire axi_ad9371_rx_xcvr_up_pll_rst;
  wire [3:0]axi_ad9371_tx_jesd_tx_phy0_txcharisk;
  wire [31:0]axi_ad9371_tx_jesd_tx_phy0_txdata;
  wire [3:0]axi_ad9371_tx_jesd_tx_phy1_txcharisk;
  wire [31:0]axi_ad9371_tx_jesd_tx_phy1_txdata;
  wire [3:0]axi_ad9371_tx_jesd_tx_phy2_txcharisk;
  wire [31:0]axi_ad9371_tx_jesd_tx_phy2_txdata;
  wire [3:0]axi_ad9371_tx_jesd_tx_phy3_txcharisk;
  wire [31:0]axi_ad9371_tx_jesd_tx_phy3_txdata;
  wire [11:0]axi_ad9371_tx_xcvr_up_ch_0_addr;
  wire axi_ad9371_tx_xcvr_up_ch_0_enb;
  wire axi_ad9371_tx_xcvr_up_ch_0_lpm_dfe_n;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_0_out_clk_sel;
  wire axi_ad9371_tx_xcvr_up_ch_0_pll_locked;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_0_rate;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_0_rdata;
  wire axi_ad9371_tx_xcvr_up_ch_0_ready;
  wire axi_ad9371_tx_xcvr_up_ch_0_rst;
  wire axi_ad9371_tx_xcvr_up_ch_0_rst_done;
  wire [1:0]axi_ad9371_tx_xcvr_up_ch_0_sys_clk_sel;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_0_tx_diffctrl;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_0_tx_postcursor;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_0_tx_precursor;
  wire axi_ad9371_tx_xcvr_up_ch_0_user_ready;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_0_wdata;
  wire axi_ad9371_tx_xcvr_up_ch_0_wr;
  wire [11:0]axi_ad9371_tx_xcvr_up_ch_1_addr;
  wire axi_ad9371_tx_xcvr_up_ch_1_enb;
  wire axi_ad9371_tx_xcvr_up_ch_1_lpm_dfe_n;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_1_out_clk_sel;
  wire axi_ad9371_tx_xcvr_up_ch_1_pll_locked;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_1_rate;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_1_rdata;
  wire axi_ad9371_tx_xcvr_up_ch_1_ready;
  wire axi_ad9371_tx_xcvr_up_ch_1_rst;
  wire axi_ad9371_tx_xcvr_up_ch_1_rst_done;
  wire [1:0]axi_ad9371_tx_xcvr_up_ch_1_sys_clk_sel;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_1_tx_diffctrl;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_1_tx_postcursor;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_1_tx_precursor;
  wire axi_ad9371_tx_xcvr_up_ch_1_user_ready;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_1_wdata;
  wire axi_ad9371_tx_xcvr_up_ch_1_wr;
  wire [11:0]axi_ad9371_tx_xcvr_up_ch_2_addr;
  wire axi_ad9371_tx_xcvr_up_ch_2_enb;
  wire axi_ad9371_tx_xcvr_up_ch_2_lpm_dfe_n;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_2_out_clk_sel;
  wire axi_ad9371_tx_xcvr_up_ch_2_pll_locked;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_2_rate;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_2_rdata;
  wire axi_ad9371_tx_xcvr_up_ch_2_ready;
  wire axi_ad9371_tx_xcvr_up_ch_2_rst;
  wire axi_ad9371_tx_xcvr_up_ch_2_rst_done;
  wire [1:0]axi_ad9371_tx_xcvr_up_ch_2_sys_clk_sel;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_2_tx_diffctrl;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_2_tx_postcursor;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_2_tx_precursor;
  wire axi_ad9371_tx_xcvr_up_ch_2_user_ready;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_2_wdata;
  wire axi_ad9371_tx_xcvr_up_ch_2_wr;
  wire [11:0]axi_ad9371_tx_xcvr_up_ch_3_addr;
  wire axi_ad9371_tx_xcvr_up_ch_3_enb;
  wire axi_ad9371_tx_xcvr_up_ch_3_lpm_dfe_n;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_3_out_clk_sel;
  wire axi_ad9371_tx_xcvr_up_ch_3_pll_locked;
  wire [2:0]axi_ad9371_tx_xcvr_up_ch_3_rate;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_3_rdata;
  wire axi_ad9371_tx_xcvr_up_ch_3_ready;
  wire axi_ad9371_tx_xcvr_up_ch_3_rst;
  wire axi_ad9371_tx_xcvr_up_ch_3_rst_done;
  wire [1:0]axi_ad9371_tx_xcvr_up_ch_3_sys_clk_sel;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_3_tx_diffctrl;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_3_tx_postcursor;
  wire [4:0]axi_ad9371_tx_xcvr_up_ch_3_tx_precursor;
  wire axi_ad9371_tx_xcvr_up_ch_3_user_ready;
  wire [15:0]axi_ad9371_tx_xcvr_up_ch_3_wdata;
  wire axi_ad9371_tx_xcvr_up_ch_3_wr;
  wire [11:0]axi_ad9371_tx_xcvr_up_cm_0_addr;
  wire axi_ad9371_tx_xcvr_up_cm_0_enb;
  wire [15:0]axi_ad9371_tx_xcvr_up_cm_0_rdata;
  wire axi_ad9371_tx_xcvr_up_cm_0_ready;
  wire [15:0]axi_ad9371_tx_xcvr_up_cm_0_wdata;
  wire axi_ad9371_tx_xcvr_up_cm_0_wr;
  wire axi_ad9371_tx_xcvr_up_pll_rst;
  wire [31:0]dac_data_0_0_1;
  wire [31:0]dac_data_1_0_1;
  wire [31:0]dac_data_2_0_1;
  wire [31:0]dac_data_3_0_1;
  wire dac_dunf_0_1;
  wire [31:0]rx_ad9371_tpl_core_adc_data_0;
  wire [31:0]rx_ad9371_tpl_core_adc_data_1;
  wire [31:0]rx_ad9371_tpl_core_adc_data_2;
  wire [31:0]rx_ad9371_tpl_core_adc_data_3;
  wire [0:0]rx_ad9371_tpl_core_adc_enable_0;
  wire [0:0]rx_ad9371_tpl_core_adc_enable_1;
  wire [0:0]rx_ad9371_tpl_core_adc_enable_2;
  wire [0:0]rx_ad9371_tpl_core_adc_enable_3;
  wire [0:0]rx_ad9371_tpl_core_adc_valid_0;
  wire [0:0]rx_ad9371_tpl_core_adc_valid_1;
  wire [0:0]rx_ad9371_tpl_core_adc_valid_2;
  wire [0:0]rx_ad9371_tpl_core_adc_valid_3;
  wire rx_data_0_n_1;
  wire rx_data_0_p_1;
  wire rx_data_1_n_1;
  wire rx_data_1_p_1;
  wire rx_data_2_n_1;
  wire rx_data_2_p_1;
  wire rx_data_3_n_1;
  wire rx_data_3_p_1;
  wire rx_ref_clk_0_1;
  wire [15:0]s_axi_0_1_ARADDR;
  wire [2:0]s_axi_0_1_ARPROT;
  wire s_axi_0_1_ARREADY;
  wire s_axi_0_1_ARVALID;
  wire [15:0]s_axi_0_1_AWADDR;
  wire [2:0]s_axi_0_1_AWPROT;
  wire s_axi_0_1_AWREADY;
  wire s_axi_0_1_AWVALID;
  wire s_axi_0_1_BREADY;
  wire [1:0]s_axi_0_1_BRESP;
  wire s_axi_0_1_BVALID;
  wire [31:0]s_axi_0_1_RDATA;
  wire s_axi_0_1_RREADY;
  wire [1:0]s_axi_0_1_RRESP;
  wire s_axi_0_1_RVALID;
  wire [31:0]s_axi_0_1_WDATA;
  wire s_axi_0_1_WREADY;
  wire [3:0]s_axi_0_1_WSTRB;
  wire s_axi_0_1_WVALID;
  wire [15:0]s_axi_0_2_ARADDR;
  wire [2:0]s_axi_0_2_ARPROT;
  wire s_axi_0_2_ARREADY;
  wire s_axi_0_2_ARVALID;
  wire [15:0]s_axi_0_2_AWADDR;
  wire [2:0]s_axi_0_2_AWPROT;
  wire s_axi_0_2_AWREADY;
  wire s_axi_0_2_AWVALID;
  wire s_axi_0_2_BREADY;
  wire [1:0]s_axi_0_2_BRESP;
  wire s_axi_0_2_BVALID;
  wire [31:0]s_axi_0_2_RDATA;
  wire s_axi_0_2_RREADY;
  wire [1:0]s_axi_0_2_RRESP;
  wire s_axi_0_2_RVALID;
  wire [31:0]s_axi_0_2_WDATA;
  wire s_axi_0_2_WREADY;
  wire [3:0]s_axi_0_2_WSTRB;
  wire s_axi_0_2_WVALID;
  wire [12:0]s_axi_0_3_ARADDR;
  wire [2:0]s_axi_0_3_ARPROT;
  wire s_axi_0_3_ARREADY;
  wire s_axi_0_3_ARVALID;
  wire [12:0]s_axi_0_3_AWADDR;
  wire [2:0]s_axi_0_3_AWPROT;
  wire s_axi_0_3_AWREADY;
  wire s_axi_0_3_AWVALID;
  wire s_axi_0_3_BREADY;
  wire [1:0]s_axi_0_3_BRESP;
  wire s_axi_0_3_BVALID;
  wire [31:0]s_axi_0_3_RDATA;
  wire s_axi_0_3_RREADY;
  wire [1:0]s_axi_0_3_RRESP;
  wire s_axi_0_3_RVALID;
  wire [31:0]s_axi_0_3_WDATA;
  wire s_axi_0_3_WREADY;
  wire [3:0]s_axi_0_3_WSTRB;
  wire s_axi_0_3_WVALID;
  wire [12:0]s_axi_0_4_ARADDR;
  wire [2:0]s_axi_0_4_ARPROT;
  wire s_axi_0_4_ARREADY;
  wire s_axi_0_4_ARVALID;
  wire [12:0]s_axi_0_4_AWADDR;
  wire [2:0]s_axi_0_4_AWPROT;
  wire s_axi_0_4_AWREADY;
  wire s_axi_0_4_AWVALID;
  wire s_axi_0_4_BREADY;
  wire [1:0]s_axi_0_4_BRESP;
  wire s_axi_0_4_BVALID;
  wire [31:0]s_axi_0_4_RDATA;
  wire s_axi_0_4_RREADY;
  wire [1:0]s_axi_0_4_RRESP;
  wire s_axi_0_4_RVALID;
  wire [31:0]s_axi_0_4_WDATA;
  wire s_axi_0_4_WREADY;
  wire [3:0]s_axi_0_4_WSTRB;
  wire s_axi_0_4_WVALID;
  wire [13:0]s_axi_0_5_ARADDR;
  wire [2:0]s_axi_0_5_ARPROT;
  wire s_axi_0_5_ARREADY;
  wire s_axi_0_5_ARVALID;
  wire [13:0]s_axi_0_5_AWADDR;
  wire [2:0]s_axi_0_5_AWPROT;
  wire s_axi_0_5_AWREADY;
  wire s_axi_0_5_AWVALID;
  wire s_axi_0_5_BREADY;
  wire [1:0]s_axi_0_5_BRESP;
  wire s_axi_0_5_BVALID;
  wire [31:0]s_axi_0_5_RDATA;
  wire s_axi_0_5_RREADY;
  wire [1:0]s_axi_0_5_RRESP;
  wire s_axi_0_5_RVALID;
  wire [31:0]s_axi_0_5_WDATA;
  wire s_axi_0_5_WREADY;
  wire [3:0]s_axi_0_5_WSTRB;
  wire s_axi_0_5_WVALID;
  wire [13:0]s_axi_0_6_ARADDR;
  wire [2:0]s_axi_0_6_ARPROT;
  wire s_axi_0_6_ARREADY;
  wire s_axi_0_6_ARVALID;
  wire [13:0]s_axi_0_6_AWADDR;
  wire [2:0]s_axi_0_6_AWPROT;
  wire s_axi_0_6_AWREADY;
  wire s_axi_0_6_AWVALID;
  wire s_axi_0_6_BREADY;
  wire [1:0]s_axi_0_6_BRESP;
  wire s_axi_0_6_BVALID;
  wire [31:0]s_axi_0_6_RDATA;
  wire s_axi_0_6_RREADY;
  wire [1:0]s_axi_0_6_RRESP;
  wire s_axi_0_6_RVALID;
  wire [31:0]s_axi_0_6_WDATA;
  wire s_axi_0_6_WREADY;
  wire [3:0]s_axi_0_6_WSTRB;
  wire s_axi_0_6_WVALID;
  wire [15:0]s_axi_0_7_ARADDR;
  wire [2:0]s_axi_0_7_ARPROT;
  wire s_axi_0_7_ARREADY;
  wire s_axi_0_7_ARVALID;
  wire [15:0]s_axi_0_7_AWADDR;
  wire [2:0]s_axi_0_7_AWPROT;
  wire s_axi_0_7_AWREADY;
  wire s_axi_0_7_AWVALID;
  wire s_axi_0_7_BREADY;
  wire [1:0]s_axi_0_7_BRESP;
  wire s_axi_0_7_BVALID;
  wire [31:0]s_axi_0_7_RDATA;
  wire s_axi_0_7_RREADY;
  wire [1:0]s_axi_0_7_RRESP;
  wire s_axi_0_7_RVALID;
  wire [31:0]s_axi_0_7_WDATA;
  wire s_axi_0_7_WREADY;
  wire [3:0]s_axi_0_7_WSTRB;
  wire s_axi_0_7_WVALID;
  wire [15:0]s_axi_0_8_ARADDR;
  wire [2:0]s_axi_0_8_ARPROT;
  wire s_axi_0_8_ARREADY;
  wire s_axi_0_8_ARVALID;
  wire [15:0]s_axi_0_8_AWADDR;
  wire [2:0]s_axi_0_8_AWPROT;
  wire s_axi_0_8_AWREADY;
  wire s_axi_0_8_AWVALID;
  wire s_axi_0_8_BREADY;
  wire [1:0]s_axi_0_8_BRESP;
  wire s_axi_0_8_BVALID;
  wire [31:0]s_axi_0_8_RDATA;
  wire s_axi_0_8_RREADY;
  wire [1:0]s_axi_0_8_RRESP;
  wire s_axi_0_8_RVALID;
  wire [31:0]s_axi_0_8_WDATA;
  wire s_axi_0_8_WREADY;
  wire [3:0]s_axi_0_8_WSTRB;
  wire s_axi_0_8_WVALID;
  wire s_axi_aclk_0_1;
  wire s_axi_aresetn_0_1;
  wire [0:0]sync_1;
  wire sysref_1;
  wire sysref_2;
  wire [0:0]tx_ad9371_tpl_core_dac_enable_0;
  wire [0:0]tx_ad9371_tpl_core_dac_enable_1;
  wire [0:0]tx_ad9371_tpl_core_dac_enable_2;
  wire [0:0]tx_ad9371_tpl_core_dac_enable_3;
  wire [0:0]tx_ad9371_tpl_core_dac_valid_0;
  wire [0:0]tx_ad9371_tpl_core_dac_valid_1;
  wire [0:0]tx_ad9371_tpl_core_dac_valid_2;
  wire [0:0]tx_ad9371_tpl_core_dac_valid_3;
  wire [127:0]tx_data_1_TDATA;
  wire tx_data_1_TREADY;
  wire tx_data_1_TVALID;
  wire tx_ref_clk_0_1;
  wire [3:0]util_ad9371_xcvr_rx_0_rxcharisk;
  wire [31:0]util_ad9371_xcvr_rx_0_rxdata;
  wire [3:0]util_ad9371_xcvr_rx_0_rxdisperr;
  wire [3:0]util_ad9371_xcvr_rx_0_rxnotintable;
  wire [3:0]util_ad9371_xcvr_rx_1_rxcharisk;
  wire [31:0]util_ad9371_xcvr_rx_1_rxdata;
  wire [3:0]util_ad9371_xcvr_rx_1_rxdisperr;
  wire [3:0]util_ad9371_xcvr_rx_1_rxnotintable;
  wire [3:0]util_ad9371_xcvr_rx_2_rxcharisk;
  wire [31:0]util_ad9371_xcvr_rx_2_rxdata;
  wire [3:0]util_ad9371_xcvr_rx_2_rxdisperr;
  wire [3:0]util_ad9371_xcvr_rx_2_rxnotintable;
  wire [3:0]util_ad9371_xcvr_rx_3_rxcharisk;
  wire [31:0]util_ad9371_xcvr_rx_3_rxdata;
  wire [3:0]util_ad9371_xcvr_rx_3_rxdisperr;
  wire [3:0]util_ad9371_xcvr_rx_3_rxnotintable;
  wire util_ad9371_xcvr_rx_out_clk_0;
  wire util_ad9371_xcvr_tx_0_n;
  wire util_ad9371_xcvr_tx_0_p;
  wire util_ad9371_xcvr_tx_1_n;
  wire util_ad9371_xcvr_tx_1_p;
  wire util_ad9371_xcvr_tx_2_n;
  wire util_ad9371_xcvr_tx_2_p;
  wire util_ad9371_xcvr_tx_3_n;
  wire util_ad9371_xcvr_tx_3_p;
  wire util_ad9371_xcvr_tx_out_clk_0;

  assign adc_data_0[31:0] = rx_ad9371_tpl_core_adc_data_0;
  assign adc_data_1[31:0] = rx_ad9371_tpl_core_adc_data_1;
  assign adc_data_2[31:0] = rx_ad9371_tpl_core_adc_data_2;
  assign adc_data_3[31:0] = rx_ad9371_tpl_core_adc_data_3;
  assign adc_dovf_0_1 = adc_dovf;
  assign adc_enable_0[0] = rx_ad9371_tpl_core_adc_enable_0;
  assign adc_enable_1[0] = rx_ad9371_tpl_core_adc_enable_1;
  assign adc_enable_2[0] = rx_ad9371_tpl_core_adc_enable_2;
  assign adc_enable_3[0] = rx_ad9371_tpl_core_adc_enable_3;
  assign adc_valid_0[0] = rx_ad9371_tpl_core_adc_valid_0;
  assign adc_valid_1[0] = rx_ad9371_tpl_core_adc_valid_1;
  assign adc_valid_2[0] = rx_ad9371_tpl_core_adc_valid_2;
  assign adc_valid_3[0] = rx_ad9371_tpl_core_adc_valid_3;
  assign dac_data_0_0_1 = dac_data_0[31:0];
  assign dac_data_1_0_1 = dac_data_1[31:0];
  assign dac_data_2_0_1 = dac_data_2[31:0];
  assign dac_data_3_0_1 = dac_data_3[31:0];
  assign dac_dunf_0_1 = dac_dunf;
  assign dac_enable_0[0] = tx_ad9371_tpl_core_dac_enable_0;
  assign dac_enable_1[0] = tx_ad9371_tpl_core_dac_enable_1;
  assign dac_enable_2[0] = tx_ad9371_tpl_core_dac_enable_2;
  assign dac_enable_3[0] = tx_ad9371_tpl_core_dac_enable_3;
  assign dac_valid_0[0] = tx_ad9371_tpl_core_dac_valid_0;
  assign dac_valid_1[0] = tx_ad9371_tpl_core_dac_valid_1;
  assign dac_valid_2[0] = tx_ad9371_tpl_core_dac_valid_2;
  assign dac_valid_3[0] = tx_ad9371_tpl_core_dac_valid_3;
  assign rx_data_0_n_1 = rx_data_0_n;
  assign rx_data_0_p_1 = rx_data_0_p;
  assign rx_data_1_n_1 = rx_data_1_n;
  assign rx_data_1_p_1 = rx_data_1_p;
  assign rx_data_2_n_1 = rx_data_2_n;
  assign rx_data_2_p_1 = rx_data_2_p;
  assign rx_data_3_n_1 = rx_data_3_n;
  assign rx_data_3_p_1 = rx_data_3_p;
  assign rx_ref_clk_0_1 = rx_ref_clk_0;
  assign rx_sync[0] = axi_ad9371_rx_jesd_sync;
  assign s_axi_0_1_ARADDR = s_axi_rx_adxcvr_araddr[15:0];
  assign s_axi_0_1_ARPROT = s_axi_rx_adxcvr_arprot[2:0];
  assign s_axi_0_1_ARVALID = s_axi_rx_adxcvr_arvalid;
  assign s_axi_0_1_AWADDR = s_axi_rx_adxcvr_awaddr[15:0];
  assign s_axi_0_1_AWPROT = s_axi_rx_adxcvr_awprot[2:0];
  assign s_axi_0_1_AWVALID = s_axi_rx_adxcvr_awvalid;
  assign s_axi_0_1_BREADY = s_axi_rx_adxcvr_bready;
  assign s_axi_0_1_RREADY = s_axi_rx_adxcvr_rready;
  assign s_axi_0_1_WDATA = s_axi_rx_adxcvr_wdata[31:0];
  assign s_axi_0_1_WSTRB = s_axi_rx_adxcvr_wstrb[3:0];
  assign s_axi_0_1_WVALID = s_axi_rx_adxcvr_wvalid;
  assign s_axi_0_2_ARADDR = s_axi_tx_adxcvr_araddr[15:0];
  assign s_axi_0_2_ARPROT = s_axi_tx_adxcvr_arprot[2:0];
  assign s_axi_0_2_ARVALID = s_axi_tx_adxcvr_arvalid;
  assign s_axi_0_2_AWADDR = s_axi_tx_adxcvr_awaddr[15:0];
  assign s_axi_0_2_AWPROT = s_axi_tx_adxcvr_awprot[2:0];
  assign s_axi_0_2_AWVALID = s_axi_tx_adxcvr_awvalid;
  assign s_axi_0_2_BREADY = s_axi_tx_adxcvr_bready;
  assign s_axi_0_2_RREADY = s_axi_tx_adxcvr_rready;
  assign s_axi_0_2_WDATA = s_axi_tx_adxcvr_wdata[31:0];
  assign s_axi_0_2_WSTRB = s_axi_tx_adxcvr_wstrb[3:0];
  assign s_axi_0_2_WVALID = s_axi_tx_adxcvr_wvalid;
  assign s_axi_0_3_ARADDR = s_axi_tx_tpl_araddr[12:0];
  assign s_axi_0_3_ARPROT = s_axi_tx_tpl_arprot[2:0];
  assign s_axi_0_3_ARVALID = s_axi_tx_tpl_arvalid;
  assign s_axi_0_3_AWADDR = s_axi_tx_tpl_awaddr[12:0];
  assign s_axi_0_3_AWPROT = s_axi_tx_tpl_awprot[2:0];
  assign s_axi_0_3_AWVALID = s_axi_tx_tpl_awvalid;
  assign s_axi_0_3_BREADY = s_axi_tx_tpl_bready;
  assign s_axi_0_3_RREADY = s_axi_tx_tpl_rready;
  assign s_axi_0_3_WDATA = s_axi_tx_tpl_wdata[31:0];
  assign s_axi_0_3_WSTRB = s_axi_tx_tpl_wstrb[3:0];
  assign s_axi_0_3_WVALID = s_axi_tx_tpl_wvalid;
  assign s_axi_0_4_ARADDR = s_axi_rx_tpl_araddr[12:0];
  assign s_axi_0_4_ARPROT = s_axi_rx_tpl_arprot[2:0];
  assign s_axi_0_4_ARVALID = s_axi_rx_tpl_arvalid;
  assign s_axi_0_4_AWADDR = s_axi_rx_tpl_awaddr[12:0];
  assign s_axi_0_4_AWPROT = s_axi_rx_tpl_awprot[2:0];
  assign s_axi_0_4_AWVALID = s_axi_rx_tpl_awvalid;
  assign s_axi_0_4_BREADY = s_axi_rx_tpl_bready;
  assign s_axi_0_4_RREADY = s_axi_rx_tpl_rready;
  assign s_axi_0_4_WDATA = s_axi_rx_tpl_wdata[31:0];
  assign s_axi_0_4_WSTRB = s_axi_rx_tpl_wstrb[3:0];
  assign s_axi_0_4_WVALID = s_axi_rx_tpl_wvalid;
  assign s_axi_0_5_ARADDR = s_axi_tx_jesd_araddr[13:0];
  assign s_axi_0_5_ARPROT = s_axi_tx_jesd_arprot[2:0];
  assign s_axi_0_5_ARVALID = s_axi_tx_jesd_arvalid;
  assign s_axi_0_5_AWADDR = s_axi_tx_jesd_awaddr[13:0];
  assign s_axi_0_5_AWPROT = s_axi_tx_jesd_awprot[2:0];
  assign s_axi_0_5_AWVALID = s_axi_tx_jesd_awvalid;
  assign s_axi_0_5_BREADY = s_axi_tx_jesd_bready;
  assign s_axi_0_5_RREADY = s_axi_tx_jesd_rready;
  assign s_axi_0_5_WDATA = s_axi_tx_jesd_wdata[31:0];
  assign s_axi_0_5_WSTRB = s_axi_tx_jesd_wstrb[3:0];
  assign s_axi_0_5_WVALID = s_axi_tx_jesd_wvalid;
  assign s_axi_0_6_ARADDR = s_axi_rx_jesd_araddr[13:0];
  assign s_axi_0_6_ARPROT = s_axi_rx_jesd_arprot[2:0];
  assign s_axi_0_6_ARVALID = s_axi_rx_jesd_arvalid;
  assign s_axi_0_6_AWADDR = s_axi_rx_jesd_awaddr[13:0];
  assign s_axi_0_6_AWPROT = s_axi_rx_jesd_awprot[2:0];
  assign s_axi_0_6_AWVALID = s_axi_rx_jesd_awvalid;
  assign s_axi_0_6_BREADY = s_axi_rx_jesd_bready;
  assign s_axi_0_6_RREADY = s_axi_rx_jesd_rready;
  assign s_axi_0_6_WDATA = s_axi_rx_jesd_wdata[31:0];
  assign s_axi_0_6_WSTRB = s_axi_rx_jesd_wstrb[3:0];
  assign s_axi_0_6_WVALID = s_axi_rx_jesd_wvalid;
  assign s_axi_0_7_ARADDR = s_axi_tx_clkgen_araddr[15:0];
  assign s_axi_0_7_ARPROT = s_axi_tx_clkgen_arprot[2:0];
  assign s_axi_0_7_ARVALID = s_axi_tx_clkgen_arvalid;
  assign s_axi_0_7_AWADDR = s_axi_tx_clkgen_awaddr[15:0];
  assign s_axi_0_7_AWPROT = s_axi_tx_clkgen_awprot[2:0];
  assign s_axi_0_7_AWVALID = s_axi_tx_clkgen_awvalid;
  assign s_axi_0_7_BREADY = s_axi_tx_clkgen_bready;
  assign s_axi_0_7_RREADY = s_axi_tx_clkgen_rready;
  assign s_axi_0_7_WDATA = s_axi_tx_clkgen_wdata[31:0];
  assign s_axi_0_7_WSTRB = s_axi_tx_clkgen_wstrb[3:0];
  assign s_axi_0_7_WVALID = s_axi_tx_clkgen_wvalid;
  assign s_axi_0_8_ARADDR = s_axi_rx_clkgen_araddr[15:0];
  assign s_axi_0_8_ARPROT = s_axi_rx_clkgen_arprot[2:0];
  assign s_axi_0_8_ARVALID = s_axi_rx_clkgen_arvalid;
  assign s_axi_0_8_AWADDR = s_axi_rx_clkgen_awaddr[15:0];
  assign s_axi_0_8_AWPROT = s_axi_rx_clkgen_awprot[2:0];
  assign s_axi_0_8_AWVALID = s_axi_rx_clkgen_awvalid;
  assign s_axi_0_8_BREADY = s_axi_rx_clkgen_bready;
  assign s_axi_0_8_RREADY = s_axi_rx_clkgen_rready;
  assign s_axi_0_8_WDATA = s_axi_rx_clkgen_wdata[31:0];
  assign s_axi_0_8_WSTRB = s_axi_rx_clkgen_wstrb[3:0];
  assign s_axi_0_8_WVALID = s_axi_rx_clkgen_wvalid;
  assign s_axi_aclk_0_1 = s_axi_aclk;
  assign s_axi_aresetn_0_1 = s_axi_aresetn;
  assign s_axi_rx_adxcvr_arready = s_axi_0_1_ARREADY;
  assign s_axi_rx_adxcvr_awready = s_axi_0_1_AWREADY;
  assign s_axi_rx_adxcvr_bresp[1:0] = s_axi_0_1_BRESP;
  assign s_axi_rx_adxcvr_bvalid = s_axi_0_1_BVALID;
  assign s_axi_rx_adxcvr_rdata[31:0] = s_axi_0_1_RDATA;
  assign s_axi_rx_adxcvr_rresp[1:0] = s_axi_0_1_RRESP;
  assign s_axi_rx_adxcvr_rvalid = s_axi_0_1_RVALID;
  assign s_axi_rx_adxcvr_wready = s_axi_0_1_WREADY;
  assign s_axi_rx_clkgen_arready = s_axi_0_8_ARREADY;
  assign s_axi_rx_clkgen_awready = s_axi_0_8_AWREADY;
  assign s_axi_rx_clkgen_bresp[1:0] = s_axi_0_8_BRESP;
  assign s_axi_rx_clkgen_bvalid = s_axi_0_8_BVALID;
  assign s_axi_rx_clkgen_rdata[31:0] = s_axi_0_8_RDATA;
  assign s_axi_rx_clkgen_rresp[1:0] = s_axi_0_8_RRESP;
  assign s_axi_rx_clkgen_rvalid = s_axi_0_8_RVALID;
  assign s_axi_rx_clkgen_wready = s_axi_0_8_WREADY;
  assign s_axi_rx_jesd_arready = s_axi_0_6_ARREADY;
  assign s_axi_rx_jesd_awready = s_axi_0_6_AWREADY;
  assign s_axi_rx_jesd_bresp[1:0] = s_axi_0_6_BRESP;
  assign s_axi_rx_jesd_bvalid = s_axi_0_6_BVALID;
  assign s_axi_rx_jesd_rdata[31:0] = s_axi_0_6_RDATA;
  assign s_axi_rx_jesd_rresp[1:0] = s_axi_0_6_RRESP;
  assign s_axi_rx_jesd_rvalid = s_axi_0_6_RVALID;
  assign s_axi_rx_jesd_wready = s_axi_0_6_WREADY;
  assign s_axi_rx_tpl_arready = s_axi_0_4_ARREADY;
  assign s_axi_rx_tpl_awready = s_axi_0_4_AWREADY;
  assign s_axi_rx_tpl_bresp[1:0] = s_axi_0_4_BRESP;
  assign s_axi_rx_tpl_bvalid = s_axi_0_4_BVALID;
  assign s_axi_rx_tpl_rdata[31:0] = s_axi_0_4_RDATA;
  assign s_axi_rx_tpl_rresp[1:0] = s_axi_0_4_RRESP;
  assign s_axi_rx_tpl_rvalid = s_axi_0_4_RVALID;
  assign s_axi_rx_tpl_wready = s_axi_0_4_WREADY;
  assign s_axi_tx_adxcvr_arready = s_axi_0_2_ARREADY;
  assign s_axi_tx_adxcvr_awready = s_axi_0_2_AWREADY;
  assign s_axi_tx_adxcvr_bresp[1:0] = s_axi_0_2_BRESP;
  assign s_axi_tx_adxcvr_bvalid = s_axi_0_2_BVALID;
  assign s_axi_tx_adxcvr_rdata[31:0] = s_axi_0_2_RDATA;
  assign s_axi_tx_adxcvr_rresp[1:0] = s_axi_0_2_RRESP;
  assign s_axi_tx_adxcvr_rvalid = s_axi_0_2_RVALID;
  assign s_axi_tx_adxcvr_wready = s_axi_0_2_WREADY;
  assign s_axi_tx_clkgen_arready = s_axi_0_7_ARREADY;
  assign s_axi_tx_clkgen_awready = s_axi_0_7_AWREADY;
  assign s_axi_tx_clkgen_bresp[1:0] = s_axi_0_7_BRESP;
  assign s_axi_tx_clkgen_bvalid = s_axi_0_7_BVALID;
  assign s_axi_tx_clkgen_rdata[31:0] = s_axi_0_7_RDATA;
  assign s_axi_tx_clkgen_rresp[1:0] = s_axi_0_7_RRESP;
  assign s_axi_tx_clkgen_rvalid = s_axi_0_7_RVALID;
  assign s_axi_tx_clkgen_wready = s_axi_0_7_WREADY;
  assign s_axi_tx_jesd_arready = s_axi_0_5_ARREADY;
  assign s_axi_tx_jesd_awready = s_axi_0_5_AWREADY;
  assign s_axi_tx_jesd_bresp[1:0] = s_axi_0_5_BRESP;
  assign s_axi_tx_jesd_bvalid = s_axi_0_5_BVALID;
  assign s_axi_tx_jesd_rdata[31:0] = s_axi_0_5_RDATA;
  assign s_axi_tx_jesd_rresp[1:0] = s_axi_0_5_RRESP;
  assign s_axi_tx_jesd_rvalid = s_axi_0_5_RVALID;
  assign s_axi_tx_jesd_wready = s_axi_0_5_WREADY;
  assign s_axi_tx_tpl_arready = s_axi_0_3_ARREADY;
  assign s_axi_tx_tpl_awready = s_axi_0_3_AWREADY;
  assign s_axi_tx_tpl_bresp[1:0] = s_axi_0_3_BRESP;
  assign s_axi_tx_tpl_bvalid = s_axi_0_3_BVALID;
  assign s_axi_tx_tpl_rdata[31:0] = s_axi_0_3_RDATA;
  assign s_axi_tx_tpl_rresp[1:0] = s_axi_0_3_RRESP;
  assign s_axi_tx_tpl_rvalid = s_axi_0_3_RVALID;
  assign s_axi_tx_tpl_wready = s_axi_0_3_WREADY;
  assign sync_1 = tx_sync[0];
  assign sysref_1 = tx_sysref_0;
  assign sysref_2 = rx_sysref_0;
  assign tx_data_0_n = util_ad9371_xcvr_tx_0_n;
  assign tx_data_0_p = util_ad9371_xcvr_tx_0_p;
  assign tx_data_1_n = util_ad9371_xcvr_tx_1_n;
  assign tx_data_1_p = util_ad9371_xcvr_tx_1_p;
  assign tx_data_2_n = util_ad9371_xcvr_tx_2_n;
  assign tx_data_2_p = util_ad9371_xcvr_tx_2_p;
  assign tx_data_3_n = util_ad9371_xcvr_tx_3_n;
  assign tx_data_3_p = util_ad9371_xcvr_tx_3_p;
  assign tx_ref_clk_0_1 = tx_ref_clk_0;
  system_axi_ad9371_rx_clkgen_0 axi_ad9371_rx_clkgen
       (.clk(util_ad9371_xcvr_rx_out_clk_0),
        .clk_0(ad9371_rx_device_clk),
        .s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_8_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_8_ARPROT),
        .s_axi_arready(s_axi_0_8_ARREADY),
        .s_axi_arvalid(s_axi_0_8_ARVALID),
        .s_axi_awaddr(s_axi_0_8_AWADDR),
        .s_axi_awprot(s_axi_0_8_AWPROT),
        .s_axi_awready(s_axi_0_8_AWREADY),
        .s_axi_awvalid(s_axi_0_8_AWVALID),
        .s_axi_bready(s_axi_0_8_BREADY),
        .s_axi_bresp(s_axi_0_8_BRESP),
        .s_axi_bvalid(s_axi_0_8_BVALID),
        .s_axi_rdata(s_axi_0_8_RDATA),
        .s_axi_rready(s_axi_0_8_RREADY),
        .s_axi_rresp(s_axi_0_8_RRESP),
        .s_axi_rvalid(s_axi_0_8_RVALID),
        .s_axi_wdata(s_axi_0_8_WDATA),
        .s_axi_wready(s_axi_0_8_WREADY),
        .s_axi_wstrb(s_axi_0_8_WSTRB),
        .s_axi_wvalid(s_axi_0_8_WVALID));
  axi_ad9371_rx_jesd_imp_GUTUBY axi_ad9371_rx_jesd
       (.device_clk(ad9371_rx_device_clk),
        .phy_en_char_align(axi_ad9371_rx_jesd_phy_en_char_align),
        .rx_data_tdata(axi_ad9371_rx_jesd_rx_data_tdata),
        .rx_data_tvalid(axi_ad9371_rx_jesd_rx_data_tvalid),
        .rx_phy0_rxcharisk(util_ad9371_xcvr_rx_0_rxcharisk),
        .rx_phy0_rxdata(util_ad9371_xcvr_rx_0_rxdata),
        .rx_phy0_rxdisperr(util_ad9371_xcvr_rx_0_rxdisperr),
        .rx_phy0_rxnotintable(util_ad9371_xcvr_rx_0_rxnotintable),
        .rx_phy1_rxcharisk(util_ad9371_xcvr_rx_1_rxcharisk),
        .rx_phy1_rxdata(util_ad9371_xcvr_rx_1_rxdata),
        .rx_phy1_rxdisperr(util_ad9371_xcvr_rx_1_rxdisperr),
        .rx_phy1_rxnotintable(util_ad9371_xcvr_rx_1_rxnotintable),
        .rx_phy2_rxcharisk(util_ad9371_xcvr_rx_2_rxcharisk),
        .rx_phy2_rxdata(util_ad9371_xcvr_rx_2_rxdata),
        .rx_phy2_rxdisperr(util_ad9371_xcvr_rx_2_rxdisperr),
        .rx_phy2_rxnotintable(util_ad9371_xcvr_rx_2_rxnotintable),
        .rx_phy3_rxcharisk(util_ad9371_xcvr_rx_3_rxcharisk),
        .rx_phy3_rxdata(util_ad9371_xcvr_rx_3_rxdata),
        .rx_phy3_rxdisperr(util_ad9371_xcvr_rx_3_rxdisperr),
        .rx_phy3_rxnotintable(util_ad9371_xcvr_rx_3_rxnotintable),
        .rx_sof(axi_ad9371_rx_jesd_rx_sof),
        .s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_6_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_6_ARPROT),
        .s_axi_arready(s_axi_0_6_ARREADY),
        .s_axi_arvalid(s_axi_0_6_ARVALID),
        .s_axi_awaddr(s_axi_0_6_AWADDR),
        .s_axi_awprot(s_axi_0_6_AWPROT),
        .s_axi_awready(s_axi_0_6_AWREADY),
        .s_axi_awvalid(s_axi_0_6_AWVALID),
        .s_axi_bready(s_axi_0_6_BREADY),
        .s_axi_bresp(s_axi_0_6_BRESP),
        .s_axi_bvalid(s_axi_0_6_BVALID),
        .s_axi_rdata(s_axi_0_6_RDATA),
        .s_axi_rready(s_axi_0_6_RREADY),
        .s_axi_rresp(s_axi_0_6_RRESP),
        .s_axi_rvalid(s_axi_0_6_RVALID),
        .s_axi_wdata(s_axi_0_6_WDATA),
        .s_axi_wready(s_axi_0_6_WREADY),
        .s_axi_wstrb(s_axi_0_6_WSTRB),
        .s_axi_wvalid(s_axi_0_6_WVALID),
        .sync(axi_ad9371_rx_jesd_sync),
        .sysref(sysref_2));
  system_axi_ad9371_rx_xcvr_0 axi_ad9371_rx_xcvr
       (.m_axi_arready(1'b0),
        .m_axi_awready(1'b0),
        .m_axi_bresp({1'b0,1'b0}),
        .m_axi_bvalid(1'b0),
        .m_axi_rdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .m_axi_rresp({1'b0,1'b0}),
        .m_axi_rvalid(1'b0),
        .m_axi_wready(1'b0),
        .s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_1_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_1_ARPROT),
        .s_axi_arready(s_axi_0_1_ARREADY),
        .s_axi_arvalid(s_axi_0_1_ARVALID),
        .s_axi_awaddr(s_axi_0_1_AWADDR),
        .s_axi_awprot(s_axi_0_1_AWPROT),
        .s_axi_awready(s_axi_0_1_AWREADY),
        .s_axi_awvalid(s_axi_0_1_AWVALID),
        .s_axi_bready(s_axi_0_1_BREADY),
        .s_axi_bresp(s_axi_0_1_BRESP),
        .s_axi_bvalid(s_axi_0_1_BVALID),
        .s_axi_rdata(s_axi_0_1_RDATA),
        .s_axi_rready(s_axi_0_1_RREADY),
        .s_axi_rresp(s_axi_0_1_RRESP),
        .s_axi_rvalid(s_axi_0_1_RVALID),
        .s_axi_wdata(s_axi_0_1_WDATA),
        .s_axi_wready(s_axi_0_1_WREADY),
        .s_axi_wstrb(s_axi_0_1_WSTRB),
        .s_axi_wvalid(s_axi_0_1_WVALID),
        .up_ch_addr_0(axi_ad9371_rx_xcvr_up_ch_0_addr),
        .up_ch_addr_1(axi_ad9371_rx_xcvr_up_ch_1_addr),
        .up_ch_enb_0(axi_ad9371_rx_xcvr_up_ch_0_enb),
        .up_ch_enb_1(axi_ad9371_rx_xcvr_up_ch_1_enb),
        .up_ch_lpm_dfe_n_0(axi_ad9371_rx_xcvr_up_ch_0_lpm_dfe_n),
        .up_ch_lpm_dfe_n_1(axi_ad9371_rx_xcvr_up_ch_1_lpm_dfe_n),
        .up_ch_out_clk_sel_0(axi_ad9371_rx_xcvr_up_ch_0_out_clk_sel),
        .up_ch_out_clk_sel_1(axi_ad9371_rx_xcvr_up_ch_1_out_clk_sel),
        .up_ch_pll_locked_0(axi_ad9371_rx_xcvr_up_ch_0_pll_locked),
        .up_ch_pll_locked_1(axi_ad9371_rx_xcvr_up_ch_1_pll_locked),
        .up_ch_rate_0(axi_ad9371_rx_xcvr_up_ch_0_rate),
        .up_ch_rate_1(axi_ad9371_rx_xcvr_up_ch_1_rate),
        .up_ch_rdata_0(axi_ad9371_rx_xcvr_up_ch_0_rdata),
        .up_ch_rdata_1(axi_ad9371_rx_xcvr_up_ch_1_rdata),
        .up_ch_ready_0(axi_ad9371_rx_xcvr_up_ch_0_ready),
        .up_ch_ready_1(axi_ad9371_rx_xcvr_up_ch_1_ready),
        .up_ch_rst_0(axi_ad9371_rx_xcvr_up_ch_0_rst),
        .up_ch_rst_1(axi_ad9371_rx_xcvr_up_ch_1_rst),
        .up_ch_rst_done_0(axi_ad9371_rx_xcvr_up_ch_0_rst_done),
        .up_ch_rst_done_1(axi_ad9371_rx_xcvr_up_ch_1_rst_done),
        .up_ch_sys_clk_sel_0(axi_ad9371_rx_xcvr_up_ch_0_sys_clk_sel),
        .up_ch_sys_clk_sel_1(axi_ad9371_rx_xcvr_up_ch_1_sys_clk_sel),
        .up_ch_user_ready_0(axi_ad9371_rx_xcvr_up_ch_0_user_ready),
        .up_ch_user_ready_1(axi_ad9371_rx_xcvr_up_ch_1_user_ready),
        .up_ch_wdata_0(axi_ad9371_rx_xcvr_up_ch_0_wdata),
        .up_ch_wdata_1(axi_ad9371_rx_xcvr_up_ch_1_wdata),
        .up_ch_wr_0(axi_ad9371_rx_xcvr_up_ch_0_wr),
        .up_ch_wr_1(axi_ad9371_rx_xcvr_up_ch_1_wr),
        .up_es_addr_0(axi_ad9371_rx_xcvr_up_es_0_addr),
        .up_es_addr_1(axi_ad9371_rx_xcvr_up_es_1_addr),
        .up_es_enb_0(axi_ad9371_rx_xcvr_up_es_0_enb),
        .up_es_enb_1(axi_ad9371_rx_xcvr_up_es_1_enb),
        .up_es_rdata_0(axi_ad9371_rx_xcvr_up_es_0_rdata),
        .up_es_rdata_1(axi_ad9371_rx_xcvr_up_es_1_rdata),
        .up_es_ready_0(axi_ad9371_rx_xcvr_up_es_0_ready),
        .up_es_ready_1(axi_ad9371_rx_xcvr_up_es_1_ready),
        .up_es_reset_0(axi_ad9371_rx_xcvr_up_es_0_reset),
        .up_es_reset_1(axi_ad9371_rx_xcvr_up_es_1_reset),
        .up_es_wdata_0(axi_ad9371_rx_xcvr_up_es_0_wdata),
        .up_es_wdata_1(axi_ad9371_rx_xcvr_up_es_1_wdata),
        .up_es_wr_0(axi_ad9371_rx_xcvr_up_es_0_wr),
        .up_es_wr_1(axi_ad9371_rx_xcvr_up_es_1_wr),
        .up_pll_rst(axi_ad9371_rx_xcvr_up_pll_rst));
  system_axi_ad9371_tx_clkgen_0 axi_ad9371_tx_clkgen
       (.clk(util_ad9371_xcvr_tx_out_clk_0),
        .clk_0(ad9371_tx_device_clk),
        .s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_7_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_7_ARPROT),
        .s_axi_arready(s_axi_0_7_ARREADY),
        .s_axi_arvalid(s_axi_0_7_ARVALID),
        .s_axi_awaddr(s_axi_0_7_AWADDR),
        .s_axi_awprot(s_axi_0_7_AWPROT),
        .s_axi_awready(s_axi_0_7_AWREADY),
        .s_axi_awvalid(s_axi_0_7_AWVALID),
        .s_axi_bready(s_axi_0_7_BREADY),
        .s_axi_bresp(s_axi_0_7_BRESP),
        .s_axi_bvalid(s_axi_0_7_BVALID),
        .s_axi_rdata(s_axi_0_7_RDATA),
        .s_axi_rready(s_axi_0_7_RREADY),
        .s_axi_rresp(s_axi_0_7_RRESP),
        .s_axi_rvalid(s_axi_0_7_RVALID),
        .s_axi_wdata(s_axi_0_7_WDATA),
        .s_axi_wready(s_axi_0_7_WREADY),
        .s_axi_wstrb(s_axi_0_7_WSTRB),
        .s_axi_wvalid(s_axi_0_7_WVALID));
  axi_ad9371_tx_jesd_imp_17BPCLV axi_ad9371_tx_jesd
       (.device_clk(ad9371_tx_device_clk),
        .s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_5_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_5_ARPROT),
        .s_axi_arready(s_axi_0_5_ARREADY),
        .s_axi_arvalid(s_axi_0_5_ARVALID),
        .s_axi_awaddr(s_axi_0_5_AWADDR),
        .s_axi_awprot(s_axi_0_5_AWPROT),
        .s_axi_awready(s_axi_0_5_AWREADY),
        .s_axi_awvalid(s_axi_0_5_AWVALID),
        .s_axi_bready(s_axi_0_5_BREADY),
        .s_axi_bresp(s_axi_0_5_BRESP),
        .s_axi_bvalid(s_axi_0_5_BVALID),
        .s_axi_rdata(s_axi_0_5_RDATA),
        .s_axi_rready(s_axi_0_5_RREADY),
        .s_axi_rresp(s_axi_0_5_RRESP),
        .s_axi_rvalid(s_axi_0_5_RVALID),
        .s_axi_wdata(s_axi_0_5_WDATA),
        .s_axi_wready(s_axi_0_5_WREADY),
        .s_axi_wstrb(s_axi_0_5_WSTRB),
        .s_axi_wvalid(s_axi_0_5_WVALID),
        .sync(sync_1),
        .sysref(sysref_1),
        .tx_data_tdata(tx_data_1_TDATA),
        .tx_data_tready(tx_data_1_TREADY),
        .tx_data_tvalid(tx_data_1_TVALID),
        .tx_phy0_txcharisk(axi_ad9371_tx_jesd_tx_phy0_txcharisk),
        .tx_phy0_txdata(axi_ad9371_tx_jesd_tx_phy0_txdata),
        .tx_phy1_txcharisk(axi_ad9371_tx_jesd_tx_phy1_txcharisk),
        .tx_phy1_txdata(axi_ad9371_tx_jesd_tx_phy1_txdata),
        .tx_phy2_txcharisk(axi_ad9371_tx_jesd_tx_phy2_txcharisk),
        .tx_phy2_txdata(axi_ad9371_tx_jesd_tx_phy2_txdata),
        .tx_phy3_txcharisk(axi_ad9371_tx_jesd_tx_phy3_txcharisk),
        .tx_phy3_txdata(axi_ad9371_tx_jesd_tx_phy3_txdata));
  system_axi_ad9371_tx_xcvr_0 axi_ad9371_tx_xcvr
       (.s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_2_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_2_ARPROT),
        .s_axi_arready(s_axi_0_2_ARREADY),
        .s_axi_arvalid(s_axi_0_2_ARVALID),
        .s_axi_awaddr(s_axi_0_2_AWADDR),
        .s_axi_awprot(s_axi_0_2_AWPROT),
        .s_axi_awready(s_axi_0_2_AWREADY),
        .s_axi_awvalid(s_axi_0_2_AWVALID),
        .s_axi_bready(s_axi_0_2_BREADY),
        .s_axi_bresp(s_axi_0_2_BRESP),
        .s_axi_bvalid(s_axi_0_2_BVALID),
        .s_axi_rdata(s_axi_0_2_RDATA),
        .s_axi_rready(s_axi_0_2_RREADY),
        .s_axi_rresp(s_axi_0_2_RRESP),
        .s_axi_rvalid(s_axi_0_2_RVALID),
        .s_axi_wdata(s_axi_0_2_WDATA),
        .s_axi_wready(s_axi_0_2_WREADY),
        .s_axi_wstrb(s_axi_0_2_WSTRB),
        .s_axi_wvalid(s_axi_0_2_WVALID),
        .up_ch_addr_0(axi_ad9371_tx_xcvr_up_ch_0_addr),
        .up_ch_addr_1(axi_ad9371_tx_xcvr_up_ch_1_addr),
        .up_ch_addr_2(axi_ad9371_tx_xcvr_up_ch_2_addr),
        .up_ch_addr_3(axi_ad9371_tx_xcvr_up_ch_3_addr),
        .up_ch_enb_0(axi_ad9371_tx_xcvr_up_ch_0_enb),
        .up_ch_enb_1(axi_ad9371_tx_xcvr_up_ch_1_enb),
        .up_ch_enb_2(axi_ad9371_tx_xcvr_up_ch_2_enb),
        .up_ch_enb_3(axi_ad9371_tx_xcvr_up_ch_3_enb),
        .up_ch_lpm_dfe_n_0(axi_ad9371_tx_xcvr_up_ch_0_lpm_dfe_n),
        .up_ch_lpm_dfe_n_1(axi_ad9371_tx_xcvr_up_ch_1_lpm_dfe_n),
        .up_ch_lpm_dfe_n_2(axi_ad9371_tx_xcvr_up_ch_2_lpm_dfe_n),
        .up_ch_lpm_dfe_n_3(axi_ad9371_tx_xcvr_up_ch_3_lpm_dfe_n),
        .up_ch_out_clk_sel_0(axi_ad9371_tx_xcvr_up_ch_0_out_clk_sel),
        .up_ch_out_clk_sel_1(axi_ad9371_tx_xcvr_up_ch_1_out_clk_sel),
        .up_ch_out_clk_sel_2(axi_ad9371_tx_xcvr_up_ch_2_out_clk_sel),
        .up_ch_out_clk_sel_3(axi_ad9371_tx_xcvr_up_ch_3_out_clk_sel),
        .up_ch_pll_locked_0(axi_ad9371_tx_xcvr_up_ch_0_pll_locked),
        .up_ch_pll_locked_1(axi_ad9371_tx_xcvr_up_ch_1_pll_locked),
        .up_ch_pll_locked_2(axi_ad9371_tx_xcvr_up_ch_2_pll_locked),
        .up_ch_pll_locked_3(axi_ad9371_tx_xcvr_up_ch_3_pll_locked),
        .up_ch_rate_0(axi_ad9371_tx_xcvr_up_ch_0_rate),
        .up_ch_rate_1(axi_ad9371_tx_xcvr_up_ch_1_rate),
        .up_ch_rate_2(axi_ad9371_tx_xcvr_up_ch_2_rate),
        .up_ch_rate_3(axi_ad9371_tx_xcvr_up_ch_3_rate),
        .up_ch_rdata_0(axi_ad9371_tx_xcvr_up_ch_0_rdata),
        .up_ch_rdata_1(axi_ad9371_tx_xcvr_up_ch_1_rdata),
        .up_ch_rdata_2(axi_ad9371_tx_xcvr_up_ch_2_rdata),
        .up_ch_rdata_3(axi_ad9371_tx_xcvr_up_ch_3_rdata),
        .up_ch_ready_0(axi_ad9371_tx_xcvr_up_ch_0_ready),
        .up_ch_ready_1(axi_ad9371_tx_xcvr_up_ch_1_ready),
        .up_ch_ready_2(axi_ad9371_tx_xcvr_up_ch_2_ready),
        .up_ch_ready_3(axi_ad9371_tx_xcvr_up_ch_3_ready),
        .up_ch_rst_0(axi_ad9371_tx_xcvr_up_ch_0_rst),
        .up_ch_rst_1(axi_ad9371_tx_xcvr_up_ch_1_rst),
        .up_ch_rst_2(axi_ad9371_tx_xcvr_up_ch_2_rst),
        .up_ch_rst_3(axi_ad9371_tx_xcvr_up_ch_3_rst),
        .up_ch_rst_done_0(axi_ad9371_tx_xcvr_up_ch_0_rst_done),
        .up_ch_rst_done_1(axi_ad9371_tx_xcvr_up_ch_1_rst_done),
        .up_ch_rst_done_2(axi_ad9371_tx_xcvr_up_ch_2_rst_done),
        .up_ch_rst_done_3(axi_ad9371_tx_xcvr_up_ch_3_rst_done),
        .up_ch_sys_clk_sel_0(axi_ad9371_tx_xcvr_up_ch_0_sys_clk_sel),
        .up_ch_sys_clk_sel_1(axi_ad9371_tx_xcvr_up_ch_1_sys_clk_sel),
        .up_ch_sys_clk_sel_2(axi_ad9371_tx_xcvr_up_ch_2_sys_clk_sel),
        .up_ch_sys_clk_sel_3(axi_ad9371_tx_xcvr_up_ch_3_sys_clk_sel),
        .up_ch_tx_diffctrl_0(axi_ad9371_tx_xcvr_up_ch_0_tx_diffctrl),
        .up_ch_tx_diffctrl_1(axi_ad9371_tx_xcvr_up_ch_1_tx_diffctrl),
        .up_ch_tx_diffctrl_2(axi_ad9371_tx_xcvr_up_ch_2_tx_diffctrl),
        .up_ch_tx_diffctrl_3(axi_ad9371_tx_xcvr_up_ch_3_tx_diffctrl),
        .up_ch_tx_postcursor_0(axi_ad9371_tx_xcvr_up_ch_0_tx_postcursor),
        .up_ch_tx_postcursor_1(axi_ad9371_tx_xcvr_up_ch_1_tx_postcursor),
        .up_ch_tx_postcursor_2(axi_ad9371_tx_xcvr_up_ch_2_tx_postcursor),
        .up_ch_tx_postcursor_3(axi_ad9371_tx_xcvr_up_ch_3_tx_postcursor),
        .up_ch_tx_precursor_0(axi_ad9371_tx_xcvr_up_ch_0_tx_precursor),
        .up_ch_tx_precursor_1(axi_ad9371_tx_xcvr_up_ch_1_tx_precursor),
        .up_ch_tx_precursor_2(axi_ad9371_tx_xcvr_up_ch_2_tx_precursor),
        .up_ch_tx_precursor_3(axi_ad9371_tx_xcvr_up_ch_3_tx_precursor),
        .up_ch_user_ready_0(axi_ad9371_tx_xcvr_up_ch_0_user_ready),
        .up_ch_user_ready_1(axi_ad9371_tx_xcvr_up_ch_1_user_ready),
        .up_ch_user_ready_2(axi_ad9371_tx_xcvr_up_ch_2_user_ready),
        .up_ch_user_ready_3(axi_ad9371_tx_xcvr_up_ch_3_user_ready),
        .up_ch_wdata_0(axi_ad9371_tx_xcvr_up_ch_0_wdata),
        .up_ch_wdata_1(axi_ad9371_tx_xcvr_up_ch_1_wdata),
        .up_ch_wdata_2(axi_ad9371_tx_xcvr_up_ch_2_wdata),
        .up_ch_wdata_3(axi_ad9371_tx_xcvr_up_ch_3_wdata),
        .up_ch_wr_0(axi_ad9371_tx_xcvr_up_ch_0_wr),
        .up_ch_wr_1(axi_ad9371_tx_xcvr_up_ch_1_wr),
        .up_ch_wr_2(axi_ad9371_tx_xcvr_up_ch_2_wr),
        .up_ch_wr_3(axi_ad9371_tx_xcvr_up_ch_3_wr),
        .up_cm_addr_0(axi_ad9371_tx_xcvr_up_cm_0_addr),
        .up_cm_enb_0(axi_ad9371_tx_xcvr_up_cm_0_enb),
        .up_cm_rdata_0(axi_ad9371_tx_xcvr_up_cm_0_rdata),
        .up_cm_ready_0(axi_ad9371_tx_xcvr_up_cm_0_ready),
        .up_cm_wdata_0(axi_ad9371_tx_xcvr_up_cm_0_wdata),
        .up_cm_wr_0(axi_ad9371_tx_xcvr_up_cm_0_wr),
        .up_pll_rst(axi_ad9371_tx_xcvr_up_pll_rst));
  rx_ad9371_tpl_core_imp_WW7IEM rx_ad9371_tpl_core
       (.adc_data_0(rx_ad9371_tpl_core_adc_data_0),
        .adc_data_1(rx_ad9371_tpl_core_adc_data_1),
        .adc_data_2(rx_ad9371_tpl_core_adc_data_2),
        .adc_data_3(rx_ad9371_tpl_core_adc_data_3),
        .adc_dovf(adc_dovf_0_1),
        .adc_enable_0(rx_ad9371_tpl_core_adc_enable_0),
        .adc_enable_1(rx_ad9371_tpl_core_adc_enable_1),
        .adc_enable_2(rx_ad9371_tpl_core_adc_enable_2),
        .adc_enable_3(rx_ad9371_tpl_core_adc_enable_3),
        .adc_valid_0(rx_ad9371_tpl_core_adc_valid_0),
        .adc_valid_1(rx_ad9371_tpl_core_adc_valid_1),
        .adc_valid_2(rx_ad9371_tpl_core_adc_valid_2),
        .adc_valid_3(rx_ad9371_tpl_core_adc_valid_3),
        .link_clk(ad9371_rx_device_clk),
        .link_data(axi_ad9371_rx_jesd_rx_data_tdata),
        .link_sof(axi_ad9371_rx_jesd_rx_sof),
        .link_valid(axi_ad9371_rx_jesd_rx_data_tvalid),
        .s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_4_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_4_ARPROT),
        .s_axi_arready(s_axi_0_4_ARREADY),
        .s_axi_arvalid(s_axi_0_4_ARVALID),
        .s_axi_awaddr(s_axi_0_4_AWADDR),
        .s_axi_awprot(s_axi_0_4_AWPROT),
        .s_axi_awready(s_axi_0_4_AWREADY),
        .s_axi_awvalid(s_axi_0_4_AWVALID),
        .s_axi_bready(s_axi_0_4_BREADY),
        .s_axi_bresp(s_axi_0_4_BRESP),
        .s_axi_bvalid(s_axi_0_4_BVALID),
        .s_axi_rdata(s_axi_0_4_RDATA),
        .s_axi_rready(s_axi_0_4_RREADY),
        .s_axi_rresp(s_axi_0_4_RRESP),
        .s_axi_rvalid(s_axi_0_4_RVALID),
        .s_axi_wdata(s_axi_0_4_WDATA),
        .s_axi_wready(s_axi_0_4_WREADY),
        .s_axi_wstrb(s_axi_0_4_WSTRB),
        .s_axi_wvalid(s_axi_0_4_WVALID));
  tx_ad9371_tpl_core_imp_1J4NRGE tx_ad9371_tpl_core
       (.dac_data_0(dac_data_0_0_1),
        .dac_data_1(dac_data_1_0_1),
        .dac_data_2(dac_data_2_0_1),
        .dac_data_3(dac_data_3_0_1),
        .dac_dunf(dac_dunf_0_1),
        .dac_enable_0(tx_ad9371_tpl_core_dac_enable_0),
        .dac_enable_1(tx_ad9371_tpl_core_dac_enable_1),
        .dac_enable_2(tx_ad9371_tpl_core_dac_enable_2),
        .dac_enable_3(tx_ad9371_tpl_core_dac_enable_3),
        .dac_valid_0(tx_ad9371_tpl_core_dac_valid_0),
        .dac_valid_1(tx_ad9371_tpl_core_dac_valid_1),
        .dac_valid_2(tx_ad9371_tpl_core_dac_valid_2),
        .dac_valid_3(tx_ad9371_tpl_core_dac_valid_3),
        .link_clk(ad9371_tx_device_clk),
        .link_tdata(tx_data_1_TDATA),
        .link_tready(tx_data_1_TREADY),
        .link_tvalid(tx_data_1_TVALID),
        .s_axi_aclk(s_axi_aclk_0_1),
        .s_axi_araddr(s_axi_0_3_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_0_1),
        .s_axi_arprot(s_axi_0_3_ARPROT),
        .s_axi_arready(s_axi_0_3_ARREADY),
        .s_axi_arvalid(s_axi_0_3_ARVALID),
        .s_axi_awaddr(s_axi_0_3_AWADDR),
        .s_axi_awprot(s_axi_0_3_AWPROT),
        .s_axi_awready(s_axi_0_3_AWREADY),
        .s_axi_awvalid(s_axi_0_3_AWVALID),
        .s_axi_bready(s_axi_0_3_BREADY),
        .s_axi_bresp(s_axi_0_3_BRESP),
        .s_axi_bvalid(s_axi_0_3_BVALID),
        .s_axi_rdata(s_axi_0_3_RDATA),
        .s_axi_rready(s_axi_0_3_RREADY),
        .s_axi_rresp(s_axi_0_3_RRESP),
        .s_axi_rvalid(s_axi_0_3_RVALID),
        .s_axi_wdata(s_axi_0_3_WDATA),
        .s_axi_wready(s_axi_0_3_WREADY),
        .s_axi_wstrb(s_axi_0_3_WSTRB),
        .s_axi_wvalid(s_axi_0_3_WVALID));
  system_util_ad9371_xcvr_0 util_ad9371_xcvr
       (.cpll_ref_clk_0(rx_ref_clk_0_1),
        .cpll_ref_clk_1(rx_ref_clk_0_1),
        .cpll_ref_clk_2(rx_ref_clk_0_1),
        .cpll_ref_clk_3(rx_ref_clk_0_1),
        .qpll_ref_clk_0(tx_ref_clk_0_1),
        .rx_0_n(rx_data_0_n_1),
        .rx_0_p(rx_data_0_p_1),
        .rx_1_n(rx_data_1_n_1),
        .rx_1_p(rx_data_1_p_1),
        .rx_2_n(rx_data_2_n_1),
        .rx_2_p(rx_data_2_p_1),
        .rx_3_n(rx_data_3_n_1),
        .rx_3_p(rx_data_3_p_1),
        .rx_calign_0(axi_ad9371_rx_jesd_phy_en_char_align),
        .rx_calign_1(axi_ad9371_rx_jesd_phy_en_char_align),
        .rx_calign_2(axi_ad9371_rx_jesd_phy_en_char_align),
        .rx_calign_3(axi_ad9371_rx_jesd_phy_en_char_align),
        .rx_charisk_0(util_ad9371_xcvr_rx_0_rxcharisk),
        .rx_charisk_1(util_ad9371_xcvr_rx_1_rxcharisk),
        .rx_charisk_2(util_ad9371_xcvr_rx_2_rxcharisk),
        .rx_charisk_3(util_ad9371_xcvr_rx_3_rxcharisk),
        .rx_clk_0(ad9371_rx_device_clk),
        .rx_clk_1(ad9371_rx_device_clk),
        .rx_clk_2(ad9371_rx_device_clk),
        .rx_clk_3(ad9371_rx_device_clk),
        .rx_data_0(util_ad9371_xcvr_rx_0_rxdata),
        .rx_data_1(util_ad9371_xcvr_rx_1_rxdata),
        .rx_data_2(util_ad9371_xcvr_rx_2_rxdata),
        .rx_data_3(util_ad9371_xcvr_rx_3_rxdata),
        .rx_disperr_0(util_ad9371_xcvr_rx_0_rxdisperr),
        .rx_disperr_1(util_ad9371_xcvr_rx_1_rxdisperr),
        .rx_disperr_2(util_ad9371_xcvr_rx_2_rxdisperr),
        .rx_disperr_3(util_ad9371_xcvr_rx_3_rxdisperr),
        .rx_notintable_0(util_ad9371_xcvr_rx_0_rxnotintable),
        .rx_notintable_1(util_ad9371_xcvr_rx_1_rxnotintable),
        .rx_notintable_2(util_ad9371_xcvr_rx_2_rxnotintable),
        .rx_notintable_3(util_ad9371_xcvr_rx_3_rxnotintable),
        .rx_out_clk_0(util_ad9371_xcvr_rx_out_clk_0),
        .tx_0_n(util_ad9371_xcvr_tx_0_n),
        .tx_0_p(util_ad9371_xcvr_tx_0_p),
        .tx_1_n(util_ad9371_xcvr_tx_1_n),
        .tx_1_p(util_ad9371_xcvr_tx_1_p),
        .tx_2_n(util_ad9371_xcvr_tx_2_n),
        .tx_2_p(util_ad9371_xcvr_tx_2_p),
        .tx_3_n(util_ad9371_xcvr_tx_3_n),
        .tx_3_p(util_ad9371_xcvr_tx_3_p),
        .tx_charisk_0(axi_ad9371_tx_jesd_tx_phy3_txcharisk),
        .tx_charisk_1(axi_ad9371_tx_jesd_tx_phy0_txcharisk),
        .tx_charisk_2(axi_ad9371_tx_jesd_tx_phy1_txcharisk),
        .tx_charisk_3(axi_ad9371_tx_jesd_tx_phy2_txcharisk),
        .tx_clk_0(ad9371_tx_device_clk),
        .tx_clk_1(ad9371_tx_device_clk),
        .tx_clk_2(ad9371_tx_device_clk),
        .tx_clk_3(ad9371_tx_device_clk),
        .tx_data_0(axi_ad9371_tx_jesd_tx_phy3_txdata),
        .tx_data_1(axi_ad9371_tx_jesd_tx_phy0_txdata),
        .tx_data_2(axi_ad9371_tx_jesd_tx_phy1_txdata),
        .tx_data_3(axi_ad9371_tx_jesd_tx_phy2_txdata),
        .tx_out_clk_0(util_ad9371_xcvr_tx_out_clk_0),
        .up_clk(s_axi_aclk_0_1),
        .up_cm_addr_0(axi_ad9371_tx_xcvr_up_cm_0_addr),
        .up_cm_enb_0(axi_ad9371_tx_xcvr_up_cm_0_enb),
        .up_cm_rdata_0(axi_ad9371_tx_xcvr_up_cm_0_rdata),
        .up_cm_ready_0(axi_ad9371_tx_xcvr_up_cm_0_ready),
        .up_cm_wdata_0(axi_ad9371_tx_xcvr_up_cm_0_wdata),
        .up_cm_wr_0(axi_ad9371_tx_xcvr_up_cm_0_wr),
        .up_cpll_rst_0(axi_ad9371_rx_xcvr_up_pll_rst),
        .up_cpll_rst_1(axi_ad9371_rx_xcvr_up_pll_rst),
        .up_cpll_rst_2(axi_ad9371_rx_xcvr_up_pll_rst),
        .up_cpll_rst_3(axi_ad9371_rx_xcvr_up_pll_rst),
        .up_es_addr_0(axi_ad9371_rx_xcvr_up_es_0_addr),
        .up_es_addr_1(axi_ad9371_rx_xcvr_up_es_1_addr),
        .up_es_addr_2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_es_addr_3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_es_enb_0(axi_ad9371_rx_xcvr_up_es_0_enb),
        .up_es_enb_1(axi_ad9371_rx_xcvr_up_es_1_enb),
        .up_es_enb_2(1'b0),
        .up_es_enb_3(1'b0),
        .up_es_rdata_0(axi_ad9371_rx_xcvr_up_es_0_rdata),
        .up_es_rdata_1(axi_ad9371_rx_xcvr_up_es_1_rdata),
        .up_es_ready_0(axi_ad9371_rx_xcvr_up_es_0_ready),
        .up_es_ready_1(axi_ad9371_rx_xcvr_up_es_1_ready),
        .up_es_reset_0(axi_ad9371_rx_xcvr_up_es_0_reset),
        .up_es_reset_1(axi_ad9371_rx_xcvr_up_es_1_reset),
        .up_es_reset_2(1'b0),
        .up_es_reset_3(1'b0),
        .up_es_wdata_0(axi_ad9371_rx_xcvr_up_es_0_wdata),
        .up_es_wdata_1(axi_ad9371_rx_xcvr_up_es_1_wdata),
        .up_es_wdata_2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_es_wdata_3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_es_wr_0(axi_ad9371_rx_xcvr_up_es_0_wr),
        .up_es_wr_1(axi_ad9371_rx_xcvr_up_es_1_wr),
        .up_es_wr_2(1'b0),
        .up_es_wr_3(1'b0),
        .up_qpll_rst_0(axi_ad9371_tx_xcvr_up_pll_rst),
        .up_rstn(1'b0),
        .up_rx_addr_0(axi_ad9371_rx_xcvr_up_ch_0_addr),
        .up_rx_addr_1(axi_ad9371_rx_xcvr_up_ch_1_addr),
        .up_rx_addr_2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_rx_addr_3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_rx_enb_0(axi_ad9371_rx_xcvr_up_ch_0_enb),
        .up_rx_enb_1(axi_ad9371_rx_xcvr_up_ch_1_enb),
        .up_rx_enb_2(1'b0),
        .up_rx_enb_3(1'b0),
        .up_rx_lpm_dfe_n_0(axi_ad9371_rx_xcvr_up_ch_0_lpm_dfe_n),
        .up_rx_lpm_dfe_n_1(axi_ad9371_rx_xcvr_up_ch_1_lpm_dfe_n),
        .up_rx_lpm_dfe_n_2(1'b0),
        .up_rx_lpm_dfe_n_3(1'b0),
        .up_rx_out_clk_sel_0(axi_ad9371_rx_xcvr_up_ch_0_out_clk_sel),
        .up_rx_out_clk_sel_1(axi_ad9371_rx_xcvr_up_ch_1_out_clk_sel),
        .up_rx_out_clk_sel_2({1'b0,1'b0,1'b0}),
        .up_rx_out_clk_sel_3({1'b0,1'b0,1'b0}),
        .up_rx_pll_locked_0(axi_ad9371_rx_xcvr_up_ch_0_pll_locked),
        .up_rx_pll_locked_1(axi_ad9371_rx_xcvr_up_ch_1_pll_locked),
        .up_rx_rate_0(axi_ad9371_rx_xcvr_up_ch_0_rate),
        .up_rx_rate_1(axi_ad9371_rx_xcvr_up_ch_1_rate),
        .up_rx_rate_2({1'b0,1'b0,1'b0}),
        .up_rx_rate_3({1'b0,1'b0,1'b0}),
        .up_rx_rdata_0(axi_ad9371_rx_xcvr_up_ch_0_rdata),
        .up_rx_rdata_1(axi_ad9371_rx_xcvr_up_ch_1_rdata),
        .up_rx_ready_0(axi_ad9371_rx_xcvr_up_ch_0_ready),
        .up_rx_ready_1(axi_ad9371_rx_xcvr_up_ch_1_ready),
        .up_rx_rst_0(axi_ad9371_rx_xcvr_up_ch_0_rst),
        .up_rx_rst_1(axi_ad9371_rx_xcvr_up_ch_1_rst),
        .up_rx_rst_2(1'b0),
        .up_rx_rst_3(1'b0),
        .up_rx_rst_done_0(axi_ad9371_rx_xcvr_up_ch_0_rst_done),
        .up_rx_rst_done_1(axi_ad9371_rx_xcvr_up_ch_1_rst_done),
        .up_rx_sys_clk_sel_0(axi_ad9371_rx_xcvr_up_ch_0_sys_clk_sel),
        .up_rx_sys_clk_sel_1(axi_ad9371_rx_xcvr_up_ch_1_sys_clk_sel),
        .up_rx_sys_clk_sel_2({1'b0,1'b0}),
        .up_rx_sys_clk_sel_3({1'b0,1'b0}),
        .up_rx_user_ready_0(axi_ad9371_rx_xcvr_up_ch_0_user_ready),
        .up_rx_user_ready_1(axi_ad9371_rx_xcvr_up_ch_1_user_ready),
        .up_rx_user_ready_2(1'b0),
        .up_rx_user_ready_3(1'b0),
        .up_rx_wdata_0(axi_ad9371_rx_xcvr_up_ch_0_wdata),
        .up_rx_wdata_1(axi_ad9371_rx_xcvr_up_ch_1_wdata),
        .up_rx_wdata_2({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_rx_wdata_3({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .up_rx_wr_0(axi_ad9371_rx_xcvr_up_ch_0_wr),
        .up_rx_wr_1(axi_ad9371_rx_xcvr_up_ch_1_wr),
        .up_rx_wr_2(1'b0),
        .up_rx_wr_3(1'b0),
        .up_tx_addr_0(axi_ad9371_tx_xcvr_up_ch_3_addr),
        .up_tx_addr_1(axi_ad9371_tx_xcvr_up_ch_0_addr),
        .up_tx_addr_2(axi_ad9371_tx_xcvr_up_ch_1_addr),
        .up_tx_addr_3(axi_ad9371_tx_xcvr_up_ch_2_addr),
        .up_tx_diffctrl_0(axi_ad9371_tx_xcvr_up_ch_3_tx_diffctrl),
        .up_tx_diffctrl_1(axi_ad9371_tx_xcvr_up_ch_0_tx_diffctrl),
        .up_tx_diffctrl_2(axi_ad9371_tx_xcvr_up_ch_1_tx_diffctrl),
        .up_tx_diffctrl_3(axi_ad9371_tx_xcvr_up_ch_2_tx_diffctrl),
        .up_tx_enb_0(axi_ad9371_tx_xcvr_up_ch_3_enb),
        .up_tx_enb_1(axi_ad9371_tx_xcvr_up_ch_0_enb),
        .up_tx_enb_2(axi_ad9371_tx_xcvr_up_ch_1_enb),
        .up_tx_enb_3(axi_ad9371_tx_xcvr_up_ch_2_enb),
        .up_tx_lpm_dfe_n_0(axi_ad9371_tx_xcvr_up_ch_3_lpm_dfe_n),
        .up_tx_lpm_dfe_n_1(axi_ad9371_tx_xcvr_up_ch_0_lpm_dfe_n),
        .up_tx_lpm_dfe_n_2(axi_ad9371_tx_xcvr_up_ch_1_lpm_dfe_n),
        .up_tx_lpm_dfe_n_3(axi_ad9371_tx_xcvr_up_ch_2_lpm_dfe_n),
        .up_tx_out_clk_sel_0(axi_ad9371_tx_xcvr_up_ch_3_out_clk_sel),
        .up_tx_out_clk_sel_1(axi_ad9371_tx_xcvr_up_ch_0_out_clk_sel),
        .up_tx_out_clk_sel_2(axi_ad9371_tx_xcvr_up_ch_1_out_clk_sel),
        .up_tx_out_clk_sel_3(axi_ad9371_tx_xcvr_up_ch_2_out_clk_sel),
        .up_tx_pll_locked_0(axi_ad9371_tx_xcvr_up_ch_3_pll_locked),
        .up_tx_pll_locked_1(axi_ad9371_tx_xcvr_up_ch_0_pll_locked),
        .up_tx_pll_locked_2(axi_ad9371_tx_xcvr_up_ch_1_pll_locked),
        .up_tx_pll_locked_3(axi_ad9371_tx_xcvr_up_ch_2_pll_locked),
        .up_tx_postcursor_0(axi_ad9371_tx_xcvr_up_ch_3_tx_postcursor),
        .up_tx_postcursor_1(axi_ad9371_tx_xcvr_up_ch_0_tx_postcursor),
        .up_tx_postcursor_2(axi_ad9371_tx_xcvr_up_ch_1_tx_postcursor),
        .up_tx_postcursor_3(axi_ad9371_tx_xcvr_up_ch_2_tx_postcursor),
        .up_tx_precursor_0(axi_ad9371_tx_xcvr_up_ch_3_tx_precursor),
        .up_tx_precursor_1(axi_ad9371_tx_xcvr_up_ch_0_tx_precursor),
        .up_tx_precursor_2(axi_ad9371_tx_xcvr_up_ch_1_tx_precursor),
        .up_tx_precursor_3(axi_ad9371_tx_xcvr_up_ch_2_tx_precursor),
        .up_tx_rate_0(axi_ad9371_tx_xcvr_up_ch_3_rate),
        .up_tx_rate_1(axi_ad9371_tx_xcvr_up_ch_0_rate),
        .up_tx_rate_2(axi_ad9371_tx_xcvr_up_ch_1_rate),
        .up_tx_rate_3(axi_ad9371_tx_xcvr_up_ch_2_rate),
        .up_tx_rdata_0(axi_ad9371_tx_xcvr_up_ch_3_rdata),
        .up_tx_rdata_1(axi_ad9371_tx_xcvr_up_ch_0_rdata),
        .up_tx_rdata_2(axi_ad9371_tx_xcvr_up_ch_1_rdata),
        .up_tx_rdata_3(axi_ad9371_tx_xcvr_up_ch_2_rdata),
        .up_tx_ready_0(axi_ad9371_tx_xcvr_up_ch_3_ready),
        .up_tx_ready_1(axi_ad9371_tx_xcvr_up_ch_0_ready),
        .up_tx_ready_2(axi_ad9371_tx_xcvr_up_ch_1_ready),
        .up_tx_ready_3(axi_ad9371_tx_xcvr_up_ch_2_ready),
        .up_tx_rst_0(axi_ad9371_tx_xcvr_up_ch_3_rst),
        .up_tx_rst_1(axi_ad9371_tx_xcvr_up_ch_0_rst),
        .up_tx_rst_2(axi_ad9371_tx_xcvr_up_ch_1_rst),
        .up_tx_rst_3(axi_ad9371_tx_xcvr_up_ch_2_rst),
        .up_tx_rst_done_0(axi_ad9371_tx_xcvr_up_ch_3_rst_done),
        .up_tx_rst_done_1(axi_ad9371_tx_xcvr_up_ch_0_rst_done),
        .up_tx_rst_done_2(axi_ad9371_tx_xcvr_up_ch_1_rst_done),
        .up_tx_rst_done_3(axi_ad9371_tx_xcvr_up_ch_2_rst_done),
        .up_tx_sys_clk_sel_0(axi_ad9371_tx_xcvr_up_ch_3_sys_clk_sel),
        .up_tx_sys_clk_sel_1(axi_ad9371_tx_xcvr_up_ch_0_sys_clk_sel),
        .up_tx_sys_clk_sel_2(axi_ad9371_tx_xcvr_up_ch_1_sys_clk_sel),
        .up_tx_sys_clk_sel_3(axi_ad9371_tx_xcvr_up_ch_2_sys_clk_sel),
        .up_tx_user_ready_0(axi_ad9371_tx_xcvr_up_ch_3_user_ready),
        .up_tx_user_ready_1(axi_ad9371_tx_xcvr_up_ch_0_user_ready),
        .up_tx_user_ready_2(axi_ad9371_tx_xcvr_up_ch_1_user_ready),
        .up_tx_user_ready_3(axi_ad9371_tx_xcvr_up_ch_2_user_ready),
        .up_tx_wdata_0(axi_ad9371_tx_xcvr_up_ch_3_wdata),
        .up_tx_wdata_1(axi_ad9371_tx_xcvr_up_ch_0_wdata),
        .up_tx_wdata_2(axi_ad9371_tx_xcvr_up_ch_1_wdata),
        .up_tx_wdata_3(axi_ad9371_tx_xcvr_up_ch_2_wdata),
        .up_tx_wr_0(axi_ad9371_tx_xcvr_up_ch_3_wr),
        .up_tx_wr_1(axi_ad9371_tx_xcvr_up_ch_0_wr),
        .up_tx_wr_2(axi_ad9371_tx_xcvr_up_ch_1_wr),
        .up_tx_wr_3(axi_ad9371_tx_xcvr_up_ch_2_wr));
endmodule

module tx_ad9371_tpl_core_imp_1J4NRGE
   (dac_data_0,
    dac_data_1,
    dac_data_2,
    dac_data_3,
    dac_dunf,
    dac_enable_0,
    dac_enable_1,
    dac_enable_2,
    dac_enable_3,
    dac_valid_0,
    dac_valid_1,
    dac_valid_2,
    dac_valid_3,
    link_clk,
    link_tdata,
    link_tready,
    link_tvalid,
    s_axi_aclk,
    s_axi_araddr,
    s_axi_aresetn,
    s_axi_arprot,
    s_axi_arready,
    s_axi_arvalid,
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awready,
    s_axi_awvalid,
    s_axi_bready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_rdata,
    s_axi_rready,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_wdata,
    s_axi_wready,
    s_axi_wstrb,
    s_axi_wvalid);
  input [31:0]dac_data_0;
  input [31:0]dac_data_1;
  input [31:0]dac_data_2;
  input [31:0]dac_data_3;
  input dac_dunf;
  output [0:0]dac_enable_0;
  output [0:0]dac_enable_1;
  output [0:0]dac_enable_2;
  output [0:0]dac_enable_3;
  output [0:0]dac_valid_0;
  output [0:0]dac_valid_1;
  output [0:0]dac_valid_2;
  output [0:0]dac_valid_3;
  input link_clk;
  output [127:0]link_tdata;
  input link_tready;
  output link_tvalid;
  input s_axi_aclk;
  input [12:0]s_axi_araddr;
  input s_axi_aresetn;
  input [2:0]s_axi_arprot;
  output s_axi_arready;
  input s_axi_arvalid;
  input [12:0]s_axi_awaddr;
  input [2:0]s_axi_awprot;
  output s_axi_awready;
  input s_axi_awvalid;
  input s_axi_bready;
  output [1:0]s_axi_bresp;
  output s_axi_bvalid;
  output [31:0]s_axi_rdata;
  input s_axi_rready;
  output [1:0]s_axi_rresp;
  output s_axi_rvalid;
  input [31:0]s_axi_wdata;
  output s_axi_wready;
  input [3:0]s_axi_wstrb;
  input s_axi_wvalid;

  wire [31:0]dac_data_0_1;
  wire [31:0]dac_data_1_1;
  wire [31:0]dac_data_2_1;
  wire [31:0]dac_data_3_1;
  wire dac_dunf_1;
  wire [3:0]dac_tpl_core_dac_valid;
  wire [3:0]dac_tpl_core_enable;
  wire [127:0]dac_tpl_core_link_TDATA;
  wire dac_tpl_core_link_TREADY;
  wire dac_tpl_core_link_TVALID;
  wire [127:0]data_concat0_dout;
  wire [0:0]enable_slice_0_Dout;
  wire [0:0]enable_slice_1_Dout;
  wire [0:0]enable_slice_2_Dout;
  wire [0:0]enable_slice_3_Dout;
  wire link_clk_1;
  wire [12:0]s_axi_1_ARADDR;
  wire [2:0]s_axi_1_ARPROT;
  wire s_axi_1_ARREADY;
  wire s_axi_1_ARVALID;
  wire [12:0]s_axi_1_AWADDR;
  wire [2:0]s_axi_1_AWPROT;
  wire s_axi_1_AWREADY;
  wire s_axi_1_AWVALID;
  wire s_axi_1_BREADY;
  wire [1:0]s_axi_1_BRESP;
  wire s_axi_1_BVALID;
  wire [31:0]s_axi_1_RDATA;
  wire s_axi_1_RREADY;
  wire [1:0]s_axi_1_RRESP;
  wire s_axi_1_RVALID;
  wire [31:0]s_axi_1_WDATA;
  wire s_axi_1_WREADY;
  wire [3:0]s_axi_1_WSTRB;
  wire s_axi_1_WVALID;
  wire s_axi_aclk_1;
  wire s_axi_aresetn_1;
  wire [0:0]valid_slice_0_Dout;
  wire [0:0]valid_slice_1_Dout;
  wire [0:0]valid_slice_2_Dout;
  wire [0:0]valid_slice_3_Dout;

  assign dac_data_0_1 = dac_data_0[31:0];
  assign dac_data_1_1 = dac_data_1[31:0];
  assign dac_data_2_1 = dac_data_2[31:0];
  assign dac_data_3_1 = dac_data_3[31:0];
  assign dac_dunf_1 = dac_dunf;
  assign dac_enable_0[0] = enable_slice_0_Dout;
  assign dac_enable_1[0] = enable_slice_1_Dout;
  assign dac_enable_2[0] = enable_slice_2_Dout;
  assign dac_enable_3[0] = enable_slice_3_Dout;
  assign dac_tpl_core_link_TREADY = link_tready;
  assign dac_valid_0[0] = valid_slice_0_Dout;
  assign dac_valid_1[0] = valid_slice_1_Dout;
  assign dac_valid_2[0] = valid_slice_2_Dout;
  assign dac_valid_3[0] = valid_slice_3_Dout;
  assign link_clk_1 = link_clk;
  assign link_tdata[127:0] = dac_tpl_core_link_TDATA;
  assign link_tvalid = dac_tpl_core_link_TVALID;
  assign s_axi_1_ARADDR = s_axi_araddr[12:0];
  assign s_axi_1_ARPROT = s_axi_arprot[2:0];
  assign s_axi_1_ARVALID = s_axi_arvalid;
  assign s_axi_1_AWADDR = s_axi_awaddr[12:0];
  assign s_axi_1_AWPROT = s_axi_awprot[2:0];
  assign s_axi_1_AWVALID = s_axi_awvalid;
  assign s_axi_1_BREADY = s_axi_bready;
  assign s_axi_1_RREADY = s_axi_rready;
  assign s_axi_1_WDATA = s_axi_wdata[31:0];
  assign s_axi_1_WSTRB = s_axi_wstrb[3:0];
  assign s_axi_1_WVALID = s_axi_wvalid;
  assign s_axi_aclk_1 = s_axi_aclk;
  assign s_axi_aresetn_1 = s_axi_aresetn;
  assign s_axi_arready = s_axi_1_ARREADY;
  assign s_axi_awready = s_axi_1_AWREADY;
  assign s_axi_bresp[1:0] = s_axi_1_BRESP;
  assign s_axi_bvalid = s_axi_1_BVALID;
  assign s_axi_rdata[31:0] = s_axi_1_RDATA;
  assign s_axi_rresp[1:0] = s_axi_1_RRESP;
  assign s_axi_rvalid = s_axi_1_RVALID;
  assign s_axi_wready = s_axi_1_WREADY;
  system_dac_tpl_core_0 dac_tpl_core
       (.dac_ddata(data_concat0_dout),
        .dac_dunf(dac_dunf_1),
        .dac_sync_in(1'b0),
        .dac_valid(dac_tpl_core_dac_valid),
        .enable(dac_tpl_core_enable),
        .link_clk(link_clk_1),
        .link_data(dac_tpl_core_link_TDATA),
        .link_ready(dac_tpl_core_link_TREADY),
        .link_valid(dac_tpl_core_link_TVALID),
        .s_axi_aclk(s_axi_aclk_1),
        .s_axi_araddr(s_axi_1_ARADDR),
        .s_axi_aresetn(s_axi_aresetn_1),
        .s_axi_arprot(s_axi_1_ARPROT),
        .s_axi_arready(s_axi_1_ARREADY),
        .s_axi_arvalid(s_axi_1_ARVALID),
        .s_axi_awaddr(s_axi_1_AWADDR),
        .s_axi_awprot(s_axi_1_AWPROT),
        .s_axi_awready(s_axi_1_AWREADY),
        .s_axi_awvalid(s_axi_1_AWVALID),
        .s_axi_bready(s_axi_1_BREADY),
        .s_axi_bresp(s_axi_1_BRESP),
        .s_axi_bvalid(s_axi_1_BVALID),
        .s_axi_rdata(s_axi_1_RDATA),
        .s_axi_rready(s_axi_1_RREADY),
        .s_axi_rresp(s_axi_1_RRESP),
        .s_axi_rvalid(s_axi_1_RVALID),
        .s_axi_wdata(s_axi_1_WDATA),
        .s_axi_wready(s_axi_1_WREADY),
        .s_axi_wstrb(s_axi_1_WSTRB),
        .s_axi_wvalid(s_axi_1_WVALID));
  system_data_concat0_0 data_concat0
       (.In0(dac_data_0_1),
        .In1(dac_data_1_1),
        .In2(dac_data_2_1),
        .In3(dac_data_3_1),
        .dout(data_concat0_dout));
  system_enable_slice_0_0 enable_slice_0
       (.Din(dac_tpl_core_enable),
        .Dout(enable_slice_0_Dout));
  system_enable_slice_1_0 enable_slice_1
       (.Din(dac_tpl_core_enable),
        .Dout(enable_slice_1_Dout));
  system_enable_slice_2_0 enable_slice_2
       (.Din(dac_tpl_core_enable),
        .Dout(enable_slice_2_Dout));
  system_enable_slice_3_0 enable_slice_3
       (.Din(dac_tpl_core_enable),
        .Dout(enable_slice_3_Dout));
  system_valid_slice_0_0 valid_slice_0
       (.Din(dac_tpl_core_dac_valid),
        .Dout(valid_slice_0_Dout));
  system_valid_slice_1_0 valid_slice_1
       (.Din(dac_tpl_core_dac_valid),
        .Dout(valid_slice_1_Dout));
  system_valid_slice_2_0 valid_slice_2
       (.Din(dac_tpl_core_dac_valid),
        .Dout(valid_slice_2_Dout));
  system_valid_slice_3_0 valid_slice_3
       (.Din(dac_tpl_core_dac_valid),
        .Dout(valid_slice_3_Dout));
endmodule
