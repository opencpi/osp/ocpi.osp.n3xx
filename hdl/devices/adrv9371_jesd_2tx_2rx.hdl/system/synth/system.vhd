--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Tue Oct 26 15:51:26 2021
--Host        : localhost.localdomain running 64-bit CentOS Linux release 7.9.2009 (Core)
--Command     : generate_target system.bd
--Design      : system
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity axi_ad9371_rx_jesd_imp_GUTUBY is
  port (
    device_clk : in STD_LOGIC;
    irq : out STD_LOGIC;
    phy_en_char_align : out STD_LOGIC;
    rx_data_tdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
    rx_data_tvalid : out STD_LOGIC;
    rx_eof : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy0_rxcharisk : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy0_rxdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_phy0_rxdisperr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy0_rxnotintable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy1_rxcharisk : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy1_rxdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_phy1_rxdisperr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy1_rxnotintable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy2_rxcharisk : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy2_rxdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_phy2_rxdisperr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy2_rxnotintable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy3_rxcharisk : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy3_rxdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_phy3_rxdisperr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_phy3_rxnotintable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_sof : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_aclk : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_aresetn : in STD_LOGIC;
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    sysref : in STD_LOGIC
  );
end axi_ad9371_rx_jesd_imp_GUTUBY;

architecture STRUCTURE of axi_ad9371_rx_jesd_imp_GUTUBY is
  component system_rx_axi_0 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    irq : out STD_LOGIC;
    core_clk : in STD_LOGIC;
    core_reset_ext : in STD_LOGIC;
    core_reset : out STD_LOGIC;
    core_cfg_lanes_disable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    core_cfg_links_disable : out STD_LOGIC_VECTOR ( 0 to 0 );
    core_cfg_beats_per_multiframe : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_octets_per_frame : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_disable_scrambler : out STD_LOGIC;
    core_cfg_disable_char_replacement : out STD_LOGIC;
    core_cfg_lmfc_offset : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_sysref_oneshot : out STD_LOGIC;
    core_cfg_sysref_disable : out STD_LOGIC;
    core_cfg_buffer_early_release : out STD_LOGIC;
    core_cfg_buffer_delay : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_frame_align_err_threshold : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_ilas_config_valid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    core_ilas_config_addr : in STD_LOGIC_VECTOR ( 7 downto 0 );
    core_ilas_config_data : in STD_LOGIC_VECTOR ( 127 downto 0 );
    core_event_sysref_alignment_error : in STD_LOGIC;
    core_event_sysref_edge : in STD_LOGIC;
    core_event_frame_alignment_error : in STD_LOGIC;
    core_event_unexpected_lane_state_error : in STD_LOGIC;
    core_ctrl_err_statistics_mask : out STD_LOGIC_VECTOR ( 6 downto 0 );
    core_ctrl_err_statistics_reset : out STD_LOGIC;
    core_status_err_statistics_cnt : in STD_LOGIC_VECTOR ( 127 downto 0 );
    core_status_ctrl_state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    core_status_lane_cgs_state : in STD_LOGIC_VECTOR ( 7 downto 0 );
    core_status_lane_emb_state : in STD_LOGIC_VECTOR ( 11 downto 0 );
    core_status_lane_ifs_ready : in STD_LOGIC_VECTOR ( 3 downto 0 );
    core_status_lane_latency : in STD_LOGIC_VECTOR ( 55 downto 0 );
    core_status_lane_frame_align_err_cnt : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component system_rx_axi_0;
  component system_rx_0 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    phy_data : in STD_LOGIC_VECTOR ( 127 downto 0 );
    phy_header : in STD_LOGIC_VECTOR ( 7 downto 0 );
    phy_charisk : in STD_LOGIC_VECTOR ( 15 downto 0 );
    phy_notintable : in STD_LOGIC_VECTOR ( 15 downto 0 );
    phy_disperr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    phy_block_sync : in STD_LOGIC_VECTOR ( 3 downto 0 );
    sysref : in STD_LOGIC;
    lmfc_edge : out STD_LOGIC;
    lmfc_clk : out STD_LOGIC;
    event_sysref_alignment_error : out STD_LOGIC;
    event_sysref_edge : out STD_LOGIC;
    event_frame_alignment_error : out STD_LOGIC;
    event_unexpected_lane_state_error : out STD_LOGIC;
    sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    phy_en_char_align : out STD_LOGIC;
    rx_data : out STD_LOGIC_VECTOR ( 127 downto 0 );
    rx_valid : out STD_LOGIC;
    rx_eof : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_sof : out STD_LOGIC_VECTOR ( 3 downto 0 );
    cfg_lanes_disable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    cfg_links_disable : in STD_LOGIC_VECTOR ( 0 to 0 );
    cfg_beats_per_multiframe : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_octets_per_frame : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_lmfc_offset : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_sysref_disable : in STD_LOGIC;
    cfg_sysref_oneshot : in STD_LOGIC;
    cfg_buffer_early_release : in STD_LOGIC;
    cfg_buffer_delay : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_disable_char_replacement : in STD_LOGIC;
    cfg_disable_scrambler : in STD_LOGIC;
    ctrl_err_statistics_reset : in STD_LOGIC;
    ctrl_err_statistics_mask : in STD_LOGIC_VECTOR ( 6 downto 0 );
    cfg_frame_align_err_threshold : in STD_LOGIC_VECTOR ( 7 downto 0 );
    status_err_statistics_cnt : out STD_LOGIC_VECTOR ( 127 downto 0 );
    ilas_config_valid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ilas_config_addr : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ilas_config_data : out STD_LOGIC_VECTOR ( 127 downto 0 );
    status_ctrl_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    status_lane_cgs_state : out STD_LOGIC_VECTOR ( 7 downto 0 );
    status_lane_ifs_ready : out STD_LOGIC_VECTOR ( 3 downto 0 );
    status_lane_latency : out STD_LOGIC_VECTOR ( 55 downto 0 );
    status_lane_emb_state : out STD_LOGIC_VECTOR ( 11 downto 0 );
    status_lane_frame_align_err_cnt : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component system_rx_0;
  signal device_clk_1 : STD_LOGIC;
  signal rx_axi_core_reset : STD_LOGIC;
  signal rx_axi_irq : STD_LOGIC;
  signal rx_axi_rx_cfg_beats_per_multiframe : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_axi_rx_cfg_buffer_delay : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_axi_rx_cfg_buffer_early_release : STD_LOGIC;
  signal rx_axi_rx_cfg_disable_char_replacement : STD_LOGIC;
  signal rx_axi_rx_cfg_disable_scrambler : STD_LOGIC;
  signal rx_axi_rx_cfg_err_statistics_mask : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal rx_axi_rx_cfg_err_statistics_reset : STD_LOGIC;
  signal rx_axi_rx_cfg_frame_align_err_threshold : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_axi_rx_cfg_lanes_disable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_axi_rx_cfg_links_disable : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_axi_rx_cfg_lmfc_offset : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_axi_rx_cfg_octets_per_frame : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_axi_rx_cfg_sysref_disable : STD_LOGIC;
  signal rx_axi_rx_cfg_sysref_oneshot : STD_LOGIC;
  signal rx_phy0_1_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy0_1_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_phy0_1_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy0_1_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy1_1_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy1_1_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_phy1_1_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy1_1_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy2_1_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy2_1_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_phy2_1_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy2_1_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy3_1_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy3_1_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_phy3_1_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy3_1_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_phy_en_char_align : STD_LOGIC;
  signal rx_rx_data : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal rx_rx_eof : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_rx_event_frame_alignment_error : STD_LOGIC;
  signal rx_rx_event_sysref_alignment_error : STD_LOGIC;
  signal rx_rx_event_sysref_edge : STD_LOGIC;
  signal rx_rx_event_unexpected_lane_state_error : STD_LOGIC;
  signal rx_rx_ilas_config_addr : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_rx_ilas_config_data : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal rx_rx_ilas_config_valid : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_rx_sof : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_rx_status_ctrl_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal rx_rx_status_err_statistics_cnt : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal rx_rx_status_lane_cgs_state : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_rx_status_lane_emb_state : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal rx_rx_status_lane_frame_align_err_cnt : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_rx_status_lane_ifs_ready : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal rx_rx_status_lane_latency : STD_LOGIC_VECTOR ( 55 downto 0 );
  signal rx_rx_valid : STD_LOGIC;
  signal rx_sync : STD_LOGIC_VECTOR ( 0 to 0 );
  signal s_axi_1_ARADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_ARREADY : STD_LOGIC;
  signal s_axi_1_ARVALID : STD_LOGIC;
  signal s_axi_1_AWADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_AWREADY : STD_LOGIC;
  signal s_axi_1_AWVALID : STD_LOGIC;
  signal s_axi_1_BREADY : STD_LOGIC;
  signal s_axi_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_BVALID : STD_LOGIC;
  signal s_axi_1_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_RREADY : STD_LOGIC;
  signal s_axi_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_RVALID : STD_LOGIC;
  signal s_axi_1_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_WREADY : STD_LOGIC;
  signal s_axi_1_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_1_WVALID : STD_LOGIC;
  signal s_axi_aclk_1 : STD_LOGIC;
  signal s_axi_aresetn_1 : STD_LOGIC;
  signal sysref_1 : STD_LOGIC;
  signal NLW_rx_lmfc_clk_UNCONNECTED : STD_LOGIC;
  signal NLW_rx_lmfc_edge_UNCONNECTED : STD_LOGIC;
begin
  device_clk_1 <= device_clk;
  irq <= rx_axi_irq;
  phy_en_char_align <= rx_phy_en_char_align;
  rx_data_tdata(127 downto 0) <= rx_rx_data(127 downto 0);
  rx_data_tvalid <= rx_rx_valid;
  rx_eof(3 downto 0) <= rx_rx_eof(3 downto 0);
  rx_phy0_1_rxcharisk(3 downto 0) <= rx_phy0_rxcharisk(3 downto 0);
  rx_phy0_1_rxdata(31 downto 0) <= rx_phy0_rxdata(31 downto 0);
  rx_phy0_1_rxdisperr(3 downto 0) <= rx_phy0_rxdisperr(3 downto 0);
  rx_phy0_1_rxnotintable(3 downto 0) <= rx_phy0_rxnotintable(3 downto 0);
  rx_phy1_1_rxcharisk(3 downto 0) <= rx_phy1_rxcharisk(3 downto 0);
  rx_phy1_1_rxdata(31 downto 0) <= rx_phy1_rxdata(31 downto 0);
  rx_phy1_1_rxdisperr(3 downto 0) <= rx_phy1_rxdisperr(3 downto 0);
  rx_phy1_1_rxnotintable(3 downto 0) <= rx_phy1_rxnotintable(3 downto 0);
  rx_phy2_1_rxcharisk(3 downto 0) <= rx_phy2_rxcharisk(3 downto 0);
  rx_phy2_1_rxdata(31 downto 0) <= rx_phy2_rxdata(31 downto 0);
  rx_phy2_1_rxdisperr(3 downto 0) <= rx_phy2_rxdisperr(3 downto 0);
  rx_phy2_1_rxnotintable(3 downto 0) <= rx_phy2_rxnotintable(3 downto 0);
  rx_phy3_1_rxcharisk(3 downto 0) <= rx_phy3_rxcharisk(3 downto 0);
  rx_phy3_1_rxdata(31 downto 0) <= rx_phy3_rxdata(31 downto 0);
  rx_phy3_1_rxdisperr(3 downto 0) <= rx_phy3_rxdisperr(3 downto 0);
  rx_phy3_1_rxnotintable(3 downto 0) <= rx_phy3_rxnotintable(3 downto 0);
  rx_sof(3 downto 0) <= rx_rx_sof(3 downto 0);
  s_axi_1_ARADDR(13 downto 0) <= s_axi_araddr(13 downto 0);
  s_axi_1_ARPROT(2 downto 0) <= s_axi_arprot(2 downto 0);
  s_axi_1_ARVALID <= s_axi_arvalid;
  s_axi_1_AWADDR(13 downto 0) <= s_axi_awaddr(13 downto 0);
  s_axi_1_AWPROT(2 downto 0) <= s_axi_awprot(2 downto 0);
  s_axi_1_AWVALID <= s_axi_awvalid;
  s_axi_1_BREADY <= s_axi_bready;
  s_axi_1_RREADY <= s_axi_rready;
  s_axi_1_WDATA(31 downto 0) <= s_axi_wdata(31 downto 0);
  s_axi_1_WSTRB(3 downto 0) <= s_axi_wstrb(3 downto 0);
  s_axi_1_WVALID <= s_axi_wvalid;
  s_axi_aclk_1 <= s_axi_aclk;
  s_axi_aresetn_1 <= s_axi_aresetn;
  s_axi_arready <= s_axi_1_ARREADY;
  s_axi_awready <= s_axi_1_AWREADY;
  s_axi_bresp(1 downto 0) <= s_axi_1_BRESP(1 downto 0);
  s_axi_bvalid <= s_axi_1_BVALID;
  s_axi_rdata(31 downto 0) <= s_axi_1_RDATA(31 downto 0);
  s_axi_rresp(1 downto 0) <= s_axi_1_RRESP(1 downto 0);
  s_axi_rvalid <= s_axi_1_RVALID;
  s_axi_wready <= s_axi_1_WREADY;
  sync(0) <= rx_sync(0);
  sysref_1 <= sysref;
rx: component system_rx_0
     port map (
      cfg_beats_per_multiframe(7 downto 0) => rx_axi_rx_cfg_beats_per_multiframe(7 downto 0),
      cfg_buffer_delay(7 downto 0) => rx_axi_rx_cfg_buffer_delay(7 downto 0),
      cfg_buffer_early_release => rx_axi_rx_cfg_buffer_early_release,
      cfg_disable_char_replacement => rx_axi_rx_cfg_disable_char_replacement,
      cfg_disable_scrambler => rx_axi_rx_cfg_disable_scrambler,
      cfg_frame_align_err_threshold(7 downto 0) => rx_axi_rx_cfg_frame_align_err_threshold(7 downto 0),
      cfg_lanes_disable(3 downto 0) => rx_axi_rx_cfg_lanes_disable(3 downto 0),
      cfg_links_disable(0) => rx_axi_rx_cfg_links_disable(0),
      cfg_lmfc_offset(7 downto 0) => rx_axi_rx_cfg_lmfc_offset(7 downto 0),
      cfg_octets_per_frame(7 downto 0) => rx_axi_rx_cfg_octets_per_frame(7 downto 0),
      cfg_sysref_disable => rx_axi_rx_cfg_sysref_disable,
      cfg_sysref_oneshot => rx_axi_rx_cfg_sysref_oneshot,
      clk => device_clk_1,
      ctrl_err_statistics_mask(6 downto 0) => rx_axi_rx_cfg_err_statistics_mask(6 downto 0),
      ctrl_err_statistics_reset => rx_axi_rx_cfg_err_statistics_reset,
      event_frame_alignment_error => rx_rx_event_frame_alignment_error,
      event_sysref_alignment_error => rx_rx_event_sysref_alignment_error,
      event_sysref_edge => rx_rx_event_sysref_edge,
      event_unexpected_lane_state_error => rx_rx_event_unexpected_lane_state_error,
      ilas_config_addr(7 downto 0) => rx_rx_ilas_config_addr(7 downto 0),
      ilas_config_data(127 downto 0) => rx_rx_ilas_config_data(127 downto 0),
      ilas_config_valid(3 downto 0) => rx_rx_ilas_config_valid(3 downto 0),
      lmfc_clk => NLW_rx_lmfc_clk_UNCONNECTED,
      lmfc_edge => NLW_rx_lmfc_edge_UNCONNECTED,
      phy_block_sync(3 downto 0) => B"0000",
      phy_charisk(15 downto 12) => rx_phy3_1_rxcharisk(3 downto 0),
      phy_charisk(11 downto 8) => rx_phy2_1_rxcharisk(3 downto 0),
      phy_charisk(7 downto 4) => rx_phy1_1_rxcharisk(3 downto 0),
      phy_charisk(3 downto 0) => rx_phy0_1_rxcharisk(3 downto 0),
      phy_data(127 downto 96) => rx_phy3_1_rxdata(31 downto 0),
      phy_data(95 downto 64) => rx_phy2_1_rxdata(31 downto 0),
      phy_data(63 downto 32) => rx_phy1_1_rxdata(31 downto 0),
      phy_data(31 downto 0) => rx_phy0_1_rxdata(31 downto 0),
      phy_disperr(15 downto 12) => rx_phy3_1_rxdisperr(3 downto 0),
      phy_disperr(11 downto 8) => rx_phy2_1_rxdisperr(3 downto 0),
      phy_disperr(7 downto 4) => rx_phy1_1_rxdisperr(3 downto 0),
      phy_disperr(3 downto 0) => rx_phy0_1_rxdisperr(3 downto 0),
      phy_en_char_align => rx_phy_en_char_align,
      phy_header(7 downto 0) => B"00000000",
      phy_notintable(15 downto 12) => rx_phy3_1_rxnotintable(3 downto 0),
      phy_notintable(11 downto 8) => rx_phy2_1_rxnotintable(3 downto 0),
      phy_notintable(7 downto 4) => rx_phy1_1_rxnotintable(3 downto 0),
      phy_notintable(3 downto 0) => rx_phy0_1_rxnotintable(3 downto 0),
      reset => rx_axi_core_reset,
      rx_data(127 downto 0) => rx_rx_data(127 downto 0),
      rx_eof(3 downto 0) => rx_rx_eof(3 downto 0),
      rx_sof(3 downto 0) => rx_rx_sof(3 downto 0),
      rx_valid => rx_rx_valid,
      status_ctrl_state(1 downto 0) => rx_rx_status_ctrl_state(1 downto 0),
      status_err_statistics_cnt(127 downto 0) => rx_rx_status_err_statistics_cnt(127 downto 0),
      status_lane_cgs_state(7 downto 0) => rx_rx_status_lane_cgs_state(7 downto 0),
      status_lane_emb_state(11 downto 0) => rx_rx_status_lane_emb_state(11 downto 0),
      status_lane_frame_align_err_cnt(31 downto 0) => rx_rx_status_lane_frame_align_err_cnt(31 downto 0),
      status_lane_ifs_ready(3 downto 0) => rx_rx_status_lane_ifs_ready(3 downto 0),
      status_lane_latency(55 downto 0) => rx_rx_status_lane_latency(55 downto 0),
      sync(0) => rx_sync(0),
      sysref => sysref_1
    );
rx_axi: component system_rx_axi_0
     port map (
      core_cfg_beats_per_multiframe(7 downto 0) => rx_axi_rx_cfg_beats_per_multiframe(7 downto 0),
      core_cfg_buffer_delay(7 downto 0) => rx_axi_rx_cfg_buffer_delay(7 downto 0),
      core_cfg_buffer_early_release => rx_axi_rx_cfg_buffer_early_release,
      core_cfg_disable_char_replacement => rx_axi_rx_cfg_disable_char_replacement,
      core_cfg_disable_scrambler => rx_axi_rx_cfg_disable_scrambler,
      core_cfg_frame_align_err_threshold(7 downto 0) => rx_axi_rx_cfg_frame_align_err_threshold(7 downto 0),
      core_cfg_lanes_disable(3 downto 0) => rx_axi_rx_cfg_lanes_disable(3 downto 0),
      core_cfg_links_disable(0) => rx_axi_rx_cfg_links_disable(0),
      core_cfg_lmfc_offset(7 downto 0) => rx_axi_rx_cfg_lmfc_offset(7 downto 0),
      core_cfg_octets_per_frame(7 downto 0) => rx_axi_rx_cfg_octets_per_frame(7 downto 0),
      core_cfg_sysref_disable => rx_axi_rx_cfg_sysref_disable,
      core_cfg_sysref_oneshot => rx_axi_rx_cfg_sysref_oneshot,
      core_clk => device_clk_1,
      core_ctrl_err_statistics_mask(6 downto 0) => rx_axi_rx_cfg_err_statistics_mask(6 downto 0),
      core_ctrl_err_statistics_reset => rx_axi_rx_cfg_err_statistics_reset,
      core_event_frame_alignment_error => rx_rx_event_frame_alignment_error,
      core_event_sysref_alignment_error => rx_rx_event_sysref_alignment_error,
      core_event_sysref_edge => rx_rx_event_sysref_edge,
      core_event_unexpected_lane_state_error => rx_rx_event_unexpected_lane_state_error,
      core_ilas_config_addr(7 downto 0) => rx_rx_ilas_config_addr(7 downto 0),
      core_ilas_config_data(127 downto 0) => rx_rx_ilas_config_data(127 downto 0),
      core_ilas_config_valid(3 downto 0) => rx_rx_ilas_config_valid(3 downto 0),
      core_reset => rx_axi_core_reset,
      core_reset_ext => '0',
      core_status_ctrl_state(1 downto 0) => rx_rx_status_ctrl_state(1 downto 0),
      core_status_err_statistics_cnt(127 downto 0) => rx_rx_status_err_statistics_cnt(127 downto 0),
      core_status_lane_cgs_state(7 downto 0) => rx_rx_status_lane_cgs_state(7 downto 0),
      core_status_lane_emb_state(11 downto 0) => rx_rx_status_lane_emb_state(11 downto 0),
      core_status_lane_frame_align_err_cnt(31 downto 0) => rx_rx_status_lane_frame_align_err_cnt(31 downto 0),
      core_status_lane_ifs_ready(3 downto 0) => rx_rx_status_lane_ifs_ready(3 downto 0),
      core_status_lane_latency(55 downto 0) => rx_rx_status_lane_latency(55 downto 0),
      irq => rx_axi_irq,
      s_axi_aclk => s_axi_aclk_1,
      s_axi_araddr(13 downto 0) => s_axi_1_ARADDR(13 downto 0),
      s_axi_aresetn => s_axi_aresetn_1,
      s_axi_arprot(2 downto 0) => s_axi_1_ARPROT(2 downto 0),
      s_axi_arready => s_axi_1_ARREADY,
      s_axi_arvalid => s_axi_1_ARVALID,
      s_axi_awaddr(13 downto 0) => s_axi_1_AWADDR(13 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_1_AWPROT(2 downto 0),
      s_axi_awready => s_axi_1_AWREADY,
      s_axi_awvalid => s_axi_1_AWVALID,
      s_axi_bready => s_axi_1_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_1_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_1_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_1_RDATA(31 downto 0),
      s_axi_rready => s_axi_1_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_1_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_1_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_1_WDATA(31 downto 0),
      s_axi_wready => s_axi_1_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_1_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_1_WVALID
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity axi_ad9371_tx_jesd_imp_17BPCLV is
  port (
    device_clk : in STD_LOGIC;
    irq : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_aresetn : in STD_LOGIC;
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    sysref : in STD_LOGIC;
    tx_data_tdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    tx_data_tready : out STD_LOGIC;
    tx_data_tvalid : in STD_LOGIC;
    tx_phy0_txcharisk : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_phy0_txdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    tx_phy1_txcharisk : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_phy1_txdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    tx_phy2_txcharisk : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_phy2_txdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    tx_phy3_txcharisk : out STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_phy3_txdata : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end axi_ad9371_tx_jesd_imp_17BPCLV;

architecture STRUCTURE of axi_ad9371_tx_jesd_imp_17BPCLV is
  component system_tx_axi_0 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    irq : out STD_LOGIC;
    core_clk : in STD_LOGIC;
    core_reset_ext : in STD_LOGIC;
    core_reset : out STD_LOGIC;
    core_cfg_lanes_disable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    core_cfg_links_disable : out STD_LOGIC_VECTOR ( 0 to 0 );
    core_cfg_beats_per_multiframe : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_octets_per_frame : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_lmfc_offset : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_sysref_oneshot : out STD_LOGIC;
    core_cfg_sysref_disable : out STD_LOGIC;
    core_cfg_continuous_cgs : out STD_LOGIC;
    core_cfg_continuous_ilas : out STD_LOGIC;
    core_cfg_skip_ilas : out STD_LOGIC;
    core_cfg_mframes_per_ilas : out STD_LOGIC_VECTOR ( 7 downto 0 );
    core_cfg_disable_char_replacement : out STD_LOGIC;
    core_cfg_disable_scrambler : out STD_LOGIC;
    core_ilas_config_rd : in STD_LOGIC;
    core_ilas_config_addr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    core_ilas_config_data : out STD_LOGIC_VECTOR ( 127 downto 0 );
    core_event_sysref_alignment_error : in STD_LOGIC;
    core_event_sysref_edge : in STD_LOGIC;
    core_ctrl_manual_sync_request : out STD_LOGIC;
    core_status_state : in STD_LOGIC_VECTOR ( 1 downto 0 );
    core_status_sync : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_tx_axi_0;
  component system_tx_0 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    phy_data : out STD_LOGIC_VECTOR ( 127 downto 0 );
    phy_charisk : out STD_LOGIC_VECTOR ( 15 downto 0 );
    phy_header : out STD_LOGIC_VECTOR ( 7 downto 0 );
    sysref : in STD_LOGIC;
    lmfc_edge : out STD_LOGIC;
    lmfc_clk : out STD_LOGIC;
    sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_data : in STD_LOGIC_VECTOR ( 127 downto 0 );
    tx_ready : out STD_LOGIC;
    tx_valid : in STD_LOGIC;
    cfg_lanes_disable : in STD_LOGIC_VECTOR ( 3 downto 0 );
    cfg_links_disable : in STD_LOGIC_VECTOR ( 0 to 0 );
    cfg_beats_per_multiframe : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_octets_per_frame : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_lmfc_offset : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_sysref_oneshot : in STD_LOGIC;
    cfg_sysref_disable : in STD_LOGIC;
    cfg_continuous_cgs : in STD_LOGIC;
    cfg_continuous_ilas : in STD_LOGIC;
    cfg_skip_ilas : in STD_LOGIC;
    cfg_mframes_per_ilas : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_disable_char_replacement : in STD_LOGIC;
    cfg_disable_scrambler : in STD_LOGIC;
    ilas_config_rd : out STD_LOGIC;
    ilas_config_addr : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ilas_config_data : in STD_LOGIC_VECTOR ( 127 downto 0 );
    ctrl_manual_sync_request : in STD_LOGIC;
    event_sysref_edge : out STD_LOGIC;
    event_sysref_alignment_error : out STD_LOGIC;
    status_sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    status_state : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  end component system_tx_0;
  signal device_clk_1 : STD_LOGIC;
  signal s_axi_1_ARADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_ARREADY : STD_LOGIC;
  signal s_axi_1_ARVALID : STD_LOGIC;
  signal s_axi_1_AWADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_AWREADY : STD_LOGIC;
  signal s_axi_1_AWVALID : STD_LOGIC;
  signal s_axi_1_BREADY : STD_LOGIC;
  signal s_axi_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_BVALID : STD_LOGIC;
  signal s_axi_1_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_RREADY : STD_LOGIC;
  signal s_axi_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_RVALID : STD_LOGIC;
  signal s_axi_1_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_WREADY : STD_LOGIC;
  signal s_axi_1_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_1_WVALID : STD_LOGIC;
  signal s_axi_aclk_1 : STD_LOGIC;
  signal s_axi_aresetn_1 : STD_LOGIC;
  signal sync_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sysref_1 : STD_LOGIC;
  signal tx_axi_core_reset : STD_LOGIC;
  signal tx_axi_irq : STD_LOGIC;
  signal tx_axi_tx_cfg_beats_per_multiframe : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tx_axi_tx_cfg_continuous_cgs : STD_LOGIC;
  signal tx_axi_tx_cfg_continuous_ilas : STD_LOGIC;
  signal tx_axi_tx_cfg_disable_char_replacement : STD_LOGIC;
  signal tx_axi_tx_cfg_disable_scrambler : STD_LOGIC;
  signal tx_axi_tx_cfg_lanes_disable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal tx_axi_tx_cfg_links_disable : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_axi_tx_cfg_lmfc_offset : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tx_axi_tx_cfg_mframes_per_ilas : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tx_axi_tx_cfg_octets_per_frame : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal tx_axi_tx_cfg_skip_ilas : STD_LOGIC;
  signal tx_axi_tx_cfg_sysref_disable : STD_LOGIC;
  signal tx_axi_tx_cfg_sysref_oneshot : STD_LOGIC;
  signal tx_axi_tx_ctrl_manual_sync_request : STD_LOGIC;
  signal tx_data_1_TDATA : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal tx_data_1_TREADY : STD_LOGIC;
  signal tx_data_1_TVALID : STD_LOGIC;
  signal tx_tx_event_sysref_alignment_error : STD_LOGIC;
  signal tx_tx_event_sysref_edge : STD_LOGIC;
  signal tx_tx_ilas_config_addr : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal tx_tx_ilas_config_data : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal tx_tx_ilas_config_rd : STD_LOGIC;
  signal tx_tx_phy0_txcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal tx_tx_phy0_txdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal tx_tx_phy1_txcharisk : STD_LOGIC_VECTOR ( 7 downto 4 );
  signal tx_tx_phy1_txdata : STD_LOGIC_VECTOR ( 63 downto 32 );
  signal tx_tx_phy2_txcharisk : STD_LOGIC_VECTOR ( 11 downto 8 );
  signal tx_tx_phy2_txdata : STD_LOGIC_VECTOR ( 95 downto 64 );
  signal tx_tx_phy3_txcharisk : STD_LOGIC_VECTOR ( 15 downto 12 );
  signal tx_tx_phy3_txdata : STD_LOGIC_VECTOR ( 127 downto 96 );
  signal tx_tx_status_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal tx_tx_status_sync : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_tx_lmfc_clk_UNCONNECTED : STD_LOGIC;
  signal NLW_tx_lmfc_edge_UNCONNECTED : STD_LOGIC;
  signal NLW_tx_phy_header_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
begin
  device_clk_1 <= device_clk;
  irq <= tx_axi_irq;
  s_axi_1_ARADDR(13 downto 0) <= s_axi_araddr(13 downto 0);
  s_axi_1_ARPROT(2 downto 0) <= s_axi_arprot(2 downto 0);
  s_axi_1_ARVALID <= s_axi_arvalid;
  s_axi_1_AWADDR(13 downto 0) <= s_axi_awaddr(13 downto 0);
  s_axi_1_AWPROT(2 downto 0) <= s_axi_awprot(2 downto 0);
  s_axi_1_AWVALID <= s_axi_awvalid;
  s_axi_1_BREADY <= s_axi_bready;
  s_axi_1_RREADY <= s_axi_rready;
  s_axi_1_WDATA(31 downto 0) <= s_axi_wdata(31 downto 0);
  s_axi_1_WSTRB(3 downto 0) <= s_axi_wstrb(3 downto 0);
  s_axi_1_WVALID <= s_axi_wvalid;
  s_axi_aclk_1 <= s_axi_aclk;
  s_axi_aresetn_1 <= s_axi_aresetn;
  s_axi_arready <= s_axi_1_ARREADY;
  s_axi_awready <= s_axi_1_AWREADY;
  s_axi_bresp(1 downto 0) <= s_axi_1_BRESP(1 downto 0);
  s_axi_bvalid <= s_axi_1_BVALID;
  s_axi_rdata(31 downto 0) <= s_axi_1_RDATA(31 downto 0);
  s_axi_rresp(1 downto 0) <= s_axi_1_RRESP(1 downto 0);
  s_axi_rvalid <= s_axi_1_RVALID;
  s_axi_wready <= s_axi_1_WREADY;
  sync_1(0) <= sync(0);
  sysref_1 <= sysref;
  tx_data_1_TDATA(127 downto 0) <= tx_data_tdata(127 downto 0);
  tx_data_1_TVALID <= tx_data_tvalid;
  tx_data_tready <= tx_data_1_TREADY;
  tx_phy0_txcharisk(3 downto 0) <= tx_tx_phy0_txcharisk(3 downto 0);
  tx_phy0_txdata(31 downto 0) <= tx_tx_phy0_txdata(31 downto 0);
  tx_phy1_txcharisk(3 downto 0) <= tx_tx_phy1_txcharisk(7 downto 4);
  tx_phy1_txdata(31 downto 0) <= tx_tx_phy1_txdata(63 downto 32);
  tx_phy2_txcharisk(3 downto 0) <= tx_tx_phy2_txcharisk(11 downto 8);
  tx_phy2_txdata(31 downto 0) <= tx_tx_phy2_txdata(95 downto 64);
  tx_phy3_txcharisk(3 downto 0) <= tx_tx_phy3_txcharisk(15 downto 12);
  tx_phy3_txdata(31 downto 0) <= tx_tx_phy3_txdata(127 downto 96);
tx: component system_tx_0
     port map (
      cfg_beats_per_multiframe(7 downto 0) => tx_axi_tx_cfg_beats_per_multiframe(7 downto 0),
      cfg_continuous_cgs => tx_axi_tx_cfg_continuous_cgs,
      cfg_continuous_ilas => tx_axi_tx_cfg_continuous_ilas,
      cfg_disable_char_replacement => tx_axi_tx_cfg_disable_char_replacement,
      cfg_disable_scrambler => tx_axi_tx_cfg_disable_scrambler,
      cfg_lanes_disable(3 downto 0) => tx_axi_tx_cfg_lanes_disable(3 downto 0),
      cfg_links_disable(0) => tx_axi_tx_cfg_links_disable(0),
      cfg_lmfc_offset(7 downto 0) => tx_axi_tx_cfg_lmfc_offset(7 downto 0),
      cfg_mframes_per_ilas(7 downto 0) => tx_axi_tx_cfg_mframes_per_ilas(7 downto 0),
      cfg_octets_per_frame(7 downto 0) => tx_axi_tx_cfg_octets_per_frame(7 downto 0),
      cfg_skip_ilas => tx_axi_tx_cfg_skip_ilas,
      cfg_sysref_disable => tx_axi_tx_cfg_sysref_disable,
      cfg_sysref_oneshot => tx_axi_tx_cfg_sysref_oneshot,
      clk => device_clk_1,
      ctrl_manual_sync_request => tx_axi_tx_ctrl_manual_sync_request,
      event_sysref_alignment_error => tx_tx_event_sysref_alignment_error,
      event_sysref_edge => tx_tx_event_sysref_edge,
      ilas_config_addr(1 downto 0) => tx_tx_ilas_config_addr(1 downto 0),
      ilas_config_data(127 downto 0) => tx_tx_ilas_config_data(127 downto 0),
      ilas_config_rd => tx_tx_ilas_config_rd,
      lmfc_clk => NLW_tx_lmfc_clk_UNCONNECTED,
      lmfc_edge => NLW_tx_lmfc_edge_UNCONNECTED,
      phy_charisk(15 downto 12) => tx_tx_phy3_txcharisk(15 downto 12),
      phy_charisk(11 downto 8) => tx_tx_phy2_txcharisk(11 downto 8),
      phy_charisk(7 downto 4) => tx_tx_phy1_txcharisk(7 downto 4),
      phy_charisk(3 downto 0) => tx_tx_phy0_txcharisk(3 downto 0),
      phy_data(127 downto 96) => tx_tx_phy3_txdata(127 downto 96),
      phy_data(95 downto 64) => tx_tx_phy2_txdata(95 downto 64),
      phy_data(63 downto 32) => tx_tx_phy1_txdata(63 downto 32),
      phy_data(31 downto 0) => tx_tx_phy0_txdata(31 downto 0),
      phy_header(7 downto 0) => NLW_tx_phy_header_UNCONNECTED(7 downto 0),
      reset => tx_axi_core_reset,
      status_state(1 downto 0) => tx_tx_status_state(1 downto 0),
      status_sync(0) => tx_tx_status_sync(0),
      sync(0) => sync_1(0),
      sysref => sysref_1,
      tx_data(127 downto 0) => tx_data_1_TDATA(127 downto 0),
      tx_ready => tx_data_1_TREADY,
      tx_valid => tx_data_1_TVALID
    );
tx_axi: component system_tx_axi_0
     port map (
      core_cfg_beats_per_multiframe(7 downto 0) => tx_axi_tx_cfg_beats_per_multiframe(7 downto 0),
      core_cfg_continuous_cgs => tx_axi_tx_cfg_continuous_cgs,
      core_cfg_continuous_ilas => tx_axi_tx_cfg_continuous_ilas,
      core_cfg_disable_char_replacement => tx_axi_tx_cfg_disable_char_replacement,
      core_cfg_disable_scrambler => tx_axi_tx_cfg_disable_scrambler,
      core_cfg_lanes_disable(3 downto 0) => tx_axi_tx_cfg_lanes_disable(3 downto 0),
      core_cfg_links_disable(0) => tx_axi_tx_cfg_links_disable(0),
      core_cfg_lmfc_offset(7 downto 0) => tx_axi_tx_cfg_lmfc_offset(7 downto 0),
      core_cfg_mframes_per_ilas(7 downto 0) => tx_axi_tx_cfg_mframes_per_ilas(7 downto 0),
      core_cfg_octets_per_frame(7 downto 0) => tx_axi_tx_cfg_octets_per_frame(7 downto 0),
      core_cfg_skip_ilas => tx_axi_tx_cfg_skip_ilas,
      core_cfg_sysref_disable => tx_axi_tx_cfg_sysref_disable,
      core_cfg_sysref_oneshot => tx_axi_tx_cfg_sysref_oneshot,
      core_clk => device_clk_1,
      core_ctrl_manual_sync_request => tx_axi_tx_ctrl_manual_sync_request,
      core_event_sysref_alignment_error => tx_tx_event_sysref_alignment_error,
      core_event_sysref_edge => tx_tx_event_sysref_edge,
      core_ilas_config_addr(1 downto 0) => tx_tx_ilas_config_addr(1 downto 0),
      core_ilas_config_data(127 downto 0) => tx_tx_ilas_config_data(127 downto 0),
      core_ilas_config_rd => tx_tx_ilas_config_rd,
      core_reset => tx_axi_core_reset,
      core_reset_ext => '0',
      core_status_state(1 downto 0) => tx_tx_status_state(1 downto 0),
      core_status_sync(0) => tx_tx_status_sync(0),
      irq => tx_axi_irq,
      s_axi_aclk => s_axi_aclk_1,
      s_axi_araddr(13 downto 0) => s_axi_1_ARADDR(13 downto 0),
      s_axi_aresetn => s_axi_aresetn_1,
      s_axi_arprot(2 downto 0) => s_axi_1_ARPROT(2 downto 0),
      s_axi_arready => s_axi_1_ARREADY,
      s_axi_arvalid => s_axi_1_ARVALID,
      s_axi_awaddr(13 downto 0) => s_axi_1_AWADDR(13 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_1_AWPROT(2 downto 0),
      s_axi_awready => s_axi_1_AWREADY,
      s_axi_awvalid => s_axi_1_AWVALID,
      s_axi_bready => s_axi_1_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_1_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_1_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_1_RDATA(31 downto 0),
      s_axi_rready => s_axi_1_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_1_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_1_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_1_WDATA(31 downto 0),
      s_axi_wready => s_axi_1_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_1_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_1_WVALID
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity rx_ad9371_tpl_core_imp_WW7IEM is
  port (
    adc_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_dovf : in STD_LOGIC;
    adc_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    link_clk : in STD_LOGIC;
    link_data : in STD_LOGIC_VECTOR ( 127 downto 0 );
    link_sof : in STD_LOGIC_VECTOR ( 3 downto 0 );
    link_valid : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_aresetn : in STD_LOGIC;
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC
  );
end rx_ad9371_tpl_core_imp_WW7IEM;

architecture STRUCTURE of rx_ad9371_tpl_core_imp_WW7IEM is
  component system_adc_tpl_core_0 is
  port (
    link_clk : in STD_LOGIC;
    link_sof : in STD_LOGIC_VECTOR ( 3 downto 0 );
    link_valid : in STD_LOGIC;
    link_data : in STD_LOGIC_VECTOR ( 127 downto 0 );
    link_ready : out STD_LOGIC;
    enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    adc_valid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    adc_data : out STD_LOGIC_VECTOR ( 127 downto 0 );
    adc_dovf : in STD_LOGIC;
    adc_sync_in : in STD_LOGIC;
    adc_rst : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component system_adc_tpl_core_0;
  component system_data_slice_0_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 127 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component system_data_slice_0_0;
  component system_enable_slice_0_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_0_1;
  component system_valid_slice_0_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_0_1;
  component system_data_slice_1_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 127 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component system_data_slice_1_0;
  component system_enable_slice_1_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_1_1;
  component system_valid_slice_1_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_1_1;
  component system_data_slice_2_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 127 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component system_data_slice_2_0;
  component system_enable_slice_2_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_2_1;
  component system_valid_slice_2_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_2_1;
  component system_data_slice_3_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 127 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component system_data_slice_3_0;
  component system_enable_slice_3_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_3_1;
  component system_valid_slice_3_1 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_3_1;
  signal adc_dovf_1 : STD_LOGIC;
  signal adc_tpl_core_adc_data : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal adc_tpl_core_adc_valid : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal adc_tpl_core_enable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal data_slice_0_Dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal data_slice_1_Dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal data_slice_2_Dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal data_slice_3_Dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal enable_slice_0_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal enable_slice_1_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal enable_slice_2_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal enable_slice_3_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal link_clk_1 : STD_LOGIC;
  signal link_data_1 : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal link_sof_1 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal link_valid_1 : STD_LOGIC;
  signal s_axi_1_ARADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_ARREADY : STD_LOGIC;
  signal s_axi_1_ARVALID : STD_LOGIC;
  signal s_axi_1_AWADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_AWREADY : STD_LOGIC;
  signal s_axi_1_AWVALID : STD_LOGIC;
  signal s_axi_1_BREADY : STD_LOGIC;
  signal s_axi_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_BVALID : STD_LOGIC;
  signal s_axi_1_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_RREADY : STD_LOGIC;
  signal s_axi_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_RVALID : STD_LOGIC;
  signal s_axi_1_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_WREADY : STD_LOGIC;
  signal s_axi_1_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_1_WVALID : STD_LOGIC;
  signal s_axi_aclk_1 : STD_LOGIC;
  signal s_axi_aresetn_1 : STD_LOGIC;
  signal valid_slice_0_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal valid_slice_1_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal valid_slice_2_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal valid_slice_3_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_adc_tpl_core_adc_rst_UNCONNECTED : STD_LOGIC;
  signal NLW_adc_tpl_core_link_ready_UNCONNECTED : STD_LOGIC;
begin
  adc_data_0(31 downto 0) <= data_slice_0_Dout(31 downto 0);
  adc_data_1(31 downto 0) <= data_slice_1_Dout(31 downto 0);
  adc_data_2(31 downto 0) <= data_slice_2_Dout(31 downto 0);
  adc_data_3(31 downto 0) <= data_slice_3_Dout(31 downto 0);
  adc_dovf_1 <= adc_dovf;
  adc_enable_0(0) <= enable_slice_0_Dout(0);
  adc_enable_1(0) <= enable_slice_1_Dout(0);
  adc_enable_2(0) <= enable_slice_2_Dout(0);
  adc_enable_3(0) <= enable_slice_3_Dout(0);
  adc_valid_0(0) <= valid_slice_0_Dout(0);
  adc_valid_1(0) <= valid_slice_1_Dout(0);
  adc_valid_2(0) <= valid_slice_2_Dout(0);
  adc_valid_3(0) <= valid_slice_3_Dout(0);
  link_clk_1 <= link_clk;
  link_data_1(127 downto 0) <= link_data(127 downto 0);
  link_sof_1(3 downto 0) <= link_sof(3 downto 0);
  link_valid_1 <= link_valid;
  s_axi_1_ARADDR(12 downto 0) <= s_axi_araddr(12 downto 0);
  s_axi_1_ARPROT(2 downto 0) <= s_axi_arprot(2 downto 0);
  s_axi_1_ARVALID <= s_axi_arvalid;
  s_axi_1_AWADDR(12 downto 0) <= s_axi_awaddr(12 downto 0);
  s_axi_1_AWPROT(2 downto 0) <= s_axi_awprot(2 downto 0);
  s_axi_1_AWVALID <= s_axi_awvalid;
  s_axi_1_BREADY <= s_axi_bready;
  s_axi_1_RREADY <= s_axi_rready;
  s_axi_1_WDATA(31 downto 0) <= s_axi_wdata(31 downto 0);
  s_axi_1_WSTRB(3 downto 0) <= s_axi_wstrb(3 downto 0);
  s_axi_1_WVALID <= s_axi_wvalid;
  s_axi_aclk_1 <= s_axi_aclk;
  s_axi_aresetn_1 <= s_axi_aresetn;
  s_axi_arready <= s_axi_1_ARREADY;
  s_axi_awready <= s_axi_1_AWREADY;
  s_axi_bresp(1 downto 0) <= s_axi_1_BRESP(1 downto 0);
  s_axi_bvalid <= s_axi_1_BVALID;
  s_axi_rdata(31 downto 0) <= s_axi_1_RDATA(31 downto 0);
  s_axi_rresp(1 downto 0) <= s_axi_1_RRESP(1 downto 0);
  s_axi_rvalid <= s_axi_1_RVALID;
  s_axi_wready <= s_axi_1_WREADY;
adc_tpl_core: component system_adc_tpl_core_0
     port map (
      adc_data(127 downto 0) => adc_tpl_core_adc_data(127 downto 0),
      adc_dovf => adc_dovf_1,
      adc_rst => NLW_adc_tpl_core_adc_rst_UNCONNECTED,
      adc_sync_in => '0',
      adc_valid(3 downto 0) => adc_tpl_core_adc_valid(3 downto 0),
      enable(3 downto 0) => adc_tpl_core_enable(3 downto 0),
      link_clk => link_clk_1,
      link_data(127 downto 0) => link_data_1(127 downto 0),
      link_ready => NLW_adc_tpl_core_link_ready_UNCONNECTED,
      link_sof(3 downto 0) => link_sof_1(3 downto 0),
      link_valid => link_valid_1,
      s_axi_aclk => s_axi_aclk_1,
      s_axi_araddr(12 downto 0) => s_axi_1_ARADDR(12 downto 0),
      s_axi_aresetn => s_axi_aresetn_1,
      s_axi_arprot(2 downto 0) => s_axi_1_ARPROT(2 downto 0),
      s_axi_arready => s_axi_1_ARREADY,
      s_axi_arvalid => s_axi_1_ARVALID,
      s_axi_awaddr(12 downto 0) => s_axi_1_AWADDR(12 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_1_AWPROT(2 downto 0),
      s_axi_awready => s_axi_1_AWREADY,
      s_axi_awvalid => s_axi_1_AWVALID,
      s_axi_bready => s_axi_1_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_1_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_1_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_1_RDATA(31 downto 0),
      s_axi_rready => s_axi_1_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_1_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_1_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_1_WDATA(31 downto 0),
      s_axi_wready => s_axi_1_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_1_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_1_WVALID
    );
data_slice_0: component system_data_slice_0_0
     port map (
      Din(127 downto 0) => adc_tpl_core_adc_data(127 downto 0),
      Dout(31 downto 0) => data_slice_0_Dout(31 downto 0)
    );
data_slice_1: component system_data_slice_1_0
     port map (
      Din(127 downto 0) => adc_tpl_core_adc_data(127 downto 0),
      Dout(31 downto 0) => data_slice_1_Dout(31 downto 0)
    );
data_slice_2: component system_data_slice_2_0
     port map (
      Din(127 downto 0) => adc_tpl_core_adc_data(127 downto 0),
      Dout(31 downto 0) => data_slice_2_Dout(31 downto 0)
    );
data_slice_3: component system_data_slice_3_0
     port map (
      Din(127 downto 0) => adc_tpl_core_adc_data(127 downto 0),
      Dout(31 downto 0) => data_slice_3_Dout(31 downto 0)
    );
enable_slice_0: component system_enable_slice_0_1
     port map (
      Din(3 downto 0) => adc_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_0_Dout(0)
    );
enable_slice_1: component system_enable_slice_1_1
     port map (
      Din(3 downto 0) => adc_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_1_Dout(0)
    );
enable_slice_2: component system_enable_slice_2_1
     port map (
      Din(3 downto 0) => adc_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_2_Dout(0)
    );
enable_slice_3: component system_enable_slice_3_1
     port map (
      Din(3 downto 0) => adc_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_3_Dout(0)
    );
valid_slice_0: component system_valid_slice_0_1
     port map (
      Din(3 downto 0) => adc_tpl_core_adc_valid(3 downto 0),
      Dout(0) => valid_slice_0_Dout(0)
    );
valid_slice_1: component system_valid_slice_1_1
     port map (
      Din(3 downto 0) => adc_tpl_core_adc_valid(3 downto 0),
      Dout(0) => valid_slice_1_Dout(0)
    );
valid_slice_2: component system_valid_slice_2_1
     port map (
      Din(3 downto 0) => adc_tpl_core_adc_valid(3 downto 0),
      Dout(0) => valid_slice_2_Dout(0)
    );
valid_slice_3: component system_valid_slice_3_1
     port map (
      Din(3 downto 0) => adc_tpl_core_adc_valid(3 downto 0),
      Dout(0) => valid_slice_3_Dout(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tx_ad9371_tpl_core_imp_1J4NRGE is
  port (
    dac_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_dunf : in STD_LOGIC;
    dac_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    link_clk : in STD_LOGIC;
    link_tdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
    link_tready : in STD_LOGIC;
    link_tvalid : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_aresetn : in STD_LOGIC;
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC
  );
end tx_ad9371_tpl_core_imp_1J4NRGE;

architecture STRUCTURE of tx_ad9371_tpl_core_imp_1J4NRGE is
  component system_dac_tpl_core_0 is
  port (
    link_clk : in STD_LOGIC;
    link_valid : out STD_LOGIC;
    link_ready : in STD_LOGIC;
    link_data : out STD_LOGIC_VECTOR ( 127 downto 0 );
    enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dac_valid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    dac_ddata : in STD_LOGIC_VECTOR ( 127 downto 0 );
    dac_dunf : in STD_LOGIC;
    dac_sync_in : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  end component system_dac_tpl_core_0;
  component system_data_concat0_0 is
  port (
    In0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    In1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    In2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    In3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dout : out STD_LOGIC_VECTOR ( 127 downto 0 )
  );
  end component system_data_concat0_0;
  component system_enable_slice_0_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_0_0;
  component system_valid_slice_0_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_0_0;
  component system_enable_slice_1_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_1_0;
  component system_valid_slice_1_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_1_0;
  component system_enable_slice_2_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_2_0;
  component system_valid_slice_2_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_2_0;
  component system_enable_slice_3_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_enable_slice_3_0;
  component system_valid_slice_3_0 is
  port (
    Din : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Dout : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component system_valid_slice_3_0;
  signal dac_data_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_data_1_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_data_2_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_data_3_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_dunf_1 : STD_LOGIC;
  signal dac_tpl_core_dac_valid : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal dac_tpl_core_enable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal dac_tpl_core_link_TDATA : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal dac_tpl_core_link_TREADY : STD_LOGIC;
  signal dac_tpl_core_link_TVALID : STD_LOGIC;
  signal data_concat0_dout : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal enable_slice_0_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal enable_slice_1_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal enable_slice_2_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal enable_slice_3_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal link_clk_1 : STD_LOGIC;
  signal s_axi_1_ARADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_ARREADY : STD_LOGIC;
  signal s_axi_1_ARVALID : STD_LOGIC;
  signal s_axi_1_AWADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_1_AWREADY : STD_LOGIC;
  signal s_axi_1_AWVALID : STD_LOGIC;
  signal s_axi_1_BREADY : STD_LOGIC;
  signal s_axi_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_BVALID : STD_LOGIC;
  signal s_axi_1_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_RREADY : STD_LOGIC;
  signal s_axi_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_1_RVALID : STD_LOGIC;
  signal s_axi_1_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_1_WREADY : STD_LOGIC;
  signal s_axi_1_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_1_WVALID : STD_LOGIC;
  signal s_axi_aclk_1 : STD_LOGIC;
  signal s_axi_aresetn_1 : STD_LOGIC;
  signal valid_slice_0_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal valid_slice_1_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal valid_slice_2_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
  signal valid_slice_3_Dout : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  dac_data_0_1(31 downto 0) <= dac_data_0(31 downto 0);
  dac_data_1_1(31 downto 0) <= dac_data_1(31 downto 0);
  dac_data_2_1(31 downto 0) <= dac_data_2(31 downto 0);
  dac_data_3_1(31 downto 0) <= dac_data_3(31 downto 0);
  dac_dunf_1 <= dac_dunf;
  dac_enable_0(0) <= enable_slice_0_Dout(0);
  dac_enable_1(0) <= enable_slice_1_Dout(0);
  dac_enable_2(0) <= enable_slice_2_Dout(0);
  dac_enable_3(0) <= enable_slice_3_Dout(0);
  dac_tpl_core_link_TREADY <= link_tready;
  dac_valid_0(0) <= valid_slice_0_Dout(0);
  dac_valid_1(0) <= valid_slice_1_Dout(0);
  dac_valid_2(0) <= valid_slice_2_Dout(0);
  dac_valid_3(0) <= valid_slice_3_Dout(0);
  link_clk_1 <= link_clk;
  link_tdata(127 downto 0) <= dac_tpl_core_link_TDATA(127 downto 0);
  link_tvalid <= dac_tpl_core_link_TVALID;
  s_axi_1_ARADDR(12 downto 0) <= s_axi_araddr(12 downto 0);
  s_axi_1_ARPROT(2 downto 0) <= s_axi_arprot(2 downto 0);
  s_axi_1_ARVALID <= s_axi_arvalid;
  s_axi_1_AWADDR(12 downto 0) <= s_axi_awaddr(12 downto 0);
  s_axi_1_AWPROT(2 downto 0) <= s_axi_awprot(2 downto 0);
  s_axi_1_AWVALID <= s_axi_awvalid;
  s_axi_1_BREADY <= s_axi_bready;
  s_axi_1_RREADY <= s_axi_rready;
  s_axi_1_WDATA(31 downto 0) <= s_axi_wdata(31 downto 0);
  s_axi_1_WSTRB(3 downto 0) <= s_axi_wstrb(3 downto 0);
  s_axi_1_WVALID <= s_axi_wvalid;
  s_axi_aclk_1 <= s_axi_aclk;
  s_axi_aresetn_1 <= s_axi_aresetn;
  s_axi_arready <= s_axi_1_ARREADY;
  s_axi_awready <= s_axi_1_AWREADY;
  s_axi_bresp(1 downto 0) <= s_axi_1_BRESP(1 downto 0);
  s_axi_bvalid <= s_axi_1_BVALID;
  s_axi_rdata(31 downto 0) <= s_axi_1_RDATA(31 downto 0);
  s_axi_rresp(1 downto 0) <= s_axi_1_RRESP(1 downto 0);
  s_axi_rvalid <= s_axi_1_RVALID;
  s_axi_wready <= s_axi_1_WREADY;
dac_tpl_core: component system_dac_tpl_core_0
     port map (
      dac_ddata(127 downto 0) => data_concat0_dout(127 downto 0),
      dac_dunf => dac_dunf_1,
      dac_sync_in => '0',
      dac_valid(3 downto 0) => dac_tpl_core_dac_valid(3 downto 0),
      enable(3 downto 0) => dac_tpl_core_enable(3 downto 0),
      link_clk => link_clk_1,
      link_data(127 downto 0) => dac_tpl_core_link_TDATA(127 downto 0),
      link_ready => dac_tpl_core_link_TREADY,
      link_valid => dac_tpl_core_link_TVALID,
      s_axi_aclk => s_axi_aclk_1,
      s_axi_araddr(12 downto 0) => s_axi_1_ARADDR(12 downto 0),
      s_axi_aresetn => s_axi_aresetn_1,
      s_axi_arprot(2 downto 0) => s_axi_1_ARPROT(2 downto 0),
      s_axi_arready => s_axi_1_ARREADY,
      s_axi_arvalid => s_axi_1_ARVALID,
      s_axi_awaddr(12 downto 0) => s_axi_1_AWADDR(12 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_1_AWPROT(2 downto 0),
      s_axi_awready => s_axi_1_AWREADY,
      s_axi_awvalid => s_axi_1_AWVALID,
      s_axi_bready => s_axi_1_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_1_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_1_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_1_RDATA(31 downto 0),
      s_axi_rready => s_axi_1_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_1_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_1_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_1_WDATA(31 downto 0),
      s_axi_wready => s_axi_1_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_1_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_1_WVALID
    );
data_concat0: component system_data_concat0_0
     port map (
      In0(31 downto 0) => dac_data_0_1(31 downto 0),
      In1(31 downto 0) => dac_data_1_1(31 downto 0),
      In2(31 downto 0) => dac_data_2_1(31 downto 0),
      In3(31 downto 0) => dac_data_3_1(31 downto 0),
      dout(127 downto 0) => data_concat0_dout(127 downto 0)
    );
enable_slice_0: component system_enable_slice_0_0
     port map (
      Din(3 downto 0) => dac_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_0_Dout(0)
    );
enable_slice_1: component system_enable_slice_1_0
     port map (
      Din(3 downto 0) => dac_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_1_Dout(0)
    );
enable_slice_2: component system_enable_slice_2_0
     port map (
      Din(3 downto 0) => dac_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_2_Dout(0)
    );
enable_slice_3: component system_enable_slice_3_0
     port map (
      Din(3 downto 0) => dac_tpl_core_enable(3 downto 0),
      Dout(0) => enable_slice_3_Dout(0)
    );
valid_slice_0: component system_valid_slice_0_0
     port map (
      Din(3 downto 0) => dac_tpl_core_dac_valid(3 downto 0),
      Dout(0) => valid_slice_0_Dout(0)
    );
valid_slice_1: component system_valid_slice_1_0
     port map (
      Din(3 downto 0) => dac_tpl_core_dac_valid(3 downto 0),
      Dout(0) => valid_slice_1_Dout(0)
    );
valid_slice_2: component system_valid_slice_2_0
     port map (
      Din(3 downto 0) => dac_tpl_core_dac_valid(3 downto 0),
      Dout(0) => valid_slice_2_Dout(0)
    );
valid_slice_3: component system_valid_slice_3_0
     port map (
      Din(3 downto 0) => dac_tpl_core_dac_valid(3 downto 0),
      Dout(0) => valid_slice_3_Dout(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity system is
  port (
    adc_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_dovf : in STD_LOGIC;
    adc_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    adc_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dac_dunf : in STD_LOGIC;
    dac_enable_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_enable_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    dac_valid_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_clkgen_ref : in STD_LOGIC;
    rx_data_0_n : in STD_LOGIC;
    rx_data_0_p : in STD_LOGIC;
    rx_data_1_n : in STD_LOGIC;
    rx_data_1_p : in STD_LOGIC;
    rx_data_2_n : in STD_LOGIC;
    rx_data_2_p : in STD_LOGIC;
    rx_data_3_n : in STD_LOGIC;
    rx_data_3_p : in STD_LOGIC;
    rx_linkclk : out STD_LOGIC;
    rx_ref_clk_0 : in STD_LOGIC;
    rx_sync : out STD_LOGIC_VECTOR ( 0 to 0 );
    rx_sysref_0 : in STD_LOGIC;
    rxoutclk : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_rx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_arready : out STD_LOGIC;
    s_axi_rx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_adxcvr_awready : out STD_LOGIC;
    s_axi_rx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_rx_adxcvr_bready : in STD_LOGIC;
    s_axi_rx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_rready : in STD_LOGIC;
    s_axi_rx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_rx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_adxcvr_wready : out STD_LOGIC;
    s_axi_rx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_rx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_arready : out STD_LOGIC;
    s_axi_rx_clkgen_arvalid : in STD_LOGIC;
    s_axi_rx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_rx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_clkgen_awready : out STD_LOGIC;
    s_axi_rx_clkgen_awvalid : in STD_LOGIC;
    s_axi_rx_clkgen_bready : in STD_LOGIC;
    s_axi_rx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_bvalid : out STD_LOGIC;
    s_axi_rx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_rready : in STD_LOGIC;
    s_axi_rx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_clkgen_rvalid : out STD_LOGIC;
    s_axi_rx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_clkgen_wready : out STD_LOGIC;
    s_axi_rx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_clkgen_wvalid : in STD_LOGIC;
    s_axi_rx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_arready : out STD_LOGIC;
    s_axi_rx_jesd_arvalid : in STD_LOGIC;
    s_axi_rx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_rx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_jesd_awready : out STD_LOGIC;
    s_axi_rx_jesd_awvalid : in STD_LOGIC;
    s_axi_rx_jesd_bready : in STD_LOGIC;
    s_axi_rx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_bvalid : out STD_LOGIC;
    s_axi_rx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_rready : in STD_LOGIC;
    s_axi_rx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_jesd_rvalid : out STD_LOGIC;
    s_axi_rx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_jesd_wready : out STD_LOGIC;
    s_axi_rx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_jesd_wvalid : in STD_LOGIC;
    s_axi_rx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_arready : out STD_LOGIC;
    s_axi_rx_tpl_arvalid : in STD_LOGIC;
    s_axi_rx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_rx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_rx_tpl_awready : out STD_LOGIC;
    s_axi_rx_tpl_awvalid : in STD_LOGIC;
    s_axi_rx_tpl_bready : in STD_LOGIC;
    s_axi_rx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_bvalid : out STD_LOGIC;
    s_axi_rx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_rready : in STD_LOGIC;
    s_axi_rx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rx_tpl_rvalid : out STD_LOGIC;
    s_axi_rx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rx_tpl_wready : out STD_LOGIC;
    s_axi_rx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rx_tpl_wvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_arready : out STD_LOGIC;
    s_axi_tx_adxcvr_arvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_adxcvr_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_adxcvr_awready : out STD_LOGIC;
    s_axi_tx_adxcvr_awvalid : in STD_LOGIC;
    s_axi_tx_adxcvr_bready : in STD_LOGIC;
    s_axi_tx_adxcvr_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_bvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_rready : in STD_LOGIC;
    s_axi_tx_adxcvr_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_adxcvr_rvalid : out STD_LOGIC;
    s_axi_tx_adxcvr_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_adxcvr_wready : out STD_LOGIC;
    s_axi_tx_adxcvr_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_adxcvr_wvalid : in STD_LOGIC;
    s_axi_tx_clkgen_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_arready : out STD_LOGIC;
    s_axi_tx_clkgen_arvalid : in STD_LOGIC;
    s_axi_tx_clkgen_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_tx_clkgen_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_clkgen_awready : out STD_LOGIC;
    s_axi_tx_clkgen_awvalid : in STD_LOGIC;
    s_axi_tx_clkgen_bready : in STD_LOGIC;
    s_axi_tx_clkgen_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_bvalid : out STD_LOGIC;
    s_axi_tx_clkgen_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_rready : in STD_LOGIC;
    s_axi_tx_clkgen_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_clkgen_rvalid : out STD_LOGIC;
    s_axi_tx_clkgen_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_clkgen_wready : out STD_LOGIC;
    s_axi_tx_clkgen_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_clkgen_wvalid : in STD_LOGIC;
    s_axi_tx_jesd_araddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_arready : out STD_LOGIC;
    s_axi_tx_jesd_arvalid : in STD_LOGIC;
    s_axi_tx_jesd_awaddr : in STD_LOGIC_VECTOR ( 13 downto 0 );
    s_axi_tx_jesd_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_jesd_awready : out STD_LOGIC;
    s_axi_tx_jesd_awvalid : in STD_LOGIC;
    s_axi_tx_jesd_bready : in STD_LOGIC;
    s_axi_tx_jesd_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_bvalid : out STD_LOGIC;
    s_axi_tx_jesd_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_rready : in STD_LOGIC;
    s_axi_tx_jesd_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_jesd_rvalid : out STD_LOGIC;
    s_axi_tx_jesd_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_jesd_wready : out STD_LOGIC;
    s_axi_tx_jesd_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_jesd_wvalid : in STD_LOGIC;
    s_axi_tx_tpl_araddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_arready : out STD_LOGIC;
    s_axi_tx_tpl_arvalid : in STD_LOGIC;
    s_axi_tx_tpl_awaddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    s_axi_tx_tpl_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_tx_tpl_awready : out STD_LOGIC;
    s_axi_tx_tpl_awvalid : in STD_LOGIC;
    s_axi_tx_tpl_bready : in STD_LOGIC;
    s_axi_tx_tpl_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_bvalid : out STD_LOGIC;
    s_axi_tx_tpl_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_rready : in STD_LOGIC;
    s_axi_tx_tpl_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_tx_tpl_rvalid : out STD_LOGIC;
    s_axi_tx_tpl_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_tx_tpl_wready : out STD_LOGIC;
    s_axi_tx_tpl_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_tx_tpl_wvalid : in STD_LOGIC;
    tx_clkgen_ref : in STD_LOGIC;
    tx_data_0_n : out STD_LOGIC;
    tx_data_0_p : out STD_LOGIC;
    tx_data_1_n : out STD_LOGIC;
    tx_data_1_p : out STD_LOGIC;
    tx_data_2_n : out STD_LOGIC;
    tx_data_2_p : out STD_LOGIC;
    tx_data_3_n : out STD_LOGIC;
    tx_data_3_p : out STD_LOGIC;
    tx_linkclk : out STD_LOGIC;
    tx_ref_clk_0 : in STD_LOGIC;
    tx_sync : in STD_LOGIC_VECTOR ( 0 to 0 );
    tx_sysref_0 : in STD_LOGIC;
    txoutclk : out STD_LOGIC
  );
  attribute core_generation_info : string;
  attribute core_generation_info of system : entity is "system,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=system,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=36,numReposBlks=32,numNonXlnxBlks=11,numHierBlks=4,maxHierDepth=1,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_BD}";
  attribute hw_handoff : string;
  attribute hw_handoff of system : entity is "system.hwdef";
end system;

architecture STRUCTURE of system is
  component system_axi_ad9371_tx_clkgen_0 is
  port (
    clk : in STD_LOGIC;
    clk_0 : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  end component system_axi_ad9371_tx_clkgen_0;
  component system_axi_ad9371_tx_xcvr_0 is
  port (
    up_cm_enb_0 : out STD_LOGIC;
    up_cm_addr_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_cm_wr_0 : out STD_LOGIC;
    up_cm_wdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_cm_rdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_cm_ready_0 : in STD_LOGIC;
    up_ch_pll_locked_0 : in STD_LOGIC;
    up_ch_rst_0 : out STD_LOGIC;
    up_ch_user_ready_0 : out STD_LOGIC;
    up_ch_rst_done_0 : in STD_LOGIC;
    up_ch_lpm_dfe_n_0 : out STD_LOGIC;
    up_ch_rate_0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_0 : out STD_LOGIC;
    up_ch_addr_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_0 : out STD_LOGIC;
    up_ch_wdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_0 : in STD_LOGIC;
    up_ch_pll_locked_1 : in STD_LOGIC;
    up_ch_rst_1 : out STD_LOGIC;
    up_ch_user_ready_1 : out STD_LOGIC;
    up_ch_rst_done_1 : in STD_LOGIC;
    up_ch_lpm_dfe_n_1 : out STD_LOGIC;
    up_ch_rate_1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_1 : out STD_LOGIC;
    up_ch_addr_1 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_1 : out STD_LOGIC;
    up_ch_wdata_1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_1 : in STD_LOGIC;
    up_ch_pll_locked_2 : in STD_LOGIC;
    up_ch_rst_2 : out STD_LOGIC;
    up_ch_user_ready_2 : out STD_LOGIC;
    up_ch_rst_done_2 : in STD_LOGIC;
    up_ch_lpm_dfe_n_2 : out STD_LOGIC;
    up_ch_rate_2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_2 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_2 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_2 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_2 : out STD_LOGIC;
    up_ch_addr_2 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_2 : out STD_LOGIC;
    up_ch_wdata_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_2 : in STD_LOGIC;
    up_ch_pll_locked_3 : in STD_LOGIC;
    up_ch_rst_3 : out STD_LOGIC;
    up_ch_user_ready_3 : out STD_LOGIC;
    up_ch_rst_done_3 : in STD_LOGIC;
    up_ch_lpm_dfe_n_3 : out STD_LOGIC;
    up_ch_rate_3 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_3 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_3 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_3 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_3 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_3 : out STD_LOGIC;
    up_ch_addr_3 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_3 : out STD_LOGIC;
    up_ch_wdata_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_3 : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    up_status : out STD_LOGIC;
    up_pll_rst : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rready : in STD_LOGIC
  );
  end component system_axi_ad9371_tx_xcvr_0;
  component system_axi_ad9371_rx_clkgen_0 is
  port (
    clk : in STD_LOGIC;
    clk_0 : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rready : in STD_LOGIC;
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  end component system_axi_ad9371_rx_clkgen_0;
  component system_axi_ad9371_rx_xcvr_0 is
  port (
    up_es_enb_0 : out STD_LOGIC;
    up_es_addr_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_0 : out STD_LOGIC;
    up_es_reset_0 : out STD_LOGIC;
    up_es_wdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_0 : in STD_LOGIC;
    up_ch_pll_locked_0 : in STD_LOGIC;
    up_ch_rst_0 : out STD_LOGIC;
    up_ch_user_ready_0 : out STD_LOGIC;
    up_ch_rst_done_0 : in STD_LOGIC;
    up_ch_lpm_dfe_n_0 : out STD_LOGIC;
    up_ch_rate_0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_0 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_0 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_0 : out STD_LOGIC;
    up_ch_addr_0 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_0 : out STD_LOGIC;
    up_ch_wdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_0 : in STD_LOGIC;
    up_es_enb_1 : out STD_LOGIC;
    up_es_addr_1 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_1 : out STD_LOGIC;
    up_es_reset_1 : out STD_LOGIC;
    up_es_wdata_1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_1 : in STD_LOGIC;
    up_ch_pll_locked_1 : in STD_LOGIC;
    up_ch_rst_1 : out STD_LOGIC;
    up_ch_user_ready_1 : out STD_LOGIC;
    up_ch_rst_done_1 : in STD_LOGIC;
    up_ch_lpm_dfe_n_1 : out STD_LOGIC;
    up_ch_rate_1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_1 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_1 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_1 : out STD_LOGIC;
    up_ch_addr_1 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_1 : out STD_LOGIC;
    up_ch_wdata_1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_1 : in STD_LOGIC;
    up_es_enb_2 : out STD_LOGIC;
    up_es_addr_2 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_2 : out STD_LOGIC;
    up_es_reset_2 : out STD_LOGIC;
    up_es_wdata_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_2 : in STD_LOGIC;
    up_ch_pll_locked_2 : in STD_LOGIC;
    up_ch_rst_2 : out STD_LOGIC;
    up_ch_user_ready_2 : out STD_LOGIC;
    up_ch_rst_done_2 : in STD_LOGIC;
    up_ch_lpm_dfe_n_2 : out STD_LOGIC;
    up_ch_rate_2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_2 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_2 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_2 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_2 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_2 : out STD_LOGIC;
    up_ch_addr_2 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_2 : out STD_LOGIC;
    up_ch_wdata_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_2 : in STD_LOGIC;
    up_es_enb_3 : out STD_LOGIC;
    up_es_addr_3 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_3 : out STD_LOGIC;
    up_es_reset_3 : out STD_LOGIC;
    up_es_wdata_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_3 : in STD_LOGIC;
    up_ch_pll_locked_3 : in STD_LOGIC;
    up_ch_rst_3 : out STD_LOGIC;
    up_ch_user_ready_3 : out STD_LOGIC;
    up_ch_rst_done_3 : in STD_LOGIC;
    up_ch_lpm_dfe_n_3 : out STD_LOGIC;
    up_ch_rate_3 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_sys_clk_sel_3 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    up_ch_out_clk_sel_3 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    up_ch_tx_diffctrl_3 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_postcursor_3 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_tx_precursor_3 : out STD_LOGIC_VECTOR ( 4 downto 0 );
    up_ch_enb_3 : out STD_LOGIC;
    up_ch_addr_3 : out STD_LOGIC_VECTOR ( 11 downto 0 );
    up_ch_wr_3 : out STD_LOGIC;
    up_ch_wdata_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_rdata_3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_ch_ready_3 : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_aresetn : in STD_LOGIC;
    up_status : out STD_LOGIC;
    up_pll_rst : out STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bready : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rready : in STD_LOGIC;
    m_axi_awvalid : out STD_LOGIC;
    m_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_awready : in STD_LOGIC;
    m_axi_wvalid : out STD_LOGIC;
    m_axi_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_axi_wready : in STD_LOGIC;
    m_axi_bvalid : in STD_LOGIC;
    m_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_bready : out STD_LOGIC;
    m_axi_arvalid : out STD_LOGIC;
    m_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axi_arready : in STD_LOGIC;
    m_axi_rvalid : in STD_LOGIC;
    m_axi_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_axi_rready : out STD_LOGIC
  );
  end component system_axi_ad9371_rx_xcvr_0;
  component system_util_ad9371_xcvr_0 is
  port (
    up_rstn : in STD_LOGIC;
    up_clk : in STD_LOGIC;
    qpll_ref_clk_0 : in STD_LOGIC;
    up_qpll_rst_0 : in STD_LOGIC;
    cpll_ref_clk_0 : in STD_LOGIC;
    up_cpll_rst_0 : in STD_LOGIC;
    rx_0_p : in STD_LOGIC;
    rx_0_n : in STD_LOGIC;
    rx_out_clk_0 : out STD_LOGIC;
    rx_clk_0 : in STD_LOGIC;
    rx_charisk_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_disperr_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_notintable_0 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_data_0 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_calign_0 : in STD_LOGIC;
    tx_0_p : out STD_LOGIC;
    tx_0_n : out STD_LOGIC;
    tx_out_clk_0 : out STD_LOGIC;
    tx_clk_0 : in STD_LOGIC;
    tx_charisk_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_data_0 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    up_cm_enb_0 : in STD_LOGIC;
    up_cm_addr_0 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_cm_wr_0 : in STD_LOGIC;
    up_cm_wdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_cm_rdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_cm_ready_0 : out STD_LOGIC;
    up_es_enb_0 : in STD_LOGIC;
    up_es_addr_0 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_0 : in STD_LOGIC;
    up_es_wdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_0 : out STD_LOGIC;
    up_es_reset_0 : in STD_LOGIC;
    up_rx_pll_locked_0 : out STD_LOGIC;
    up_rx_rst_0 : in STD_LOGIC;
    up_rx_user_ready_0 : in STD_LOGIC;
    up_rx_rst_done_0 : out STD_LOGIC;
    up_rx_lpm_dfe_n_0 : in STD_LOGIC;
    up_rx_rate_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_sys_clk_sel_0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_rx_out_clk_sel_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_enb_0 : in STD_LOGIC;
    up_rx_addr_0 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_rx_wr_0 : in STD_LOGIC;
    up_rx_wdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_rdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_ready_0 : out STD_LOGIC;
    up_tx_pll_locked_0 : out STD_LOGIC;
    up_tx_rst_0 : in STD_LOGIC;
    up_tx_user_ready_0 : in STD_LOGIC;
    up_tx_rst_done_0 : out STD_LOGIC;
    up_tx_lpm_dfe_n_0 : in STD_LOGIC;
    up_tx_rate_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_sys_clk_sel_0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_tx_out_clk_sel_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_diffctrl_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_postcursor_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_precursor_0 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_enb_0 : in STD_LOGIC;
    up_tx_addr_0 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_tx_wr_0 : in STD_LOGIC;
    up_tx_wdata_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_rdata_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_ready_0 : out STD_LOGIC;
    cpll_ref_clk_1 : in STD_LOGIC;
    up_cpll_rst_1 : in STD_LOGIC;
    rx_1_p : in STD_LOGIC;
    rx_1_n : in STD_LOGIC;
    rx_out_clk_1 : out STD_LOGIC;
    rx_clk_1 : in STD_LOGIC;
    rx_charisk_1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_disperr_1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_notintable_1 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_data_1 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_calign_1 : in STD_LOGIC;
    tx_1_p : out STD_LOGIC;
    tx_1_n : out STD_LOGIC;
    tx_out_clk_1 : out STD_LOGIC;
    tx_clk_1 : in STD_LOGIC;
    tx_charisk_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_data_1 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    up_es_enb_1 : in STD_LOGIC;
    up_es_addr_1 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_1 : in STD_LOGIC;
    up_es_wdata_1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_1 : out STD_LOGIC;
    up_es_reset_1 : in STD_LOGIC;
    up_rx_pll_locked_1 : out STD_LOGIC;
    up_rx_rst_1 : in STD_LOGIC;
    up_rx_user_ready_1 : in STD_LOGIC;
    up_rx_rst_done_1 : out STD_LOGIC;
    up_rx_lpm_dfe_n_1 : in STD_LOGIC;
    up_rx_rate_1 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_sys_clk_sel_1 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_rx_out_clk_sel_1 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_enb_1 : in STD_LOGIC;
    up_rx_addr_1 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_rx_wr_1 : in STD_LOGIC;
    up_rx_wdata_1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_rdata_1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_ready_1 : out STD_LOGIC;
    up_tx_pll_locked_1 : out STD_LOGIC;
    up_tx_rst_1 : in STD_LOGIC;
    up_tx_user_ready_1 : in STD_LOGIC;
    up_tx_rst_done_1 : out STD_LOGIC;
    up_tx_lpm_dfe_n_1 : in STD_LOGIC;
    up_tx_rate_1 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_sys_clk_sel_1 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_tx_out_clk_sel_1 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_diffctrl_1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_postcursor_1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_precursor_1 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_enb_1 : in STD_LOGIC;
    up_tx_addr_1 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_tx_wr_1 : in STD_LOGIC;
    up_tx_wdata_1 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_rdata_1 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_ready_1 : out STD_LOGIC;
    cpll_ref_clk_2 : in STD_LOGIC;
    up_cpll_rst_2 : in STD_LOGIC;
    rx_2_p : in STD_LOGIC;
    rx_2_n : in STD_LOGIC;
    rx_out_clk_2 : out STD_LOGIC;
    rx_clk_2 : in STD_LOGIC;
    rx_charisk_2 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_disperr_2 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_notintable_2 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_data_2 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_calign_2 : in STD_LOGIC;
    tx_2_p : out STD_LOGIC;
    tx_2_n : out STD_LOGIC;
    tx_out_clk_2 : out STD_LOGIC;
    tx_clk_2 : in STD_LOGIC;
    tx_charisk_2 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_data_2 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    up_es_enb_2 : in STD_LOGIC;
    up_es_addr_2 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_2 : in STD_LOGIC;
    up_es_wdata_2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_2 : out STD_LOGIC;
    up_es_reset_2 : in STD_LOGIC;
    up_rx_pll_locked_2 : out STD_LOGIC;
    up_rx_rst_2 : in STD_LOGIC;
    up_rx_user_ready_2 : in STD_LOGIC;
    up_rx_rst_done_2 : out STD_LOGIC;
    up_rx_lpm_dfe_n_2 : in STD_LOGIC;
    up_rx_rate_2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_sys_clk_sel_2 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_rx_out_clk_sel_2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_enb_2 : in STD_LOGIC;
    up_rx_addr_2 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_rx_wr_2 : in STD_LOGIC;
    up_rx_wdata_2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_rdata_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_ready_2 : out STD_LOGIC;
    up_tx_pll_locked_2 : out STD_LOGIC;
    up_tx_rst_2 : in STD_LOGIC;
    up_tx_user_ready_2 : in STD_LOGIC;
    up_tx_rst_done_2 : out STD_LOGIC;
    up_tx_lpm_dfe_n_2 : in STD_LOGIC;
    up_tx_rate_2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_sys_clk_sel_2 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_tx_out_clk_sel_2 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_diffctrl_2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_postcursor_2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_precursor_2 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_enb_2 : in STD_LOGIC;
    up_tx_addr_2 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_tx_wr_2 : in STD_LOGIC;
    up_tx_wdata_2 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_rdata_2 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_ready_2 : out STD_LOGIC;
    cpll_ref_clk_3 : in STD_LOGIC;
    up_cpll_rst_3 : in STD_LOGIC;
    rx_3_p : in STD_LOGIC;
    rx_3_n : in STD_LOGIC;
    rx_out_clk_3 : out STD_LOGIC;
    rx_clk_3 : in STD_LOGIC;
    rx_charisk_3 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_disperr_3 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_notintable_3 : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rx_data_3 : out STD_LOGIC_VECTOR ( 31 downto 0 );
    rx_calign_3 : in STD_LOGIC;
    tx_3_p : out STD_LOGIC;
    tx_3_n : out STD_LOGIC;
    tx_out_clk_3 : out STD_LOGIC;
    tx_clk_3 : in STD_LOGIC;
    tx_charisk_3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    tx_data_3 : in STD_LOGIC_VECTOR ( 31 downto 0 );
    up_es_enb_3 : in STD_LOGIC;
    up_es_addr_3 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_es_wr_3 : in STD_LOGIC;
    up_es_wdata_3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_rdata_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_es_ready_3 : out STD_LOGIC;
    up_es_reset_3 : in STD_LOGIC;
    up_rx_pll_locked_3 : out STD_LOGIC;
    up_rx_rst_3 : in STD_LOGIC;
    up_rx_user_ready_3 : in STD_LOGIC;
    up_rx_rst_done_3 : out STD_LOGIC;
    up_rx_lpm_dfe_n_3 : in STD_LOGIC;
    up_rx_rate_3 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_sys_clk_sel_3 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_rx_out_clk_sel_3 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_rx_enb_3 : in STD_LOGIC;
    up_rx_addr_3 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_rx_wr_3 : in STD_LOGIC;
    up_rx_wdata_3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_rdata_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_rx_ready_3 : out STD_LOGIC;
    up_tx_pll_locked_3 : out STD_LOGIC;
    up_tx_rst_3 : in STD_LOGIC;
    up_tx_user_ready_3 : in STD_LOGIC;
    up_tx_rst_done_3 : out STD_LOGIC;
    up_tx_lpm_dfe_n_3 : in STD_LOGIC;
    up_tx_rate_3 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_sys_clk_sel_3 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    up_tx_out_clk_sel_3 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    up_tx_diffctrl_3 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_postcursor_3 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_precursor_3 : in STD_LOGIC_VECTOR ( 4 downto 0 );
    up_tx_enb_3 : in STD_LOGIC;
    up_tx_addr_3 : in STD_LOGIC_VECTOR ( 11 downto 0 );
    up_tx_wr_3 : in STD_LOGIC;
    up_tx_wdata_3 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_rdata_3 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    up_tx_ready_3 : out STD_LOGIC
  );
  end component system_util_ad9371_xcvr_0;
  signal ad9371_rx_device_clk : STD_LOGIC;
  signal ad9371_tx_device_clk : STD_LOGIC;
  signal adc_dovf_0_1 : STD_LOGIC;
  signal axi_ad9371_rx_jesd_phy_en_char_align : STD_LOGIC;
  signal axi_ad9371_rx_jesd_rx_data_tdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal axi_ad9371_rx_jesd_rx_data_tvalid : STD_LOGIC;
  signal axi_ad9371_rx_jesd_rx_sof : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ad9371_rx_jesd_sync : STD_LOGIC_VECTOR ( 0 to 0 );
  signal axi_ad9371_rx_xcvr_up_ch_0_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_0_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_0_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_0_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_0_pll_locked : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_0_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_0_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_0_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_0_rst : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_0_rst_done : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_0_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_0_user_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_0_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_0_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_1_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_1_pll_locked : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_1_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_1_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_rst : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_rst_done : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_1_user_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_1_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_1_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_2_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_2_pll_locked : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_2_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_2_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_rst : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_rst_done : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_2_user_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_2_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_2_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_3_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_3_pll_locked : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_3_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_3_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_rst : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_rst_done : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_3_user_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_ch_3_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_ch_3_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_0_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_0_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_0_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_0_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_0_reset : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_0_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_0_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_1_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_1_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_1_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_1_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_1_reset : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_1_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_1_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_2_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_2_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_2_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_2_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_2_reset : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_2_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_2_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_3_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_3_enb : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_3_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_3_ready : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_3_reset : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_es_3_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_rx_xcvr_up_es_3_wr : STD_LOGIC;
  signal axi_ad9371_rx_xcvr_up_pll_rst : STD_LOGIC;
  signal axi_ad9371_tx_jesd_tx_phy0_txcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ad9371_tx_jesd_tx_phy0_txdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_ad9371_tx_jesd_tx_phy1_txcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ad9371_tx_jesd_tx_phy1_txdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_ad9371_tx_jesd_tx_phy2_txcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ad9371_tx_jesd_tx_phy2_txdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_ad9371_tx_jesd_tx_phy3_txcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal axi_ad9371_tx_jesd_tx_phy3_txdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_enb : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_0_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_0_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_pll_locked : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_0_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_0_rst : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_0_rst_done : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_0_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_tx_diffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_tx_postcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_tx_precursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_user_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_0_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_0_wr : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_enb : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_pll_locked : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_rst : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_rst_done : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_tx_diffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_tx_postcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_tx_precursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_user_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_1_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_1_wr : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_enb : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_pll_locked : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_rst : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_rst_done : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_tx_diffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_tx_postcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_tx_precursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_user_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_2_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_2_wr : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_enb : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_lpm_dfe_n : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_out_clk_sel : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_pll_locked : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_rst : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_rst_done : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_sys_clk_sel : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_tx_diffctrl : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_tx_postcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_tx_precursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_user_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_ch_3_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_ch_3_wr : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_cm_0_addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal axi_ad9371_tx_xcvr_up_cm_0_enb : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_cm_0_rdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_cm_0_ready : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_cm_0_wdata : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal axi_ad9371_tx_xcvr_up_cm_0_wr : STD_LOGIC;
  signal axi_ad9371_tx_xcvr_up_pll_rst : STD_LOGIC;
  signal clk_0_1 : STD_LOGIC;
  signal clk_0_2 : STD_LOGIC;
  signal dac_data_0_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_data_1_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_data_2_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_data_3_0_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal dac_dunf_0_1 : STD_LOGIC;
  signal rx_ad9371_tpl_core_adc_data_0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_ad9371_tpl_core_adc_data_1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_ad9371_tpl_core_adc_data_2 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_ad9371_tpl_core_adc_data_3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal rx_ad9371_tpl_core_adc_enable_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_ad9371_tpl_core_adc_enable_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_ad9371_tpl_core_adc_enable_2 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_ad9371_tpl_core_adc_enable_3 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_ad9371_tpl_core_adc_valid_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_ad9371_tpl_core_adc_valid_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_ad9371_tpl_core_adc_valid_2 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_ad9371_tpl_core_adc_valid_3 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal rx_data_0_n_1 : STD_LOGIC;
  signal rx_data_0_p_1 : STD_LOGIC;
  signal rx_data_1_n_1 : STD_LOGIC;
  signal rx_data_1_p_1 : STD_LOGIC;
  signal rx_data_2_n_1 : STD_LOGIC;
  signal rx_data_2_p_1 : STD_LOGIC;
  signal rx_data_3_n_1 : STD_LOGIC;
  signal rx_data_3_p_1 : STD_LOGIC;
  signal rx_ref_clk_0_1 : STD_LOGIC;
  signal s_axi_0_1_ARADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_1_ARREADY : STD_LOGIC;
  signal s_axi_0_1_ARVALID : STD_LOGIC;
  signal s_axi_0_1_AWADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_1_AWREADY : STD_LOGIC;
  signal s_axi_0_1_AWVALID : STD_LOGIC;
  signal s_axi_0_1_BREADY : STD_LOGIC;
  signal s_axi_0_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_1_BVALID : STD_LOGIC;
  signal s_axi_0_1_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_1_RREADY : STD_LOGIC;
  signal s_axi_0_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_1_RVALID : STD_LOGIC;
  signal s_axi_0_1_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_1_WREADY : STD_LOGIC;
  signal s_axi_0_1_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_1_WVALID : STD_LOGIC;
  signal s_axi_0_2_ARADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_2_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_2_ARREADY : STD_LOGIC;
  signal s_axi_0_2_ARVALID : STD_LOGIC;
  signal s_axi_0_2_AWADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_2_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_2_AWREADY : STD_LOGIC;
  signal s_axi_0_2_AWVALID : STD_LOGIC;
  signal s_axi_0_2_BREADY : STD_LOGIC;
  signal s_axi_0_2_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_2_BVALID : STD_LOGIC;
  signal s_axi_0_2_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_2_RREADY : STD_LOGIC;
  signal s_axi_0_2_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_2_RVALID : STD_LOGIC;
  signal s_axi_0_2_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_2_WREADY : STD_LOGIC;
  signal s_axi_0_2_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_2_WVALID : STD_LOGIC;
  signal s_axi_0_3_ARADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_0_3_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_3_ARREADY : STD_LOGIC;
  signal s_axi_0_3_ARVALID : STD_LOGIC;
  signal s_axi_0_3_AWADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_0_3_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_3_AWREADY : STD_LOGIC;
  signal s_axi_0_3_AWVALID : STD_LOGIC;
  signal s_axi_0_3_BREADY : STD_LOGIC;
  signal s_axi_0_3_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_3_BVALID : STD_LOGIC;
  signal s_axi_0_3_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_3_RREADY : STD_LOGIC;
  signal s_axi_0_3_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_3_RVALID : STD_LOGIC;
  signal s_axi_0_3_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_3_WREADY : STD_LOGIC;
  signal s_axi_0_3_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_3_WVALID : STD_LOGIC;
  signal s_axi_0_4_ARADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_0_4_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_4_ARREADY : STD_LOGIC;
  signal s_axi_0_4_ARVALID : STD_LOGIC;
  signal s_axi_0_4_AWADDR : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal s_axi_0_4_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_4_AWREADY : STD_LOGIC;
  signal s_axi_0_4_AWVALID : STD_LOGIC;
  signal s_axi_0_4_BREADY : STD_LOGIC;
  signal s_axi_0_4_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_4_BVALID : STD_LOGIC;
  signal s_axi_0_4_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_4_RREADY : STD_LOGIC;
  signal s_axi_0_4_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_4_RVALID : STD_LOGIC;
  signal s_axi_0_4_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_4_WREADY : STD_LOGIC;
  signal s_axi_0_4_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_4_WVALID : STD_LOGIC;
  signal s_axi_0_5_ARADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_0_5_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_5_ARREADY : STD_LOGIC;
  signal s_axi_0_5_ARVALID : STD_LOGIC;
  signal s_axi_0_5_AWADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_0_5_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_5_AWREADY : STD_LOGIC;
  signal s_axi_0_5_AWVALID : STD_LOGIC;
  signal s_axi_0_5_BREADY : STD_LOGIC;
  signal s_axi_0_5_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_5_BVALID : STD_LOGIC;
  signal s_axi_0_5_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_5_RREADY : STD_LOGIC;
  signal s_axi_0_5_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_5_RVALID : STD_LOGIC;
  signal s_axi_0_5_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_5_WREADY : STD_LOGIC;
  signal s_axi_0_5_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_5_WVALID : STD_LOGIC;
  signal s_axi_0_6_ARADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_0_6_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_6_ARREADY : STD_LOGIC;
  signal s_axi_0_6_ARVALID : STD_LOGIC;
  signal s_axi_0_6_AWADDR : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal s_axi_0_6_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_6_AWREADY : STD_LOGIC;
  signal s_axi_0_6_AWVALID : STD_LOGIC;
  signal s_axi_0_6_BREADY : STD_LOGIC;
  signal s_axi_0_6_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_6_BVALID : STD_LOGIC;
  signal s_axi_0_6_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_6_RREADY : STD_LOGIC;
  signal s_axi_0_6_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_6_RVALID : STD_LOGIC;
  signal s_axi_0_6_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_6_WREADY : STD_LOGIC;
  signal s_axi_0_6_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_6_WVALID : STD_LOGIC;
  signal s_axi_0_7_ARADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_7_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_7_ARREADY : STD_LOGIC;
  signal s_axi_0_7_ARVALID : STD_LOGIC;
  signal s_axi_0_7_AWADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_7_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_7_AWREADY : STD_LOGIC;
  signal s_axi_0_7_AWVALID : STD_LOGIC;
  signal s_axi_0_7_BREADY : STD_LOGIC;
  signal s_axi_0_7_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_7_BVALID : STD_LOGIC;
  signal s_axi_0_7_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_7_RREADY : STD_LOGIC;
  signal s_axi_0_7_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_7_RVALID : STD_LOGIC;
  signal s_axi_0_7_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_7_WREADY : STD_LOGIC;
  signal s_axi_0_7_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_7_WVALID : STD_LOGIC;
  signal s_axi_0_8_ARADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_8_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_8_ARREADY : STD_LOGIC;
  signal s_axi_0_8_ARVALID : STD_LOGIC;
  signal s_axi_0_8_AWADDR : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axi_0_8_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal s_axi_0_8_AWREADY : STD_LOGIC;
  signal s_axi_0_8_AWVALID : STD_LOGIC;
  signal s_axi_0_8_BREADY : STD_LOGIC;
  signal s_axi_0_8_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_8_BVALID : STD_LOGIC;
  signal s_axi_0_8_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_8_RREADY : STD_LOGIC;
  signal s_axi_0_8_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal s_axi_0_8_RVALID : STD_LOGIC;
  signal s_axi_0_8_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal s_axi_0_8_WREADY : STD_LOGIC;
  signal s_axi_0_8_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axi_0_8_WVALID : STD_LOGIC;
  signal s_axi_aclk_0_1 : STD_LOGIC;
  signal s_axi_aresetn_0_1 : STD_LOGIC;
  signal sync_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sysref_1 : STD_LOGIC;
  signal sysref_2 : STD_LOGIC;
  signal tx_ad9371_tpl_core_dac_enable_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_ad9371_tpl_core_dac_enable_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_ad9371_tpl_core_dac_enable_2 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_ad9371_tpl_core_dac_enable_3 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_ad9371_tpl_core_dac_valid_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_ad9371_tpl_core_dac_valid_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_ad9371_tpl_core_dac_valid_2 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_ad9371_tpl_core_dac_valid_3 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal tx_data_1_TDATA : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal tx_data_1_TREADY : STD_LOGIC;
  signal tx_data_1_TVALID : STD_LOGIC;
  signal tx_ref_clk_0_1 : STD_LOGIC;
  signal util_ad9371_xcvr_rx_0_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_0_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal util_ad9371_xcvr_rx_0_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_0_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_1_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_1_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal util_ad9371_xcvr_rx_1_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_1_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_2_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_2_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal util_ad9371_xcvr_rx_2_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_2_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_3_rxcharisk : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_3_rxdata : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal util_ad9371_xcvr_rx_3_rxdisperr : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_3_rxnotintable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal util_ad9371_xcvr_rx_out_clk_0 : STD_LOGIC;
  signal util_ad9371_xcvr_tx_0_n : STD_LOGIC;
  signal util_ad9371_xcvr_tx_0_p : STD_LOGIC;
  signal util_ad9371_xcvr_tx_1_n : STD_LOGIC;
  signal util_ad9371_xcvr_tx_1_p : STD_LOGIC;
  signal util_ad9371_xcvr_tx_2_n : STD_LOGIC;
  signal util_ad9371_xcvr_tx_2_p : STD_LOGIC;
  signal util_ad9371_xcvr_tx_3_n : STD_LOGIC;
  signal util_ad9371_xcvr_tx_3_p : STD_LOGIC;
  signal util_ad9371_xcvr_tx_out_clk_0 : STD_LOGIC;
  signal NLW_axi_ad9371_rx_jesd_irq_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_rx_jesd_rx_eof_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_m_axi_arvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_rx_xcvr_m_axi_awvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_rx_xcvr_m_axi_bready_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_rx_xcvr_m_axi_rready_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_rx_xcvr_m_axi_wvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_rx_xcvr_up_status_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_rx_xcvr_m_axi_araddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_m_axi_arprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_m_axi_awaddr_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_m_axi_awprot_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_m_axi_wdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_m_axi_wstrb_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_0_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_1_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_2_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_3_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_0_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_1_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_2_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_3_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_0_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_1_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_2_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_3_UNCONNECTED : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal NLW_axi_ad9371_tx_jesd_irq_UNCONNECTED : STD_LOGIC;
  signal NLW_axi_ad9371_tx_xcvr_up_status_UNCONNECTED : STD_LOGIC;
  signal NLW_util_ad9371_xcvr_rx_out_clk_1_UNCONNECTED : STD_LOGIC;
  signal NLW_util_ad9371_xcvr_rx_out_clk_2_UNCONNECTED : STD_LOGIC;
  signal NLW_util_ad9371_xcvr_rx_out_clk_3_UNCONNECTED : STD_LOGIC;
  signal NLW_util_ad9371_xcvr_tx_out_clk_1_UNCONNECTED : STD_LOGIC;
  signal NLW_util_ad9371_xcvr_tx_out_clk_2_UNCONNECTED : STD_LOGIC;
  signal NLW_util_ad9371_xcvr_tx_out_clk_3_UNCONNECTED : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of rx_clkgen_ref : signal is "xilinx.com:signal:clock:1.0 CLK.RX_CLKGEN_REF CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of rx_clkgen_ref : signal is "XIL_INTERFACENAME CLK.RX_CLKGEN_REF, CLK_DOMAIN system_clk_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute x_interface_info of s_axi_aclk : signal is "xilinx.com:signal:clock:1.0 CLK.S_AXI_ACLK CLK";
  attribute x_interface_parameter of s_axi_aclk : signal is "XIL_INTERFACENAME CLK.S_AXI_ACLK, ASSOCIATED_BUSIF s_axi_rx_adxcvr:s_axi_tx_adxcvr:s_axi_tx_tpl:s_axi_rx_tpl:s_axi_tx_jesd:s_axi_rx_jesd:s_axi_tx_clkgen:s_axi_rx_clkgen, ASSOCIATED_RESET s_axi_aresetn, CLK_DOMAIN system_s_axi_aclk_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute x_interface_info of s_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 RST.S_AXI_ARESETN RST";
  attribute x_interface_parameter of s_axi_aresetn : signal is "XIL_INTERFACENAME RST.S_AXI_ARESETN, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute x_interface_info of s_axi_rx_adxcvr_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr ARREADY";
  attribute x_interface_info of s_axi_rx_adxcvr_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr ARVALID";
  attribute x_interface_info of s_axi_rx_adxcvr_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr AWREADY";
  attribute x_interface_info of s_axi_rx_adxcvr_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr AWVALID";
  attribute x_interface_info of s_axi_rx_adxcvr_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr BREADY";
  attribute x_interface_info of s_axi_rx_adxcvr_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr BVALID";
  attribute x_interface_info of s_axi_rx_adxcvr_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr RREADY";
  attribute x_interface_info of s_axi_rx_adxcvr_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr RVALID";
  attribute x_interface_info of s_axi_rx_adxcvr_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr WREADY";
  attribute x_interface_info of s_axi_rx_adxcvr_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr WVALID";
  attribute x_interface_info of s_axi_rx_clkgen_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen ARREADY";
  attribute x_interface_info of s_axi_rx_clkgen_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen ARVALID";
  attribute x_interface_info of s_axi_rx_clkgen_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen AWREADY";
  attribute x_interface_info of s_axi_rx_clkgen_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen AWVALID";
  attribute x_interface_info of s_axi_rx_clkgen_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen BREADY";
  attribute x_interface_info of s_axi_rx_clkgen_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen BVALID";
  attribute x_interface_info of s_axi_rx_clkgen_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen RREADY";
  attribute x_interface_info of s_axi_rx_clkgen_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen RVALID";
  attribute x_interface_info of s_axi_rx_clkgen_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen WREADY";
  attribute x_interface_info of s_axi_rx_clkgen_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen WVALID";
  attribute x_interface_info of s_axi_rx_jesd_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd ARREADY";
  attribute x_interface_info of s_axi_rx_jesd_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd ARVALID";
  attribute x_interface_info of s_axi_rx_jesd_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd AWREADY";
  attribute x_interface_info of s_axi_rx_jesd_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd AWVALID";
  attribute x_interface_info of s_axi_rx_jesd_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd BREADY";
  attribute x_interface_info of s_axi_rx_jesd_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd BVALID";
  attribute x_interface_info of s_axi_rx_jesd_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd RREADY";
  attribute x_interface_info of s_axi_rx_jesd_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd RVALID";
  attribute x_interface_info of s_axi_rx_jesd_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd WREADY";
  attribute x_interface_info of s_axi_rx_jesd_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd WVALID";
  attribute x_interface_info of s_axi_rx_tpl_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl ARREADY";
  attribute x_interface_info of s_axi_rx_tpl_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl ARVALID";
  attribute x_interface_info of s_axi_rx_tpl_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl AWREADY";
  attribute x_interface_info of s_axi_rx_tpl_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl AWVALID";
  attribute x_interface_info of s_axi_rx_tpl_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl BREADY";
  attribute x_interface_info of s_axi_rx_tpl_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl BVALID";
  attribute x_interface_info of s_axi_rx_tpl_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl RREADY";
  attribute x_interface_info of s_axi_rx_tpl_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl RVALID";
  attribute x_interface_info of s_axi_rx_tpl_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl WREADY";
  attribute x_interface_info of s_axi_rx_tpl_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl WVALID";
  attribute x_interface_info of s_axi_tx_adxcvr_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr ARREADY";
  attribute x_interface_info of s_axi_tx_adxcvr_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr ARVALID";
  attribute x_interface_info of s_axi_tx_adxcvr_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr AWREADY";
  attribute x_interface_info of s_axi_tx_adxcvr_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr AWVALID";
  attribute x_interface_info of s_axi_tx_adxcvr_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr BREADY";
  attribute x_interface_info of s_axi_tx_adxcvr_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr BVALID";
  attribute x_interface_info of s_axi_tx_adxcvr_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr RREADY";
  attribute x_interface_info of s_axi_tx_adxcvr_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr RVALID";
  attribute x_interface_info of s_axi_tx_adxcvr_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr WREADY";
  attribute x_interface_info of s_axi_tx_adxcvr_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr WVALID";
  attribute x_interface_info of s_axi_tx_clkgen_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen ARREADY";
  attribute x_interface_info of s_axi_tx_clkgen_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen ARVALID";
  attribute x_interface_info of s_axi_tx_clkgen_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen AWREADY";
  attribute x_interface_info of s_axi_tx_clkgen_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen AWVALID";
  attribute x_interface_info of s_axi_tx_clkgen_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen BREADY";
  attribute x_interface_info of s_axi_tx_clkgen_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen BVALID";
  attribute x_interface_info of s_axi_tx_clkgen_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen RREADY";
  attribute x_interface_info of s_axi_tx_clkgen_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen RVALID";
  attribute x_interface_info of s_axi_tx_clkgen_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen WREADY";
  attribute x_interface_info of s_axi_tx_clkgen_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen WVALID";
  attribute x_interface_info of s_axi_tx_jesd_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd ARREADY";
  attribute x_interface_info of s_axi_tx_jesd_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd ARVALID";
  attribute x_interface_info of s_axi_tx_jesd_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd AWREADY";
  attribute x_interface_info of s_axi_tx_jesd_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd AWVALID";
  attribute x_interface_info of s_axi_tx_jesd_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd BREADY";
  attribute x_interface_info of s_axi_tx_jesd_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd BVALID";
  attribute x_interface_info of s_axi_tx_jesd_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd RREADY";
  attribute x_interface_info of s_axi_tx_jesd_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd RVALID";
  attribute x_interface_info of s_axi_tx_jesd_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd WREADY";
  attribute x_interface_info of s_axi_tx_jesd_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd WVALID";
  attribute x_interface_info of s_axi_tx_tpl_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl ARREADY";
  attribute x_interface_info of s_axi_tx_tpl_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl ARVALID";
  attribute x_interface_info of s_axi_tx_tpl_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl AWREADY";
  attribute x_interface_info of s_axi_tx_tpl_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl AWVALID";
  attribute x_interface_info of s_axi_tx_tpl_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl BREADY";
  attribute x_interface_info of s_axi_tx_tpl_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl BVALID";
  attribute x_interface_info of s_axi_tx_tpl_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl RREADY";
  attribute x_interface_info of s_axi_tx_tpl_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl RVALID";
  attribute x_interface_info of s_axi_tx_tpl_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl WREADY";
  attribute x_interface_info of s_axi_tx_tpl_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl WVALID";
  attribute x_interface_info of tx_clkgen_ref : signal is "xilinx.com:signal:clock:1.0 CLK.TX_CLKGEN_REF CLK";
  attribute x_interface_parameter of tx_clkgen_ref : signal is "XIL_INTERFACENAME CLK.TX_CLKGEN_REF, CLK_DOMAIN system_clk_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute x_interface_info of s_axi_rx_adxcvr_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr ARADDR";
  attribute x_interface_parameter of s_axi_rx_adxcvr_araddr : signal is "XIL_INTERFACENAME s_axi_rx_adxcvr, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_rx_adxcvr_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr ARPROT";
  attribute x_interface_info of s_axi_rx_adxcvr_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr AWADDR";
  attribute x_interface_info of s_axi_rx_adxcvr_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr AWPROT";
  attribute x_interface_info of s_axi_rx_adxcvr_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr BRESP";
  attribute x_interface_info of s_axi_rx_adxcvr_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr RDATA";
  attribute x_interface_info of s_axi_rx_adxcvr_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr RRESP";
  attribute x_interface_info of s_axi_rx_adxcvr_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr WDATA";
  attribute x_interface_info of s_axi_rx_adxcvr_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_adxcvr WSTRB";
  attribute x_interface_info of s_axi_rx_clkgen_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen ARADDR";
  attribute x_interface_parameter of s_axi_rx_clkgen_araddr : signal is "XIL_INTERFACENAME s_axi_rx_clkgen, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_rx_clkgen_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen ARPROT";
  attribute x_interface_info of s_axi_rx_clkgen_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen AWADDR";
  attribute x_interface_info of s_axi_rx_clkgen_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen AWPROT";
  attribute x_interface_info of s_axi_rx_clkgen_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen BRESP";
  attribute x_interface_info of s_axi_rx_clkgen_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen RDATA";
  attribute x_interface_info of s_axi_rx_clkgen_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen RRESP";
  attribute x_interface_info of s_axi_rx_clkgen_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen WDATA";
  attribute x_interface_info of s_axi_rx_clkgen_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_clkgen WSTRB";
  attribute x_interface_info of s_axi_rx_jesd_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd ARADDR";
  attribute x_interface_parameter of s_axi_rx_jesd_araddr : signal is "XIL_INTERFACENAME s_axi_rx_jesd, ADDR_WIDTH 14, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_rx_jesd_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd ARPROT";
  attribute x_interface_info of s_axi_rx_jesd_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd AWADDR";
  attribute x_interface_info of s_axi_rx_jesd_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd AWPROT";
  attribute x_interface_info of s_axi_rx_jesd_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd BRESP";
  attribute x_interface_info of s_axi_rx_jesd_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd RDATA";
  attribute x_interface_info of s_axi_rx_jesd_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd RRESP";
  attribute x_interface_info of s_axi_rx_jesd_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd WDATA";
  attribute x_interface_info of s_axi_rx_jesd_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_jesd WSTRB";
  attribute x_interface_info of s_axi_rx_tpl_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl ARADDR";
  attribute x_interface_parameter of s_axi_rx_tpl_araddr : signal is "XIL_INTERFACENAME s_axi_rx_tpl, ADDR_WIDTH 13, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_rx_tpl_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl ARPROT";
  attribute x_interface_info of s_axi_rx_tpl_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl AWADDR";
  attribute x_interface_info of s_axi_rx_tpl_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl AWPROT";
  attribute x_interface_info of s_axi_rx_tpl_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl BRESP";
  attribute x_interface_info of s_axi_rx_tpl_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl RDATA";
  attribute x_interface_info of s_axi_rx_tpl_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl RRESP";
  attribute x_interface_info of s_axi_rx_tpl_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl WDATA";
  attribute x_interface_info of s_axi_rx_tpl_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_rx_tpl WSTRB";
  attribute x_interface_info of s_axi_tx_adxcvr_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr ARADDR";
  attribute x_interface_parameter of s_axi_tx_adxcvr_araddr : signal is "XIL_INTERFACENAME s_axi_tx_adxcvr, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_tx_adxcvr_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr ARPROT";
  attribute x_interface_info of s_axi_tx_adxcvr_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr AWADDR";
  attribute x_interface_info of s_axi_tx_adxcvr_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr AWPROT";
  attribute x_interface_info of s_axi_tx_adxcvr_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr BRESP";
  attribute x_interface_info of s_axi_tx_adxcvr_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr RDATA";
  attribute x_interface_info of s_axi_tx_adxcvr_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr RRESP";
  attribute x_interface_info of s_axi_tx_adxcvr_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr WDATA";
  attribute x_interface_info of s_axi_tx_adxcvr_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_adxcvr WSTRB";
  attribute x_interface_info of s_axi_tx_clkgen_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen ARADDR";
  attribute x_interface_parameter of s_axi_tx_clkgen_araddr : signal is "XIL_INTERFACENAME s_axi_tx_clkgen, ADDR_WIDTH 16, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 2, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 2, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_tx_clkgen_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen ARPROT";
  attribute x_interface_info of s_axi_tx_clkgen_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen AWADDR";
  attribute x_interface_info of s_axi_tx_clkgen_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen AWPROT";
  attribute x_interface_info of s_axi_tx_clkgen_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen BRESP";
  attribute x_interface_info of s_axi_tx_clkgen_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen RDATA";
  attribute x_interface_info of s_axi_tx_clkgen_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen RRESP";
  attribute x_interface_info of s_axi_tx_clkgen_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen WDATA";
  attribute x_interface_info of s_axi_tx_clkgen_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_clkgen WSTRB";
  attribute x_interface_info of s_axi_tx_jesd_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd ARADDR";
  attribute x_interface_parameter of s_axi_tx_jesd_araddr : signal is "XIL_INTERFACENAME s_axi_tx_jesd, ADDR_WIDTH 14, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_tx_jesd_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd ARPROT";
  attribute x_interface_info of s_axi_tx_jesd_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd AWADDR";
  attribute x_interface_info of s_axi_tx_jesd_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd AWPROT";
  attribute x_interface_info of s_axi_tx_jesd_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd BRESP";
  attribute x_interface_info of s_axi_tx_jesd_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd RDATA";
  attribute x_interface_info of s_axi_tx_jesd_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd RRESP";
  attribute x_interface_info of s_axi_tx_jesd_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd WDATA";
  attribute x_interface_info of s_axi_tx_jesd_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_jesd WSTRB";
  attribute x_interface_info of s_axi_tx_tpl_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl ARADDR";
  attribute x_interface_parameter of s_axi_tx_tpl_araddr : signal is "XIL_INTERFACENAME s_axi_tx_tpl, ADDR_WIDTH 13, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN system_s_axi_aclk_0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 0, HAS_CACHE 0, HAS_LOCK 0, HAS_PROT 1, HAS_QOS 0, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 0, INSERT_VIP 0, MAX_BURST_LENGTH 1, NUM_READ_OUTSTANDING 1, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 1, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute x_interface_info of s_axi_tx_tpl_arprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl ARPROT";
  attribute x_interface_info of s_axi_tx_tpl_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl AWADDR";
  attribute x_interface_info of s_axi_tx_tpl_awprot : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl AWPROT";
  attribute x_interface_info of s_axi_tx_tpl_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl BRESP";
  attribute x_interface_info of s_axi_tx_tpl_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl RDATA";
  attribute x_interface_info of s_axi_tx_tpl_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl RRESP";
  attribute x_interface_info of s_axi_tx_tpl_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl WDATA";
  attribute x_interface_info of s_axi_tx_tpl_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi_tx_tpl WSTRB";
begin
  adc_data_0(31 downto 0) <= rx_ad9371_tpl_core_adc_data_0(31 downto 0);
  adc_data_1(31 downto 0) <= rx_ad9371_tpl_core_adc_data_1(31 downto 0);
  adc_data_2(31 downto 0) <= rx_ad9371_tpl_core_adc_data_2(31 downto 0);
  adc_data_3(31 downto 0) <= rx_ad9371_tpl_core_adc_data_3(31 downto 0);
  adc_dovf_0_1 <= adc_dovf;
  adc_enable_0(0) <= rx_ad9371_tpl_core_adc_enable_0(0);
  adc_enable_1(0) <= rx_ad9371_tpl_core_adc_enable_1(0);
  adc_enable_2(0) <= rx_ad9371_tpl_core_adc_enable_2(0);
  adc_enable_3(0) <= rx_ad9371_tpl_core_adc_enable_3(0);
  adc_valid_0(0) <= rx_ad9371_tpl_core_adc_valid_0(0);
  adc_valid_1(0) <= rx_ad9371_tpl_core_adc_valid_1(0);
  adc_valid_2(0) <= rx_ad9371_tpl_core_adc_valid_2(0);
  adc_valid_3(0) <= rx_ad9371_tpl_core_adc_valid_3(0);
  clk_0_1 <= rx_clkgen_ref;
  clk_0_2 <= tx_clkgen_ref;
  dac_data_0_0_1(31 downto 0) <= dac_data_0(31 downto 0);
  dac_data_1_0_1(31 downto 0) <= dac_data_1(31 downto 0);
  dac_data_2_0_1(31 downto 0) <= dac_data_2(31 downto 0);
  dac_data_3_0_1(31 downto 0) <= dac_data_3(31 downto 0);
  dac_dunf_0_1 <= dac_dunf;
  dac_enable_0(0) <= tx_ad9371_tpl_core_dac_enable_0(0);
  dac_enable_1(0) <= tx_ad9371_tpl_core_dac_enable_1(0);
  dac_enable_2(0) <= tx_ad9371_tpl_core_dac_enable_2(0);
  dac_enable_3(0) <= tx_ad9371_tpl_core_dac_enable_3(0);
  dac_valid_0(0) <= tx_ad9371_tpl_core_dac_valid_0(0);
  dac_valid_1(0) <= tx_ad9371_tpl_core_dac_valid_1(0);
  dac_valid_2(0) <= tx_ad9371_tpl_core_dac_valid_2(0);
  dac_valid_3(0) <= tx_ad9371_tpl_core_dac_valid_3(0);
  rx_data_0_n_1 <= rx_data_0_n;
  rx_data_0_p_1 <= rx_data_0_p;
  rx_data_1_n_1 <= rx_data_1_n;
  rx_data_1_p_1 <= rx_data_1_p;
  rx_data_2_n_1 <= rx_data_2_n;
  rx_data_2_p_1 <= rx_data_2_p;
  rx_data_3_n_1 <= rx_data_3_n;
  rx_data_3_p_1 <= rx_data_3_p;
  rx_linkclk <= ad9371_rx_device_clk;
  rx_ref_clk_0_1 <= rx_ref_clk_0;
  rx_sync(0) <= axi_ad9371_rx_jesd_sync(0);
  rxoutclk <= util_ad9371_xcvr_rx_out_clk_0;
  s_axi_0_1_ARADDR(15 downto 0) <= s_axi_rx_adxcvr_araddr(15 downto 0);
  s_axi_0_1_ARPROT(2 downto 0) <= s_axi_rx_adxcvr_arprot(2 downto 0);
  s_axi_0_1_ARVALID <= s_axi_rx_adxcvr_arvalid;
  s_axi_0_1_AWADDR(15 downto 0) <= s_axi_rx_adxcvr_awaddr(15 downto 0);
  s_axi_0_1_AWPROT(2 downto 0) <= s_axi_rx_adxcvr_awprot(2 downto 0);
  s_axi_0_1_AWVALID <= s_axi_rx_adxcvr_awvalid;
  s_axi_0_1_BREADY <= s_axi_rx_adxcvr_bready;
  s_axi_0_1_RREADY <= s_axi_rx_adxcvr_rready;
  s_axi_0_1_WDATA(31 downto 0) <= s_axi_rx_adxcvr_wdata(31 downto 0);
  s_axi_0_1_WSTRB(3 downto 0) <= s_axi_rx_adxcvr_wstrb(3 downto 0);
  s_axi_0_1_WVALID <= s_axi_rx_adxcvr_wvalid;
  s_axi_0_2_ARADDR(15 downto 0) <= s_axi_tx_adxcvr_araddr(15 downto 0);
  s_axi_0_2_ARPROT(2 downto 0) <= s_axi_tx_adxcvr_arprot(2 downto 0);
  s_axi_0_2_ARVALID <= s_axi_tx_adxcvr_arvalid;
  s_axi_0_2_AWADDR(15 downto 0) <= s_axi_tx_adxcvr_awaddr(15 downto 0);
  s_axi_0_2_AWPROT(2 downto 0) <= s_axi_tx_adxcvr_awprot(2 downto 0);
  s_axi_0_2_AWVALID <= s_axi_tx_adxcvr_awvalid;
  s_axi_0_2_BREADY <= s_axi_tx_adxcvr_bready;
  s_axi_0_2_RREADY <= s_axi_tx_adxcvr_rready;
  s_axi_0_2_WDATA(31 downto 0) <= s_axi_tx_adxcvr_wdata(31 downto 0);
  s_axi_0_2_WSTRB(3 downto 0) <= s_axi_tx_adxcvr_wstrb(3 downto 0);
  s_axi_0_2_WVALID <= s_axi_tx_adxcvr_wvalid;
  s_axi_0_3_ARADDR(12 downto 0) <= s_axi_tx_tpl_araddr(12 downto 0);
  s_axi_0_3_ARPROT(2 downto 0) <= s_axi_tx_tpl_arprot(2 downto 0);
  s_axi_0_3_ARVALID <= s_axi_tx_tpl_arvalid;
  s_axi_0_3_AWADDR(12 downto 0) <= s_axi_tx_tpl_awaddr(12 downto 0);
  s_axi_0_3_AWPROT(2 downto 0) <= s_axi_tx_tpl_awprot(2 downto 0);
  s_axi_0_3_AWVALID <= s_axi_tx_tpl_awvalid;
  s_axi_0_3_BREADY <= s_axi_tx_tpl_bready;
  s_axi_0_3_RREADY <= s_axi_tx_tpl_rready;
  s_axi_0_3_WDATA(31 downto 0) <= s_axi_tx_tpl_wdata(31 downto 0);
  s_axi_0_3_WSTRB(3 downto 0) <= s_axi_tx_tpl_wstrb(3 downto 0);
  s_axi_0_3_WVALID <= s_axi_tx_tpl_wvalid;
  s_axi_0_4_ARADDR(12 downto 0) <= s_axi_rx_tpl_araddr(12 downto 0);
  s_axi_0_4_ARPROT(2 downto 0) <= s_axi_rx_tpl_arprot(2 downto 0);
  s_axi_0_4_ARVALID <= s_axi_rx_tpl_arvalid;
  s_axi_0_4_AWADDR(12 downto 0) <= s_axi_rx_tpl_awaddr(12 downto 0);
  s_axi_0_4_AWPROT(2 downto 0) <= s_axi_rx_tpl_awprot(2 downto 0);
  s_axi_0_4_AWVALID <= s_axi_rx_tpl_awvalid;
  s_axi_0_4_BREADY <= s_axi_rx_tpl_bready;
  s_axi_0_4_RREADY <= s_axi_rx_tpl_rready;
  s_axi_0_4_WDATA(31 downto 0) <= s_axi_rx_tpl_wdata(31 downto 0);
  s_axi_0_4_WSTRB(3 downto 0) <= s_axi_rx_tpl_wstrb(3 downto 0);
  s_axi_0_4_WVALID <= s_axi_rx_tpl_wvalid;
  s_axi_0_5_ARADDR(13 downto 0) <= s_axi_tx_jesd_araddr(13 downto 0);
  s_axi_0_5_ARPROT(2 downto 0) <= s_axi_tx_jesd_arprot(2 downto 0);
  s_axi_0_5_ARVALID <= s_axi_tx_jesd_arvalid;
  s_axi_0_5_AWADDR(13 downto 0) <= s_axi_tx_jesd_awaddr(13 downto 0);
  s_axi_0_5_AWPROT(2 downto 0) <= s_axi_tx_jesd_awprot(2 downto 0);
  s_axi_0_5_AWVALID <= s_axi_tx_jesd_awvalid;
  s_axi_0_5_BREADY <= s_axi_tx_jesd_bready;
  s_axi_0_5_RREADY <= s_axi_tx_jesd_rready;
  s_axi_0_5_WDATA(31 downto 0) <= s_axi_tx_jesd_wdata(31 downto 0);
  s_axi_0_5_WSTRB(3 downto 0) <= s_axi_tx_jesd_wstrb(3 downto 0);
  s_axi_0_5_WVALID <= s_axi_tx_jesd_wvalid;
  s_axi_0_6_ARADDR(13 downto 0) <= s_axi_rx_jesd_araddr(13 downto 0);
  s_axi_0_6_ARPROT(2 downto 0) <= s_axi_rx_jesd_arprot(2 downto 0);
  s_axi_0_6_ARVALID <= s_axi_rx_jesd_arvalid;
  s_axi_0_6_AWADDR(13 downto 0) <= s_axi_rx_jesd_awaddr(13 downto 0);
  s_axi_0_6_AWPROT(2 downto 0) <= s_axi_rx_jesd_awprot(2 downto 0);
  s_axi_0_6_AWVALID <= s_axi_rx_jesd_awvalid;
  s_axi_0_6_BREADY <= s_axi_rx_jesd_bready;
  s_axi_0_6_RREADY <= s_axi_rx_jesd_rready;
  s_axi_0_6_WDATA(31 downto 0) <= s_axi_rx_jesd_wdata(31 downto 0);
  s_axi_0_6_WSTRB(3 downto 0) <= s_axi_rx_jesd_wstrb(3 downto 0);
  s_axi_0_6_WVALID <= s_axi_rx_jesd_wvalid;
  s_axi_0_7_ARADDR(15 downto 0) <= s_axi_tx_clkgen_araddr(15 downto 0);
  s_axi_0_7_ARPROT(2 downto 0) <= s_axi_tx_clkgen_arprot(2 downto 0);
  s_axi_0_7_ARVALID <= s_axi_tx_clkgen_arvalid;
  s_axi_0_7_AWADDR(15 downto 0) <= s_axi_tx_clkgen_awaddr(15 downto 0);
  s_axi_0_7_AWPROT(2 downto 0) <= s_axi_tx_clkgen_awprot(2 downto 0);
  s_axi_0_7_AWVALID <= s_axi_tx_clkgen_awvalid;
  s_axi_0_7_BREADY <= s_axi_tx_clkgen_bready;
  s_axi_0_7_RREADY <= s_axi_tx_clkgen_rready;
  s_axi_0_7_WDATA(31 downto 0) <= s_axi_tx_clkgen_wdata(31 downto 0);
  s_axi_0_7_WSTRB(3 downto 0) <= s_axi_tx_clkgen_wstrb(3 downto 0);
  s_axi_0_7_WVALID <= s_axi_tx_clkgen_wvalid;
  s_axi_0_8_ARADDR(15 downto 0) <= s_axi_rx_clkgen_araddr(15 downto 0);
  s_axi_0_8_ARPROT(2 downto 0) <= s_axi_rx_clkgen_arprot(2 downto 0);
  s_axi_0_8_ARVALID <= s_axi_rx_clkgen_arvalid;
  s_axi_0_8_AWADDR(15 downto 0) <= s_axi_rx_clkgen_awaddr(15 downto 0);
  s_axi_0_8_AWPROT(2 downto 0) <= s_axi_rx_clkgen_awprot(2 downto 0);
  s_axi_0_8_AWVALID <= s_axi_rx_clkgen_awvalid;
  s_axi_0_8_BREADY <= s_axi_rx_clkgen_bready;
  s_axi_0_8_RREADY <= s_axi_rx_clkgen_rready;
  s_axi_0_8_WDATA(31 downto 0) <= s_axi_rx_clkgen_wdata(31 downto 0);
  s_axi_0_8_WSTRB(3 downto 0) <= s_axi_rx_clkgen_wstrb(3 downto 0);
  s_axi_0_8_WVALID <= s_axi_rx_clkgen_wvalid;
  s_axi_aclk_0_1 <= s_axi_aclk;
  s_axi_aresetn_0_1 <= s_axi_aresetn;
  s_axi_rx_adxcvr_arready <= s_axi_0_1_ARREADY;
  s_axi_rx_adxcvr_awready <= s_axi_0_1_AWREADY;
  s_axi_rx_adxcvr_bresp(1 downto 0) <= s_axi_0_1_BRESP(1 downto 0);
  s_axi_rx_adxcvr_bvalid <= s_axi_0_1_BVALID;
  s_axi_rx_adxcvr_rdata(31 downto 0) <= s_axi_0_1_RDATA(31 downto 0);
  s_axi_rx_adxcvr_rresp(1 downto 0) <= s_axi_0_1_RRESP(1 downto 0);
  s_axi_rx_adxcvr_rvalid <= s_axi_0_1_RVALID;
  s_axi_rx_adxcvr_wready <= s_axi_0_1_WREADY;
  s_axi_rx_clkgen_arready <= s_axi_0_8_ARREADY;
  s_axi_rx_clkgen_awready <= s_axi_0_8_AWREADY;
  s_axi_rx_clkgen_bresp(1 downto 0) <= s_axi_0_8_BRESP(1 downto 0);
  s_axi_rx_clkgen_bvalid <= s_axi_0_8_BVALID;
  s_axi_rx_clkgen_rdata(31 downto 0) <= s_axi_0_8_RDATA(31 downto 0);
  s_axi_rx_clkgen_rresp(1 downto 0) <= s_axi_0_8_RRESP(1 downto 0);
  s_axi_rx_clkgen_rvalid <= s_axi_0_8_RVALID;
  s_axi_rx_clkgen_wready <= s_axi_0_8_WREADY;
  s_axi_rx_jesd_arready <= s_axi_0_6_ARREADY;
  s_axi_rx_jesd_awready <= s_axi_0_6_AWREADY;
  s_axi_rx_jesd_bresp(1 downto 0) <= s_axi_0_6_BRESP(1 downto 0);
  s_axi_rx_jesd_bvalid <= s_axi_0_6_BVALID;
  s_axi_rx_jesd_rdata(31 downto 0) <= s_axi_0_6_RDATA(31 downto 0);
  s_axi_rx_jesd_rresp(1 downto 0) <= s_axi_0_6_RRESP(1 downto 0);
  s_axi_rx_jesd_rvalid <= s_axi_0_6_RVALID;
  s_axi_rx_jesd_wready <= s_axi_0_6_WREADY;
  s_axi_rx_tpl_arready <= s_axi_0_4_ARREADY;
  s_axi_rx_tpl_awready <= s_axi_0_4_AWREADY;
  s_axi_rx_tpl_bresp(1 downto 0) <= s_axi_0_4_BRESP(1 downto 0);
  s_axi_rx_tpl_bvalid <= s_axi_0_4_BVALID;
  s_axi_rx_tpl_rdata(31 downto 0) <= s_axi_0_4_RDATA(31 downto 0);
  s_axi_rx_tpl_rresp(1 downto 0) <= s_axi_0_4_RRESP(1 downto 0);
  s_axi_rx_tpl_rvalid <= s_axi_0_4_RVALID;
  s_axi_rx_tpl_wready <= s_axi_0_4_WREADY;
  s_axi_tx_adxcvr_arready <= s_axi_0_2_ARREADY;
  s_axi_tx_adxcvr_awready <= s_axi_0_2_AWREADY;
  s_axi_tx_adxcvr_bresp(1 downto 0) <= s_axi_0_2_BRESP(1 downto 0);
  s_axi_tx_adxcvr_bvalid <= s_axi_0_2_BVALID;
  s_axi_tx_adxcvr_rdata(31 downto 0) <= s_axi_0_2_RDATA(31 downto 0);
  s_axi_tx_adxcvr_rresp(1 downto 0) <= s_axi_0_2_RRESP(1 downto 0);
  s_axi_tx_adxcvr_rvalid <= s_axi_0_2_RVALID;
  s_axi_tx_adxcvr_wready <= s_axi_0_2_WREADY;
  s_axi_tx_clkgen_arready <= s_axi_0_7_ARREADY;
  s_axi_tx_clkgen_awready <= s_axi_0_7_AWREADY;
  s_axi_tx_clkgen_bresp(1 downto 0) <= s_axi_0_7_BRESP(1 downto 0);
  s_axi_tx_clkgen_bvalid <= s_axi_0_7_BVALID;
  s_axi_tx_clkgen_rdata(31 downto 0) <= s_axi_0_7_RDATA(31 downto 0);
  s_axi_tx_clkgen_rresp(1 downto 0) <= s_axi_0_7_RRESP(1 downto 0);
  s_axi_tx_clkgen_rvalid <= s_axi_0_7_RVALID;
  s_axi_tx_clkgen_wready <= s_axi_0_7_WREADY;
  s_axi_tx_jesd_arready <= s_axi_0_5_ARREADY;
  s_axi_tx_jesd_awready <= s_axi_0_5_AWREADY;
  s_axi_tx_jesd_bresp(1 downto 0) <= s_axi_0_5_BRESP(1 downto 0);
  s_axi_tx_jesd_bvalid <= s_axi_0_5_BVALID;
  s_axi_tx_jesd_rdata(31 downto 0) <= s_axi_0_5_RDATA(31 downto 0);
  s_axi_tx_jesd_rresp(1 downto 0) <= s_axi_0_5_RRESP(1 downto 0);
  s_axi_tx_jesd_rvalid <= s_axi_0_5_RVALID;
  s_axi_tx_jesd_wready <= s_axi_0_5_WREADY;
  s_axi_tx_tpl_arready <= s_axi_0_3_ARREADY;
  s_axi_tx_tpl_awready <= s_axi_0_3_AWREADY;
  s_axi_tx_tpl_bresp(1 downto 0) <= s_axi_0_3_BRESP(1 downto 0);
  s_axi_tx_tpl_bvalid <= s_axi_0_3_BVALID;
  s_axi_tx_tpl_rdata(31 downto 0) <= s_axi_0_3_RDATA(31 downto 0);
  s_axi_tx_tpl_rresp(1 downto 0) <= s_axi_0_3_RRESP(1 downto 0);
  s_axi_tx_tpl_rvalid <= s_axi_0_3_RVALID;
  s_axi_tx_tpl_wready <= s_axi_0_3_WREADY;
  sync_1(0) <= tx_sync(0);
  sysref_1 <= tx_sysref_0;
  sysref_2 <= rx_sysref_0;
  tx_data_0_n <= util_ad9371_xcvr_tx_0_n;
  tx_data_0_p <= util_ad9371_xcvr_tx_0_p;
  tx_data_1_n <= util_ad9371_xcvr_tx_1_n;
  tx_data_1_p <= util_ad9371_xcvr_tx_1_p;
  tx_data_2_n <= util_ad9371_xcvr_tx_2_n;
  tx_data_2_p <= util_ad9371_xcvr_tx_2_p;
  tx_data_3_n <= util_ad9371_xcvr_tx_3_n;
  tx_data_3_p <= util_ad9371_xcvr_tx_3_p;
  tx_linkclk <= ad9371_tx_device_clk;
  tx_ref_clk_0_1 <= tx_ref_clk_0;
  txoutclk <= util_ad9371_xcvr_tx_out_clk_0;
axi_ad9371_rx_clkgen: component system_axi_ad9371_rx_clkgen_0
     port map (
      clk => clk_0_1,
      clk_0 => ad9371_rx_device_clk,
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(15 downto 0) => s_axi_0_8_ARADDR(15 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_8_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_8_ARREADY,
      s_axi_arvalid => s_axi_0_8_ARVALID,
      s_axi_awaddr(15 downto 0) => s_axi_0_8_AWADDR(15 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_8_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_8_AWREADY,
      s_axi_awvalid => s_axi_0_8_AWVALID,
      s_axi_bready => s_axi_0_8_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_8_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_8_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_8_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_8_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_8_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_8_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_8_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_8_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_8_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_8_WVALID
    );
axi_ad9371_rx_jesd: entity work.axi_ad9371_rx_jesd_imp_GUTUBY
     port map (
      device_clk => ad9371_rx_device_clk,
      irq => NLW_axi_ad9371_rx_jesd_irq_UNCONNECTED,
      phy_en_char_align => axi_ad9371_rx_jesd_phy_en_char_align,
      rx_data_tdata(127 downto 0) => axi_ad9371_rx_jesd_rx_data_tdata(127 downto 0),
      rx_data_tvalid => axi_ad9371_rx_jesd_rx_data_tvalid,
      rx_eof(3 downto 0) => NLW_axi_ad9371_rx_jesd_rx_eof_UNCONNECTED(3 downto 0),
      rx_phy0_rxcharisk(3 downto 0) => util_ad9371_xcvr_rx_0_rxcharisk(3 downto 0),
      rx_phy0_rxdata(31 downto 0) => util_ad9371_xcvr_rx_0_rxdata(31 downto 0),
      rx_phy0_rxdisperr(3 downto 0) => util_ad9371_xcvr_rx_0_rxdisperr(3 downto 0),
      rx_phy0_rxnotintable(3 downto 0) => util_ad9371_xcvr_rx_0_rxnotintable(3 downto 0),
      rx_phy1_rxcharisk(3 downto 0) => util_ad9371_xcvr_rx_1_rxcharisk(3 downto 0),
      rx_phy1_rxdata(31 downto 0) => util_ad9371_xcvr_rx_1_rxdata(31 downto 0),
      rx_phy1_rxdisperr(3 downto 0) => util_ad9371_xcvr_rx_1_rxdisperr(3 downto 0),
      rx_phy1_rxnotintable(3 downto 0) => util_ad9371_xcvr_rx_1_rxnotintable(3 downto 0),
      rx_phy2_rxcharisk(3 downto 0) => util_ad9371_xcvr_rx_2_rxcharisk(3 downto 0),
      rx_phy2_rxdata(31 downto 0) => util_ad9371_xcvr_rx_2_rxdata(31 downto 0),
      rx_phy2_rxdisperr(3 downto 0) => util_ad9371_xcvr_rx_2_rxdisperr(3 downto 0),
      rx_phy2_rxnotintable(3 downto 0) => util_ad9371_xcvr_rx_2_rxnotintable(3 downto 0),
      rx_phy3_rxcharisk(3 downto 0) => util_ad9371_xcvr_rx_3_rxcharisk(3 downto 0),
      rx_phy3_rxdata(31 downto 0) => util_ad9371_xcvr_rx_3_rxdata(31 downto 0),
      rx_phy3_rxdisperr(3 downto 0) => util_ad9371_xcvr_rx_3_rxdisperr(3 downto 0),
      rx_phy3_rxnotintable(3 downto 0) => util_ad9371_xcvr_rx_3_rxnotintable(3 downto 0),
      rx_sof(3 downto 0) => axi_ad9371_rx_jesd_rx_sof(3 downto 0),
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(13 downto 0) => s_axi_0_6_ARADDR(13 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_6_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_6_ARREADY,
      s_axi_arvalid => s_axi_0_6_ARVALID,
      s_axi_awaddr(13 downto 0) => s_axi_0_6_AWADDR(13 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_6_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_6_AWREADY,
      s_axi_awvalid => s_axi_0_6_AWVALID,
      s_axi_bready => s_axi_0_6_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_6_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_6_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_6_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_6_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_6_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_6_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_6_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_6_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_6_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_6_WVALID,
      sync(0) => axi_ad9371_rx_jesd_sync(0),
      sysref => sysref_2
    );
axi_ad9371_rx_xcvr: component system_axi_ad9371_rx_xcvr_0
     port map (
      m_axi_araddr(31 downto 0) => NLW_axi_ad9371_rx_xcvr_m_axi_araddr_UNCONNECTED(31 downto 0),
      m_axi_arprot(2 downto 0) => NLW_axi_ad9371_rx_xcvr_m_axi_arprot_UNCONNECTED(2 downto 0),
      m_axi_arready => '0',
      m_axi_arvalid => NLW_axi_ad9371_rx_xcvr_m_axi_arvalid_UNCONNECTED,
      m_axi_awaddr(31 downto 0) => NLW_axi_ad9371_rx_xcvr_m_axi_awaddr_UNCONNECTED(31 downto 0),
      m_axi_awprot(2 downto 0) => NLW_axi_ad9371_rx_xcvr_m_axi_awprot_UNCONNECTED(2 downto 0),
      m_axi_awready => '0',
      m_axi_awvalid => NLW_axi_ad9371_rx_xcvr_m_axi_awvalid_UNCONNECTED,
      m_axi_bready => NLW_axi_ad9371_rx_xcvr_m_axi_bready_UNCONNECTED,
      m_axi_bresp(1 downto 0) => B"00",
      m_axi_bvalid => '0',
      m_axi_rdata(31 downto 0) => B"00000000000000000000000000000000",
      m_axi_rready => NLW_axi_ad9371_rx_xcvr_m_axi_rready_UNCONNECTED,
      m_axi_rresp(1 downto 0) => B"00",
      m_axi_rvalid => '0',
      m_axi_wdata(31 downto 0) => NLW_axi_ad9371_rx_xcvr_m_axi_wdata_UNCONNECTED(31 downto 0),
      m_axi_wready => '0',
      m_axi_wstrb(3 downto 0) => NLW_axi_ad9371_rx_xcvr_m_axi_wstrb_UNCONNECTED(3 downto 0),
      m_axi_wvalid => NLW_axi_ad9371_rx_xcvr_m_axi_wvalid_UNCONNECTED,
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(15 downto 0) => s_axi_0_1_ARADDR(15 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_1_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_1_ARREADY,
      s_axi_arvalid => s_axi_0_1_ARVALID,
      s_axi_awaddr(15 downto 0) => s_axi_0_1_AWADDR(15 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_1_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_1_AWREADY,
      s_axi_awvalid => s_axi_0_1_AWVALID,
      s_axi_bready => s_axi_0_1_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_1_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_1_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_1_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_1_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_1_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_1_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_1_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_1_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_1_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_1_WVALID,
      up_ch_addr_0(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_addr(11 downto 0),
      up_ch_addr_1(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_addr(11 downto 0),
      up_ch_addr_2(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_addr(11 downto 0),
      up_ch_addr_3(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_addr(11 downto 0),
      up_ch_enb_0 => axi_ad9371_rx_xcvr_up_ch_0_enb,
      up_ch_enb_1 => axi_ad9371_rx_xcvr_up_ch_1_enb,
      up_ch_enb_2 => axi_ad9371_rx_xcvr_up_ch_2_enb,
      up_ch_enb_3 => axi_ad9371_rx_xcvr_up_ch_3_enb,
      up_ch_lpm_dfe_n_0 => axi_ad9371_rx_xcvr_up_ch_0_lpm_dfe_n,
      up_ch_lpm_dfe_n_1 => axi_ad9371_rx_xcvr_up_ch_1_lpm_dfe_n,
      up_ch_lpm_dfe_n_2 => axi_ad9371_rx_xcvr_up_ch_2_lpm_dfe_n,
      up_ch_lpm_dfe_n_3 => axi_ad9371_rx_xcvr_up_ch_3_lpm_dfe_n,
      up_ch_out_clk_sel_0(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_out_clk_sel(2 downto 0),
      up_ch_out_clk_sel_1(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_out_clk_sel(2 downto 0),
      up_ch_out_clk_sel_2(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_out_clk_sel(2 downto 0),
      up_ch_out_clk_sel_3(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_out_clk_sel(2 downto 0),
      up_ch_pll_locked_0 => axi_ad9371_rx_xcvr_up_ch_0_pll_locked,
      up_ch_pll_locked_1 => axi_ad9371_rx_xcvr_up_ch_1_pll_locked,
      up_ch_pll_locked_2 => axi_ad9371_rx_xcvr_up_ch_2_pll_locked,
      up_ch_pll_locked_3 => axi_ad9371_rx_xcvr_up_ch_3_pll_locked,
      up_ch_rate_0(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_rate(2 downto 0),
      up_ch_rate_1(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_rate(2 downto 0),
      up_ch_rate_2(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_rate(2 downto 0),
      up_ch_rate_3(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_rate(2 downto 0),
      up_ch_rdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_rdata(15 downto 0),
      up_ch_rdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_rdata(15 downto 0),
      up_ch_rdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_rdata(15 downto 0),
      up_ch_rdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_rdata(15 downto 0),
      up_ch_ready_0 => axi_ad9371_rx_xcvr_up_ch_0_ready,
      up_ch_ready_1 => axi_ad9371_rx_xcvr_up_ch_1_ready,
      up_ch_ready_2 => axi_ad9371_rx_xcvr_up_ch_2_ready,
      up_ch_ready_3 => axi_ad9371_rx_xcvr_up_ch_3_ready,
      up_ch_rst_0 => axi_ad9371_rx_xcvr_up_ch_0_rst,
      up_ch_rst_1 => axi_ad9371_rx_xcvr_up_ch_1_rst,
      up_ch_rst_2 => axi_ad9371_rx_xcvr_up_ch_2_rst,
      up_ch_rst_3 => axi_ad9371_rx_xcvr_up_ch_3_rst,
      up_ch_rst_done_0 => axi_ad9371_rx_xcvr_up_ch_0_rst_done,
      up_ch_rst_done_1 => axi_ad9371_rx_xcvr_up_ch_1_rst_done,
      up_ch_rst_done_2 => axi_ad9371_rx_xcvr_up_ch_2_rst_done,
      up_ch_rst_done_3 => axi_ad9371_rx_xcvr_up_ch_3_rst_done,
      up_ch_sys_clk_sel_0(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_sys_clk_sel(1 downto 0),
      up_ch_sys_clk_sel_1(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_sys_clk_sel(1 downto 0),
      up_ch_sys_clk_sel_2(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_sys_clk_sel(1 downto 0),
      up_ch_sys_clk_sel_3(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_sys_clk_sel(1 downto 0),
      up_ch_tx_diffctrl_0(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_0_UNCONNECTED(4 downto 0),
      up_ch_tx_diffctrl_1(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_1_UNCONNECTED(4 downto 0),
      up_ch_tx_diffctrl_2(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_2_UNCONNECTED(4 downto 0),
      up_ch_tx_diffctrl_3(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_diffctrl_3_UNCONNECTED(4 downto 0),
      up_ch_tx_postcursor_0(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_0_UNCONNECTED(4 downto 0),
      up_ch_tx_postcursor_1(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_1_UNCONNECTED(4 downto 0),
      up_ch_tx_postcursor_2(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_2_UNCONNECTED(4 downto 0),
      up_ch_tx_postcursor_3(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_postcursor_3_UNCONNECTED(4 downto 0),
      up_ch_tx_precursor_0(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_0_UNCONNECTED(4 downto 0),
      up_ch_tx_precursor_1(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_1_UNCONNECTED(4 downto 0),
      up_ch_tx_precursor_2(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_2_UNCONNECTED(4 downto 0),
      up_ch_tx_precursor_3(4 downto 0) => NLW_axi_ad9371_rx_xcvr_up_ch_tx_precursor_3_UNCONNECTED(4 downto 0),
      up_ch_user_ready_0 => axi_ad9371_rx_xcvr_up_ch_0_user_ready,
      up_ch_user_ready_1 => axi_ad9371_rx_xcvr_up_ch_1_user_ready,
      up_ch_user_ready_2 => axi_ad9371_rx_xcvr_up_ch_2_user_ready,
      up_ch_user_ready_3 => axi_ad9371_rx_xcvr_up_ch_3_user_ready,
      up_ch_wdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_wdata(15 downto 0),
      up_ch_wdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_wdata(15 downto 0),
      up_ch_wdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_wdata(15 downto 0),
      up_ch_wdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_wdata(15 downto 0),
      up_ch_wr_0 => axi_ad9371_rx_xcvr_up_ch_0_wr,
      up_ch_wr_1 => axi_ad9371_rx_xcvr_up_ch_1_wr,
      up_ch_wr_2 => axi_ad9371_rx_xcvr_up_ch_2_wr,
      up_ch_wr_3 => axi_ad9371_rx_xcvr_up_ch_3_wr,
      up_es_addr_0(11 downto 0) => axi_ad9371_rx_xcvr_up_es_0_addr(11 downto 0),
      up_es_addr_1(11 downto 0) => axi_ad9371_rx_xcvr_up_es_1_addr(11 downto 0),
      up_es_addr_2(11 downto 0) => axi_ad9371_rx_xcvr_up_es_2_addr(11 downto 0),
      up_es_addr_3(11 downto 0) => axi_ad9371_rx_xcvr_up_es_3_addr(11 downto 0),
      up_es_enb_0 => axi_ad9371_rx_xcvr_up_es_0_enb,
      up_es_enb_1 => axi_ad9371_rx_xcvr_up_es_1_enb,
      up_es_enb_2 => axi_ad9371_rx_xcvr_up_es_2_enb,
      up_es_enb_3 => axi_ad9371_rx_xcvr_up_es_3_enb,
      up_es_rdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_es_0_rdata(15 downto 0),
      up_es_rdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_es_1_rdata(15 downto 0),
      up_es_rdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_es_2_rdata(15 downto 0),
      up_es_rdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_es_3_rdata(15 downto 0),
      up_es_ready_0 => axi_ad9371_rx_xcvr_up_es_0_ready,
      up_es_ready_1 => axi_ad9371_rx_xcvr_up_es_1_ready,
      up_es_ready_2 => axi_ad9371_rx_xcvr_up_es_2_ready,
      up_es_ready_3 => axi_ad9371_rx_xcvr_up_es_3_ready,
      up_es_reset_0 => axi_ad9371_rx_xcvr_up_es_0_reset,
      up_es_reset_1 => axi_ad9371_rx_xcvr_up_es_1_reset,
      up_es_reset_2 => axi_ad9371_rx_xcvr_up_es_2_reset,
      up_es_reset_3 => axi_ad9371_rx_xcvr_up_es_3_reset,
      up_es_wdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_es_0_wdata(15 downto 0),
      up_es_wdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_es_1_wdata(15 downto 0),
      up_es_wdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_es_2_wdata(15 downto 0),
      up_es_wdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_es_3_wdata(15 downto 0),
      up_es_wr_0 => axi_ad9371_rx_xcvr_up_es_0_wr,
      up_es_wr_1 => axi_ad9371_rx_xcvr_up_es_1_wr,
      up_es_wr_2 => axi_ad9371_rx_xcvr_up_es_2_wr,
      up_es_wr_3 => axi_ad9371_rx_xcvr_up_es_3_wr,
      up_pll_rst => axi_ad9371_rx_xcvr_up_pll_rst,
      up_status => NLW_axi_ad9371_rx_xcvr_up_status_UNCONNECTED
    );
axi_ad9371_tx_clkgen: component system_axi_ad9371_tx_clkgen_0
     port map (
      clk => clk_0_2,
      clk_0 => ad9371_tx_device_clk,
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(15 downto 0) => s_axi_0_7_ARADDR(15 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_7_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_7_ARREADY,
      s_axi_arvalid => s_axi_0_7_ARVALID,
      s_axi_awaddr(15 downto 0) => s_axi_0_7_AWADDR(15 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_7_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_7_AWREADY,
      s_axi_awvalid => s_axi_0_7_AWVALID,
      s_axi_bready => s_axi_0_7_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_7_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_7_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_7_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_7_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_7_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_7_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_7_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_7_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_7_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_7_WVALID
    );
axi_ad9371_tx_jesd: entity work.axi_ad9371_tx_jesd_imp_17BPCLV
     port map (
      device_clk => ad9371_tx_device_clk,
      irq => NLW_axi_ad9371_tx_jesd_irq_UNCONNECTED,
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(13 downto 0) => s_axi_0_5_ARADDR(13 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_5_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_5_ARREADY,
      s_axi_arvalid => s_axi_0_5_ARVALID,
      s_axi_awaddr(13 downto 0) => s_axi_0_5_AWADDR(13 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_5_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_5_AWREADY,
      s_axi_awvalid => s_axi_0_5_AWVALID,
      s_axi_bready => s_axi_0_5_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_5_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_5_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_5_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_5_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_5_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_5_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_5_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_5_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_5_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_5_WVALID,
      sync(0) => sync_1(0),
      sysref => sysref_1,
      tx_data_tdata(127 downto 0) => tx_data_1_TDATA(127 downto 0),
      tx_data_tready => tx_data_1_TREADY,
      tx_data_tvalid => tx_data_1_TVALID,
      tx_phy0_txcharisk(3 downto 0) => axi_ad9371_tx_jesd_tx_phy0_txcharisk(3 downto 0),
      tx_phy0_txdata(31 downto 0) => axi_ad9371_tx_jesd_tx_phy0_txdata(31 downto 0),
      tx_phy1_txcharisk(3 downto 0) => axi_ad9371_tx_jesd_tx_phy1_txcharisk(3 downto 0),
      tx_phy1_txdata(31 downto 0) => axi_ad9371_tx_jesd_tx_phy1_txdata(31 downto 0),
      tx_phy2_txcharisk(3 downto 0) => axi_ad9371_tx_jesd_tx_phy2_txcharisk(3 downto 0),
      tx_phy2_txdata(31 downto 0) => axi_ad9371_tx_jesd_tx_phy2_txdata(31 downto 0),
      tx_phy3_txcharisk(3 downto 0) => axi_ad9371_tx_jesd_tx_phy3_txcharisk(3 downto 0),
      tx_phy3_txdata(31 downto 0) => axi_ad9371_tx_jesd_tx_phy3_txdata(31 downto 0)
    );
axi_ad9371_tx_xcvr: component system_axi_ad9371_tx_xcvr_0
     port map (
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(15 downto 0) => s_axi_0_2_ARADDR(15 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_2_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_2_ARREADY,
      s_axi_arvalid => s_axi_0_2_ARVALID,
      s_axi_awaddr(15 downto 0) => s_axi_0_2_AWADDR(15 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_2_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_2_AWREADY,
      s_axi_awvalid => s_axi_0_2_AWVALID,
      s_axi_bready => s_axi_0_2_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_2_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_2_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_2_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_2_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_2_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_2_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_2_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_2_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_2_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_2_WVALID,
      up_ch_addr_0(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_addr(11 downto 0),
      up_ch_addr_1(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_addr(11 downto 0),
      up_ch_addr_2(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_addr(11 downto 0),
      up_ch_addr_3(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_addr(11 downto 0),
      up_ch_enb_0 => axi_ad9371_tx_xcvr_up_ch_0_enb,
      up_ch_enb_1 => axi_ad9371_tx_xcvr_up_ch_1_enb,
      up_ch_enb_2 => axi_ad9371_tx_xcvr_up_ch_2_enb,
      up_ch_enb_3 => axi_ad9371_tx_xcvr_up_ch_3_enb,
      up_ch_lpm_dfe_n_0 => axi_ad9371_tx_xcvr_up_ch_0_lpm_dfe_n,
      up_ch_lpm_dfe_n_1 => axi_ad9371_tx_xcvr_up_ch_1_lpm_dfe_n,
      up_ch_lpm_dfe_n_2 => axi_ad9371_tx_xcvr_up_ch_2_lpm_dfe_n,
      up_ch_lpm_dfe_n_3 => axi_ad9371_tx_xcvr_up_ch_3_lpm_dfe_n,
      up_ch_out_clk_sel_0(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_out_clk_sel(2 downto 0),
      up_ch_out_clk_sel_1(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_out_clk_sel(2 downto 0),
      up_ch_out_clk_sel_2(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_out_clk_sel(2 downto 0),
      up_ch_out_clk_sel_3(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_out_clk_sel(2 downto 0),
      up_ch_pll_locked_0 => axi_ad9371_tx_xcvr_up_ch_0_pll_locked,
      up_ch_pll_locked_1 => axi_ad9371_tx_xcvr_up_ch_1_pll_locked,
      up_ch_pll_locked_2 => axi_ad9371_tx_xcvr_up_ch_2_pll_locked,
      up_ch_pll_locked_3 => axi_ad9371_tx_xcvr_up_ch_3_pll_locked,
      up_ch_rate_0(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_rate(2 downto 0),
      up_ch_rate_1(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_rate(2 downto 0),
      up_ch_rate_2(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_rate(2 downto 0),
      up_ch_rate_3(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_rate(2 downto 0),
      up_ch_rdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_rdata(15 downto 0),
      up_ch_rdata_1(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_rdata(15 downto 0),
      up_ch_rdata_2(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_rdata(15 downto 0),
      up_ch_rdata_3(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_rdata(15 downto 0),
      up_ch_ready_0 => axi_ad9371_tx_xcvr_up_ch_0_ready,
      up_ch_ready_1 => axi_ad9371_tx_xcvr_up_ch_1_ready,
      up_ch_ready_2 => axi_ad9371_tx_xcvr_up_ch_2_ready,
      up_ch_ready_3 => axi_ad9371_tx_xcvr_up_ch_3_ready,
      up_ch_rst_0 => axi_ad9371_tx_xcvr_up_ch_0_rst,
      up_ch_rst_1 => axi_ad9371_tx_xcvr_up_ch_1_rst,
      up_ch_rst_2 => axi_ad9371_tx_xcvr_up_ch_2_rst,
      up_ch_rst_3 => axi_ad9371_tx_xcvr_up_ch_3_rst,
      up_ch_rst_done_0 => axi_ad9371_tx_xcvr_up_ch_0_rst_done,
      up_ch_rst_done_1 => axi_ad9371_tx_xcvr_up_ch_1_rst_done,
      up_ch_rst_done_2 => axi_ad9371_tx_xcvr_up_ch_2_rst_done,
      up_ch_rst_done_3 => axi_ad9371_tx_xcvr_up_ch_3_rst_done,
      up_ch_sys_clk_sel_0(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_sys_clk_sel(1 downto 0),
      up_ch_sys_clk_sel_1(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_sys_clk_sel(1 downto 0),
      up_ch_sys_clk_sel_2(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_sys_clk_sel(1 downto 0),
      up_ch_sys_clk_sel_3(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_sys_clk_sel(1 downto 0),
      up_ch_tx_diffctrl_0(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_tx_diffctrl(4 downto 0),
      up_ch_tx_diffctrl_1(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_tx_diffctrl(4 downto 0),
      up_ch_tx_diffctrl_2(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_tx_diffctrl(4 downto 0),
      up_ch_tx_diffctrl_3(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_tx_diffctrl(4 downto 0),
      up_ch_tx_postcursor_0(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_tx_postcursor(4 downto 0),
      up_ch_tx_postcursor_1(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_tx_postcursor(4 downto 0),
      up_ch_tx_postcursor_2(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_tx_postcursor(4 downto 0),
      up_ch_tx_postcursor_3(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_tx_postcursor(4 downto 0),
      up_ch_tx_precursor_0(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_tx_precursor(4 downto 0),
      up_ch_tx_precursor_1(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_tx_precursor(4 downto 0),
      up_ch_tx_precursor_2(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_tx_precursor(4 downto 0),
      up_ch_tx_precursor_3(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_tx_precursor(4 downto 0),
      up_ch_user_ready_0 => axi_ad9371_tx_xcvr_up_ch_0_user_ready,
      up_ch_user_ready_1 => axi_ad9371_tx_xcvr_up_ch_1_user_ready,
      up_ch_user_ready_2 => axi_ad9371_tx_xcvr_up_ch_2_user_ready,
      up_ch_user_ready_3 => axi_ad9371_tx_xcvr_up_ch_3_user_ready,
      up_ch_wdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_wdata(15 downto 0),
      up_ch_wdata_1(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_wdata(15 downto 0),
      up_ch_wdata_2(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_wdata(15 downto 0),
      up_ch_wdata_3(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_wdata(15 downto 0),
      up_ch_wr_0 => axi_ad9371_tx_xcvr_up_ch_0_wr,
      up_ch_wr_1 => axi_ad9371_tx_xcvr_up_ch_1_wr,
      up_ch_wr_2 => axi_ad9371_tx_xcvr_up_ch_2_wr,
      up_ch_wr_3 => axi_ad9371_tx_xcvr_up_ch_3_wr,
      up_cm_addr_0(11 downto 0) => axi_ad9371_tx_xcvr_up_cm_0_addr(11 downto 0),
      up_cm_enb_0 => axi_ad9371_tx_xcvr_up_cm_0_enb,
      up_cm_rdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_cm_0_rdata(15 downto 0),
      up_cm_ready_0 => axi_ad9371_tx_xcvr_up_cm_0_ready,
      up_cm_wdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_cm_0_wdata(15 downto 0),
      up_cm_wr_0 => axi_ad9371_tx_xcvr_up_cm_0_wr,
      up_pll_rst => axi_ad9371_tx_xcvr_up_pll_rst,
      up_status => NLW_axi_ad9371_tx_xcvr_up_status_UNCONNECTED
    );
rx_ad9371_tpl_core: entity work.rx_ad9371_tpl_core_imp_WW7IEM
     port map (
      adc_data_0(31 downto 0) => rx_ad9371_tpl_core_adc_data_0(31 downto 0),
      adc_data_1(31 downto 0) => rx_ad9371_tpl_core_adc_data_1(31 downto 0),
      adc_data_2(31 downto 0) => rx_ad9371_tpl_core_adc_data_2(31 downto 0),
      adc_data_3(31 downto 0) => rx_ad9371_tpl_core_adc_data_3(31 downto 0),
      adc_dovf => adc_dovf_0_1,
      adc_enable_0(0) => rx_ad9371_tpl_core_adc_enable_0(0),
      adc_enable_1(0) => rx_ad9371_tpl_core_adc_enable_1(0),
      adc_enable_2(0) => rx_ad9371_tpl_core_adc_enable_2(0),
      adc_enable_3(0) => rx_ad9371_tpl_core_adc_enable_3(0),
      adc_valid_0(0) => rx_ad9371_tpl_core_adc_valid_0(0),
      adc_valid_1(0) => rx_ad9371_tpl_core_adc_valid_1(0),
      adc_valid_2(0) => rx_ad9371_tpl_core_adc_valid_2(0),
      adc_valid_3(0) => rx_ad9371_tpl_core_adc_valid_3(0),
      link_clk => ad9371_rx_device_clk,
      link_data(127 downto 0) => axi_ad9371_rx_jesd_rx_data_tdata(127 downto 0),
      link_sof(3 downto 0) => axi_ad9371_rx_jesd_rx_sof(3 downto 0),
      link_valid => axi_ad9371_rx_jesd_rx_data_tvalid,
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(12 downto 0) => s_axi_0_4_ARADDR(12 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_4_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_4_ARREADY,
      s_axi_arvalid => s_axi_0_4_ARVALID,
      s_axi_awaddr(12 downto 0) => s_axi_0_4_AWADDR(12 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_4_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_4_AWREADY,
      s_axi_awvalid => s_axi_0_4_AWVALID,
      s_axi_bready => s_axi_0_4_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_4_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_4_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_4_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_4_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_4_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_4_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_4_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_4_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_4_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_4_WVALID
    );
tx_ad9371_tpl_core: entity work.tx_ad9371_tpl_core_imp_1J4NRGE
     port map (
      dac_data_0(31 downto 0) => dac_data_0_0_1(31 downto 0),
      dac_data_1(31 downto 0) => dac_data_1_0_1(31 downto 0),
      dac_data_2(31 downto 0) => dac_data_2_0_1(31 downto 0),
      dac_data_3(31 downto 0) => dac_data_3_0_1(31 downto 0),
      dac_dunf => dac_dunf_0_1,
      dac_enable_0(0) => tx_ad9371_tpl_core_dac_enable_0(0),
      dac_enable_1(0) => tx_ad9371_tpl_core_dac_enable_1(0),
      dac_enable_2(0) => tx_ad9371_tpl_core_dac_enable_2(0),
      dac_enable_3(0) => tx_ad9371_tpl_core_dac_enable_3(0),
      dac_valid_0(0) => tx_ad9371_tpl_core_dac_valid_0(0),
      dac_valid_1(0) => tx_ad9371_tpl_core_dac_valid_1(0),
      dac_valid_2(0) => tx_ad9371_tpl_core_dac_valid_2(0),
      dac_valid_3(0) => tx_ad9371_tpl_core_dac_valid_3(0),
      link_clk => ad9371_tx_device_clk,
      link_tdata(127 downto 0) => tx_data_1_TDATA(127 downto 0),
      link_tready => tx_data_1_TREADY,
      link_tvalid => tx_data_1_TVALID,
      s_axi_aclk => s_axi_aclk_0_1,
      s_axi_araddr(12 downto 0) => s_axi_0_3_ARADDR(12 downto 0),
      s_axi_aresetn => s_axi_aresetn_0_1,
      s_axi_arprot(2 downto 0) => s_axi_0_3_ARPROT(2 downto 0),
      s_axi_arready => s_axi_0_3_ARREADY,
      s_axi_arvalid => s_axi_0_3_ARVALID,
      s_axi_awaddr(12 downto 0) => s_axi_0_3_AWADDR(12 downto 0),
      s_axi_awprot(2 downto 0) => s_axi_0_3_AWPROT(2 downto 0),
      s_axi_awready => s_axi_0_3_AWREADY,
      s_axi_awvalid => s_axi_0_3_AWVALID,
      s_axi_bready => s_axi_0_3_BREADY,
      s_axi_bresp(1 downto 0) => s_axi_0_3_BRESP(1 downto 0),
      s_axi_bvalid => s_axi_0_3_BVALID,
      s_axi_rdata(31 downto 0) => s_axi_0_3_RDATA(31 downto 0),
      s_axi_rready => s_axi_0_3_RREADY,
      s_axi_rresp(1 downto 0) => s_axi_0_3_RRESP(1 downto 0),
      s_axi_rvalid => s_axi_0_3_RVALID,
      s_axi_wdata(31 downto 0) => s_axi_0_3_WDATA(31 downto 0),
      s_axi_wready => s_axi_0_3_WREADY,
      s_axi_wstrb(3 downto 0) => s_axi_0_3_WSTRB(3 downto 0),
      s_axi_wvalid => s_axi_0_3_WVALID
    );
util_ad9371_xcvr: component system_util_ad9371_xcvr_0
     port map (
      cpll_ref_clk_0 => rx_ref_clk_0_1,
      cpll_ref_clk_1 => rx_ref_clk_0_1,
      cpll_ref_clk_2 => rx_ref_clk_0_1,
      cpll_ref_clk_3 => rx_ref_clk_0_1,
      qpll_ref_clk_0 => tx_ref_clk_0_1,
      rx_0_n => rx_data_0_n_1,
      rx_0_p => rx_data_0_p_1,
      rx_1_n => rx_data_1_n_1,
      rx_1_p => rx_data_1_p_1,
      rx_2_n => rx_data_2_n_1,
      rx_2_p => rx_data_2_p_1,
      rx_3_n => rx_data_3_n_1,
      rx_3_p => rx_data_3_p_1,
      rx_calign_0 => axi_ad9371_rx_jesd_phy_en_char_align,
      rx_calign_1 => axi_ad9371_rx_jesd_phy_en_char_align,
      rx_calign_2 => axi_ad9371_rx_jesd_phy_en_char_align,
      rx_calign_3 => axi_ad9371_rx_jesd_phy_en_char_align,
      rx_charisk_0(3 downto 0) => util_ad9371_xcvr_rx_0_rxcharisk(3 downto 0),
      rx_charisk_1(3 downto 0) => util_ad9371_xcvr_rx_1_rxcharisk(3 downto 0),
      rx_charisk_2(3 downto 0) => util_ad9371_xcvr_rx_2_rxcharisk(3 downto 0),
      rx_charisk_3(3 downto 0) => util_ad9371_xcvr_rx_3_rxcharisk(3 downto 0),
      rx_clk_0 => ad9371_rx_device_clk,
      rx_clk_1 => ad9371_rx_device_clk,
      rx_clk_2 => ad9371_rx_device_clk,
      rx_clk_3 => ad9371_rx_device_clk,
      rx_data_0(31 downto 0) => util_ad9371_xcvr_rx_0_rxdata(31 downto 0),
      rx_data_1(31 downto 0) => util_ad9371_xcvr_rx_1_rxdata(31 downto 0),
      rx_data_2(31 downto 0) => util_ad9371_xcvr_rx_2_rxdata(31 downto 0),
      rx_data_3(31 downto 0) => util_ad9371_xcvr_rx_3_rxdata(31 downto 0),
      rx_disperr_0(3 downto 0) => util_ad9371_xcvr_rx_0_rxdisperr(3 downto 0),
      rx_disperr_1(3 downto 0) => util_ad9371_xcvr_rx_1_rxdisperr(3 downto 0),
      rx_disperr_2(3 downto 0) => util_ad9371_xcvr_rx_2_rxdisperr(3 downto 0),
      rx_disperr_3(3 downto 0) => util_ad9371_xcvr_rx_3_rxdisperr(3 downto 0),
      rx_notintable_0(3 downto 0) => util_ad9371_xcvr_rx_0_rxnotintable(3 downto 0),
      rx_notintable_1(3 downto 0) => util_ad9371_xcvr_rx_1_rxnotintable(3 downto 0),
      rx_notintable_2(3 downto 0) => util_ad9371_xcvr_rx_2_rxnotintable(3 downto 0),
      rx_notintable_3(3 downto 0) => util_ad9371_xcvr_rx_3_rxnotintable(3 downto 0),
      rx_out_clk_0 => util_ad9371_xcvr_rx_out_clk_0,
      rx_out_clk_1 => NLW_util_ad9371_xcvr_rx_out_clk_1_UNCONNECTED,
      rx_out_clk_2 => NLW_util_ad9371_xcvr_rx_out_clk_2_UNCONNECTED,
      rx_out_clk_3 => NLW_util_ad9371_xcvr_rx_out_clk_3_UNCONNECTED,
      tx_0_n => util_ad9371_xcvr_tx_0_n,
      tx_0_p => util_ad9371_xcvr_tx_0_p,
      tx_1_n => util_ad9371_xcvr_tx_1_n,
      tx_1_p => util_ad9371_xcvr_tx_1_p,
      tx_2_n => util_ad9371_xcvr_tx_2_n,
      tx_2_p => util_ad9371_xcvr_tx_2_p,
      tx_3_n => util_ad9371_xcvr_tx_3_n,
      tx_3_p => util_ad9371_xcvr_tx_3_p,
      tx_charisk_0(3 downto 0) => axi_ad9371_tx_jesd_tx_phy0_txcharisk(3 downto 0),
      tx_charisk_1(3 downto 0) => axi_ad9371_tx_jesd_tx_phy1_txcharisk(3 downto 0),
      tx_charisk_2(3 downto 0) => axi_ad9371_tx_jesd_tx_phy2_txcharisk(3 downto 0),
      tx_charisk_3(3 downto 0) => axi_ad9371_tx_jesd_tx_phy3_txcharisk(3 downto 0),
      tx_clk_0 => ad9371_tx_device_clk,
      tx_clk_1 => ad9371_tx_device_clk,
      tx_clk_2 => ad9371_tx_device_clk,
      tx_clk_3 => ad9371_tx_device_clk,
      tx_data_0(31 downto 0) => axi_ad9371_tx_jesd_tx_phy0_txdata(31 downto 0),
      tx_data_1(31 downto 0) => axi_ad9371_tx_jesd_tx_phy1_txdata(31 downto 0),
      tx_data_2(31 downto 0) => axi_ad9371_tx_jesd_tx_phy2_txdata(31 downto 0),
      tx_data_3(31 downto 0) => axi_ad9371_tx_jesd_tx_phy3_txdata(31 downto 0),
      tx_out_clk_0 => util_ad9371_xcvr_tx_out_clk_0,
      tx_out_clk_1 => NLW_util_ad9371_xcvr_tx_out_clk_1_UNCONNECTED,
      tx_out_clk_2 => NLW_util_ad9371_xcvr_tx_out_clk_2_UNCONNECTED,
      tx_out_clk_3 => NLW_util_ad9371_xcvr_tx_out_clk_3_UNCONNECTED,
      up_clk => s_axi_aclk_0_1,
      up_cm_addr_0(11 downto 0) => axi_ad9371_tx_xcvr_up_cm_0_addr(11 downto 0),
      up_cm_enb_0 => axi_ad9371_tx_xcvr_up_cm_0_enb,
      up_cm_rdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_cm_0_rdata(15 downto 0),
      up_cm_ready_0 => axi_ad9371_tx_xcvr_up_cm_0_ready,
      up_cm_wdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_cm_0_wdata(15 downto 0),
      up_cm_wr_0 => axi_ad9371_tx_xcvr_up_cm_0_wr,
      up_cpll_rst_0 => axi_ad9371_rx_xcvr_up_pll_rst,
      up_cpll_rst_1 => axi_ad9371_rx_xcvr_up_pll_rst,
      up_cpll_rst_2 => axi_ad9371_rx_xcvr_up_pll_rst,
      up_cpll_rst_3 => axi_ad9371_rx_xcvr_up_pll_rst,
      up_es_addr_0(11 downto 0) => axi_ad9371_rx_xcvr_up_es_0_addr(11 downto 0),
      up_es_addr_1(11 downto 0) => axi_ad9371_rx_xcvr_up_es_1_addr(11 downto 0),
      up_es_addr_2(11 downto 0) => axi_ad9371_rx_xcvr_up_es_2_addr(11 downto 0),
      up_es_addr_3(11 downto 0) => axi_ad9371_rx_xcvr_up_es_3_addr(11 downto 0),
      up_es_enb_0 => axi_ad9371_rx_xcvr_up_es_0_enb,
      up_es_enb_1 => axi_ad9371_rx_xcvr_up_es_1_enb,
      up_es_enb_2 => axi_ad9371_rx_xcvr_up_es_2_enb,
      up_es_enb_3 => axi_ad9371_rx_xcvr_up_es_3_enb,
      up_es_rdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_es_0_rdata(15 downto 0),
      up_es_rdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_es_1_rdata(15 downto 0),
      up_es_rdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_es_2_rdata(15 downto 0),
      up_es_rdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_es_3_rdata(15 downto 0),
      up_es_ready_0 => axi_ad9371_rx_xcvr_up_es_0_ready,
      up_es_ready_1 => axi_ad9371_rx_xcvr_up_es_1_ready,
      up_es_ready_2 => axi_ad9371_rx_xcvr_up_es_2_ready,
      up_es_ready_3 => axi_ad9371_rx_xcvr_up_es_3_ready,
      up_es_reset_0 => axi_ad9371_rx_xcvr_up_es_0_reset,
      up_es_reset_1 => axi_ad9371_rx_xcvr_up_es_1_reset,
      up_es_reset_2 => axi_ad9371_rx_xcvr_up_es_2_reset,
      up_es_reset_3 => axi_ad9371_rx_xcvr_up_es_3_reset,
      up_es_wdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_es_0_wdata(15 downto 0),
      up_es_wdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_es_1_wdata(15 downto 0),
      up_es_wdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_es_2_wdata(15 downto 0),
      up_es_wdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_es_3_wdata(15 downto 0),
      up_es_wr_0 => axi_ad9371_rx_xcvr_up_es_0_wr,
      up_es_wr_1 => axi_ad9371_rx_xcvr_up_es_1_wr,
      up_es_wr_2 => axi_ad9371_rx_xcvr_up_es_2_wr,
      up_es_wr_3 => axi_ad9371_rx_xcvr_up_es_3_wr,
      up_qpll_rst_0 => axi_ad9371_tx_xcvr_up_pll_rst,
      up_rstn => s_axi_aresetn_0_1,
      up_rx_addr_0(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_addr(11 downto 0),
      up_rx_addr_1(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_addr(11 downto 0),
      up_rx_addr_2(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_addr(11 downto 0),
      up_rx_addr_3(11 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_addr(11 downto 0),
      up_rx_enb_0 => axi_ad9371_rx_xcvr_up_ch_0_enb,
      up_rx_enb_1 => axi_ad9371_rx_xcvr_up_ch_1_enb,
      up_rx_enb_2 => axi_ad9371_rx_xcvr_up_ch_2_enb,
      up_rx_enb_3 => axi_ad9371_rx_xcvr_up_ch_3_enb,
      up_rx_lpm_dfe_n_0 => axi_ad9371_rx_xcvr_up_ch_0_lpm_dfe_n,
      up_rx_lpm_dfe_n_1 => axi_ad9371_rx_xcvr_up_ch_1_lpm_dfe_n,
      up_rx_lpm_dfe_n_2 => axi_ad9371_rx_xcvr_up_ch_2_lpm_dfe_n,
      up_rx_lpm_dfe_n_3 => axi_ad9371_rx_xcvr_up_ch_3_lpm_dfe_n,
      up_rx_out_clk_sel_0(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_out_clk_sel(2 downto 0),
      up_rx_out_clk_sel_1(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_out_clk_sel(2 downto 0),
      up_rx_out_clk_sel_2(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_out_clk_sel(2 downto 0),
      up_rx_out_clk_sel_3(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_out_clk_sel(2 downto 0),
      up_rx_pll_locked_0 => axi_ad9371_rx_xcvr_up_ch_0_pll_locked,
      up_rx_pll_locked_1 => axi_ad9371_rx_xcvr_up_ch_1_pll_locked,
      up_rx_pll_locked_2 => axi_ad9371_rx_xcvr_up_ch_2_pll_locked,
      up_rx_pll_locked_3 => axi_ad9371_rx_xcvr_up_ch_3_pll_locked,
      up_rx_rate_0(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_rate(2 downto 0),
      up_rx_rate_1(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_rate(2 downto 0),
      up_rx_rate_2(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_rate(2 downto 0),
      up_rx_rate_3(2 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_rate(2 downto 0),
      up_rx_rdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_rdata(15 downto 0),
      up_rx_rdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_rdata(15 downto 0),
      up_rx_rdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_rdata(15 downto 0),
      up_rx_rdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_rdata(15 downto 0),
      up_rx_ready_0 => axi_ad9371_rx_xcvr_up_ch_0_ready,
      up_rx_ready_1 => axi_ad9371_rx_xcvr_up_ch_1_ready,
      up_rx_ready_2 => axi_ad9371_rx_xcvr_up_ch_2_ready,
      up_rx_ready_3 => axi_ad9371_rx_xcvr_up_ch_3_ready,
      up_rx_rst_0 => axi_ad9371_rx_xcvr_up_ch_0_rst,
      up_rx_rst_1 => axi_ad9371_rx_xcvr_up_ch_1_rst,
      up_rx_rst_2 => axi_ad9371_rx_xcvr_up_ch_2_rst,
      up_rx_rst_3 => axi_ad9371_rx_xcvr_up_ch_3_rst,
      up_rx_rst_done_0 => axi_ad9371_rx_xcvr_up_ch_0_rst_done,
      up_rx_rst_done_1 => axi_ad9371_rx_xcvr_up_ch_1_rst_done,
      up_rx_rst_done_2 => axi_ad9371_rx_xcvr_up_ch_2_rst_done,
      up_rx_rst_done_3 => axi_ad9371_rx_xcvr_up_ch_3_rst_done,
      up_rx_sys_clk_sel_0(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_sys_clk_sel(1 downto 0),
      up_rx_sys_clk_sel_1(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_sys_clk_sel(1 downto 0),
      up_rx_sys_clk_sel_2(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_sys_clk_sel(1 downto 0),
      up_rx_sys_clk_sel_3(1 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_sys_clk_sel(1 downto 0),
      up_rx_user_ready_0 => axi_ad9371_rx_xcvr_up_ch_0_user_ready,
      up_rx_user_ready_1 => axi_ad9371_rx_xcvr_up_ch_1_user_ready,
      up_rx_user_ready_2 => axi_ad9371_rx_xcvr_up_ch_2_user_ready,
      up_rx_user_ready_3 => axi_ad9371_rx_xcvr_up_ch_3_user_ready,
      up_rx_wdata_0(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_0_wdata(15 downto 0),
      up_rx_wdata_1(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_1_wdata(15 downto 0),
      up_rx_wdata_2(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_2_wdata(15 downto 0),
      up_rx_wdata_3(15 downto 0) => axi_ad9371_rx_xcvr_up_ch_3_wdata(15 downto 0),
      up_rx_wr_0 => axi_ad9371_rx_xcvr_up_ch_0_wr,
      up_rx_wr_1 => axi_ad9371_rx_xcvr_up_ch_1_wr,
      up_rx_wr_2 => axi_ad9371_rx_xcvr_up_ch_2_wr,
      up_rx_wr_3 => axi_ad9371_rx_xcvr_up_ch_3_wr,
      up_tx_addr_0(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_addr(11 downto 0),
      up_tx_addr_1(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_addr(11 downto 0),
      up_tx_addr_2(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_addr(11 downto 0),
      up_tx_addr_3(11 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_addr(11 downto 0),
      up_tx_diffctrl_0(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_tx_diffctrl(4 downto 0),
      up_tx_diffctrl_1(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_tx_diffctrl(4 downto 0),
      up_tx_diffctrl_2(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_tx_diffctrl(4 downto 0),
      up_tx_diffctrl_3(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_tx_diffctrl(4 downto 0),
      up_tx_enb_0 => axi_ad9371_tx_xcvr_up_ch_3_enb,
      up_tx_enb_1 => axi_ad9371_tx_xcvr_up_ch_0_enb,
      up_tx_enb_2 => axi_ad9371_tx_xcvr_up_ch_1_enb,
      up_tx_enb_3 => axi_ad9371_tx_xcvr_up_ch_2_enb,
      up_tx_lpm_dfe_n_0 => axi_ad9371_tx_xcvr_up_ch_3_lpm_dfe_n,
      up_tx_lpm_dfe_n_1 => axi_ad9371_tx_xcvr_up_ch_0_lpm_dfe_n,
      up_tx_lpm_dfe_n_2 => axi_ad9371_tx_xcvr_up_ch_1_lpm_dfe_n,
      up_tx_lpm_dfe_n_3 => axi_ad9371_tx_xcvr_up_ch_2_lpm_dfe_n,
      up_tx_out_clk_sel_0(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_out_clk_sel(2 downto 0),
      up_tx_out_clk_sel_1(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_out_clk_sel(2 downto 0),
      up_tx_out_clk_sel_2(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_out_clk_sel(2 downto 0),
      up_tx_out_clk_sel_3(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_out_clk_sel(2 downto 0),
      up_tx_pll_locked_0 => axi_ad9371_tx_xcvr_up_ch_3_pll_locked,
      up_tx_pll_locked_1 => axi_ad9371_tx_xcvr_up_ch_0_pll_locked,
      up_tx_pll_locked_2 => axi_ad9371_tx_xcvr_up_ch_1_pll_locked,
      up_tx_pll_locked_3 => axi_ad9371_tx_xcvr_up_ch_2_pll_locked,
      up_tx_postcursor_0(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_tx_postcursor(4 downto 0),
      up_tx_postcursor_1(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_tx_postcursor(4 downto 0),
      up_tx_postcursor_2(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_tx_postcursor(4 downto 0),
      up_tx_postcursor_3(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_tx_postcursor(4 downto 0),
      up_tx_precursor_0(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_tx_precursor(4 downto 0),
      up_tx_precursor_1(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_tx_precursor(4 downto 0),
      up_tx_precursor_2(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_tx_precursor(4 downto 0),
      up_tx_precursor_3(4 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_tx_precursor(4 downto 0),
      up_tx_rate_0(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_rate(2 downto 0),
      up_tx_rate_1(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_rate(2 downto 0),
      up_tx_rate_2(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_rate(2 downto 0),
      up_tx_rate_3(2 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_rate(2 downto 0),
      up_tx_rdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_rdata(15 downto 0),
      up_tx_rdata_1(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_rdata(15 downto 0),
      up_tx_rdata_2(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_rdata(15 downto 0),
      up_tx_rdata_3(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_rdata(15 downto 0),
      up_tx_ready_0 => axi_ad9371_tx_xcvr_up_ch_3_ready,
      up_tx_ready_1 => axi_ad9371_tx_xcvr_up_ch_0_ready,
      up_tx_ready_2 => axi_ad9371_tx_xcvr_up_ch_1_ready,
      up_tx_ready_3 => axi_ad9371_tx_xcvr_up_ch_2_ready,
      up_tx_rst_0 => axi_ad9371_tx_xcvr_up_ch_3_rst,
      up_tx_rst_1 => axi_ad9371_tx_xcvr_up_ch_0_rst,
      up_tx_rst_2 => axi_ad9371_tx_xcvr_up_ch_1_rst,
      up_tx_rst_3 => axi_ad9371_tx_xcvr_up_ch_2_rst,
      up_tx_rst_done_0 => axi_ad9371_tx_xcvr_up_ch_3_rst_done,
      up_tx_rst_done_1 => axi_ad9371_tx_xcvr_up_ch_0_rst_done,
      up_tx_rst_done_2 => axi_ad9371_tx_xcvr_up_ch_1_rst_done,
      up_tx_rst_done_3 => axi_ad9371_tx_xcvr_up_ch_2_rst_done,
      up_tx_sys_clk_sel_0(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_sys_clk_sel(1 downto 0),
      up_tx_sys_clk_sel_1(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_sys_clk_sel(1 downto 0),
      up_tx_sys_clk_sel_2(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_sys_clk_sel(1 downto 0),
      up_tx_sys_clk_sel_3(1 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_sys_clk_sel(1 downto 0),
      up_tx_user_ready_0 => axi_ad9371_tx_xcvr_up_ch_3_user_ready,
      up_tx_user_ready_1 => axi_ad9371_tx_xcvr_up_ch_0_user_ready,
      up_tx_user_ready_2 => axi_ad9371_tx_xcvr_up_ch_1_user_ready,
      up_tx_user_ready_3 => axi_ad9371_tx_xcvr_up_ch_2_user_ready,
      up_tx_wdata_0(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_3_wdata(15 downto 0),
      up_tx_wdata_1(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_0_wdata(15 downto 0),
      up_tx_wdata_2(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_1_wdata(15 downto 0),
      up_tx_wdata_3(15 downto 0) => axi_ad9371_tx_xcvr_up_ch_2_wdata(15 downto 0),
      up_tx_wr_0 => axi_ad9371_tx_xcvr_up_ch_3_wr,
      up_tx_wr_1 => axi_ad9371_tx_xcvr_up_ch_0_wr,
      up_tx_wr_2 => axi_ad9371_tx_xcvr_up_ch_1_wr,
      up_tx_wr_3 => axi_ad9371_tx_xcvr_up_ch_2_wr
    );
end STRUCTURE;
