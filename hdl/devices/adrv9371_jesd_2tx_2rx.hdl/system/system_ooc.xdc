################################################################################

# This XDC is used only for OOC mode of synthesis, implementation
# This constraints file contains default clock frequencies to be used during
# out-of-context flows such as OOC Synthesis and Hierarchical Designs.
# This constraints file is not used in normal top-down synthesis (default flow
# of Vivado)
################################################################################
create_clock -name s_axi_aclk -period 10 [get_ports s_axi_aclk]
create_clock -name rx_clkgen_ref -period 10 [get_ports rx_clkgen_ref]
create_clock -name tx_clkgen_ref -period 10 [get_ports tx_clkgen_ref]

################################################################################