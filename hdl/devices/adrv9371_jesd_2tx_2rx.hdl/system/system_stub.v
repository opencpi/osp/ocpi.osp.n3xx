// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Oct 26 15:54:06 2021
// Host        : localhost.localdomain running 64-bit CentOS Linux release 7.9.2009 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/opencpi/Xilinx/adrv9371_jesd/adrv9371_jesd.srcs/sources_1/bd/system/system_stub.v
// Design      : system
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z045ffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module system(adc_data_0, adc_data_1, adc_data_2, adc_data_3, 
  adc_dovf, adc_enable_0, adc_enable_1, adc_enable_2, adc_enable_3, adc_valid_0, adc_valid_1, 
  adc_valid_2, adc_valid_3, dac_data_0, dac_data_1, dac_data_2, dac_data_3, dac_dunf, 
  dac_enable_0, dac_enable_1, dac_enable_2, dac_enable_3, dac_valid_0, dac_valid_1, 
  dac_valid_2, dac_valid_3, rx_clkgen_ref, rx_data_0_n, rx_data_0_p, rx_data_1_n, rx_data_1_p, 
  rx_data_2_n, rx_data_2_p, rx_data_3_n, rx_data_3_p, rx_linkclk, rx_ref_clk_0, rx_sync, 
  rx_sysref_0, rxoutclk, s_axi_aclk, s_axi_aresetn, s_axi_rx_adxcvr_araddr, 
  s_axi_rx_adxcvr_arprot, s_axi_rx_adxcvr_arready, s_axi_rx_adxcvr_arvalid, 
  s_axi_rx_adxcvr_awaddr, s_axi_rx_adxcvr_awprot, s_axi_rx_adxcvr_awready, 
  s_axi_rx_adxcvr_awvalid, s_axi_rx_adxcvr_bready, s_axi_rx_adxcvr_bresp, 
  s_axi_rx_adxcvr_bvalid, s_axi_rx_adxcvr_rdata, s_axi_rx_adxcvr_rready, 
  s_axi_rx_adxcvr_rresp, s_axi_rx_adxcvr_rvalid, s_axi_rx_adxcvr_wdata, 
  s_axi_rx_adxcvr_wready, s_axi_rx_adxcvr_wstrb, s_axi_rx_adxcvr_wvalid, 
  s_axi_rx_clkgen_araddr, s_axi_rx_clkgen_arprot, s_axi_rx_clkgen_arready, 
  s_axi_rx_clkgen_arvalid, s_axi_rx_clkgen_awaddr, s_axi_rx_clkgen_awprot, 
  s_axi_rx_clkgen_awready, s_axi_rx_clkgen_awvalid, s_axi_rx_clkgen_bready, 
  s_axi_rx_clkgen_bresp, s_axi_rx_clkgen_bvalid, s_axi_rx_clkgen_rdata, 
  s_axi_rx_clkgen_rready, s_axi_rx_clkgen_rresp, s_axi_rx_clkgen_rvalid, 
  s_axi_rx_clkgen_wdata, s_axi_rx_clkgen_wready, s_axi_rx_clkgen_wstrb, 
  s_axi_rx_clkgen_wvalid, s_axi_rx_jesd_araddr, s_axi_rx_jesd_arprot, 
  s_axi_rx_jesd_arready, s_axi_rx_jesd_arvalid, s_axi_rx_jesd_awaddr, 
  s_axi_rx_jesd_awprot, s_axi_rx_jesd_awready, s_axi_rx_jesd_awvalid, 
  s_axi_rx_jesd_bready, s_axi_rx_jesd_bresp, s_axi_rx_jesd_bvalid, s_axi_rx_jesd_rdata, 
  s_axi_rx_jesd_rready, s_axi_rx_jesd_rresp, s_axi_rx_jesd_rvalid, s_axi_rx_jesd_wdata, 
  s_axi_rx_jesd_wready, s_axi_rx_jesd_wstrb, s_axi_rx_jesd_wvalid, s_axi_rx_tpl_araddr, 
  s_axi_rx_tpl_arprot, s_axi_rx_tpl_arready, s_axi_rx_tpl_arvalid, s_axi_rx_tpl_awaddr, 
  s_axi_rx_tpl_awprot, s_axi_rx_tpl_awready, s_axi_rx_tpl_awvalid, s_axi_rx_tpl_bready, 
  s_axi_rx_tpl_bresp, s_axi_rx_tpl_bvalid, s_axi_rx_tpl_rdata, s_axi_rx_tpl_rready, 
  s_axi_rx_tpl_rresp, s_axi_rx_tpl_rvalid, s_axi_rx_tpl_wdata, s_axi_rx_tpl_wready, 
  s_axi_rx_tpl_wstrb, s_axi_rx_tpl_wvalid, s_axi_tx_adxcvr_araddr, 
  s_axi_tx_adxcvr_arprot, s_axi_tx_adxcvr_arready, s_axi_tx_adxcvr_arvalid, 
  s_axi_tx_adxcvr_awaddr, s_axi_tx_adxcvr_awprot, s_axi_tx_adxcvr_awready, 
  s_axi_tx_adxcvr_awvalid, s_axi_tx_adxcvr_bready, s_axi_tx_adxcvr_bresp, 
  s_axi_tx_adxcvr_bvalid, s_axi_tx_adxcvr_rdata, s_axi_tx_adxcvr_rready, 
  s_axi_tx_adxcvr_rresp, s_axi_tx_adxcvr_rvalid, s_axi_tx_adxcvr_wdata, 
  s_axi_tx_adxcvr_wready, s_axi_tx_adxcvr_wstrb, s_axi_tx_adxcvr_wvalid, 
  s_axi_tx_clkgen_araddr, s_axi_tx_clkgen_arprot, s_axi_tx_clkgen_arready, 
  s_axi_tx_clkgen_arvalid, s_axi_tx_clkgen_awaddr, s_axi_tx_clkgen_awprot, 
  s_axi_tx_clkgen_awready, s_axi_tx_clkgen_awvalid, s_axi_tx_clkgen_bready, 
  s_axi_tx_clkgen_bresp, s_axi_tx_clkgen_bvalid, s_axi_tx_clkgen_rdata, 
  s_axi_tx_clkgen_rready, s_axi_tx_clkgen_rresp, s_axi_tx_clkgen_rvalid, 
  s_axi_tx_clkgen_wdata, s_axi_tx_clkgen_wready, s_axi_tx_clkgen_wstrb, 
  s_axi_tx_clkgen_wvalid, s_axi_tx_jesd_araddr, s_axi_tx_jesd_arprot, 
  s_axi_tx_jesd_arready, s_axi_tx_jesd_arvalid, s_axi_tx_jesd_awaddr, 
  s_axi_tx_jesd_awprot, s_axi_tx_jesd_awready, s_axi_tx_jesd_awvalid, 
  s_axi_tx_jesd_bready, s_axi_tx_jesd_bresp, s_axi_tx_jesd_bvalid, s_axi_tx_jesd_rdata, 
  s_axi_tx_jesd_rready, s_axi_tx_jesd_rresp, s_axi_tx_jesd_rvalid, s_axi_tx_jesd_wdata, 
  s_axi_tx_jesd_wready, s_axi_tx_jesd_wstrb, s_axi_tx_jesd_wvalid, s_axi_tx_tpl_araddr, 
  s_axi_tx_tpl_arprot, s_axi_tx_tpl_arready, s_axi_tx_tpl_arvalid, s_axi_tx_tpl_awaddr, 
  s_axi_tx_tpl_awprot, s_axi_tx_tpl_awready, s_axi_tx_tpl_awvalid, s_axi_tx_tpl_bready, 
  s_axi_tx_tpl_bresp, s_axi_tx_tpl_bvalid, s_axi_tx_tpl_rdata, s_axi_tx_tpl_rready, 
  s_axi_tx_tpl_rresp, s_axi_tx_tpl_rvalid, s_axi_tx_tpl_wdata, s_axi_tx_tpl_wready, 
  s_axi_tx_tpl_wstrb, s_axi_tx_tpl_wvalid, tx_clkgen_ref, tx_data_0_n, tx_data_0_p, 
  tx_data_1_n, tx_data_1_p, tx_data_2_n, tx_data_2_p, tx_data_3_n, tx_data_3_p, tx_linkclk, 
  tx_ref_clk_0, tx_sync, tx_sysref_0, txoutclk)
/* synthesis syn_black_box black_box_pad_pin="adc_data_0[31:0],adc_data_1[31:0],adc_data_2[31:0],adc_data_3[31:0],adc_dovf,adc_enable_0[0:0],adc_enable_1[0:0],adc_enable_2[0:0],adc_enable_3[0:0],adc_valid_0[0:0],adc_valid_1[0:0],adc_valid_2[0:0],adc_valid_3[0:0],dac_data_0[31:0],dac_data_1[31:0],dac_data_2[31:0],dac_data_3[31:0],dac_dunf,dac_enable_0[0:0],dac_enable_1[0:0],dac_enable_2[0:0],dac_enable_3[0:0],dac_valid_0[0:0],dac_valid_1[0:0],dac_valid_2[0:0],dac_valid_3[0:0],rx_clkgen_ref,rx_data_0_n,rx_data_0_p,rx_data_1_n,rx_data_1_p,rx_data_2_n,rx_data_2_p,rx_data_3_n,rx_data_3_p,rx_linkclk,rx_ref_clk_0,rx_sync[0:0],rx_sysref_0,rxoutclk,s_axi_aclk,s_axi_aresetn,s_axi_rx_adxcvr_araddr[15:0],s_axi_rx_adxcvr_arprot[2:0],s_axi_rx_adxcvr_arready,s_axi_rx_adxcvr_arvalid,s_axi_rx_adxcvr_awaddr[15:0],s_axi_rx_adxcvr_awprot[2:0],s_axi_rx_adxcvr_awready,s_axi_rx_adxcvr_awvalid,s_axi_rx_adxcvr_bready,s_axi_rx_adxcvr_bresp[1:0],s_axi_rx_adxcvr_bvalid,s_axi_rx_adxcvr_rdata[31:0],s_axi_rx_adxcvr_rready,s_axi_rx_adxcvr_rresp[1:0],s_axi_rx_adxcvr_rvalid,s_axi_rx_adxcvr_wdata[31:0],s_axi_rx_adxcvr_wready,s_axi_rx_adxcvr_wstrb[3:0],s_axi_rx_adxcvr_wvalid,s_axi_rx_clkgen_araddr[15:0],s_axi_rx_clkgen_arprot[2:0],s_axi_rx_clkgen_arready,s_axi_rx_clkgen_arvalid,s_axi_rx_clkgen_awaddr[15:0],s_axi_rx_clkgen_awprot[2:0],s_axi_rx_clkgen_awready,s_axi_rx_clkgen_awvalid,s_axi_rx_clkgen_bready,s_axi_rx_clkgen_bresp[1:0],s_axi_rx_clkgen_bvalid,s_axi_rx_clkgen_rdata[31:0],s_axi_rx_clkgen_rready,s_axi_rx_clkgen_rresp[1:0],s_axi_rx_clkgen_rvalid,s_axi_rx_clkgen_wdata[31:0],s_axi_rx_clkgen_wready,s_axi_rx_clkgen_wstrb[3:0],s_axi_rx_clkgen_wvalid,s_axi_rx_jesd_araddr[13:0],s_axi_rx_jesd_arprot[2:0],s_axi_rx_jesd_arready,s_axi_rx_jesd_arvalid,s_axi_rx_jesd_awaddr[13:0],s_axi_rx_jesd_awprot[2:0],s_axi_rx_jesd_awready,s_axi_rx_jesd_awvalid,s_axi_rx_jesd_bready,s_axi_rx_jesd_bresp[1:0],s_axi_rx_jesd_bvalid,s_axi_rx_jesd_rdata[31:0],s_axi_rx_jesd_rready,s_axi_rx_jesd_rresp[1:0],s_axi_rx_jesd_rvalid,s_axi_rx_jesd_wdata[31:0],s_axi_rx_jesd_wready,s_axi_rx_jesd_wstrb[3:0],s_axi_rx_jesd_wvalid,s_axi_rx_tpl_araddr[12:0],s_axi_rx_tpl_arprot[2:0],s_axi_rx_tpl_arready,s_axi_rx_tpl_arvalid,s_axi_rx_tpl_awaddr[12:0],s_axi_rx_tpl_awprot[2:0],s_axi_rx_tpl_awready,s_axi_rx_tpl_awvalid,s_axi_rx_tpl_bready,s_axi_rx_tpl_bresp[1:0],s_axi_rx_tpl_bvalid,s_axi_rx_tpl_rdata[31:0],s_axi_rx_tpl_rready,s_axi_rx_tpl_rresp[1:0],s_axi_rx_tpl_rvalid,s_axi_rx_tpl_wdata[31:0],s_axi_rx_tpl_wready,s_axi_rx_tpl_wstrb[3:0],s_axi_rx_tpl_wvalid,s_axi_tx_adxcvr_araddr[15:0],s_axi_tx_adxcvr_arprot[2:0],s_axi_tx_adxcvr_arready,s_axi_tx_adxcvr_arvalid,s_axi_tx_adxcvr_awaddr[15:0],s_axi_tx_adxcvr_awprot[2:0],s_axi_tx_adxcvr_awready,s_axi_tx_adxcvr_awvalid,s_axi_tx_adxcvr_bready,s_axi_tx_adxcvr_bresp[1:0],s_axi_tx_adxcvr_bvalid,s_axi_tx_adxcvr_rdata[31:0],s_axi_tx_adxcvr_rready,s_axi_tx_adxcvr_rresp[1:0],s_axi_tx_adxcvr_rvalid,s_axi_tx_adxcvr_wdata[31:0],s_axi_tx_adxcvr_wready,s_axi_tx_adxcvr_wstrb[3:0],s_axi_tx_adxcvr_wvalid,s_axi_tx_clkgen_araddr[15:0],s_axi_tx_clkgen_arprot[2:0],s_axi_tx_clkgen_arready,s_axi_tx_clkgen_arvalid,s_axi_tx_clkgen_awaddr[15:0],s_axi_tx_clkgen_awprot[2:0],s_axi_tx_clkgen_awready,s_axi_tx_clkgen_awvalid,s_axi_tx_clkgen_bready,s_axi_tx_clkgen_bresp[1:0],s_axi_tx_clkgen_bvalid,s_axi_tx_clkgen_rdata[31:0],s_axi_tx_clkgen_rready,s_axi_tx_clkgen_rresp[1:0],s_axi_tx_clkgen_rvalid,s_axi_tx_clkgen_wdata[31:0],s_axi_tx_clkgen_wready,s_axi_tx_clkgen_wstrb[3:0],s_axi_tx_clkgen_wvalid,s_axi_tx_jesd_araddr[13:0],s_axi_tx_jesd_arprot[2:0],s_axi_tx_jesd_arready,s_axi_tx_jesd_arvalid,s_axi_tx_jesd_awaddr[13:0],s_axi_tx_jesd_awprot[2:0],s_axi_tx_jesd_awready,s_axi_tx_jesd_awvalid,s_axi_tx_jesd_bready,s_axi_tx_jesd_bresp[1:0],s_axi_tx_jesd_bvalid,s_axi_tx_jesd_rdata[31:0],s_axi_tx_jesd_rready,s_axi_tx_jesd_rresp[1:0],s_axi_tx_jesd_rvalid,s_axi_tx_jesd_wdata[31:0],s_axi_tx_jesd_wready,s_axi_tx_jesd_wstrb[3:0],s_axi_tx_jesd_wvalid,s_axi_tx_tpl_araddr[12:0],s_axi_tx_tpl_arprot[2:0],s_axi_tx_tpl_arready,s_axi_tx_tpl_arvalid,s_axi_tx_tpl_awaddr[12:0],s_axi_tx_tpl_awprot[2:0],s_axi_tx_tpl_awready,s_axi_tx_tpl_awvalid,s_axi_tx_tpl_bready,s_axi_tx_tpl_bresp[1:0],s_axi_tx_tpl_bvalid,s_axi_tx_tpl_rdata[31:0],s_axi_tx_tpl_rready,s_axi_tx_tpl_rresp[1:0],s_axi_tx_tpl_rvalid,s_axi_tx_tpl_wdata[31:0],s_axi_tx_tpl_wready,s_axi_tx_tpl_wstrb[3:0],s_axi_tx_tpl_wvalid,tx_clkgen_ref,tx_data_0_n,tx_data_0_p,tx_data_1_n,tx_data_1_p,tx_data_2_n,tx_data_2_p,tx_data_3_n,tx_data_3_p,tx_linkclk,tx_ref_clk_0,tx_sync[0:0],tx_sysref_0,txoutclk" */;
  output [31:0]adc_data_0;
  output [31:0]adc_data_1;
  output [31:0]adc_data_2;
  output [31:0]adc_data_3;
  input adc_dovf;
  output [0:0]adc_enable_0;
  output [0:0]adc_enable_1;
  output [0:0]adc_enable_2;
  output [0:0]adc_enable_3;
  output [0:0]adc_valid_0;
  output [0:0]adc_valid_1;
  output [0:0]adc_valid_2;
  output [0:0]adc_valid_3;
  input [31:0]dac_data_0;
  input [31:0]dac_data_1;
  input [31:0]dac_data_2;
  input [31:0]dac_data_3;
  input dac_dunf;
  output [0:0]dac_enable_0;
  output [0:0]dac_enable_1;
  output [0:0]dac_enable_2;
  output [0:0]dac_enable_3;
  output [0:0]dac_valid_0;
  output [0:0]dac_valid_1;
  output [0:0]dac_valid_2;
  output [0:0]dac_valid_3;
  input rx_clkgen_ref;
  input rx_data_0_n;
  input rx_data_0_p;
  input rx_data_1_n;
  input rx_data_1_p;
  input rx_data_2_n;
  input rx_data_2_p;
  input rx_data_3_n;
  input rx_data_3_p;
  output rx_linkclk;
  input rx_ref_clk_0;
  output [0:0]rx_sync;
  input rx_sysref_0;
  output rxoutclk;
  input s_axi_aclk;
  input s_axi_aresetn;
  input [15:0]s_axi_rx_adxcvr_araddr;
  input [2:0]s_axi_rx_adxcvr_arprot;
  output s_axi_rx_adxcvr_arready;
  input s_axi_rx_adxcvr_arvalid;
  input [15:0]s_axi_rx_adxcvr_awaddr;
  input [2:0]s_axi_rx_adxcvr_awprot;
  output s_axi_rx_adxcvr_awready;
  input s_axi_rx_adxcvr_awvalid;
  input s_axi_rx_adxcvr_bready;
  output [1:0]s_axi_rx_adxcvr_bresp;
  output s_axi_rx_adxcvr_bvalid;
  output [31:0]s_axi_rx_adxcvr_rdata;
  input s_axi_rx_adxcvr_rready;
  output [1:0]s_axi_rx_adxcvr_rresp;
  output s_axi_rx_adxcvr_rvalid;
  input [31:0]s_axi_rx_adxcvr_wdata;
  output s_axi_rx_adxcvr_wready;
  input [3:0]s_axi_rx_adxcvr_wstrb;
  input s_axi_rx_adxcvr_wvalid;
  input [15:0]s_axi_rx_clkgen_araddr;
  input [2:0]s_axi_rx_clkgen_arprot;
  output s_axi_rx_clkgen_arready;
  input s_axi_rx_clkgen_arvalid;
  input [15:0]s_axi_rx_clkgen_awaddr;
  input [2:0]s_axi_rx_clkgen_awprot;
  output s_axi_rx_clkgen_awready;
  input s_axi_rx_clkgen_awvalid;
  input s_axi_rx_clkgen_bready;
  output [1:0]s_axi_rx_clkgen_bresp;
  output s_axi_rx_clkgen_bvalid;
  output [31:0]s_axi_rx_clkgen_rdata;
  input s_axi_rx_clkgen_rready;
  output [1:0]s_axi_rx_clkgen_rresp;
  output s_axi_rx_clkgen_rvalid;
  input [31:0]s_axi_rx_clkgen_wdata;
  output s_axi_rx_clkgen_wready;
  input [3:0]s_axi_rx_clkgen_wstrb;
  input s_axi_rx_clkgen_wvalid;
  input [13:0]s_axi_rx_jesd_araddr;
  input [2:0]s_axi_rx_jesd_arprot;
  output s_axi_rx_jesd_arready;
  input s_axi_rx_jesd_arvalid;
  input [13:0]s_axi_rx_jesd_awaddr;
  input [2:0]s_axi_rx_jesd_awprot;
  output s_axi_rx_jesd_awready;
  input s_axi_rx_jesd_awvalid;
  input s_axi_rx_jesd_bready;
  output [1:0]s_axi_rx_jesd_bresp;
  output s_axi_rx_jesd_bvalid;
  output [31:0]s_axi_rx_jesd_rdata;
  input s_axi_rx_jesd_rready;
  output [1:0]s_axi_rx_jesd_rresp;
  output s_axi_rx_jesd_rvalid;
  input [31:0]s_axi_rx_jesd_wdata;
  output s_axi_rx_jesd_wready;
  input [3:0]s_axi_rx_jesd_wstrb;
  input s_axi_rx_jesd_wvalid;
  input [12:0]s_axi_rx_tpl_araddr;
  input [2:0]s_axi_rx_tpl_arprot;
  output s_axi_rx_tpl_arready;
  input s_axi_rx_tpl_arvalid;
  input [12:0]s_axi_rx_tpl_awaddr;
  input [2:0]s_axi_rx_tpl_awprot;
  output s_axi_rx_tpl_awready;
  input s_axi_rx_tpl_awvalid;
  input s_axi_rx_tpl_bready;
  output [1:0]s_axi_rx_tpl_bresp;
  output s_axi_rx_tpl_bvalid;
  output [31:0]s_axi_rx_tpl_rdata;
  input s_axi_rx_tpl_rready;
  output [1:0]s_axi_rx_tpl_rresp;
  output s_axi_rx_tpl_rvalid;
  input [31:0]s_axi_rx_tpl_wdata;
  output s_axi_rx_tpl_wready;
  input [3:0]s_axi_rx_tpl_wstrb;
  input s_axi_rx_tpl_wvalid;
  input [15:0]s_axi_tx_adxcvr_araddr;
  input [2:0]s_axi_tx_adxcvr_arprot;
  output s_axi_tx_adxcvr_arready;
  input s_axi_tx_adxcvr_arvalid;
  input [15:0]s_axi_tx_adxcvr_awaddr;
  input [2:0]s_axi_tx_adxcvr_awprot;
  output s_axi_tx_adxcvr_awready;
  input s_axi_tx_adxcvr_awvalid;
  input s_axi_tx_adxcvr_bready;
  output [1:0]s_axi_tx_adxcvr_bresp;
  output s_axi_tx_adxcvr_bvalid;
  output [31:0]s_axi_tx_adxcvr_rdata;
  input s_axi_tx_adxcvr_rready;
  output [1:0]s_axi_tx_adxcvr_rresp;
  output s_axi_tx_adxcvr_rvalid;
  input [31:0]s_axi_tx_adxcvr_wdata;
  output s_axi_tx_adxcvr_wready;
  input [3:0]s_axi_tx_adxcvr_wstrb;
  input s_axi_tx_adxcvr_wvalid;
  input [15:0]s_axi_tx_clkgen_araddr;
  input [2:0]s_axi_tx_clkgen_arprot;
  output s_axi_tx_clkgen_arready;
  input s_axi_tx_clkgen_arvalid;
  input [15:0]s_axi_tx_clkgen_awaddr;
  input [2:0]s_axi_tx_clkgen_awprot;
  output s_axi_tx_clkgen_awready;
  input s_axi_tx_clkgen_awvalid;
  input s_axi_tx_clkgen_bready;
  output [1:0]s_axi_tx_clkgen_bresp;
  output s_axi_tx_clkgen_bvalid;
  output [31:0]s_axi_tx_clkgen_rdata;
  input s_axi_tx_clkgen_rready;
  output [1:0]s_axi_tx_clkgen_rresp;
  output s_axi_tx_clkgen_rvalid;
  input [31:0]s_axi_tx_clkgen_wdata;
  output s_axi_tx_clkgen_wready;
  input [3:0]s_axi_tx_clkgen_wstrb;
  input s_axi_tx_clkgen_wvalid;
  input [13:0]s_axi_tx_jesd_araddr;
  input [2:0]s_axi_tx_jesd_arprot;
  output s_axi_tx_jesd_arready;
  input s_axi_tx_jesd_arvalid;
  input [13:0]s_axi_tx_jesd_awaddr;
  input [2:0]s_axi_tx_jesd_awprot;
  output s_axi_tx_jesd_awready;
  input s_axi_tx_jesd_awvalid;
  input s_axi_tx_jesd_bready;
  output [1:0]s_axi_tx_jesd_bresp;
  output s_axi_tx_jesd_bvalid;
  output [31:0]s_axi_tx_jesd_rdata;
  input s_axi_tx_jesd_rready;
  output [1:0]s_axi_tx_jesd_rresp;
  output s_axi_tx_jesd_rvalid;
  input [31:0]s_axi_tx_jesd_wdata;
  output s_axi_tx_jesd_wready;
  input [3:0]s_axi_tx_jesd_wstrb;
  input s_axi_tx_jesd_wvalid;
  input [12:0]s_axi_tx_tpl_araddr;
  input [2:0]s_axi_tx_tpl_arprot;
  output s_axi_tx_tpl_arready;
  input s_axi_tx_tpl_arvalid;
  input [12:0]s_axi_tx_tpl_awaddr;
  input [2:0]s_axi_tx_tpl_awprot;
  output s_axi_tx_tpl_awready;
  input s_axi_tx_tpl_awvalid;
  input s_axi_tx_tpl_bready;
  output [1:0]s_axi_tx_tpl_bresp;
  output s_axi_tx_tpl_bvalid;
  output [31:0]s_axi_tx_tpl_rdata;
  input s_axi_tx_tpl_rready;
  output [1:0]s_axi_tx_tpl_rresp;
  output s_axi_tx_tpl_rvalid;
  input [31:0]s_axi_tx_tpl_wdata;
  output s_axi_tx_tpl_wready;
  input [3:0]s_axi_tx_tpl_wstrb;
  input s_axi_tx_tpl_wvalid;
  input tx_clkgen_ref;
  output tx_data_0_n;
  output tx_data_0_p;
  output tx_data_1_n;
  output tx_data_1_p;
  output tx_data_2_n;
  output tx_data_2_p;
  output tx_data_3_n;
  output tx_data_3_p;
  output tx_linkclk;
  input tx_ref_clk_0;
  input [0:0]tx_sync;
  input tx_sysref_0;
  output txoutclk;
endmodule
