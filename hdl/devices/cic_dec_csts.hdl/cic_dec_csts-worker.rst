.. cic_dec_csts HDL worker


.. _cic_dec_csts-HDL-worker:


``cic_dec_csts`` HDL Worker
===========================

Detail
------
The HDL implementation of ``cic_dec_csts`` device workers, which implements the CIC decimator primitive, with two instances declared, one for the real data values and the second for the imaginary data values.  

This device worker implements a **BY-PASS Mode**: ``down_sample_factor`` **= 1**.


.. ocpi_documentation_worker::

   VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)

   ocpi_buffer_size_output: OpenCPI Framework Property
   ocpi_blocked_output: OpenCPI Framework Property
   ocpi_max_latency_output: OpenCPI Framework Property
   ocpi_latency_output: OpenCPI Framework Property
   ocpi_messages_output: OpenCPI Framework Property
   ocpi_bytes_output: OpenCPI Framework Property


   input: Input Data Port
   output: Output Data Port

.. Utilization
   -----------
   .. ocpi_documentation_utilization::
