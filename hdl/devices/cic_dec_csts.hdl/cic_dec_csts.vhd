-- THIS FILE WAS ORIGINALLY GENERATED ON Wed May 25 10:13:22 2022 EDT
-- BASED ON THE FILE: cic_dec_csts.xml
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cic_dec;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of worker is

  -- Set input and output sizes to 16 bit
  constant input_word_size_c  : integer := 16;
  constant output_word_size_c : integer := 16;
  -- Sets the size of the internal registers used to store the cic calculations
  constant cic_word_size_c    : integer := to_integer(cic_reg_size);
  -- Sets number of comb and integrator stages
  constant comb_stages_c      : integer := to_integer(cic_order);
  constant int_stages_c       : integer := to_integer(cic_order);
  constant diff_delay_c       : integer := to_integer(cic_diff_delay);
  -- Set delay through CIC and rounding primitive
  constant delay_c            : integer := comb_stages_c + int_stages_c + 2;
  -- Interface signals
  signal enable               : std_logic;
  signal input_interface_in   : worker_input_in_t;
  signal input_interface_out  : worker_input_out_t;
  signal output_interface     : worker_output_out_t;
  signal input_hold           : std_logic;
  signal data_valid_in        : std_logic;
  signal data_in_i            : signed(input_word_size_c - 1 downto 0);
  signal data_in_q            : signed(input_word_size_c - 1 downto 0);
  signal output               : worker_output_out_t;

  -- CIC signals
  signal cic_data_valid     : std_logic;
  signal data_out_i         : signed(cic_word_size_c - 1 downto 0);
  signal data_out_q         : signed(cic_word_size_c - 1 downto 0);
  signal rounded_data_out_i : signed(output_word_size_c - 1 downto 0);
  signal rounded_data_out_q : signed(output_word_size_c - 1 downto 0);
  signal scale_factor       : integer range 0 to cic_word_size_c - 1;
  signal round_data_valid   : std_logic;
  signal processed_data     : std_logic_vector(output_out.data'high downto 0);

  signal input_out_take       : std_logic;
  signal output_out_data      : std_logic_vector(2*output_word_size_c-1 downto 0);
  signal output_out_valid     : std_logic;

begin

  ------------------------------------------------------------------------------
  -- Flush inject
  ------------------------------------------------------------------------------
  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take (request) data when output port ready.
  flush_insert_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output)/4
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => enable,
      input_in        => input_in,
      flush_length    => props_in.flush_length,
      input_out       => input_interface_out,
      input_interface => input_interface_in
      );

  -- Bypass Mode = Sample_Factor = 1
    input_out_take <= input_in.valid when (props_in.down_sample_factor = 1) else input_interface_out.take;
    input_out.take <= input_out_take;


  ------------------------------------------------------------------------------
  -- Data interface
  ------------------------------------------------------------------------------
  -- CIC processing runs when data output is ready
  enable <= output_in.ready and (not input_hold);

  data_valid_in <= '1' when input_interface_in.opcode = complex_short_timed_sample_sample_op_e
                   and input_interface_in.valid = '1'
                   else '0';

  -- Split I and Q data
  data_in_i <= signed(input_interface_in.data(input_word_size_c - 1 downto 0));
  data_in_q <= signed(input_interface_in.data((2 * input_word_size_c) - 1 downto input_word_size_c));

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the CIC calculation.
  interface_delay_i : entity work.complex_short_down_sampled_protocol_delay
    generic map (
      delay_g         => delay_c,
      data_in_width_g => input_in.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_interface_in,
      processed_stream_in => processed_data,
      processed_mask_in   => round_data_valid,  -- 1 when CIC is outputting data
      output_out          => output_interface,
      input_hold_out      => input_hold
      );

    -- Bypass Mode = Sample_Factor = 1
    output_out_data <= input_in.data when (props_in.down_sample_factor = 1) else output_interface.data;
    output_out_valid <= input_in.valid when (props_in.down_sample_factor = 1) else output_interface.valid;

    output_out.data <= output_out_data;
    output_out.valid <= output_out_valid;
    output_out.som <= input_in.som when (props_in.down_sample_factor = 1) else output_interface.som;
    output_out.eom <= input_in.eom when (props_in.down_sample_factor = 1) else output_interface.eom;
    output_out.eof <= input_in.eof when (props_in.down_sample_factor = 1) else output_interface.eof;
    output_out.opcode <= input_in.opcode when (props_in.down_sample_factor = 1) else output_interface.opcode;
    output_out.byte_enable <= input_in.byte_enable when (props_in.down_sample_factor = 1) else output_interface.byte_enable;
    output_out.give <= '0' when (props_in.down_sample_factor = 1) else output_interface.give;


  -- ---------------------------------------------------------------------------
  -- CIC Decimators
  -- ---------------------------------------------------------------------------
  -- Instantiate two CIC modules for I and Q streams
  cic_module_i : cic_dec
    generic map (
      int_stages_g       => int_stages_c,
      comb_stages_g      => comb_stages_c,
      diff_delay_g       => diff_delay_c,
      input_word_size_g  => input_word_size_c,
      output_word_size_g => cic_word_size_c
      )
    port map (
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      clk_en             => enable,
      data_valid_in      => data_valid_in,
      data_in            => data_in_i,
      down_sample_factor => props_in.down_sample_factor,
      data_valid_out     => cic_data_valid,
      data_out           => data_out_i
      );

  cic_module_q : cic_dec
    generic map (
      int_stages_g       => int_stages_c,
      comb_stages_g      => comb_stages_c,
      diff_delay_g       => diff_delay_c,
      input_word_size_g  => input_word_size_c,
      output_word_size_g => cic_word_size_c
      )
    port map (
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      clk_en             => enable,
      data_valid_in      => data_valid_in,
      data_in            => data_in_q,
      down_sample_factor => props_in.down_sample_factor,
      data_valid_out     => open,
      data_out           => data_out_q
      );

  -- ---------------------------------------------------------------------------
  -- Output rounding and scaling
  -- ---------------------------------------------------------------------------
  -- Round output using half-up adder
  scale_factor <= to_integer(props_in.scale_output);

  halfup_rounder_i : rounding_halfup
    generic map (
      input_width_g  => cic_word_size_c,
      output_width_g => output_word_size_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => enable,
      data_in        => data_out_i,
      data_valid_in  => cic_data_valid,
      binary_point   => scale_factor,
      data_out       => rounded_data_out_i,
      data_valid_out => round_data_valid
      );

  halfup_rounder_q : rounding_halfup
    generic map (
      input_width_g  => cic_word_size_c,
      output_width_g => output_word_size_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => ctl_in.reset,
      clk_en         => enable,
      data_in        => data_out_q,
      data_valid_in  => cic_data_valid,
      binary_point   => scale_factor,
      data_out       => rounded_data_out_q,
      data_valid_out => open
      );

  processed_data <= std_logic_vector(rounded_data_out_q) & std_logic_vector(rounded_data_out_i);

  DEBUG_ILA: if its(VIVADO_ILA_p) generate  

    signal input_data   : std_logic_vector(31 downto 0);
    signal input_vld    : std_logic_vector(0 downto 0);
    signal input_rdy    : std_logic_vector(0 downto 0);
    signal input_take   : std_logic_vector(0 downto 0);
    signal cic_I_in     : std_logic_vector(15 downto 0);
    signal cic_I_out    : std_logic_vector(35 downto 0);
    signal cic_I_vld    : std_logic_vector(0 downto 0);
    signal cic_Q_in     : std_logic_vector(15 downto 0);
    signal cic_Q_vld    : std_logic_vector(0 downto 0);
    signal cic_Q_out    : std_logic_vector(35 downto 0);
    signal output_data  : std_logic_vector(31 downto 0);
    signal output_vld   : std_logic_vector(0 downto 0); 
    signal output_rdy   : std_logic_vector(0 downto 0);
    signal rounded_I    : std_logic_vector(15 downto 0);
    signal rounded_q    : std_logic_vector(15 downto 0);

  begin

    input_data <= input_in.data(31 downto 0);
    input_vld(0) <= input_in.valid;
    input_rdy(0) <= input_in.ready;
    input_take(0) <= input_out_take;
    cic_I_in <= std_logic_vector(data_in_i);
    cic_I_out <= std_logic_vector(data_out_i);
    cic_I_vld(0) <= data_valid_in;
    cic_Q_in <= std_logic_vector(data_in_q);
    cic_Q_out <= std_logic_vector(data_out_q);
    cic_Q_vld(0) <= data_valid_in;
    rounded_I <= std_logic_vector(rounded_data_out_i);
    rounded_Q <= std_logic_vector(rounded_data_out_q);
    output_data <= output_out_data;
    output_vld(0) <= output_out_valid;
    output_rdy(0) <= output_in.ready;

    
    
    ila : work.cic_dec_csts_pkg.ila_0
      port map ( clk => ctl_in.clk,
        probe0 => input_data,
        probe1 => input_vld,
        probe2 => input_rdy,
        probe3 => input_take,
        probe4 => cic_I_in,
        probe5 => cic_I_vld,
        probe6 => cic_Q_in,
        probe7 => cic_Q_vld,
        probe8 => rounded_I,
        probe9 => rounded_Q,
        probe10 => output_data,
        probe11 => output_vld,
        probe12 => output_rdy,
        probe13 => cic_I_out,
        probe14 => cic_Q_out );
  
  end generate;

end rtl;
