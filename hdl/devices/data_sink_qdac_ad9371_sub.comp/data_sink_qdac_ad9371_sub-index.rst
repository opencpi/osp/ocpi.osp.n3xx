.. data_sink_qdac_ad9371_sub documentation

.. _data_sink_qdac_ad9371_sub:


JESD204B qDAC Subdevice Worker (``data_sink_qdac_ad9371_sub``)
==============================================================
OpenCPI qDAC Class Subdevice Worker for Analog Devices AD9371 Transceiver on the ``n310`` platform.


Implementations
---------------
.. ocpi_documentation_implementations:: ../data_sink_qdac_ad9371_sub.hdl


Dependencies
------------
There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``data_sink_qdac_ad9371_sub`` are:

  * Fixed configuration for the N310 AD9371 JESD parameters

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
