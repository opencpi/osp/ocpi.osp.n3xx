.. data_sink_qdac_ad9371_sub HDL worker


.. _data_sink_qdac_ad9371_sub-HDL-worker:


``data_sink_qdac_ad9371_sub`` HDL Worker
========================================


Detail
------
HDL implementation of ``data_sink_qdac_ad9371_sub`` subdevice worker that handles data formatting and mapping between the qDAC Class Worker (``n310_qdac_csts``) and the JESD204B Interface (``adrv9371_jesd_2tx_2rx``).  This device worker supports up to 2 qDAC workers and mapping to 4 JESD lanes.  A qDAC worker is mapped to 2 JESD lanes for data transfer to the AD9371 transceiver.  Data mapping and masking is supported as run-time properties.  

.. ocpi_documentation_worker::

   LOOPBACK_p: Build-time parameter for Baseband Loopback Mode
   VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)

   channels_are_swapped: Channel Mapping: ``true`` = Channel1 to JESD Lane0/1, Channel0 to JESD Lane2/3, ``false`` = Channel0 to JESD Lane0/1 and Channel1 to Lane2/3.
   lanes_are_swapped: JESD Lane Mapping: ``true`` = JESD Lane0 is swapped with JESD Lane1, JESD Lane2 is swapped with JESD Lane3, ``false`` = JESD Lanes are not swapped.
   mask_ch0i: I-Bit Masking for Channel0
   mask_ch0q: Q-Bit Masking for Channel0
   mask_ch1i: I-Bit Masking for Channel1
   mask_ch1q: Q-Bit Masking for Channel1

   on_off0: Channel 0 Tx Event Port (not connected)
   on_off1: Channel 1 Tx Event Port (not connected)

   dev_qDAC0_csts: Channel 0 Data Interface
   dev_qDAC1_csts: Channel 1 Data Interface
   jesd_data: JESD Lane Interface
   tx_ctl: Control interface for ``n310_gpio`` device workers.


.. Utilization
   -----------
   .. ocpi_documentation_utilization::
