library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;

library ocpi; use ocpi.types.all;
library util; use util.util.all;
library cdc; use cdc.all;
library protocol; use protocol.all;

architecture rtl of worker is

-- JESD INTERFACE PARAMETERS in AD9371 UG-992
-- N = converter resolution                  for ad9371 it is 14 or 16
-- CS = # of control bits per sample         for ad9371 it is 2
-- N' = Total # of bits per sample           for ad9371 it is 16
-- C = converter
-- M = # of converters                       for ad9371 it is 2 or 4
-- i = converter #
-- S = samples per converter per frame cycle for ad9371 it is 1
-- j = sample #

    -- width for each lane of the jesd
    constant jesd_lane_width : positive := 32;
    -- constant dac_width : positive := 16;

    -- no clock domain / static signals
    signal r1_worker_present  : std_logic;
    signal r2_worker_present  : std_logic;
    -- WCI (control) clock domain signals

    signal lane0_data : std_logic_vector(jesd_lane_width-1 downto 0) := (others => '0');
    signal lane1_data : std_logic_vector(jesd_lane_width-1 downto 0) := (others => '0');
    signal lane2_data : std_logic_vector(jesd_lane_width-1 downto 0) := (others => '0');
    signal lane3_data : std_logic_vector(jesd_lane_width-1 downto 0) := (others => '0');

    -- Keep 16 bits for easy shifting
    -- If dac width is less than 16 there will be 0's in the other bits
    -- Need qdac to have DAC_OUTPUT_IS _LSB_OF_IN_PORT to be true
    signal ch0i : std_logic_vector(15 downto 0) := (others => '0');
    signal ch0q : std_logic_vector(15 downto 0) := (others => '0');
    signal ch0_i : std_logic_vector(15 downto 0) := (others => '0');
    signal ch0_q : std_logic_vector(15 downto 0) := (others => '0');

    signal ch1i : std_logic_vector(15 downto 0) := (others => '0');
    signal ch1q : std_logic_vector(15 downto 0) := (others => '0');
    signal ch1_i : std_logic_vector(15 downto 0) := (others => '0');
    signal ch1_q : std_logic_vector(15 downto 0) := (others => '0');


    signal wsi_in0_opcode : protocol.tx_event.opcode_t
                            := protocol.tx_event.TXOFF;
    signal wsi_in1_opcode : protocol.tx_event.opcode_t
                            := protocol.tx_event.TXOFF;
    signal wsi_in0_demarshaller_oprotocol : protocol.tx_event.protocol_t
                                            := protocol.tx_event.PROTOCOL_ZERO;
    signal wsi_in1_demarshaller_oprotocol : protocol.tx_event.protocol_t
                                            := protocol.tx_event.PROTOCOL_ZERO;

    signal wci_channels_are_swapped_slv : std_logic_vector(0 downto 0);
    signal dac_channels_are_swapped_slv : std_logic_vector(0 downto 0);
    signal dac_channels_are_swapped : std_logic;

    signal wci_lanes_are_swapped_slv : std_logic_vector(0 downto 0);
    signal dac_lanes_are_swapped_slv : std_logic_vector(0 downto 0);
    signal dac_lanes_are_swapped : std_logic;

    signal jesd_data_out_dac_data_0 : std_logic_vector(31 downto 0);
    signal jesd_data_out_dac_data_1 : std_logic_vector(31 downto 0);
    signal jesd_data_out_dac_data_2 : std_logic_vector(31 downto 0);
    signal jesd_data_out_dac_data_3 : std_logic_vector(31 downto 0);
    signal lane0_enable : std_logic;
    signal lane1_enable : std_logic;
    signal lane2_enable : std_logic;
    signal lane3_enable : std_logic;

    signal qdac0_lane_enable  : std_logic;
    signal qdac1_lane_enable  : std_logic;
    signal qdac0_tx_enable : std_logic;
    signal qdac1_tx_enable : std_logic;

    signal tx_ctl_out_tx0 : std_logic;
    signal tx_ctl_out_tx1 : std_logic;

    signal dev_qdac0_clk : std_logic;
    signal dev_qdac0_data_i : std_logic_vector(15 downto 0);
    signal dev_qdac0_data_q : std_logic_vector(15 downto 0);
    signal dev_qdac0_valid : std_logic;
    signal dev_qdac0_present : std_logic;
    signal dev_qdac0_enable : std_logic;

    signal dev_qdac1_clk : std_logic;
    signal dev_qdac1_data_i: std_logic_vector(15 downto 0);
    signal dev_qdac1_data_q: std_logic_vector(15 downto 0);
    signal dev_qdac1_valid : std_logic;
    signal dev_qdac1_present : std_logic;
    signal dev_qdac1_enable : std_logic;

    signal mask_ch0i : std_logic_vector(15 downto 0);
    signal mask_ch0q : std_logic_vector(15 downto 0);
    signal mask_ch1i : std_logic_vector(15 downto 0);
    signal mask_ch1q : std_logic_vector(15 downto 0);    


begin



    -- sync (WCI clock domain) -> (DAC clock domain)
    -- note that we don't care if WCI clock is much faster and bits are
    -- dropped - props_in.channels_are_swapped is a configuration bit which is
    -- expected to change very rarely in relation to either clock
    wci_channels_are_swapped_slv(0) <= '1' when (props_in.channels_are_swapped = btrue) else '0';

    chan_swap_sync : util.util.SyncRegister
    generic map(width   => 1,
                init    => 0)
    port map   (sCLK    => ctl_in.clk,
                sRST    => ctl_in.reset,
                dCLK    => jesd_data_in.devclk,
                sEN     => '1',
                sRDY    => open,
                sD_IN   => wci_channels_are_swapped_slv,
                dD_OUT  => dac_channels_are_swapped_slv);

    dac_channels_are_swapped <= dac_channels_are_swapped_slv(0);



    dev_qDAC0_csts_out.clk      <= dev_qdac0_clk when (dac_channels_are_swapped = bfalse) else dev_qdac1_clk;
    dev_qdac0_data_i            <= dev_qDAC0_csts_in.data_i when (dac_channels_are_swapped = bfalse) else dev_qDAC1_csts_in.data_i;
    dev_qdac0_data_q            <= dev_qDAC0_csts_in.data_q when (dac_channels_are_swapped = bfalse) else dev_qDAC1_csts_in.data_q;
    dev_qdac0_valid             <= dev_qDAC0_csts_in.valid when (dac_channels_are_swapped = bfalse) else dev_qDAC1_csts_in.valid;
    dev_qdac0_present           <= dev_qDAC0_csts_in.present when (dac_channels_are_swapped = bfalse) else dev_qDAC1_csts_in.present;
    dev_qDAC0_csts_out.enable   <= dev_qdac0_enable when (dac_channels_are_swapped = bfalse) else dev_qdac1_enable;
    dev_qDAC1_csts_out.clk      <= dev_qdac1_clk when (dac_channels_are_swapped = bfalse) else dev_qdac0_clk;
    dev_qdac1_data_i            <= dev_qDAC1_csts_in.data_i when (dac_channels_are_swapped = bfalse) else dev_qDAC0_csts_in.data_i;
    dev_qdac1_data_q            <= dev_qDAC1_csts_in.data_q when (dac_channels_are_swapped = bfalse) else dev_qDAC0_csts_in.data_q;
    dev_qdac1_valid             <= dev_qDAC1_csts_in.valid when (dac_channels_are_swapped = bfalse) else dev_qDAC0_csts_in.valid;
    dev_qdac1_present           <= dev_qDAC1_csts_in.present when (dac_channels_are_swapped = bfalse) else dev_qDAC0_csts_in.present;
    dev_qDAC1_csts_out.enable   <= dev_qdac1_enable when (dac_channels_are_swapped = bfalse) else dev_qdac0_enable;


    -- assert dac_width <= 16 report "data width too big" severity failure;


    -- Signal from the qdac works telling if they are there in the assembly
    r1_worker_present <= dev_qdac0_present;
    r2_worker_present <= dev_qdac1_present;

    -- Enables for the tx channels
    tx_ctl_out_tx0 <= wsi_in0_demarshaller_oprotocol.txOn and not wsi_in0_demarshaller_oprotocol.txOff;
    tx_ctl_out_tx1 <= wsi_in1_demarshaller_oprotocol.txOn and not wsi_in1_demarshaller_oprotocol.txOff;

    tx_ctl_out.tx0 <= tx_ctl_out_tx0 when (dac_channels_are_swapped = bfalse) else tx_ctl_out_tx1;
    tx_ctl_out.tx1 <= tx_ctl_out_tx1 when (dac_channels_are_swapped = bfalse) else tx_ctl_out_tx0;

    -- Wait until JESD lanes are ready and DRC FSM = start state
    qdac0_tx_enable <= tx_ctl_out_tx0 or tx_ctl_in.qdac0;
    qdac1_tx_enable <= tx_ctl_out_tx1 or tx_ctl_in.qdac1;

    qdac0_lane_enable <= lane0_enable and lane1_enable and qdac0_tx_enable;
    qdac1_lane_enable <= lane2_enable and lane3_enable and qdac1_tx_enable;

    dev_qdac0_enable <= qdac0_lane_enable;
    dev_qdac1_enable <= qdac1_lane_enable;



    -- sync (WCI clock domain) -> (DAC clock domain)
    wci_lanes_are_swapped_slv(0) <= '1' when (props_in.lanes_are_swapped = btrue) else '0';

    lanes_swap_sync : util.util.SyncRegister
    generic map(width   => 1,
                init    => 0)
    port map   (sCLK    => ctl_in.clk,
                sRST    => ctl_in.reset,
                dCLK    => jesd_data_in.devclk,
                sEN     => '1',
                sRDY    => open,
                sD_IN   => wci_lanes_are_swapped_slv,
                dD_OUT  => dac_lanes_are_swapped_slv);

    dac_lanes_are_swapped <= dac_lanes_are_swapped_slv(0);



    dev_qdac0_clk <= jesd_data_in.devclk;

    mask_ch0i <= from_ushort(props_in.mask_ch0i) when (dac_channels_are_swapped = bfalse) else from_ushort(props_in.mask_ch1i);
    mask_ch0q <= from_ushort(props_in.mask_ch0q) when (dac_channels_are_swapped = bfalse) else from_ushort(props_in.mask_ch1q);

    ch0i <= dev_qdac0_data_i(15 downto 0) and mask_ch0i;
    ch0q <= dev_qdac0_data_q(15 downto 0) and mask_ch0q;

    ch0_i <= ch0i when (dac_lanes_are_swapped = bfalse) else ch0q;
    ch0_q <= ch0q when (dac_lanes_are_swapped = bfalse) else ch0i;



    dev_qdac1_clk <= jesd_data_in.devclk;

    mask_ch1i <= from_ushort(props_in.mask_ch1i) when (dac_channels_are_swapped = bfalse) else from_ushort(props_in.mask_ch0i);
    mask_ch1q <= from_ushort(props_in.mask_ch1q) when (dac_channels_are_swapped = bfalse) else from_ushort(props_in.mask_ch0q);
    
    ch1i <= dev_qdac1_data_i(15 downto 0) and mask_ch1i;
    ch1q <= dev_qdac1_data_q(15 downto 0) and mask_ch1q;

    ch1_i <= ch1i when (dac_lanes_are_swapped = bfalse) else ch1q;
    ch1_q <= ch1q when (dac_lanes_are_swapped = bfalse) else ch1i;



    -- TODO possibly add handling for other configurations
    -- Configuration for jesd interface with 4 lanes and 4 converters
    --L4_M4 : if L = 4 and M = 4 generate

        lane0_enable <= jesd_data_in.dac_valid_0(0) and jesd_data_in.dac_enable_0(0) and r1_worker_present;
        lane1_enable <= jesd_data_in.dac_valid_1(0) and jesd_data_in.dac_enable_1(0) and r1_worker_present;
        lane2_enable <= jesd_data_in.dac_valid_2(0) and jesd_data_in.dac_enable_2(0) and r2_worker_present;
        lane3_enable <= jesd_data_in.dac_valid_3(0) and jesd_data_in.dac_enable_3(0) and r2_worker_present;

        -- Shift data into the lane
        process(jesd_data_in.devclk)
            constant zero : std_logic_vector(15 downto 0) := (others => '0');
        begin
            if rising_edge(jesd_data_in.devclk) then
                if (dev_qdac0_valid = '1') then
                    lane0_data(31 downto 16)    <= ch0_i;
                    lane0_data(15 downto 0)     <= lane0_data(31 downto 16);

                    lane1_data(31 downto 16)    <= ch0_q;
                    lane1_data(15 downto 0)     <= lane1_data(31 downto 16);
                else
                    lane0_data(31 downto 16)    <= zero;
                    lane0_data(15 downto 0)     <= lane0_data(31 downto 16);

                    lane1_data(31 downto 16)    <= zero;
                    lane1_data(15 downto 0)     <= lane1_data(31 downto 16);
                end if;

                if (dev_qdac1_valid = '1') then
                    lane2_data(31 downto 16)    <= ch1_i;
                    lane2_data(15 downto 0)     <= lane2_data(31 downto 16);

                    lane3_data(31 downto 16)    <= ch1_q;
                    lane3_data(15 downto 0)     <= lane3_data(31 downto 16);
                else
                    lane2_data(31 downto 16)    <= zero;
                    lane2_data(15 downto 0)     <= lane2_data(31 downto 16);

                    lane3_data(31 downto 16)    <= zero;
                    lane3_data(15 downto 0)     <= lane3_data(31 downto 16);
                end if;
            end if;
        end process;

    --end generate L4_M4;

    LOOPBACK: if (LOOPBACK_P = '1') generate
        process (jesd_data_in.devclk)
        begin
            if (rising_edge(jesd_data_in.devclk)) then
                jesd_data_out.loopback_dac0_vld <= dev_qdac0_valid;
                jesd_data_out.loopback_dac1_vld <= dev_qdac1_valid;
            end if;
        end process;
    end generate;


    -- Set this to 0 as it is meant for a dac fifo underflow
    jesd_data_out.dac_dunf <= '0';

    -- Supply good data to the lanes when the enable is true
    jesd_data_out_dac_data_0 <= lane0_data when lane0_enable = '1' else (others => '0');
    jesd_data_out_dac_data_1 <= lane1_data when lane1_enable = '1' else (others => '0');
    jesd_data_out_dac_data_2 <= lane2_data when lane2_enable = '1' else (others => '0');
    jesd_data_out_dac_data_3 <= lane3_data when lane3_enable = '1' else (others => '0');

    jesd_data_out.dac_data_0 <= jesd_data_out_dac_data_0;
    jesd_data_out.dac_data_1 <= jesd_data_out_dac_data_1;
    jesd_data_out.dac_data_2 <= jesd_data_out_dac_data_2;
    jesd_data_out.dac_data_3 <= jesd_data_out_dac_data_3;



    wsi_in0_opcode <=
        protocol.tx_event.TXOFF when on_off0_in.opcode = tx_event_txOff_op_e else
        protocol.tx_event.TXON  when on_off0_in.opcode = tx_event_txOn_op_e  else
        protocol.tx_event.TXOFF;
    
    on_off0_demarshaller : protocol.tx_event.tx_event_demarshaller
        port map(
        clk       => on_off0_in.clk, -- same as the dac clock
        rst       => on_off0_in.reset,
        -- INPUT
        iready    => on_off0_in.ready,
        iopcode   => wsi_in0_opcode,
        ieof      => on_off0_in.eof,
        itake     => on_off0_out.take,
        -- OUTPUT
        oprotocol => wsi_in0_demarshaller_oprotocol,
        ordy      => '1');

    wsi_in1_opcode <= 
        protocol.tx_event.TXOFF when on_off1_in.opcode = tx_event_txOff_op_e else
        protocol.tx_event.TXON  when on_off1_in.opcode = tx_event_txOn_op_e  else
        protocol.tx_event.TXOFF;

    on_off1_demarshaller : protocol.tx_event.tx_event_demarshaller
        port map(
        clk       => on_off1_in.clk, -- same as the dac clock
        rst       => on_off1_in.reset,
        -- INPUT
        iready    => on_off1_in.ready,
        iopcode   => wsi_in1_opcode,
        ieof      => on_off1_in.eof,
        itake     => on_off1_out.take,
        -- OUTPUT
        oprotocol => wsi_in1_demarshaller_oprotocol,
        ordy      => '1');



  DEBUG: if its(VIVADO_ILA_p) generate  

    signal qDAC0_enable               : std_logic_vector(0 downto 0);
    signal qDAC0_data_i               : std_logic_vector(15 downto 0);
    signal qDAC0_data_q               : std_logic_vector(15 downto 0);
    signal jesd_data_out_dac_data0    : std_logic_vector(31 downto 0);
    signal jesd_data_out_dac_data1    : std_logic_vector(31 downto 0);
    signal jesd_data_in_dac_enable_0  : std_logic_vector(0 downto 0);
    signal qDAC1_enable               : std_logic_vector(0 downto 0);
    signal qDAC1_data_i               : std_logic_vector(15 downto 0);
    signal qDAC1_data_q               : std_logic_vector(15 downto 0);
    signal jesd_data_out_dac_data2    : std_logic_vector(31 downto 0);
    signal jesd_data_out_dac_data3    : std_logic_vector(31 downto 0);
    signal jesd_data_in_dac_enable_2  : std_logic_vector(0 downto 0);

  begin

    qDAC0_enable(0) <= dev_qdac0_enable;
    qDAC0_data_i <= dev_qdac0_data_i;
    qDAC0_data_q <= dev_qdac0_data_q; 
    jesd_data_out_dac_data0 <= jesd_data_out_dac_data_0; 
    jesd_data_out_dac_data1 <= jesd_data_out_dac_data_1;
    jesd_data_in_dac_enable_0(0) <= jesd_data_in.dac_enable_0(0);
    qDAC1_enable(0) <= dev_qdac1_enable;
    qDAC1_data_i <= dev_qdac1_data_i;
    qDAC1_data_q <= dev_qdac1_data_q;
    jesd_data_out_dac_data2 <= jesd_data_out_dac_data_2; 
    jesd_data_out_dac_data3 <= jesd_data_out_dac_data_3;
    jesd_data_in_dac_enable_2(0) <= jesd_data_in.dac_enable_2(0);

    
    qdac_sub_ila : work.qdac_sub_ila_pkg.ila_0
      port map ( clk => jesd_data_in.devclk,
        probe0 => qDAC0_enable,
        probe1 => qDAC0_data_q,
        probe2 => qDAC0_data_i,
        probe3 => jesd_data_out_dac_data0,
        probe4 => jesd_data_out_dac_data1,
        probe5 => jesd_data_in_dac_enable_0,
        probe6 => qDAC1_enable,
        probe7 => qDAC1_data_q,
        probe8 => qDAC1_data_i,
        probe9 => jesd_data_out_dac_data2,
        probe10 => jesd_data_out_dac_data3,
        probe11 => jesd_data_in_dac_enable_2 );  
  
  end generate;

end architecture rtl;
