create_clock -period 6.510 -name data_clock -waveform {0.000 3.255} [get_pins worker/adc_clk]
create_clock -period 10.000 -name control_clock -waveform {0.000 5.000} [get_pins {worker/ctl_in[clk]}]
set_clock_groups -name cdc_data_ctl -asynchronous -group [get_clocks data_clock] -group [get_clocks control_clock]
