.. data_src_qadc_ad9371_sub HDL worker


.. _data_src_qadc_ad9371_sub-HDL-worker:


``data_src_qadc_ad9371_sub`` HDL Worker
=======================================

Detail
------
HDL implementation of ``data_src_qadc_ad9371_sub`` subdevice worker that handles data formatting and mapping between the qADC Class Worker (``n310_qadc_csts``) and the JESD204B Interface (``adrv9371_jesd_2tx_2rx``).  This device worker supports up to 2 qADC workers and mapping to 4 JESD lanes.  A qADC worker is mapped from 2 JESD lanes interfacing with the AD9371 transceiver.  Data mapping and masking is supported as run-time properties.  

.. ocpi_documentation_worker::

   LOOPBACK_p: Build-time parameter for Baseband Loopback Mode
   VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)

   channels_are_swapped: Channel Mapping: ``true`` = JESD Lane0/1 to Channel1, JESD Lane2/3 to Channel0, ``false`` = JESD Lane0/1 to Channel0 and JESD Lane2/3 to Channel1.
   lanes_are_swapped: JESD Lane Mapping: ``true`` = JESD Lane0 is swapped with JESD Lane1, JESD Lane2 is swapped with JESD Lane3, ``false`` = JESD Lanes are not swapped.
   mask_ln0: Bit masking for JESD Lane0.
   mask_ln1: Bit masking for JESD Lane1.
   mask_ln2: Bit masking for JESD Lane2.
   mask_ln3: Bit masking for JESD Lane3.

   dev_qADC0_csts: Channel 0 Data Interface
   dev_qADC1_csts: Channel 1 Data Interface
   jesd_data: JESD Lane Interface
   rx_ctl: Control interface for  ``n310_gpio`` device workers.


.. Utilization
   -----------
   .. ocpi_documentation_utilization::
