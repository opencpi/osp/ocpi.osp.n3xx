library ieee; use ieee.std_logic_1164.all; use ieee.numeric_std.all;

library ocpi; use ocpi.types.all;
library util; use util.util.all;
library cdc; use cdc.all;
library work;

architecture rtl of worker is

-- JESD INTERFACE PARAMETERS in AD9371 UG-992
-- N = converter resolution                  for ad9371 it is 14
-- CS = # of control bits per sample         for ad9371 it is 2
-- N' = Total # of bits per sample           for ad9371 it is 16
-- C = converter
-- M = # of converters                       for ad9371 it is 2 or 4
-- i = converter #
-- S = samples per converter per frame cycle for ad9371 it is 1
-- j = sample #

    constant jesd_lane_width : positive := 32;
    constant adc_width : positive := 16;
    constant num_lanes : positive := 4;

    -- no clock domain / static signals
    signal r0_worker_present  : std_logic;
    signal r1_worker_present  : std_logic;
    -- WCI (control) clock domain signals
    signal wci_r0_samps_dropped_clear : std_logic;
    signal wci_r1_samps_dropped_clear : std_logic;

    signal wci_channels_are_swapped_slv : std_logic_vector(0 downto 0);
    signal adc_channels_are_swapped_slv : std_logic_vector(0 downto 0);
    signal adc_channels_are_swapped : std_logic;
    signal wci_lanes_are_swapped_slv : std_logic_vector(0 downto 0);
    signal adc_lanes_are_swapped_slv : std_logic_vector(0 downto 0);
    signal adc_lanes_are_swapped : std_logic;

    signal adc_r0_samps_dropped : std_logic;
    signal adc_r1_samps_dropped : std_logic;  

    type jesd_lanes is array (0 to num_lanes-1) of std_logic_vector(31 downto 0);
    type samp_lanes is array (0 to num_lanes-1) of std_logic_vector(15 downto 0);
    signal jesd_lane_data : jesd_lanes;
    signal samp_lane_data : samp_lanes;

    signal lane_in_vlds  : std_logic_vector(0 to num_lanes-1);
    signal lane_out_vlds : std_logic_vector(0 to num_lanes-1);

    signal rx_ctl_out_rx0  : std_logic := '0';
    signal rx_ctl_out_rx1  : std_logic := '0';
    signal rx_enable0 : std_logic;
    signal rx_enable1 : std_logic;

    signal qadc0_rx_enable : std_logic;
    signal qadc1_rx_enable : std_logic;

    signal dev_qadc0_present : std_logic;
    signal dev_qadc0_clk : std_logic;
    signal dev_qadc0_valid : std_logic;
    signal dev_qadc1_present : std_logic;
    signal dev_qadc1_clk : std_logic;
    signal dev_qadc1_valid : std_logic;

    signal dev_lane0_data : std_logic_vector(15 downto 0);
    signal dev_lane1_data : std_logic_vector(15 downto 0);
    signal dev_lane2_data : std_logic_vector(15 downto 0);
    signal dev_lane3_data : std_logic_vector(15 downto 0);

    signal dev_ADC0_i : std_logic_vector(15 downto 0);
    signal dev_ADC0_q : std_logic_vector(15 downto 0);
    signal dev_ADC1_i : std_logic_vector(15 downto 0);
    signal dev_ADC1_q : std_logic_vector(15 downto 0);



begin

    -- sync (WCI clock domain) -> (ADC clock domain)
    -- note that we don't care if WCI clock is much faster and bits are
    -- dropped - props_in.channels_are_swapped is a configuration bit which is
    -- expected to change very rarely in relation to either clock
    wci_channels_are_swapped_slv(0) <= '1' when (props_in.channels_are_swapped = btrue) else '0';

    chan_swap_sync : util.util.SyncRegister
    generic map(width   => 1,
                init    => 0)
    port map   (sCLK    => ctl_in.clk,
                sRST    => ctl_in.reset,
                dCLK    => jesd_data_in.devclk,
                sEN     => '1',
                sRDY    => open,
                sD_IN   => wci_channels_are_swapped_slv,
                dD_OUT  => adc_channels_are_swapped_slv);

    adc_channels_are_swapped <= adc_channels_are_swapped_slv(0);



    wci_lanes_are_swapped_slv(0) <= '1' when (props_in.lanes_are_swapped = btrue) else '0';

    lanes_swap_sync : util.util.SyncRegister
    generic map(width   => 1,
                init    => 0)
    port map   (sCLK    => ctl_in.clk,
                sRST    => ctl_in.reset,
                dCLK    => jesd_data_in.devclk,
                sEN     => '1',
                sRDY    => open,
                sD_IN   => wci_lanes_are_swapped_slv,
                dD_OUT  => adc_lanes_are_swapped_slv);

    adc_lanes_are_swapped <= adc_lanes_are_swapped_slv(0);



    -- Tells if the qadc worker is in the assembly
    r0_worker_present <= dev_qadc0_present when (adc_channels_are_swapped = bfalse) else dev_qadc1_present;
    r1_worker_present <= dev_qadc1_present when (adc_channels_are_swapped = bfalse) else dev_qadc0_present;


    -- Set to 0, meant for fifo overflow
    jesd_data_out.adc_dovf <= '0';


    -- TODO add support for other jesd configurations
    -- Configuration for jesd interface with 4 lanes and 4 converters
    -- L4_M4 : if num_lanes = 4 and M = 4 generate

        jesd_lane_data(0) <= jesd_data_in.adc_data_0;
        jesd_lane_data(1) <= jesd_data_in.adc_data_1;
        jesd_lane_data(2) <= jesd_data_in.adc_data_2;
        jesd_lane_data(3) <= jesd_data_in.adc_data_3;

        -- Wait until JESD lane are ready and DRC FSM = start state
        NO_LOOPBACK: if (LOOPBACK_p = '0') generate
            rx_enable0 <= rx_ctl_out_rx0 or rx_ctl_in.qadc0;
            rx_enable1 <= rx_ctl_out_rx1 or rx_ctl_in.qadc1;
            qadc0_rx_enable <= rx_enable0 when (adc_channels_are_swapped = bfalse) else rx_enable1;
            qadc1_rx_enable <= rx_enable1 when (adc_channels_are_swapped = bfalse) else rx_enable0;
        end generate;


        LOOPBACK: if (LOOPBACK_p = '1') generate
            qadc0_rx_enable <= '1'; 
            qadc1_rx_enable <= '1'; 
        end generate;


        rx_ctl_out.rx0 <= rx_ctl_out_rx0 when (adc_channels_are_swapped = bfalse) else rx_ctl_out_rx1; 
        rx_ctl_out.rx1 <= rx_ctl_out_rx1 when (adc_channels_are_swapped = bfalse) else rx_ctl_out_rx0;

        lane_in_vlds(0) <= jesd_data_in.adc_enable_0(0) and jesd_data_in.adc_valid_0(0) and qadc0_rx_enable;
        lane_in_vlds(1) <= jesd_data_in.adc_enable_1(0) and jesd_data_in.adc_valid_1(0) and qadc0_rx_enable;
        lane_in_vlds(2) <= jesd_data_in.adc_enable_2(0) and jesd_data_in.adc_valid_2(0) and qadc1_rx_enable;
        lane_in_vlds(3) <= jesd_data_in.adc_enable_3(0) and jesd_data_in.adc_valid_3(0) and qadc1_rx_enable;

        -- Map the 4 lanes to the 2 channels
        lane_mapping : for I in 0 to num_lanes-1 generate
            lane_mapper : entity work.one_32_to_2_16
            port map(
                clock   => jesd_data_in.devclk,
                reset   => ctl_in.reset,
                in_vld  => lane_in_vlds(I),
                din     => jesd_lane_data(I),
                out_vld => lane_out_vlds(I),
                dout    => samp_lane_data(I));
        end generate;
    --end generate;


    -- i and q signal buses driven where each signal bus is MSB-justified
    -- (data_src_qadc.hdl takes care of justification standardization)
    dev_lane0_data(dev_lane0_data'left downto dev_lane0_data'left-adc_width+1) <= samp_lane_data(0)(adc_width-1 downto 0);
    dev_lane0_data(dev_lane0_data'left-adc_width downto 0) <= (others => '0');

    dev_lane1_data(dev_lane1_data'left downto dev_lane1_data'left-adc_width+1) <= samp_lane_data(1)(adc_width-1 downto 0);
    dev_lane1_data(dev_lane1_data'left-adc_width downto 0) <= (others => '0');


    dev_ADC0_i <= (dev_lane0_data and from_ushort(props_in.mask_ln0)) when (adc_lanes_are_swapped = bfalse) 
                    else (dev_lane1_data and from_ushort(props_in.mask_ln1));
    dev_ADC0_q <= (dev_lane1_data and from_ushort(props_in.mask_ln1)) when (adc_lanes_are_swapped = bfalse) 
                    else (dev_lane0_data and from_ushort(props_in.mask_ln0));




    dev_lane2_data(dev_lane2_data'left downto dev_lane2_data'left-adc_width+1) <= samp_lane_data(2)(adc_width-1 downto 0);
    dev_lane2_data(dev_lane2_data'left-adc_width downto 0) <= (others => '0');

    dev_lane3_data(dev_lane3_data'left downto dev_lane3_data'left-adc_width+1) <= samp_lane_data(3)(adc_width-1 downto 0);
    dev_lane3_data(dev_lane3_data'left-adc_width downto 0) <= (others => '0');


    dev_ADC1_i <= (dev_lane2_data and from_ushort(props_in.mask_ln2)) when (adc_lanes_are_swapped = bfalse) 
                    else (dev_lane3_data and from_ushort(props_in.mask_ln3));
    dev_ADC1_q <= (dev_lane3_data and from_ushort(props_in.mask_ln3)) when (adc_lanes_are_swapped = bfalse) 
                    else (dev_lane2_data and from_ushort(props_in.mask_ln2));


    dev_qADC0_csts_out.clk       <= jesd_data_in.devclk;
    dev_qADC0_csts_out.data_i    <= dev_ADC0_i when (adc_channels_are_swapped = bfalse) else dev_ADC1_i;
    dev_qADC0_csts_out.data_q    <= dev_ADC0_q when (adc_channels_are_swapped = bfalse) else dev_ADC1_q;
    dev_qADC0_csts_out.valid     <= lane_out_vlds(0) when (adc_channels_are_swapped = bfalse) else lane_out_vlds(2);
    dev_qadc0_present            <= dev_qadc0_present when (adc_channels_are_swapped = bfalse) else dev_qadc1_present;
    dev_qADC1_csts_out.clk       <= jesd_data_in.devclk;
    dev_qADC1_csts_out.data_i    <= dev_ADC1_i when (adc_channels_are_swapped = bfalse) else dev_ADC0_i;
    dev_qADC1_csts_out.data_q    <= dev_ADC1_q when (adc_channels_are_swapped = bfalse) else dev_ADC0_q;
    dev_qADC1_csts_out.valid     <= lane_out_vlds(2) when (adc_channels_are_swapped = bfalse) else lane_out_vlds(0);
    dev_qadc1_present            <= dev_qadc1_present when (adc_channels_are_swapped = bfalse) else dev_qadc0_present;


    samples_dropped : process(jesd_data_in.devclk)
    begin
        if rising_edge(jesd_data_in.devclk) then
            adc_r0_samps_dropped <= not r0_worker_present;
            adc_r1_samps_dropped <= not r1_worker_present;
        end if;
    end process samples_dropped;


    -- if valid R1 channel samples are received but there is no
    -- qadc worker to ingest them, detect the error
    wci_r0_samps_dropped_clear <= '1'
                                    when (props_in.r0_samps_dropped_written = '1')
                                        and (props_in.r0_samps_dropped = '0')
                                    else '0';
    r0_samps_dropped_sync : util.util.sync_status
        port map   (clk         => ctl_in.clk,
                    reset       => ctl_in.reset,
                    operating   => ctl_in.is_operating,
                    start       => ctl_in.is_operating,
                    clear       => wci_r0_samps_dropped_clear,
                    status      => props_out.r0_samps_dropped,
                    other_clk   => jesd_data_in.devclk,
                    other_reset => open,
                    event       => adc_r0_samps_dropped);

    wci_r1_samps_dropped_clear <= '1'
                                    when (props_in.r1_samps_dropped_written = '1')
                                        and (props_in.r1_samps_dropped = '0')
                                    else '0';

    -- if valid R2 channel samples are received but there is no
    -- qadc worker to ingest them, detect the error
    r1_samps_dropped_sync : util.util.sync_status
        port map  (clk         => ctl_in.clk,
                    reset       => ctl_in.reset,
                    operating   => ctl_in.is_operating,
                    start       => ctl_in.is_operating,
                    clear       => wci_r1_samps_dropped_clear,
                    status      => props_out.r1_samps_dropped,
                    other_clk   => jesd_data_in.devclk,
                    other_reset => open,
                    event       => adc_r1_samps_dropped);



    -- Vivadio ILA Intergation
    DEBUG: if its(VIVADO_ILA_p) generate  

        signal jesd_data_in_adc_enable0 : std_logic_vector(0 downto 0);
        signal jesd_data_in_adc_enable2 : std_logic_vector(0 downto 0);
        signal rx_ctl_qadc0_enable      : std_logic_vector(0 downto 0);
        signal rx_ctl_qadc1_enable      : std_logic_vector(0 downto 0);
        signal lane_out0_vlds           : std_logic_vector(0 downto 0);
        signal lane_out2_vlds           : std_logic_vector(0 downto 0);

    begin

        jesd_data_in_adc_enable0(0) <= jesd_data_in.adc_enable_0(0);
        jesd_data_in_adc_enable2(0) <= jesd_data_in.adc_enable_2(0);
        rx_ctl_qadc0_enable(0) <= qadc0_rx_enable;
        rx_ctl_qadc1_enable(0) <= qadc1_rx_enable;
        lane_out0_vlds(0) <= lane_out_vlds(0);
        lane_out2_vlds(0) <= lane_out_vlds(2);

    
    qadc_sub_ila : work.qadc_sub_ila_pkg.ila_0
      port map ( clk => jesd_data_in.devclk,
        probe0 => rx_ctl_qadc0_enable,
        probe1 => jesd_data_in.adc_data_0,
        probe2 => jesd_data_in.adc_data_1,
        probe3 => dev_lane0_data,
        probe4 => dev_lane1_data,
        probe5 => lane_out0_vlds,
        probe6 => rx_ctl_qadc1_enable,
        probe7 => jesd_data_in.adc_data_2,
        probe8 => jesd_data_in.adc_data_3,
        probe9 => dev_lane2_data,
        probe10 => dev_lane3_data,
        probe11 => lane_out2_vlds );  
  
    end generate;

end architecture rtl;