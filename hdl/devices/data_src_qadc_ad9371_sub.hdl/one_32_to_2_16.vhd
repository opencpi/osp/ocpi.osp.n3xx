library ieee;
use ieee.std_logic_1164.all;

-- This is essentially a multiplexer
entity one_32_to_2_16 is
    port (
        clock   : in std_logic;
        reset   : in std_logic;
        in_vld  : in std_logic;
        din     : in std_logic_vector(31 downto 0);
        out_vld : out std_logic;
        dout    : out std_logic_vector(15 downto 0));
end entity one_32_to_2_16;

architecture rtl of one_32_to_2_16 is

    signal cnt : std_logic := '0';

begin

process(clock)

begin

    if (rising_edge(clock)) then
        if (in_vld = '1' ) then
            if (cnt = '0') then
                dout <= din(15 downto 0);
            else
                dout <= din(31 downto 16);
            end if;
            cnt <= not cnt;
        end if;

        --cnt <= not cnt;
        out_vld <= in_vld;
    end if;

end process;

end architecture rtl;