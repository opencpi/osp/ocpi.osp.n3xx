.. drc_n310 documentation

.. _drc_n310:


N310 Digital Radio Controller (``drc_n310``)
============================================
OpenCPI Digital Radio Controller (DRC) for ``n310`` platform.

Design
------
``drc_n310`` architecture consists of the following primary layers:

 * **DRC_N310**: Implementation Layer of the OpenCPI DRC Finite-State Machine (FSM) Operations
 * **DAUGHTERBOARD (DB)**: Configuration Layer for each N310 daughterboard, which utilizes the API controllers for each hardware chip.
 * **PROXY/SLAVE**: Communication Layer between the API controllers and the device workers 

.. _n310_drc_architecture:

.. figure:: ./N310_DRC_Architecture.svg
   :alt: N310 Digital Radio Controller Architecture
   :align: center
   :height: 600px
   :width: 1200px

   N310 Digital Radio Controller Architecture

Interface
---------
.. literalinclude:: ../../../imports/ocpi.platform/components/drc.comp/drc-comp.xml
   :language: xml
   :lines: 24,118-165,238


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::
   :component_spec: ../../../imports/ocpi.platform/components/drc.comp/drc-comp.xml

.. Ports
   ~~~~~
   .. ocpi_documentation_ports::
      :component_spec: ../../../imports/ocpi.platform/components/drc.comp/drc-comp.xml


Implementations
---------------
.. ocpi_documentation_implementations:: ../drc_n310.rcc


Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ocpi.platform - DRC Specifications

.. There is also a dependency on:

   * ``ieee.std_logic_1164``
   * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``drc_n310`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
