
/**
 * @file i2c_n310.hpp
 * @author KBR N310 Team
 * @brief Provides a higher-level API of lower-level I2C calls to access different IO expanders
 *        associated with the N310 and provides specific calls for reading/writing to SFPs.
 * @version 0.1
 * @date 2022-01-31
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#include "i2c.hpp"

#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <bitset>
#include <map>

#ifndef I2C_N310_HPP
#define I2C_N310_HPP

/**
 * @brief I2C_N310 is the class that is utilized for incorporating the high-level I2C
 *        calls in an application or within the DRC.
 * 
 */
class I2C_N310
{
private: 
    IO_Expander PROXY_IO_EXPANDER_SFP1; // channel 1
    IO_Expander PROXY_IO_EXPANDER_SFP0; // channel 2

    IO_Expander TCA6408A_IO_EXPANDER_PWR; // IO_Expander_PWR
    IO_Expander TCA6424A_IO_EXPANDER_CLK; // IO_Expander_CLK
    
    // holding pin configuration for DBA
    std::bitset <8> PINS_CONFIGURATION_TCA6408A_IO_EXPANDER_PWR;
    // same IO expander, but holding the pin configuration for DBB 
    std::bitset <8> PINS_CONFIGURATION_TCA6408A_IO_EXPANDER_PWR_DBB;

    
    std::bitset <28> PINS_CONFIGURATION_TCA6424A_IO_EXPANDER_CLK;
    // PINS_CONFIGURATION_TCA6424A_IO_EXPANDER_CLK
    std::bitset <8> PINS_CONFIGURATION_TCA6424A_IO_EXPANDER_CLK_REG1; // Ports 0 through Port 7
    std::bitset <8> PINS_CONFIGURATION_TCA6424A_IO_EXPANDER_CLK_REG2; // Ports 10 through Ports 17
    std::bitset <8> PINS_CONFIGURATION_TCA6424A_IO_EXPANDER_CLK_REG3; // Ports 20 through Ports 27


    // helper private functions for the public functions
     void helper_pwr_status(__s32 theinputregister, __s32 theoutputregister);
     void helper_pwr_status_dbb(__s32 theinputregister, __s32 theoutputregister);

     void helper_clk_status_reg1(__s32 theinputregister, __s32 theoutputregister);
     void helper_clk_status_reg2(__s32 theinputregister, __s32 theoutputregister);
     void helper_clk_status_reg3(__s32 theinputregister, __s32 theoutputregister);
    
     unsigned long TCA6408A_bits_no_configure_io_expander_for_pins();
     unsigned long TCA6408A_bits_no_configure_io_expander_for_pins_dbb();

    // Deprecated
     unsigned long TCA6424A_bits_no_configure_io_expander_for_pins();
   
   // New Ones
     unsigned long TCA6424A_bits_no_configure_io_expander_for_pins_reg1();
     unsigned long TCA6424A_bits_no_configure_io_expander_for_pins_reg2();
     unsigned long TCA6424A_bits_no_configure_io_expander_for_pins_reg3();

     int usrp_a_i2c_openread(std::string s);
     int usrp_b_i2c_openread(std::string s);

     int clk_i2c_openread_reg1(std::string s);
     int clk_i2c_openread_reg2(std::string s);
     int clk_i2c_openread_reg3(std::string s);

     int usrp_a_i2c_openwrite(std::string s, __u8 thevalue);
     int usrp_b_i2c_openwrite(std::string s, __u8 thevalue);
    
     int clk_i2c_openwrite_reg1(std::string s, __u8 thevalue);
     int clk_i2c_openwrite_reg2(std::string s, __u8 thevalue);
     int clk_i2c_openwrite_reg3(std::string s, __u8 thevalue);

     int usrp_a_i2c_set(std::string s);
     int usrp_b_i2c_set(std::string s);
     int clk_i2c_set(std::string s);
   
     int helper_enable_sfp1(std::string s); // channel 1
     int helper_enable_sfp0(std::string s); // channel 2
    // int helper_enable_clock(std::string s); // channel 3 (i2c-6)

     int helper_read_sfp1(std::string s, __u8 thedataaddress, __u16 * rvalue, __u8 theslaveaddress);
     int helper_read_sfp0(std::string s, __u8 thedataaddress, __u16 * rvalue, __u8 theslaveaddress);
 //    int helper_read_clock(std::string s, __u8 thedataaddress, __u16 * rvalue, __u8 theslaveaddress);

     int helper_write_sfp1(std::string s, __u8 thedataaddress, __u8 thevalue, __u8 theslaveaddress);
     int helper_write_sfp0(std::string s, __u8 thedataaddress, __u8 thevalue, __u8 theslaveaddress);

     int helper_write_clock(std::string s, __u8 thedataaddress, __u8 thevalue, __u8 theslaveaddress);

public:
     I2C_N310(void);
        
     void printRegister(__u16 the_app_return_value, __u16 the_converted_register);
    
     void unbind_n310_registers();
     void disable_usrp_hwd();

     void dba_status();
     void dbb_status();
     void clk_status();

     void dba_pwr_on();
     void dbb_pwr_on();

    // Configuration Options 
     void clk_pwr_25mhz(); // Internal: enable 25 MHz configuration for IO Expander
     void clk_pwr_20mhz(); // GPS: enables 20 MHz and also powers GPS 
                                  // TCA6424A_IO_EXPANDER_CLK_POR() also makes everything 1 (inputs) which is 
                                  // is the behavior on power-up
     void clk_pwr_external_ref();

     void dba_pwr_off();
     void dbb_pwr_off();


     void TCA6408A_IO_EXPANDER_PWR_POR();
     void TCA6408A_IO_EXPANDER_PWR_DBB_POR();
     void TCA6424A_IO_EXPANDER_CLK_POR();

    //SPF functions
    /**
     * @brief enable_sfp1() provides enabling the sfp on channel 1
     * 
     */
     void enable_sfp1(); // channel 1
    
    /**
     * @brief enable_sfp0() provides enabling the sfp on channel 2
     * 
     */
     void enable_sfp0(); // channel 2

    /**
     * @brief read_sfp1(...) provides a rvalue for the data address specified 
     *        and the slave address specified for sfp1.  Be aware that sometimes
     *        sfp1 and sfp0 can potentially be flipped in practice based on other underlying
     *        code.  
     * 
     * @param thedataaddress
     * @param rvalue 
     * @param theslaveaddress
     */
         void read_sfp1(__u8 thedataaddress, __u16 * rvalue, __u8 theslaveaddress = 0x50);
     
     /**
      * @brief read_sfp0(...) provides a rvalue for the data address specified
      *        and the slave address specified for sfp0.  Be aware that sometimes sfp1 and sfp0
      *        can potentially be flipped in practice based on other underlying code.
      * 
      * @param thedataaddress
      * @param rvalue
      * @param theslaveaddress
      */
         void read_sfp0(__u8 thedataaddress, __u16 * rvalue, __u8 theslaveaddress = 0x50);

     /**
      * @brief read_clock(...) provides a rvalue for the data address specified
      *        and the slave address specified for clock.
      * 
      * @param thedataaddress 
      * @param rvalue 
      * @param theslaveaddress 
      */
        // void read_clock(__u8 thedataaddress, __u16 * rvalue, __u8 theslaveaddress);

    /**
     * @brief write_sfp1(...) specifies thevalue to be written to the data address for the
     *        appropriate slave address.
     * 
     * @param thedataaddress 
     * @param thevalue 
     * @param theslaveaddress 
     */
         void write_sfp1(__u8 thedataaddress, __u16 thevalue, __u8 theslaveaddress = 0x50);
        
     /**
      * @brief write_sfp0(...) specifies thevalue to be written to the data address for
      *        the appropriate slave address.
      * 
      * @param thedataaddress 
      * @param thevalue 
      * @param theslaveaddress 
      */
         void write_sfp0(__u8 thedataaddress, __u16 thevalue, __u8 theslaveaddress = 0x50);

          /**
     * @brief write_clock(...) specifies thevalue to be written to the data address for the
     *        appropriate slave address.
     * 
     * @param thedataaddress
     * @param thevalue
     * @param theslaveaddress
     */
         void write_clock(__u8 thedataaddress, __u16 thevalue, __u8 theslaveaddress);
    
};

/////////////////////////////////////////////

#endif
