Readme

adf435x

_lo_enable
_lo_disable


///////////////////////////////////////////////////////

// Figure out the default Don't knows

OWD (can't be in OWS):

<specproperty name='_lo_enable' writesync='1' default='DONTKNOW' />
<specproperty name='_lo_disable' writesync='1' default='DONTKNOW' />


C++:

RCCResult _lo_enable_written()
{
	return RCC_OK;
}

RCCResult _lo_disable_written()
{
	return RCC_OK;
}
