Readme

CPLD (from cpldController.hpp)

set_tx_switches
set_rx_switches
set_tx_atr_bits
set_trx_sw_atr_bits
set_rx_input_atr_bits
set_rx_atr_bits
set_rx_lna_atr_bits

CPLD Registers (do we need anything from this header?)

// Possibilities below for input - need to check default values
///////////////////////////////////////////////////////////////

OWD(can't be in OWS)

<specproperty name='set_tx_switches' writesync='1' default='DONTKNOW' />
<specproperty name='set_rx_switches' writesync='1' default='DONTKNOW' />
<specproperty name='set_tx_atr_bits' writesync='1' default='DONTKNOW' />
<specproperty name='set_trx_sw_atr_bits' writesync='1' default='DONTKNOW' />
<specproperty name='set_rx_input_atr_bits' writesync='1' default='DONTKNOW' />
<specproperty name='set_rx_atr_bits' writesync='1' default='DONTKNOW' />
<specproperty name='set_rx_lna_atr_bits' writesync='1' default='DONTKNOW' />

C++:

RCCResult set_tx_switches_written()
{
    return RCC_OK;
}

RCCResult set_rx_switches_written()
{
    return RCC_OK;
}

RCCResult set_tx_atr_bits_written()
{
    return RCC_OK;
}

RCCResult set_trx_sw_atr_bits_written()
{
    return RCC_OK;
}

RCCResult set_rx_input_atr_bits_written()
{
    return RCC_OK;
}

RCCResult set_rx_atr_bits_written()
{
    return RCC_OK;
}

RCCResult set_rx_lna_atr_bits_written()
{
    return RCC_OK;
}


