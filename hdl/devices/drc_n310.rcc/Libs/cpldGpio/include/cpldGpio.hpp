#pragma once

#include <functional>
#include <bitset>

class N310_CPLD_GPIO{

public:

	using write_fn_t = std::function<void(bool,std::bitset<4>)>;  //callback functor definition

	write_fn_t _write_fn;  // callback declaration

	void writeGpio(bool set, std::bitset<4> channels);  // GPIO write API

	N310_CPLD_GPIO(write_fn_t write_fn); //constructor requiring callback


};

