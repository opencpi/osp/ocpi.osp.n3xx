#ifndef _AXI_ACCESS_H_
#define _AXI_ACCESS_H_

#ifdef __cplusplus // if this is a C++ compiler, use C linkage
extern "C" {
#endif

// #define _GNU_SOURCE // would need this for O_DIRECT but it's not supported on embedded platform
	
#include <sys/mman.h> // mmap, memory manager

/*
	O_DIRECT  is  not  specified  in  POSIX;  one has to define _GNU_SOURCE
       (before including any header files) to get its definition.
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> // file open system call support


#include <unistd.h> // getpagesize()
#include <stdlib.h>

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h> // for debugging

#include <n310parameters.h>

// #include <sys/stat.h> // file attributes
	// stat(), lstat(), fstat()

// function prototypes

// this is a static function
// void init_page_size(void);

// global linkable functions

// uint32_t calc_page_offset(uint32_t addr);

// uint32_t axi_access_rw(uint32_t pageAddr, uint32_t pageOffset, uint32_t val, bool r_w); // 0 = R, 1 = W

// uint32_t axi_access_rw_v2(uint32_t pageAddr, uint32_t pageOffset, uint32_t val, bool r_w);

uint32_t axi_access_rw(uint32_t addressIn, uint32_t val, bool r_w);

void init_mem(void);

void deinit_mem(void);

#ifdef __cplusplus // if this is a C++ compiler, end C linkage
}
#endif

// void axi_access_write(uint32_t addr, uint32_t val);

// uint32_t axi_access_read(uint32_t addr);


#endif//_AXI_ACCESS_H_