#ifndef __MG_DAUGHTERBOARD_HPP__
#define __MG_DAUGHTERBOARD_HPP__
#include <fstream> // has an issue being assigned below cassert?

#include <array>
#include <algorithm>
#include <cpldController.hpp>
#include <lmk04828Controller.hpp>
#include <dsa.hpp>
#include <adf435x.hpp>
#include <ad9371Gpio.hpp>
#include <transceiver.h>
#include <cpldGpio.hpp>


#undef max
#undef abs
#include <cmath>
#include <cassert>

#undef swap

#include <modular_logger.hpp>

// constraints/constants

static constexpr double AD9371_MIN_RX_GAIN  = 0.0; // dB
static constexpr double AD9371_MAX_RX_GAIN  = 30.0; // dB
static constexpr double AD9371_RX_GAIN_STEP = 0.5;
static constexpr double DSA_MIN_GAIN        = 0; // dB
static constexpr double DSA_MAX_GAIN        = 31.5; // dB
static constexpr double DSA_GAIN_STEP       = 0.5; // db
static constexpr double AMP_MIN_GAIN        = 0; // dB
static constexpr double AMP_MAX_GAIN        = 10; // dB
static constexpr double AMP_GAIN_STEP       = 10; // dB
static constexpr double AD9371_MIN_TX_GAIN  = 0.0; // dB
static constexpr double AD9371_MAX_TX_GAIN  = 41.95; // dB
static constexpr double AD9371_TX_GAIN_STEP = 0.05;
static constexpr double ALL_RX_MIN_GAIN     = 0.0;
static constexpr double ALL_RX_MAX_GAIN     = 75.0;
static constexpr double ALL_RX_GAIN_STEP    = 0.5;
static constexpr double ALL_TX_MIN_GAIN     = 0.0;
static constexpr double ALL_TX_MAX_GAIN     = 65.0;
static constexpr double ALL_TX_GAIN_STEP    = 0.5;


namespace {
	const size_t TX_LOWBAND  = 0;
	const size_t TX_HIGHBAND = 1;
	const size_t RX_LOWBAND  = 0;
	const size_t RX_MIDBAND  = 1;
	const size_t RX_HIGHBAND = 2;
}

enum class systemChip {
	c_LMK04828,
	c_AD9371,
	c_CPLD_SPI,
	c_CPLD_GPIO,
	c_DSA,
	c_ADF4351
};


// these should only be created from the lambda wrappers in the dboard constructor
struct abstractRoutingTuple {
	abstractRoutingTuple(systemChip _chipName, std::uint16_t _chipIndex, std::uint16_t _dbIndex);
	systemChip get_chipName(void) const;
	std::uint16_t get_chipIndex(void) const;
	std::uint16_t get_dbIndex(void) const;
	void setTuple(struct abstractRoutingTuple &_refTuple);
private:
	systemChip chipName;
	std::uint16_t chipIndex;
	std::uint16_t dbIndex; // pass through
};

struct chipRouter { // only defined in middleware??
	std::uint16_t get_chipSelect(const struct abstractRoutingTuple _tupleRequest) const;
	chipRouter(const std::vector<std::vector<std::uint16_t>> _systemMap);
private:
	const std::vector<std::vector<std::uint16_t>> systemMap;
};


using cpld_write_spi_t = std::function<void(uint32_t)>;
//! SPI read functor: Return SPI
using cpld_read_spi_t = std::function<uint32_t(uint32_t)>;

// CPLD GPIO Callback
using cpldGpio_write_t = std::function<void(bool)>;

// DSA Callback
using dsa_write_t = std::function<void((unsigned char))>;

//New Transceiver SPI
using ad9371_write_spi_trans_t = std::function<void(std::uint8_t *)>; // frame pointer
using ad9371_read_spi_trans_t = std::function<std::uint8_t(std::uint8_t *)>; // frame pointer

using requestSysref_t = std::function<void(std::uint16_t)>;
using mykGpioReset_t = std::function<void(void)>;

using ad9371Gpio_read_t = std::function<std::uint8_t(void)>;

// this memory space is controlled by DRC explicitly
using jesdInitMemSpace_t = std::function<std::array<std::uint32_t,8>&(void)>; // initialize Transceiver pageArray std::array
using jesdMemTranslator_t = std::function<std::uint32_t(std::uint32_t, std::uint32_t, bool)>;

// DRC will inherit this

// typenmae T --> Implementation of DRC middleware

// #define __USING_SINGLETON


// CRTP --> Curiously Recurring Template Pattern
template<typename T> // based on the DRC implementation
class drcMiddleware { 
protected: // only friends/derived classes can see this

		// dboard will write to this
		// drcMiddleware will write/read from this

	const chipRouter _chipRouter;
	drcMiddleware(std::vector<std::vector<std::uint16_t>> systemMap)
		:
			_chipRouter(systemMap),
			_routingTuple(systemChip::c_LMK04828,0,0)
		{;}
public:
	abstractRoutingTuple _routingTuple;
	#if __USING_SINGLETON
		static T& get_instance() noexcept(std::is_nothrow_constructible<T>::value) // ?
		{ // return only ONE possible instance for singleton
			// Guaranteed to be destroyed.
			// Instantiated on first use.
			// Thread safe in C++11
			std::cout << "HELP\n";
			static T instance{}; // one static instance of DERIVED class, constructs ONCE
			instance.throwaway();

			return instance;
		} // can use drcMiddleware<T?>::get_instance().DEFINED_METHOD() --> can now use this member
	#endif

	// routingTuple _routingTuple; // chip and db select

	// static std::mutex _set_mutex; // protects access to this _routingTuple (singleton could be shared resource)

	// pure virtual functions to be defined BY DRC implementation --> Available for V-Table routing

	// cpld_read_spi
	virtual std::uint32_t cpld_read_spi(std::uint32_t frame) = 0; // virtual
	// cpld_write_spi
	virtual void cpld_write_spi(std::uint_least32_t frame) = 0; // virtual
	// writeCpldGpio
	virtual void writeCpldGpio(bool set) = 0; // virtual
	// readAd9371Gpio
	virtual std::uint8_t readAd9371Gpio(void) = 0; // virtual
	// writeDsaIo
	virtual void writeDsaIo(unsigned char value) = 0; // virtual

	// NEXT TWO MIGHT BE REDUNDANT

	// sysref_wrapper
	virtual void sysref_wrapper(std::uint16_t the_sysref) = 0; // virtual // LMK controller redirect
	// mykReset_wrapper
	virtual void mykReset_wrapper(void) = 0; // virtual // CPLD controller redirect

	// ad9371_write_spi
	virtual void ad9371_write_spi(std::uint8_t * bufIn) = 0; // virtual
	// ad9371_read_spi
	virtual std::uint8_t ad9371_read_spi(std::uint8_t * bufIn) = 0; // virtual
	// jesdInitMemSpace_t
	virtual std::array<std::uint32_t,8>& jesd_mem_manager(void) = 0; // virtual
	// jesdMemTranslator_t
	virtual std::uint32_t jesd_mem_mapper(std::uint32_t addressIn, std::uint32_t value, bool _wr) = 0; // virtual

	// logging function
	virtual void log_helper(std::int32_t _log_level, std::string _log_data) = 0; // pure virtual

};

// typename T --> Implementation of DRC middleware


template<typename T>
struct daughterboard {
	friend class drcMiddleware<T>;

	// configuring the daughterboard locks it (from being RECONFIGURED)
	// only resetting will unlock the daughterboard
private:
	static std::uint16_t _globalDaughterboardCount;
	static std::mutex _set_mutex; // use this for all daughterboard shared R/W access, even to middleware routing tupple
	enum class state { UNLOCKED,LOCKED } dbState; // can only TOGGLE when in the LOCKED state
	std::uint64_t dev_clk_Hz;
	//
public:
	drcMiddleware<T> *referenceMiddleware;

	using log_access_t = std::function<void(std::int32_t,std::string)>;
	const std::uint16_t dbOrdinal;
	const std::string whichDB;
	log_access_t _app_logger; // application level logging

	magnesium_cpld_ctrl cpldController; // one per db
	lmk04828_ctrl lmkController; // one per db
	std::array<adf435x_iface::sptr,2> adfController; // two per db
	std::array<N310_DSA,4> dsaController; // four per db
	N310_CPLD_GPIO cpldGpioController; // one per db
	Transceiver transceiverController; // one per db
	N310_AD9371_GPIO ad9371GpioController; // one per db

	std::uint_least8_t POR; // bit pattern for each controller POR status:
	// bit vs controller
	// 0x0 = unset
	// 0x1 = cpld (bit: 0)
	// 0x2 = lmk  (bit: 1)
	// 0x4 = adf  (bit: 2)f
	// 0x8 = dsa  (bit: 3)
	// 0x10 = cpld gpio (bit: 4)
	// 0x20 = ad9371 GPIO (bit: 5)
	// 0x40 = transceiver object (bit: 6)
	// 0x80 = ad9371GpioController (bit: 7) -- no need to reset?
	const bool active;
	const bool ad9371_gain_ctrl;
	std::bitset<4> ports_tracking;

	#if 0
		struct profile_log_data : csv_logger {
			const std::uint16_t dbOrdinal;
			std::bitset<4> ports_used; // 0b0000 initialized
			std::array<std::pair<bool,double>,4> gain_request;
			std::array<std::pair<bool,double>,4> gain_actual;

			std::array<std::pair<bool,std::int32_t>,2> frontend_band_id;
			std::array<std::pair<bool,std::int32_t>,2> frontend_band_cat;

			profile_log_data(std::string db_identifier, std::uint16_t _dbOrdinal) :
				modular_logger(db_identifier+"_gain_log.csv",false,"port_num,frontend_id,frontend_category,gain_requested,gain_actual\n"),
				dbOrdinal(_dbOrdinal),
				ports_used{0}, 
				gain_request{{
					{false,NAN},
					{false,NAN},
					{false,NAN},
					{false,NAN}
				}},
				gain_actual{{
					{false,NAN},
					{false,NAN},
					{false,NAN},
					{false,NAN}
				}},
				frontend_band_id{{
					{false,0},
					{false,0}
				}},
				frontend_band_cat{{
					{false,0},
					{false,0}
				}}
			{;}
			void set_gain_request(std::uint16_t db_port, double _gain_request){
				gain_request.at(db_port).first = true;
				ports_used.set(db_port);
				gain_request.at(db_port).second = _gain_request;
			}
			void set_gain_actual(std::uint16_t db_port, double _gain_actual){
				gain_actual.at(db_port).first = true;
				gain_actual.at(db_port).second = _gain_actual;
			}
			void set_frontend_band_id(bool _rx, std::int32_t _band_id){
				frontend_band_id.at(_rx).first = true;
				frontend_band_id.at(_rx).second = _band_id;
			}
			void set_frontend_band_cat(bool _rx, std::int32_t _band_cat){
				frontend_band_cat.at(_rx).first = true;
				frontend_band_cat.at(_rx).second = _band_cat;
			}
			void completed_port_check(std::uint16_t db_port){
				auto _rx = (db_port & 0x2) >> 1;
				assert(gain_request.at(db_port).first);
				assert(gain_actual.at(db_port).first);
				assert(frontend_band_id.at(_rx).first);
				assert(frontend_band_cat.at(_rx).first);
			}
			// pure virtual modular_logger methods
			void write_log_buffer(void){
				for(std::uint16_t db_port = 0; db_port < this->ports_used.size(); db_port++){
					if(this->ports_used.test(db_port)){
						this->completed_port_check(db_port); // assert all values have been set by now
						// create a new CSV record
						std::stringstream csv_record;
						auto _rx = (db_port & 0x2) >> 1;
						csv_record << ((dbOrdinal << 2) + db_port) << ",";
						csv_record << this->frontend_band_id.at(_rx).second << ",";
						csv_record << this->frontend_band_cat.at(_rx).second << ",";
						csv_record << this->gain_request.at(db_port).second << ",";
						csv_record << this->gain_actual.at(db_port).second << "\n";
						// finished CSV record, now append it to the proper CSV log file
						this->flush_log_data(csv_record.str());
					}
				}
				this->clear_log_buffer(); // clear the log
			}
			// only gets called by write_log_buffer
			void clear_log_buffer(void){
				ports_used=std::bitset<4>(0);
				gain_request={{{false,NAN},{false,NAN},{false,NAN},{false,NAN}}};
				gain_actual={{{false,NAN},{false,NAN},{false,NAN},{false,NAN}}};
				frontend_band_id=std::array<std::pair<bool,std::int32_t>,2>();
				frontend_band_cat=std::array<std::pair<bool,std::int32_t>,2>();
			}
			// "flushing log" -- write (at max) 4 lines to CSV
				// uses the log_helper with negative log level
				// log_helper will PARSE the port number and replace with + 4 depending on which daughterboard
		} _profile_log_data;
	#endif

	void toggle_profile(bool on){
		if(this->dbState == state::LOCKED){
			Transceiver::radioState selection = (on) ? Transceiver::radioState::ON : Transceiver::radioState::OFF;
			// toggle the ad9371
			this->transceiverController.radioControl(selection); // mykonos API
			// toggle the frontend
			this->cpldGpioController.writeGpio(on,this->ports_tracking); // which ports this daughterboard configuration is currently tracking
			this->POR |= 0x10;
			// this is giving issues that I don't feel like resolving until logging gets merged back in
			#if 0
				if(on){
					for(std::uint16_t bit_test = 0; bit_test < _profile_log_data.ports_used.size(); bit_test++){
						if(this->_profile_log_data.ports_used.test(bit_test)){
							// Analog devices recommended to try setting gain in the radio::ON state	
							// This did not fix the problem*
							// set_gain(bit_test,this->_profile_log_data.gain_request.at(bit_test).second);
							this->_profile_log_data.set_gain_actual(bit_test,this->get_gain(bit_test)); // finish log CSV
						}
					}
					// Can only flush the full profile log AFTER turning it on -- all the data has been fully updated
					this->flush_profile_log_data(); // flush the log
				}
			#endif
		}
	}

	// struct channel_config_struct;
	bool set_profile(std::vector<struct drcConfig> profile_configuration){
		// pre validated --
		// can only set the profile (program IC's) when the daughterboard is in UNLOCKED state
		if((this->dbState == state::UNLOCKED) && (profile_configuration.size())){
			// daughterboard is unlocked AND if vector has profiles in it (protect from configuring "nothing")

			// store a copy of ORIGINAL, UNTOUCHED profile configurations

			const auto original_profile = profile_configuration;

			if(this->POR){
				this->resetDaughterboard();
			}
			struct gain_channel_tuple {
				std::uint16_t port_num;
				double gain_request;
				gain_channel_tuple(std::uint16_t _port_num, double _gain_request)
					: port_num(_port_num),gain_request(_gain_request)
				{;}
			};
			std::vector<struct gain_channel_tuple> gainTracker;
			// initial configuration loop
			std::set<std::uint16_t> channelTypeTracker;
			for(auto &conf_channel : profile_configuration){
				conf_channel.rf_port_num &= 0x3; // db_port 
				// redundant port configuration check
				if(this->ports_tracking.test(conf_channel.rf_port_num)){
					// @TODO: Log why this failed
					// prepare_config_logOut << "User redundant port configuration of rf_port_num = " << static_cast<std::uint16_t>(conf_channel.rf_port_num) << "\n";
					// prepare_config_logOut << "prepare_config operation could not be completed for ordinal " << configOrdinal << "\n";
					return false;
				} else {
					this->ports_tracking.set(conf_channel.rf_port_num);
				}
				// pre process some configuration data
				// @TODO: before overwriting check tolerance?

				// gain boundary condition clipping
				if(ad9371_gain_ctrl){ // gain clipping for dynamic control of both DSA and ad9371 gain blocks
					if((conf_channel.gain_dB >= 75) and conf_channel.rx){
						conf_channel.gain_dB = 75;
					} else if((conf_channel.gain_dB >= 65) and !conf_channel.rx){
						conf_channel.gain_dB = 65;
					} else if(conf_channel.gain_dB < 0){
						conf_channel.gain_dB = 0;
					}
				} else { // gain clipping for only the DSA gain
					auto &which_dsa = this->dsaController.at(conf_channel.rf_port_num);
					if(conf_channel.gain_dB > which_dsa.get_default_maximum_attenuation()){
						conf_channel.gain_dB = which_dsa.get_default_maximum_attenuation();
					} else if(conf_channel.gain_dB < 0) {
						conf_channel.gain_dB = 0;
					}
				}
				// track this channel's gain request for post transceiver configuration gain programming sequence
				gainTracker.push_back(gain_channel_tuple(conf_channel.rf_port_num,conf_channel.gain_dB));
				// coupled channel section
				if(channelTypeTracker.find(conf_channel.rx) == channelTypeTracker.end()){
					channelTypeTracker.insert(conf_channel.rx);
					// prioritize first channel's request of the devclock rate
					if(!this->dev_clk_Hz){ // this ONLY needs to be configured FROM one channel configuration struct in the vector
						std::vector<double> mcrPool_Hz = this->lmkController.getValidMCRs();
						double maxDelta = std::numeric_limits<double>::max(); // #include <limits>
						for(auto &it : mcrPool_Hz){
							double curDelta = std::abs(it - (conf_channel.sampling_rate_Msps * 1e6));
							if(curDelta < maxDelta){ // choose the closest VALID MCR from mcrPool_Hz
								this->dev_clk_Hz = it;
								maxDelta = curDelta;
							}
						}
						this->lmkController.mcrConfigure(static_cast<std::int_least32_t>(this->dev_clk_Hz), 25e6); // 2nd argument is constant for now
						this->POR |= 0x2;
					}
					// should have a valid device clock programmed by here
					this->configure_frontend_profile(conf_channel.rx, conf_channel.tuning_freq_MHz);

					// set the dualChannel configuration
					// Step 6A: Transceiver object (AD9371) control

					// only configure transceiver if DUAL CHANNEL not programmed
					if(!this->transceiverController.channel_configured(conf_channel.rx)){
						// only configures once per channel type
						this->transceiverController.configure_transceiver(conf_channel);
						this->POR |= 0x40;
					}
				}
			} // channel loop
			// program the transceiver
			if(this->transceiverController.run_configuration()){
				this->_app_logger(3,"Transceiver failed in running its configuration\n");
				return false;
			}
			// if fail, perhaps catch in a branch and log (this function should return bool?)

			// set the gains
			for(auto &channel_gain : gainTracker){
				set_gain(channel_gain.port_num,channel_gain.gain_request);
			}

			// tolerance checking -- @TODO: when using ad9371 gain control, this becomes more complicated due to gain updating at radio ON time
			bool passed_tolerance_check = true;
			for(auto &conf_channel : original_profile){ // secondary profile loop for tolerance checking
				// run this validation procdedure for every channel even if one failed already
				passed_tolerance_check &= this->validate_channel_config(conf_channel);
			}
			if(!passed_tolerance_check){
				return false;
			}
			this->dbState = state::LOCKED; // only when successful configuration
		}
		return true; // success?
	}

	// retval: success = true
	// this method only to be called IF: daughterboard config state = "locked" AND transceiver controller is configured for given channel
	bool update_tuning_frequency(bool _rx, double tuning_freq_MHz){
		if(this->dbState == state::LOCKED){ // this section only executes if
			// confirm tuning frequency value is different
			// auto _ad9371_LO_tracker = this->transceiverController._get_tracker_ref();
			auto previous_tuning_Hz = this->transceiverController._get_tracker_ref()->get_tuning_hz(_rx);
			std::uint64_t requested_compare_value = static_cast<std::uint64_t>(std::abs(tuning_freq_MHz)*1e6);
			if(previous_tuning_Hz == requested_compare_value){
				// return false or true?
				this->_app_logger(3,"update_tuning_frequency bypassed due to parameter remaining unchanged\n");
				return true;
			}
			this->configure_frontend_profile(_rx, tuning_freq_MHz);
			// ONLY update the frequency IF the requested CHANNEL has been activated (check from transceiver)
			if(!this->transceiverController.channel_configured(_rx)){
				// requested to update a channel that has NOT been activated/configured
				// this is an error
				std::stringstream error_log;
				auto chan_str = (_rx) ? "rx" : "tx";
				error_log << "Failure updating the tuning frequency: attempted to update frequency for an inactive channel (";
				error_log << chan_str;
				error_log << ")\n";
				this->_app_logger(3,error_log.str().c_str()); // DRC_LOG_THREE
				return false;
			}
			// update the transceiver
			this->transceiverController.update_tuning_frequency(_rx,tuning_freq_MHz);
			// @TODO -- VALIDATE TUNING FREQUENCY??? even though DRC documentation says to do this in PREPARE state
			return true;
		}
	}

	void configure_frontend_profile(bool _rx, double tuning_freq_MHz){
		// NEED TO HAVE LMK CONFIGURED BY THIS PIONT -- > LMK method for this?
		// Tuning Frequency boundary condition clipping
		bool wideBandSelect = false;
		if(tuning_freq_MHz < 0){
			wideBandSelect = true;
		}
		if(std::abs(tuning_freq_MHz) < 10.0){
			tuning_freq_MHz = (wideBandSelect)? -10.0 : 10.0;
		} else if(std::abs(tuning_freq_MHz) > 6000.0) { // outside of the ad9371 range
			tuning_freq_MHz = (wideBandSelect)? -6000.0 : 6000.0;
		}

		// from here just need to track the tuning frequency deltas

		// program front end pathing
		this->cpldController.cpldSwitchingPathSetup((tuning_freq_MHz*1e6),_rx);
		this->POR |= 0x1;

		tuning_freq_MHz = std::abs(tuning_freq_MHz);
		// Program the frontend LO's (ADF4351) depending on if tuning frequency is in the LOWBAND
		if(tuning_freq_MHz <= 300){ // if frequency too low, need to upmix
			// actualTuningFreq stays the same, LO_freq_req_hz is the superheterodyne Intermediate Frequency
			double LO_freq_req_hz =  (_rx)
				? (2441.0 * 1e6) + (tuning_freq_MHz * 1e6) // receive
				: (1950.0 * 1e6) + (tuning_freq_MHz * 1e6); // transmit
			this->adfController.at(!(_rx))->_lo_enable(LO_freq_req_hz,this->dev_clk_Hz,false);
			this->POR |= 0x4;
			// Check if PLL is locked for maximum 500 milliseconds
			static const std::array<std::uint8_t,2> SYNTH_LOCK_DETECT = {0,4}; // RX bit, TX bit
			std::uint8_t LOCK_DETECT_READBACK = 0;
			std::uint16_t msCountDown = 500;
			while(!(LOCK_DETECT_READBACK & (1 << SYNTH_LOCK_DETECT.at(!_rx))) and msCountDown){
				// 500 milliseconds for LO to lock, only enabled during countdown
				LOCK_DETECT_READBACK = this->cpldController.get_reg(0x12);
				// call transceiver library delay function
				mdelay(1); // one millisecond
				msCountDown--;
			}
			if(!(LOCK_DETECT_READBACK & (1 << SYNTH_LOCK_DETECT.at(!_rx)))){
				// superheterodyne synthesizer configuration failed to lock its PLL in time
				const std::array<std::string,2> RX_TX = {"RX","TX"};
				std::stringstream set_profile_log;
				set_profile_log << "set_profile failed due to " << RX_TX.at(!_rx);
				set_profile_log << " synthesizer failure to lock within 500 milliseconds\n";
				// m_properties.status.data[configOrdinal].state = Drc_n310WorkerTypes::State::STATUS_STATE_ERROR;
				this->_app_logger(3,set_profile_log.str().c_str()); // DRC_LOG_THREE
				// return something to indicate bad set_profile
				// this assertion will fail
				assert(LOCK_DETECT_READBACK & (1 << SYNTH_LOCK_DETECT.at(!_rx)));
			}
		} // adf branch
	}

	// Ettus gain tables -- current working prototype
	#if 1
		struct gain_tuple_t
		{
		    //! Attenuation value of the DSA in dB
		    double dsa_att;
		    //! Attenuation value of Mykonos (AD9371) in dB
		    double ad9371_att;
		    //! If true, bypass LNA or PA section
		    bool bypass;
		};

		using gain_tuple_map_t = std::map<std::int32_t, gain_tuple_t>;

		//! Maps band -> gain_tuple_map_t
		using gain_tables_t = std::map<size_t, gain_tuple_map_t>;

	    /*! RX gain tables
	     */

		// map requested channel gain to corresponding API
		
		const gain_tables_t rx_gain_tables = {
		    {RX_LOWBAND, {
		            // Gain, DSA att, Myk att, bypass
		            {0,    {30, 30, true}},
		            {1,    {30, 29, true}},
		            {2,    {30, 28, true}},
		            {3,    {30, 27, true}},
		            {4,    {30, 26, true}},
		            {5,    {30, 25, true}},
		            {6,    {29, 25, true}},
		            {7,    {28, 25, true}},
		            {8,    {27, 25, true}},
		            {9,    {26, 25, true}},
		            {10,   {25, 25, true}},
		            {11,   {25, 24, true}},
		            {12,   {25, 23, true}},
		            {13,   {25, 22, true}},
		            {14,   {25, 21, true}},
		            {15,   {25, 20, true}},
		            {16,   {24, 20, true}},
		            {17,   {23, 20, true}},
		            {18,   {22, 20, true}},
		            {19,   {21, 20, true}},
		            {20,   {20, 20, true}},
		            {21,   {20, 19, true}},
		            {22,   {20, 18, true}},
		            {23,   {20, 17, true}},
		            {24,   {20, 16, true}},
		            {25,   {20, 15, true}},
		            {26,   {19, 15, true}},
		            {27,   {18, 15, true}},
		            {28,   {17, 15, true}},
		            {29,   {16, 15, true}},
		            {30,   {15, 15, true}},
		            {31,   {14, 15, true}},
		            {32,   {13, 15, true}},
		            {33,   {12, 15, true}},
		            {34,   {11, 15, true}},
		            {35,   {10, 15, true}},
		            {36,   {10, 14, true}},
		            {37,   {10, 13, true}},
		            {38,   {10, 12, true}},
		            {39,   {10, 11, true}},
		            {40,   {10, 10, true}},
		            {41,   {9, 10, true}},
		            {42,   {8, 10, true}},
		            {43,   {7, 10, true}},
		            {44,   {6, 10, true}},
		            {45,   {5, 10, true}},
		            {46,   {4, 10, true}},
		            {47,   {3, 10, true}},
		            {48,   {2, 10, true}},
		            {49,   {1, 10, true}},
		            {50,   {15, 15, false}},
		            {51,   {14, 15, false}},
		            {52,   {13, 15, false}},
		            {53,   {12, 15, false}},
		            {54,   {11, 15, false}},
		            {55,   {10, 15, false}},
		            {56,   {10, 14, false}},
		            {57,   {10, 13, false}},
		            {58,   {10, 12, false}},
		            {59,   {10, 11, false}},
		            {60,   {10, 10, false}},
		            {61,   {9, 10, false}},
		            {62,   {8, 10, false}},
		            {63,   {7, 10, false}},
		            {64,   {6, 10, false}},
		            {65,   {5, 10, false}},
		            {66,   {4, 10, false}},
		            {67,   {3, 10, false}},
		            {68,   {2, 10, false}},
		            {69,   {1, 10, false}},
		            {70,   {0, 10, false}},
		            {71,   {0, 9, false}},
		            {72,   {0, 8, false}},
		            {73,   {0, 7, false}},
		            {74,   {0, 6, false}},
		            {75,   {0, 5, false}}
		    }},
		    {RX_MIDBAND, { // Valid for bands 0, 1, 2, 3
		            {0, {30, 30, true}},
		            {1, {30, 29, true}},
		            {2, {30, 28, true}},
		            {3, {30, 27, true}},
		            {4, {30, 26, true}},
		            {5, {30, 25, true}},
		            {6, {30, 24, true}},
		            {7, {30, 23, true}},
		            {8, {30, 22, true}},
		            {9, {30, 21, true}},
		            {10, {30, 20, true}},
		            {11, {30, 19, true}},
		            {12, {30, 18, true}},
		            {13, {30, 17, true}},
		            {14, {30, 16, true}},
		            {15, {30, 15, true}},
		            {16, {29, 15, true}},
		            {17, {28, 15, true}},
		            {18, {27, 15, true}},
		            {19, {26, 15, true}},
		            {20, {25, 15, true}},
		            {21, {24, 15, true}},
		            {22, {23, 15, true}},
		            {23, {22, 15, true}},
		            {24, {21, 15, true}},
		            {25, {20, 15, true}},
		            {26, {19, 15, true}},
		            {27, {18, 15, true}},
		            {28, {17, 15, true}},
		            {29, {16, 15, true}},
		            {30, {15, 15, true}},
		            {31, {14, 15, true}},
		            {32, {13, 15, true}},
		            {33, {12, 15, true}},
		            {34, {11, 15, true}},
		            {35, {10, 15, true}},
		            {36, {9, 15, true}},
		            {37, {8, 15, true}},
		            {38, {7, 15, true}},
		            {39, {6, 15, true}},
		            {40, {5, 15, true}},
		            {41, {5, 14, true}},
		            {42, {5, 13, true}},
		            {43, {5, 12, true}},
		            {44, {5, 11, true}},
		            {45, {5, 10, true}},
		            {46, {4, 10, true}},
		            {47, {3, 10, true}},
		            {48, {2, 10, true}},
		            {49, {1, 10, true}},
		            {50, {15, 15, false}},
		            {51, {15, 14, false}},
		            {52, {15, 13, false}},
		            {53, {15, 12, false}},
		            {54, {15, 11, false}},
		            {55, {15, 10, false}},
		            {56, {15, 9, false}},
		            {57, {15, 8, false}},
		            {58, {15, 7, false}},
		            {59, {15, 6, false}},
		            {60, {15, 5, false}},
		            {61, {14, 5, false}},
		            {62, {13, 5, false}},
		            {63, {12, 5, false}},
		            {64, {11, 5, false}},
		            {65, {10, 5, false}},
		            {66, {10, 4, false}},
		            {67, {10, 3, false}},
		            {68, {10, 2, false}},
		            {69, {10, 1, false}},
		            {70, {10, 0, false}},
		            {71, {9, 0, false}},
		            {72, {8, 0, false}},
		            {73, {7, 0, false}},
		            {74, {6, 0, false}},
		            {75, {5, 0, false}}
		    }},
		    {RX_HIGHBAND, { // Valid for bands 4, 5, 6
		            {0, {30, 30, true}},
		            {1, {30, 29, true}},
		            {2, {30, 28, true}},
		            {3, {30, 27, true}},
		            {4, {30, 26, true}},
		            {5, {30, 25, true}},
		            {6, {30, 24, true}},
		            {7, {30, 23, true}},
		            {8, {30, 22, true}},
		            {9, {30, 21, true}},
		            {10, {30, 20, true}},
		            {11, {30, 19, true}},
		            {12, {30, 18, true}},
		            {13, {30, 17, true}},
		            {14, {30, 16, true}},
		            {15, {30, 15, true}},
		            {16, {29, 15, true}},
		            {17, {28, 15, true}},
		            {18, {27, 15, true}},
		            {19, {26, 15, true}},
		            {20, {25, 15, true}},
		            {21, {24, 15, true}},
		            {22, {23, 15, true}},
		            {23, {22, 15, true}},
		            {24, {21, 15, true}},
		            {25, {20, 15, true}},
		            {26, {19, 15, true}},
		            {27, {18, 15, true}},
		            {28, {17, 15, true}},
		            {29, {16, 15, true}},
		            {30, {15, 15, true}},
		            {31, {15, 14, true}},
		            {32, {15, 13, true}},
		            {33, {15, 12, true}},
		            {34, {15, 11, true}},
		            {35, {15, 10, true}},
		            {36, {14, 10, true}},
		            {37, {13, 10, true}},
		            {38, {12, 10, true}},
		            {39, {11, 10, true}},
		            {40, {10, 10, true}},
		            {41, {9, 10, true}},
		            {42, {8, 10, true}},
		            {43, {7, 10, true}},
		            {44, {6, 10, true}},
		            {45, {5, 10, true}},
		            {46, {4, 10, true}},
		            {47, {3, 10, true}},
		            {48, {2, 10, true}},
		            {49, {1, 10, true}},
		            {50, {15, 15, false}},
		            {51, {15, 14, false}},
		            {52, {15, 13, false}},
		            {53, {15, 12, false}},
		            {54, {15, 11, false}},
		            {55, {15, 10, false}},
		            {56, {14, 10, false}},
		            {57, {13, 10, false}},
		            {58, {12, 10, false}},
		            {59, {11, 10, false}},
		            {60, {10, 10, false}},
		            {61, {10, 9, false}},
		            {62, {10, 8, false}},
		            {63, {10, 7, false}},
		            {64, {10, 6, false}},
		            {65, {10, 5, false}},
		            {66, {9, 5, false}},
		            {67, {8, 5, false}},
		            {68, {7, 5, false}},
		            {69, {6, 5, false}},
		            {70, {5, 5, false}},
		            {71, {5, 4, false}},
		            {72, {5, 3, false}},
		            {73, {5, 2, false}},
		            {74, {5, 1, false}},
		            {75, {5, 0, false}}
		        },
		    }
		}; /* rx_gain_tables */

		const gain_tables_t tx_gain_tables = {
		    {TX_LOWBAND, {
		            // Gain, DSA att, Myk att, bypass
		            {0, {30, 20, true}},
		            {1, {29, 20, true}},
		            {2, {28, 20, true}},
		            {3, {27, 20, true}},
		            {4, {26, 20, true}},
		            {5, {25, 20, true}},
		            {6, {24, 20, true}},
		            {7, {23, 20, true}},
		            {8, {22, 20, true}},
		            {9, {21, 20, true}},
		            {10, {20, 20, true}},
		            {11, {19, 20, true}},
		            {12, {18, 20, true}},
		            {13, {17, 20, true}},
		            {14, {16, 20, true}},
		            {15, {15, 20, true}},
		            {16, {14, 20, true}},
		            {17, {13, 20, true}},
		            {18, {12, 20, true}},
		            {19, {11, 20, true}},
		            {20, {10, 20, true}},
		            {21, {9, 20, true}},
		            {22, {8, 20, true}},
		            {23, {7, 20, true}},
		            {24, {6, 20, true}},
		            {25, {5, 20, true}},
		            {26, {4, 20, true}},
		            {27, {3, 20, true}},
		            {28, {2, 20, true}},
		            {29, {1, 20, true}},
		            {30, {0, 20, true}},
		            {31, {0, 19, true}},
		            {32, {0, 18, true}},
		            {33, {0, 17, true}},
		            {34, {0, 16, true}},
		            {35, {0, 15, true}},
		            {36, {0, 14, true}},
		            {37, {0, 13, true}},
		            {38, {0, 12, true}},
		            {39, {0, 11, true}},
		            {40, {10, 15, false}},
		            {41, {9, 15, false}},
		            {42, {8, 15, false}},
		            {43, {7, 15, false}},
		            {44, {6, 15, false}},
		            {45, {5, 15, false}},
		            {46, {4, 15, false}},
		            {47, {3, 15, false}},
		            {48, {2, 15, false}},
		            {49, {1, 15, false}},
		            {50, {0, 15, false}},
		            {51, {0, 14, false}},
		            {52, {0, 13, false}},
		            {53, {0, 12, false}},
		            {54, {0, 11, false}},
		            {55, {0, 10, false}},
		            {56, {0, 9, false}},
		            {57, {0, 8, false}},
		            {58, {0, 7, false}},
		            {59, {0, 6, false}},
		            {60, {0, 5, false}},
		            {61, {0, 4, false}},
		            {62, {0, 3, false}},
		            {63, {0, 2, false}},
		            {64, {0, 1, false}},
		            {65, {0, 0, false}}
		    }},
		    {TX_HIGHBAND, { // Valid for bands 1, 2, 3, 4
		            {0, {30, 20, true}},
		            {1, {29, 20, true}},
		            {2, {28, 20, true}},
		            {3, {27, 20, true}},
		            {4, {26, 20, true}},
		            {5, {25, 20, true}},
		            {6, {24, 20, true}},
		            {7, {23, 20, true}},
		            {8, {22, 20, true}},
		            {9, {21, 20, true}},
		            {10, {20, 20, true}},
		            {11, {19, 20, true}},
		            {12, {18, 20, true}},
		            {13, {17, 20, true}},
		            {14, {16, 20, true}},
		            {15, {15, 20, true}},
		            {16, {14, 20, true}},
		            {17, {13, 20, true}},
		            {18, {12, 20, true}},
		            {19, {11, 20, true}},
		            {20, {10, 20, true}},
		            {21, {9, 20, true}},
		            {22, {8, 20, true}},
		            {23, {7, 20, true}},
		            {24, {6, 20, true}},
		            {25, {5, 20, true}},
		            {26, {4, 20, true}},
		            {27, {3, 20, true}},
		            {28, {2, 20, true}},
		            {29, {1, 20, true}},
		            {30, {0, 20, true}},
		            {31, {0, 19, true}},
		            {32, {0, 18, true}},
		            {33, {0, 17, true}},
		            {34, {0, 16, true}},
		            {35, {5, 20, false}},
		            {36, {4, 20, false}},
		            {37, {3, 20, false}},
		            {38, {2, 20, false}},
		            {39, {1, 20, false}},
		            {40, {0, 20, false}},
		            {41, {0, 19, false}},
		            {42, {0, 18, false}},
		            {43, {0, 17, false}},
		            {44, {0, 16, false}},
		            {45, {0, 15, false}},
		            {46, {0, 14, false}},
		            {47, {0, 13, false}},
		            {48, {0, 12, false}},
		            {49, {0, 11, false}},
		            {50, {0, 10, false}},
		            {51, {0, 9, false}},
		            {52, {0, 8, false}},
		            {53, {0, 7, false}},
		            {54, {0, 6, false}},
		            {55, {0, 5, false}},
		            {56, {0, 4, false}},
		            {57, {0, 3, false}},
		            {58, {0, 2, false}},
		            {59, {0, 1, false}},
		            {60, {0, 0, false}},
		            // Rest is fake to keep same gain range as low band
		            {61, {0, 0, false}},
		            {62, {0, 0, false}},
		            {63, {0, 0, false}},
		            {64, {0, 0, false}},
		            {65, {0, 0, false}}
		    }}
		}; /* tx_gain_tables */
	#endif

	double get_gain(const std::uint16_t port_num){
		auto db_port = port_num & 0x3;
		double _actual_gain;
		if(ad9371_gain_ctrl){ // allow the dynamic control of the ad9731 gain blocks
			auto dsa_gain = this->dsaController.at(db_port).get_adjusted_gain();
			auto transceiver_gain = this->transceiverController.get_gain(db_port);
			_actual_gain = (dsa_gain + transceiver_gain);
			// @TODO: When deltas are better understood -- add the computations in here
			// example: RX side & non bypass is roughly off by a factor of 18 decibels (close to amplifier gains)
		} else { // ONLY dynamically controlling the DSA
			_actual_gain = this->dsaController.at(db_port).get_adjusted_gain();
		}
		return _actual_gain;
	}

	void set_gain(const std::uint16_t port_num, const double gain){
		std::uint16_t db_port = port_num & 0x3;
		if(ad9371_gain_ctrl){ // allow the dynamic control of the ad9731 gain blocks
			// can ONLY set gain AFTER cpld front end is programmed
			bool _rx = (port_num & 0x2);
			auto &whichCpldBandContainer = this->cpldController.dualChannelBand.at(_rx);
			auto channelBandCategory = whichCpldBandContainer->get_band_category();
			assert(channelBandCategory != -1);
			auto gain_tuple =
				(_rx) // rx direction
					? get_rx_gain_tuple(gain, channelBandCategory)
					: get_tx_gain_tuple(gain, channelBandCategory);

			const double ad9371_gain =
				((_rx) ? AD9371_MAX_RX_GAIN : AD9371_MAX_TX_GAIN)
				- gain_tuple.ad9371_att;

			// make sure bypass matches the front end setup

			bool userBypassSel =
				(_rx) // RX?
					? (channelBandCategory == 2) // RX
					: (channelBandCategory == 1); // TX

			assert(userBypassSel == gain_tuple.bypass); // if this is not true, then we cannot run this configuration

			// transceiver controller set the gain using the ad9371_gain + channel/dir
			this->transceiverController.set_gain(port_num,ad9371_gain);
			// set the dsa attenuation
			this->dsaController.at(port_num).double_to_hex(gain_tuple.dsa_att); // this method directly uses attenuation
			// set 3 of the 4 current log measurements
			// this->_profile_log_data.set_gain_request(port_num & 0x3,gain);
			// following 2 based only on channel type
			// this->_profile_log_data.set_frontend_band_id(_rx,whichCpldBandContainer->get_band_id());
			// this->_profile_log_data.set_frontend_band_cat(_rx,channelBandCategory);
		} else { // ONLY dynamically controlling the DSA
			// convert gain to attenuation
			// this->_profile_log_data.set_gain_request(port_num & 0x3,gain);
			auto dsa_max_gain = this->dsaController.at(port_num).get_default_maximum_attenuation();
			this->dsaController.at(port_num).double_to_hex(dsa_max_gain - gain);
		}
		return;
	}

	double get_tuning_freq_MHz(const std::uint16_t port_num){
		bool _rx = port_num & 0x2;
		double adf4351TuningFreq_Hz = 0;
		adf4351TuningFreq_Hz = this->adfController.at(!_rx)->get_tuning_freq_Hz();
		double AD9371TuningFreq_MHz = this->transceiverController.get_tuning_freq_MHz(port_num);
		double adf4351TuningFreq_MHz = static_cast<double>(adf4351TuningFreq_Hz)/1e6;
		double adjusted_tuning_freq = std::abs(adf4351TuningFreq_MHz - AD9371TuningFreq_MHz);
		return adjusted_tuning_freq;
	}

	bool validate_channel_config(const struct drcConfig requested_chan_conf){
		// make sure configurations are within tolerance values
		bool valid_configuration = true;
		std::stringstream validateChannelConfig_logOut;
		validateChannelConfig_logOut << "validateChannelConfig(): db_port_num=" << static_cast<std::uint16_t>(requested_chan_conf.rf_port_num) << "\n";
		// channel configuration checks
		auto recorded_tuning_freq_MHz = this->get_tuning_freq_MHz(requested_chan_conf.rf_port_num & 0x3);
		const auto requested_tuning_freq_MHz = std::abs(requested_chan_conf.tuning_freq_MHz); // a negative value is used for special Front End Configuration
		if((std::abs(recorded_tuning_freq_MHz - requested_tuning_freq_MHz)) > requested_chan_conf.tolerance_tuning_freq_MHz){
			validateChannelConfig_logOut << "Failed tolerance_tuning_freq_MHz check:\n";
			validateChannelConfig_logOut << "\tProgrammed value: " << recorded_tuning_freq_MHz << "\n";
			validateChannelConfig_logOut << "\tRequested value: " << requested_tuning_freq_MHz << "\n";
			validateChannelConfig_logOut << "\tDefined Tolerance: " << requested_chan_conf.tolerance_tuning_freq_MHz << "\n";
			valid_configuration = false;
		}
		// @TODO: Debugging this issue right now
		auto recorded_bandwidth_3dB_MHz = this->transceiverController.get_bandwidth_3dB_MHz(requested_chan_conf.rf_port_num & 0x3);
		if((std::abs(recorded_bandwidth_3dB_MHz - requested_chan_conf.bandwidth_3dB_MHz)) > requested_chan_conf.tolerance_bandwidth_3dB_MHz){
			validateChannelConfig_logOut << "Failed tolerance_bandwidth_3dB_MHz check:\n";
			validateChannelConfig_logOut << "\tProgrammed value: " << recorded_bandwidth_3dB_MHz << "\n";
			validateChannelConfig_logOut << "\tRequested value: " << requested_chan_conf.bandwidth_3dB_MHz << "\n";
			validateChannelConfig_logOut << "\tDefined Tolerance: " << requested_chan_conf.tolerance_bandwidth_3dB_MHz << "\n";
			valid_configuration = false;
		}
		auto recorded_sampling_rate_Msps = this->lmkController.get_sampling_rate_Msps();
		if((std::abs(recorded_sampling_rate_Msps - requested_chan_conf.sampling_rate_Msps)) > requested_chan_conf.tolerance_sampling_rate_Msps){
			validateChannelConfig_logOut << "Failed tolerance_sampling_rate_Msps check:\n";
			validateChannelConfig_logOut << "\tProgrammed value: " << recorded_sampling_rate_Msps << "\n";
			validateChannelConfig_logOut << "\tRequested value: " << requested_chan_conf.sampling_rate_Msps << "\n";
			validateChannelConfig_logOut << "\tDefined Tolerance: " << requested_chan_conf.tolerance_sampling_rate_Msps << "\n";
			valid_configuration = false;
		}
		// @TODO : Warning -- when using AD9371 gain control, this validation becomes more convoluted (supposed to program this after turning radio on?)
		auto recorded_gain_dB = this->get_gain(requested_chan_conf.rf_port_num & 0x3);
		if((std::abs(recorded_gain_dB - requested_chan_conf.gain_dB)) > requested_chan_conf.tolerance_gain_dB){
			validateChannelConfig_logOut << "Failed tolerance_gain_dB check:\n";
			validateChannelConfig_logOut << "\tProgrammed value: " << recorded_gain_dB << "\n";
			validateChannelConfig_logOut << "\tRequested value: " << requested_chan_conf.gain_dB << "\n";
			validateChannelConfig_logOut << "\tDefined Tolerance: " << requested_chan_conf.tolerance_gain_dB << "\n";
			valid_configuration = false;
		}
		if(not valid_configuration){
			this->_app_logger(3,validateChannelConfig_logOut.str().c_str());
		}
		return valid_configuration;
	}

	gain_tuple_t fine_tune_ad9371_att(const gain_tuple_t gain_tuple, const double gain_index)
	{
	    // Here, we hardcode the half-dB steps. We soak up all half-dB
	    // steps by twiddling the AD9371 attenuation, but we need to make
	    // sure we don't make it negative.
	    if (gain_index - int(gain_index) >= .5) {
	        gain_tuple_t gt2 = gain_tuple;
	        gt2.ad9371_att   = std::max(0.0, gain_tuple.ad9371_att - .5);
	        return gt2;
	    }
	    return gain_tuple;
	}

	gain_tuple_t get_rx_gain_tuple(
	    const double gain_index, std::int16_t _band)
	{
	    assert(gain_index <= ALL_RX_MAX_GAIN);
	    assert(gain_index >= ALL_RX_MIN_GAIN);
	    auto& gain_table            = rx_gain_tables.at(_band);
	    const int gain_index_truncd = int(gain_index);
	    return fine_tune_ad9371_att(gain_table.at(gain_index_truncd), gain_index);
	}

	gain_tuple_t get_tx_gain_tuple(
	    const double gain_index, std::int16_t _band)
	{
	    assert(gain_index <= ALL_TX_MAX_GAIN);
	    assert(gain_index >= ALL_TX_MIN_GAIN);
	    auto& gain_table            = tx_gain_tables.at(_band);
	    const int gain_index_truncd = int(gain_index);
	    return fine_tune_ad9371_att(gain_table.at(gain_index_truncd), gain_index);
	}

	daughterboard & operator = (const daughterboard &other) = default; // assignment operator
	daughterboard(const daughterboard &other) = default; // copy constructor
	#if 1
	daughterboard(std::string _whichDB)
	:
	POR(0x00),
	whichDB(_whichDB)
			{ } // log
	#endif

	daughterboard(drcMiddleware<T> *_drcMiddlewareSingleton, std::string _whichDB, bool _active = true, bool _ad9371_gain_ctrl = false)
		:
			dbState(state::UNLOCKED),
			dev_clk_Hz(0),
			referenceMiddleware(_drcMiddlewareSingleton),
			dbOrdinal(daughterboard::_globalDaughterboardCount),
			whichDB(_whichDB),
			_app_logger(
					[this](std::int32_t _log_level, std::string _message)->void{
						this->referenceMiddleware->log_helper(_log_level,whichDB + ": " + _message);
					}
				),
			cpldController(
				[this](std::uint32_t frame)->void {
					// cpld internal memory checking
					std::uint16_t chip_index;
					if(((frame >> 16) & 0x7f) < 0x14) { // PS Memory -- 0x05
						chip_index = 0;
					}
					else { // PL Memory -- 0x00
						chip_index = 1;
					}
					abstractRoutingTuple _routingTuple(systemChip::c_CPLD_SPI,chip_index,dbOrdinal);
					// lock the shared reference middleware access calls here
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_write_spi(frame);
				},
				[this](std::uint32_t frame)->std::uint16_t {
					// cpld internal memory checking
					std::uint16_t chip_index;
					if(((frame >> 16) & 0x7f) < 0x14) { // PS Memory -- 0x05
						chip_index = 0;
					}
					else { // PL Memory -- 0x00
						chip_index = 1;
					}
					abstractRoutingTuple _routingTuple(systemChip::c_CPLD_SPI,chip_index,dbOrdinal);
					// set the reference routing tuple to this routing tuple
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_read_spi(frame);
				}
				),
			lmkController(
				[this](std::uint32_t frame)->void {
					abstractRoutingTuple _routingTuple(systemChip::c_LMK04828,0,dbOrdinal); // -- 0x1
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_write_spi(frame);
				},
				[this](std::uint32_t frame)->std::uint32_t {
					abstractRoutingTuple _routingTuple(systemChip::c_LMK04828,0,dbOrdinal); // -- 0x1
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_read_spi(frame);
				},
				[this](std::int32_t _log_level, std::string _message)->void{
					this->referenceMiddleware->log_helper(_log_level,whichDB + " lmkController: " + _message);
				},
				whichDB
				),
			dsaController{{
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,0,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				},
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,1,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				},
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,2,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				},
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,3,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				}
			}},
			cpldGpioController(
				[this](bool _set, std::bitset<4> _channels)->void{ // API now expects bitset
					// Note: This routing is controlled outside of the library API; Uses 'chip select' even though it should not
					// regardless, DON'T lock mutex here
					for(std::uint16_t bit = 0; bit < 4; bit++){ // every bit position that's set
						if(_channels.test(bit)){
							abstractRoutingTuple _routingTuple(systemChip::c_CPLD_GPIO,bit,dbOrdinal); // -- channel
							std::lock_guard<std::mutex> l(this->_set_mutex);
							this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
							this->referenceMiddleware->writeCpldGpio(_set);
						}
					}
				}
				),
			transceiverController( // some of these require dbIndexing information, but no chip select information
				[this](std::uint8_t * _bufIn)->void{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					this->referenceMiddleware->ad9371_write_spi(_bufIn);
				},
				[this](std::uint8_t * _bufIn)->std::uint8_t{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->ad9371_read_spi(_bufIn);
				},
				[this](void)->std::array<std::uint32_t,8>&{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					// the systemChip ^^ and 0 are just fillers.. only need to relay daughterboard indexing information
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->jesd_mem_manager(); 
				},
				// accesses lmk and cpldGpioController, so those MUST be constructed first in the daughterboard list initialization sequence
				[this](std::uint16_t _pattern)->void{
					this->lmkController.requestSysref(_pattern);
				},
				[this](void)->void{
					this->cpldController.mykonosReset();
				},
				[this](std::uint32_t _addressIn, std::uint32_t _value, bool _wr)->std::uint32_t{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->jesd_mem_mapper(_addressIn,_value,_wr); // how should get this? Do we need DB select anymore??
				},
				[this](std::int32_t _log_level, std::string _message)->void{
					this->referenceMiddleware->log_helper(_log_level,whichDB + " Transceiver: " + _message);
				},
				whichDB
			),
			ad9371GpioController(
				[this](void)->std::uint8_t{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					// c_AD9371 and 0 are both filler information, NOT USED HERE
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->readAd9371Gpio();
				}
				),
			POR(0x7F),
			active(_active),
			ad9371_gain_ctrl(_ad9371_gain_ctrl)
			// _profile_log_data(this->whichDB,this->dbOrdinal)
	{
		{ // thread safety
			// really drcMiddleware

			std::lock_guard<std::mutex>	l(this->_set_mutex); // R/W to shared resource
			// this->_routingTuple.db_select = daughterboard::_globalDaughterboardCount;
			daughterboard::_globalDaughterboardCount++;
		}
		std::array<std::uint16_t,2> idx = {0,1}; // chip select indexors
		// std::array<std::uint16_t,2> adfCS = {4,3}; // rx, tx actual chip select artifacts from DRC level
		for(auto &x : idx){
			adfController[x] = adf435x_iface::make_adf4351(
				[this,x](std::vector<uint32_t> vecIn)->void{
					abstractRoutingTuple _routingTuple(systemChip::c_ADF4351,x,dbOrdinal); // RX (?) VS TX (?) LO chip indexor
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					for(auto &it : vecIn){
						this->referenceMiddleware->cpld_write_spi(it);
					}
					return;
				}
				);
		}
	}
	// ~daughterboard() = default;
	daughterboard() = delete;
	~daughterboard(void){
		;
	}
	void resetDaughterboard(void)
	{
		std::stringstream reset_logOut;
		if(active){
			this->ports_tracking=std::bitset<4>(0);
			reset_logOut << "\nPower Off Reset Control";
			// Make a table CHIP | POR
			reset_logOut << "\n-----------------------";
			reset_logOut << "\nCHIP            | POR";
			reset_logOut << "\n-----------------------";
			reset_logOut << "\nCPLD Pathing    |";
			if(this->POR & 0x1){
				reset_logOut << " x";
				this->cpldController.reset(); // call reset routine
				this->POR &= ~(0x1);
			}
			reset_logOut << "\nLMK04828        |";
			if(this->POR & 0x2){
				reset_logOut << " x";
				this->lmkController.reset(); // call reset routine
				this->dev_clk_Hz = 0;
				this->POR &= ~(0x2);
			}
			reset_logOut << "\nADF4351         |";
			if(this->POR & 0x4){
				reset_logOut << " x";
				for(auto &LO : this->adfController){
					LO->reset();
					// LO->_lo_enable(0.0,0.0,false); // @TODO: Does this reset the lock detect?
					// LO->_lo_disable();
				}
				this->POR &= ~(0x4);
			}
			reset_logOut << "\nDSA             |";
			if(this->POR & 0x8){
				reset_logOut << " x";
				for (int i = 0; i < 4; i++){
					this->dsaController[i].double_to_hex(static_cast<double>(0.0));
				}
				this->POR &= ~(0x8);
			}
			reset_logOut << "\nCPLD GPIO POR   |";
			if(this->POR & 0x10){
				reset_logOut << " x";
				this->cpldGpioController.writeGpio(false,0xF); // set all channels off
				this->POR &= ~(0x10);
			}
			// this reset vector MUST be supplied to the transceiver object, thus this POR bit is deprecated

			reset_logOut << "\nTransceiver Obj |";
			if(this->POR & 0x40){
				reset_logOut << " x";
				this->transceiverController.reset();
				this->POR &= ~(0x40);
			}
			reset_logOut << "\n-----------------------";
		} else {
			reset_logOut << "\nDaughterboard--" + this->whichDB + ": INACTIVE";
			reset_logOut << "\nFailed reset operation";
		}
		reset_logOut << "\n";
		this->_app_logger(3,reset_logOut.str()); // DRC_LOG_THREE
		// reset important data members
		this->dbState = state::UNLOCKED;
		return;
	}
};

// since we essentiall #pragma once this header file, this static initialization should ONLY be taken care of once
template<typename T>
std::uint16_t daughterboard<T>::_globalDaughterboardCount = 0; // starts off at 0

template<typename T> std::mutex daughterboard<T>::_set_mutex;

#endif // __MG_DAUGHTERBOARD_HPP__