#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

// these required for syscall stat
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
// need the following two for getting errno
#include <cerrno>
#include <cstring>
#include <vector>
#include <map>

struct csv_logger {
	// manages the log stream object and metadata (name/directory path)
	// this object is abstract -- will be inherited from
	std::string log_file_name; // only public data member
	struct csv_record {
		std::string value;
		bool set; // set to true means the value is valid
	};
private:
	std::map<std::string,struct csv_record> csv_record_container;
	std::vector<std::string> ordered_csv_columns;
public:
	// all FILE MANAGEMENT is protected by THIS boolean -- no file/dir will be created/written to UNLESS SPECIFIED
	bool logging_enabled;
	// all logging objects will manage the output/construction of its name
	csv_logger(std::string _log_file_name, std::vector<std::string> _csv_collumns, bool _logging_enabled = false);
	void insert_element(std::string column_name, std::string value);
	void insert_element(std::string column_name, std::uint64_t value);
	void insert_element(std::string column_name, std::int64_t value);
	void insert_element(std::string column_name, double value);
	// write_log_buffer is implementation specific -- but it is a PUBLIC method
	// virtual void write_log_buffer(void) = 0; // pure virtual method -- needs to format + send to log helper method
// protected:
	// NOTE: these two ONLY get called inside write_log_buffer implementation

	void flush_log_data(void); // write to file safely using csv_record_container
	// virtual void clear_log_buffer(void) = 0;
private:
	// NEED to make sure this relative_directory
	const static std::string relative_directory;
	std::ofstream log_file_stream; // no direct access to this
};