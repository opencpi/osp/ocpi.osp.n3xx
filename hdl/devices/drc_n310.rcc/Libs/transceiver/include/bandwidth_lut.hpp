#ifndef __BANDWIDTH_LUT_H__
#define __BANDWIDTH_LUT_H__
#pragma once

#include <set>
#include <map>
#include <utility> // for pair
#include <vector>
#include <type_traits> // templating for map and set helper functions -- treating map keys like set values (vice versa)
#include <transceiver.h>

typedef struct // overwriting previous defintion of this struct
{
    std::int8_t gain_dB;         /*!< Filter gain in dB*/
    std::uint8_t numFirCoefs;    /*!< Number of coefficients in the FIR filter */
    std::vector<std::int16_t> coefs;         /*!< A pointer to an array of filter coefficients */
} SelfContained_mykonosFir_t;

typedef struct {
	std::uint32_t clocks__deviceClock_kHz;
	std::uint32_t clocks__clkPllVcoFreq_kHz;
	mykonosVcoDiv_t clocks__clkPllVcoDiv;
	std::uint8_t clocks__clkPllHsDiv;
	// std::uint32_t clocks__clkPllRefClkDiv; // unused information generated from filter wizard profile

	// mykonosRxProfile_t
	std::uint8_t rx__adcDiv;
	std::uint8_t rx__rxFirDecimation;
	std::uint8_t rx__rxDec5Decimation;
	std::uint8_t rx__enHighRejDec5;
	std::uint8_t rx__rhb1Decimation;
	std::uint32_t rx__iqRate_kHz;
	std::uint32_t rx__rfBandwidth_Hz;
	std::uint32_t rx__rxBbf3dBCorner_kHz;
	//struct
	SelfContained_mykonosFir_t rx__filter; // this is a * pointer in mykonosRxProfile_t -- could cause issues if swapped over to that method

	//array
	std::uint16_t rx__adc_profile[16]; // std::array or just normal array? there's a numerator

	// mykonosRxProfile_t
	std::uint8_t orx__adcDiv;
	std::uint8_t orx__rxFirDecimation;
	std::uint8_t orx__rxDec5Decimation;
	std::uint8_t orx__enHighRejDec5;
	std::uint8_t orx__rhb1Decimation;
	std::uint32_t orx__iqRate_kHz;
	std::uint32_t orx__rfBandwidth_Hz;
	std::uint32_t orx__rxBbf3dBCorner_kHz;
	//struct
	SelfContained_mykonosFir_t orx__filter; // this is a * pointer in mykonosRxProfile_t -- could cause issues if swapped over to that method

	//array
	std::uint16_t orx__adc_profile[16]; // std::array or just normal array? there's a numerator
	//array
	std::uint16_t orx__lpbk_adc_profile[16];

	// mykonosTxProfile_t
	mykonosDacDiv_t tx__dacDiv;
	std::uint8_t tx__txFirInterpolation;
	std::uint8_t tx__thb1Interpolation;
	std::uint8_t tx__thb2Interpolation;
	std::uint8_t tx__txInputHbInterpolation;
	std::uint32_t tx__iqRate_kHz;
	std::uint32_t tx__primarySigBandwidth_Hz;
	std::uint32_t tx__rfBandwidth_Hz;
	std::uint32_t tx__txDac3dBCorner_kHz;
	std::uint32_t tx__txBbf3dBCorner_kHz;
	//struct
	SelfContained_mykonosFir_t tx__filter; // this is a * pointer in mykonosTxProfile_t -- could cause issues if swapped over to that method
} configuration_block;

/*
	template <class K, class V>
	class bandwidth_mapper : public std::map<K,V> {
		using value_type_v = typename std::map<K,V>::value_type;
	public:
		bandwidth_mapper(std::initializer_list<value_type_v> init)
			: std::map<K,V>(init) {}
	};
*/

using rx_tx_key = std::pair<double,double>;

using duo_channel_dictionary = std::map<rx_tx_key,configuration_block>;

// creating helper template
namespace { // anonymous namespace -- scoped helper functions
	template<typename... Ts> struct make_void { typedef void type; };
	template<typename... Ts> using void_t = typename make_void<Ts...>::type;


	// for all OTHER cases (namely, our SET container type) -- returns FALSE (std::false_type) (when using ::value)
	template<typename T, typename = void>
	struct is_associative : std::false_type { };

	// for MAP container cases -- returns TRUE (std::true_type) (when using ::value)
	template<typename T>
	struct is_associative<T, void_t<typename T::mapped_type>> : std::true_type { };

	template<typename V, typename T>
	auto extract_key(T&& it) -> typename std::enable_if<is_associative<V>::value, decltype(it->first)>::type {
		return it->first;
	}


	template<typename V, typename T>
	auto extract_key(T&& it) -> typename std::enable_if<not is_associative<V>::value, decltype(*it)>::type {
		return *it;
	}
}


// dual_key_map will be part of the STATIC lookup table object
class dual_key_map : public duo_channel_dictionary {
	using value_type_v = typename duo_channel_dictionary::value_type;
	using fundamental_element = std::pair<rx_tx_key,configuration_block>;
public:
	dual_key_map(std::initializer_list<value_type_v> init)
		// : duo_channel_dictionary(init) // WILL MANUALLY INITIALIZE THIS
		{
			// instead of _directly_ passing initializer list to base MAP class, manually parsing it here
			for(auto it : init){
				insert(it);
			}
		}
	// data members
	std::map<double,std::set<double>> _rx_sided_keypairs; // set of TX bandwidths that are mappable with given rx bandwidth key
	std::map<double,std::set<double>> _tx_sided_keypairs; // set of RX bandwidths that are mappable with given tx bandwidth key
	// defining our own insertion method to properly track data members above
	void insert(fundamental_element key_val){
		auto rx_key = key_val.first.first;
		auto tx_key = key_val.first.second;
		// rx
		if(_rx_sided_keypairs.find(rx_key)==_rx_sided_keypairs.end()){
			// initialize the set at this key location
			_rx_sided_keypairs.insert({rx_key,{tx_key}});
		} else {
			// add to the mapped set
			_rx_sided_keypairs.at(rx_key).insert(tx_key);
		}
		// tx
		if(_tx_sided_keypairs.find(tx_key)==_tx_sided_keypairs.end()){
			// initialize the set at this key location
			_tx_sided_keypairs.insert({tx_key,{rx_key}});
		} else {
			// add to the mapped set
			_tx_sided_keypairs.at(tx_key).insert(rx_key);
		}
		// finally insert using our inherited container and its method
		duo_channel_dictionary::insert(key_val);
	}
};

// bandwidth_mapper object is LOCAL to a Transceiver object
// build with -DLOG_BANDWIDTH_MAPPER for extra standard output to debug this container some more
class bandwidth_mapper {
public:
	bandwidth_mapper()
		{
			#ifdef LOG_BANDWIDTH_MAPPER
				std::cout << "initializing bandwidth_mapper\n";
			#endif
			reset();
			// sort the supported sampling rates
			for(auto it : global_bandwidth_profile_lut){
				#ifdef LOG_BANDWIDTH_MAPPER
					std::cout << it.first << std::endl;
				#endif
				supported_sampling_rates.insert(it.first);
			}
		}
	// data members
	const static std::map<double,dual_key_map> global_bandwidth_profile_lut;
	std::set<double> supported_sampling_rates;
	double _sampling_rate;
	double _rx_bandwidth, _tx_bandwidth;
	// std::pair<double,double> bandwidth_pair;
	// methods
	void reset(void){
		// explicitly reset its tracking
		_sampling_rate = _rx_bandwidth = _tx_bandwidth = -1;
	}

	template <typename T> // container type
	double get_nearest(double _requested_value, T _container_to_search){
		// typename T::iterator cur_iter, prev_iter;
		typename T::const_iterator cur_iter, prev_iter;
		#ifdef LOG_BANDWIDTH_MAPPER
			std::cout << "Inside bandwidth_mapper::get_nearest call\n";
			for(auto it = _container_to_search.begin(); it != _container_to_search.end(); ++it){
				std::cout << extract_key<T>(it) << std::endl;
			}
		#endif

		double ret_val;
		cur_iter = _container_to_search.lower_bound(_requested_value);
		if(cur_iter == _container_to_search.end()){
			// requested higher than highest value in set
			ret_val = extract_key<T>(_container_to_search.rbegin());
		} else if(cur_iter == _container_to_search.begin()){
			// requested lower than lowest value in set
			ret_val = extract_key<T>(_container_to_search.begin());
		} else {
			// compare closest two iterations to see which is closest
			prev_iter = std::prev(cur_iter);
			auto cur_val = extract_key<T>(cur_iter), prev_val = extract_key<T>(prev_iter);
			if((_requested_value - prev_val) < (cur_val - _requested_value)){
				ret_val = prev_val;
			} else {
				ret_val = cur_val;
			}
		}
		return ret_val;
	}

	template <typename T> // container type
	double safe_saturate_upwards(double _requested_value, T _container_to_search){
		typename T::const_iterator search_iter;
		search_iter = _container_to_search.lower_bound(_requested_value);
		return (search_iter != _container_to_search.end())? extract_key<T>(search_iter) : // return current value
															extract_key<T>(_container_to_search.rbegin()); // return highest value
	}

	double set_sampling_rate(double _requested_rate){
		#ifdef LOG_BANDWIDTH_MAPPER
			std::cout << "Inside bandwidth_mapper::set_sampling_rate call\n";
			std::cout << "_rx_bandwidth value: "<<_rx_bandwidth<<"\n";
			std::cout << "_tx_bandwidth value: "<<_tx_bandwidth<<"\n";
			for(auto it : global_bandwidth_profile_lut){
					std::cout << it.first << std::endl;
			}
		#endif
		_sampling_rate = get_nearest<decltype(global_bandwidth_profile_lut)>(_requested_rate,global_bandwidth_profile_lut);
		return _sampling_rate;
	}
	configuration_block set_bandwidth(bool _rx, double _requested_bandwidth){
		// ideal -- saturate UPWARDS
		// first: check to see if the OTHER bandwidth is selected
		auto &other_bandwidth = (_rx) ? _tx_bandwidth : _rx_bandwidth;
		auto &selected_bandwidth = (_rx) ? _rx_bandwidth : _tx_bandwidth;
		auto &tmp = global_bandwidth_profile_lut.at(_sampling_rate);
		auto &other_sided_keypairs = (_rx) ? tmp._tx_sided_keypairs : tmp._rx_sided_keypairs;
		auto &selected_sided_keypairs = (_rx) ? tmp._rx_sided_keypairs : tmp._tx_sided_keypairs;
		// better way to do ^^?
		#ifdef LOG_BANDWIDTH_MAPPER
			std::cout << "Inside bandwidth_mapper::set_bandwidth\n";
		#endif
		if(other_bandwidth < 0){
			// will default set the other bandwidth to MAX available value
			selected_bandwidth = safe_saturate_upwards<std::remove_reference<decltype(selected_sided_keypairs)>::type>(_requested_bandwidth,selected_sided_keypairs);
			// set the other bandwidth to maximum related value
			other_bandwidth = *selected_sided_keypairs.at(selected_bandwidth).rbegin();
		} else {
			// constrained to the other bandwidth
			selected_bandwidth = safe_saturate_upwards<std::remove_reference<decltype(other_sided_keypairs.at(other_bandwidth))>::type>(_requested_bandwidth,other_sided_keypairs.at(other_bandwidth));
		}

		return get_configuration_block();
	}
	const configuration_block& get_configuration_block(void){
		// generate our sub_key from the two bandwidths
		rx_tx_key sub_key{_rx_bandwidth,_tx_bandwidth};
		// access the global LUT for the configuration_block structure
		return global_bandwidth_profile_lut.at(_sampling_rate).at(sub_key);
	}
	double get_bandwidth(bool _rx){
		if(_rx){
			return _rx_bandwidth;
		} else {
			return _tx_bandwidth;
		}
	}
	double get_sampling_rate(void){
		return _sampling_rate;
	}
};


#endif // __BANDWIDTH_LUT_H__
