#ifndef __TRANSCEIVER_H__
#define __TRANSCEIVER_H__

// this will be included in jesd/mykonos C library source code files
// bottom has C related things, top/mid is all about the class API


#include <stdint.h>

#ifdef __cplusplus
#include <modular_logger.hpp>
#include <memory> // unique_ptr for LUT stuff
#include <algorithm>
#include <iterator>
#include <bitset>
#include <vector>
#include <map>
#include <chrono>
#include <functional>
#include <mutex>
#include <cerrno>
#include <iostream>
#include <array>
#include <cstdint>
#include <cmath> // for logarithmic and power
// #include <cstring> // ?
//#include <memory>
#include <iomanip> // std::setfill
#include <sstream> // std::stringstream
#include <cassert> // assertion

// include necessary headers for class

// required mykonos headers
extern "C" {
// #include <Mykonos_M3.h> // firmware binary to flash
	// can't put ^^ in this header file
#include <mykonos.h>
#include <common.h>
#include <clk_axi_clkgen.h>
#include <axi_adxcvr.h>
#include <axi_jesd204_rx.h>
#include <axi_jesd204_tx.h>
#include <axi_dac_core.h>
#include <axi_adc_core.h>
#include <error.h>
#include <t_mykonos_gpio.h>
#include <mykonos_gpio.h>
#include <mykonos_macros.h>
} // extern "C"

// swap macro breaks <set> header file
#ifdef swap
#undef swap
#endif
#include <bandwidth_lut.hpp>


// needed JESD headers
#include <unistd.h>
// #include <>
// for axi memory access via linux drivers
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h> // strerror
// #include <n310parameters.h>

#undef max
#undef abs
#include <cmath>

#define ADDR_REPLACE	(0) // placeholders in cpp code
#define DEBUG_CLOCKS
// #define ALPHA_RELEASE // state of the DRC
#define BETA_RELEASE

#ifdef DEBUG_CLOCKS
#include "config.h"
#endif

struct drcConfig {
	bool rx;
	// transmitter or receiver tuning
	double tuning_freq_MHz;
	double bandwidth_3dB_MHz;
	double sampling_rate_Msps;
	bool samples_are_complex;
	double gain_dB;
	bool gain_mode; // string converted here
	double tolerance_tuning_freq_MHz;
	double tolerance_bandwidth_3dB_MHz;
	double tolerance_sampling_rate_Msps;
	double tolerance_gain_dB;
	std::uint8_t rf_port_num;
	// 4 total 'ports' but really transceiver only can configure 2
		// --> CAN ENABLE 4 individually though? @TODO
};

struct LO_div_by_2_tracker {
	struct LO_div_by_2_bounds {
		std::uint64_t lower_limit_Hz, upper_limit_Hz;
	};
	// going to include UNDOCUMENTED LOWER BAND LIMITS (300-400)
	std::array<struct LO_div_by_2_bounds,5> LO_bounds_obj;
	// rx and tx tracker
	std::uint64_t rx_tuning_Hz;
	std::int64_t _dummy;// = 0xDEADC0DE; // constant
	std::uint64_t tx_tuning_Hz;
	// get, set methods
	void set_tuning_hz(bool _rx, std::uint64_t tuning_Hz);
	std::uint64_t get_tuning_hz(bool _rx);
	struct LO_div_by_2_bounds * get_lo_bounds_ref(bool _rx);
	void reset(void);
};

class Transceiver {
private:
	#ifdef LOG_TIME_STAMPS
		std::chrono::high_resolution_clock::time_point transceiver_epoch;
	#endif
public:
	using write_spi_t = std::function<void(std::uint8_t *)>; // frame pointer
	using read_spi_t = std::function<std::uint8_t(std::uint8_t *)>; // frame pointer
	using mem_access_t = std::function
							<
								std::array<std::uint32_t,8>&(void) // pass void, return reference
							>; // initialize Transceiver pageArray std::array

	using simJesdAccess_t = std::function<std::uint32_t(std::uint32_t, std::uint32_t, bool)>;

	using requestSysref_t = std::function<void(std::uint16_t)>;
	using resetGpio_t = std::function<void(void)>;

	using log_access_t = std::function<void(std::int32_t,std::string)>;

	// initialize callback methods, and 0-ize JESD / Mykonos structures
	// the functors passed here access the middleware of the RCC proxy Worker
	Transceiver(write_spi_t write_spi_fn, read_spi_t read_spi_fn, mem_access_t mem_access_fn,
				requestSysref_t requestSysref_in,resetGpio_t resetGpio_fn, simJesdAccess_t simJesdAccess_fn,
				log_access_t app_logger, std::string identifier = "", bool _unitTesting = false);
	~Transceiver(void); // simply calls reset
	inline void init_c_call(void); // interact with global .data variable Transceiver_Callback with 

	void class_MYKONOS_setRx1ManualGain(std::uint8_t gain);
	void class_axi_jesd204_rx_init(void);

	void class_axi_clkgen_set_rate(uint32_t rate);
	void class_axi_clkgen_get_rate(uint32_t * rate);
	void class_axi_clkgen_init(uint32_t parent_rate);

	void class_MYKONOS_initDigitalClocks(void);
	// void class_MYKONOS_calculateDigitalClocks(uint32_t *hsDigClk_kHz, uint32_t *hsDigClkDiv4or5_kHz);
	void class_MYKONOS_getProductId(uint8_t *productId);

	void defaultConstructorSequence(void);
	void initializeFirFilters(void);
	// this is going to be the main method
	// @TODO: Add parameters
	void configure_transceiver(struct drcConfig config);
	// @TODO: make private method for configure_jesd/mykonos
	void configure_jesd(bool _rx);
	bool configure_mykonos(bool _rx);
	bool headless(bool _rx);
	bool run_configuration(void);
	bool runningStatus(void);
	void state_reset(bool invoke_callbacks); // GPIO/myk/jesd state reset
	void reset(bool invoke_callbacks = true); // resets the tracking of the ports as well


    double get_tuning_freq_MHz(std::uint8_t constrained_rf_port_num);

    double get_bandwidth_3dB_MHz(std::uint8_t constrained_rf_port_num);

    double get_sampling_rate_Msps(std::uint8_t constrained_rf_port_num);

	void logSRC(bool configuration_passed);
	void logMykonosError(mykonosErr_t x);
	enum class radioState {
		OFF,
		ON
	};
	void radioControl(radioState _toggle);

	mykonosGpioErr_t gpioTester(std::uint32_t _scenario);
	std::int32_t calcDPathRateConversion_x2(bool _rx);
	std::int32_t calcClkRateConversion_x2(void);
	bool validateVCORate(void);
	bool rateSpaceTester(std::vector<std::string> _constraints, bool _rx);

	void set_bandwidth(bool _rx, double bandwidth_3dB_MHz, double sampling_rate_Msps);
	void set_configuration_block(const configuration_block& _configuration_block);
	double set_gain(std::uint16_t port_num, const double value);
	double get_gain(std::uint16_t port_num);
	std::uint8_t _convert_rx_gain_to_mykonos(const double gain);
	double _convert_rx_gain_from_mykonos(const std::uint8_t gain);
	std::uint16_t _convert_tx_gain_to_mykonos(const double gain);
	double _convert_tx_gain_from_mykonos(const std::uint16_t gain);

	// JESD Memory Manager methods
	// must be public
	std::uint32_t				axi_access_rw(std::uint32_t addressIn, std::uint32_t val, bool r_w);
	// these can be private
	void						init_mem(void); // make calls to callback middleware
	void						deinit_mem(void);
	inline std::uint32_t		calc_page_offset(std::uint32_t addr);
	std::int8_t					binarySearch(std::uint32_t pageRequest, std::int8_t idx0, std::int8_t idxN);
	void						init_page_size(void);

	enum class transceiverStateAlias {
		JESD,
		MYKONOS
	} enabledStateMachine;
	enum class jesdState { // 3-bit
		initCLKGEN,
		initJESD,
		initADXCVR,
		txClkEnable, // begining of 'mixed' states
		enableSYSREF, // also rxClkEnable
		finalJESD
	};
	enum class mykonosState { // 4-bit
		initialize,
		checkClkPLL,
		loadARM,
		setRFPLL,
		BBPGPIO,
		manualGains,
		attenuate,
		ARMCalibrate,
		ExtPACalibrate,
		bringUpJESD,
		enableSysrefToDeframer, // this is being placed in JESD section
		framerCheck,
		deframerCheck,
		enableCalTrack,
	};

	enum class portConfig {
		MULTI_PORT,	// 0
		SINGLE_PORT,	// 1
		NO_PORT
	};

	enum class portName {
		TX,
		RX
	};


	std::uint8_t							configState;
		// MSNibble == JESD state
		// LSNibble == Myko state

	inline std::uint8_t			get_config_state(transceiverStateAlias which);
	inline void					set_config_state(transceiverStateAlias which, std::uint8_t newVal);
	inline void					reset_config_state(void);
	inline void					set_done_flag(bool value);
	inline portConfig			portConfigTest(void);
	inline void					portSet(std::uint8_t portSet);
	bool					channel_configured(std::uint8_t _port_num);

	// add two simple 1 : 1 calls of JESD library and mykonos



	// private:
	write_spi_t 		_write_myk_fn;
	read_spi_t 			_read_myk_fn;
	mem_access_t 		_access_jesd_fn;
	simJesdAccess_t		_jesdSim_fn;
	requestSysref_t		_requestSysref_fn;

	resetGpio_t _resetGpio_fn;
	log_access_t _app_logger; // application level logging
	bool unsupervisedLogging;

	// per daughterboard basis, not for each daughterboard
	bool			clockTreeInitialized;

	// rx / tx profiles, per daugterboard basis
	// bool			portsActive[2][2];
	// per daughterboard basis
	std::bitset<2> 	portsActive;

	struct LO_div_by_2_tracker ad9371_LO_tracker;
	struct LO_div_by_2_tracker * _get_tracker_ref(void);
	void update_tuning_frequency(bool _rx, double tuning_freq_MHz);

	// following two members are defaulted FALSE
	bool			recoveryEnabled; // allows the headless.c alg to try the rateSpaceTester looping recovery method upon any (mykonos api) failure
	bool			first_run_configurations_framer_deframer_failure;
	bool			unitTesting;

	// all important library data members: // -------- will separate each major structure

	// Note: Only structures will be listed, enumerations and other number like sub members get set literally

	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ START OF MYKONOS STRUCTURES $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	// ------------------------------------------------------------------------------
	mykonosErr_t							mykError;
	std::string								mykError_enumString;
	std::int32_t							jesdApiStatus;
	ADI_ERR									adiError;
	mykonosGpioErr_t 						mykGpioErr;

	std::uint8_t							errorFlag, errorCode;
	mykonosInitCalStatus_t 					initCalStatus;
	const bool								dualCalCoupling;
	
	std::uint8_t 							framerStatus;
	std::uint8_t 							obsFramerStatus;
	std::uint8_t 							deframerStatus;

	std::uint8_t 							pllLockStatus;
	std::uint32_t 							trackingCalMask;
	std::uint32_t 							initCalMask;

	// ------------------------------------------------------------------------------
	mykonosDevice_t 						mykDevice; // major structure

	// below follows config.c with some extra management
	std::array<std::int16_t*,6> txFirCoefs; // initialize this to the 6 pointers below
	std::int16_t txFirCoefs16[16];
	std::int16_t txFirCoefs32[32];
	std::int16_t txFirCoefs48[48];
	std::int16_t txFirCoefs64[64];
	std::int16_t txFirCoefs80[80];
	std::int16_t txFirCoefs96[96];

	mykonosFir_t txFir;

	std::array<std::int16_t*,3> rxFirCoefs; // initialize this to the 6 pointers below
	std::int16_t rxFirCoefs24[24];
	std::int16_t rxFirCoefs48[48];
	std::int16_t rxFirCoefs72[72];

	mykonosFir_t rxFir;

	std::array<std::int16_t*,3> obsrxFirCoefs; // initialize this to the 6 pointers below
	std::int16_t obsrxFirCoefs24[24];
	std::int16_t obsrxFirCoefs48[48];
	std::int16_t obsrxFirCoefs72[72];	

	mykonosFir_t obsrxFir;

	std::array<std::int16_t*,3> snifferFirCoefs; // initialize this to the 6 pointers below
	std::int16_t snifferFirCoefs24[24];
	std::int16_t snifferFirCoefs48[48];
	std::int16_t snifferFirCoefs72[72];

	mykonosFir_t snifferRxFir;

	mykonosJesd204bFramerConfig_t rxFramer;

	mykonosJesd204bFramerConfig_t obsRxFramer;

	mykonosJesd204bDeframerConfig_t deframer;

	mykonosRxGainControl_t rxGainControl;

	mykonosORxGainControl_t orxGainControl;

	mykonosSnifferGainControl_t snifferGainControl;

	mykonosPeakDetAgcCfg_t rxPeakAgc;

	mykonosPowerMeasAgcCfg_t rxPwrAgc;

	mykonosAgcCfg_t rxAgcConfig;

	mykonosPeakDetAgcCfg_t obsRxPeakAgc;

	mykonosPowerMeasAgcCfg_t obsRxPwrAgc;

	mykonosAgcCfg_t obsRxAgcConfig;

	mykonosRxProfile_t rxProfile;

	mykonosRxProfile_t orxProfile;

	mykonosRxProfile_t snifferProfile;

	mykonosTxProfile_t txProfile;

	mykonosDigClocks_t mykonosClocks;

	mykonosRxSettings_t rxSettings;

	mykonosDpdConfig_t dpdConfig;

	mykonosClgcConfig_t clgcConfig;

	mykonosVswrConfig_t vswrConfig;

	mykonosTxSettings_t txSettings;

	mykonosObsRxSettings_t obsRxSettings;

	mykonosArmGpioConfig_t armGpio;

	mykonosGpio3v3_t gpio3v3;

	mykonosGpioLowVoltage_t gpio;

	mykonosAuxIo_t mykonosAuxIo;

	spiSettings_t mykSpiSettings;

	mykonosTempSensorConfig_t tempSensor;

	mykonosTempSensorStatus_t tempStatus;

	// ------------------------------------------------------------------------------

	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ START OF JESD STRUCTURES $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

	// ------------------------------------------------------------------------------
	std::uint32_t 			rx_lane_rate_khz;
	std::uint32_t			rx_div40_rate_hz;
	std::uint32_t			tx_lane_rate_khz;
	std::uint32_t			tx_div40_rate_hz;

	// these two are pointless, but will stick to convention of the C library
	struct axi_clkgen		*rx_clkgen, 	*tx_clkgen;
	struct axi_clkgen_init 	rx_clkgen_init, tx_clkgen_init;

	struct axi_jesd204_rx 	*rx_jesd;
	struct jesd204_rx_init 	rx_jesd_init;

	struct axi_jesd204_tx	*tx_jesd;
	struct jesd204_tx_init	tx_jesd_init;

	struct adxcvr			*rx_adxcvr,		*tx_adxcvr;
	struct adxcvr_init		rx_adxcvr_init,	tx_adxcvr_init;

	struct axi_dac			*tx_dac;
	struct axi_dac_init		tx_dac_init;

	struct axi_adc			*rx_adc;
	struct axi_adc_init		rx_adc_init;
	// ------------------------------------------------------------------------------

	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ END OF JESD STRUCTURES $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ START OF MEMORY ACCESS data members $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	static std::uint32_t						pageSize;
	// volatile std::uint32_t 			*virtualPageArray[9]; // returned via callback
	std::array<volatile std::uint32_t *,8>		virtualPageArray; // array of pointers to VA space, volatile, mapped to physical mem
	static std::int32_t							fd; // two transceivers will BOTH access dev/mem
	static std::mutex							_set_mutex;
	static std::int8_t							activeTransceivers;
	bool										axiJesdMemInit;
	// std::uint32_t					*pageAddrs; // base address of physical memory locations
	std::array<std::uint32_t,8>					pageAddrs;	// 8 peripherals needed to control
	// not storing LED address in here
	// static const std::uint8_t 	pagesAccessed;// length of pages accessed by one transceiver

	// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ END OF MEMORY ACCESS data members $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

	csv_logger profile_log_data;
	static std::uint16_t profile_index;
	bool original_profile; // NOT from recovery_method
	std::string c_lib_call;

	// referenced (READ ONLY) by all transceiver objects
	// static const std::map<double,bandwidth_mapper<std::pair<double,double>,configuration_block>> bandwidth_profile_lut;
	bandwidth_mapper _bandwidth_mapper;
};

#else
#include <stdbool.h> // need boolean type
// typedef struct Transceiver Transceiver; // can't work with this because C++ needs to manage a pointer of this type

#endif
// does the C code even need access to this declaration in my instance?
// typedef struct Transceiver_C; // forward declaration of opaque pointer
// this will need to be casted to (Transciever *) when used in callback functions



// static translational unit to communicate between C api and C++
// Transceiver_C * thisTransceiver; // should have one per daughter card
// common.c reference this now


#ifdef __cplusplus
extern "C" {
#endif

// in order to make calls linkable to C
#include <delay.h>
#if defined(__STDC__) || defined(__cplusplus) //
	// link C++ STL Chrono from C++ defs to C library
	// extern void mdelay(uint32_t usecs); // included in delay.h
	// extern void udelay(uint32_t msecs);
	// Beginning of C -> C++ wrappers
	// extern void c_function(Transceiver *); /* ANSI C prototypes */

	// C - C++ separator for callback functions -> Map from C call to Object method call
	extern uint32_t page_memory_cpp_callback(uint32_t addressIn, uint32_t val, bool r_w);
	extern void spi_write_cpp_callback(uint8_t * frame);
	extern uint8_t spi_read_cpp_callback(uint8_t * frame);
	extern void common_logging_access(int32_t log_level, const char * log_message);


#else
	// K&$ Standards, for now don't target if build error come back and figure it out
	// https://isocpp.org/wiki/faq/mixing-c-and-cpp#cpp-objs-passed-to-c
#endif

#ifdef __cplusplus
}
#endif

#endif//__TRANSCEIVER_H__
