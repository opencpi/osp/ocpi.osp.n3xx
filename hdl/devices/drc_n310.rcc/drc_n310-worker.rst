.. drc_n310 RCC worker

.. _drc_n310-RCC-worker:


``drc_n310`` RCC Worker
=======================

Detail
------
RCC Implementation of ``drc_n310`` Digital Radio Controller (DRC) for the ``n310`` platform.  The below diagram illustrates the ``rf_port_num`` mapping to ``n310`` RF ports:  

.. _n310_drc_channel_mapping:

.. figure:: ../drc_n310.comp/DRC_Channel_Mapping.svg
   :alt: N310 DRC Channel Mapping
   :align: center
   :height: 1200px
   :width: 1200px

   ``n310`` DRC Channel Mapping

* **DRC RF Port Number**: 3-Bit Mapping
   * rf_port_num[2]: {0:DBA, 1:DBB} - Daughterboard (DB) Selection
   * rf_port_num[1]: {0:TX, 1:RX} - Channel Type
   * rf_port_num[0]: {0:CH0, 1:CH1} - Channel Selection
* **Max Deployment Channel**: 8
   * 4 Tx Channels (2 per DB)
   * 4 Rx Channels (2 per DB)
* **AD9371 Constraint**: Same Channel Profile per Channel Type
   * Max Tx Channel Profiles: 2 (1 per DB)
   * Max Rx Channel Profiles: 2 (1 per DB)



.. ocpi_documentation_worker::

   dba_en: Build-time parameter for Daugtherboard A: ``true`` = ``DBA_jesd_axi_mapper`` and ``DBA_gpio`` device workers are enabled within the slave assembly, ``false`` = All device workers are disabled within the slave assembly. 
   dba_tx0_en: Build-time parameter for Daugtherboard A TX0 channel:  ``true`` = ``DBA_qDAC0`` and ``DBA_qdac_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBA_qDAC0`` and ``DBA_qdac_sub`` device workers are disabled within the slave assembly.
   dba_tx1_en: Build-time parameter for Daugtherboard A TX1 channel:  ``true`` = ``DBA_qDAC1`` and ``DBA_qdac_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBA_qDAC0`` and ``DBA_qdac_sub`` device workers are disabled within the slave assembly.
   dba_rx0_en: Build-time parameter for Daugtherboard A RX0 channel:  ``true`` = ``DBA_qADC0`` and ``DBA_qadc_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBA_qADC0`` and ``DBA_qadc_sub`` device workers are disabled within the slave assembly.
   dba_rx1_en: Build-time parameter for Daugtherboard A RX1 channel:  ``true`` = ``DBA_qADC1`` and ``DBA_qadc_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBA_qADC1`` and ``DBA_qadc_sub`` device worker are disabled within the slave assembly.
   
   dbb_en: Build-time parameter for Daugtherboard B: ``true`` = ``DBB_jesd_axi_mapper`` and ``DBB_gpio`` device workers are enabled within the slave assembly, ``false`` = All device workers are disabled within the slave assembly. 
   dbb_tx0_en: Build-time parameter for Daugtherboard B TX0 channel:  ``true`` = ``DBB_qDAC0`` and ``DBB_qdac_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBB_qDAC0`` and ``DBB_qdac_sub`` device workers are disabled within the slave assembly.
   dbb_tx1_en: Build-time parameter for Daugtherboard B TX1 channel:  ``true`` = ``DBB_qDAC1`` and ``DBB_qdac_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBB_qDAC0`` and ``DBB_qdac_sub`` device workers are disabled within the slave assembly.
   dbb_rx0_en: Build-time parameter for Daugtherboard B RX0 channel:  ```true`` = ``DBB_qADC0`` and ``DBB_qadc_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBB_qADC0`` and ``DBB_qadc_sub`` device workers are disabled within the slave assembly.
   dbb_rx1_en: Build-time parameter for Daugtherboard B RX1 channel:  ``true`` = ``DBB_qADC1`` and ``DBB_qadc_sub`` device workers are enabled within the slave assembly, ``false`` = ``DBB_qADC1`` and ``DBB_qadc_sub`` device worker are disabled within the slave assembly.
   
   dba_cic_int_en: Build-time parameter for CIC Interpolation Workers for Daughterboard A:  ``true`` = ``DBA_cic_int0`` and ``DBA_cic_int1`` device workers are enabled within the slave assembly, ``false`` = CIC Interpolation device workers are disabled within the slave assembly.
   dba_cic_dec_en: Build-time parameter for CIC Decimation Workers for Daugtherboard A:  ``true`` = ``DBA_cic_dec0`` and ``DBA_cic_dec01`` device workers are enabled within the slave assembly, ``false`` = CIC Decimation device workers are disabled within the slave assembly.
   dbb_cic_int_en: Build-time parameter for CIC Interpolation Workers for Daugtherboard B:  ``true`` = ``DBB_cic_int0`` and ``DBB_cic_int1`` device workers are enabled within the slave assembly, ``false`` = Interpolation device workers are disabled within the slave assembly.
   dbb_cic_dec_en: Build-time parameter for CIC Decimation Workers for Daugtherboard B:  ``true`` = ``DBA_cic_dec0`` and ``DBA_cic_dec1`` device workers are enabled within the slave assembly, ``false`` = CIC Decimation device workers are disabled within the slave assembly.
   
   dba_int0_up_sample_factor: Interpolation Factor for Daughterboard A TX0 CIC (``DBA_cic_int0``) device worker.  
   dba_int0_scale_output: Output scale factor for Daughterboard A TX0 CIC (``DBA_cic_int0``) device worker. The output is divided by :math:`2^{\texttt{dba_int0_scale_output}}`.
   dba_int0_flush_length: The number of zero samples inserted into the Daughterboard A TX0 CIC (``DBA_cic_int0``) device worker on receipt of a flush opcode.
   dba_int1_up_sample_factor: Interpolation Factor for Daughterboard A TX1 CIC (``DBA_cic_int1``) device worker.
   dba_int1_scale_output: Output scale factor for Daughterboard A TX1 CIC (``DBA_cic_int1``) device worker. The output is divided by :math:`2^{\texttt{dba_int1_scale_output}}`.
   dba_int1_flush_length: The number of zero samples inserted into the Daughterboard A TX1 CIC (``DBA_cic_int1``) device worker on receipt of a flush opcode.
   
   dba_dec0_down_sample_factor: Decimation Factor for Daughterboard A RX0 CIC (``DBA_cic_dec0``) device worker.  
   dba_dec0_scale_output: Output scale factor for Daughterboard A RX0 CIC (``DBA_cic_dec0``) device worker. The output is divided by :math:`2^{\texttt{dba_dec0_scale_output}}`.
   dba_dec0_flush_length: The number of zero samples inserted into the Daughterboard A RX0 CIC (``DBA_cic_dec0``) device worker on receipt of a flush opcode.
   dba_dec1_down_sample_factor: Decimation Factor for Daughterboard A RX1 CIC (``DBA_cic_dec1``) device worker.  
   dba_dec1_scale_output: Output scale factor for Daughterboard A RX1 CIC (``DBA_cic_dec1``) device worker. The output is divided by :math:`2^{\texttt{dba_dec1_scale_output}}`.
   dba_dec1_flush_length: The number of zero samples inserted into the Daughterboard A RX1 CIC (``DBB_cic_dec1``) device worker on receipt of a flush opcode.
   
   dbb_int0_up_sample_factor: Interpolation Factor for Daughterboard B TX0 CIC (``DBB_cic_int0``) device worker.  
   dbb_int0_scale_output: Output scale factor for Daughterboard B TX0 CIC (``DBB_cic_int0``) device worker. The output is divided by :math:`2^{\texttt{dbb_int0_scale_output}}`.
   dbb_int0_flush_length: The number of zero samples inserted into the Daughterboard B TX0 CIC (``DBB_cic_int0``) device worker on receipt of a flush opcode.
   dbb_int1_up_sample_factor: Interpolation Factor for Daughterboard B TX1 CIC (``DBB_cic_int1``) device worker.  
   dbb_int1_scale_output: Output scale factor for Daughterboard B TX1 CIC (``DBB_cic_int1``) device worker. The output is divided by :math:`2^{\texttt{dbb_int1_scale_output}}`.
   dbb_int1_flush_length: The number of zero samples inserted into the Daughterboard B TX1 CIC (``DBB_cic_int1``) device worker on receipt of a flush opcode.
   
   dbb_dec0_down_sample_factor: Decimation Factor for Daughterboard B RX0 CIC (``DBB_cic_dec0``) device worker.  
   dbb_dec0_scale_output: Output scale factor for Daughterboard B RX0 CIC (``DBB_cic_dec0``) device worker. The output is divided by :math:`2^{\texttt{dbb_dec0_scale_output}}`.
   dbb_dec0_flush_length: The number of zero samples inserted into the Daughterboard B RX0 CIC (``DBB_cic_dec0``) device worker on receipt of a flush opcode.
   dbb_dec1_down_sample_factor: Decimation Factor for Daughterboard B RX1 CIC (``DBB_cic_dec1``) device worker.  
   dbb_dec1_scale_output: Output scale factor for Daughterboard B RX1 CIC (``DBB_cic_dec1``) device worker. The output is divided by :math:`2^{\texttt{dbb_dec1_scale_output}}`.
   dbb_dec1_flush_length: The number of zero samples inserted into the Daughterboard B RX1 CIC (``DBB_cic_dec1``) device worker on receipt of a flush opcode.
   
   dba_dac_channels_are_swapped: Channel mapping for Daughterboard A qDAC device sub-worker (``DBA_qdac_sub``): ``true`` = qDAC1 to DAC0/1, qDAC0 to DAC2/3, ``false`` = qDAC0 to DAC0/1 and qDAC1 to DAC2/3.
   dba_dac_lanes_are_swapped: JESD Lane mapping for the Daughterboard A qDAC device sub-worker (``DBA_qdac_sub``): ``true`` = DAC0 is swapped with DAC1, DAC2 is swapped with DAC3, ``false`` = DAC Lanes are not swapped.
   dba_dac_mask_ch0i: Bit masking for channel0 of Daughterboard A qDAC device sub-worker (``DBA_qdac_sub``).
   dba_dac_mask_ch0q: Bit masking for channel0 of Daughterboard A qDAC device sub-worker (``DBA_qdac_sub``).
   dba_dac_mask_ch1i: Bit masking for channel1 of Daughterboard A qDAC device sub-worker (``DBA_qdac_sub``).
   dba_dac_mask_ch1q: Bit masking for channel1 of Daughterboard A qDAC device sub-worker (``DBA_qdac_sub``).

   dba_adc_channels_are_swapped: Channel mapping for Daughterboard A qADC device sub-worker (``DBA_qadc_sub``): ``true`` = ADC0/1 to qADC1, ADC2/3 to qADC0, ``false`` = ADC0/1 to qADC0 and ADC2/3 to qADC1.
   dba_adc_lanes_are_swapped: JESD lane mapping for the Daughterboard A qADC device sub-worker (``DBA_qadc_sub``): ``true`` = ADC0 is swapped with ADC1, ADC2 is swapped with ADC3, ``false`` = ADC Lanes are not swapped.
   dba_adc_mask_ln0: Bit masking for ADC0 of Daughterboard A qADC device sub-worker (``DBA_qadc_sub``).
   dba_adc_mask_ln1: Bit masking for ADC1 of Daughterboard A qADC device sub-worker (``DBA_qadc_sub``).
   dba_adc_mask_ln2: Bit masking for ADC2 of Daughterboard A qADC device sub-worker (``DBA_qadc_sub``).
   dba_adc_mask_ln3: Bit masking for ADC3 of Daughterboard A qADC device sub-worker (``DBA_qadc_sub``).

   dbb_dac_channels_are_swapped: Channel mapping for Daughterboard B qDAC device sub-worker (``DBB_qdac_sub``): ``true`` = qDAC1 to DAC0/1, qDAC0 to DAC2/3, ``false`` = qDAC0 to DAC0/1 and qDAC1 to DAC2/3.
   dba_dac_lanes_are_swapped: JESD Lane mapping for the Daughterboard B qDAC device sub-worker (``DBB_qdac_sub``): ``true`` = DAC0 is swapped with DAC1, DAC2 is swapped with DAC, ``false`` = DAC Lanes are not swapped.
   dbb_dac_mask_ch0i: Bit masking for channel0 of Daughterboard B qDAC device sub-worker (``DBB_qdac_sub``).
   dbb_dac_mask_ch0q: Bit masking for channel0 of Daughterboard B qDAC device sub-worker (``DBB_qdac_sub``).
   dbb_dac_mask_ch1i: Bit masking for channel1 of Daughterboard B qDAC device sub-worker (``DBB_qdac_sub``).
   dbb_dac_mask_ch1q: Bit masking for channel1 of Daughterboard B qDAC device sub-worker (``DBB_qdac_sub``).

   dbb_adc_channels_are_swapped: Channel mapping for Daughterboard B qADC device sub-worker (``DBB_qadc_sub``): ``true`` = ADC0/1 to qADC1, ADC2/3 to qADC0, ``false`` = ADC0/1 to qADC0 and ADC2/3 to qADC1.
   dbb_adc_lanes_are_swapped: JESD lane mapping for the Daughterboard B qADC device sub-worker (``DBB_qadc_sub``): ``true`` = ADC0 is swapped with ADC1, ADC2 is swapped with ADC3, ``false`` = ADC Lanes are not swapped.
   dbb_adc_mask_ln0: Bit masking for ADC0 of Daughterboard B qADC device sub-worker (``DBB_qadc_sub``).
   dbb_adc_mask_ln1: Bit masking for ADC1 of Daughterboard B qADC device sub-worker (``DBB_qadc_sub``).
   dbb_adc_mask_ln2: Bit masking for ADC2 of Daughterboard B qADC device sub-worker (``DBB_qadc_sub``).
   dbb_adc_mask_ln3: Bit masking for ADC3 of Daughterboard B qADC device sub-worker (``DBB_qadc_sub``).
   ocpi_buffer_size_rx: Maximum Message Size for RX Ports 
   
   tx: Transmitter Ports (up to 4 channels - 2 channels per DB)
   rx: Receiver Ports (up to 4 channels - 2 channels per DB)


   drcConfigStruct: **(Property will be deprecated in future releases)** Channel Configuration Structure used by API Controllers.
   spiControlStruct: **(Property will be deprecated in future releases)** Data Structure for SPI communication to hardware chips.
   spiReadback: **(Property will be deprecated in future releases)** Readback Register for SPI communication from hardware chips.
   dbSelectStruct: **(Property will be deprecated in future releases)** Daughterboard Select Structure for SPI communication. 
   cpldStruct: **(Property will be deprecated in future releases)** Configuration Structure used by CPLD API Controller.
   cpldGpioStruct: **(Property will be deprecated in future releases)** Configuration Structure used by CPLD GPIO API Controller.
   dsaStruct: **(Property will be deprecated in future releases)** Configuration Structure used by the N310 Digital Step Attenuators (DSA) API Controller.
   lmkStruct: **(Property will be deprecated in future releases)** Configuration Structure used by the Clock Source (LMK) API Controller.
   transceiverStruct: **(Property will be deprecated in future releases)** Configuration Structure used by the AD9371 Transceiver API Controller.
   adfStruct: **(Property will be deprecated in future releases)** Configuration Structure used by the RF LO Synthesizers  API Controller.
   channelConfig: **(Property will be deprecated in future releases)** ISR switch to handle drcConfigStruct datatype and algorithm/controller flow of a full channel configuration
   debugConfig: **(Property will be deprecated in future releases)** Debug the inner workings of the m_properties.configurations
   cpldControllerOrdinal: **(Property will be deprecated in future releases)** CPLD API Controller Calls
   cpldGpioControllerOrdinal: **(Property will be deprecated in future releases)** CPLD GPIO API Controller Calls
   dsaControllerOrdinal: **(Property will be deprecated in future releases)** Digital Step Attenuators (DSA) API Controller Calls
   lmk04828ControllerOrdinal: **(Property will be deprecated in future releases)** Clock Source (LMK) API Controller Calls
   adfControllerOrdinal: **(Property will be deprecated in future releases)** RF LO Synthesizer API Controller Calls
   transceiverControllerOrdinal: **(Property will be deprecated in future releases)** AD9371 Transceiver API Controller Calls
   intControllerOrdinal: **(Property will be deprecated in future releases)** CIC Intepolation API Controller Calls
   decControllerOrdinal: **(Property will be deprecated in future releases)** CIC Decimation API Controller Calls


