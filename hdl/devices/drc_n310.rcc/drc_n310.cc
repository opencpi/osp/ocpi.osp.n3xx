/*
 * THIS FILE WAS ORIGINALLY GENERATED ON Thu Jul 15 12:44:48 2021 EDT
 * BASED ON THE FILE: drc_n310.xml
 */


#include "drc_n310-worker.hh"
// #include <cpldController.hpp>
// #include <lmk04828Controller.hpp>
// #include <dsa.hpp>
// #include <adf435x.hpp>
// #include <ad9371Gpio.hpp>

// #include <transceiver.h>
#include <mgDaughterboard.hpp>
// #include <array>
#include <vector>
// #include <algorithm>
#include <n310parameters.h>
// #include "cpldGpio.hpp"
#include <i2c.hpp>  // i2c integration
#include <i2c_n310.hpp> // i2c integration
#include <unistd.h> // sleep()
#include <iomanip>
// #include <cmath>
#include <iterator>
#include <uchar.h>
#include <csignal>


#define IS_ENABLED_I2C 1 // change this to 1 when DRC objects is updated with latest LMK additions

// added for creating a stringstream to send to the log
#ifdef max
  #undef max
#endif

#ifdef min
  #undef min
#endif

#ifdef abs
  #undef abs
#endif

#ifdef swap
  #undef swap
#endif

#include <sstream> // std::stringstream
#include <iomanip> // std::setfill

#define DRC_LOG_THREE 3 
#define DRC_LOG_EIGHT 8 

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Drc_n310WorkerTypes;

//OCPIDrcProxyAPI.hh used after namespace as specified
#include <OcpiDrcProxyApi.hh> 
#define OCPI_DRC_CAT_(a,b) a##b
#define OCPI_DRC_CAT(a,b) OCPI_DRC_CAT_(a,b)
#define OCPI_DRC_MAX_CONFIGURATIONS OCPI_DRC_CAT(OCPI_WORKER_NAME,_MAX_CONFIGURATIONS_P)
namespace OD = OCPI::DRC;


namespace {
  void terminateGracefully(std::int32_t signum);
}

class Drc_n310Worker : public OD::DrcProxyBase, public drcMiddleware<Drc_n310Worker> {
  #if IS_ENABLED_I2C
  I2C_N310 mI2C_N310;
  #endif

  friend class drcMiddleware<Drc_n310Worker>;
  // drcMiddleware VTable method definitions
    void log_helper(std::int32_t _log_level, std::string _log_data){
      if(_log_level < 0){
        log(3,"Invalid log level detected");
      } else {
        // normal log
        log(_log_level,_log_data.c_str());
      }
    }
    std::uint32_t cpld_read_spi(std::uint32_t frame) {
      slaves.n310_cpld_spi.set_CPLD_DB_SEL(this->_routingTuple.get_dbIndex()); // possibly another bit 
      slaves.n310_cpld_spi.set_CPLD_ADDR_CS(this->_chipRouter.get_chipSelect(this->_routingTuple)); // possibly another bit for adf only
      slaves.n310_cpld_spi.set_CPLD_FRAME(frame);
      slaves.n310_cpld_spi.set_CPLD_RENABLE(true); // set on
      while(!slaves.n310_cpld_spi.get_CPLD_DONE()){
          ; // blocking read
      }
      slaves.n310_cpld_spi.set_CPLD_RENABLE(false); // set off - must be controlled via rcc
      return slaves.n310_cpld_spi.get_CPLD_RDATA(); // where do I place this
    }
    // cpld_write_spi
    void cpld_write_spi(std::uint_least32_t frame) {
      slaves.n310_cpld_spi.set_CPLD_DB_SEL(this->_routingTuple.get_dbIndex()); // Daughterboard routing
      slaves.n310_cpld_spi.set_CPLD_ADDR_CS(this->_chipRouter.get_chipSelect(this->_routingTuple)); // Chip Select Routing
      slaves.n310_cpld_spi.set_CPLD_FRAME(frame);
      slaves.n310_cpld_spi.set_CPLD_WENABLE(true); // set on
      while(!slaves.n310_cpld_spi.get_CPLD_DONE()){
          ; // blocking read
      }
      slaves.n310_cpld_spi.set_CPLD_WENABLE(false); // set off - must be controlled via rcc
      return;
    }
    // writeCpldGpio
    void writeCpldGpio(bool set) {
      struct n310GpioInterface { // somehow treated like an inherited class of this type
        // methods needed
        // grab from slave definitions in ./gen/drc_n310-slaves.hh
        std::function<void(std::uint8_t)> _set_Chip_Select;
        std::function<void(bool)> _set_Set;

        n310GpioInterface()
          :
            _set_Chip_Select(nullptr),
            _set_Set(nullptr)
          {
            ;
          };
        n310GpioInterface(std::function<void(std::uint8_t)> __set_Chip_Select, std::function<void(bool)> __set_Set)
          :
            _set_Chip_Select(__set_Chip_Select),
            _set_Set(__set_Set)
          {
            ;
          };
      };

      auto _n310GpioInterface = n310GpioInterface(); // empty

      switch(this->_routingTuple.get_dbIndex())
      {
        #if (OCPI_PARAM_drc_n310_dba_en() == 1)
          case 0: // enumeration for this = DBA_ORDINAL
            {
              auto &gpio_a = slaves.DBA_gpio;
              _n310GpioInterface =
              n310GpioInterface(
                [&gpio_a](std::uint8_t _Chip_Select)->void{gpio_a.set_Chip_Select(_Chip_Select);},
                [&gpio_a](bool _Set)->void{gpio_a.set_Set(_Set);}
              );
              break;
            }
        #endif
        #if (OCPI_PARAM_drc_n310_dbb_en() == 1) // needs to be true
          case 1: // enumeration DBB_ORDINAL
            {
              auto &gpio_b = slaves.DBB_gpio;
              _n310GpioInterface =
              n310GpioInterface(
                [&gpio_b](std::uint8_t _Chip_Select)->void{gpio_b.set_Chip_Select(_Chip_Select);},
                [&gpio_b](bool _Set)->void{gpio_b.set_Set(_Set);}
              );
              break;
            }
        #endif
        default:
          log(DRC_LOG_EIGHT,"Unexpected daughterboard case selection");
          throw; // throw an exception --
          break;
      }
      _n310GpioInterface._set_Chip_Select(this->_chipRouter.get_chipSelect(this->_routingTuple));
      _n310GpioInterface._set_Set(set);
    }
    // readAd9371Gpio
    std::uint8_t readAd9371Gpio(void) {
      #if 0
        GPIO pins read from LSb to MSb
          0 - GPIO_0
          1 - GPIO_1
          2 - GPIO_3
          3 - GPIO_4
          4 - GPIO_12
          5 - GPIO_13
          6 - GPIO_14
          7 - GPIO_15
      #endif
      slaves.n310_ad9371_gpio.set_DB_Select(this->_routingTuple.get_dbIndex());
      return slaves.n310_ad9371_gpio.get_regValue();
    }
    // writeDsaIo
    void writeDsaIo(unsigned char value) {
      slaves.n310_dsa.set_DB_Select(this->_routingTuple.get_dbIndex());
      slaves.n310_dsa.set_Chip_Select(this->_chipRouter.get_chipSelect(this->_routingTuple));
      slaves.n310_dsa.set_Attenuation(value);
    }

    // NEXT TWO MIGHT BE REDUNDANT

    // sysref_wrapper
    void sysref_wrapper(std::uint16_t the_sysref) {
      this->dbArray.at(this->_routingTuple.get_dbIndex()).lmkController.requestSysref(the_sysref); 
      return;
    }
    // mykReset_wrapper
    void mykReset_wrapper(void) {
      this->dbArray.at(this->_routingTuple.get_dbIndex()).cpldController.mykonosReset(); 
      return;
    }

    // ad9371_write_spi
    void ad9371_write_spi(std::uint8_t * bufIn) {
      std::uint_least32_t spiFrame = (bufIn[0] << 16) | (bufIn[1] << 8) | (bufIn[2]);

      slaves.n310_ad9371_spi.set_MK_DB_SEL(this->_routingTuple.get_dbIndex());
      slaves.n310_ad9371_spi.set_MK_FRAME(spiFrame);
      slaves.n310_ad9371_spi.set_MK_WENABLE(true); // write enable
      while(!slaves.n310_ad9371_spi.get_MK_DONE()) {
        ;
      }
      slaves.n310_ad9371_spi.set_MK_WENABLE(false);
      return;
    }
    // ad9371_read_spi
    std::uint8_t ad9371_read_spi(std::uint8_t * bufIn) {
      std::uint_least32_t spiFrame = (bufIn[0] << 16) | (bufIn[1] << 8) | (bufIn[2]);
      slaves.n310_ad9371_spi.set_MK_DB_SEL(this->_routingTuple.get_dbIndex());
      // Did not add chip select
      slaves.n310_ad9371_spi.set_MK_FRAME(spiFrame);
      slaves.n310_ad9371_spi.set_MK_RENABLE(true); // read enable
      while(!slaves.n310_ad9371_spi.get_MK_DONE()) {
        ;
      }
      slaves.n310_ad9371_spi.set_MK_RENABLE(false); // read enable
      return slaves.n310_ad9371_spi.get_MK_RDATA();
    }
    // jesdInitMemSpace_t
    std::array<std::uint32_t,8>& jesd_mem_manager(void) {
      static std::array<std::uint32_t,8> DBA_axi{DBA_TX_XCVR_BASEADDR,DBA_TX_JESD_BASEADDR,DBA_TX_CORE_BASEADDR,DBA_TX_CLKGEN_BASEADDR,DBA_RX_XCVR_BASEADDR,DBA_RX_JESD_BASEADDR,DBA_RX_CORE_BASEADDR,DBA_RX_CLKGEN_BASEADDR};
      static std::array<std::uint32_t,8> DBB_axi{DBB_TX_XCVR_BASEADDR,DBB_TX_JESD_BASEADDR,DBB_TX_CORE_BASEADDR,DBB_TX_CLKGEN_BASEADDR,DBB_RX_XCVR_BASEADDR,DBB_RX_JESD_BASEADDR,DBB_RX_CORE_BASEADDR,DBB_RX_CLKGEN_BASEADDR};
      return (this->_routingTuple.get_dbIndex()) ? DBB_axi : DBA_axi;
    }
    // jesdMemTranslator_t
    std::uint32_t jesd_mem_mapper(std::uint32_t addressIn, std::uint32_t value, bool _wr) {
      std::uint32_t pageSize = getpagesize();
      std::uint32_t pageOffset = (addressIn) & (pageSize - 1);
      std::uint32_t pageAddr = addressIn - pageOffset;

      // uint32_t addr = pageAddr - addressIn;
      // std::cout << "-------------------------" << std::endl;
      // std::cout << "in ADDR: "        << std::hex << addressIn << std::endl;
      // std::cout << "page offset: "    << std::hex << pageOffset << std::endl;
      // std::cout << "Page addr: "      << std::hex << pageAddr << std::endl;

      std::uint32_t _4ByteIndex = pageOffset / 4;

      // determine which daughterboard it is in?
      // bool db = (pageAddr >= DBB_TX_XCVR_BASEADDR) ? true: false; // DBB, DBA

      struct jesdInterface { // somehow treated like an inherited class of this type
        using _getter = std::function<uint32_t(unsigned)>;
        using _setter = std::function<void(unsigned,uint32_t)>;
        // methods needed
        // grab from slave definitions 6,7 in ./gen/drc_n310-slaves.hh
        _setter _set_phy_tx_raw;
        _getter _get_phy_tx_raw;

        _setter _set_link_tx_raw;
        _getter _get_link_tx_raw;
        _setter _set_tpl_tx_raw;
        _getter _get_tpl_tx_raw;

        _setter _set_phy_rx_raw;
        _getter _get_phy_rx_raw;

        _setter _set_link_rx_raw;
        _getter _get_link_rx_raw;
        _setter _set_tpl_rx_raw;
        _getter _get_tpl_rx_raw;
        _setter _set_clk_rx_raw;
        _getter _get_clk_rx_raw;
        _setter _set_clk_tx_raw;
        _getter _get_clk_tx_raw;

        jesdInterface()
          :
            _set_phy_tx_raw(nullptr),
            _get_phy_tx_raw(nullptr),
            _set_link_tx_raw(nullptr),
            _get_link_tx_raw(nullptr),
            _set_tpl_tx_raw(nullptr),
            _get_tpl_tx_raw(nullptr),
            _set_phy_rx_raw(nullptr),
            _get_phy_rx_raw(nullptr),
            _set_link_rx_raw(nullptr),
            _get_link_rx_raw(nullptr),
            _set_tpl_rx_raw(nullptr),
            _get_tpl_rx_raw(nullptr),
            _set_clk_rx_raw(nullptr),
            _get_clk_rx_raw(nullptr),
            _set_clk_tx_raw(nullptr),
            _get_clk_tx_raw(nullptr)
          {
            ;
          };

        jesdInterface(_setter __set_phy_tx_raw, _getter __get_phy_tx_raw, _setter __set_link_tx_raw, _getter __get_link_tx_raw, _setter __set_tpl_tx_raw, 
                      _getter __get_tpl_tx_raw, _setter __set_phy_rx_raw, _getter __get_phy_rx_raw, _setter __set_link_rx_raw,
                      _getter __get_link_rx_raw, _setter __set_tpl_rx_raw, _getter __get_tpl_rx_raw, _setter __set_clk_rx_raw, _getter __get_clk_rx_raw,
                      _setter __set_clk_tx_raw, _getter __get_clk_tx_raw)
          :
            _set_phy_tx_raw(__set_phy_tx_raw),
            _get_phy_tx_raw(__get_phy_tx_raw),
            _set_link_tx_raw(__set_link_tx_raw),
            _get_link_tx_raw(__get_link_tx_raw),
            _set_tpl_tx_raw(__set_tpl_tx_raw),
            _get_tpl_tx_raw(__get_tpl_tx_raw),
            _set_phy_rx_raw(__set_phy_rx_raw),
            _get_phy_rx_raw(__get_phy_rx_raw),
            _set_link_rx_raw(__set_link_rx_raw),
            _get_link_rx_raw(__get_link_rx_raw),
            _set_tpl_rx_raw(__set_tpl_rx_raw),
            _get_tpl_rx_raw(__get_tpl_rx_raw),
            _set_clk_rx_raw(__set_clk_rx_raw),
            _get_clk_rx_raw(__get_clk_rx_raw),
            _set_clk_tx_raw(__set_clk_tx_raw),
            _get_clk_tx_raw(__get_clk_tx_raw)
          {
            ;
          };
      };

      auto _jesd_axi_mapper_worker = jesdInterface(); // empty

      switch(this->_routingTuple.get_dbIndex())
      {
        #if (OCPI_PARAM_drc_n310_dba_en() == 1)
          case 0: // enumeration for this = DBA_ORDINAL
            {
              auto &raw2axi_a = slaves.DBA_jesd_axi_mapper;
              _jesd_axi_mapper_worker = 
              jesdInterface(
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_phy_tx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_phy_tx_raw(idx);},
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_link_tx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_link_tx_raw(idx);},
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_tpl_tx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_tpl_tx_raw(idx);},
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_phy_rx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_phy_rx_raw(idx);},
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_link_rx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_link_rx_raw(idx);},
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_tpl_rx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_tpl_rx_raw(idx);},
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_clk_rx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_clk_rx_raw(idx);},
                [&raw2axi_a](unsigned idx, uint32_t value)->void{return raw2axi_a.set_clk_tx_raw(idx,value);},
                [&raw2axi_a](unsigned idx)->uint32_t{return raw2axi_a.get_clk_tx_raw(idx);}
              );
              break;
            }
        #endif
        #if (OCPI_PARAM_drc_n310_dbb_en() == 1) // needs to be true
          case 1: // enumeration DBB_ORDINAL
            {
              auto &raw2axi_b = slaves.DBB_jesd_axi_mapper;
              _jesd_axi_mapper_worker =
              jesdInterface(
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_phy_tx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_phy_tx_raw(idx);},
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_link_tx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_link_tx_raw(idx);},
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_tpl_tx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_tpl_tx_raw(idx);},
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_phy_rx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_phy_rx_raw(idx);},
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_link_rx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_link_rx_raw(idx);},
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_tpl_rx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_tpl_rx_raw(idx);},
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_clk_rx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_clk_rx_raw(idx);},
                [&raw2axi_b](unsigned idx, uint32_t value)->void{return raw2axi_b.set_clk_tx_raw(idx,value);},
                [&raw2axi_b](unsigned idx)->uint32_t{return raw2axi_b.get_clk_tx_raw(idx);}
              );
              break;
            }
        #endif
        default:
          log(DRC_LOG_EIGHT,"Unexpected daughterboard case selection");
          throw; // throw an exception --
          break;
      }

      // translate to "agnostic" addresses --> mask out bits that determine daughterboard
      const std::uint32_t dbBit = (this->_routingTuple.get_dbIndex()) ? 1 << 19 : 0; // right now only 2 daughterboards, @TODO if ever more than 2 
      // 0x83c70000
      // 0x00080000
      std::uint32_t dbAgnosticPageAddr = pageAddr & (~dbBit); // masked out --> Make dbBit dynamic (1, or 0, or 0x3, 0x7, 0xF, etc..)
      // if dbBit(s) are dynamic, then if a specific DB address is passed when it SHOULDNT be active, then it'll NOT be masked out which works for default
      switch(pageAddr) // without using dbAgnosticPageAddr, doubles (tripples, quadruples, etc) number of cases?
      {
        case DBA_TX_XCVR_BASEADDR: // what if DBA address passed when only DBB is active? Still works here, maybe should not, #if statements will exponentially make larger
        case DBB_TX_XCVR_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_phy_tx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_phy_tx_raw(_4ByteIndex);
        case DBA_TX_JESD_BASEADDR:
        case DBB_TX_JESD_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_link_tx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_link_tx_raw(_4ByteIndex);
        case DBA_TX_CORE_BASEADDR:
        case DBB_TX_CORE_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_tpl_tx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_tpl_tx_raw(_4ByteIndex);
        case DBA_RX_XCVR_BASEADDR:
        case DBB_RX_XCVR_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_phy_rx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_phy_rx_raw(_4ByteIndex);
        case DBA_RX_JESD_BASEADDR:
        case DBB_RX_JESD_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_link_rx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_link_rx_raw(_4ByteIndex);
        case DBA_RX_CORE_BASEADDR:
        case DBB_RX_CORE_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_tpl_rx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_tpl_rx_raw(_4ByteIndex);
        case DBA_RX_CLKGEN_BASEADDR:
        case DBB_RX_CLKGEN_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_clk_rx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_clk_rx_raw(_4ByteIndex);
        case DBA_TX_CLKGEN_BASEADDR:
        case DBB_TX_CLKGEN_BASEADDR:
          if(!_wr)
          {
            _jesd_axi_mapper_worker._set_clk_tx_raw(_4ByteIndex, value);
            return 0;
          }
          else
            return _jesd_axi_mapper_worker._get_clk_tx_raw(_4ByteIndex);
        default:
          log(DRC_LOG_EIGHT,"UNEXPECTED ADDRESS FOR JESD HARDWARE");
          throw;
          return 0;
      }
        return 0;
    }
  // end of drcMiddleware VTable method definitions

    private:
      std::uint64_t magic_number;

      std::uint64_t computeChecksum(ConfigurationsChannels *channelConfigAddress){
    // length of memory: 0x357 --> compute checksum
        std::uint64_t checksum = 0;
        const std::uint32_t memoryLength = 0x357;
        for(std::uint32_t x = 0; x < memoryLength; x++){
          if((x < 0x104) or (x > 0x120 and x < 0x228) or (x > 0x249 and x < 0x354)
            or (x > 0x104 and x < 0x108) or (x > 0x355)
            ){
        continue; // char array does not default to 0
    }
      checksum += static_cast<std::uint64_t>(*(reinterpret_cast<std::uint8_t*>(channelConfigAddress) + x)); // every byte in the section of memory
      // for now, just add up
    }
    
    [=]() // variables placed inside lambda so they have their own scope
    {
      std::ostringstream logstringstream;
      logstringstream << "Checksum calculated: " << checksum << "\n";
      std::string log_string = logstringstream.str();
      log(DRC_LOG_EIGHT, log_string.c_str());
    };
    
    return checksum;
  }

  //I2C_N310 mI2C_N310; // I2C

  bool defaultChannel(ConfigurationsChannels *channelConfigAddress){
    // return false if different
    if(computeChecksum(channelConfigAddress) == magic_number){
      return true;
    } else {
      return false;
    }
  }

  //using clock_chip_write_t = std::function<void(std::int32_t,std::int32_t)>;
  void mcrConfigure_wrapper(std::int32_t mcrclock, std::int32_t refclock) {
    this->dbArray.at(this->_routingTuple.get_dbIndex()).lmkController.mcrConfigure(mcrclock, refclock);
    return;
  }

  // end of ocpi access callback functions

  #if 0
        slaves.n310_cpld_spi.set_CPLD_DB_SEL(this->_routingTuple.get_dbIndex()); // possibly another bit 
        slaves.n310_cpld_spi.set_CPLD_ADDR_CS(this->_chipRouter.get_chipSelect(this->_routingTuple)); // possibly another bit for adf only
        slaves.n310_cpld_spi.set_CPLD_FRAME(frame);
        slaves.n310_cpld_spi.set_CPLD_RENABLE(true); // set on
#endif



  RCCResult intControllerOrdinal_written()
  {
      // 2 Interpolators per daughterboard
      #if (OCPI_PARAM_drc_n310_dba_en())
        // Interp 0 - DBA
        #if (OCPI_PARAM_drc_n310_dba_tx0_en() && OCPI_PARAM_drc_n310_dba_cic_int_en())
          ushort a_samplefactor = m_properties.dba_int0_up_sample_factor;
          unsigned char  b_scaleoutput  = m_properties.dba_int0_scale_output;
          ushort c_flushlength = m_properties.dba_int0_flush_length;
        #endif

        // Interp 1 - DBA
        #if (OCPI_PARAM_drc_n310_dba_tx1_en() && OCPI_PARAM_drc_n310_dba_cic_int_en())
          ushort d_samplefactor = m_properties.dba_int1_up_sample_factor;
          unsigned char  e_scaleoutput  = m_properties.dba_int1_scale_output;
          ushort f_flushlength = m_properties.dba_int1_flush_length;
        #endif
      #endif

      #if (OCPI_PARAM_drc_n310_dbb_en())
        // Interp 0 - DBB
        #if (OCPI_PARAM_drc_n310_dbb_tx0_en() && OCPI_PARAM_drc_n310_dbb_cic_int_en())
          ushort g_samplefactor = m_properties.dbb_int0_up_sample_factor;
          unsigned char  h_scaleoutput  = m_properties.dbb_int0_scale_output;
          ushort i_flushlength = m_properties.dbb_int0_flush_length;
        #endif

        // Interp 1 - DBB
        #if (OCPI_PARAM_drc_n310_dbb_tx1_en() && OCPI_PARAM_drc_n310_dbb_cic_int_en())
          ushort j_samplefactor = m_properties.dbb_int1_up_sample_factor;
          unsigned char  k_scaleoutput  = m_properties.dbb_int1_scale_output;
          ushort l_flushlength = m_properties.dbb_int1_flush_length;
        #endif
      #endif

    int option = m_properties.intControllerOrdinal;

    #if (OCPI_PARAM_drc_n310_dba_en())
      #if (OCPI_PARAM_drc_n310_dba_tx0_en() && OCPI_PARAM_drc_n310_dba_cic_int_en())
        if(0 == option)      { slaves.DBA_cic_int0.set_up_sample_factor(a_samplefactor);
        }
        else if(1 == option) { slaves.DBA_cic_int0.set_scale_output(b_scaleoutput);
        }
        else if(2 == option) { slaves.DBA_cic_int0.set_flush_length(c_flushlength);
        }
        else {;}
      #endif

      #if (OCPI_PARAM_drc_n310_dba_tx1_en() && OCPI_PARAM_drc_n310_dba_cic_int_en())
        if(0 == option)      { slaves.DBA_cic_int1.set_up_sample_factor(d_samplefactor);
        }
        else if(1 == option) { slaves.DBA_cic_int1.set_scale_output(e_scaleoutput);
        }
        else if(2 == option) { slaves.DBA_cic_int1.set_flush_length(f_flushlength);
        }
        else {;}
      #endif
    #endif


    #if (OCPI_PARAM_drc_n310_dbb_en())
      #if (OCPI_PARAM_drc_n310_dbb_tx0_en() && OCPI_PARAM_drc_n310_dbb_cic_int_en())
        if(0 == option)      { slaves.DBB_cic_int0.set_up_sample_factor(g_samplefactor);
        }
        else if(1 == option) { slaves.DBB_cic_int0.set_scale_output(h_scaleoutput);
        }
        else if(2 == option) { slaves.DBB_cic_int0.set_flush_length(i_flushlength);
        }
        else {;}
      #endif

      #if (OCPI_PARAM_drc_n310_dbb_tx1_en() && OCPI_PARAM_drc_n310_dbb_cic_int_en())
        if(0 == option)      { slaves.DBB_cic_int1.set_up_sample_factor(j_samplefactor);
        }
        else if(1 == option) { slaves.DBB_cic_int1.set_scale_output(k_scaleoutput);
        }
        else if(2 == option) { slaves.DBB_cic_int1.set_flush_length(l_flushlength);
        }
        else {;}
      #endif
    #endif

    return RCC_OK;
  }


  RCCResult decControllerOrdinal_written()
  {
      // 2 Decim. per daughterboard
      #if (OCPI_PARAM_drc_n310_dba_en())
      // Decim 0 - DBA
        #if (OCPI_PARAM_drc_n310_dba_rx0_en() && OCPI_PARAM_drc_n310_dba_cic_dec_en())
          ushort a_samplefactor = m_properties.dba_dec0_down_sample_factor;
          unsigned char  b_scaleoutput  = m_properties.dba_dec0_scale_output;
          ushort c_flushlength = m_properties.dba_dec0_flush_length;
        #endif

        // Decim 1 - DBA
        #if (OCPI_PARAM_drc_n310_dba_rx1_en() && OCPI_PARAM_drc_n310_dba_cic_dec_en())
          ushort d_samplefactor = m_properties.dba_dec1_down_sample_factor;
          unsigned char  e_scaleoutput  = m_properties.dba_dec1_scale_output;
          ushort f_flushlength = m_properties.dba_dec1_flush_length;
        #endif
      #endif
    
      #if (OCPI_PARAM_drc_n310_dbb_en())
        //Decim 0 - DBB
        #if (OCPI_PARAM_drc_n310_dbb_rx0_en() && OCPI_PARAM_drc_n310_dbb_cic_dec_en())
          ushort g_samplefactor = m_properties.dbb_dec0_down_sample_factor;
          unsigned char  h_scaleoutput  = m_properties.dbb_dec0_scale_output;
          ushort i_flushlength = m_properties.dbb_dec0_flush_length;
        #endif

        // Decim 1 - DBB
        #if (OCPI_PARAM_drc_n310_dbb_rx1_en() && OCPI_PARAM_drc_n310_dbb_cic_dec_en())
          ushort j_samplefactor = m_properties.dbb_dec1_down_sample_factor;
          unsigned char  k_scaleoutput  = m_properties.dbb_dec1_scale_output;
          ushort l_flushlength = m_properties.dbb_dec1_flush_length;
        #endif
      #endif

    int option = m_properties.decControllerOrdinal;

      #if (OCPI_PARAM_drc_n310_dba_en())
        #if (OCPI_PARAM_drc_n310_dba_rx0_en() && OCPI_PARAM_drc_n310_dba_cic_dec_en())
          if(0 == option)      { slaves.DBA_cic_dec0.set_down_sample_factor(a_samplefactor);
          }
          else if(1 == option) { slaves.DBA_cic_dec0.set_scale_output(b_scaleoutput);
          }
          else if(2 == option) { slaves.DBA_cic_dec0.set_flush_length(c_flushlength);
          }
          else {;}
        #endif

        #if (OCPI_PARAM_drc_n310_dba_rx1_en() && OCPI_PARAM_drc_n310_dba_cic_dec_en())
          if(0 == option)      { slaves.DBA_cic_dec1.set_down_sample_factor(d_samplefactor);
          }
          else if(1 == option) { slaves.DBA_cic_dec1.set_scale_output(e_scaleoutput);
          }
          else if(2 == option) { slaves.DBA_cic_dec1.set_flush_length(f_flushlength);
          }
          else {;}
        #endif
      #endif

      #if (OCPI_PARAM_drc_n310_dbb_en())
        #if (OCPI_PARAM_drc_n310_dbb_rx0_en() && OCPI_PARAM_drc_n310_dbb_cic_dec_en())
          if(0 == option)      {slaves.DBB_cic_dec0.set_down_sample_factor(g_samplefactor);
          }
          else if(1 == option) {slaves.DBB_cic_dec0.set_scale_output(h_scaleoutput);
          }
          else if(2 == option) {slaves.DBB_cic_dec0.set_flush_length(i_flushlength);
          }
          else {;} 
        #endif

        #if (OCPI_PARAM_drc_n310_dbb_rx1_en() && OCPI_PARAM_drc_n310_dbb_cic_dec_en())
          if(0 == option)      {slaves.DBB_cic_dec1.set_down_sample_factor(j_samplefactor);
          }
          else if(1 == option) {slaves.DBB_cic_dec1.set_scale_output(k_scaleoutput);
          }
          else if(2 == option) {slaves.DBB_cic_dec1.set_flush_length(l_flushlength);
          }
          else {;}
        #endif
      #endif

    return RCC_OK;
  }


  RCCResult cpldControllerOrdinal_written() {
    int dbselect = (m_properties.dbSelectStruct.cpld == DBSELECTSTRUCT_CPLD_DBA) ? 0 : 1;

    if(this->dbArray.at(dbselect).POR & 0x1){
      std::cout << "proxyWorker: calling the CPLD POR" << std::endl;
      this->dbArray.at(dbselect).cpldController.reset(); // call reset routine
      this->dbArray.at(dbselect).POR &= ~(0x1);
    }
    switch(m_properties.cpldControllerOrdinal) {
      case 0:
      break;
      case 1:
      break;
      case 2: // set_tx_switches
        [=]() -> void // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: Calling the set_tx_switches CPLD routine" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        this->dbArray.at(dbselect).cpldController.set_tx_switches(
          static_cast<magnesium_cpld_ctrl::chan_sel_t>(m_properties.cpldStruct[dbselect].chan_sel),
          static_cast<magnesium_cpld_ctrl::tx_sw1_t>(m_properties.cpldStruct[dbselect].tx_sw1),
          static_cast<magnesium_cpld_ctrl::tx_sw2_t>(m_properties.cpldStruct[dbselect].tx_sw2),
          static_cast<magnesium_cpld_ctrl::tx_sw3_t>(m_properties.cpldStruct[dbselect].tx_sw3),
          static_cast<magnesium_cpld_ctrl::lowband_mixer_path_sel_t>(m_properties.cpldStruct[dbselect].lowband_mixer_path_sel),
          m_properties.cpldStruct[dbselect].enb_lowband_mixer,
          static_cast<magnesium_cpld_ctrl::atr_state_t>(m_properties.cpldStruct[dbselect].atr_state),
          m_properties.cpldStruct[dbselect].deferCommit );
        break;
      case 3: // set_rx_switches
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: Calling the set_rx_switches CPLD routine" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };

        this->dbArray.at(dbselect).cpldController.set_rx_switches(
          static_cast<magnesium_cpld_ctrl::chan_sel_t>(m_properties.cpldStruct[dbselect].chan_sel),
          static_cast<magnesium_cpld_ctrl::rx_sw2_t>(m_properties.cpldStruct[dbselect].rx_sw2),
          static_cast<magnesium_cpld_ctrl::rx_sw3_t>(m_properties.cpldStruct[dbselect].rx_sw3),
          static_cast<magnesium_cpld_ctrl::rx_sw4_t>(m_properties.cpldStruct[dbselect].rx_sw4),
          static_cast<magnesium_cpld_ctrl::rx_sw5_t>(m_properties.cpldStruct[dbselect].rx_sw5),
          static_cast<magnesium_cpld_ctrl::rx_sw6_t>(m_properties.cpldStruct[dbselect].rx_sw6),
          static_cast<magnesium_cpld_ctrl::lowband_mixer_path_sel_t>(m_properties.cpldStruct[dbselect].lowband_mixer_path_sel),
          m_properties.cpldStruct[dbselect].enb_lowband_mixer,
          static_cast<magnesium_cpld_ctrl::atr_state_t>(m_properties.cpldStruct[dbselect].atr_state),
          m_properties.cpldStruct[dbselect].deferCommit );
        break;
      case 4: // set_tx_atr_bits
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: Calling the set_tx_atr_bits CPLD routine" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        this->dbArray.at(dbselect).cpldController.set_tx_atr_bits(
          static_cast<magnesium_cpld_ctrl::chan_sel_t>(m_properties.cpldStruct[dbselect].chan_sel), 
          static_cast<magnesium_cpld_ctrl::atr_state_t>(m_properties.cpldStruct[dbselect].atr_state),
          m_properties.cpldStruct[dbselect].tx_led,
          m_properties.cpldStruct[dbselect].tx_pa_enb,
          m_properties.cpldStruct[dbselect].tx_amp_enb,
          m_properties.cpldStruct[dbselect].tx_myk_enb,
          m_properties.cpldStruct[dbselect].deferCommit );
        break;
      case 5: // set_trx_sw_atr_bits
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: Calling the set_tx_sw_atr_bits CPLD routine" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        this->dbArray.at(dbselect).cpldController.set_trx_sw_atr_bits(
          static_cast<magnesium_cpld_ctrl::chan_sel_t>(m_properties.cpldStruct[dbselect].chan_sel),
          static_cast<magnesium_cpld_ctrl::atr_state_t>(m_properties.cpldStruct[dbselect].atr_state),
          static_cast<magnesium_cpld_ctrl::sw_trx_t>(m_properties.cpldStruct[dbselect].sw_trx),
          m_properties.cpldStruct[dbselect].deferCommit );
        break;
      case 6: // set_rx_input_atr_bits
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: Calling the set_rx_input_atr_bits CPLD routine" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        this->dbArray.at(dbselect).cpldController.set_rx_input_atr_bits(
          static_cast<magnesium_cpld_ctrl::chan_sel_t>(m_properties.cpldStruct[dbselect].chan_sel),
          static_cast<magnesium_cpld_ctrl::atr_state_t>(m_properties.cpldStruct[dbselect].atr_state),
          static_cast<magnesium_cpld_ctrl::rx_sw1_t>(m_properties.cpldStruct[dbselect].rx_sw1),
          m_properties.cpldStruct[dbselect].rx_led,
          m_properties.cpldStruct[dbselect].rx2_led,
          m_properties.cpldStruct[dbselect].deferCommit );
        break;
      case 7: // set_rx_atr_bits
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: Calling the set_rx_atr_bits CPLD routine" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        this->dbArray.at(dbselect).cpldController.set_rx_atr_bits(
          static_cast<magnesium_cpld_ctrl::chan_sel_t>(m_properties.cpldStruct[dbselect].chan_sel),
          static_cast<magnesium_cpld_ctrl::atr_state_t>(m_properties.cpldStruct[dbselect].atr_state),
          m_properties.cpldStruct[dbselect].rx_amp_enb,
          m_properties.cpldStruct[dbselect].rx_myk_enb,
          m_properties.cpldStruct[dbselect].deferCommit );
        break;
      case 8: // set_rx_lna_atr_bits
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: calling the set_rx_lna_atr_bits routine" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };

        this->dbArray.at(dbselect).cpldController.set_rx_lna_atr_bits(
          static_cast<magnesium_cpld_ctrl::chan_sel_t>(m_properties.cpldStruct[dbselect].chan_sel),
          static_cast<magnesium_cpld_ctrl::atr_state_t>(m_properties.cpldStruct[dbselect].atr_state),
          m_properties.cpldStruct[dbselect].rx_lna1_enb,
          m_properties.cpldStruct[dbselect].rx_lna2_enb,
          m_properties.cpldStruct[dbselect].deferCommit );
        break;
      case 9: // set_scratch
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: setting CPLD PL scratch register" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };   
        this->dbArray.at(dbselect).cpldController.set_scratch(m_properties.cpldStruct[dbselect].regData);
        break;
      case 10: {// get_scratch
        uint16_t getScratchReturnData = 0;
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: getting CPLD PL scratch register" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        getScratchReturnData = this->dbArray.at(dbselect).cpldController.get_scratch();
        
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Daughterboard[" << dbselect << "]Scratch:" << getScratchReturnData << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };       
        break;
      }
      case 11: // reset (cpldreset)
        [=]() // variables placed inside lambda so they have their own scope
        {
         std::ostringstream logstringstream;
         logstringstream << "proxyWorker: calling CPLD reset" << "\n";
         std::string log_string = logstringstream.str();
         log(DRC_LOG_EIGHT, log_string.c_str());
       };
       this->dbArray.at(dbselect).cpldController.reset();
       break;
      case 12: // PS_SignatureReg
      case 13: // PS_MinorRevReg
      case 14: // PS_MajorRevReg
      case 15: // PS_BuildCodeLSB
      case 16: // PS_BuildCodeMSB
      case 17: // PS_Scratch
      //case 18: // PS_CpldControl - Writable Only
      case 19: // PS_LmkControl
      case 20: // PS_LoStatus
      case 21: // PS_MykonosControl
      case 22: {// getCpldReg
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: getting CPLD PS register" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        // Register values from case 12 through case 21
        static std::map<std::uint16_t, std::uint16_t> LUT_MAP =
        {{12,0x00},{13,0x01},{14,0x02},{15,0x03},{16,0x04},
        {17,0x05},{18,0x10},{19,0x11},{20,0x12},{21,0x13}};

        uint8_t getRegAddr = 0;
        if (m_properties.cpldControllerOrdinal == 22) {
          getRegAddr = m_properties.cpldStruct[dbselect].regAddr;
        } else {
          getRegAddr = LUT_MAP.at(m_properties.cpldControllerOrdinal);
        }
        uint16_t getRegReturnData = 0;
        getRegReturnData = this->dbArray.at(dbselect).cpldController.get_reg(getRegAddr);

        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Register Address: 0x" << std::hex << static_cast<std::uint16_t>(getRegAddr) << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 

        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Register Data   : 0x" << std::hex << getRegReturnData << std::dec << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };

        break;
      }
      case 23: {// setCpldReg
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: setting CPLD PS register" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 

        uint8_t setRegAddr = m_properties.cpldStruct[dbselect].regAddr;
        uint16_t setRegData = m_properties.cpldStruct[dbselect].regData;
        this->dbArray.at(dbselect).cpldController.set_reg(setRegAddr,setRegData);

        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Register Address: 0x" << std::hex << static_cast<std::uint16_t>(setRegAddr) << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };

        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Register Data: 0x" << std::hex << setRegData << std::dec << "\n";  
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        break;
      }
      case 24:
      {
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: resetting Mykonos TXRX" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        this->dbArray.at(dbselect).cpldController.mykonosReset();
        break;
      }
      case 25: 
      {
        bool dbselect = m_properties.spiControlStruct.dbselect;
          // use spi struct to read/write
        if(m_properties.spiControlStruct.r_w) { 
          this->dbArray.at(dbselect).cpldController.set_reg(m_properties.spiControlStruct.address,m_properties.spiControlStruct.value);
          } else { // read            
            m_properties.spiReadback = this->dbArray.at(dbselect).cpldController.get_reg(m_properties.spiControlStruct.address);
          }
          
          break;
        }
        default:
        break;
      }
      
      return RCC_OK;
    }

    RCCResult cpldGpioControllerOrdinal_written(){
      int dbselect = (m_properties.dbSelectStruct.gpio == DBSELECTSTRUCT_GPIO_DBA) ? 0 : 1;
      
      if(this->dbArray.at(dbselect).POR & 0x10) {
      [=]() // variables placed inside lambda so they have their own scope
      {
        std::ostringstream logstringstream;
        logstringstream << "proxyWorker: setting CPLD GPIO POR" << "\n";
        std::string log_string = logstringstream.str();
        log(DRC_LOG_EIGHT, log_string.c_str()); 
      };
      this->dbArray.at(dbselect).cpldGpioController.writeGpio(static_cast<bool>(false),0xF);
      this->dbArray.at(dbselect).POR &= ~(0x10);
    }
    std::bitset<4> channelSelect;
    switch(m_properties.cpldGpioControllerOrdinal){
      case 0:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting CPLD GPIO TX1" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          channelSelect = 0x1; // bit 0 active
          this->dbArray.at(dbselect).cpldGpioController.writeGpio(m_properties.cpldGpioStruct[dbselect].cpld_gpio_tx1,channelSelect);
          break;
          case 1:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting CPLD GPIO TX2" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          channelSelect = 0x2; // bit 1 active
          this->dbArray.at(dbselect).cpldGpioController.writeGpio(m_properties.cpldGpioStruct[dbselect].cpld_gpio_tx2,channelSelect);
          
          break;
          case 2:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting CPLD GPIO RX1" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          channelSelect = 0x4; // bit 2 active
          this->dbArray.at(dbselect).cpldGpioController.writeGpio(m_properties.cpldGpioStruct[dbselect].cpld_gpio_rx1,channelSelect);
          break;
          case 3:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting CPLD GPIO RX2" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          channelSelect = 0x8; // bit 3 active
          this->dbArray.at(dbselect).cpldGpioController.writeGpio(m_properties.cpldGpioStruct[dbselect].cpld_gpio_rx2,channelSelect);
          break;
          case 4:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: resetting CPLD GPIOs" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          channelSelect = 0xF;
          this->dbArray.at(dbselect).cpldGpioController.writeGpio(static_cast<bool>(false),channelSelect);
          break;
          default:
          break;
        }
        return RCC_OK;
      }

      RCCResult dsaControllerOrdinal_written() {
        int dbselect = (m_properties.dbSelectStruct.dsa == DBSELECTSTRUCT_DSA_DBA) ? 0 : 1;

        if(this->dbArray.at(dbselect).POR & 0x8){
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: setting DSA POR " << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str()); 
        };
        for (int i = 0; i < 4; i++){
          this->dbArray.at(dbselect).dsaController[i].double_to_hex(static_cast<double>(0.0));
        }
        this->dbArray.at(dbselect).POR &= ~(0x8);
      }

      switch(m_properties.dsaControllerOrdinal) {
        case 0:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting DSA TX1 " << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          this->dbArray.at(dbselect).dsaController[0].double_to_hex(
            static_cast<double>(m_properties.dsaStruct[dbselect].dsa_tx1_atten) );
          break;
          case 1:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting DSA TX2 " << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          this->dbArray.at(dbselect).dsaController[1].double_to_hex(
            static_cast<double>(m_properties.dsaStruct[dbselect].dsa_tx2_atten) );
          break;
          case 2:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting DSA RX1 " << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };

          this->dbArray.at(dbselect).dsaController[2].double_to_hex(
            static_cast<double>(m_properties.dsaStruct[dbselect].dsa_rx1_atten) );
          break;
          case 3:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting DSA RX2" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };

          this->dbArray.at(dbselect).dsaController[3].double_to_hex(
            static_cast<double>(m_properties.dsaStruct[dbselect].dsa_rx2_atten) );
          break;
          case 4:
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: resetting DSAs" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          for (int i = 0; i < 4; i++){
            this->dbArray.at(dbselect).dsaController[i].double_to_hex(static_cast<double>(0.0));
          }
          default:
          break;
        }

        return RCC_OK;
      }

      RCCResult lmk04828ControllerOrdinal_written() {
        int dbselect = (m_properties.dbSelectStruct.lmk == DBSELECTSTRUCT_LMK_DBA) ? 0 : 1;

        if(this->dbArray.at(dbselect).POR & 0x2) {
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: calling the LMK POR" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        this->dbArray.at(dbselect).lmkController.reset(); // call reset routine
        this->dbArray.at(dbselect).POR &= ~(0x2);
      }

      switch(m_properties.lmk04828ControllerOrdinal) {
      case 0: // getProductID
      {
        std::uint16_t productID = 0;
        productID = this->dbArray.at(dbselect).lmkController.get_reg(0x4) << 8;
        productID |= this->dbArray.at(dbselect).lmkController.get_reg(0x5);
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream  << "LMK Product ID for daughterboard[" << dbselect << "] " << std::hex << productID << std::dec << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };          
          break;
        }
        case 1:
        break;
      case 2: // mcrConfigure
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream  << "proxyWorker: calling LMK mcrConfigure" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str()); 
          };
          this->dbArray.at(dbselect).lmkController.mcrConfigure(
            static_cast<std::int_least32_t>(m_properties.lmkStruct[dbselect].masterClockRate),
            static_cast<std::int_least32_t>(m_properties.lmkStruct[dbselect].referenceClockRate) );
          break;
      case 3: // fullConfigure
      this->dbArray.at(dbselect).lmkController.fullConfigure();
      break;
      case 4: // requestSysref
      {
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: calling LMK requestSysRef" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          }; 
          uint16_t the_pattern = m_properties.lmkStruct[dbselect].requestSysrefPattern;
          this->dbArray.at(dbselect).lmkController.requestSysref(the_pattern); 
          break;
        }
      case 5: // getLmkReg
      {   
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: getting LMK register" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          };
          uint8_t getRegReturnData = 0;
          uint16_t getRegAddr = m_properties.lmkStruct[dbselect].regAddr;
          getRegReturnData = this->dbArray.at(dbselect).lmkController.get_reg(getRegAddr);

          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "Register Address: 0x" << std::hex << getRegAddr << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          }; 
          
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "Register Data   : 0x" << std::hex << static_cast<std::uint16_t>(getRegReturnData) << std::dec << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          };
          break;
        }
      case 6: // setLmkReg
      {
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "proxyWorker: setting LMK register" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          };
          uint16_t setRegAddr = m_properties.lmkStruct[dbselect].regAddr;
          uint8_t setRegData = m_properties.lmkStruct[dbselect].regData;
          this->dbArray.at(dbselect).lmkController.set_reg(setRegAddr,setRegData);
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "Register Address: 0x" << std::hex << setRegAddr << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          }; 
          
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "Register Data   : 0x" << std::hex << static_cast<std::uint16_t>(setRegData) << std::dec << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          };
          break;
        }
      case 7: // lmk04828_example_application_testing
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "Example Application Testing LMK" << "\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          }; 
          break;
      case 8: // spiReadWrite
      {
        bool dbselect = m_properties.spiControlStruct.dbselect;
          if(m_properties.spiControlStruct.r_w) { // write
            this->dbArray.at(dbselect).lmkController.set_reg(m_properties.spiControlStruct.address,m_properties.spiControlStruct.value);
          } else { // read
            m_properties.spiReadback = this->dbArray.at(dbselect).lmkController.get_reg(m_properties.spiControlStruct.address);
          }
        }
        default:
        break;
      };

      return RCC_OK;
    }

    RCCResult transceiverControllerOrdinal_written() {
      int dbselect = (m_properties.dbSelectStruct.txrx == DBSELECTSTRUCT_TXRX_DBA) ? 0 : 1;  

      uint8_t productId = 0xFF;
      uint32_t getRate = 0;
      uint32_t parentRate = m_properties.transceiverStruct[dbselect].clkgen_parentRate;
      uint32_t setRate = m_properties.transceiverStruct[dbselect].clkgen_setRate;

    // Configuring the transceiver
      struct drcConfig localConfig;
      localConfig.rx = static_cast<bool>(m_properties.drcConfigStruct.rx);
      localConfig.tuning_freq_MHz = static_cast<double>(m_properties.drcConfigStruct.tuning_freq_MHz);
      localConfig.bandwidth_3dB_MHz = static_cast<double>(m_properties.drcConfigStruct.bandwidth_3dB_MHz);
      localConfig.sampling_rate_Msps = static_cast<double>(m_properties.drcConfigStruct.sampling_rate_Msps);
      localConfig.samples_are_complex = static_cast<bool>(m_properties.drcConfigStruct.samples_are_complex);
      localConfig.gain_dB = static_cast<double>(m_properties.drcConfigStruct.gain_dB);
      localConfig.gain_mode = static_cast<bool>(m_properties.drcConfigStruct.gain_mode);
      localConfig.tolerance_tuning_freq_MHz = static_cast<double>(m_properties.drcConfigStruct.tolerance_tuning_freq_MHz);
      localConfig.tolerance_bandwidth_3dB_MHz = static_cast<double>(m_properties.drcConfigStruct.tolerance_bandwidth_3dB_MHz);
      localConfig.tolerance_sampling_rate_Msps = static_cast<double>(m_properties.drcConfigStruct.tolerance_sampling_rate_Msps);
      localConfig.tolerance_gain_dB = static_cast<double>(m_properties.drcConfigStruct.tolerance_gain_dB);
      localConfig.rf_port_num = static_cast<uint8_t>(m_properties.drcConfigStruct.rf_port_num);
      
      switch(m_properties.transceiverControllerOrdinal) {
      case 0: // class_MYKONOS_getProductId
      //this->dbArray.at(dbselect).transceiverController.class_MYKONOS_getProductId(&productId);
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "productId = 0x" << std::hex << static_cast<uint16_t>(productId) << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        break;
      case 1: // configure_transceiver
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << std::dec << "proxyWorker: configuring TXRX" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        this->dbArray.at(dbselect).transceiverController.configure_transceiver(localConfig);
        break;
      case 2: // clkgen_set_rate
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: setting CLK GEN: " << std::dec << setRate / 1000 << " kHz" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        //this->dbArray.at(dbselect).transceiverController.class_axi_clkgen_set_rate(setRate);
        break;
      case 3: // clkgen_get_r[ate
      //this->dbArray.at(dbselect).transceiverController.class_axi_clkgen_get_rate(&getRate);
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Clkgen Get Rate Value:" << getRate << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        break;
      case 4: // clkgen_init
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: initalizing CLK GEN" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        //this->dbArray.at(dbselect).transceiverController.class_axi_clkgen_init(parentRate);
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Base Address: 0x" <<  std::hex << this->dbArray.at(dbselect).transceiverController.tx_clkgen->base << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        break;
      case 5: // headless
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: running headless.c sequence" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        }; 
        this->dbArray.at(dbselect).transceiverController.headless(localConfig.rx);
        break;
      case 6: // run configuration
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: running Transceiver::run_configuration" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        this->dbArray.at(dbselect).transceiverController.run_configuration();
        break;
      case 7: // gpioTester method
      [=](){
        std::ostringstream logstringstream;
        logstringstream << "proxyWorker: running Transceiver::gpioTester" << "\n";
        log(DRC_LOG_EIGHT, logstringstream.str().c_str());
      };
      enum gpioCases {
        getGpioSetLevel = 0,
        getGpio3v3SetLevel,
        getGpio3v3SourceCtrl,
        getGpioOe,
        getGpioSourceCtrl,
        END
      };
      for(std::uint32_t scenario = getGpioSetLevel; scenario != END; scenario++){
        this->dbArray.at(dbselect).transceiverController.gpioTester(scenario);
      }
      break;
      case 8: // adrv9371_gpio_test method
      {
        auto _lambda = [=](){
          std::ostringstream logstringstream;
          logstringstream << "proxyWorker: running N310_AD9371_GPIO::readGpio\n";
          logstringstream << this->dbArray.at(dbselect).whichDB;
          logstringstream << " Gpio pins:\n\t0x";
          logstringstream << std::hex << static_cast<std::uint16_t>(this->dbArray.at(dbselect).ad9371GpioController.readGpio());
          log(DRC_LOG_THREE, logstringstream.str().c_str());
        };
        _lambda();
        break;
      }
      default:
        [=]() // variables placed inside lambda so they have their own scope
        {
          std::ostringstream logstringstream;
          logstringstream << "Wrong case statement for Transceiver Controller" << "\n";
          std::string log_string = logstringstream.str();
          log(DRC_LOG_EIGHT, log_string.c_str());
        };
        // Let us log above and also keep the cerr 
        std::cerr << "Wrong case statement for Transceiver Controller" << std::flush;
        break;
      };
      return RCC_OK;
    }

    RCCResult adfControllerOrdinal_written() {
      int dbselect = (m_properties.dbSelectStruct.adf == DBSELECTSTRUCT_ADF_DBA) ? 0 : 1;  

      switch(m_properties.adfControllerOrdinal) {
        case 0:
        break;
        case 1:
        break;
        case 2:
        try {
          (this->dbArray.at(dbselect).adfController[0])->_lo_enable(
           m_properties.adfStruct[dbselect].lo_freq,
           m_properties.adfStruct[dbselect].ref_clock_freq,
           m_properties.adfStruct[dbselect].int_n_mode);
        }
        catch(int e) {
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "An exception occurred in enabling the lo_enable for daughterboard[" << dbselect << "].\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          }; 
        }
        try {
          (this->dbArray.at(dbselect).adfController[1])->_lo_enable(
           m_properties.adfStruct[dbselect].lo_freq,
           m_properties.adfStruct[dbselect].ref_clock_freq,
           m_properties.adfStruct[dbselect].int_n_mode);
        }
        catch(int e) {
          [=]() // variables placed inside lambda so they have their own scope
          {
            std::ostringstream logstringstream;
            logstringstream << "An exception occurred in enabling the lo_enable for daughterboard[" << dbselect << "].\n";
            std::string log_string = logstringstream.str();
            log(DRC_LOG_EIGHT, log_string.c_str());
          }; 
        }
        break;
        case 3:
        (this->dbArray.at(dbselect).adfController[0])->_lo_disable();
        (this->dbArray.at(dbselect).adfController[1])->_lo_disable();
        break;
        default:
        break;
      };
      return RCC_OK;
    }

    RCCResult channelConfig_written() {

    // THE LATEST VERSION OF CONSTRAINED RF PORT NUM IS THE FOLLOWING:
    // rf_port_num channelConfiguration.rf_port_num:
    // xyz: 3 bits = 8 total motherboard (unique) channels
    // x = daughterboard (db) select
    // y = tx, rx_
    // z = dualChannel individual channel selection

      return RCC_OK;
    }

  // Used to debug the m_properties.configurations and m_properties.status DRC Worker Properties

    RCCResult debugConfig_written() {
      std::stringstream drcConfigLogOut;

      drcConfigLogOut << "\n";

      std::uint16_t tabCount = 0;

    auto tabHelper = [](std::uint16_t index)->std::string{ // only needed inside this debugging function
      std::string tabs = "";
      for(std::uint16_t x = 0; x < index; x++){
        tabs += "\t";
      }
      return tabs;
    };

    drcConfigLogOut << tabHelper(tabCount) << "Logging the DRC Configuration data structure\n";

    drcConfigLogOut << tabHelper(tabCount) << "m_properties.configurations = { // beginning worker property\n";

    tabCount++; // for the struct
    drcConfigLogOut << tabHelper(tabCount) << "length = " << m_properties.configurations.length << "\n";

    drcConfigLogOut << tabHelper(tabCount) << "data[DRC_N310_MAX_CONFIGURATIONS_P] = {\n"; // array of MAX length DRC_N310_MAX_CONFIGURATIONS_P

    tabCount++; // before loop
    std::int16_t drcConfig_count = -1;
    for(auto &drcConfig : m_properties.configurations.data){
      drcConfig_count++;
      drcConfigLogOut << tabHelper(tabCount) << drcConfig_count << " = {\n"; // each configuration
      tabCount++; // beginning of structure
      // description
      drcConfigLogOut << tabHelper(tabCount) << "description = " << drcConfig.description << "\n"; // each configuration
      // recoverable
      std::string recoverable = (drcConfig.recoverable)? "true": "false";
      drcConfigLogOut << tabHelper(tabCount) << "recoverable = " << recoverable << "\n"; // each configuration
      // channels
      drcConfigLogOut << tabHelper(tabCount) << "channels = { // beginning of channels struct\n"; // each configuration
      tabCount++; // beginning of channels struct
      drcConfigLogOut << tabHelper(tabCount) << "length = " << drcConfig.channels.length << "\n"; // each configuration
      std::int16_t channelCount = -1;
      drcConfigLogOut << tabHelper(tabCount) << "data = [DRC_N310_MAX_CHANNELS_P] // beginning of channels.data array\n"; // each configuration
      tabCount++; // beginning of channels.data array
      for(auto &channelConfiguration : drcConfig.channels.data){
        channelCount++;
        if(validPortNum(channelConfiguration.rf_port_num)){
          continue;
        }
        drcConfigLogOut << tabHelper(tabCount)<< channelCount << " = {\n"; // each channel
        tabCount++; // beginning of structure
        drcConfigLogOut << tabHelper(tabCount) << "description = "                  << channelConfiguration.description << "\n";
        std::string rx = (channelConfiguration.rx)? "true": "false";
        drcConfigLogOut << tabHelper(tabCount) << "rx = "                           << rx << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "tuning_freq_MHz = "              << channelConfiguration.tuning_freq_MHz << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "bandwidth_3dB_MHz = "            << channelConfiguration.bandwidth_3dB_MHz << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "sampling_rate_Msps = "           << channelConfiguration.sampling_rate_Msps << "\n";
        std::string samples_are_complex = (channelConfiguration.samples_are_complex)? "true": "false";
        drcConfigLogOut << tabHelper(tabCount) << "samples_are_complex = "          << samples_are_complex << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "gain_mode = "                    << channelConfiguration.gain_mode << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "gain_dB = "                      << channelConfiguration.gain_dB << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "tolerance_tuning_freq_MHz = "    << channelConfiguration.tolerance_tuning_freq_MHz << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "tolerance_bandwidth_3dB_MHz = "  << channelConfiguration.tolerance_bandwidth_3dB_MHz << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "tolerance_sampling_rate_Msps = " << channelConfiguration.tolerance_sampling_rate_Msps << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "tolerance_gain_dB = "            << channelConfiguration.tolerance_gain_dB << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "rf_port_name = "                 << channelConfiguration.rf_port_name << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "rf_port_num = "                  << static_cast<std::uint16_t>(channelConfiguration.rf_port_num) << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "app_port_num = "                 << static_cast<std::uint16_t>(channelConfiguration.app_port_num) << "\n";
        tabCount--; // end of structure
        drcConfigLogOut << tabHelper(tabCount) << "}, // m_properties.configureations.data["<<channelCount<<"] struct\n";
      }
      tabCount--; // end of array of channels.data array
      drcConfigLogOut << tabHelper(tabCount) << "] // m_properties.configurations.channels.data["<<drcConfig_count<<"].data array\n";
      tabCount--; // end of channels struct

      tabCount--; // end of structure -- c
      drcConfigLogOut << tabHelper(tabCount) << "} // m_properties.configurations.channels structure\n";
    }
    tabCount--; // end of array of drcConfigurations
    drcConfigLogOut << tabHelper(tabCount) << "] // m_properties.configurations.data array\n";
    tabCount--; // end of worker property
    drcConfigLogOut << tabHelper(tabCount) << "} // m_properties.configurations structure\n";


    log(DRC_LOG_THREE,drcConfigLogOut.str().c_str()); // configurations structure

    // clear the log
    drcConfigLogOut.str(std::string());

    drcConfigLogOut << "\n";

    // m_properties.status
      // .length
      // .data[DRC_N310_MAX_CONFIGURATIONS_P]
        // enum .state
        // char error[]
        // .channels struct
          // .length
          // .data[DRC_N310_MAX_CHANNELS_P] --> type StatusChannels
            // .tuning_freq_MHz --> double
            // .bandwidth_3dB_MHz --> double
            // .sampling_rate_Msps --> double
            // .gain_dB --> double

    tabCount = 0; // just reset this

    drcConfigLogOut << tabHelper(tabCount) << "m_properties.status = { // beginning worker property\n";
    tabCount++;
    drcConfigLogOut << tabHelper(tabCount) << "length = " << m_properties.status.length << "\n";
    drcConfigLogOut << tabHelper(tabCount) << "data[DRC_N310_MAX_CONFIGURATIONS_P] = {\n"; // array of MAX length DRC_N310_MAX_CONFIGURATIONS_P
    drcConfig_count = -1;
    tabCount++; // before the loop
    for(auto &drcConfigStatus : m_properties.status.data){
      drcConfig_count++;
      drcConfigLogOut << tabHelper(tabCount) << drcConfig_count << " = {\n"; // each configuration
      tabCount++; // beginning of structure
      // enumeration for state
      drcConfigLogOut << tabHelper(tabCount) << "state = " << drcConfigStatus.state << "\n"; // each configuration
      // error string (RCC char array)
      drcConfigLogOut << tabHelper(tabCount) << "error = " << drcConfigStatus.error << "\n"; // each configuration
      tabCount++; // beginning of channels struct
      drcConfigLogOut << tabHelper(tabCount) << "length = " << drcConfigStatus.channels.length << "\n"; // each configuration
      std::int16_t channelCount = -1;
      drcConfigLogOut << tabHelper(tabCount) << "data = [DRC_N310_MAX_CHANNELS_P] // beginning of channels.data array\n"; // each configuration
      tabCount++; // beginning of channels.data array
      for(auto &channelConfiguration : drcConfigStatus.channels.data){
        channelCount++;
        drcConfigLogOut << tabHelper(tabCount)<< channelCount << " = {\n"; // each cahnnel
        tabCount++; // beginning of structure
        drcConfigLogOut << tabHelper(tabCount) << "tuning_freq_MHz = "    << channelConfiguration.tuning_freq_MHz << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "bandwidth_3dB_MHz = "  << channelConfiguration.bandwidth_3dB_MHz << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "sampling_rate_Msps = " << channelConfiguration.sampling_rate_Msps << "\n";
        drcConfigLogOut << tabHelper(tabCount) << "gain_dB = "            << channelConfiguration.gain_dB << "\n";
        tabCount--; // end of structure
      }
      tabCount--; // end of array of channels.data array
      drcConfigLogOut << tabHelper(tabCount) << "] // m_properties.status.channels.data["<<drcConfig_count<<"].data array\n";
      tabCount--; // end of channels struct

      tabCount--; // end of structure -- c
      drcConfigLogOut << tabHelper(tabCount) << "} // m_properties.status.channels structure\n";
    }
    tabCount--; // end of array of drcConfigurations
    drcConfigLogOut << tabHelper(tabCount) << "] // m_properties.status.data array\n";
    tabCount--; // end of worker property
    drcConfigLogOut << tabHelper(tabCount) << "} // m_properties.status structure\n";

    // log the status structure

    log(DRC_LOG_THREE,drcConfigLogOut.str().c_str());

    return RCC_OK;
  }

  // Below are the helper methods for the DRC's FSM operations

  RCCResult init_config(std::uint32_t configOrdinal){

    (void) configOrdinal; // this variable is known to be unused
    
    return RCC_OK;
  }

  RCCResult check_state(std::uint32_t configOrdinal){ // make sure ordinal is within LENGTH of status
    std::stringstream check_state_logOut;
    check_state_logOut << "check_state():\n";
    if(configOrdinal > m_properties.status.size()){ // error: User requested ordinal outside of range
      check_state_logOut << "User requested value (" << configOrdinal << ") > status length (" << m_properties.status.length << ")\n";
      log(DRC_LOG_THREE,check_state_logOut.str().c_str());
      return RCC_ERROR;
    }
    return RCC_OK;
  }

  static const std::array<std::string,4> stateEnums;

  // use tolerances with 'actual' channel structure (internally managed) to confirm whether channel configuration is valid
  bool validateChannelConfig(struct drcConfig &actualChannel,struct ConfigurationsChannels userChannel){
    // make sure configurations are within tolerance values
    std::stringstream validateChannelConfig_logOut;
    validateChannelConfig_logOut << "validateChannelConfig(): rf_port_num=" << static_cast<std::uint16_t>(actualChannel.rf_port_num)
    << " | " << static_cast<std::uint16_t>(userChannel.rf_port_num) << "\n";
    
    // channel configuration checks
    userChannel.tuning_freq_MHz = std::abs(userChannel.tuning_freq_MHz);
    if((std::abs(actualChannel.tuning_freq_MHz - userChannel.tuning_freq_MHz)) > userChannel.tolerance_tuning_freq_MHz){
      validateChannelConfig_logOut << "Failed tolerance_tuning_freq_MHz check:\n";
      validateChannelConfig_logOut << "\tProgrammed value: " << actualChannel.tuning_freq_MHz << "\n";
      validateChannelConfig_logOut << "\tRequested value: " << userChannel.tuning_freq_MHz << "\n";
      validateChannelConfig_logOut << "\tDefined Tolerance: " << userChannel.tolerance_tuning_freq_MHz << "\n";
      log(DRC_LOG_THREE,validateChannelConfig_logOut.str().c_str());
      return false;
    }
    if((std::abs(actualChannel.bandwidth_3dB_MHz - userChannel.bandwidth_3dB_MHz)) > userChannel.tolerance_bandwidth_3dB_MHz){
      validateChannelConfig_logOut << "Failed tolerance_bandwidth_3dB_MHz check:\n";
      validateChannelConfig_logOut << "\tProgrammed value: " << actualChannel.bandwidth_3dB_MHz << "\n";
      validateChannelConfig_logOut << "\tRequested value: " << userChannel.bandwidth_3dB_MHz << "\n";
      validateChannelConfig_logOut << "\tDefined Tolerance: " << userChannel.tolerance_bandwidth_3dB_MHz << "\n";
      log(DRC_LOG_THREE,validateChannelConfig_logOut.str().c_str());
      return false;
    }
    if((std::abs(actualChannel.sampling_rate_Msps - userChannel.sampling_rate_Msps)) > userChannel.tolerance_sampling_rate_Msps){
      validateChannelConfig_logOut << "Failed tolerance_sampling_rate_Msps check:\n";
      validateChannelConfig_logOut << "\tProgrammed value: " << actualChannel.sampling_rate_Msps << "\n";
      validateChannelConfig_logOut << "\tRequested value: " << userChannel.sampling_rate_Msps << "\n";
      validateChannelConfig_logOut << "\tDefined Tolerance: " << userChannel.tolerance_sampling_rate_Msps << "\n";
      log(DRC_LOG_THREE,validateChannelConfig_logOut.str().c_str());
      return false;
    }
    if((std::abs(actualChannel.gain_dB - userChannel.gain_dB)) > userChannel.tolerance_gain_dB){
      validateChannelConfig_logOut << "Failed tolerance_gain_dB check:\n";
      validateChannelConfig_logOut << "\tProgrammed value: " << actualChannel.gain_dB << "\n";
      validateChannelConfig_logOut << "\tRequested value: " << userChannel.gain_dB << "\n";
      validateChannelConfig_logOut << "\tDefined Tolerance: " << userChannel.tolerance_gain_dB << "\n";
      log(DRC_LOG_THREE,validateChannelConfig_logOut.str().c_str());
      return false;
    }
    return true;
  }

  void toggleFrontEnd (std::uint32_t configOrdinal, bool doActivate){
    std::array<std::bitset<4>,2> channelTogglePattern;
    for(auto &channelConfiguration : m_properties.configurations.data[configOrdinal].channels.data){ // every channel
      if(validPortNum(channelConfiguration.rf_port_num)){ // only configure channels with this description
        std::uint16_t dbSelect = (channelConfiguration.rf_port_num & 0x4) >> 2;
        // this->_routingTuple.setCS((channelConfiguration.rf_port_num & 0x3));
        // set ATR -> ON 
        channelTogglePattern.at(dbSelect).set(channelConfiguration.rf_port_num & 0x3);
      }
    }
    for(int dbSelect = 0; dbSelect < dbArray.size(); dbSelect++){ // toggle all the channels
      this->dbArray.at(dbSelect).cpldGpioController.writeGpio(doActivate,channelTogglePattern.at(dbSelect));
      this->dbArray.at(dbSelect).POR |= 0x10;
    }
  }


  //$ prepare_config(ordinal)
  // no extra prepare_config mentioned in the drc proxy api
  RCCResult prepare_config (std::uint32_t configOrdinal){
    std::stringstream prepare_config_logOut;
    prepare_config_logOut << "prepare_config():\n";

    #if (OCPI_PARAM_drc_n310_dba_en())
      // Setting of DBA CIC Interpolation Configurations
      #if (OCPI_PARAM_drc_n310_dba_tx0_en() && OCPI_PARAM_drc_n310_dba_cic_int_en())
        slaves.DBA_cic_int0.set_up_sample_factor(properties().dba_int0_up_sample_factor);
        slaves.DBA_cic_int0.set_scale_output(properties().dba_int0_scale_output);
        slaves.DBA_cic_int0.set_flush_length(properties().dba_int0_flush_length);
      #endif

      #if (OCPI_PARAM_drc_n310_dba_tx1_en() & OCPI_PARAM_drc_n310_dba_cic_int_en())
        slaves.DBA_cic_int1.set_up_sample_factor(properties().dba_int1_up_sample_factor);
        slaves.DBA_cic_int1.set_scale_output(properties().dba_int1_scale_output);
        slaves.DBA_cic_int1.set_flush_length(properties().dba_int1_flush_length);
      #endif

      // Setting of DBA CIC Decimation Configurations
      #if (OCPI_PARAM_drc_n310_dba_rx0_en() && OCPI_PARAM_drc_n310_dba_cic_dec_en())
        slaves.DBA_cic_dec0.set_down_sample_factor(properties().dba_dec0_down_sample_factor);
        slaves.DBA_cic_dec0.set_scale_output(properties().dba_dec0_scale_output);
        slaves.DBA_cic_dec0.set_flush_length(properties().dba_dec0_flush_length);
      #endif

      #if (OCPI_PARAM_drc_n310_dba_rx1_en() & OCPI_PARAM_drc_n310_dba_cic_dec_en())
        slaves.DBA_cic_dec1.set_down_sample_factor(properties().dba_dec1_down_sample_factor);
        slaves.DBA_cic_dec1.set_scale_output(properties().dba_dec1_scale_output);
        slaves.DBA_cic_dec1.set_flush_length(properties().dba_dec1_flush_length);
      #endif

      // Setting of DBA qDAC / qADC Sub-Worker Configurations
      #if (OCPI_PARAM_drc_n310_dba_tx0_en() || OCPI_PARAM_drc_n310_dba_tx1_en())
        slaves.DBA_qdac_sub.set_channels_are_swapped(properties().dba_dac_channels_are_swapped);
        slaves.DBA_qdac_sub.set_lanes_are_swapped(properties().dba_dac_lanes_are_swapped);
        slaves.DBA_qdac_sub.set_mask_ch0i(properties().dba_dac_mask_ch0i);
        slaves.DBA_qdac_sub.set_mask_ch0q(properties().dba_dac_mask_ch0q);
        slaves.DBA_qdac_sub.set_mask_ch1i(properties().dba_dac_mask_ch1i);
        slaves.DBA_qdac_sub.set_mask_ch1q(properties().dba_dac_mask_ch1q);
      #endif

      #if (OCPI_PARAM_drc_n310_dba_rx0_en() || OCPI_PARAM_drc_n310_dba_rx1_en())        
        slaves.DBA_qadc_sub.set_channels_are_swapped(properties().dba_adc_channels_are_swapped);
        slaves.DBA_qadc_sub.set_lanes_are_swapped(properties().dba_adc_lanes_are_swapped);
        slaves.DBA_qadc_sub.set_mask_ln0(properties().dba_adc_mask_ln0);
        slaves.DBA_qadc_sub.set_mask_ln1(properties().dba_adc_mask_ln1);
        slaves.DBA_qadc_sub.set_mask_ln2(properties().dba_adc_mask_ln2);
        slaves.DBA_qadc_sub.set_mask_ln3(properties().dba_adc_mask_ln3);
      #endif
    #endif

    #if (OCPI_PARAM_drc_n310_dbb_en())
    // Setting of DBB CIC Interpolation Configurations
      #if (OCPI_PARAM_drc_n310_dbb_tx0_en() && OCPI_PARAM_drc_n310_dbb_cic_int_en())
        slaves.DBB_cic_int0.set_up_sample_factor(properties().dbb_int0_up_sample_factor);
        slaves.DBB_cic_int0.set_scale_output(properties().dbb_int0_scale_output);
        slaves.DBB_cic_int0.set_flush_length(properties().dbb_int0_flush_length);
      #endif

      #if (OCPI_PARAM_drc_n310_dbb_tx1_en() & OCPI_PARAM_drc_n310_dbb_cic_int_en())
        slaves.DBB_cic_int1.set_up_sample_factor(properties().dbb_int1_up_sample_factor);
        slaves.DBB_cic_int1.set_scale_output(properties().dbb_int1_scale_output);
        slaves.DBB_cic_int1.set_flush_length(properties().dbb_int1_flush_length);
      #endif

      // Setting of DBB CIC Decimation Configurations
      #if (OCPI_PARAM_drc_n310_dbb_rx0_en() && OCPI_PARAM_drc_n310_dbb_cic_dec_en())
        slaves.DBB_cic_dec0.set_down_sample_factor(properties().dbb_dec0_down_sample_factor);
        slaves.DBB_cic_dec0.set_scale_output(properties().dbb_dec0_scale_output);
        slaves.DBB_cic_dec0.set_flush_length(properties().dbb_dec0_flush_length);
      #endif

      #if (OCPI_PARAM_drc_n310_dbb_rx1_en() & OCPI_PARAM_drc_n310_dbb_cic_dec_en())
        slaves.DBB_cic_dec1.set_down_sample_factor(properties().dbb_dec1_down_sample_factor);
        slaves.DBB_cic_dec1.set_scale_output(properties().dbb_dec1_scale_output);
        slaves.DBB_cic_dec1.set_flush_length(properties().dbb_dec1_flush_length);
      #endif

      // Setting of DBB qDAC / qADC Sub-Worker Configurations
      #if (OCPI_PARAM_drc_n310_dba_tx0_en() || OCPI_PARAM_drc_n310_dba_tx1_en())
        slaves.DBB_qdac_sub.set_channels_are_swapped(properties().dbb_dac_channels_are_swapped);
        slaves.DBB_qdac_sub.set_lanes_are_swapped(properties().dbb_dac_lanes_are_swapped);
        slaves.DBB_qdac_sub.set_mask_ch0i(properties().dbb_dac_mask_ch0i);
        slaves.DBB_qdac_sub.set_mask_ch0q(properties().dbb_dac_mask_ch0q);
        slaves.DBB_qdac_sub.set_mask_ch1i(properties().dbb_dac_mask_ch1i);
        slaves.DBB_qdac_sub.set_mask_ch1q(properties().dbb_dac_mask_ch1q);
      #endif

      #if (OCPI_PARAM_drc_n310_dba_rx0_en() || OCPI_PARAM_drc_n310_dba_rx1_en())
        slaves.DBB_qadc_sub.set_channels_are_swapped(properties().dbb_adc_channels_are_swapped);
        slaves.DBB_qadc_sub.set_lanes_are_swapped(properties().dbb_adc_lanes_are_swapped);
        slaves.DBB_qadc_sub.set_mask_ln0(properties().dbb_adc_mask_ln0);
        slaves.DBB_qadc_sub.set_mask_ln1(properties().dbb_adc_mask_ln1);
        slaves.DBB_qadc_sub.set_mask_ln2(properties().dbb_adc_mask_ln2);
        slaves.DBB_qadc_sub.set_mask_ln3(properties().dbb_adc_mask_ln3);
      #endif
    #endif


    //$ [condition State_equals_error_state_equals_inactive user-description]
    if((init_config(configOrdinal)) != RCC_OK){
      //$ Exit: "Already Prepared Or Operating"
      return RCC_ERROR;
    }
    // prepare config -- have to do state checking here since API doesn't do it for us
    if(m_properties.status.data[configOrdinal].state == Drc_n310WorkerTypes::State::STATUS_STATE_PREPARED){
      return RCC_OK;
    } else if(m_properties.status.data[configOrdinal].state == Drc_n310WorkerTypes::State::STATUS_STATE_OPERATING){
      return RCC_ERROR;
    }
     
     auto &conf = m_properties.configurations.data[configOrdinal];

    // dual channel tracker -- only GAIN is independently programmed between 'dual coupled channel's, first come first serve for other parameters
    std::array<std::vector<struct drcConfig>,2> daughterboard_configuration_container;
    for(unsigned n = 0; n < conf.channels.size(); ++n){
      auto &conf_channel = conf.channels.data[n];
      if(validPortNum(conf_channel.rf_port_num)){ // only configure channels with a valid port num -- app dev manually changed this
        bool dbselect = (((conf_channel.rf_port_num) & 0x4) >> 2); // port number relates to daughterboard
        // make sure rx lines up with rf_port_num
        if((conf_channel.rx) != ((conf_channel.rf_port_num & 0x2) >> 1) ){ // first error generation
          std::string x = (conf_channel.rx)? "true":"false" ;
          prepare_config_logOut << "User misconfiguration between channel RX boolean (" << x << ") and rf_port_num (";
          prepare_config_logOut << static_cast<std::uint16_t>(conf_channel.rf_port_num) << ") -- configuration placed into error state\n";
          prepare_config_logOut << "(rf_port_num & 0x2) == (rx << 1)\n";
          log(DRC_LOG_THREE,prepare_config_logOut.str().c_str());
          return RCC_ERROR;
        }

        // append to the two vectors
        struct drcConfig drc_conf_struct = {
          static_cast<bool>(conf_channel.rx), // only 1 receive and 1 transmit port/channel per daughterboard, 2 max per configuration
          static_cast<double>(conf_channel.tuning_freq_MHz), // can be negative
          static_cast<double>(conf_channel.bandwidth_3dB_MHz),
          static_cast<double>(conf_channel.sampling_rate_Msps),
          static_cast<bool>(conf_channel.samples_are_complex),
          static_cast<double>(conf_channel.gain_dB),
          static_cast<bool>(conf_channel.gain_mode),
          static_cast<double>(conf_channel.tolerance_tuning_freq_MHz),
          static_cast<double>(conf_channel.tolerance_bandwidth_3dB_MHz),
          static_cast<double>(conf_channel.tolerance_sampling_rate_Msps),
          static_cast<double>(conf_channel.tolerance_gain_dB),
          static_cast<std::uint8_t>(conf_channel.rf_port_num)
        };
        daughterboard_configuration_container.at(dbselect).push_back(drc_conf_struct);
      } // each active channel (valid port num)
    } // for each channel in configuration array

    // configure the daughterboards
    for(int whichDB_idx = 0; whichDB_idx < this->dbArray.size(); whichDB_idx++){
      if(daughterboard_configuration_container.at(whichDB_idx).size()){ // only if daughterboard targeted channel profiles
        bool passed_configuration = this->dbArray.at(whichDB_idx).set_profile(daughterboard_configuration_container.at(whichDB_idx));
        if(!passed_configuration){
          prepare_config_logOut << this->dbArray.at(whichDB_idx).whichDB << " failed to properly configure its profile\n";
          log(DRC_LOG_THREE,prepare_config_logOut.str().c_str());
          return RCC_ERROR;
        }
      }
    }
    prepare_config_logOut << "Upgraded config " << configOrdinal << " from "
    << stateEnums.at(m_properties.status.data[configOrdinal].state) << " state to prepared\n";
    log(DRC_LOG_THREE,prepare_config_logOut.str().c_str());

    // if make it to end, then prepared!
    return RCC_OK;
  }

  RCCResult start_config (std::uint32_t configOrdinal){
    std::stringstream start_config_logOut;

    start_config_logOut << "start_config():\n";
    if((init_config(configOrdinal)) == RCC_OK){
      // 1st check to see if we need to update the frequencies
      auto &conf = m_properties.configurations.data[configOrdinal];
      for(unsigned n = 0; n < conf.channels.size(); ++n){
        auto &conf_channel = conf.channels.data[n];
        if(validPortNum(conf_channel.rf_port_num)){ // only configure channels with a valid port num -- app dev manually changed this
          bool dbselect = (((conf_channel.rf_port_num) & 0x4) >> 2); // port number relates to daughterboard
          // make sure rx lines up with rf_port_num
          if((conf_channel.rx) != ((conf_channel.rf_port_num & 0x2) >> 1) ){ // first error generation
            std::string x = (conf_channel.rx)? "true":"false" ;
            start_config_logOut << "User misconfiguration between channel RX boolean (" << x << ") and rf_port_num (";
            start_config_logOut << static_cast<std::uint16_t>(conf_channel.rf_port_num) << ") -- configuration placed into error state\n";
            start_config_logOut << "(rf_port_num & 0x2) == (rx << 1)\n";
            log(DRC_LOG_THREE,start_config_logOut.str().c_str());
            return RCC_ERROR;
          }
          // check the tuning frequency
          auto which_dboard = (conf_channel.rf_port_num & 0x4) ? &this->dbArray.at(1) : &this->dbArray.at(0);
          which_dboard->update_tuning_frequency(conf_channel.rx,conf_channel.tuning_freq_MHz);
        } // each active channel (valid port num)
      } // for each channel in configuration array
      // need to ONLY turn on transceivers that are READY, right now just doing both
      for(auto &dboard : this->dbArray){
        dboard.toggle_profile(true); // only toggles daughterboards with locked in profiles
      }
      // Paradigm: Only one configuration can run at any point in time, so both daughterboard FE's can be shutoff here
      start_config_logOut << "Upgraded config " << configOrdinal << " from " << stateEnums.at(m_properties.status.data[configOrdinal].state) << " state to OPERATING state\n";
      log(DRC_LOG_THREE,start_config_logOut.str().c_str());
      return RCC_OK;
    } else {
      start_config_logOut << "Failed init_config(): check\n";
      log(DRC_LOG_THREE,start_config_logOut.str().c_str());
      return RCC_ERROR;
    }
  }

  RCCResult stop_config (std::uint32_t configOrdinal){
    std::stringstream stop_config_logOut;
    stop_config_logOut << "stop_config():\n";
    if((init_config(configOrdinal)) == RCC_OK){
      // need to only disable transceivers that are 'ready'
      for(auto &dboard : this->dbArray){
        dboard.toggle_profile(false); // only toggles daughterboards with locked in profiles
      }
      // should say OPERATING state here
      stop_config_logOut << "Downgraded from " << stateEnums.at(m_properties.status.data[configOrdinal].state) << " state to PREPARED state\n";
      log(DRC_LOG_THREE,stop_config_logOut.str().c_str());
      return RCC_OK;
    } else {
      stop_config_logOut << "Failed init_config(): check\n";
      log(DRC_LOG_THREE,stop_config_logOut.str().c_str());
      return RCC_ERROR;
    }

    return RCC_OK;
  }

  RCCResult release_config (std::uint32_t configOrdinal){
    std::stringstream release_config_logOut;
    release_config_logOut << "release_config():\n";
    RCCResult retVal;
    if((retVal = init_config(configOrdinal)) == RCC_OK){ // index valid, can interact with STATE
      if(m_properties.status.data[configOrdinal].state == Drc_n310WorkerTypes::State::STATUS_STATE_OPERATING){ // must stop the config
        if((retVal = stop_config(configOrdinal)) != RCC_OK){
          release_config_logOut << "Could not release config " << configOrdinal << " - error with stop_config method\n";
          log(DRC_LOG_THREE,release_config_logOut.str().c_str());
          return retVal;
        }
      }
      // reset only the daughterboards that are active
      for(auto &dboard : this->dbArray){
        if(dboard.POR){ // some chips on this board need to be reset
          dboard.resetDaughterboard();
          // log(DRC_LOG_THREE,dboard.resetDaughterboard().c_str());
        }
      }
      // for(int dbSel = 0; dbSel < 2; dbSel++){
      release_config_logOut << "Downgraded config " << configOrdinal << " from "
      << stateEnums.at(m_properties.status.data[configOrdinal].state) << " state to inactive state\n";
      log(DRC_LOG_THREE,release_config_logOut.str().c_str());
      return RCC_OK;
      // m_properties.status.data[configOrdinal].state = Drc_n310WorkerTypes::State::STATUS_STATE_INACTIVE;
    } else {
      release_config_logOut << "Failed init_config(): check\n";
      log(DRC_LOG_THREE,release_config_logOut.str().c_str());
      return RCC_ERROR;
    }
  }

  // Below are the official methods / write-sync properties of a DRC Worker to actuate the FSM

  // Initialize status for config if not already present.
  // This config is already error checked


  RCCResult status_config(std::uint32_t configOrdinal) {


    unsigned int conf_length = (sizeof(m_properties.configurations.data)/
      sizeof(m_properties.configurations.data[0]));
    unsigned int status_length = (sizeof(m_properties.status.data)/
      sizeof(m_properties.status.data[0]));

    if((configOrdinal > conf_length) || (configOrdinal > status_length))
    {
      return RCC_ERROR;
    }

    if  ((m_properties.status.data[configOrdinal].state == 
      Drc_n310WorkerTypes::State::STATUS_STATE_INACTIVE) || (m_properties.status.data[configOrdinal].state == 
      Drc_n310WorkerTypes::State::STATUS_STATE_ERROR))
    {
        return RCC_OK; // just use the state_read() for other states as we are not 
                       // guaranteed the chips are setup for reading
      }

      auto &conf = m_properties.configurations.data[configOrdinal];
      auto &status = m_properties.status.data[configOrdinal];


      for(unsigned n = 0; n < conf.channels.size(); ++n)
      {
        auto &conf_channel = conf.channels.data[n];
        auto &status_channel = status.channels.data[n];

        if  ((m_properties.status.data[configOrdinal].state == 
          Drc_n310WorkerTypes::State::STATUS_STATE_INACTIVE) || (m_properties.status.data[configOrdinal].state == 
          Drc_n310WorkerTypes::State::STATUS_STATE_ERROR) || (!validPortNum(conf_channel.rf_port_num)) )
        {
          // For channels that were not programmed, make status -1
          status_channel.tuning_freq_MHz = -1;
          status_channel.bandwidth_3dB_MHz = -1;
          status_channel.sampling_rate_Msps = -1;
          status_channel.gain_dB = -1;
          continue;
        }

      // uses logic from prepare_config to select daughterboard
      bool dbselect = ((conf_channel.rf_port_num & 0x4) >> 2);  // port number relates to daughterboard

      uint8_t rfportnum = conf_channel.rf_port_num;
      
      double adf4351TuningFreq_Hz = 0;

      adf4351TuningFreq_Hz = this->dbArray.at(dbselect).adfController[!(conf_channel.rx)]->get_tuning_freq_Hz();

      double AD9371TuningFreq_MHz = this->dbArray.at(dbselect).transceiverController.get_tuning_freq_MHz(rfportnum);
      double adf4351TuningFreq_MHz = static_cast<double>(adf4351TuningFreq_Hz)/1e6;
      
      // Tuning freq to the floored integer in Mhz
      //double floor_adf4351TuningFreq_MHz = adf4351TuningFreq_MHz;
      
      //status_channel.tuning_freq_MHz = AD9371TuningFreq_MHz;

      // Use Configuration channel tuning freq in status until AD & ADF chips 
      // have been verified 
      double adjusted_tuning_freq = std::abs(adf4351TuningFreq_MHz - AD9371TuningFreq_MHz);

      status_channel.tuning_freq_MHz = adjusted_tuning_freq;

      status_channel.bandwidth_3dB_MHz = this->dbArray.at(dbselect).transceiverController.get_bandwidth_3dB_MHz(rfportnum);
      
      status_channel.sampling_rate_Msps = this->dbArray.at(dbselect).lmkController.get_sampling_rate_Msps();;

      #if 1 // This is the actual dB value
        // status_channel.gain_dB = static_cast<double>(this->dbArray.at(dbselect).dsaController[rfportnum &0x3].get_adjusted_gain());
        status_channel.gain_dB = this->dbArray.at(dbselect).get_gain(rfportnum);
      #endif

      // DSA MAPS the values 
      // 0x0 TX0
      // 0x1 TX1
      // 0x2 RX0
      // 0x3 RX1
      // Instead of 
      // 0x0 RX0
      // 0x1 RX1
      // 0x2 TX0
      // 0X3 TX1

      // Assume Rx0 (Port 0), Tx0 (Port 2)
      // Assume Rx1 (Port 1), Tx1 (Port 3)
      // This means Port 0 and Port 2 would need to be used in a pair
      // This means Port 1 and Port 3 would need to be used in a pair
      // In order for reverse mapping between Tx and Rx to work for gain purposes


    }

    return RCC_OK;
  };

  bool validPortNum(std::int32_t _portNum){
    // check that port is valid between max channels
    std::ostringstream logstringstream;
    logstringstream << "validPortNum()" << "\n";
    switch(_portNum)
    {
      #if (OCPI_PARAM_drc_n310_dba_tx0_en() == 1)
      case 0:
      #endif
      #if (OCPI_PARAM_drc_n310_dba_tx1_en() == 1)
      case 1:
      #endif
      #if (OCPI_PARAM_drc_n310_dba_rx0_en() == 1)
      case 2:
      #endif
      #if (OCPI_PARAM_drc_n310_dba_rx1_en() == 1)
      case 3:
      #endif
      #if (OCPI_PARAM_drc_n310_dbb_tx0_en() == 1)
      case 4:
      #endif
      #if (OCPI_PARAM_drc_n310_dbb_tx1_en() == 1)
      case 5:
      #endif
      #if (OCPI_PARAM_drc_n310_dbb_rx0_en() == 1)
      case 6:
      #endif
      #if (OCPI_PARAM_drc_n310_dbb_rx1_en() == 1)
      case 7:
      #endif
      #if (OCPI_PARAM_drc_n310_dba_tx0_en() || OCPI_PARAM_drc_n310_dba_tx1_en() || OCPI_PARAM_drc_n310_dba_rx0_en() || OCPI_PARAM_drc_n310_dba_rx1_en() || OCPI_PARAM_drc_n310_dbb_tx0_en() || OCPI_PARAM_drc_n310_dbb_tx1_en() || OCPI_PARAM_drc_n310_dbb_rx0_en() || OCPI_PARAM_drc_n310_dbb_rx1_en())
      logstringstream << "port number " << _portNum << " is being tracked\n";
      log(DRC_LOG_EIGHT, logstringstream.str().c_str());
      return true;
      #endif
      default:
        // don't think logging is necessary

      logstringstream << "port number " << _portNum << " not being tracked\n";
      log(DRC_LOG_EIGHT, logstringstream.str().c_str());
      return false;
    }
    // return ((_portNum >= 0) && (_portNum < DRC_N310_MAX_CHANNELS_P)) ? true : false;
  }
public:  
  ~Drc_n310Worker() {
    // release config on each configuration
    size_t nConfigs = m_properties.status.length;
    for(size_t n = 0; n < nConfigs; n++){ // OCPI_DRC_MAX_CONFIGURATIONS ?
      release_config(n); // can't check for RCC_OK now, just destructing
    }
  }
  Drc_n310Worker() 
  :
      drcMiddleware({ // systemChipMap
        {0x1}, // systemChip :: c_LMK04828
        {}, // systemChip :: c_AD9371 -- empty?
        {0x5,0x0}, // systemChip :: c_CPLD
        {0x0,0x1,0x2,0x3}, // systemChip :: c_CPLD_GPIO
        {0x0,0x1,0x2,0x3}, // systemChip :: c_DSA
        {0x4,0x3}, // systemChip :: c_ADF4351
      }),
      dbArray{{
        {this,"DBA",OCPI_PARAM_drc_n310_dba_en()},
        {this,"DBB",OCPI_PARAM_drc_n310_dbb_en()}
      }}
        {
          //std::cout << "finished initializer list\n";
          
      #if IS_ENABLED_I2C
          mI2C_N310.clk_pwr_25mhz();
      #endif

          //std::cout << "End of daughterboard constructor\n";
          Drc_n310Worker::worker_instance = this;
          signal(SIGINT, terminateGracefully);
        }
        static Drc_n310Worker * worker_instance;
      private:
        std::array<daughterboard<Drc_n310Worker>,2> dbArray;
      };


      const std::array<std::string,4> Drc_n310Worker::stateEnums = {"inactive","prepared","operating","error"};

      Drc_n310Worker* Drc_n310Worker::worker_instance = nullptr;
      namespace {
        void terminateGracefully(std::int32_t signum){
          std::cout << "Interrupt signal (" << signum << ") received.\n";
          Drc_n310Worker::worker_instance->~Drc_n310Worker(); // deconstruct
          exit(signum);
        }
      }

      DRC_N310_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in any case
      DRC_N310_END_INFO
