/*
 *Author: Tellrell White 
 *
 *Name of file: dsa-dev.hpp
 *
 *Date : May 7th, 2021
 *  

*/

#ifndef __DSA_H__
#define __DSA_H__

#include <functional>
#include <cmath>
#include <bitset>


// Class used for implementing function to map float attenuation values to hex values for DSA worker 


// This DSA refers to Chip(s) PE43704
class N310_DSA{

private:
    double m_default_max_attenuation; // 31.5
    unsigned char m_converted_atten_hex;
    double m_original_double_atten;

    double m_lower_boundary_input_attenuation; // 0.25
    double m_higher_boundary_input_attenuation; // 31.25

    unsigned char m_atten_hex_lowest_possible; // 0x00
    unsigned char m_atten_hex_maximum_possible; // 0x3F


public:

    /*----------------DSA Function pointer----------------------*/
    
    using write_dsa = std::function<void(unsigned char)>;//type alias to std function
    


/*-------------------------Constructor---------------------------------------*/

    N310_DSA(write_dsa write_dsa_fn);

    /*-------------------API------------------------------------------------*/


    // The typical input is (31.5 Max Attenuation DB - Gain) to this function
    // so an input of 31.5 - 0 GAIN = 31.5, would be least gain; hence max attenuation
    // so an input of 31.5 - 31.5 GAIN = 0, would be maximum gain; hence 0 attenuation

    // thus the value here is attenuation, but often gain is used with the calculation above
    // as input to this function.
    
    //ATTENUATION VALUE in DB to CHIP HEX VALUE
    void double_to_hex(double input_attenuation_db);

    // GET CHIP HEX VALUE
    unsigned char get_converted_atten_hex();
    
    // GET ATTENUATION VALUE
    double get_original_double_atten();


    // GET GAIN FROM ORIGINAL INPUT ATTENUATION VALUE
    double get_original_gain();

    // CONVERT CHIP HEX VALUE TO AN ATTENUATION VALUE
    double hex_to_attenuation_value(unsigned char hexvalue);

    // GET ADJUSTED GAIN FROM CHIP HEX VALUE
    double get_adjusted_gain();


    // By default the max attenuation is 31.5
    double get_default_maximum_attenuation();
    // By default the maximum attenuation is 31.5 -- This only exists for override of the default
    void override_default_maximum_attenuation(double the_maximum_attenuation);
    

    using write_fn_t = std::function<void(unsigned char)>;


    //Write functor
    write_fn_t _write_fn;

};


#endif//__DSA_H__
