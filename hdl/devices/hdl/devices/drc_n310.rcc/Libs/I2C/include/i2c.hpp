/**
 * @file i2c.hpp
 * @author Tellrell White with modifications by N310 Team
 * @brief Provides low-level linux implementation of I2C calls
 * @version 0.1
 * @date 2022-01-31
 * 
 * @copyright Copyright (c) 2022
 * 
 */

#ifndef I2C_HPP
#define I2C_HPP

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <fcntl.h>
#include <unistd.h>
#include <linux/types.h>
#include <string>
#include <errno.h>
#include<cstring>


/**
 * @brief i2c_msg (I2C Message) - used for pure i2c transaction, also from /dev interface
 *
 */
struct i2c_msg {

__u16 addr; /* slave address        */

unsigned short flags;

short len;     /* msg length           */

char *buf;     /* pointer to msg data        */

};


#define I2C_M_TEN             0x10 /* we have a ten bit chip address */

#define I2C_M_RD              0x01

#define I2C_M_NOSTART         0x4000

#define I2C_M_REV_DIR_ADDR    0x2000

#define I2C_M_IGNORE_NAK      0x1000

#define I2C_M_NO_RD_ACK       0x0800

/* To determine what functionality is present */



#define I2C_FUNC_I2C       0x00000001

#define I2C_FUNC_10BIT_ADDR      0x00000002

#define I2C_FUNC_PROTOCOL_MANGLING  0x00000004 /* I2C_M_{REV_DIR_ADDR,NOSTART,..} */

#define I2C_FUNC_SMBUS_PEC    0x00000008

#define I2C_FUNC_SMBUS_BLOCK_PROC_CALL 0x00008000 /* SMBus 2.0 */

#define I2C_FUNC_SMBUS_QUICK     0x00010000

#define I2C_FUNC_SMBUS_READ_BYTE 0x00020000

#define I2C_FUNC_SMBUS_WRITE_BYTE   0x00040000

#define I2C_FUNC_SMBUS_READ_BYTE_DATA  0x00080000

#define I2C_FUNC_SMBUS_WRITE_BYTE_DATA 0x00100000

#define I2C_FUNC_SMBUS_READ_WORD_DATA  0x00200000

#define I2C_FUNC_SMBUS_WRITE_WORD_DATA 0x00400000

#define I2C_FUNC_SMBUS_PROC_CALL 0x00800000

#define I2C_FUNC_SMBUS_READ_BLOCK_DATA 0x01000000

#define I2C_FUNC_SMBUS_WRITE_BLOCK_DATA 0x02000000

#define I2C_FUNC_SMBUS_READ_I2C_BLOCK  0x04000000 /* I2C-like block xfer  */

#define I2C_FUNC_SMBUS_WRITE_I2C_BLOCK 0x08000000 /* w/ 1-byte reg. addr. */


#define I2C_FUNC_SMBUS_BYTE (I2C_FUNC_SMBUS_READ_BYTE | I2C_FUNC_SMBUS_WRITE_BYTE)

#define I2C_FUNC_SMBUS_BYTE_DATA (I2C_FUNC_SMBUS_READ_BYTE_DATA |  I2C_FUNC_SMBUS_WRITE_BYTE_DATA)

#define I2C_FUNC_SMBUS_WORD_DATA (I2C_FUNC_SMBUS_READ_WORD_DATA | I2C_FUNC_SMBUS_WRITE_WORD_DATA)

#define I2C_FUNC_SMBUS_BLOCK_DATA (I2C_FUNC_SMBUS_READ_BLOCK_DATA | I2C_FUNC_SMBUS_WRITE_BLOCK_DATA)

#define I2C_FUNC_SMBUS_I2C_BLOCK (I2C_FUNC_SMBUS_READ_I2C_BLOCK | I2C_FUNC_SMBUS_WRITE_I2C_BLOCK)


/* Old name, for compatibility */

#define I2C_FUNC_SMBUS_HWPEC_CALC   I2C_FUNC_SMBUS_PEC



/*

 * Data for SMBus Messages

 */

#define I2C_SMBUS_BLOCK_MAX   32 /* As specified in SMBus standard */

#define I2C_SMBUS_I2C_BLOCK_MAX  32 /* Not specified but we use same structure */

/**
 * @brief i2c_smbus_data is a union of a byte, word, and block.
 *        __u8 byte
 *        __u16 word
 *        __u8 block[I2C_SMBUS_BLOCK_MAX + 2]
 *
 *        block[0] is used for length.
 *
 */
union i2c_smbus_data {

__u8 byte;

__u16 word;

__u8 block[I2C_SMBUS_BLOCK_MAX + 2]; /* block[0] is used for length */

                                            /* and one more for PEC */

};


/* smbus_access read or write markers */

#define I2C_SMBUS_READ  1

#define I2C_SMBUS_WRITE 0

/* SMBus transaction types (size parameter in the above functions)

   Note: these no longer correspond to the (arbitrary) PIIX4 internal codes! */

#define I2C_SMBUS_QUICK             0

#define I2C_SMBUS_BYTE              1

#define I2C_SMBUS_BYTE_DATA         2

#define I2C_SMBUS_WORD_DATA         3

#define I2C_SMBUS_PROC_CALL         4

#define I2C_SMBUS_BLOCK_DATA        5

#define I2C_SMBUS_I2C_BLOCK_BROKEN  6

#define I2C_SMBUS_BLOCK_PROC_CALL   7     /* SMBus 2.0 */

#define I2C_SMBUS_I2C_BLOCK_DATA    8



/* ----- commands for the ioctl like i2c_command call:

 * note that additional calls are defined in the algorithm and hw

 * dependent layers - these can be listed here, or see the

 * corresponding header files.

 */

/* -> bit-adapter specific ioctls   */

#define I2C_RETRIES  0x0701   /* number of times a device address      */

/* should be polled when not            */

                                /* acknowledging         */

#define I2C_TIMEOUT  0x0702   /* set timeout - call with int      */




#define I2C_SLAVE 0x0703   /* Change slave address       */

/* Attn.: Slave address is 7 or 10 bits */

#define I2C_SLAVE_FORCE 0x0706   /* Change slave address       */

/* Attn.: Slave address is 7 or 10 bits */

/* This changes the address, even if it */

/* is already taken!       */

#define I2C_TENBIT   0x0704   /* 0 for 7 bit addrs, != 0 for 10 bit  */



#define I2C_FUNCS 0x0705   /* Get the adapter functionality */

#define I2C_RDWR  0x0707   /* Combined R/W transfer (one stop only)*/

#define I2C_PEC      0x0708   /* != 0 for SMBus PEC                   */



#define I2C_SMBUS 0x0720   /* SMBus-level access */



//IO Expander register values on ZC706

#define  TCA6146_SLAVE_ADDR      0x21

#define TCA6146_INPUT_REG0    0X00

#define TCA6146_OUTPUT_REG0      0X02

#define  TCA6146_CONFIG_REG0     0x06


/*NOTE : The I2C slave address for the SFP/SFP+ module depends on the PHY
 *on the module connected to the SFP/SFP+ connector.
 *Please refer to the datasheet for the module you are using.
 *For more info : Refer AR http://www.xilinx.com/support/answers/63038.html
 */
//SLAVE ADDRESS MAY DEPEND ON PHY
//#define IIC_SLAVE_ADDR            0x56
// These two may change based on the SFP/SFP+ module
#define PROXY_SFP1_SLAVE_ADDR     0x50
#define PROXY_SFP1_SLAVE_ADDR2   0x51

#define PROXY_SFP0_SLAVE_ADDR     0x50
#define PROXY_SFP0_SLAVE_ADDR2    0x51

//Register values for PCA9548(Switch)

#define  PCA9548_SLAVE_ADDR        0x74

// TCA9548 Datasheet page 18 of 39

#define TCA9548_NOCHANNELS_ENABLED 0x00

#define TCA9548_CHAN0_ENABLE     0x01

#define TCA9548_CHAN1_ENABLE     0x02   // SFP-1-I2C

#define TCA9548_CHAN2_ENABLE     0x04   // SFP-0-I2C

#define TCA9548_CHAN3_ENABLE     0X08   // CLK_I2C

#define TCA9548_CHAN4_ENABLE     0X10

#define TCA9548_CHAN6_ENABLE     0X40   // PWR Daughterboard A

#define TCA9548_CHAN7_ENABLE     0X80   // PWR Daughterboard B

//Register values for TCA6408 (IO EXPANDER-DB POWER)

#define  TCA6408A_SLAVE_ADDR     0x20

#define  TCA6408A_INPUT_REG        0x00

#define  TCA6408A_OUTPUT_REG       0x01

#define  TCA6408A_CONFIG_REG       0x03

#define  TCA6408A_CONFIG_VALUE      0x15


//Register values for TCA6424A (IO EXPANDER-CLK-ARCH)

#define  TCA6424A_SLAVE_ADDR         0x22

#define  TCA6424A_INPUT_REG1         0x80

#define TCA6424A_OUTPUT_REG1     0X84

#define TCA6424A_CONFIG_REG1     0X8C

#define  TCA6424A_CONFIG_VALUE      0x6A


// Register 2
#define TCA6424A_INPUT_REG2         0x81

#define TCA6424A_OUTPUT_REG2        0X85

#define TCA6424A_CONFIG_REG2        0X8D

// Register 3
#define TCA6424A_INPUT_REG3         0x82

#define TCA6424A_OUTPUT_REG3        0X86

#define TCA6424A_CONFIG_REG3        0X8E



/* Note: 10-bit addresses are NOT supported! */



/* This is the structure as used in the I2C_SMBUS ioctl call */

/**
 * @brief i2c_smbus_ioctl_data is the structure used in the I2C SMBUS ioctl call
 *
 */
struct i2c_smbus_ioctl_data {

char read_write;

__u8 command;

int size;

union i2c_smbus_data *data;

};



/* This is the structure as used in the I2C_RDWR ioctl call */

/**
 * @brief i2c_rdwr_ioctl_data is the structure used in the I2C_RDWR ioctl call
 */
struct i2c_rdwr_ioctl_data {

struct i2c_msg *msgs;   /* pointers to i2c_msgs */

int nmsgs;     /* number of i2c_msgs */

};


__s32 i2c_smbus_access(int file, char read_write, __u8 command, int size, union i2c_smbus_data *data);


//IO_EXPANDER CLASS USED FOR POWERING DAUGHTERBOARD AND CLOCK SOURCE ARCH ON N310

/**
 * @brief The IO_EXPANDER class is used for powering daughterboard
 *        and clock source architecture on the N310.
 */
class IO_Expander{

public:

    /**
     * @brief channel_select(...) selects channel for communication on switch
     *
     * @param file
     * @param value
     * @return __s32
     */
    __s32 channel_select(int file, __u8 value); //Selects channel for communication on switch

    /**
     * @brief IO_config is the configuration of the port expander
     *
     * @param file
     * @param command
     * @param value
     * @return __s32
     */
    __s32 IO_config(int file, __u8 command, __u8 value);//configuration of port expander

   /**
    * @brief IO_config_PROXYSFP1(...) is the configuration of the port expander (chan 1)
    *
    * @param file
    * @param command
    * @param value
    * @return __s32
    */
       __s32 IO_config_PROXYSFP1(int file, __u8 command, __u8 value);//configuration of port expander (chan 1)

   /**
    * @brief IO_config_PROXYSFP0(...) is the configuration of port expander (chan 2)
    * @param file
    * @param command
    * @param value
    * @return __s32
    */
       __s32 IO_config_PROXYSFP0(int file, __u8 command, __u8 value);//configuration of port expander (chan 2)

   /**
    * @brief IO_config_TCA6408A configures a port expander of the same name
    *
    * @param file
    * @param command
    * @param value
    * @return __s32
    */
       __s32 IO_config_TCA6408A(int file, __u8 command, __u8 value);//configuration of port expander

   /**
    * @brief IO_config_TCA6424A configures a port expander of the same name.
    *
    * @param file
    * @param command
    * @param value
    * @return __s32
    */
       __s32 IO_config_TCA6424A(int file, __u8 command, __u8 value);//configuration of port expander

   /**
    * @brief write_data(...) outputs data to port expander
    *
    * @param file
    * @param command
    * @param value
    * @return __s32
    */
       __s32 write_data(int file, __u8 command, __u8 value);//outputting data to port expander

   /**
    * @brief read_data(...) reads data from the port expander
    *
    * @param file
    * @param command
    * @return __s32
    */
       __s32 read_data(int file, __u8 command);//reading in data from port expander
}; 



__s32 i2c_smbus_access(int file, char read_write, __u8 command, int size, union i2c_smbus_data *data);


#endif