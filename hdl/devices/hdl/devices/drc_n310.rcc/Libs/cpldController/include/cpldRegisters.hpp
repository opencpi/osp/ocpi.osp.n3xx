#ifndef __CPLDREGISTERS_H__
#define __CPLDREGISTERS_H__


#include <set>
// #include <vector>
#include <stdint.h>

class magnesium_cpld_regs_t{
public:
    uint16_t scratch;
    uint8_t cpld_reset;
    enum ch1_idle_tx_sw1_t{
        CH1_IDLE_TX_SW1_SHUTDOWNTXSW1 = 0,
        CH1_IDLE_TX_SW1_FROMTXFILTERLP1700MHZ = 1,
        CH1_IDLE_TX_SW1_FROMTXFILTERLP3400MHZ = 2,
        CH1_IDLE_TX_SW1_FROMTXFILTERLP0800MHZ = 3
    };
    ch1_idle_tx_sw1_t ch1_idle_tx_sw1;
    enum ch1_idle_tx_sw2_t{
        CH1_IDLE_TX_SW2_TOTXFILTERLP3400MHZ = 1,
        CH1_IDLE_TX_SW2_TOTXFILTERLP1700MHZ = 2,
        CH1_IDLE_TX_SW2_TOTXFILTERLP0800MHZ = 4,
        CH1_IDLE_TX_SW2_TOTXFILTERLP6400MHZ = 8
    };
    ch1_idle_tx_sw2_t ch1_idle_tx_sw2;
    enum ch1_idle_tx_sw3_t{
        CH1_IDLE_TX_SW3_TOTXFILTERBANKS = 0,
        CH1_IDLE_TX_SW3_BYPASSPATHTOTRXSW = 1
    };
    ch1_idle_tx_sw3_t ch1_idle_tx_sw3;
    enum ch1_idle_tx_lowband_mixer_path_select_t{
        CH1_IDLE_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS = 0,
        CH1_IDLE_TX_LOWBAND_MIXER_PATH_SELECT_ENABLE = 1
    };
    ch1_idle_tx_lowband_mixer_path_select_t ch1_idle_tx_lowband_mixer_path_select;
    uint8_t ch1_idle_tx_mixer_en;
    uint8_t ch1_idle_tx_amp_en;
    uint8_t ch1_idle_tx_pa_en;
    enum ch1_idle_sw_trx_t{
        CH1_IDLE_SW_TRX_FROMLOWERFILTERBANKTXSW1 = 0,
        CH1_IDLE_SW_TRX_FROMTXUPPERFILTERBANKLP6400MHZ = 1,
        CH1_IDLE_SW_TRX_RXCHANNELPATH = 2,
        CH1_IDLE_SW_TRX_BYPASSPATHTOTXSW3 = 3
    };
    ch1_idle_sw_trx_t ch1_idle_sw_trx;
    uint8_t ch1_idle_tx_led;
    uint8_t ch1_idle_tx_myk_en;
    enum ch1_idle_rx_sw1_t{
        CH1_IDLE_RX_SW1_TXRXINPUT = 0,
        CH1_IDLE_RX_SW1_RXLOCALINPUT = 1,
        CH1_IDLE_RX_SW1_TRXSWITCHOUTPUT = 2,
        CH1_IDLE_RX_SW1_RX2INPUT = 3
    };
    ch1_idle_rx_sw1_t ch1_idle_rx_sw1;
    enum ch1_idle_rx_sw2_t{
        CH1_IDLE_RX_SW2_SHUTDOWNSW2 = 0,
        CH1_IDLE_RX_SW2_LOWERFILTERBANKTOSWITCH3 = 1,
        CH1_IDLE_RX_SW2_BYPASSPATHTOSWITCH6 = 2,
        CH1_IDLE_RX_SW2_UPPERFILTERBANKTOSWITCH4 = 3
    };
    ch1_idle_rx_sw2_t ch1_idle_rx_sw2;
    enum ch1_idle_rx_sw3_t{
        CH1_IDLE_RX_SW3_FILTER2100X2850MHZ = 0,
        CH1_IDLE_RX_SW3_FILTER0490LPMHZ = 1,
        CH1_IDLE_RX_SW3_FILTER1600X2250MHZ = 2,
        CH1_IDLE_RX_SW3_FILTER0440X0530MHZ = 4,
        CH1_IDLE_RX_SW3_FILTER0650X1000MHZ = 5,
        CH1_IDLE_RX_SW3_FILTER1100X1575MHZ = 6,
        CH1_IDLE_RX_SW3_SHUTDOWNSW3 = 7
    };
    ch1_idle_rx_sw3_t ch1_idle_rx_sw3;
    enum ch1_idle_rx_sw4_t{
        CH1_IDLE_RX_SW4_FILTER2100X2850MHZFROM = 1,
        CH1_IDLE_RX_SW4_FILTER1600X2250MHZFROM = 2,
        CH1_IDLE_RX_SW4_FILTER2700HPMHZ = 4
    };
    ch1_idle_rx_sw4_t ch1_idle_rx_sw4;
    enum ch1_idle_rx_sw5_t{
        CH1_IDLE_RX_SW5_FILTER0440X0530MHZFROM = 1,
        CH1_IDLE_RX_SW5_FILTER1100X1575MHZFROM = 2,
        CH1_IDLE_RX_SW5_FILTER0490LPMHZFROM = 4,
        CH1_IDLE_RX_SW5_FILTER0650X1000MHZFROM = 8
    };
    ch1_idle_rx_sw5_t ch1_idle_rx_sw5;
    enum ch1_idle_rx_sw6_t{
        CH1_IDLE_RX_SW6_LOWERFILTERBANKFROMSWITCH5 = 1,
        CH1_IDLE_RX_SW6_UPPERFILTERBANKFROMSWITCH4 = 2,
        CH1_IDLE_RX_SW6_BYPASSPATHFROMSWITCH2 = 4
    };
    ch1_idle_rx_sw6_t ch1_idle_rx_sw6;
    enum ch1_idle_rx_loband_mixer_path_sel_t{
        CH1_IDLE_RX_LOBAND_MIXER_PATH_SEL_BYPASS = 0,
        CH1_IDLE_RX_LOBAND_MIXER_PATH_SEL_LOBAND = 1
    };
    ch1_idle_rx_loband_mixer_path_sel_t ch1_idle_rx_loband_mixer_path_sel;
    uint8_t ch1_idle_rx_mixer_en;
    uint8_t ch1_idle_rx_amp_en;
    uint8_t ch1_idle_rx_lna1_en;
    uint8_t ch1_idle_rx_lna2_en;
    uint8_t ch1_idle_rx2_led;
    uint8_t ch1_idle_rx_led;
    uint8_t ch1_idle_rx_myk_en;
    enum ch1_on_tx_sw1_t{
        CH1_ON_TX_SW1_SHUTDOWNTXSW1 = 0,
        CH1_ON_TX_SW1_FROMTXFILTERLP1700MHZ = 1,
        CH1_ON_TX_SW1_FROMTXFILTERLP3400MHZ = 2,
        CH1_ON_TX_SW1_FROMTXFILTERLP0800MHZ = 3
    };
    ch1_on_tx_sw1_t ch1_on_tx_sw1;
    enum ch1_on_tx_sw2_t{
        CH1_ON_TX_SW2_TOTXFILTERLP3400MHZ = 1,
        CH1_ON_TX_SW2_TOTXFILTERLP1700MHZ = 2,
        CH1_ON_TX_SW2_TOTXFILTERLP0800MHZ = 4,
        CH1_ON_TX_SW2_TOTXFILTERLP6400MHZ = 8
    };
    ch1_on_tx_sw2_t ch1_on_tx_sw2;
    enum ch1_on_tx_sw3_t{
        CH1_ON_TX_SW3_TOTXFILTERBANKS = 0,
        CH1_ON_TX_SW3_BYPASSPATHTOTRXSW = 1
    };
    ch1_on_tx_sw3_t ch1_on_tx_sw3;
    enum ch1_on_tx_lowband_mixer_path_select_t{
        CH1_ON_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS = 0,
        CH1_ON_TX_LOWBAND_MIXER_PATH_SELECT_ENABLE = 1
    };
    ch1_on_tx_lowband_mixer_path_select_t ch1_on_tx_lowband_mixer_path_select;
    uint8_t ch1_on_tx_mixer_en;
    uint8_t ch1_on_tx_amp_en;
    uint8_t ch1_on_tx_pa_en;
    enum ch1_on_sw_trx_t{
        CH1_ON_SW_TRX_FROMLOWERFILTERBANKTXSW1 = 0,
        CH1_ON_SW_TRX_FROMTXUPPERFILTERBANKLP6400MHZ = 1,
        CH1_ON_SW_TRX_RXCHANNELPATH = 2,
        CH1_ON_SW_TRX_BYPASSPATHTOTXSW3 = 3
    };
    ch1_on_sw_trx_t ch1_on_sw_trx;
    uint8_t ch1_on_tx_led;
    uint8_t ch1_on_tx_myk_en;
    enum ch1_on_rx_sw1_t{
        CH1_ON_RX_SW1_TXRXINPUT = 0,
        CH1_ON_RX_SW1_RXLOCALINPUT = 1,
        CH1_ON_RX_SW1_TRXSWITCHOUTPUT = 2,
        CH1_ON_RX_SW1_RX2INPUT = 3
    };
    ch1_on_rx_sw1_t ch1_on_rx_sw1;
    enum ch1_on_rx_sw2_t{
        CH1_ON_RX_SW2_SHUTDOWNSW2 = 0,
        CH1_ON_RX_SW2_LOWERFILTERBANKTOSWITCH3 = 1,
        CH1_ON_RX_SW2_BYPASSPATHTOSWITCH6 = 2,
        CH1_ON_RX_SW2_UPPERFILTERBANKTOSWITCH4 = 3
    };
    ch1_on_rx_sw2_t ch1_on_rx_sw2;
    enum ch1_on_rx_sw3_t{
        CH1_ON_RX_SW3_FILTER2100X2850MHZ = 0,
        CH1_ON_RX_SW3_FILTER0490LPMHZ = 1,
        CH1_ON_RX_SW3_FILTER1600X2250MHZ = 2,
        CH1_ON_RX_SW3_FILTER0440X0530MHZ = 4,
        CH1_ON_RX_SW3_FILTER0650X1000MHZ = 5,
        CH1_ON_RX_SW3_FILTER1100X1575MHZ = 6,
        CH1_ON_RX_SW3_SHUTDOWNSW3 = 7
    };
    ch1_on_rx_sw3_t ch1_on_rx_sw3;
    enum ch1_on_rx_sw4_t{
        CH1_ON_RX_SW4_FILTER2100X2850MHZFROM = 1,
        CH1_ON_RX_SW4_FILTER1600X2250MHZFROM = 2,
        CH1_ON_RX_SW4_FILTER2700HPMHZ = 4
    };
    ch1_on_rx_sw4_t ch1_on_rx_sw4;
    enum ch1_on_rx_sw5_t{
        CH1_ON_RX_SW5_FILTER0440X0530MHZFROM = 1,
        CH1_ON_RX_SW5_FILTER1100X1575MHZFROM = 2,
        CH1_ON_RX_SW5_FILTER0490LPMHZFROM = 4,
        CH1_ON_RX_SW5_FILTER0650X1000MHZFROM = 8
    };
    ch1_on_rx_sw5_t ch1_on_rx_sw5;
    enum ch1_on_rx_sw6_t{
        CH1_ON_RX_SW6_LOWERFILTERBANKFROMSWITCH5 = 1,
        CH1_ON_RX_SW6_UPPERFILTERBANKFROMSWITCH4 = 2,
        CH1_ON_RX_SW6_BYPASSPATHFROMSWITCH2 = 4
    };
    ch1_on_rx_sw6_t ch1_on_rx_sw6;
    enum ch1_on_rx_loband_mixer_path_sel_t{
        CH1_ON_RX_LOBAND_MIXER_PATH_SEL_BYPASS = 0,
        CH1_ON_RX_LOBAND_MIXER_PATH_SEL_LOBAND = 1
    };
    ch1_on_rx_loband_mixer_path_sel_t ch1_on_rx_loband_mixer_path_sel;
    uint8_t ch1_on_rx_mixer_en;
    uint8_t ch1_on_rx_amp_en;
    uint8_t ch1_on_rx_lna1_en;
    uint8_t ch1_on_rx_lna2_en;
    uint8_t ch1_on_rx2_led;
    uint8_t ch1_on_rx_led;
    uint8_t ch1_on_rx_myk_en;
    enum ch2_idle_tx_sw1_t{
        CH2_IDLE_TX_SW1_SHUTDOWNTXSW1 = 0,
        CH2_IDLE_TX_SW1_FROMTXFILTERLP1700MHZ = 1,
        CH2_IDLE_TX_SW1_FROMTXFILTERLP3400MHZ = 2,
        CH2_IDLE_TX_SW1_FROMTXFILTERLP0800MHZ = 3
    };
    ch2_idle_tx_sw1_t ch2_idle_tx_sw1;
    enum ch2_idle_tx_sw2_t{
        CH2_IDLE_TX_SW2_TOTXFILTERLP3400MHZ = 1,
        CH2_IDLE_TX_SW2_TOTXFILTERLP1700MHZ = 2,
        CH2_IDLE_TX_SW2_TOTXFILTERLP0800MHZ = 4,
        CH2_IDLE_TX_SW2_TOTXFILTERLP6400MHZ = 8
    };
    ch2_idle_tx_sw2_t ch2_idle_tx_sw2;
    enum ch2_idle_tx_sw3_t{
        CH2_IDLE_TX_SW3_TOTXFILTERBANKS = 0,
        CH2_IDLE_TX_SW3_BYPASSPATHTOTRXSW = 1
    };
    ch2_idle_tx_sw3_t ch2_idle_tx_sw3;
    enum ch2_idle_tx_lowband_mixer_path_select_t{
        CH2_IDLE_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS = 0,
        CH2_IDLE_TX_LOWBAND_MIXER_PATH_SELECT_ENABLE = 1
    };
    ch2_idle_tx_lowband_mixer_path_select_t ch2_idle_tx_lowband_mixer_path_select;
    uint8_t ch2_idle_tx_mixer_en;
    uint8_t ch2_idle_tx_amp_en;
    uint8_t ch2_idle_tx_pa_en;
    enum ch2_idle_sw_trx_t{
        CH2_IDLE_SW_TRX_FROMLOWERFILTERBANKTXSW1 = 0,
        CH2_IDLE_SW_TRX_FROMTXUPPERFILTERBANKLP6400MHZ = 1,
        CH2_IDLE_SW_TRX_RXCHANNELPATH = 2,
        CH2_IDLE_SW_TRX_BYPASSPATHTOTXSW3 = 3
    };
    ch2_idle_sw_trx_t ch2_idle_sw_trx;
    uint8_t ch2_idle_tx_led;
    uint8_t ch2_idle_tx_myk_en;
    enum ch2_idle_rx_sw1_t{
        CH2_IDLE_RX_SW1_TXRXINPUT = 0,
        CH2_IDLE_RX_SW1_RXLOCALINPUT = 1,
        CH2_IDLE_RX_SW1_TRXSWITCHOUTPUT = 2,
        CH2_IDLE_RX_SW1_RX2INPUT = 3
    };
    ch2_idle_rx_sw1_t ch2_idle_rx_sw1;
    enum ch2_idle_rx_sw2_t{
        CH2_IDLE_RX_SW2_SHUTDOWNSW2 = 0,
        CH2_IDLE_RX_SW2_LOWERFILTERBANKTOSWITCH3 = 1,
        CH2_IDLE_RX_SW2_BYPASSPATHTOSWITCH6 = 2,
        CH2_IDLE_RX_SW2_UPPERFILTERBANKTOSWITCH4 = 3
    };
    ch2_idle_rx_sw2_t ch2_idle_rx_sw2;
    enum ch2_idle_rx_sw3_t{
        CH2_IDLE_RX_SW3_FILTER2100X2850MHZ = 0,
        CH2_IDLE_RX_SW3_FILTER0490LPMHZ = 1,
        CH2_IDLE_RX_SW3_FILTER1600X2250MHZ = 2,
        CH2_IDLE_RX_SW3_FILTER0440X0530MHZ = 4,
        CH2_IDLE_RX_SW3_FILTER0650X1000MHZ = 5,
        CH2_IDLE_RX_SW3_FILTER1100X1575MHZ = 6,
        CH2_IDLE_RX_SW3_SHUTDOWNSW3 = 7
    };
    ch2_idle_rx_sw3_t ch2_idle_rx_sw3;
    enum ch2_idle_rx_sw4_t{
        CH2_IDLE_RX_SW4_FILTER2100X2850MHZFROM = 1,
        CH2_IDLE_RX_SW4_FILTER1600X2250MHZFROM = 2,
        CH2_IDLE_RX_SW4_FILTER2700HPMHZ = 4
    };
    ch2_idle_rx_sw4_t ch2_idle_rx_sw4;
    enum ch2_idle_rx_sw5_t{
        CH2_IDLE_RX_SW5_FILTER0440X0530MHZFROM = 1,
        CH2_IDLE_RX_SW5_FILTER1100X1575MHZFROM = 2,
        CH2_IDLE_RX_SW5_FILTER0490LPMHZFROM = 4,
        CH2_IDLE_RX_SW5_FILTER0650X1000MHZFROM = 8
    };
    ch2_idle_rx_sw5_t ch2_idle_rx_sw5;
    enum ch2_idle_rx_sw6_t{
        CH2_IDLE_RX_SW6_LOWERFILTERBANKFROMSWITCH5 = 1,
        CH2_IDLE_RX_SW6_UPPERFILTERBANKFROMSWITCH4 = 2,
        CH2_IDLE_RX_SW6_BYPASSPATHFROMSWITCH2 = 4
    };
    ch2_idle_rx_sw6_t ch2_idle_rx_sw6;
    enum ch2_idle_rx_loband_mixer_path_sel_t{
        CH2_IDLE_RX_LOBAND_MIXER_PATH_SEL_BYPASS = 0,
        CH2_IDLE_RX_LOBAND_MIXER_PATH_SEL_LOBAND = 1
    };
    ch2_idle_rx_loband_mixer_path_sel_t ch2_idle_rx_loband_mixer_path_sel;
    uint8_t ch2_idle_rx_mixer_en;
    uint8_t ch2_idle_rx_amp_en;
    uint8_t ch2_idle_rx_lna1_en;
    uint8_t ch2_idle_rx_lna2_en;
    uint8_t ch2_idle_rx2_led;
    uint8_t ch2_idle_rx_led;
    uint8_t ch2_idle_rx_myk_en;
    enum ch2_on_tx_sw1_t{
        CH2_ON_TX_SW1_SHUTDOWNTXSW1 = 0,
        CH2_ON_TX_SW1_FROMTXFILTERLP1700MHZ = 1,
        CH2_ON_TX_SW1_FROMTXFILTERLP3400MHZ = 2,
        CH2_ON_TX_SW1_FROMTXFILTERLP0800MHZ = 3
    };
    ch2_on_tx_sw1_t ch2_on_tx_sw1;
    enum ch2_on_tx_sw2_t{
        CH2_ON_TX_SW2_TOTXFILTERLP3400MHZ = 1,
        CH2_ON_TX_SW2_TOTXFILTERLP1700MHZ = 2,
        CH2_ON_TX_SW2_TOTXFILTERLP0800MHZ = 4,
        CH2_ON_TX_SW2_TOTXFILTERLP6400MHZ = 8
    };
    ch2_on_tx_sw2_t ch2_on_tx_sw2;
    enum ch2_on_tx_sw3_t{
        CH2_ON_TX_SW3_TOTXFILTERBANKS = 0,
        CH2_ON_TX_SW3_BYPASSPATHTOTRXSW = 1
    };
    ch2_on_tx_sw3_t ch2_on_tx_sw3;
    enum ch2_on_tx_lowband_mixer_path_select_t{
        CH2_ON_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS = 0,
        CH2_ON_TX_LOWBAND_MIXER_PATH_SELECT_ENABLE = 1
    };
    ch2_on_tx_lowband_mixer_path_select_t ch2_on_tx_lowband_mixer_path_select;
    uint8_t ch2_on_tx_mixer_en;
    uint8_t ch2_on_tx_amp_en;
    uint8_t ch2_on_tx_pa_en;
    enum ch2_on_sw_trx_t{
        CH2_ON_SW_TRX_FROMLOWERFILTERBANKTXSW1 = 0,
        CH2_ON_SW_TRX_FROMTXUPPERFILTERBANKLP6400MHZ = 1,
        CH2_ON_SW_TRX_RXCHANNELPATH = 2,
        CH2_ON_SW_TRX_BYPASSPATHTOTXSW3 = 3
    };
    ch2_on_sw_trx_t ch2_on_sw_trx;
    uint8_t ch2_on_tx_led;
    uint8_t ch2_on_tx_myk_en;
    enum ch2_on_rx_sw1_t{
        CH2_ON_RX_SW1_TXRXINPUT = 0,
        CH2_ON_RX_SW1_RXLOCALINPUT = 1,
        CH2_ON_RX_SW1_TRXSWITCHOUTPUT = 2,
        CH2_ON_RX_SW1_RX2INPUT = 3
    };
    ch2_on_rx_sw1_t ch2_on_rx_sw1;
    enum ch2_on_rx_sw2_t{
        CH2_ON_RX_SW2_SHUTDOWNSW2 = 0,
        CH2_ON_RX_SW2_LOWERFILTERBANKTOSWITCH3 = 1,
        CH2_ON_RX_SW2_BYPASSPATHTOSWITCH6 = 2,
        CH2_ON_RX_SW2_UPPERFILTERBANKTOSWITCH4 = 3
    };
    ch2_on_rx_sw2_t ch2_on_rx_sw2;
    enum ch2_on_rx_sw3_t{
        CH2_ON_RX_SW3_FILTER2100X2850MHZ = 0,
        CH2_ON_RX_SW3_FILTER0490LPMHZ = 1,
        CH2_ON_RX_SW3_FILTER1600X2250MHZ = 2,
        CH2_ON_RX_SW3_FILTER0440X0530MHZ = 4,
        CH2_ON_RX_SW3_FILTER0650X1000MHZ = 5,
        CH2_ON_RX_SW3_FILTER1100X1575MHZ = 6,
        CH2_ON_RX_SW3_SHUTDOWNSW3 = 7
    };
    ch2_on_rx_sw3_t ch2_on_rx_sw3;
    enum ch2_on_rx_sw4_t{
        CH2_ON_RX_SW4_FILTER2100X2850MHZFROM = 1,
        CH2_ON_RX_SW4_FILTER1600X2250MHZFROM = 2,
        CH2_ON_RX_SW4_FILTER2700HPMHZ = 4
    };
    ch2_on_rx_sw4_t ch2_on_rx_sw4;
    enum ch2_on_rx_sw5_t{
        CH2_ON_RX_SW5_FILTER0440X0530MHZFROM = 1,
        CH2_ON_RX_SW5_FILTER1100X1575MHZFROM = 2,
        CH2_ON_RX_SW5_FILTER0490LPMHZFROM = 4,
        CH2_ON_RX_SW5_FILTER0650X1000MHZFROM = 8
    };
    ch2_on_rx_sw5_t ch2_on_rx_sw5;
    enum ch2_on_rx_sw6_t{
        CH2_ON_RX_SW6_LOWERFILTERBANKFROMSWITCH5 = 1,
        CH2_ON_RX_SW6_UPPERFILTERBANKFROMSWITCH4 = 2,
        CH2_ON_RX_SW6_BYPASSPATHFROMSWITCH2 = 4
    };
    ch2_on_rx_sw6_t ch2_on_rx_sw6;
    enum ch2_on_rx_loband_mixer_path_sel_t{
        CH2_ON_RX_LOBAND_MIXER_PATH_SEL_BYPASS = 0,
        CH2_ON_RX_LOBAND_MIXER_PATH_SEL_LOBAND = 1
    };
    ch2_on_rx_loband_mixer_path_sel_t ch2_on_rx_loband_mixer_path_sel;
    uint8_t ch2_on_rx_mixer_en;
    uint8_t ch2_on_rx_amp_en;
    uint8_t ch2_on_rx_lna1_en;
    uint8_t ch2_on_rx_lna2_en;
    uint8_t ch2_on_rx2_led;
    uint8_t ch2_on_rx_led;
    uint8_t ch2_on_rx_myk_en;

    // PS Added bitfields
    uint16_t ProductSignature;
    uint16_t CpldMinorRevision;
    uint16_t CpldMajorRevision;
    uint8_t BuildCodeHH;
    uint8_t BuildCodeDD;
    uint8_t BuildCodeMM;
    uint8_t BuildCodeYY;
    uint16_t PSScratch;
    uint8_t PSCpldReset;
    uint8_t VcxoControl;
    uint8_t RxLoLockDetect;
    uint8_t TxLoLockDetect;
    uint8_t MykonosReset;


    magnesium_cpld_regs_t(void){
        _state = nullptr;
        scratch = 0;
        cpld_reset = 0;
        ch1_idle_tx_sw1 = CH1_IDLE_TX_SW1_SHUTDOWNTXSW1;
        ch1_idle_tx_sw2 = CH1_IDLE_TX_SW2_TOTXFILTERLP3400MHZ;
        ch1_idle_tx_sw3 = CH1_IDLE_TX_SW3_TOTXFILTERBANKS;
        ch1_idle_tx_lowband_mixer_path_select = CH1_IDLE_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS;
        ch1_idle_tx_mixer_en = 0;
        ch1_idle_tx_amp_en = 1;
        ch1_idle_tx_pa_en = 1;
        ch1_idle_sw_trx = CH1_IDLE_SW_TRX_FROMLOWERFILTERBANKTXSW1;
        ch1_idle_tx_led = 0;
        ch1_idle_tx_myk_en = 0;
        ch1_idle_rx_sw1 = CH1_IDLE_RX_SW1_RX2INPUT;
        ch1_idle_rx_sw2 = CH1_IDLE_RX_SW2_SHUTDOWNSW2;
        ch1_idle_rx_sw3 = CH1_IDLE_RX_SW3_FILTER2100X2850MHZ;
        ch1_idle_rx_sw4 = CH1_IDLE_RX_SW4_FILTER2100X2850MHZFROM;
        ch1_idle_rx_sw5 = CH1_IDLE_RX_SW5_FILTER0440X0530MHZFROM;
        ch1_idle_rx_sw6 = CH1_IDLE_RX_SW6_LOWERFILTERBANKFROMSWITCH5;
        ch1_idle_rx_loband_mixer_path_sel = CH1_IDLE_RX_LOBAND_MIXER_PATH_SEL_BYPASS;
        ch1_idle_rx_mixer_en = 0;
        ch1_idle_rx_amp_en = 0;
        ch1_idle_rx_lna1_en = 0;
        ch1_idle_rx_lna2_en = 0;
        ch1_idle_rx2_led = 0;
        ch1_idle_rx_led = 0;
        ch1_idle_rx_myk_en = 0;
        ch1_on_tx_sw1 = CH1_ON_TX_SW1_SHUTDOWNTXSW1;
        ch1_on_tx_sw2 = CH1_ON_TX_SW2_TOTXFILTERLP3400MHZ;
        ch1_on_tx_sw3 = CH1_ON_TX_SW3_TOTXFILTERBANKS;
        ch1_on_tx_lowband_mixer_path_select = CH1_ON_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS;
        ch1_on_tx_mixer_en = 0;
        ch1_on_tx_amp_en = 1;
        ch1_on_tx_pa_en = 1;
        ch1_on_sw_trx = CH1_ON_SW_TRX_FROMLOWERFILTERBANKTXSW1;
        ch1_on_tx_led = 0;
        ch1_on_tx_myk_en = 0;
        ch1_on_rx_sw1 = CH1_ON_RX_SW1_RX2INPUT;
        ch1_on_rx_sw2 = CH1_ON_RX_SW2_LOWERFILTERBANKTOSWITCH3;
        ch1_on_rx_sw3 = CH1_ON_RX_SW3_FILTER2100X2850MHZ;
        ch1_on_rx_sw4 = CH1_ON_RX_SW4_FILTER2100X2850MHZFROM;
        ch1_on_rx_sw5 = CH1_ON_RX_SW5_FILTER0490LPMHZFROM;
        ch1_on_rx_sw6 = CH1_ON_RX_SW6_UPPERFILTERBANKFROMSWITCH4;
        ch1_on_rx_loband_mixer_path_sel = CH1_ON_RX_LOBAND_MIXER_PATH_SEL_BYPASS;
        ch1_on_rx_mixer_en = 0;
        ch1_on_rx_amp_en = 1;
        ch1_on_rx_lna1_en = 1;
        ch1_on_rx_lna2_en = 1;
        ch1_on_rx2_led = 0;
        ch1_on_rx_led = 0;
        ch1_on_rx_myk_en = 1;
        ch2_idle_tx_sw1 = CH2_IDLE_TX_SW1_SHUTDOWNTXSW1;
        ch2_idle_tx_sw2 = CH2_IDLE_TX_SW2_TOTXFILTERLP3400MHZ;
        ch2_idle_tx_sw3 = CH2_IDLE_TX_SW3_TOTXFILTERBANKS;
        ch2_idle_tx_lowband_mixer_path_select = CH2_IDLE_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS;
        ch2_idle_tx_mixer_en = 0;
        ch2_idle_tx_amp_en = 1;
        ch2_idle_tx_pa_en = 1;
        ch2_idle_sw_trx = CH2_IDLE_SW_TRX_FROMLOWERFILTERBANKTXSW1;
        ch2_idle_tx_led = 0;
        ch2_idle_tx_myk_en = 0;
        ch2_idle_rx_sw1 = CH2_IDLE_RX_SW1_RX2INPUT;
        ch2_idle_rx_sw2 = CH2_IDLE_RX_SW2_SHUTDOWNSW2;
        ch2_idle_rx_sw3 = CH2_IDLE_RX_SW3_FILTER2100X2850MHZ;
        ch2_idle_rx_sw4 = CH2_IDLE_RX_SW4_FILTER2100X2850MHZFROM;
        ch2_idle_rx_sw5 = CH2_IDLE_RX_SW5_FILTER0440X0530MHZFROM;
        ch2_idle_rx_sw6 = CH2_IDLE_RX_SW6_LOWERFILTERBANKFROMSWITCH5;
        ch2_idle_rx_loband_mixer_path_sel = CH2_IDLE_RX_LOBAND_MIXER_PATH_SEL_BYPASS;
        ch2_idle_rx_mixer_en = 0;
        ch2_idle_rx_amp_en = 0;
        ch2_idle_rx_lna1_en = 0;
        ch2_idle_rx_lna2_en = 0;
        ch2_idle_rx2_led = 0;
        ch2_idle_rx_led = 0;
        ch2_idle_rx_myk_en = 0;
        ch2_on_tx_sw1 = CH2_ON_TX_SW1_SHUTDOWNTXSW1;
        ch2_on_tx_sw2 = CH2_ON_TX_SW2_TOTXFILTERLP3400MHZ;
        ch2_on_tx_sw3 = CH2_ON_TX_SW3_TOTXFILTERBANKS;
        ch2_on_tx_lowband_mixer_path_select = CH2_ON_TX_LOWBAND_MIXER_PATH_SELECT_BYPASS;
        ch2_on_tx_mixer_en = 0;
        ch2_on_tx_amp_en = 1;
        ch2_on_tx_pa_en = 1;
        ch2_on_sw_trx = CH2_ON_SW_TRX_FROMLOWERFILTERBANKTXSW1;
        ch2_on_tx_led = 0;
        ch2_on_tx_myk_en = 0;
        ch2_on_rx_sw1 = CH2_ON_RX_SW1_RX2INPUT;
        ch2_on_rx_sw2 = CH2_ON_RX_SW2_LOWERFILTERBANKTOSWITCH3;
        ch2_on_rx_sw3 = CH2_ON_RX_SW3_FILTER2100X2850MHZ;
        ch2_on_rx_sw4 = CH2_ON_RX_SW4_FILTER2100X2850MHZFROM;
        ch2_on_rx_sw5 = CH2_ON_RX_SW5_FILTER0490LPMHZFROM;
        ch2_on_rx_sw6 = CH2_ON_RX_SW6_UPPERFILTERBANKFROMSWITCH4;
        ch2_on_rx_loband_mixer_path_sel = CH2_ON_RX_LOBAND_MIXER_PATH_SEL_BYPASS;
        ch2_on_rx_mixer_en = 0;
        ch2_on_rx_amp_en = 1;
        ch2_on_rx_lna1_en = 1;
        ch2_on_rx_lna2_en = 1;
        ch2_on_rx2_led = 0;
        ch2_on_rx_led = 0;
        ch2_on_rx_myk_en = 1;
        // changes reflect hardware
        ProductSignature = 0xCAFE;
        CpldMinorRevision = 0;
        CpldMajorRevision = 5;
        BuildCodeHH = 0x08;
        BuildCodeDD = 0x04;
        BuildCodeMM = 0x01;
        BuildCodeYY = 0x18;
        PSScratch = 0;
        PSCpldReset = 0;
        VcxoControl = 0;
        RxLoLockDetect = 0;
        TxLoLockDetect = 0;
        MykonosReset = 0;
    }

    ~magnesium_cpld_regs_t(void){
        delete _state;
    }

    uint32_t get_reg(uint8_t addr){
        uint32_t reg = 0;
        switch(addr){
        case 0x0:
            // SignatureReg = full two bytes
            reg |= (uint32_t(ProductSignature) & 0xffff) << 0;
            break;
        case 0x1:
            // MinorRevReg = full two bytes
            reg |= (uint32_t(CpldMinorRevision) & 0xffff) << 0;
            break;
        case 0x2:
            // MajorRevReg = full two bytes
            reg |= (uint32_t(CpldMajorRevision) & 0xffff) << 0;
            break;
        case 0x3:
            // BuildCodeLSB: Build code... right now it's the date it was built. LSB in this register
                // BuildCodeHH = LSB, hour code.
                // BuildCodeDD = MSB, day code
            reg |= (uint32_t(BuildCodeHH) & 0xff) << 0;
            reg |= (uint32_t(BuildCodeDD) & 0xff) << 8;
            break;
        case 0x4:
            // BuildCodeMSB
                // BuildCodeMM = LSB, revision month code
                // BuildCodeYY = MSB, revision year code
            reg |= (uint32_t(BuildCodeMM) & 0xff) << 0;
            reg |= (uint32_t(BuildCodeYY) & 0xff) << 8;
            break;
        case 0x5:
            // PS Scratch, full two bytes
            reg |= (uint32_t(PSScratch) & 0xffff) << 0;
            break;
        case 0x10:
            // CpldControl
                // PSCpldReset = lsbit
                // Asserting this bit resets all the CPLD logic.
                // This reset will return all registers on the PS SPI interface to their default
                // state! To use this reset correctly, first write PSCpldReset to '1', then write
                // it to '0'. Registers will be reset on the _falling_ edge of PSCpldReset.
            reg |= (uint32_t(PSCpldReset) & 0x1) << 0;
            break;
        case 0x11:
            // LmkControl
                // VcxoControl = 4th bit (not 0,1,2,3)
                    // Setting this bit to '0' will allow the Phase DAC to exclusively control the
                    // VCXO voltage. Defaults to '1', which allows the Phase DAC to adjust the
                    // voltage (but the LMK still has control as well).
            reg |= (uint32_t(VcxoControl) & 0x1) << 4;
            break;
        case 0x12:
            // LoStatus
                // RxLoLockDetect = lsbit (0), Live lock detect status from the RX LO.
                // TxLoLockDetect = bit 4, Live lock detect status from TX LO
            reg |= (uint32_t(RxLoLockDetect) & 0x1) << 0;
            reg |= (uint32_t(TxLoLockDetect) & 0x1) << 4;
            break;
        case 0x13:
            // MykonosControl
                // MykonosReset = lsbit (0), Drives the Mykonos hard reset line. Defaults to de-asserted. Write a '1' to
                    // assert the reset, and a '0' to de-assert.
            reg |= (uint32_t(MykonosReset) & 0x1) << 0;
            break;
        case 64:
            reg |= (uint32_t(scratch) & 0xffff) << 0;
            break;
        case 65:
            reg |= (uint32_t(cpld_reset) & 0x1) << 0;
            break;
        case 80:
            reg |= (uint32_t(ch1_idle_tx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch1_idle_tx_sw2) & 0xf) << 2;
            reg |= (uint32_t(ch1_idle_tx_sw3) & 0x1) << 6;
            reg |= (uint32_t(ch1_idle_tx_lowband_mixer_path_select) & 0x1) << 7;
            reg |= (uint32_t(ch1_idle_tx_mixer_en) & 0x1) << 8;
            reg |= (uint32_t(ch1_idle_tx_amp_en) & 0x1) << 9;
            reg |= (uint32_t(ch1_idle_tx_pa_en) & 0x1) << 10;
            reg |= (uint32_t(ch1_idle_sw_trx) & 0x3) << 11;
            reg |= (uint32_t(ch1_idle_tx_led) & 0x1) << 13;
            reg |= (uint32_t(ch1_idle_tx_myk_en) & 0x1) << 14;
            break;
        case 81:
            reg |= (uint32_t(ch1_idle_rx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch1_idle_rx_sw2) & 0x3) << 2;
            reg |= (uint32_t(ch1_idle_rx_sw3) & 0x7) << 4;
            reg |= (uint32_t(ch1_idle_rx_sw4) & 0x7) << 7;
            reg |= (uint32_t(ch1_idle_rx_sw5) & 0xf) << 10;
            break;
        case 82:
            reg |= (uint32_t(ch1_idle_rx_sw6) & 0x7) << 0;
            reg |= (uint32_t(ch1_idle_rx_loband_mixer_path_sel) & 0x1) << 3;
            reg |= (uint32_t(ch1_idle_rx_mixer_en) & 0x1) << 4;
            reg |= (uint32_t(ch1_idle_rx_amp_en) & 0x1) << 5;
            reg |= (uint32_t(ch1_idle_rx_lna1_en) & 0x1) << 6;
            reg |= (uint32_t(ch1_idle_rx_lna2_en) & 0x1) << 7;
            reg |= (uint32_t(ch1_idle_rx2_led) & 0x1) << 8;
            reg |= (uint32_t(ch1_idle_rx_led) & 0x1) << 9;
            reg |= (uint32_t(ch1_idle_rx_myk_en) & 0x1) << 10;
            break;
        case 83:
            reg |= (uint32_t(ch1_on_tx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch1_on_tx_sw2) & 0xf) << 2;
            reg |= (uint32_t(ch1_on_tx_sw3) & 0x1) << 6;
            reg |= (uint32_t(ch1_on_tx_lowband_mixer_path_select) & 0x1) << 7;
            reg |= (uint32_t(ch1_on_tx_mixer_en) & 0x1) << 8;
            reg |= (uint32_t(ch1_on_tx_amp_en) & 0x1) << 9;
            reg |= (uint32_t(ch1_on_tx_pa_en) & 0x1) << 10;
            reg |= (uint32_t(ch1_on_sw_trx) & 0x3) << 11;
            reg |= (uint32_t(ch1_on_tx_led) & 0x1) << 13;
            reg |= (uint32_t(ch1_on_tx_myk_en) & 0x1) << 14;
            break;
        case 84:
            reg |= (uint32_t(ch1_on_rx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch1_on_rx_sw2) & 0x3) << 2;
            reg |= (uint32_t(ch1_on_rx_sw3) & 0x7) << 4;
            reg |= (uint32_t(ch1_on_rx_sw4) & 0x7) << 7;
            reg |= (uint32_t(ch1_on_rx_sw5) & 0xf) << 10;
            break;
        case 85:
            reg |= (uint32_t(ch1_on_rx_sw6) & 0x7) << 0;
            reg |= (uint32_t(ch1_on_rx_loband_mixer_path_sel) & 0x1) << 3;
            reg |= (uint32_t(ch1_on_rx_mixer_en) & 0x1) << 4;
            reg |= (uint32_t(ch1_on_rx_amp_en) & 0x1) << 5;
            reg |= (uint32_t(ch1_on_rx_lna1_en) & 0x1) << 6;
            reg |= (uint32_t(ch1_on_rx_lna2_en) & 0x1) << 7;
            reg |= (uint32_t(ch1_on_rx2_led) & 0x1) << 8;
            reg |= (uint32_t(ch1_on_rx_led) & 0x1) << 9;
            reg |= (uint32_t(ch1_on_rx_myk_en) & 0x1) << 10;
            break;
        case 96:
            reg |= (uint32_t(ch2_idle_tx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch2_idle_tx_sw2) & 0xf) << 2;
            reg |= (uint32_t(ch2_idle_tx_sw3) & 0x1) << 6;
            reg |= (uint32_t(ch2_idle_tx_lowband_mixer_path_select) & 0x1) << 7;
            reg |= (uint32_t(ch2_idle_tx_mixer_en) & 0x1) << 8;
            reg |= (uint32_t(ch2_idle_tx_amp_en) & 0x1) << 9;
            reg |= (uint32_t(ch2_idle_tx_pa_en) & 0x1) << 10;
            reg |= (uint32_t(ch2_idle_sw_trx) & 0x3) << 11;
            reg |= (uint32_t(ch2_idle_tx_led) & 0x1) << 13;
            reg |= (uint32_t(ch2_idle_tx_myk_en) & 0x1) << 14;
            break;
        case 97:
            reg |= (uint32_t(ch2_idle_rx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch2_idle_rx_sw2) & 0x3) << 2;
            reg |= (uint32_t(ch2_idle_rx_sw3) & 0x7) << 4;
            reg |= (uint32_t(ch2_idle_rx_sw4) & 0x7) << 7;
            reg |= (uint32_t(ch2_idle_rx_sw5) & 0xf) << 10;
            break;
        case 98:
            reg |= (uint32_t(ch2_idle_rx_sw6) & 0x7) << 0;
            reg |= (uint32_t(ch2_idle_rx_loband_mixer_path_sel) & 0x1) << 3;
            reg |= (uint32_t(ch2_idle_rx_mixer_en) & 0x1) << 4;
            reg |= (uint32_t(ch2_idle_rx_amp_en) & 0x1) << 5;
            reg |= (uint32_t(ch2_idle_rx_lna1_en) & 0x1) << 6;
            reg |= (uint32_t(ch2_idle_rx_lna2_en) & 0x1) << 7;
            reg |= (uint32_t(ch2_idle_rx2_led) & 0x1) << 8;
            reg |= (uint32_t(ch2_idle_rx_led) & 0x1) << 9;
            reg |= (uint32_t(ch2_idle_rx_myk_en) & 0x1) << 10;
            break;
        case 99:
            reg |= (uint32_t(ch2_on_tx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch2_on_tx_sw2) & 0xf) << 2;
            reg |= (uint32_t(ch2_on_tx_sw3) & 0x1) << 6;
            reg |= (uint32_t(ch2_on_tx_lowband_mixer_path_select) & 0x1) << 7;
            reg |= (uint32_t(ch2_on_tx_mixer_en) & 0x1) << 8;
            reg |= (uint32_t(ch2_on_tx_amp_en) & 0x1) << 9;
            reg |= (uint32_t(ch2_on_tx_pa_en) & 0x1) << 10;
            reg |= (uint32_t(ch2_on_sw_trx) & 0x3) << 11;
            reg |= (uint32_t(ch2_on_tx_led) & 0x1) << 13;
            reg |= (uint32_t(ch2_on_tx_myk_en) & 0x1) << 14;
            break;
        case 100:
            reg |= (uint32_t(ch2_on_rx_sw1) & 0x3) << 0;
            reg |= (uint32_t(ch2_on_rx_sw2) & 0x3) << 2;
            reg |= (uint32_t(ch2_on_rx_sw3) & 0x7) << 4;
            reg |= (uint32_t(ch2_on_rx_sw4) & 0x7) << 7;
            reg |= (uint32_t(ch2_on_rx_sw5) & 0xf) << 10;
            break;
        case 101:
            reg |= (uint32_t(ch2_on_rx_sw6) & 0x7) << 0;
            reg |= (uint32_t(ch2_on_rx_loband_mixer_path_sel) & 0x1) << 3;
            reg |= (uint32_t(ch2_on_rx_mixer_en) & 0x1) << 4;
            reg |= (uint32_t(ch2_on_rx_amp_en) & 0x1) << 5;
            reg |= (uint32_t(ch2_on_rx_lna1_en) & 0x1) << 6;
            reg |= (uint32_t(ch2_on_rx_lna2_en) & 0x1) << 7;
            reg |= (uint32_t(ch2_on_rx2_led) & 0x1) << 8;
            reg |= (uint32_t(ch2_on_rx_led) & 0x1) << 9;
            reg |= (uint32_t(ch2_on_rx_myk_en) & 0x1) << 10;
            break;
        }
        return reg;
    }
    
    std::set<std::size_t> get_all_addrs()
    {
        std::set<std::size_t> addrs;
        // PS Added section
        addrs.insert(0);
        addrs.insert(1);
        addrs.insert(2);
        addrs.insert(3);
        addrs.insert(4);
        addrs.insert(5);
        addrs.insert(0x10);
        addrs.insert(0x11);
        addrs.insert(0x12);
        addrs.insert(0x13);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(64);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(65);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(80);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(81);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(81);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(81);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(81);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(81);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(82);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(83);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(84);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(84);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(84);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(84);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(84);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(85);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(96);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(97);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(97);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(97);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(97);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(97);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(98);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(99);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(100);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(100);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(100);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(100);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(100);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        // Hopefully, compilers will optimize out this mess...
        addrs.insert(101);
        return addrs;
    }

    void save_state(void){
        if (_state == nullptr){
            _state = new magnesium_cpld_regs_t();
        } 
        _state->scratch = this->scratch;
        _state->cpld_reset = this->cpld_reset;
        _state->ch1_idle_tx_sw1 = this->ch1_idle_tx_sw1;
        _state->ch1_idle_tx_sw2 = this->ch1_idle_tx_sw2;
        _state->ch1_idle_tx_sw3 = this->ch1_idle_tx_sw3;
        _state->ch1_idle_tx_lowband_mixer_path_select = this->ch1_idle_tx_lowband_mixer_path_select;
        _state->ch1_idle_tx_mixer_en = this->ch1_idle_tx_mixer_en;
        _state->ch1_idle_tx_amp_en = this->ch1_idle_tx_amp_en;
        _state->ch1_idle_tx_pa_en = this->ch1_idle_tx_pa_en;
        _state->ch1_idle_sw_trx = this->ch1_idle_sw_trx;
        _state->ch1_idle_tx_led = this->ch1_idle_tx_led;
        _state->ch1_idle_tx_myk_en = this->ch1_idle_tx_myk_en;
        _state->ch1_idle_rx_sw1 = this->ch1_idle_rx_sw1;
        _state->ch1_idle_rx_sw2 = this->ch1_idle_rx_sw2;
        _state->ch1_idle_rx_sw3 = this->ch1_idle_rx_sw3;
        _state->ch1_idle_rx_sw4 = this->ch1_idle_rx_sw4;
        _state->ch1_idle_rx_sw5 = this->ch1_idle_rx_sw5;
        _state->ch1_idle_rx_sw6 = this->ch1_idle_rx_sw6;
        _state->ch1_idle_rx_loband_mixer_path_sel = this->ch1_idle_rx_loband_mixer_path_sel;
        _state->ch1_idle_rx_mixer_en = this->ch1_idle_rx_mixer_en;
        _state->ch1_idle_rx_amp_en = this->ch1_idle_rx_amp_en;
        _state->ch1_idle_rx_lna1_en = this->ch1_idle_rx_lna1_en;
        _state->ch1_idle_rx_lna2_en = this->ch1_idle_rx_lna2_en;
        _state->ch1_idle_rx2_led = this->ch1_idle_rx2_led;
        _state->ch1_idle_rx_led = this->ch1_idle_rx_led;
        _state->ch1_idle_rx_myk_en = this->ch1_idle_rx_myk_en;
        _state->ch1_on_tx_sw1 = this->ch1_on_tx_sw1;
        _state->ch1_on_tx_sw2 = this->ch1_on_tx_sw2;
        _state->ch1_on_tx_sw3 = this->ch1_on_tx_sw3;
        _state->ch1_on_tx_lowband_mixer_path_select = this->ch1_on_tx_lowband_mixer_path_select;
        _state->ch1_on_tx_mixer_en = this->ch1_on_tx_mixer_en;
        _state->ch1_on_tx_amp_en = this->ch1_on_tx_amp_en;
        _state->ch1_on_tx_pa_en = this->ch1_on_tx_pa_en;
        _state->ch1_on_sw_trx = this->ch1_on_sw_trx;
        _state->ch1_on_tx_led = this->ch1_on_tx_led;
        _state->ch1_on_tx_myk_en = this->ch1_on_tx_myk_en;
        _state->ch1_on_rx_sw1 = this->ch1_on_rx_sw1;
        _state->ch1_on_rx_sw2 = this->ch1_on_rx_sw2;
        _state->ch1_on_rx_sw3 = this->ch1_on_rx_sw3;
        _state->ch1_on_rx_sw4 = this->ch1_on_rx_sw4;
        _state->ch1_on_rx_sw5 = this->ch1_on_rx_sw5;
        _state->ch1_on_rx_sw6 = this->ch1_on_rx_sw6;
        _state->ch1_on_rx_loband_mixer_path_sel = this->ch1_on_rx_loband_mixer_path_sel;
        _state->ch1_on_rx_mixer_en = this->ch1_on_rx_mixer_en;
        _state->ch1_on_rx_amp_en = this->ch1_on_rx_amp_en;
        _state->ch1_on_rx_lna1_en = this->ch1_on_rx_lna1_en;
        _state->ch1_on_rx_lna2_en = this->ch1_on_rx_lna2_en;
        _state->ch1_on_rx2_led = this->ch1_on_rx2_led;
        _state->ch1_on_rx_led = this->ch1_on_rx_led;
        _state->ch1_on_rx_myk_en = this->ch1_on_rx_myk_en;
        _state->ch2_idle_tx_sw1 = this->ch2_idle_tx_sw1;
        _state->ch2_idle_tx_sw2 = this->ch2_idle_tx_sw2;
        _state->ch2_idle_tx_sw3 = this->ch2_idle_tx_sw3;
        _state->ch2_idle_tx_lowband_mixer_path_select = this->ch2_idle_tx_lowband_mixer_path_select;
        _state->ch2_idle_tx_mixer_en = this->ch2_idle_tx_mixer_en;
        _state->ch2_idle_tx_amp_en = this->ch2_idle_tx_amp_en;
        _state->ch2_idle_tx_pa_en = this->ch2_idle_tx_pa_en;
        _state->ch2_idle_sw_trx = this->ch2_idle_sw_trx;
        _state->ch2_idle_tx_led = this->ch2_idle_tx_led;
        _state->ch2_idle_tx_myk_en = this->ch2_idle_tx_myk_en;
        _state->ch2_idle_rx_sw1 = this->ch2_idle_rx_sw1;
        _state->ch2_idle_rx_sw2 = this->ch2_idle_rx_sw2;
        _state->ch2_idle_rx_sw3 = this->ch2_idle_rx_sw3;
        _state->ch2_idle_rx_sw4 = this->ch2_idle_rx_sw4;
        _state->ch2_idle_rx_sw5 = this->ch2_idle_rx_sw5;
        _state->ch2_idle_rx_sw6 = this->ch2_idle_rx_sw6;
        _state->ch2_idle_rx_loband_mixer_path_sel = this->ch2_idle_rx_loband_mixer_path_sel;
        _state->ch2_idle_rx_mixer_en = this->ch2_idle_rx_mixer_en;
        _state->ch2_idle_rx_amp_en = this->ch2_idle_rx_amp_en;
        _state->ch2_idle_rx_lna1_en = this->ch2_idle_rx_lna1_en;
        _state->ch2_idle_rx_lna2_en = this->ch2_idle_rx_lna2_en;
        _state->ch2_idle_rx2_led = this->ch2_idle_rx2_led;
        _state->ch2_idle_rx_led = this->ch2_idle_rx_led;
        _state->ch2_idle_rx_myk_en = this->ch2_idle_rx_myk_en;
        _state->ch2_on_tx_sw1 = this->ch2_on_tx_sw1;
        _state->ch2_on_tx_sw2 = this->ch2_on_tx_sw2;
        _state->ch2_on_tx_sw3 = this->ch2_on_tx_sw3;
        _state->ch2_on_tx_lowband_mixer_path_select = this->ch2_on_tx_lowband_mixer_path_select;
        _state->ch2_on_tx_mixer_en = this->ch2_on_tx_mixer_en;
        _state->ch2_on_tx_amp_en = this->ch2_on_tx_amp_en;
        _state->ch2_on_tx_pa_en = this->ch2_on_tx_pa_en;
        _state->ch2_on_sw_trx = this->ch2_on_sw_trx;
        _state->ch2_on_tx_led = this->ch2_on_tx_led;
        _state->ch2_on_tx_myk_en = this->ch2_on_tx_myk_en;
        _state->ch2_on_rx_sw1 = this->ch2_on_rx_sw1;
        _state->ch2_on_rx_sw2 = this->ch2_on_rx_sw2;
        _state->ch2_on_rx_sw3 = this->ch2_on_rx_sw3;
        _state->ch2_on_rx_sw4 = this->ch2_on_rx_sw4;
        _state->ch2_on_rx_sw5 = this->ch2_on_rx_sw5;
        _state->ch2_on_rx_sw6 = this->ch2_on_rx_sw6;
        _state->ch2_on_rx_loband_mixer_path_sel = this->ch2_on_rx_loband_mixer_path_sel;
        _state->ch2_on_rx_mixer_en = this->ch2_on_rx_mixer_en;
        _state->ch2_on_rx_amp_en = this->ch2_on_rx_amp_en;
        _state->ch2_on_rx_lna1_en = this->ch2_on_rx_lna1_en;
        _state->ch2_on_rx_lna2_en = this->ch2_on_rx_lna2_en;
        _state->ch2_on_rx2_led = this->ch2_on_rx2_led;
        _state->ch2_on_rx_led = this->ch2_on_rx_led;
        _state->ch2_on_rx_myk_en = this->ch2_on_rx_myk_en;

        // PS added section
        _state->ProductSignature = this->ProductSignature;
        _state->CpldMinorRevision = this->CpldMinorRevision;
        _state->CpldMajorRevision = this->CpldMajorRevision;
        _state->BuildCodeHH = this->BuildCodeHH;
        _state->BuildCodeDD = this->BuildCodeDD;
        _state->BuildCodeMM = this->BuildCodeMM;
        _state->BuildCodeYY = this->BuildCodeYY;
        _state->PSScratch = this->PSScratch;
        _state->PSCpldReset = this->PSCpldReset;
        _state->VcxoControl = this->VcxoControl;
        _state->RxLoLockDetect = this->RxLoLockDetect;
        _state->TxLoLockDetect = this->TxLoLockDetect;
        _state->MykonosReset = this->MykonosReset;
    }


    template<typename T> std::set<T> get_changed_addrs(void){
        if (_state == nullptr) {
        	; // add an exception here to throw..
            throw -1;
        }
        	// throw uhd::runtime_error("no saved state");
        //check each register for changes
        std::set<T> addrs;
        // PS added section
        if(_state->ProductSignature != this->ProductSignature){
            addrs.insert(0x0);
        }
        if(_state->CpldMinorRevision != this->CpldMinorRevision){
            addrs.insert(0x1);
        }
        if(_state->CpldMajorRevision != this->CpldMajorRevision){
            addrs.insert(0x1);
        }
        if(_state->BuildCodeHH != this->BuildCodeHH){
            addrs.insert(0x2);
        }
        if(_state->BuildCodeDD != this->BuildCodeDD){
            addrs.insert(0x2);
        }
        if(_state->BuildCodeMM != this->BuildCodeMM){
            addrs.insert(0x3);
        }
        if(_state->BuildCodeYY != this->BuildCodeYY){
            addrs.insert(0x4);
        }
        if(_state->PSScratch != this->PSScratch){
            addrs.insert(0x5);
        }
        if(_state->PSCpldReset != this->PSCpldReset){
            addrs.insert(0x10);
        }
        if(_state->VcxoControl != this->VcxoControl){
            addrs.insert(0x11);
        }
        if(_state->RxLoLockDetect != this->RxLoLockDetect){
            addrs.insert(0x12);
        }
        if(_state->TxLoLockDetect != this->TxLoLockDetect){
            addrs.insert(0x12);
        }
        if(_state->MykonosReset != this->MykonosReset){
            addrs.insert(0x13);
        }
        if(_state->scratch != this->scratch){
            addrs.insert(64);
        }
        if(_state->cpld_reset != this->cpld_reset){
            addrs.insert(65);
        }
        if(_state->ch1_idle_tx_sw1 != this->ch1_idle_tx_sw1){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_sw2 != this->ch1_idle_tx_sw2){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_sw3 != this->ch1_idle_tx_sw3){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_lowband_mixer_path_select != this->ch1_idle_tx_lowband_mixer_path_select){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_mixer_en != this->ch1_idle_tx_mixer_en){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_amp_en != this->ch1_idle_tx_amp_en){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_pa_en != this->ch1_idle_tx_pa_en){
            addrs.insert(80);
        }
        if(_state->ch1_idle_sw_trx != this->ch1_idle_sw_trx){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_led != this->ch1_idle_tx_led){
            addrs.insert(80);
        }
        if(_state->ch1_idle_tx_myk_en != this->ch1_idle_tx_myk_en){
            addrs.insert(80);
        }
        if(_state->ch1_idle_rx_sw1 != this->ch1_idle_rx_sw1){
            addrs.insert(81);
        }
        if(_state->ch1_idle_rx_sw2 != this->ch1_idle_rx_sw2){
            addrs.insert(81);
        }
        if(_state->ch1_idle_rx_sw3 != this->ch1_idle_rx_sw3){
            addrs.insert(81);
        }
        if(_state->ch1_idle_rx_sw4 != this->ch1_idle_rx_sw4){
            addrs.insert(81);
        }
        if(_state->ch1_idle_rx_sw5 != this->ch1_idle_rx_sw5){
            addrs.insert(81);
        }
        if(_state->ch1_idle_rx_sw6 != this->ch1_idle_rx_sw6){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx_loband_mixer_path_sel != this->ch1_idle_rx_loband_mixer_path_sel){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx_mixer_en != this->ch1_idle_rx_mixer_en){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx_amp_en != this->ch1_idle_rx_amp_en){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx_lna1_en != this->ch1_idle_rx_lna1_en){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx_lna2_en != this->ch1_idle_rx_lna2_en){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx2_led != this->ch1_idle_rx2_led){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx_led != this->ch1_idle_rx_led){
            addrs.insert(82);
        }
        if(_state->ch1_idle_rx_myk_en != this->ch1_idle_rx_myk_en){
            addrs.insert(82);
        }
        if(_state->ch1_on_tx_sw1 != this->ch1_on_tx_sw1){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_sw2 != this->ch1_on_tx_sw2){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_sw3 != this->ch1_on_tx_sw3){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_lowband_mixer_path_select != this->ch1_on_tx_lowband_mixer_path_select){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_mixer_en != this->ch1_on_tx_mixer_en){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_amp_en != this->ch1_on_tx_amp_en){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_pa_en != this->ch1_on_tx_pa_en){
            addrs.insert(83);
        }
        if(_state->ch1_on_sw_trx != this->ch1_on_sw_trx){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_led != this->ch1_on_tx_led){
            addrs.insert(83);
        }
        if(_state->ch1_on_tx_myk_en != this->ch1_on_tx_myk_en){
            addrs.insert(83);
        }
        if(_state->ch1_on_rx_sw1 != this->ch1_on_rx_sw1){
            addrs.insert(84);
        }
        if(_state->ch1_on_rx_sw2 != this->ch1_on_rx_sw2){
            addrs.insert(84);
        }
        if(_state->ch1_on_rx_sw3 != this->ch1_on_rx_sw3){
            addrs.insert(84);
        }
        if(_state->ch1_on_rx_sw4 != this->ch1_on_rx_sw4){
            addrs.insert(84);
        }
        if(_state->ch1_on_rx_sw5 != this->ch1_on_rx_sw5){
            addrs.insert(84);
        }
        if(_state->ch1_on_rx_sw6 != this->ch1_on_rx_sw6){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx_loband_mixer_path_sel != this->ch1_on_rx_loband_mixer_path_sel){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx_mixer_en != this->ch1_on_rx_mixer_en){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx_amp_en != this->ch1_on_rx_amp_en){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx_lna1_en != this->ch1_on_rx_lna1_en){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx_lna2_en != this->ch1_on_rx_lna2_en){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx2_led != this->ch1_on_rx2_led){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx_led != this->ch1_on_rx_led){
            addrs.insert(85);
        }
        if(_state->ch1_on_rx_myk_en != this->ch1_on_rx_myk_en){
            addrs.insert(85);
        }
        if(_state->ch2_idle_tx_sw1 != this->ch2_idle_tx_sw1){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_sw2 != this->ch2_idle_tx_sw2){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_sw3 != this->ch2_idle_tx_sw3){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_lowband_mixer_path_select != this->ch2_idle_tx_lowband_mixer_path_select){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_mixer_en != this->ch2_idle_tx_mixer_en){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_amp_en != this->ch2_idle_tx_amp_en){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_pa_en != this->ch2_idle_tx_pa_en){
            addrs.insert(96);
        }
        if(_state->ch2_idle_sw_trx != this->ch2_idle_sw_trx){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_led != this->ch2_idle_tx_led){
            addrs.insert(96);
        }
        if(_state->ch2_idle_tx_myk_en != this->ch2_idle_tx_myk_en){
            addrs.insert(96);
        }
        if(_state->ch2_idle_rx_sw1 != this->ch2_idle_rx_sw1){
            addrs.insert(97);
        }
        if(_state->ch2_idle_rx_sw2 != this->ch2_idle_rx_sw2){
            addrs.insert(97);
        }
        if(_state->ch2_idle_rx_sw3 != this->ch2_idle_rx_sw3){
            addrs.insert(97);
        }
        if(_state->ch2_idle_rx_sw4 != this->ch2_idle_rx_sw4){
            addrs.insert(97);
        }
        if(_state->ch2_idle_rx_sw5 != this->ch2_idle_rx_sw5){
            addrs.insert(97);
        }
        if(_state->ch2_idle_rx_sw6 != this->ch2_idle_rx_sw6){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx_loband_mixer_path_sel != this->ch2_idle_rx_loband_mixer_path_sel){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx_mixer_en != this->ch2_idle_rx_mixer_en){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx_amp_en != this->ch2_idle_rx_amp_en){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx_lna1_en != this->ch2_idle_rx_lna1_en){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx_lna2_en != this->ch2_idle_rx_lna2_en){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx2_led != this->ch2_idle_rx2_led){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx_led != this->ch2_idle_rx_led){
            addrs.insert(98);
        }
        if(_state->ch2_idle_rx_myk_en != this->ch2_idle_rx_myk_en){
            addrs.insert(98);
        }
        if(_state->ch2_on_tx_sw1 != this->ch2_on_tx_sw1){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_sw2 != this->ch2_on_tx_sw2){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_sw3 != this->ch2_on_tx_sw3){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_lowband_mixer_path_select != this->ch2_on_tx_lowband_mixer_path_select){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_mixer_en != this->ch2_on_tx_mixer_en){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_amp_en != this->ch2_on_tx_amp_en){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_pa_en != this->ch2_on_tx_pa_en){
            addrs.insert(99);
        }
        if(_state->ch2_on_sw_trx != this->ch2_on_sw_trx){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_led != this->ch2_on_tx_led){
            addrs.insert(99);
        }
        if(_state->ch2_on_tx_myk_en != this->ch2_on_tx_myk_en){
            addrs.insert(99);
        }
        if(_state->ch2_on_rx_sw1 != this->ch2_on_rx_sw1){
            addrs.insert(100);
        }
        if(_state->ch2_on_rx_sw2 != this->ch2_on_rx_sw2){
            addrs.insert(100);
        }
        if(_state->ch2_on_rx_sw3 != this->ch2_on_rx_sw3){
            addrs.insert(100);
        }
        if(_state->ch2_on_rx_sw4 != this->ch2_on_rx_sw4){
            addrs.insert(100);
        }
        if(_state->ch2_on_rx_sw5 != this->ch2_on_rx_sw5){
            addrs.insert(100);
        }
        if(_state->ch2_on_rx_sw6 != this->ch2_on_rx_sw6){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx_loband_mixer_path_sel != this->ch2_on_rx_loband_mixer_path_sel){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx_mixer_en != this->ch2_on_rx_mixer_en){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx_amp_en != this->ch2_on_rx_amp_en){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx_lna1_en != this->ch2_on_rx_lna1_en){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx_lna2_en != this->ch2_on_rx_lna2_en){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx2_led != this->ch2_on_rx2_led){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx_led != this->ch2_on_rx_led){
            addrs.insert(101);
        }
        if(_state->ch2_on_rx_myk_en != this->ch2_on_rx_myk_en){
            addrs.insert(101);
        }
        return addrs;
    }

// private:
    magnesium_cpld_regs_t *_state;
};

#endif//__CPLDREGISTERS_H__