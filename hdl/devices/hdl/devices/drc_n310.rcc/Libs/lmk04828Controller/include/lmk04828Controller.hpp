#ifndef __LMK04828_CONTROLLER_H__
#define __LMK04828_CONTROLLER_H__

#include "lmk04828Registers.hpp"

#include <functional>
#include <memory>
#include <mutex>

#include <map>
#include <cmath> // for std::ceil used in MPM helper functions that get translated here
#include <set>
#include <utility> // std::pair

#include <iostream>
#include <iomanip> // these just for nice printing for now

#include <algorithm>

/*
 *
 * Static configuration for the clock paths
 *
 * 2 System inputs:
 *		- MCR
 *		- Reference Clock Frequency
 *
 *
 * Object data members:
 *		- clkout divider 	- MCR
 *		- PLL1/2 N_Divider	- MCR
 *		- Sysref divider 	- MCR
 *		- PLL2 prescaler	- MCR
 *		- PLL2 VCO frequency- MCR
 *		- Clkin_R_Divider	- ref clock frequency
 *
 *
 *
 *
 * After configuration: 0x182/3 --> "clear some stickies, check for PLL Lock"
 * 
 *
 *
*/

class lmk04828_ctrl {
private:
	std::int_least32_t m_original_masterClockRate_Hz;

	std::int_least32_t m_external_crystal_Hz;

	std::int_least32_t m_pll2_r_divider;

public:
	using sptr = std::shared_ptr<lmk04828_ctrl>;
	// frame: 24-bit (@TODO: switch to 3 byte array?)
	// MSB = R/W_
	// next two bits 0
	// next 13 bits (rest of 2 MSBytes) = address
	// LSByte = Data
	using write_spi_t = std::function<void(uint32_t)>;
	using read_spi_t = std::function<uint8_t(uint32_t)>;

	// any controller enums here

	// lmk04828_ctrl(write_spi_t write_spi_fn, read_spi_t read_spi_fn);

	lmk04828_ctrl(bool _unitTesting = false);

	lmk04828_ctrl(write_spi_t write_fn, read_spi_t read_fn, bool _unitTesting = false);

    void print_block_registers(void);

	void reset(void);

	double get_sampling_rate_Msps();

	double get_sampling_rate_Msps_hardware();

	double get_sampling_rate_Msps_software();

	double get_external_crystal_Mhz();

	void mcrConfigure(std::int_least32_t _masterClockRate, std::int_least32_t _referenceClockRate);

	void fullConfigure(void);

	void requestSysref(const uint16_t pattern = 0); // default pattern = 1 sysref pulse

	uint16_t get_reg(const uint16_t addr);

	void set_reg(const uint16_t addr, const uint8_t data);

	std::vector<double> getValidMCRs(void);

	// keep referencing cpldController stuff

// private:

	// these sets will be fixed point for perfect comparison
	// std::int_least16_t q_n_resolution; // needs to hold the n value of Qm.n
	bool PORconfigure;

	// these map keys are Q format integers, based on the master clock rates
	struct _mcrRegs {
		std::uint16_t clkout_divider;
		std::uint16_t pll1_n_divider;
		std::uint16_t pll2_n_divider;
		std::uint16_t sysref_divider;
		std::uint16_t pll2_prescaler;
		std::uint16_t vco_mux;
	};
	using lmkPair = std::pair<std::int32_t,struct _mcrRegs>;
	using lmkLUT = std::vector<lmkPair>; // this keeps changing sizes so just make it a vector for now
	static lmkLUT devClockConfigLUT;
	// std::map<std::int_least32_t,std::uint_least8_t> clkout_divider_map;
	// static const lmkLUT clkout_divider_map;
	// std::map<std::int_least32_t,std::uint_least8_t> pll1_n_divider_map;
	// static const lmkLUT pll1_n_divider_map;
	// std::map<std::int_least32_t,std::uint_least8_t> pll2_n_divider_map;
	// static const lmkLUT pll2_n_divider_map;
	// std::map<std::int_least32_t,std::uint_least16_t> sysref_divider_map;
	// static const lmkLUT sysref_divider_map;
	// std::map<std::int_least32_t,std::uint_least8_t> pll2_prescaler_map;
	// static const lmkLUT pll2_prescaler_map;


	// based on the master clock rate input
	std::uint_least32_t clkout_divider;
	std::uint_least32_t pll1_n_divider;
	std::uint_least32_t pll2_n_divider;
	std::uint_least32_t sysref_divider;
	std::uint_least32_t pll2_prescaler;
	std::uint_least32_t vco_mux;
	
	// don't see this used in any configuration
	// std::uint_least32_t pll2_vco_frequency; // calculated, not a map

	// based on reference clock frequency input
	std::uint_least32_t clkin_r_divider; // calculated, not a map

	// writes byte
	// accepts 2 bytes for address
	using write_fn_t = std::function<void(uint16_t, uint8_t)>;

	// returns byte
	// accepts 2 bytes for address
	using read_fn_t = std::function<uint8_t(uint16_t)>;

	void commit(const bool POR = false);

	bool unitTesting;

	write_fn_t _write_fn;

	read_fn_t _read_fn;

	lmk04828_regs_t _regs;

	std::mutex _set_mutex;
};

#endif //__LMK04828_CONTROLLER_H__