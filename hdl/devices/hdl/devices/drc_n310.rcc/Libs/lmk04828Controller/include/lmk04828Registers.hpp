#ifndef __LMK04828_REGS_HPP__
#define __LMK04828_REGS_HPP__

#include <set>
#include <memory>
#include <vector>
#include <stdint.h>
class lmk04828_regs_t {
	// this one argument constructor exists to initialize _state registers
	lmk04828_regs_t(bool) {
		std::vector<uint16_t> allAddrs = get_all_addrs<uint16_t>();
		for ( auto & it : allAddrs ) {
			write_reg(it,0xFF);
		}
		return;
	}
public:
	// public members section
	uint8_t RESET;
	uint8_t SPI_3WIRE_DIS;
	uint8_t POWERDOWN;
	uint8_t ID_DEVICE_TYPE;
	uint16_t ID_PROD;
	uint8_t ID_MASKREV;
	uint16_t ID_VNDR;
	uint8_t CLKout0_1_ODL;
	uint8_t CLKout0_1_IDL;
	uint8_t DCLKout0_DIV;
	uint8_t DCLKout0_DDLY_CNTH;
	uint8_t DCLKout0_DDLY_CNTL;
	uint8_t DCLKout0_ADLY;
	uint8_t DCLKout0_ADLY_MUX;
	uint8_t DCLKout0_MUX;
	uint8_t DCLKout0_HS;
	uint8_t SDCLKout1_MUX;
	uint8_t SDCLKout1_DDLY;
	uint8_t SDCLKout1_HS;
	uint8_t SDCLKout1_ADLY_EN;
	uint8_t SDCLKout1_ADLY;
	uint8_t DCLKout0_DDLY_PD;
	uint8_t DCLKout0_HSg_PD;
	uint8_t DCLKout0_ADLYg_PD;
	uint8_t DCLKout0_ADLY_PD;
	uint8_t CLKout0_1_PD;
	uint8_t SDCLKout1_DIS_MODE;
	uint8_t SDCLKout1_PD;
	uint8_t SDCLKout1_POL;
	uint8_t CLKout1_FMT;
	uint8_t DCLKout0_POL;
	uint8_t CLKout0_FMT;
	uint8_t CLKout2_3_ODL;
	uint8_t CLKout2_3_IDL;
	uint8_t DCLKout2_DIV;
	uint8_t DCLKout2_DDLY_CNTH;
	uint8_t DCLKout2_DDLY_CNTL;
	uint8_t DCLKout2_ADLY;
	uint8_t DCLKout2_ADLY_MUX;
	uint8_t DCLKout2_MUX;
	uint8_t DCLKout2_HS;
	uint8_t SDCLKout3_MUX;
	uint8_t SDCLKout3_DDLY;
	uint8_t SDCLKout3_HS;
	uint8_t SDCLKout3_ADLY_EN;
	uint8_t SDCLKout3_ADLY;
	uint8_t DCLKout2_DDLY_PD;
	uint8_t DCLKout2_HSg_PD;
	uint8_t DCLKout2_ADLYg_PD;
	uint8_t DCLKout2_ADLY_PD;
	uint8_t CLKout2_3_PD;
	uint8_t SDCLKout3_DIS_MODE;
	uint8_t SDCLKout3_PD;
	uint8_t SDCLKout3_POL;
	uint8_t CLKout3_FMT;
	uint8_t DCLKout2_POL;
	uint8_t CLKout2_FMT;
	uint8_t CLKout4_5_ODL;
	uint8_t CLKout4_5_IDL;
	uint8_t DCLKout4_DIV;
	uint8_t DCLKout4_DDLY_CNTH;
	uint8_t DCLKout4_DDLY_CNTL;
	uint8_t DCLKout4_ADLY;
	uint8_t DCLKout4_ADLY_MUX;
	uint8_t DCLKout4_MUX;
	uint8_t DCLKout4_HS;
	uint8_t SDCLKout5_MUX;
	uint8_t SDCLKout5_DDLY;
	uint8_t SDCLKout5_HS;
	uint8_t SDCLKout5_ADLY_EN;
	uint8_t SDCLKout5_ADLY;
	uint8_t DCLKout4_DDLY_PD;
	uint8_t DCLKout4_HSg_PD;
	uint8_t DCLKout4_ADLYg_PD;
	uint8_t DCLKout4_ADLY_PD;
	uint8_t CLKout4_5_PD;
	uint8_t SDCLKout5_DIS_MODE;
	uint8_t SDCLKout5_PD;
	uint8_t SDCLKout5_POL;
	uint8_t CLKout5_FMT;
	uint8_t DCLKout4_POL;
	uint8_t CLKout4_FMT;
	uint8_t CLKout6_7_ODL;
	uint8_t CLKout6_8_IDL;
	uint8_t DCLKout6_DIV;
	uint8_t DCLKout6_DDLY_CNTH;
	uint8_t DCLKout6_DDLY_CNTL;
	uint8_t DCLKout6_ADLY;
	uint8_t DCLKout6_ADLY_MUX;
	uint8_t DCLKout6_MUX;
	uint8_t DCLKout6_HS;
	uint8_t SDCLKout7_MUX;
	uint8_t SDCLKout7_DDLY;
	uint8_t SDCLKout7_HS;
	uint8_t SDCLKout7_ADLY_EN;
	uint8_t SDCLKout7_ADLY;
	uint8_t DCLKout6_DDLY_PD;
	uint8_t DCLKout6_HSg_PD;
	uint8_t DCLKout6_ADLYg_PD;
	uint8_t DCLKout6_ADLY_PD;
	uint8_t CLKout6_7_PD;
	uint8_t SDCLKout7_DIS_MODE;
	uint8_t SDCLKout7_PD;
	uint8_t SDCLKout7_POL;
	uint8_t CLKout7_FMT;
	uint8_t DCLKout6_POL;
	uint8_t CLKout6_FMT;
	uint8_t CLKout8_9_ODL;
	uint8_t CLKout8_9_IDL;
	uint8_t DCLKout8_DIV;
	uint8_t DCLKout8_DDLY_CNTH;
	uint8_t DCLKout8_DDLY_CNTL;
	uint8_t DCLKout8_ADLY;
	uint8_t DCLKout8_ADLY_MUX;
	uint8_t DCLKout8_MUX;
	uint8_t DCLKout8_HS;
	uint8_t SDCLKout9_MUX;
	uint8_t SDCLKout9_DDLY;
	uint8_t SDCLKout9_HS;
	uint8_t SDCLKout9_ADLY_EN;
	uint8_t SDCLKout9_ADLY;
	uint8_t DCLKout8_DDLY_PD;
	uint8_t DCLKout8_HSg_PD;
	uint8_t DCLKout8_ADLYg_PD;
	uint8_t DCLKout8_ADLY_PD;
	uint8_t CLKout8_9_PD;
	uint8_t SDCLKout9_DIS_MODE;
	uint8_t SDCLKout9_PD;
	uint8_t SDCLKout9_POL;
	uint8_t CLKout9_FMT;
	uint8_t DCLKout8_POL;
	uint8_t CLKout8_FMT;
	uint8_t CLKout10_11_ODL;
	uint8_t CLKout10_11_IDL;
	uint8_t DCLKout10_DIV;
	uint8_t DCLKout10_DDLY_CNTH;
	uint8_t DCLKout10_DDLY_CNTL;
	uint8_t DCLKout10_ADLY;
	uint8_t DCLKout10_ADLY_MUX;
	uint8_t DCLKout10_MUX;
	uint8_t DCLKout10_HS;
	uint8_t SDCLKout11_MUX;
	uint8_t SDCLKout11_DDLY;
	uint8_t SDCLKout11_HS;
	uint8_t SDCKLout11_ADLY_EN;
	uint8_t SDCLKout11_ADLY;
	uint8_t DCLKout10_DDLY_PD;
	uint8_t DCLKout10_HSg_PD;
	uint8_t DLCLKout10_ADLYg_PD;
	uint8_t DCLKout10_ADLY_PD;
	uint8_t CLKout10_11_PD;
	uint8_t SDCLKout11_DIS_MODE;
	uint8_t SDCLKout11_PD;
	uint8_t SDCLKout11_POL;
	uint8_t CLKout11_FMT;
	uint8_t DCLKout10_POL;
	uint8_t CLKout10_FMT;
	uint8_t CLKout12_13_ODL;
	uint8_t CLKout12_13_IDL;
	uint8_t DCLKout12_DIV;
	uint8_t DCLKout12_DDLY_CNTH;
	uint8_t DCLKout12_DDLY_CNTL;
	uint8_t DCLKout12_ADLY;
	uint8_t DCLKout12_ADLY_MUX;
	uint8_t DCLKout12_MUX;
	uint8_t DCLKout12_HS;
	uint8_t SDCLKout13_MUX;
	uint8_t SDCLKout13_DDLY;
	uint8_t SDCLKout13_HS;
	uint8_t SDCLKout13_ADLY_EN;
	uint8_t SDCLKout13_ADLY;
	uint8_t DCLKout12_DDLY_PD;
	uint8_t DCLKout12_HSg_PD;
	uint8_t DCLKout12_ADLYg_PD;
	uint8_t DCLKout12_ADLY_PD;
	uint8_t CLKout12_13_PD;
	uint8_t SDCLKout13_DIS_MODE;
	uint8_t SDCLKout13_PD;
	uint8_t SDCLKout13_POL;
	uint8_t CLKout13_FMT;
	uint8_t DCLKout12_POL;
	uint8_t CLKout12_FMT;
	uint8_t VCO_MUX;
	uint8_t OSCout_MUX;
	uint8_t OSCout_FMT;
	uint8_t SYSREF_CLKin0_MUX;
	uint8_t SYSREF_MUX;
	uint16_t SYSREF_DIV;
	uint16_t SYSREF_DDLY;
	uint8_t SYSREF_PULSE_CNT;
	uint8_t PLL2_NCLK_MUX;
	uint8_t PLL1_NCLK_MUX;
	uint8_t FB_MUX;
	uint8_t FB_MUX_EN;
	uint8_t PLL1_PD;
	uint8_t VCO_LDO_PD;
	uint8_t VCO_PD;
	uint8_t OSCin_PD;
	uint8_t SYSREF_GBL_PD;
	uint8_t SYSREF_PD;
	uint8_t SYSREF_DDLY_PD;
	uint8_t SYSREF_PLSR_PD;
	uint8_t DDLYd_SYSREF_EN;
	uint8_t DDLYd12_EN;
	uint8_t DDLYd10_EN;
	uint8_t DDLYd7_EN;
	uint8_t DDLYd6_EN;
	uint8_t DDLYd4_EN;
	uint8_t DDLYd2_EN;
	uint8_t DDLYd0_EN;
	uint8_t DDLYd_STEP_CNT;
	uint8_t SYSREF_DDLY_CLR;
	uint8_t SYNC_1SHOT_EN;
	uint8_t SYNC_POL;
	uint8_t SYNC_EN;
	uint8_t SYNC_PLL2_DLD;
	uint8_t SYNC_PLL1_DLD;
	uint8_t SYNC_MODE;
	uint8_t SYNC_DISSYSREF;
	uint8_t SYNC_DIS12;
	uint8_t SYNC_DIS10;
	uint8_t SYNC_DIS8;
	uint8_t SYNC_DIS6;
	uint8_t SYNC_DIS4;
	uint8_t SYNC_DIS2;
	uint8_t SYNC_DIS0;
	uint8_t CLKin2_EN;
	uint8_t CLKin1_EN;
	uint8_t CLKin0_EN;
	uint8_t CLKin2_TYPE;
	uint8_t CLKin1_TYPE;
	uint8_t CLKin0_TYPE;
	uint8_t CLKin_SEL_POL;
	uint8_t CLKin_SEL_MODE;
	uint8_t CLKin1_OUT_MUX;
	uint8_t CLKin0_OUT_MUX;
	uint8_t CLKin_SEL0_MUX;
	uint8_t CLKin_SEL0_TYPE;
	uint8_t SDIO_RDBK_TYPE;
	uint8_t CLKin_SEL1_MUX;
	uint8_t CLKin_SEL1_TYPE;
	uint8_t RESET_MUX;
	uint8_t RESET_TYPE;
	uint8_t LOS_TIMEOUT;
	uint8_t LOS_EN;
	uint8_t TRACK_EN;
	uint8_t HOLDOVER_FORCE;
	uint8_t MAN_DAC_EN;
	uint16_t MAN_DAC;
	uint8_t DAC_TRIP_LOW;
	uint8_t DAC_CLK_MULT;
	uint8_t DAC_TRIP_HIGH;
	uint8_t DAC_CLK_CNTR;
	uint8_t CLKin_OVERRIDE;
	uint8_t HOLDOVER_PLL1_DET;
	uint8_t HOLDOVER_LOS_DET;
	uint8_t HOLDOVER_VTUNE_DET;
	uint8_t HOLDOVER_HITLESS_SWITCH;
	uint8_t HOLDOVER_EN;
	uint16_t HOLDOVER_DLD_CNT;
	uint16_t CLKin0_R;
	uint16_t CLKin1_R;
	uint16_t CLKin2_R;
	uint16_t PLL1_N;
	uint8_t PLL1_WND_SIZE;
	uint8_t PLL1_CP_TRI;
	uint8_t PLL1_CP_POL;
	uint8_t PLL1_CP_GAIN;
	uint16_t PLL1_DLD_CNT;
	uint8_t PLL1_R_DLY;
	uint8_t PLL1_N_DLY;
	uint8_t PLL1_LD_MUX;
	uint8_t PLL1_LD_TYPE;
	uint16_t PLL2_R;
	uint8_t PLL2_P;
	uint8_t OSCin_FREQ;
	uint8_t PLL2_XTAL_EN;
	uint8_t PLL2_REF_2X_EN;
	uint32_t PLL2_N_CAL;
	uint8_t PLL2_FCAL_DIS;
	uint32_t PLL2_N;
	uint8_t PLL2_WND_SIZE;
	uint8_t PLL2_CP_GAIN;
	uint8_t PLL2_CP_POL;
	uint8_t SYSREF_REQ_EN;
	uint16_t PLL2_DLD_CNT;
	uint8_t PLL2_LF_R4;
	uint8_t PLL2_LF_R3;
	uint8_t PLL2_LF_C4;
	uint8_t PLL2_LF_C3;
	uint8_t PLL2_LD_MUX;
	uint8_t PLL2_LD_TYPE;
	uint8_t PLL2_PRE_PD;
	uint8_t PLL2_PD;
	uint8_t VCO1_DIV;
	uint8_t OPT_REG_1;
	uint8_t OPT_REG_2;
	uint8_t RB_PLL1_LD_LOST;
	uint8_t RB_PLL1_LD;
	uint8_t CLR_PLL1_LD_LOST;
	uint8_t RB_PLL2_LD_LOST;
	uint8_t RB_PLL2_LD;
	uint8_t CLR_PLL2_LD_LOST;
	uint16_t RB_DAC_VALUE;
	uint8_t RB_CLKin2_SEL;
	uint8_t RB_CLKin1_SEL;
	uint8_t RB_CLKin0_SEL;
	uint8_t RB_CLKin1_LOS;
	uint8_t RB_CLKin0_LOS;
	uint8_t RB_HOLDOVER;
	uint32_t SPI_LOCK;
	// public methods section
	// This is the public constructor
	lmk04828_regs_t() : _state(new lmk04828_regs_t(true)) { // initialize all bitfields via register writes!
		std::vector<uint16_t> allAddrs = get_all_addrs<uint16_t>();
		for ( auto & it : allAddrs ) {
			write_reg(it,0x00);
		}
	
		write_reg(0x000,0);
		write_reg(0x002,0);
		write_reg(0x003,6);
		write_reg(0x004,208);
		write_reg(0x005,91);
		write_reg(0x006,32); // LMK04828
		write_reg(0x00C,81);
		write_reg(0x00D,4);
		// CLKoutX_Y registers
		write_reg(0x100,2);
		write_reg(0x108,4);
		write_reg(0x110,8);
		write_reg(0x118,8);
		write_reg(0x120,8);
		write_reg(0x128,8);
		write_reg(0x130,2);
		// DCLKoutX... registers
		write_reg(0x101,85); // (5<<4 | 5)
		write_reg(0x109,85); // (5<<4 | 5)
		write_reg(0x111,85); // (5<<4 | 5)
		write_reg(0x119,85); // (5<<4 | 5)
		write_reg(0x121,85); // (5<<4 | 5)
		write_reg(0x129,85); // (5<<4 | 5)
		write_reg(0x131,85); // (5<<4 | 5)
		// More DCLKoutX.. registers
		// 0x103,10B,113,11B,123,12B,133 all 0's
		
		// More DCLKoutX.. registers
		// 0x104,10c,114,11c,124,12c,134 all 0's
	
		// SDCLKoutY.. registers
		// 0x105,10D,115,11D,125,12D,135 all 0's
	
		// DCLKoutX.. registers
		write_reg(0x106,0x79); // (1<<6 | 1<<5 | 1<<4 | 1<<3 | 1)
		write_reg(0x106,0x79); // (1<<6 | 1<<5 | 1<<4 | 1<<3 | 1)
		write_reg(0x106,0x71); // (1<<6 | 1<<5 | 1<<4 | 0<<3 | 1)
		write_reg(0x106,0x71); // (1<<6 | 1<<5 | 1<<4 | 0<<3 | 1)
		write_reg(0x106,0x71); // (1<<6 | 1<<5 | 1<<4 | 0<<3 | 1)
		write_reg(0x106,0x71); // (1<<6 | 1<<5 | 1<<4 | 0<<3 | 1)
		write_reg(0x106,0x79); // (1<<6 | 1<<5 | 1<<4 | 1<<3 | 1)
	
		// SDCLKoutY registers, some of which are all 0
		// 0x107, 0x10F, 0x137 all 0's
		write_reg(0x117,1);
		write_reg(0x11F,1);
		write_reg(0x127,1);
		write_reg(0x12F,1);
	
		write_reg(0x138,0x4);
		// 0x139 is 0
		write_reg(0x13A,12);
		// 0x13B is 0
		// 0x13C is 0
		write_reg(0x13D,8);
	
		write_reg(0x13E,3);
		// 0x13F is 0
		write_reg(0x140,0x7);
		// 0x141,142 is all 0's
		write_reg(0x143,0x91);
		// x144 is 0
		// x145 is 0
		write_reg(0x171,0x0A);
		// 0x172 is 0
		write_reg(0x146,0x18);
		write_reg(0x147,0x3A);
	
		write_reg(0x148,0x2);
		write_reg(0x149,0x42);
		write_reg(0x14A,0x2);
		write_reg(0x14B,0x16);
		write_reg(0x14B,0x2); // ----------------------- READ DATASHEET ------------------------------
		// 0x14C, 14D, 14E are 0
		write_reg(0x14F,0x7F);
		write_reg(0x150,0x3);
		write_reg(0x151,0x2);
		// 0x152,153 is 0
		write_reg(0x154,120);
		// 0x155 is 0
		write_reg(0x156,150);
		// 0x157 is 0
		write_reg(0x158,150);
		// 0x159 is 0
		write_reg(0x15A,120);
		write_reg(0x15B,0xD4);
		write_reg(0x15C,32);
		// 0x15D, 15E is 0
		write_reg(0x15F,0x0E);
		// 0x160 is 0
		write_reg(0x161,0x02);
		write_reg(0x162,0x5D);
		// 0x163,164 is 0
		write_reg(0x165,12);
		// 0x166,167 is 0
		write_reg(0x168,12);
		write_reg(0x169,0x59);
		write_reg(0x16A,32);
		// 0x16B,16C,16D is 0
		write_reg(0x16E,0x16);
		// nothing specified for 0x173 POR default, guessing it's 0
		// x174 is 0
		// no POR default values for 0x17C,17D,182,183,184
		write_reg(0x184,0x80); // only 1 default valued bitfield..
		// 0x185 is 0
		// 1ffd, 1ffe, 1fff need to stay default valued for registers to be unlocked!
		// 0x1FFD, 0x1FFE are 0
		write_reg(0x1FFF,83);
	
		return;
	}
	
	uint8_t get_reg(uint16_t addr){
		uint8_t reg = 0;
		switch(addr){ // every register is 1 byte
			case 0x000:
				reg |= (uint32_t(RESET) & 0x1) << 7;
				reg |= (uint32_t(SPI_3WIRE_DIS) & 0x1) << 4;
				break;
			case 0x002:
				reg |= (uint32_t(POWERDOWN) & 0x1) << 0;
				break;
			case 0x003:
				reg |= (uint32_t(ID_DEVICE_TYPE) & 0xFF) << 0;
				break;
			case 0x004:
				reg |= ((uint32_t(ID_PROD) & 0xFF00) >> 8) << 0;
				break;
			case 0x005:
				reg |= (uint32_t(ID_PROD) & 0xFF) << 0;
				break;
			case 0x006:
				reg |= (uint32_t(ID_MASKREV) & 0xFF) << 0;
				break;
			case 0x00C:
				reg |= ((uint32_t(ID_VNDR) & 0xFF00) >> 8) << 0;
				break;
			case 0x00D:
				reg |= (uint32_t(ID_VNDR) & 0xFF) << 0;
				break;
			case 0x100:
				reg |= (uint32_t(CLKout0_1_ODL) & 0x1) << 6;
				reg |= (uint32_t(CLKout0_1_IDL) & 0x1) << 5;
				reg |= (uint32_t(DCLKout0_DIV) & 0x1F) << 0;
				break;
			case 0x101:
				reg |= (uint32_t(DCLKout0_DDLY_CNTH) & 0xF) << 4;
				reg |= (uint32_t(DCLKout0_DDLY_CNTL) & 0xF) << 0;
				break;
			case 0x103:
				reg |= (uint32_t(DCLKout0_ADLY) & 0x1F) << 3;
				reg |= (uint32_t(DCLKout0_ADLY_MUX) & 0x1) << 2;
				reg |= (uint32_t(DCLKout0_MUX) & 0x3) << 0;
				break;
			case 0x104:
				reg |= (uint32_t(DCLKout0_HS) & 0x1) << 6;
				reg |= (uint32_t(SDCLKout1_MUX) & 0x1) << 5;
				reg |= (uint32_t(SDCLKout1_DDLY) & 0xF) << 1;
				reg |= (uint32_t(SDCLKout1_HS) & 0x1) << 0;
				break;
			case 0x105:
				reg |= (uint32_t(SDCLKout1_ADLY_EN) & 0x1) << 4;
				reg |= (uint32_t(SDCLKout1_ADLY) & 0xF) << 0;
				break;
			case 0x106:
				reg |= (uint32_t(DCLKout0_DDLY_PD) & 0x1) << 7;
				reg |= (uint32_t(DCLKout0_HSg_PD) & 0x1) << 6;
				reg |= (uint32_t(DCLKout0_ADLYg_PD) & 0x1) << 5;
				reg |= (uint32_t(DCLKout0_ADLY_PD) & 0x1) << 4;
				reg |= (uint32_t(CLKout0_1_PD) & 0x1) << 3;
				reg |= (uint32_t(SDCLKout1_DIS_MODE) & 0x3) << 1;
				reg |= (uint32_t(SDCLKout1_PD) & 0x1) << 0;
				break;
			case 0x107:
				reg |= (uint32_t(SDCLKout1_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKout1_FMT) & 0x7) << 4;
				reg |= (uint32_t(DCLKout0_POL) & 0x1) << 3;
				reg |= (uint32_t(CLKout0_FMT) & 0x7) << 0;
				break;
			case 0x108:
				reg |= (uint32_t(CLKout2_3_ODL) & 0x1) << 6;
				reg |= (uint32_t(CLKout2_3_IDL) & 0x1) << 5;
				reg |= (uint32_t(DCLKout2_DIV) & 0x1F) << 0;
				break;
			case 0x109:
				reg |= (uint32_t(DCLKout2_DDLY_CNTH) & 0xF) << 4;
				reg |= (uint32_t(DCLKout2_DDLY_CNTL) & 0xF) << 0;
				break;
			case 0x10B:
				reg |= (uint32_t(DCLKout2_ADLY) & 0x1F) << 3;
				reg |= (uint32_t(DCLKout2_ADLY_MUX) & 0x1) << 2;
				reg |= (uint32_t(DCLKout2_MUX) & 0x3) << 0;
				break;
			case 0x10C:
				reg |= (uint32_t(DCLKout2_HS) & 0x1) << 6;
				reg |= (uint32_t(SDCLKout3_MUX) & 0x1) << 5;
				reg |= (uint32_t(SDCLKout3_DDLY) & 0xF) << 1;
				reg |= (uint32_t(SDCLKout3_HS) & 0x1) << 0;
				break;
			case 0x10D:
				reg |= (uint32_t(SDCLKout3_ADLY_EN) & 0x1) << 4;
				reg |= (uint32_t(SDCLKout3_ADLY) & 0xF) << 0;
				break;
			case 0x10E:
				reg |= (uint32_t(DCLKout2_DDLY_PD) & 0x1) << 7;
				reg |= (uint32_t(DCLKout2_HSg_PD) & 0x1) << 6;
				reg |= (uint32_t(DCLKout2_ADLYg_PD) & 0x1) << 5;
				reg |= (uint32_t(DCLKout2_ADLY_PD) & 0x1) << 4;
				reg |= (uint32_t(CLKout2_3_PD) & 0x1) << 3;
				reg |= (uint32_t(SDCLKout3_DIS_MODE) & 0x3) << 1;
				reg |= (uint32_t(SDCLKout3_PD) & 0x1) << 0;
				break;
			case 0x10F:
				reg |= (uint32_t(SDCLKout3_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKout3_FMT) & 0x7) << 4;
				reg |= (uint32_t(DCLKout2_POL) & 0x1) << 3;
				reg |= (uint32_t(CLKout2_FMT) & 0x7) << 0;
				break;
			case 0x110:
				reg |= (uint32_t(CLKout4_5_ODL) & 0x1) << 6;
				reg |= (uint32_t(CLKout4_5_IDL) & 0x1) << 5;
				reg |= (uint32_t(DCLKout4_DIV) & 0x1F) << 0;
				break;
			case 0x111:
				reg |= (uint32_t(DCLKout4_DDLY_CNTH) & 0xF) << 4;
				reg |= (uint32_t(DCLKout4_DDLY_CNTL) & 0xF) << 0;
				break;
			case 0x113:
				reg |= (uint32_t(DCLKout4_ADLY) & 0x1F) << 3;
				reg |= (uint32_t(DCLKout4_ADLY_MUX) & 0x1) << 2;
				reg |= (uint32_t(DCLKout4_MUX) & 0x3) << 0;
				break;
			case 0x114:
				reg |= (uint32_t(DCLKout4_HS) & 0x1) << 6;
				reg |= (uint32_t(SDCLKout5_MUX) & 0x1) << 5;
				reg |= (uint32_t(SDCLKout5_DDLY) & 0xF) << 1;
				reg |= (uint32_t(SDCLKout5_HS) & 0x1) << 0;
				break;
			case 0x115:
				reg |= (uint32_t(SDCLKout5_ADLY_EN) & 0x1) << 4;
				reg |= (uint32_t(SDCLKout5_ADLY) & 0xF) << 0;
				break;
			case 0x116:
				reg |= (uint32_t(DCLKout4_DDLY_PD) & 0x1) << 7;
				reg |= (uint32_t(DCLKout4_HSg_PD) & 0x1) << 6;
				reg |= (uint32_t(DCLKout4_ADLYg_PD) & 0x1) << 5;
				reg |= (uint32_t(DCLKout4_ADLY_PD) & 0x1) << 4;
				reg |= (uint32_t(CLKout4_5_PD) & 0x1) << 3;
				reg |= (uint32_t(SDCLKout5_DIS_MODE) & 0x3) << 1;
				reg |= (uint32_t(SDCLKout5_PD) & 0x1) << 0;
				break;
			case 0x117:
				reg |= (uint32_t(SDCLKout5_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKout5_FMT) & 0x7) << 4;
				reg |= (uint32_t(DCLKout4_POL) & 0x1) << 3;
				reg |= (uint32_t(CLKout4_FMT) & 0x7) << 0;
				break;
			case 0x118:
				reg |= (uint32_t(CLKout6_7_ODL) & 0x1) << 6;
				reg |= (uint32_t(CLKout6_8_IDL) & 0x1) << 5;
				reg |= (uint32_t(DCLKout6_DIV) & 0x1F) << 0;
				break;
			case 0x119:
				reg |= (uint32_t(DCLKout6_DDLY_CNTH) & 0xF) << 4;
				reg |= (uint32_t(DCLKout6_DDLY_CNTL) & 0xF) << 0;
				break;
			case 0x11B:
				reg |= (uint32_t(DCLKout6_ADLY) & 0x1F) << 3;
				reg |= (uint32_t(DCLKout6_ADLY_MUX) & 0x1) << 2;
				reg |= (uint32_t(DCLKout6_MUX) & 0x3) << 0;
				break;
			case 0x11C:
				reg |= (uint32_t(DCLKout6_HS) & 0x1) << 6;
				reg |= (uint32_t(SDCLKout7_MUX) & 0x1) << 5;
				reg |= (uint32_t(SDCLKout7_DDLY) & 0xF) << 1;
				reg |= (uint32_t(SDCLKout7_HS) & 0x1) << 0;
				break;
			case 0x11D:
				reg |= (uint32_t(SDCLKout7_ADLY_EN) & 0x1) << 4;
				reg |= (uint32_t(SDCLKout7_ADLY) & 0xF) << 0;
				break;
			case 0x11E:
				reg |= (uint32_t(DCLKout6_DDLY_PD) & 0x1) << 7;
				reg |= (uint32_t(DCLKout6_HSg_PD) & 0x1) << 6;
				reg |= (uint32_t(DCLKout6_ADLYg_PD) & 0x1) << 5;
				reg |= (uint32_t(DCLKout6_ADLY_PD) & 0x1) << 4;
				reg |= (uint32_t(CLKout6_7_PD) & 0x1) << 3;
				reg |= (uint32_t(SDCLKout7_DIS_MODE) & 0x3) << 1;
				reg |= (uint32_t(SDCLKout7_PD) & 0x1) << 0;
				break;
			case 0x11F:
				reg |= (uint32_t(SDCLKout7_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKout7_FMT) & 0x7) << 4;
				reg |= (uint32_t(DCLKout6_POL) & 0x1) << 3;
				reg |= (uint32_t(CLKout6_FMT) & 0x7) << 0;
				break;
			case 0x120:
				reg |= (uint32_t(CLKout8_9_ODL) & 0x1) << 6;
				reg |= (uint32_t(CLKout8_9_IDL) & 0x1) << 5;
				reg |= (uint32_t(DCLKout8_DIV) & 0x1F) << 0;
				break;
			case 0x121:
				reg |= (uint32_t(DCLKout8_DDLY_CNTH) & 0xF) << 4;
				reg |= (uint32_t(DCLKout8_DDLY_CNTL) & 0xF) << 0;
				break;
			case 0x123:
				reg |= (uint32_t(DCLKout8_ADLY) & 0x1F) << 3;
				reg |= (uint32_t(DCLKout8_ADLY_MUX) & 0x1) << 2;
				reg |= (uint32_t(DCLKout8_MUX) & 0x3) << 0;
				break;
			case 0x124:
				reg |= (uint32_t(DCLKout8_HS) & 0x1) << 6;
				reg |= (uint32_t(SDCLKout9_MUX) & 0x1) << 5;
				reg |= (uint32_t(SDCLKout9_DDLY) & 0xF) << 1;
				reg |= (uint32_t(SDCLKout9_HS) & 0x1) << 0;
				break;
			case 0x125:
				reg |= (uint32_t(SDCLKout9_ADLY_EN) & 0x1) << 4;
				reg |= (uint32_t(SDCLKout9_ADLY) & 0xF) << 0;
				break;
			case 0x126:
				reg |= (uint32_t(DCLKout8_DDLY_PD) & 0x1) << 7;
				reg |= (uint32_t(DCLKout8_HSg_PD) & 0x1) << 6;
				reg |= (uint32_t(DCLKout8_ADLYg_PD) & 0x1) << 5;
				reg |= (uint32_t(DCLKout8_ADLY_PD) & 0x1) << 4;
				reg |= (uint32_t(CLKout8_9_PD) & 0x1) << 3;
				reg |= (uint32_t(SDCLKout9_DIS_MODE) & 0x3) << 1;
				reg |= (uint32_t(SDCLKout9_PD) & 0x1) << 0;
				break;
			case 0x127:
				reg |= (uint32_t(SDCLKout9_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKout9_FMT) & 0x7) << 4;
				reg |= (uint32_t(DCLKout8_POL) & 0x1) << 3;
				reg |= (uint32_t(CLKout8_FMT) & 0x7) << 0;
				break;
			case 0x128:
				reg |= (uint32_t(CLKout10_11_ODL) & 0x1) << 6;
				reg |= (uint32_t(CLKout10_11_IDL) & 0x1) << 5;
				reg |= (uint32_t(DCLKout10_DIV) & 0x1F) << 0;
				break;
			case 0x129:
				reg |= (uint32_t(DCLKout10_DDLY_CNTH) & 0xF) << 4;
				reg |= (uint32_t(DCLKout10_DDLY_CNTL) & 0xF) << 0;
				break;
			case 0x12B:
				reg |= (uint32_t(DCLKout10_ADLY) & 0x1F) << 3;
				reg |= (uint32_t(DCLKout10_ADLY_MUX) & 0x1) << 2;
				reg |= (uint32_t(DCLKout10_MUX) & 0x3) << 0;
				break;
			case 0x12C:
				reg |= (uint32_t(DCLKout10_HS) & 0x1) << 6;
				reg |= (uint32_t(SDCLKout11_MUX) & 0x1) << 5;
				reg |= (uint32_t(SDCLKout11_DDLY) & 0xF) << 1;
				reg |= (uint32_t(SDCLKout11_HS) & 0x1) << 0;
				break;
			case 0x12D:
				reg |= (uint32_t(SDCKLout11_ADLY_EN) & 0x1) << 4;
				reg |= (uint32_t(SDCLKout11_ADLY) & 0xF) << 0;
				break;
			case 0x12E:
				reg |= (uint32_t(DCLKout10_DDLY_PD) & 0x1) << 7;
				reg |= (uint32_t(DCLKout10_HSg_PD) & 0x1) << 6;
				reg |= (uint32_t(DLCLKout10_ADLYg_PD) & 0x1) << 5;
				reg |= (uint32_t(DCLKout10_ADLY_PD) & 0x1) << 4;
				reg |= (uint32_t(CLKout10_11_PD) & 0x1) << 3;
				reg |= (uint32_t(SDCLKout11_DIS_MODE) & 0x3) << 1;
				reg |= (uint32_t(SDCLKout11_PD) & 0x1) << 0;
				break;
			case 0x12F:
				reg |= (uint32_t(SDCLKout11_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKout11_FMT) & 0x7) << 4;
				reg |= (uint32_t(DCLKout10_POL) & 0x1) << 3;
				reg |= (uint32_t(CLKout10_FMT) & 0x7) << 0;
				break;
			case 0x130:
				reg |= (uint32_t(CLKout12_13_ODL) & 0x1) << 6;
				reg |= (uint32_t(CLKout12_13_IDL) & 0x1) << 5;
				reg |= (uint32_t(DCLKout12_DIV) & 0x1F) << 0;
				break;
			case 0x131:
				reg |= (uint32_t(DCLKout12_DDLY_CNTH) & 0xF) << 4;
				reg |= (uint32_t(DCLKout12_DDLY_CNTL) & 0xF) << 0;
				break;
			case 0x133:
				reg |= (uint32_t(DCLKout12_ADLY) & 0x1F) << 3;
				reg |= (uint32_t(DCLKout12_ADLY_MUX) & 0x1) << 2;
				reg |= (uint32_t(DCLKout12_MUX) & 0x3) << 0;
				break;
			case 0x134:
				reg |= (uint32_t(DCLKout12_HS) & 0x1) << 6;
				reg |= (uint32_t(SDCLKout13_MUX) & 0x1) << 5;
				reg |= (uint32_t(SDCLKout13_DDLY) & 0xF) << 1;
				reg |= (uint32_t(SDCLKout13_HS) & 0x1) << 0;
				break;
			case 0x135:
				reg |= (uint32_t(SDCLKout13_ADLY_EN) & 0x1) << 4;
				reg |= (uint32_t(SDCLKout13_ADLY) & 0xF) << 0;
				break;
			case 0x136:
				reg |= (uint32_t(DCLKout12_DDLY_PD) & 0x1) << 7;
				reg |= (uint32_t(DCLKout12_HSg_PD) & 0x1) << 6;
				reg |= (uint32_t(DCLKout12_ADLYg_PD) & 0x1) << 5;
				reg |= (uint32_t(DCLKout12_ADLY_PD) & 0x1) << 4;
				reg |= (uint32_t(CLKout12_13_PD) & 0x1) << 3;
				reg |= (uint32_t(SDCLKout13_DIS_MODE) & 0x3) << 1;
				reg |= (uint32_t(SDCLKout13_PD) & 0x1) << 0;
				break;
			case 0x137:
				reg |= (uint32_t(SDCLKout13_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKout13_FMT) & 0x7) << 4;
				reg |= (uint32_t(DCLKout12_POL) & 0x1) << 3;
				reg |= (uint32_t(CLKout12_FMT) & 0x7) << 0;
				break;
			case 0x138:
				reg |= (uint32_t(VCO_MUX) & 0x3) << 5;
				reg |= (uint32_t(OSCout_MUX) & 0x1) << 4;
				reg |= (uint32_t(OSCout_FMT) & 0xF) << 0;
				break;
			case 0x139:
				reg |= (uint32_t(SYSREF_CLKin0_MUX) & 0x1) << 2;
				reg |= (uint32_t(SYSREF_MUX) & 0x3) << 0;
				break;
			case 0x13A:
				reg |= ((uint32_t(SYSREF_DIV) & 0x1F00) >> 8) << 0;
				break;
			case 0x13B:
				reg |= (uint32_t(SYSREF_DIV) & 0xFF) << 0;
				break;
			case 0x13C:
				reg |= ((uint32_t(SYSREF_DDLY) & 0x1F00) >> 8) << 0;
				break;
			case 0x13D:
				reg |= (uint32_t(SYSREF_DDLY) & 0xFF) << 0;
				break;
			case 0x13E:
				reg |= (uint32_t(SYSREF_PULSE_CNT) & 0x3) << 0;
				break;
			case 0x13F:
				reg |= (uint32_t(PLL2_NCLK_MUX) & 0x1) << 4;
				reg |= (uint32_t(PLL1_NCLK_MUX) & 0x1) << 3;
				reg |= (uint32_t(FB_MUX) & 0x3) << 1;
				reg |= (uint32_t(FB_MUX_EN) & 0x1) << 0;
				break;
			case 0x140:
				reg |= (uint32_t(PLL1_PD) & 0x1) << 7;
				reg |= (uint32_t(VCO_LDO_PD) & 0x1) << 6;
				reg |= (uint32_t(VCO_PD) & 0x1) << 5;
				reg |= (uint32_t(OSCin_PD) & 0x1) << 4;
				reg |= (uint32_t(SYSREF_GBL_PD) & 0x1) << 3;
				reg |= (uint32_t(SYSREF_PD) & 0x1) << 2;
				reg |= (uint32_t(SYSREF_DDLY_PD) & 0x1) << 1;
				reg |= (uint32_t(SYSREF_PLSR_PD) & 0x1) << 0;
				break;
			case 0x141:
				reg |= (uint32_t(DDLYd_SYSREF_EN) & 0x1) << 7;
				reg |= (uint32_t(DDLYd12_EN) & 0x1) << 6;
				reg |= (uint32_t(DDLYd10_EN) & 0x1) << 5;
				reg |= (uint32_t(DDLYd7_EN) & 0x1) << 4;
				reg |= (uint32_t(DDLYd6_EN) & 0x1) << 3;
				reg |= (uint32_t(DDLYd4_EN) & 0x1) << 2;
				reg |= (uint32_t(DDLYd2_EN) & 0x1) << 1;
				reg |= (uint32_t(DDLYd0_EN) & 0x1) << 0;
				break;
			case 0x142:
				reg |= (uint32_t(DDLYd_STEP_CNT) & 0x1F) << 0;
				break;
			case 0x143:
				reg |= (uint32_t(SYSREF_DDLY_CLR) & 0x1) << 7;
				reg |= (uint32_t(SYNC_1SHOT_EN) & 0x1) << 6;
				reg |= (uint32_t(SYNC_POL) & 0x1) << 5;
				reg |= (uint32_t(SYNC_EN) & 0x1) << 4;
				reg |= (uint32_t(SYNC_PLL2_DLD) & 0x1) << 3;
				reg |= (uint32_t(SYNC_PLL1_DLD) & 0x1) << 2;
				reg |= (uint32_t(SYNC_MODE) & 0x3) << 0;
				break;
			case 0x144:
				reg |= (uint32_t(SYNC_DISSYSREF) & 0x1) << 7;
				reg |= (uint32_t(SYNC_DIS12) & 0x1) << 6;
				reg |= (uint32_t(SYNC_DIS10) & 0x1) << 5;
				reg |= (uint32_t(SYNC_DIS8) & 0x1) << 4;
				reg |= (uint32_t(SYNC_DIS6) & 0x1) << 3;
				reg |= (uint32_t(SYNC_DIS4) & 0x1) << 2;
				reg |= (uint32_t(SYNC_DIS2) & 0x1) << 1;
				reg |= (uint32_t(SYNC_DIS0) & 0x1) << 0;
				break;
			case 0x145:
				reg |= 1 << 6;
				reg |= 1 << 5;
				reg |= 1 << 4;
				reg |= 1 << 3;
				reg |= 1 << 2;
				reg |= 1 << 1;
				reg |= 1 << 0;
				break;
			case 0x146:
				reg |= (uint32_t(CLKin2_EN) & 0x1) << 5;
				reg |= (uint32_t(CLKin1_EN) & 0x1) << 4;
				reg |= (uint32_t(CLKin0_EN) & 0x1) << 3;
				reg |= (uint32_t(CLKin2_TYPE) & 0x1) << 2;
				reg |= (uint32_t(CLKin1_TYPE) & 0x1) << 1;
				reg |= (uint32_t(CLKin0_TYPE) & 0x1) << 0;
				break;
			case 0x147:
				reg |= (uint32_t(CLKin_SEL_POL) & 0x1) << 7;
				reg |= (uint32_t(CLKin_SEL_MODE) & 0x7) << 4;
				reg |= (uint32_t(CLKin1_OUT_MUX) & 0x3) << 2;
				reg |= (uint32_t(CLKin0_OUT_MUX) & 0x3) << 0;
				break;
			case 0x148:
				reg |= (uint32_t(CLKin_SEL0_MUX) & 0x7) << 3;
				reg |= (uint32_t(CLKin_SEL0_TYPE) & 0x7) << 0;
				break;
			case 0x149:
				reg |= (uint32_t(SDIO_RDBK_TYPE) & 0x1) << 6;
				reg |= (uint32_t(CLKin_SEL1_MUX) & 0x7) << 3;
				reg |= (uint32_t(CLKin_SEL1_TYPE) & 0x7) << 0;
				break;
			case 0x14A:
				reg |= (uint32_t(RESET_MUX) & 0x7) << 3;
				reg |= (uint32_t(RESET_TYPE) & 0x7) << 0;
				break;
			case 0x14B:
				reg |= (uint32_t(LOS_TIMEOUT) & 0x3) << 6;
				reg |= (uint32_t(LOS_EN) & 0x1) << 5;
				reg |= (uint32_t(TRACK_EN) & 0x1) << 4;
				reg |= (uint32_t(HOLDOVER_FORCE) & 0x1) << 3;
				reg |= (uint32_t(MAN_DAC_EN) & 0x1) << 2;
				reg |= ((uint32_t(MAN_DAC) & 0x300) >> 8) << 0;
				break;
			case 0x14C:
				reg |= (uint32_t(MAN_DAC) & 0xFF) << 0;
				break;
			case 0x14D:
				reg |= (uint32_t(DAC_TRIP_LOW) & 0x3F) << 0;
				break;
			case 0x14E:
				reg |= (uint32_t(DAC_CLK_MULT) & 0x3) << 6;
				reg |= (uint32_t(DAC_TRIP_HIGH) & 0x3F) << 0;
				break;
			case 0x14F:
				reg |= (uint32_t(DAC_CLK_CNTR) & 0xFF) << 0;
				break;
			case 0x150:
				reg |= (uint32_t(CLKin_OVERRIDE) & 0x1) << 6;
				reg |= (uint32_t(HOLDOVER_PLL1_DET) & 0x1) << 4;
				reg |= (uint32_t(HOLDOVER_LOS_DET) & 0x1) << 3;
				reg |= (uint32_t(HOLDOVER_VTUNE_DET) & 0x1) << 2;
				reg |= (uint32_t(HOLDOVER_HITLESS_SWITCH) & 0x1) << 1;
				reg |= (uint32_t(HOLDOVER_EN) & 0x1) << 0;
				break;
			case 0x151:
				reg |= ((uint32_t(HOLDOVER_DLD_CNT) & 0x3F00) >> 8) << 0;
				break;
			case 0x152:
				reg |= (uint32_t(HOLDOVER_DLD_CNT) & 0xFF) << 0;
				break;
			case 0x153:
				reg |= ((uint32_t(CLKin0_R) & 0x3F00) >> 8) << 0;
				break;
			case 0x154:
				reg |= (uint32_t(CLKin0_R) & 0xFF) << 0;
				break;
			case 0x155:
				reg |= ((uint32_t(CLKin1_R) & 0x3F00) >> 8) << 0;
				break;
			case 0x156:
				reg |= (uint32_t(CLKin1_R) & 0xFF) << 0;
				break;
			case 0x157:
				reg |= ((uint32_t(CLKin2_R) & 0x3F00) >> 8) << 0;
				break;
			case 0x158:
				reg |= (uint32_t(CLKin2_R) & 0xFF) << 0;
				break;
			case 0x159:
				reg |= ((uint32_t(PLL1_N) & 0x3F00) >> 8) << 0;
				break;
			case 0x15A:
				reg |= (uint32_t(PLL1_N) & 0xFF) << 0;
				break;
			case 0x15B:
				reg |= (uint32_t(PLL1_WND_SIZE) & 0x3) << 6;
				reg |= (uint32_t(PLL1_CP_TRI) & 0x1) << 5;
				reg |= (uint32_t(PLL1_CP_POL) & 0x1) << 4;
				reg |= (uint32_t(PLL1_CP_GAIN) & 0xF) << 0;
				break;
			case 0x15C:
				reg |= ((uint32_t(PLL1_DLD_CNT) & 0x3F00) >> 8) << 0;
				break;
			case 0x15D:
				reg |= (uint32_t(PLL1_DLD_CNT) & 0xFF) << 0;
				break;
			case 0x15E:
				reg |= (uint32_t(PLL1_R_DLY) & 0x7) << 3;
				reg |= (uint32_t(PLL1_N_DLY) & 0x7) << 0;
				break;
			case 0x15F:
				reg |= (uint32_t(PLL1_LD_MUX) & 0x1F) << 3;
				reg |= (uint32_t(PLL1_LD_TYPE) & 0x7) << 0;
				break;
			case 0x160:
				reg |= ((uint32_t(PLL2_R) & 0xF00) >> 8) << 0;
				break;
			case 0x161:
				reg |= (uint32_t(PLL2_R) & 0xFF) << 0;
				break;
			case 0x162:
				reg |= (uint32_t(PLL2_P) & 0x7) << 5;
				reg |= (uint32_t(OSCin_FREQ) & 0x7) << 2;
				reg |= (uint32_t(PLL2_XTAL_EN) & 0x1) << 1;
				reg |= (uint32_t(PLL2_REF_2X_EN) & 0x1) << 0;
				break;
			case 0x163:
				reg |= ((uint32_t(PLL2_N_CAL) & 0x30000) >> 16) << 0;
				break;
			case 0x164:
				reg |= ((uint32_t(PLL2_N_CAL) & 0xFF00) >> 8) << 0;
				break;
			case 0x165:
				reg |= (uint32_t(PLL2_N_CAL) & 0xFF) << 0;
				break;
			case 0x166:
				reg |= (uint32_t(PLL2_FCAL_DIS) & 0x1) << 2;
				reg |= ((uint32_t(PLL2_N) & 0x30000) >> 16) << 0;
				break;
			case 0x167:
				reg |= ((uint32_t(PLL2_N) & 0xFF00) >> 8) << 0;
				break;
			case 0x168:
				reg |= (uint32_t(PLL2_N) & 0xFF) << 0;
				break;
			case 0x169:
				reg |= (uint32_t(PLL2_WND_SIZE) & 0x3) << 5;
				reg |= (uint32_t(PLL2_CP_GAIN) & 0x3) << 3;
				reg |= (uint32_t(PLL2_CP_POL) & 0x1) << 2;
				reg |= 1 << 0;
				break;
			case 0x16A:
				reg |= (uint32_t(SYSREF_REQ_EN) & 0x1) << 6;
				reg |= ((uint32_t(PLL2_DLD_CNT) & 0xFF00) >> 8) << 0;
				break;
			case 0x16B:
				reg |= (uint32_t(PLL2_DLD_CNT) & 0xFF) << 0;
				break;
			case 0x16C:
				reg |= (uint32_t(PLL2_LF_R4) & 0x7) << 3;
				reg |= (uint32_t(PLL2_LF_R3) & 0x7) << 0;
				break;
			case 0x16D:
				reg |= (uint32_t(PLL2_LF_C4) & 0xF) << 4;
				reg |= (uint32_t(PLL2_LF_C3) & 0xF) << 0;
				break;
			case 0x16E:
				reg |= (uint32_t(PLL2_LD_MUX) & 0x1F) << 3;
				reg |= (uint32_t(PLL2_LD_TYPE) & 0x7) << 0;
				break;
			case 0x171:
				reg |= 1 << 7;
				reg |= 1 << 5;
				reg |= 1 << 3;
				reg |= 1 << 1;
				break;
			case 0x172:
				reg |= 1 << 1;
				break;
			case 0x173:
				reg |= (uint32_t(PLL2_PRE_PD) & 0x1) << 6;
				reg |= (uint32_t(PLL2_PD) & 0x1) << 5;
				break;
			case 0x174:
				reg |= (uint32_t(VCO1_DIV) & 0x1F) << 0;
				break;
			case 0x17C:
				reg |= (uint32_t(OPT_REG_1) & 0xFF) << 0;
				break;
			case 0x17D:
				reg |= (uint32_t(OPT_REG_2) & 0xFF) << 0;
				break;
			case 0x182:
				reg |= (uint32_t(RB_PLL1_LD_LOST) & 0x1) << 2;
				reg |= (uint32_t(RB_PLL1_LD) & 0x1) << 1;
				reg |= (uint32_t(CLR_PLL1_LD_LOST) & 0x1) << 0;
				break;
			case 0x183:
				reg |= (uint32_t(RB_PLL2_LD_LOST) & 0x1) << 2;
				reg |= (uint32_t(RB_PLL2_LD) & 0x1) << 1;
				reg |= (uint32_t(CLR_PLL2_LD_LOST) & 0x1) << 0;
				break;
			case 0x184:
				reg |= ((uint32_t(RB_DAC_VALUE) & 0x300) >> 8) << 6;
				reg |= (uint32_t(RB_CLKin2_SEL) & 0x1) << 5;
				reg |= (uint32_t(RB_CLKin1_SEL) & 0x1) << 4;
				reg |= (uint32_t(RB_CLKin0_SEL) & 0x1) << 3;
				reg |= (uint32_t(RB_CLKin1_LOS) & 0x1) << 1;
				reg |= (uint32_t(RB_CLKin0_LOS) & 0x1) << 0;
				break;
			case 0x185:
				reg |= (uint32_t(RB_DAC_VALUE) & 0xFF) << 0;
				break;
			case 0x188:
				reg |= (uint32_t(RB_HOLDOVER) & 0x1) << 4;
				break;
			case 0x1FFD:
				reg |= ((uint32_t(SPI_LOCK) & 0xFF0000) >> 16) << 0;
				break;
			case 0x1FFE:
				reg |= ((uint32_t(SPI_LOCK) & 0xFF00) >> 8) << 0;
				break;
			case 0x1FFF:
				reg |= (uint32_t(SPI_LOCK) & 0xFF) << 0;
				break;
		}
		return reg;
	}
	
	void write_reg(uint16_t addr,uint8_t value){
		switch(addr){
			case 0x000:
				RESET = (RESET & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				SPI_3WIRE_DIS = (SPI_3WIRE_DIS & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				break;
			case 0x002:
				POWERDOWN = (POWERDOWN & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x003:
				ID_DEVICE_TYPE = (ID_DEVICE_TYPE & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x004:
				ID_PROD = (ID_PROD & (~0xFF00)) | ((value & (0xFF00 >> 8)) << 8);
				break;
			case 0x005:
				ID_PROD = (ID_PROD & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x006:
				ID_MASKREV = (ID_MASKREV & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x00C:
				ID_VNDR = (ID_VNDR & (~0xFF00)) | ((value & (0xFF00 >> 8)) << 8);
				break;
			case 0x00D:
				ID_VNDR = (ID_VNDR & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x100:
				CLKout0_1_ODL = (CLKout0_1_ODL & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKout0_1_IDL = (CLKout0_1_IDL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout0_DIV = (DCLKout0_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x101:
				DCLKout0_DDLY_CNTH = (DCLKout0_DDLY_CNTH & (~0xF)) | ((value & (0xF << 4)) >> 4);
				DCLKout0_DDLY_CNTL = (DCLKout0_DDLY_CNTL & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x103:
				DCLKout0_ADLY = (DCLKout0_ADLY & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				DCLKout0_ADLY_MUX = (DCLKout0_ADLY_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DCLKout0_MUX = (DCLKout0_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x104:
				DCLKout0_HS = (DCLKout0_HS & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SDCLKout1_MUX = (SDCLKout1_MUX & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SDCLKout1_DDLY = (SDCLKout1_DDLY & (~0xF)) | ((value & (0xF << 1)) >> 1);
				SDCLKout1_HS = (SDCLKout1_HS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x105:
				SDCLKout1_ADLY_EN = (SDCLKout1_ADLY_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SDCLKout1_ADLY = (SDCLKout1_ADLY & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x106:
				DCLKout0_DDLY_PD = (DCLKout0_DDLY_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DCLKout0_HSg_PD = (DCLKout0_HSg_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DCLKout0_ADLYg_PD = (DCLKout0_ADLYg_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout0_ADLY_PD = (DCLKout0_ADLY_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKout0_1_PD = (CLKout0_1_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SDCLKout1_DIS_MODE = (SDCLKout1_DIS_MODE & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				SDCLKout1_PD = (SDCLKout1_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x107:
				SDCLKout1_POL = (SDCLKout1_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKout1_FMT = (CLKout1_FMT & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				DCLKout0_POL = (DCLKout0_POL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKout0_FMT = (CLKout0_FMT & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x108:
				CLKout2_3_ODL = (CLKout2_3_ODL & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKout2_3_IDL = (CLKout2_3_IDL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout2_DIV = (DCLKout2_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x109:
				DCLKout2_DDLY_CNTH = (DCLKout2_DDLY_CNTH & (~0xF)) | ((value & (0xF << 4)) >> 4);
				DCLKout2_DDLY_CNTL = (DCLKout2_DDLY_CNTL & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x10B:
				DCLKout2_ADLY = (DCLKout2_ADLY & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				DCLKout2_ADLY_MUX = (DCLKout2_ADLY_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DCLKout2_MUX = (DCLKout2_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x10C:
				DCLKout2_HS = (DCLKout2_HS & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SDCLKout3_MUX = (SDCLKout3_MUX & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SDCLKout3_DDLY = (SDCLKout3_DDLY & (~0xF)) | ((value & (0xF << 1)) >> 1);
				SDCLKout3_HS = (SDCLKout3_HS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x10D:
				SDCLKout3_ADLY_EN = (SDCLKout3_ADLY_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SDCLKout3_ADLY = (SDCLKout3_ADLY & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x10E:
				DCLKout2_DDLY_PD = (DCLKout2_DDLY_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DCLKout2_HSg_PD = (DCLKout2_HSg_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DCLKout2_ADLYg_PD = (DCLKout2_ADLYg_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout2_ADLY_PD = (DCLKout2_ADLY_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKout2_3_PD = (CLKout2_3_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SDCLKout3_DIS_MODE = (SDCLKout3_DIS_MODE & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				SDCLKout3_PD = (SDCLKout3_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x10F:
				SDCLKout3_POL = (SDCLKout3_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKout3_FMT = (CLKout3_FMT & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				DCLKout2_POL = (DCLKout2_POL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKout2_FMT = (CLKout2_FMT & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x110:
				CLKout4_5_ODL = (CLKout4_5_ODL & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKout4_5_IDL = (CLKout4_5_IDL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout4_DIV = (DCLKout4_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x111:
				DCLKout4_DDLY_CNTH = (DCLKout4_DDLY_CNTH & (~0xF)) | ((value & (0xF << 4)) >> 4);
				DCLKout4_DDLY_CNTL = (DCLKout4_DDLY_CNTL & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x113:
				DCLKout4_ADLY = (DCLKout4_ADLY & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				DCLKout4_ADLY_MUX = (DCLKout4_ADLY_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DCLKout4_MUX = (DCLKout4_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x114:
				DCLKout4_HS = (DCLKout4_HS & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SDCLKout5_MUX = (SDCLKout5_MUX & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SDCLKout5_DDLY = (SDCLKout5_DDLY & (~0xF)) | ((value & (0xF << 1)) >> 1);
				SDCLKout5_HS = (SDCLKout5_HS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x115:
				SDCLKout5_ADLY_EN = (SDCLKout5_ADLY_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SDCLKout5_ADLY = (SDCLKout5_ADLY & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x116:
				DCLKout4_DDLY_PD = (DCLKout4_DDLY_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DCLKout4_HSg_PD = (DCLKout4_HSg_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DCLKout4_ADLYg_PD = (DCLKout4_ADLYg_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout4_ADLY_PD = (DCLKout4_ADLY_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKout4_5_PD = (CLKout4_5_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SDCLKout5_DIS_MODE = (SDCLKout5_DIS_MODE & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				SDCLKout5_PD = (SDCLKout5_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x117:
				SDCLKout5_POL = (SDCLKout5_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKout5_FMT = (CLKout5_FMT & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				DCLKout4_POL = (DCLKout4_POL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKout4_FMT = (CLKout4_FMT & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x118:
				CLKout6_7_ODL = (CLKout6_7_ODL & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKout6_8_IDL = (CLKout6_8_IDL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout6_DIV = (DCLKout6_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x119:
				DCLKout6_DDLY_CNTH = (DCLKout6_DDLY_CNTH & (~0xF)) | ((value & (0xF << 4)) >> 4);
				DCLKout6_DDLY_CNTL = (DCLKout6_DDLY_CNTL & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x11B:
				DCLKout6_ADLY = (DCLKout6_ADLY & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				DCLKout6_ADLY_MUX = (DCLKout6_ADLY_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DCLKout6_MUX = (DCLKout6_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x11C:
				DCLKout6_HS = (DCLKout6_HS & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SDCLKout7_MUX = (SDCLKout7_MUX & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SDCLKout7_DDLY = (SDCLKout7_DDLY & (~0xF)) | ((value & (0xF << 1)) >> 1);
				SDCLKout7_HS = (SDCLKout7_HS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x11D:
				SDCLKout7_ADLY_EN = (SDCLKout7_ADLY_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SDCLKout7_ADLY = (SDCLKout7_ADLY & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x11E:
				DCLKout6_DDLY_PD = (DCLKout6_DDLY_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DCLKout6_HSg_PD = (DCLKout6_HSg_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DCLKout6_ADLYg_PD = (DCLKout6_ADLYg_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout6_ADLY_PD = (DCLKout6_ADLY_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKout6_7_PD = (CLKout6_7_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SDCLKout7_DIS_MODE = (SDCLKout7_DIS_MODE & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				SDCLKout7_PD = (SDCLKout7_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x11F:
				SDCLKout7_POL = (SDCLKout7_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKout7_FMT = (CLKout7_FMT & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				DCLKout6_POL = (DCLKout6_POL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKout6_FMT = (CLKout6_FMT & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x120:
				CLKout8_9_ODL = (CLKout8_9_ODL & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKout8_9_IDL = (CLKout8_9_IDL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout8_DIV = (DCLKout8_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x121:
				DCLKout8_DDLY_CNTH = (DCLKout8_DDLY_CNTH & (~0xF)) | ((value & (0xF << 4)) >> 4);
				DCLKout8_DDLY_CNTL = (DCLKout8_DDLY_CNTL & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x123:
				DCLKout8_ADLY = (DCLKout8_ADLY & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				DCLKout8_ADLY_MUX = (DCLKout8_ADLY_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DCLKout8_MUX = (DCLKout8_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x124:
				DCLKout8_HS = (DCLKout8_HS & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SDCLKout9_MUX = (SDCLKout9_MUX & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SDCLKout9_DDLY = (SDCLKout9_DDLY & (~0xF)) | ((value & (0xF << 1)) >> 1);
				SDCLKout9_HS = (SDCLKout9_HS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x125:
				SDCLKout9_ADLY_EN = (SDCLKout9_ADLY_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SDCLKout9_ADLY = (SDCLKout9_ADLY & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x126:
				DCLKout8_DDLY_PD = (DCLKout8_DDLY_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DCLKout8_HSg_PD = (DCLKout8_HSg_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DCLKout8_ADLYg_PD = (DCLKout8_ADLYg_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout8_ADLY_PD = (DCLKout8_ADLY_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKout8_9_PD = (CLKout8_9_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SDCLKout9_DIS_MODE = (SDCLKout9_DIS_MODE & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				SDCLKout9_PD = (SDCLKout9_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x127:
				SDCLKout9_POL = (SDCLKout9_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKout9_FMT = (CLKout9_FMT & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				DCLKout8_POL = (DCLKout8_POL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKout8_FMT = (CLKout8_FMT & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x128:
				CLKout10_11_ODL = (CLKout10_11_ODL & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKout10_11_IDL = (CLKout10_11_IDL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout10_DIV = (DCLKout10_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x129:
				DCLKout10_DDLY_CNTH = (DCLKout10_DDLY_CNTH & (~0xF)) | ((value & (0xF << 4)) >> 4);
				DCLKout10_DDLY_CNTL = (DCLKout10_DDLY_CNTL & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x12B:
				DCLKout10_ADLY = (DCLKout10_ADLY & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				DCLKout10_ADLY_MUX = (DCLKout10_ADLY_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DCLKout10_MUX = (DCLKout10_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x12C:
				DCLKout10_HS = (DCLKout10_HS & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SDCLKout11_MUX = (SDCLKout11_MUX & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SDCLKout11_DDLY = (SDCLKout11_DDLY & (~0xF)) | ((value & (0xF << 1)) >> 1);
				SDCLKout11_HS = (SDCLKout11_HS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x12D:
				SDCKLout11_ADLY_EN = (SDCKLout11_ADLY_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SDCLKout11_ADLY = (SDCLKout11_ADLY & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x12E:
				DCLKout10_DDLY_PD = (DCLKout10_DDLY_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DCLKout10_HSg_PD = (DCLKout10_HSg_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DLCLKout10_ADLYg_PD = (DLCLKout10_ADLYg_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout10_ADLY_PD = (DCLKout10_ADLY_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKout10_11_PD = (CLKout10_11_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SDCLKout11_DIS_MODE = (SDCLKout11_DIS_MODE & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				SDCLKout11_PD = (SDCLKout11_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x12F:
				SDCLKout11_POL = (SDCLKout11_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKout11_FMT = (CLKout11_FMT & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				DCLKout10_POL = (DCLKout10_POL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKout10_FMT = (CLKout10_FMT & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x130:
				CLKout12_13_ODL = (CLKout12_13_ODL & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKout12_13_IDL = (CLKout12_13_IDL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout12_DIV = (DCLKout12_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x131:
				DCLKout12_DDLY_CNTH = (DCLKout12_DDLY_CNTH & (~0xF)) | ((value & (0xF << 4)) >> 4);
				DCLKout12_DDLY_CNTL = (DCLKout12_DDLY_CNTL & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x133:
				DCLKout12_ADLY = (DCLKout12_ADLY & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				DCLKout12_ADLY_MUX = (DCLKout12_ADLY_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DCLKout12_MUX = (DCLKout12_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x134:
				DCLKout12_HS = (DCLKout12_HS & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SDCLKout13_MUX = (SDCLKout13_MUX & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SDCLKout13_DDLY = (SDCLKout13_DDLY & (~0xF)) | ((value & (0xF << 1)) >> 1);
				SDCLKout13_HS = (SDCLKout13_HS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x135:
				SDCLKout13_ADLY_EN = (SDCLKout13_ADLY_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SDCLKout13_ADLY = (SDCLKout13_ADLY & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x136:
				DCLKout12_DDLY_PD = (DCLKout12_DDLY_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DCLKout12_HSg_PD = (DCLKout12_HSg_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DCLKout12_ADLYg_PD = (DCLKout12_ADLYg_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DCLKout12_ADLY_PD = (DCLKout12_ADLY_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKout12_13_PD = (CLKout12_13_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SDCLKout13_DIS_MODE = (SDCLKout13_DIS_MODE & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				SDCLKout13_PD = (SDCLKout13_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x137:
				SDCLKout13_POL = (SDCLKout13_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKout13_FMT = (CLKout13_FMT & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				DCLKout12_POL = (DCLKout12_POL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKout12_FMT = (CLKout12_FMT & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x138:
				VCO_MUX = (VCO_MUX & (~0x3)) | ((value & (0x3 << 5)) >> 5);
				OSCout_MUX = (OSCout_MUX & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				OSCout_FMT = (OSCout_FMT & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x139:
				SYSREF_CLKin0_MUX = (SYSREF_CLKin0_MUX & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				SYSREF_MUX = (SYSREF_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x13A:
				SYSREF_DIV = (SYSREF_DIV & (~0x1F00)) | ((value & (0x1F00 >> 8)) << 8);
				break;
			case 0x13B:
				SYSREF_DIV = (SYSREF_DIV & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x13C:
				SYSREF_DDLY = (SYSREF_DDLY & (~0x1F00)) | ((value & (0x1F00 >> 8)) << 8);
				break;
			case 0x13D:
				SYSREF_DDLY = (SYSREF_DDLY & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x13E:
				SYSREF_PULSE_CNT = (SYSREF_PULSE_CNT & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x13F:
				PLL2_NCLK_MUX = (PLL2_NCLK_MUX & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				PLL1_NCLK_MUX = (PLL1_NCLK_MUX & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				FB_MUX = (FB_MUX & (~0x3)) | ((value & (0x3 << 1)) >> 1);
				FB_MUX_EN = (FB_MUX_EN & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x140:
				PLL1_PD = (PLL1_PD & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				VCO_LDO_PD = (VCO_LDO_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				VCO_PD = (VCO_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				OSCin_PD = (OSCin_PD & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SYSREF_GBL_PD = (SYSREF_GBL_PD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SYSREF_PD = (SYSREF_PD & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				SYSREF_DDLY_PD = (SYSREF_DDLY_PD & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				SYSREF_PLSR_PD = (SYSREF_PLSR_PD & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x141:
				DDLYd_SYSREF_EN = (DDLYd_SYSREF_EN & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				DDLYd12_EN = (DDLYd12_EN & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				DDLYd10_EN = (DDLYd10_EN & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				DDLYd7_EN = (DDLYd7_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				DDLYd6_EN = (DDLYd6_EN & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				DDLYd4_EN = (DDLYd4_EN & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				DDLYd2_EN = (DDLYd2_EN & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				DDLYd0_EN = (DDLYd0_EN & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x142:
				DDLYd_STEP_CNT = (DDLYd_STEP_CNT & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x143:
				SYSREF_DDLY_CLR = (SYSREF_DDLY_CLR & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				SYNC_1SHOT_EN = (SYNC_1SHOT_EN & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SYNC_POL = (SYNC_POL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SYNC_EN = (SYNC_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SYNC_PLL2_DLD = (SYNC_PLL2_DLD & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SYNC_PLL1_DLD = (SYNC_PLL1_DLD & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				SYNC_MODE = (SYNC_MODE & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x144:
				SYNC_DISSYSREF = (SYNC_DISSYSREF & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				SYNC_DIS12 = (SYNC_DIS12 & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				SYNC_DIS10 = (SYNC_DIS10 & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				SYNC_DIS8 = (SYNC_DIS8 & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				SYNC_DIS6 = (SYNC_DIS6 & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				SYNC_DIS4 = (SYNC_DIS4 & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				SYNC_DIS2 = (SYNC_DIS2 & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				SYNC_DIS0 = (SYNC_DIS0 & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x145:
				break;
			case 0x146:
				CLKin2_EN = (CLKin2_EN & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				CLKin1_EN = (CLKin1_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				CLKin0_EN = (CLKin0_EN & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				CLKin2_TYPE = (CLKin2_TYPE & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				CLKin1_TYPE = (CLKin1_TYPE & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				CLKin0_TYPE = (CLKin0_TYPE & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x147:
				CLKin_SEL_POL = (CLKin_SEL_POL & (~0x1)) | ((value & (0x1 << 7)) >> 7);
				CLKin_SEL_MODE = (CLKin_SEL_MODE & (~0x7)) | ((value & (0x7 << 4)) >> 4);
				CLKin1_OUT_MUX = (CLKin1_OUT_MUX & (~0x3)) | ((value & (0x3 << 2)) >> 2);
				CLKin0_OUT_MUX = (CLKin0_OUT_MUX & (~0x3)) | ((value & (0x3 << 0)) >> 0);
				break;
			case 0x148:
				CLKin_SEL0_MUX = (CLKin_SEL0_MUX & (~0x7)) | ((value & (0x7 << 3)) >> 3);
				CLKin_SEL0_TYPE = (CLKin_SEL0_TYPE & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x149:
				SDIO_RDBK_TYPE = (SDIO_RDBK_TYPE & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				CLKin_SEL1_MUX = (CLKin_SEL1_MUX & (~0x7)) | ((value & (0x7 << 3)) >> 3);
				CLKin_SEL1_TYPE = (CLKin_SEL1_TYPE & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x14A:
				RESET_MUX = (RESET_MUX & (~0x7)) | ((value & (0x7 << 3)) >> 3);
				RESET_TYPE = (RESET_TYPE & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x14B:
				LOS_TIMEOUT = (LOS_TIMEOUT & (~0x3)) | ((value & (0x3 << 6)) >> 6);
				LOS_EN = (LOS_EN & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				TRACK_EN = (TRACK_EN & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				HOLDOVER_FORCE = (HOLDOVER_FORCE & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				MAN_DAC_EN = (MAN_DAC_EN & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				MAN_DAC = (MAN_DAC & (~0x300)) | ((value & (0x300 >> 8)) << 8);
				break;
			case 0x14C:
				MAN_DAC = (MAN_DAC & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x14D:
				DAC_TRIP_LOW = (DAC_TRIP_LOW & (~0x3F)) | ((value & (0x3F << 0)) >> 0);
				break;
			case 0x14E:
				DAC_CLK_MULT = (DAC_CLK_MULT & (~0x3)) | ((value & (0x3 << 6)) >> 6);
				DAC_TRIP_HIGH = (DAC_TRIP_HIGH & (~0x3F)) | ((value & (0x3F << 0)) >> 0);
				break;
			case 0x14F:
				DAC_CLK_CNTR = (DAC_CLK_CNTR & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x150:
				CLKin_OVERRIDE = (CLKin_OVERRIDE & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				HOLDOVER_PLL1_DET = (HOLDOVER_PLL1_DET & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				HOLDOVER_LOS_DET = (HOLDOVER_LOS_DET & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				HOLDOVER_VTUNE_DET = (HOLDOVER_VTUNE_DET & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				HOLDOVER_HITLESS_SWITCH = (HOLDOVER_HITLESS_SWITCH & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				HOLDOVER_EN = (HOLDOVER_EN & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x151:
				HOLDOVER_DLD_CNT = (HOLDOVER_DLD_CNT & (~0x3F00)) | ((value & (0x3F00 >> 8)) << 8);
				break;
			case 0x152:
				HOLDOVER_DLD_CNT = (HOLDOVER_DLD_CNT & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x153:
				CLKin0_R = (CLKin0_R & (~0x3F00)) | ((value & (0x3F00 >> 8)) << 8);
				break;
			case 0x154:
				CLKin0_R = (CLKin0_R & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x155:
				CLKin1_R = (CLKin1_R & (~0x3F00)) | ((value & (0x3F00 >> 8)) << 8);
				break;
			case 0x156:
				CLKin1_R = (CLKin1_R & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x157:
				CLKin2_R = (CLKin2_R & (~0x3F00)) | ((value & (0x3F00 >> 8)) << 8);
				break;
			case 0x158:
				CLKin2_R = (CLKin2_R & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x159:
				PLL1_N = (PLL1_N & (~0x3F00)) | ((value & (0x3F00 >> 8)) << 8);
				break;
			case 0x15A:
				PLL1_N = (PLL1_N & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x15B:
				PLL1_WND_SIZE = (PLL1_WND_SIZE & (~0x3)) | ((value & (0x3 << 6)) >> 6);
				PLL1_CP_TRI = (PLL1_CP_TRI & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				PLL1_CP_POL = (PLL1_CP_POL & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				PLL1_CP_GAIN = (PLL1_CP_GAIN & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x15C:
				PLL1_DLD_CNT = (PLL1_DLD_CNT & (~0x3F00)) | ((value & (0x3F00 >> 8)) << 8);
				break;
			case 0x15D:
				PLL1_DLD_CNT = (PLL1_DLD_CNT & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x15E:
				PLL1_R_DLY = (PLL1_R_DLY & (~0x7)) | ((value & (0x7 << 3)) >> 3);
				PLL1_N_DLY = (PLL1_N_DLY & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x15F:
				PLL1_LD_MUX = (PLL1_LD_MUX & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				PLL1_LD_TYPE = (PLL1_LD_TYPE & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x160:
				PLL2_R = (PLL2_R & (~0xF00)) | ((value & (0xF00 >> 8)) << 8);
				break;
			case 0x161:
				PLL2_R = (PLL2_R & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x162:
				PLL2_P = (PLL2_P & (~0x7)) | ((value & (0x7 << 5)) >> 5);
				OSCin_FREQ = (OSCin_FREQ & (~0x7)) | ((value & (0x7 << 2)) >> 2);
				PLL2_XTAL_EN = (PLL2_XTAL_EN & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				PLL2_REF_2X_EN = (PLL2_REF_2X_EN & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x163:
				PLL2_N_CAL = (PLL2_N_CAL & (~0x30000)) | ((value & (0x30000 >> 16)) << 16);
				break;
			case 0x164:
				PLL2_N_CAL = (PLL2_N_CAL & (~0xFF00)) | ((value & (0xFF00 >> 8)) << 8);
				break;
			case 0x165:
				PLL2_N_CAL = (PLL2_N_CAL & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x166:
				PLL2_FCAL_DIS = (PLL2_FCAL_DIS & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				PLL2_N = (PLL2_N & (~0x30000)) | ((value & (0x30000 >> 16)) << 16);
				break;
			case 0x167:
				PLL2_N = (PLL2_N & (~0xFF00)) | ((value & (0xFF00 >> 8)) << 8);
				break;
			case 0x168:
				PLL2_N = (PLL2_N & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x169:
				PLL2_WND_SIZE = (PLL2_WND_SIZE & (~0x3)) | ((value & (0x3 << 5)) >> 5);
				PLL2_CP_GAIN = (PLL2_CP_GAIN & (~0x3)) | ((value & (0x3 << 3)) >> 3);
				PLL2_CP_POL = (PLL2_CP_POL & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				break;
			case 0x16A:
				SYSREF_REQ_EN = (SYSREF_REQ_EN & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				PLL2_DLD_CNT = (PLL2_DLD_CNT & (~0xFF00)) | ((value & (0xFF00 >> 8)) << 8);
				break;
			case 0x16B:
				PLL2_DLD_CNT = (PLL2_DLD_CNT & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x16C:
				PLL2_LF_R4 = (PLL2_LF_R4 & (~0x7)) | ((value & (0x7 << 3)) >> 3);
				PLL2_LF_R3 = (PLL2_LF_R3 & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x16D:
				PLL2_LF_C4 = (PLL2_LF_C4 & (~0xF)) | ((value & (0xF << 4)) >> 4);
				PLL2_LF_C3 = (PLL2_LF_C3 & (~0xF)) | ((value & (0xF << 0)) >> 0);
				break;
			case 0x16E:
				PLL2_LD_MUX = (PLL2_LD_MUX & (~0x1F)) | ((value & (0x1F << 3)) >> 3);
				PLL2_LD_TYPE = (PLL2_LD_TYPE & (~0x7)) | ((value & (0x7 << 0)) >> 0);
				break;
			case 0x171:
				break;
			case 0x172:
				break;
			case 0x173:
				PLL2_PRE_PD = (PLL2_PRE_PD & (~0x1)) | ((value & (0x1 << 6)) >> 6);
				PLL2_PD = (PLL2_PD & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				break;
			case 0x174:
				VCO1_DIV = (VCO1_DIV & (~0x1F)) | ((value & (0x1F << 0)) >> 0);
				break;
			case 0x17C:
				OPT_REG_1 = (OPT_REG_1 & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x17D:
				OPT_REG_2 = (OPT_REG_2 & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x182:
				RB_PLL1_LD_LOST = (RB_PLL1_LD_LOST & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				RB_PLL1_LD = (RB_PLL1_LD & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				CLR_PLL1_LD_LOST = (CLR_PLL1_LD_LOST & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x183:
				RB_PLL2_LD_LOST = (RB_PLL2_LD_LOST & (~0x1)) | ((value & (0x1 << 2)) >> 2);
				RB_PLL2_LD = (RB_PLL2_LD & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				CLR_PLL2_LD_LOST = (CLR_PLL2_LD_LOST & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x184:
				RB_DAC_VALUE = (RB_DAC_VALUE & (~0x300)) | ((value & (0x300 >> 2)) << 2);
				RB_CLKin2_SEL = (RB_CLKin2_SEL & (~0x1)) | ((value & (0x1 << 5)) >> 5);
				RB_CLKin1_SEL = (RB_CLKin1_SEL & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				RB_CLKin0_SEL = (RB_CLKin0_SEL & (~0x1)) | ((value & (0x1 << 3)) >> 3);
				RB_CLKin1_LOS = (RB_CLKin1_LOS & (~0x1)) | ((value & (0x1 << 1)) >> 1);
				RB_CLKin0_LOS = (RB_CLKin0_LOS & (~0x1)) | ((value & (0x1 << 0)) >> 0);
				break;
			case 0x185:
				RB_DAC_VALUE = (RB_DAC_VALUE & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
			case 0x188:
				RB_HOLDOVER = (RB_HOLDOVER & (~0x1)) | ((value & (0x1 << 4)) >> 4);
				break;
			case 0x1FFD:
				SPI_LOCK = (SPI_LOCK & (~0xFF0000)) | ((value & (0xFF0000 >> 16)) << 16);
				break;
			case 0x1FFE:
				SPI_LOCK = (SPI_LOCK & (~0xFF00)) | ((value & (0xFF00 >> 8)) << 8);
				break;
			case 0x1FFF:
				SPI_LOCK = (SPI_LOCK & (~0xFF)) | ((value & (0xFF << 0)) >> 0);
				break;
		}
		return;
	}
	
	void save_state(void){
		if(_state == nullptr){
			return; // @TODO: Add some proper error detection here, _state smart pointer should ALWAYS be initialized properly
		}
		_state->RESET = this->RESET;
		_state->SPI_3WIRE_DIS = this->SPI_3WIRE_DIS;
		_state->POWERDOWN = this->POWERDOWN;
		_state->ID_DEVICE_TYPE = this->ID_DEVICE_TYPE;
		_state->ID_PROD = this->ID_PROD;
		_state->ID_MASKREV = this->ID_MASKREV;
		_state->ID_VNDR = this->ID_VNDR;
		_state->CLKout0_1_ODL = this->CLKout0_1_ODL;
		_state->CLKout0_1_IDL = this->CLKout0_1_IDL;
		_state->DCLKout0_DIV = this->DCLKout0_DIV;
		_state->DCLKout0_DDLY_CNTH = this->DCLKout0_DDLY_CNTH;
		_state->DCLKout0_DDLY_CNTL = this->DCLKout0_DDLY_CNTL;
		_state->DCLKout0_ADLY = this->DCLKout0_ADLY;
		_state->DCLKout0_ADLY_MUX = this->DCLKout0_ADLY_MUX;
		_state->DCLKout0_MUX = this->DCLKout0_MUX;
		_state->DCLKout0_HS = this->DCLKout0_HS;
		_state->SDCLKout1_MUX = this->SDCLKout1_MUX;
		_state->SDCLKout1_DDLY = this->SDCLKout1_DDLY;
		_state->SDCLKout1_HS = this->SDCLKout1_HS;
		_state->SDCLKout1_ADLY_EN = this->SDCLKout1_ADLY_EN;
		_state->SDCLKout1_ADLY = this->SDCLKout1_ADLY;
		_state->DCLKout0_DDLY_PD = this->DCLKout0_DDLY_PD;
		_state->DCLKout0_HSg_PD = this->DCLKout0_HSg_PD;
		_state->DCLKout0_ADLYg_PD = this->DCLKout0_ADLYg_PD;
		_state->DCLKout0_ADLY_PD = this->DCLKout0_ADLY_PD;
		_state->CLKout0_1_PD = this->CLKout0_1_PD;
		_state->SDCLKout1_DIS_MODE = this->SDCLKout1_DIS_MODE;
		_state->SDCLKout1_PD = this->SDCLKout1_PD;
		_state->SDCLKout1_POL = this->SDCLKout1_POL;
		_state->CLKout1_FMT = this->CLKout1_FMT;
		_state->DCLKout0_POL = this->DCLKout0_POL;
		_state->CLKout0_FMT = this->CLKout0_FMT;
		_state->CLKout2_3_ODL = this->CLKout2_3_ODL;
		_state->CLKout2_3_IDL = this->CLKout2_3_IDL;
		_state->DCLKout2_DIV = this->DCLKout2_DIV;
		_state->DCLKout2_DDLY_CNTH = this->DCLKout2_DDLY_CNTH;
		_state->DCLKout2_DDLY_CNTL = this->DCLKout2_DDLY_CNTL;
		_state->DCLKout2_ADLY = this->DCLKout2_ADLY;
		_state->DCLKout2_ADLY_MUX = this->DCLKout2_ADLY_MUX;
		_state->DCLKout2_MUX = this->DCLKout2_MUX;
		_state->DCLKout2_HS = this->DCLKout2_HS;
		_state->SDCLKout3_MUX = this->SDCLKout3_MUX;
		_state->SDCLKout3_DDLY = this->SDCLKout3_DDLY;
		_state->SDCLKout3_HS = this->SDCLKout3_HS;
		_state->SDCLKout3_ADLY_EN = this->SDCLKout3_ADLY_EN;
		_state->SDCLKout3_ADLY = this->SDCLKout3_ADLY;
		_state->DCLKout2_DDLY_PD = this->DCLKout2_DDLY_PD;
		_state->DCLKout2_HSg_PD = this->DCLKout2_HSg_PD;
		_state->DCLKout2_ADLYg_PD = this->DCLKout2_ADLYg_PD;
		_state->DCLKout2_ADLY_PD = this->DCLKout2_ADLY_PD;
		_state->CLKout2_3_PD = this->CLKout2_3_PD;
		_state->SDCLKout3_DIS_MODE = this->SDCLKout3_DIS_MODE;
		_state->SDCLKout3_PD = this->SDCLKout3_PD;
		_state->SDCLKout3_POL = this->SDCLKout3_POL;
		_state->CLKout3_FMT = this->CLKout3_FMT;
		_state->DCLKout2_POL = this->DCLKout2_POL;
		_state->CLKout2_FMT = this->CLKout2_FMT;
		_state->CLKout4_5_ODL = this->CLKout4_5_ODL;
		_state->CLKout4_5_IDL = this->CLKout4_5_IDL;
		_state->DCLKout4_DIV = this->DCLKout4_DIV;
		_state->DCLKout4_DDLY_CNTH = this->DCLKout4_DDLY_CNTH;
		_state->DCLKout4_DDLY_CNTL = this->DCLKout4_DDLY_CNTL;
		_state->DCLKout4_ADLY = this->DCLKout4_ADLY;
		_state->DCLKout4_ADLY_MUX = this->DCLKout4_ADLY_MUX;
		_state->DCLKout4_MUX = this->DCLKout4_MUX;
		_state->DCLKout4_HS = this->DCLKout4_HS;
		_state->SDCLKout5_MUX = this->SDCLKout5_MUX;
		_state->SDCLKout5_DDLY = this->SDCLKout5_DDLY;
		_state->SDCLKout5_HS = this->SDCLKout5_HS;
		_state->SDCLKout5_ADLY_EN = this->SDCLKout5_ADLY_EN;
		_state->SDCLKout5_ADLY = this->SDCLKout5_ADLY;
		_state->DCLKout4_DDLY_PD = this->DCLKout4_DDLY_PD;
		_state->DCLKout4_HSg_PD = this->DCLKout4_HSg_PD;
		_state->DCLKout4_ADLYg_PD = this->DCLKout4_ADLYg_PD;
		_state->DCLKout4_ADLY_PD = this->DCLKout4_ADLY_PD;
		_state->CLKout4_5_PD = this->CLKout4_5_PD;
		_state->SDCLKout5_DIS_MODE = this->SDCLKout5_DIS_MODE;
		_state->SDCLKout5_PD = this->SDCLKout5_PD;
		_state->SDCLKout5_POL = this->SDCLKout5_POL;
		_state->CLKout5_FMT = this->CLKout5_FMT;
		_state->DCLKout4_POL = this->DCLKout4_POL;
		_state->CLKout4_FMT = this->CLKout4_FMT;
		_state->CLKout6_7_ODL = this->CLKout6_7_ODL;
		_state->CLKout6_8_IDL = this->CLKout6_8_IDL;
		_state->DCLKout6_DIV = this->DCLKout6_DIV;
		_state->DCLKout6_DDLY_CNTH = this->DCLKout6_DDLY_CNTH;
		_state->DCLKout6_DDLY_CNTL = this->DCLKout6_DDLY_CNTL;
		_state->DCLKout6_ADLY = this->DCLKout6_ADLY;
		_state->DCLKout6_ADLY_MUX = this->DCLKout6_ADLY_MUX;
		_state->DCLKout6_MUX = this->DCLKout6_MUX;
		_state->DCLKout6_HS = this->DCLKout6_HS;
		_state->SDCLKout7_MUX = this->SDCLKout7_MUX;
		_state->SDCLKout7_DDLY = this->SDCLKout7_DDLY;
		_state->SDCLKout7_HS = this->SDCLKout7_HS;
		_state->SDCLKout7_ADLY_EN = this->SDCLKout7_ADLY_EN;
		_state->SDCLKout7_ADLY = this->SDCLKout7_ADLY;
		_state->DCLKout6_DDLY_PD = this->DCLKout6_DDLY_PD;
		_state->DCLKout6_HSg_PD = this->DCLKout6_HSg_PD;
		_state->DCLKout6_ADLYg_PD = this->DCLKout6_ADLYg_PD;
		_state->DCLKout6_ADLY_PD = this->DCLKout6_ADLY_PD;
		_state->CLKout6_7_PD = this->CLKout6_7_PD;
		_state->SDCLKout7_DIS_MODE = this->SDCLKout7_DIS_MODE;
		_state->SDCLKout7_PD = this->SDCLKout7_PD;
		_state->SDCLKout7_POL = this->SDCLKout7_POL;
		_state->CLKout7_FMT = this->CLKout7_FMT;
		_state->DCLKout6_POL = this->DCLKout6_POL;
		_state->CLKout6_FMT = this->CLKout6_FMT;
		_state->CLKout8_9_ODL = this->CLKout8_9_ODL;
		_state->CLKout8_9_IDL = this->CLKout8_9_IDL;
		_state->DCLKout8_DIV = this->DCLKout8_DIV;
		_state->DCLKout8_DDLY_CNTH = this->DCLKout8_DDLY_CNTH;
		_state->DCLKout8_DDLY_CNTL = this->DCLKout8_DDLY_CNTL;
		_state->DCLKout8_ADLY = this->DCLKout8_ADLY;
		_state->DCLKout8_ADLY_MUX = this->DCLKout8_ADLY_MUX;
		_state->DCLKout8_MUX = this->DCLKout8_MUX;
		_state->DCLKout8_HS = this->DCLKout8_HS;
		_state->SDCLKout9_MUX = this->SDCLKout9_MUX;
		_state->SDCLKout9_DDLY = this->SDCLKout9_DDLY;
		_state->SDCLKout9_HS = this->SDCLKout9_HS;
		_state->SDCLKout9_ADLY_EN = this->SDCLKout9_ADLY_EN;
		_state->SDCLKout9_ADLY = this->SDCLKout9_ADLY;
		_state->DCLKout8_DDLY_PD = this->DCLKout8_DDLY_PD;
		_state->DCLKout8_HSg_PD = this->DCLKout8_HSg_PD;
		_state->DCLKout8_ADLYg_PD = this->DCLKout8_ADLYg_PD;
		_state->DCLKout8_ADLY_PD = this->DCLKout8_ADLY_PD;
		_state->CLKout8_9_PD = this->CLKout8_9_PD;
		_state->SDCLKout9_DIS_MODE = this->SDCLKout9_DIS_MODE;
		_state->SDCLKout9_PD = this->SDCLKout9_PD;
		_state->SDCLKout9_POL = this->SDCLKout9_POL;
		_state->CLKout9_FMT = this->CLKout9_FMT;
		_state->DCLKout8_POL = this->DCLKout8_POL;
		_state->CLKout8_FMT = this->CLKout8_FMT;
		_state->CLKout10_11_ODL = this->CLKout10_11_ODL;
		_state->CLKout10_11_IDL = this->CLKout10_11_IDL;
		_state->DCLKout10_DIV = this->DCLKout10_DIV;
		_state->DCLKout10_DDLY_CNTH = this->DCLKout10_DDLY_CNTH;
		_state->DCLKout10_DDLY_CNTL = this->DCLKout10_DDLY_CNTL;
		_state->DCLKout10_ADLY = this->DCLKout10_ADLY;
		_state->DCLKout10_ADLY_MUX = this->DCLKout10_ADLY_MUX;
		_state->DCLKout10_MUX = this->DCLKout10_MUX;
		_state->DCLKout10_HS = this->DCLKout10_HS;
		_state->SDCLKout11_MUX = this->SDCLKout11_MUX;
		_state->SDCLKout11_DDLY = this->SDCLKout11_DDLY;
		_state->SDCLKout11_HS = this->SDCLKout11_HS;
		_state->SDCKLout11_ADLY_EN = this->SDCKLout11_ADLY_EN;
		_state->SDCLKout11_ADLY = this->SDCLKout11_ADLY;
		_state->DCLKout10_DDLY_PD = this->DCLKout10_DDLY_PD;
		_state->DCLKout10_HSg_PD = this->DCLKout10_HSg_PD;
		_state->DLCLKout10_ADLYg_PD = this->DLCLKout10_ADLYg_PD;
		_state->DCLKout10_ADLY_PD = this->DCLKout10_ADLY_PD;
		_state->CLKout10_11_PD = this->CLKout10_11_PD;
		_state->SDCLKout11_DIS_MODE = this->SDCLKout11_DIS_MODE;
		_state->SDCLKout11_PD = this->SDCLKout11_PD;
		_state->SDCLKout11_POL = this->SDCLKout11_POL;
		_state->CLKout11_FMT = this->CLKout11_FMT;
		_state->DCLKout10_POL = this->DCLKout10_POL;
		_state->CLKout10_FMT = this->CLKout10_FMT;
		_state->CLKout12_13_ODL = this->CLKout12_13_ODL;
		_state->CLKout12_13_IDL = this->CLKout12_13_IDL;
		_state->DCLKout12_DIV = this->DCLKout12_DIV;
		_state->DCLKout12_DDLY_CNTH = this->DCLKout12_DDLY_CNTH;
		_state->DCLKout12_DDLY_CNTL = this->DCLKout12_DDLY_CNTL;
		_state->DCLKout12_ADLY = this->DCLKout12_ADLY;
		_state->DCLKout12_ADLY_MUX = this->DCLKout12_ADLY_MUX;
		_state->DCLKout12_MUX = this->DCLKout12_MUX;
		_state->DCLKout12_HS = this->DCLKout12_HS;
		_state->SDCLKout13_MUX = this->SDCLKout13_MUX;
		_state->SDCLKout13_DDLY = this->SDCLKout13_DDLY;
		_state->SDCLKout13_HS = this->SDCLKout13_HS;
		_state->SDCLKout13_ADLY_EN = this->SDCLKout13_ADLY_EN;
		_state->SDCLKout13_ADLY = this->SDCLKout13_ADLY;
		_state->DCLKout12_DDLY_PD = this->DCLKout12_DDLY_PD;
		_state->DCLKout12_HSg_PD = this->DCLKout12_HSg_PD;
		_state->DCLKout12_ADLYg_PD = this->DCLKout12_ADLYg_PD;
		_state->DCLKout12_ADLY_PD = this->DCLKout12_ADLY_PD;
		_state->CLKout12_13_PD = this->CLKout12_13_PD;
		_state->SDCLKout13_DIS_MODE = this->SDCLKout13_DIS_MODE;
		_state->SDCLKout13_PD = this->SDCLKout13_PD;
		_state->SDCLKout13_POL = this->SDCLKout13_POL;
		_state->CLKout13_FMT = this->CLKout13_FMT;
		_state->DCLKout12_POL = this->DCLKout12_POL;
		_state->CLKout12_FMT = this->CLKout12_FMT;
		_state->VCO_MUX = this->VCO_MUX;
		_state->OSCout_MUX = this->OSCout_MUX;
		_state->OSCout_FMT = this->OSCout_FMT;
		_state->SYSREF_CLKin0_MUX = this->SYSREF_CLKin0_MUX;
		_state->SYSREF_MUX = this->SYSREF_MUX;
		_state->SYSREF_DIV = this->SYSREF_DIV;
		_state->SYSREF_DDLY = this->SYSREF_DDLY;
		_state->SYSREF_PULSE_CNT = this->SYSREF_PULSE_CNT;
		_state->PLL2_NCLK_MUX = this->PLL2_NCLK_MUX;
		_state->PLL1_NCLK_MUX = this->PLL1_NCLK_MUX;
		_state->FB_MUX = this->FB_MUX;
		_state->FB_MUX_EN = this->FB_MUX_EN;
		_state->PLL1_PD = this->PLL1_PD;
		_state->VCO_LDO_PD = this->VCO_LDO_PD;
		_state->VCO_PD = this->VCO_PD;
		_state->OSCin_PD = this->OSCin_PD;
		_state->SYSREF_GBL_PD = this->SYSREF_GBL_PD;
		_state->SYSREF_PD = this->SYSREF_PD;
		_state->SYSREF_DDLY_PD = this->SYSREF_DDLY_PD;
		_state->SYSREF_PLSR_PD = this->SYSREF_PLSR_PD;
		_state->DDLYd_SYSREF_EN = this->DDLYd_SYSREF_EN;
		_state->DDLYd12_EN = this->DDLYd12_EN;
		_state->DDLYd10_EN = this->DDLYd10_EN;
		_state->DDLYd7_EN = this->DDLYd7_EN;
		_state->DDLYd6_EN = this->DDLYd6_EN;
		_state->DDLYd4_EN = this->DDLYd4_EN;
		_state->DDLYd2_EN = this->DDLYd2_EN;
		_state->DDLYd0_EN = this->DDLYd0_EN;
		_state->DDLYd_STEP_CNT = this->DDLYd_STEP_CNT;
		_state->SYSREF_DDLY_CLR = this->SYSREF_DDLY_CLR;
		_state->SYNC_1SHOT_EN = this->SYNC_1SHOT_EN;
		_state->SYNC_POL = this->SYNC_POL;
		_state->SYNC_EN = this->SYNC_EN;
		_state->SYNC_PLL2_DLD = this->SYNC_PLL2_DLD;
		_state->SYNC_PLL1_DLD = this->SYNC_PLL1_DLD;
		_state->SYNC_MODE = this->SYNC_MODE;
		_state->SYNC_DISSYSREF = this->SYNC_DISSYSREF;
		_state->SYNC_DIS12 = this->SYNC_DIS12;
		_state->SYNC_DIS10 = this->SYNC_DIS10;
		_state->SYNC_DIS8 = this->SYNC_DIS8;
		_state->SYNC_DIS6 = this->SYNC_DIS6;
		_state->SYNC_DIS4 = this->SYNC_DIS4;
		_state->SYNC_DIS2 = this->SYNC_DIS2;
		_state->SYNC_DIS0 = this->SYNC_DIS0;
		_state->CLKin2_EN = this->CLKin2_EN;
		_state->CLKin1_EN = this->CLKin1_EN;
		_state->CLKin0_EN = this->CLKin0_EN;
		_state->CLKin2_TYPE = this->CLKin2_TYPE;
		_state->CLKin1_TYPE = this->CLKin1_TYPE;
		_state->CLKin0_TYPE = this->CLKin0_TYPE;
		_state->CLKin_SEL_POL = this->CLKin_SEL_POL;
		_state->CLKin_SEL_MODE = this->CLKin_SEL_MODE;
		_state->CLKin1_OUT_MUX = this->CLKin1_OUT_MUX;
		_state->CLKin0_OUT_MUX = this->CLKin0_OUT_MUX;
		_state->CLKin_SEL0_MUX = this->CLKin_SEL0_MUX;
		_state->CLKin_SEL0_TYPE = this->CLKin_SEL0_TYPE;
		_state->SDIO_RDBK_TYPE = this->SDIO_RDBK_TYPE;
		_state->CLKin_SEL1_MUX = this->CLKin_SEL1_MUX;
		_state->CLKin_SEL1_TYPE = this->CLKin_SEL1_TYPE;
		_state->RESET_MUX = this->RESET_MUX;
		_state->RESET_TYPE = this->RESET_TYPE;
		_state->LOS_TIMEOUT = this->LOS_TIMEOUT;
		_state->LOS_EN = this->LOS_EN;
		_state->TRACK_EN = this->TRACK_EN;
		_state->HOLDOVER_FORCE = this->HOLDOVER_FORCE;
		_state->MAN_DAC_EN = this->MAN_DAC_EN;
		_state->MAN_DAC = this->MAN_DAC;
		_state->DAC_TRIP_LOW = this->DAC_TRIP_LOW;
		_state->DAC_CLK_MULT = this->DAC_CLK_MULT;
		_state->DAC_TRIP_HIGH = this->DAC_TRIP_HIGH;
		_state->DAC_CLK_CNTR = this->DAC_CLK_CNTR;
		_state->CLKin_OVERRIDE = this->CLKin_OVERRIDE;
		_state->HOLDOVER_PLL1_DET = this->HOLDOVER_PLL1_DET;
		_state->HOLDOVER_LOS_DET = this->HOLDOVER_LOS_DET;
		_state->HOLDOVER_VTUNE_DET = this->HOLDOVER_VTUNE_DET;
		_state->HOLDOVER_HITLESS_SWITCH = this->HOLDOVER_HITLESS_SWITCH;
		_state->HOLDOVER_EN = this->HOLDOVER_EN;
		_state->HOLDOVER_DLD_CNT = this->HOLDOVER_DLD_CNT;
		_state->CLKin0_R = this->CLKin0_R;
		_state->CLKin1_R = this->CLKin1_R;
		_state->CLKin2_R = this->CLKin2_R;
		_state->PLL1_N = this->PLL1_N;
		_state->PLL1_WND_SIZE = this->PLL1_WND_SIZE;
		_state->PLL1_CP_TRI = this->PLL1_CP_TRI;
		_state->PLL1_CP_POL = this->PLL1_CP_POL;
		_state->PLL1_CP_GAIN = this->PLL1_CP_GAIN;
		_state->PLL1_DLD_CNT = this->PLL1_DLD_CNT;
		_state->PLL1_R_DLY = this->PLL1_R_DLY;
		_state->PLL1_N_DLY = this->PLL1_N_DLY;
		_state->PLL1_LD_MUX = this->PLL1_LD_MUX;
		_state->PLL1_LD_TYPE = this->PLL1_LD_TYPE;
		_state->PLL2_R = this->PLL2_R;
		_state->PLL2_P = this->PLL2_P;
		_state->OSCin_FREQ = this->OSCin_FREQ;
		_state->PLL2_XTAL_EN = this->PLL2_XTAL_EN;
		_state->PLL2_REF_2X_EN = this->PLL2_REF_2X_EN;
		_state->PLL2_N_CAL = this->PLL2_N_CAL;
		_state->PLL2_FCAL_DIS = this->PLL2_FCAL_DIS;
		_state->PLL2_N = this->PLL2_N;
		_state->PLL2_WND_SIZE = this->PLL2_WND_SIZE;
		_state->PLL2_CP_GAIN = this->PLL2_CP_GAIN;
		_state->PLL2_CP_POL = this->PLL2_CP_POL;
		_state->SYSREF_REQ_EN = this->SYSREF_REQ_EN;
		_state->PLL2_DLD_CNT = this->PLL2_DLD_CNT;
		_state->PLL2_LF_R4 = this->PLL2_LF_R4;
		_state->PLL2_LF_R3 = this->PLL2_LF_R3;
		_state->PLL2_LF_C4 = this->PLL2_LF_C4;
		_state->PLL2_LF_C3 = this->PLL2_LF_C3;
		_state->PLL2_LD_MUX = this->PLL2_LD_MUX;
		_state->PLL2_LD_TYPE = this->PLL2_LD_TYPE;
		_state->PLL2_PRE_PD = this->PLL2_PRE_PD;
		_state->PLL2_PD = this->PLL2_PD;
		_state->VCO1_DIV = this->VCO1_DIV;
		_state->OPT_REG_1 = this->OPT_REG_1;
		_state->OPT_REG_2 = this->OPT_REG_2;
		_state->RB_PLL1_LD_LOST = this->RB_PLL1_LD_LOST;
		_state->RB_PLL1_LD = this->RB_PLL1_LD;
		_state->CLR_PLL1_LD_LOST = this->CLR_PLL1_LD_LOST;
		_state->RB_PLL2_LD_LOST = this->RB_PLL2_LD_LOST;
		_state->RB_PLL2_LD = this->RB_PLL2_LD;
		_state->CLR_PLL2_LD_LOST = this->CLR_PLL2_LD_LOST;
		_state->RB_DAC_VALUE = this->RB_DAC_VALUE;
		_state->RB_CLKin2_SEL = this->RB_CLKin2_SEL;
		_state->RB_CLKin1_SEL = this->RB_CLKin1_SEL;
		_state->RB_CLKin0_SEL = this->RB_CLKin0_SEL;
		_state->RB_CLKin1_LOS = this->RB_CLKin1_LOS;
		_state->RB_CLKin0_LOS = this->RB_CLKin0_LOS;
		_state->RB_HOLDOVER = this->RB_HOLDOVER;
		_state->SPI_LOCK = this->SPI_LOCK;
		return;
	}
	
	template<typename T> std::vector<T> get_all_addrs(void){
		if(_state == nullptr){
			; // some error detection here
		}
		std::set<T> addrs;
		addrs.insert(0x000);
		addrs.insert(0x002);
		addrs.insert(0x003);
		addrs.insert(0x004);
		addrs.insert(0x005);
		addrs.insert(0x006);
		addrs.insert(0x00C);
		addrs.insert(0x00D);
		addrs.insert(0x100);
		addrs.insert(0x101);
		addrs.insert(0x103);
		addrs.insert(0x104);
		addrs.insert(0x105);
		addrs.insert(0x106);
		addrs.insert(0x107);
		addrs.insert(0x108);
		addrs.insert(0x109);
		addrs.insert(0x10B);
		addrs.insert(0x10C);
		addrs.insert(0x10D);
		addrs.insert(0x10E);
		addrs.insert(0x10F);
		addrs.insert(0x110);
		addrs.insert(0x111);
		addrs.insert(0x113);
		addrs.insert(0x114);
		addrs.insert(0x115);
		addrs.insert(0x116);
		addrs.insert(0x117);
		addrs.insert(0x118);
		addrs.insert(0x119);
		addrs.insert(0x11B);
		addrs.insert(0x11C);
		addrs.insert(0x11D);
		addrs.insert(0x11E);
		addrs.insert(0x11F);
		addrs.insert(0x120);
		addrs.insert(0x121);
		addrs.insert(0x123);
		addrs.insert(0x124);
		addrs.insert(0x125);
		addrs.insert(0x126);
		addrs.insert(0x127);
		addrs.insert(0x128);
		addrs.insert(0x129);
		addrs.insert(0x12B);
		addrs.insert(0x12C);
		addrs.insert(0x12D);
		addrs.insert(0x12E);
		addrs.insert(0x12F);
		addrs.insert(0x130);
		addrs.insert(0x131);
		addrs.insert(0x133);
		addrs.insert(0x134);
		addrs.insert(0x135);
		addrs.insert(0x136);
		addrs.insert(0x137);
		addrs.insert(0x138);
		addrs.insert(0x139);
		addrs.insert(0x13A);
		addrs.insert(0x13B);
		addrs.insert(0x13C);
		addrs.insert(0x13D);
		addrs.insert(0x13E);
		addrs.insert(0x13F);
		addrs.insert(0x140);
		addrs.insert(0x141);
		addrs.insert(0x142);
		addrs.insert(0x143);
		addrs.insert(0x144);
		addrs.insert(0x145);
		addrs.insert(0x146);
		addrs.insert(0x147);
		addrs.insert(0x148);
		addrs.insert(0x149);
		addrs.insert(0x14A);
		addrs.insert(0x14B);
		addrs.insert(0x14C);
		addrs.insert(0x14D);
		addrs.insert(0x14E);
		addrs.insert(0x14F);
		addrs.insert(0x150);
		addrs.insert(0x151);
		addrs.insert(0x152);
		addrs.insert(0x153);
		addrs.insert(0x154);
		addrs.insert(0x155);
		addrs.insert(0x156);
		addrs.insert(0x157);
		addrs.insert(0x158);
		addrs.insert(0x159);
		addrs.insert(0x15A);
		addrs.insert(0x15B);
		addrs.insert(0x15C);
		addrs.insert(0x15D);
		addrs.insert(0x15E);
		addrs.insert(0x15F);
		addrs.insert(0x160);
		addrs.insert(0x161);
		addrs.insert(0x162);
		addrs.insert(0x163);
		addrs.insert(0x164);
		addrs.insert(0x165);
		addrs.insert(0x166);
		addrs.insert(0x167);
		addrs.insert(0x168);
		addrs.insert(0x169);
		addrs.insert(0x16A);
		addrs.insert(0x16B);
		addrs.insert(0x16C);
		addrs.insert(0x16D);
		addrs.insert(0x16E);
		addrs.insert(0x171);
		addrs.insert(0x172);
		addrs.insert(0x173);
		addrs.insert(0x174);
		addrs.insert(0x17C);
		addrs.insert(0x17D);
		addrs.insert(0x182);
		addrs.insert(0x183);
		addrs.insert(0x184);
		addrs.insert(0x185);
		addrs.insert(0x188);
		addrs.insert(0x1FFD);
		addrs.insert(0x1FFE);
		addrs.insert(0x1FFF);
	
		std::vector<T> outVec;
		for ( auto & it : addrs ){
			outVec.push_back(it);
		}
		return outVec;
	}
	
	template<typename T> std::vector<T> get_changed_addrs(void){
		if(_state == nullptr){
			; // some error detection here
		}
		std::set<T> addrs;
		// check for all the changed registers
		if(_state->RESET != this->RESET){
			addrs.insert(0x000);
		}
		if(_state->SPI_3WIRE_DIS != this->SPI_3WIRE_DIS){
			addrs.insert(0x000);
		}
		if(_state->POWERDOWN != this->POWERDOWN){
			addrs.insert(0x002);
		}
		if(_state->ID_DEVICE_TYPE != this->ID_DEVICE_TYPE){
			addrs.insert(0x003);
		}
		if(_state->ID_PROD != this->ID_PROD){
			addrs.insert(0x004);
			addrs.insert(0x005);
		}
		if(_state->ID_MASKREV != this->ID_MASKREV){
			addrs.insert(0x006);
		}
		if(_state->ID_VNDR != this->ID_VNDR){
			addrs.insert(0x00C);
			addrs.insert(0x00D);
		}
		if(_state->CLKout0_1_ODL != this->CLKout0_1_ODL){
			addrs.insert(0x100);
		}
		if(_state->CLKout0_1_IDL != this->CLKout0_1_IDL){
			addrs.insert(0x100);
		}
		if(_state->DCLKout0_DIV != this->DCLKout0_DIV){
			addrs.insert(0x100);
		}
		if(_state->DCLKout0_DDLY_CNTH != this->DCLKout0_DDLY_CNTH){
			addrs.insert(0x101);
		}
		if(_state->DCLKout0_DDLY_CNTL != this->DCLKout0_DDLY_CNTL){
			addrs.insert(0x101);
		}
		if(_state->DCLKout0_ADLY != this->DCLKout0_ADLY){
			addrs.insert(0x103);
		}
		if(_state->DCLKout0_ADLY_MUX != this->DCLKout0_ADLY_MUX){
			addrs.insert(0x103);
		}
		if(_state->DCLKout0_MUX != this->DCLKout0_MUX){
			addrs.insert(0x103);
		}
		if(_state->DCLKout0_HS != this->DCLKout0_HS){
			addrs.insert(0x104);
		}
		if(_state->SDCLKout1_MUX != this->SDCLKout1_MUX){
			addrs.insert(0x104);
		}
		if(_state->SDCLKout1_DDLY != this->SDCLKout1_DDLY){
			addrs.insert(0x104);
		}
		if(_state->SDCLKout1_HS != this->SDCLKout1_HS){
			addrs.insert(0x104);
		}
		if(_state->SDCLKout1_ADLY_EN != this->SDCLKout1_ADLY_EN){
			addrs.insert(0x105);
		}
		if(_state->SDCLKout1_ADLY != this->SDCLKout1_ADLY){
			addrs.insert(0x105);
		}
		if(_state->DCLKout0_DDLY_PD != this->DCLKout0_DDLY_PD){
			addrs.insert(0x106);
		}
		if(_state->DCLKout0_HSg_PD != this->DCLKout0_HSg_PD){
			addrs.insert(0x106);
		}
		if(_state->DCLKout0_ADLYg_PD != this->DCLKout0_ADLYg_PD){
			addrs.insert(0x106);
		}
		if(_state->DCLKout0_ADLY_PD != this->DCLKout0_ADLY_PD){
			addrs.insert(0x106);
		}
		if(_state->CLKout0_1_PD != this->CLKout0_1_PD){
			addrs.insert(0x106);
		}
		if(_state->SDCLKout1_DIS_MODE != this->SDCLKout1_DIS_MODE){
			addrs.insert(0x106);
		}
		if(_state->SDCLKout1_PD != this->SDCLKout1_PD){
			addrs.insert(0x106);
		}
		if(_state->SDCLKout1_POL != this->SDCLKout1_POL){
			addrs.insert(0x107);
		}
		if(_state->CLKout1_FMT != this->CLKout1_FMT){
			addrs.insert(0x107);
		}
		if(_state->DCLKout0_POL != this->DCLKout0_POL){
			addrs.insert(0x107);
		}
		if(_state->CLKout0_FMT != this->CLKout0_FMT){
			addrs.insert(0x107);
		}
		if(_state->CLKout2_3_ODL != this->CLKout2_3_ODL){
			addrs.insert(0x108);
		}
		if(_state->CLKout2_3_IDL != this->CLKout2_3_IDL){
			addrs.insert(0x108);
		}
		if(_state->DCLKout2_DIV != this->DCLKout2_DIV){
			addrs.insert(0x108);
		}
		if(_state->DCLKout2_DDLY_CNTH != this->DCLKout2_DDLY_CNTH){
			addrs.insert(0x109);
		}
		if(_state->DCLKout2_DDLY_CNTL != this->DCLKout2_DDLY_CNTL){
			addrs.insert(0x109);
		}
		if(_state->DCLKout2_ADLY != this->DCLKout2_ADLY){
			addrs.insert(0x10B);
		}
		if(_state->DCLKout2_ADLY_MUX != this->DCLKout2_ADLY_MUX){
			addrs.insert(0x10B);
		}
		if(_state->DCLKout2_MUX != this->DCLKout2_MUX){
			addrs.insert(0x10B);
		}
		if(_state->DCLKout2_HS != this->DCLKout2_HS){
			addrs.insert(0x10C);
		}
		if(_state->SDCLKout3_MUX != this->SDCLKout3_MUX){
			addrs.insert(0x10C);
		}
		if(_state->SDCLKout3_DDLY != this->SDCLKout3_DDLY){
			addrs.insert(0x10C);
		}
		if(_state->SDCLKout3_HS != this->SDCLKout3_HS){
			addrs.insert(0x10C);
		}
		if(_state->SDCLKout3_ADLY_EN != this->SDCLKout3_ADLY_EN){
			addrs.insert(0x10D);
		}
		if(_state->SDCLKout3_ADLY != this->SDCLKout3_ADLY){
			addrs.insert(0x10D);
		}
		if(_state->DCLKout2_DDLY_PD != this->DCLKout2_DDLY_PD){
			addrs.insert(0x10E);
		}
		if(_state->DCLKout2_HSg_PD != this->DCLKout2_HSg_PD){
			addrs.insert(0x10E);
		}
		if(_state->DCLKout2_ADLYg_PD != this->DCLKout2_ADLYg_PD){
			addrs.insert(0x10E);
		}
		if(_state->DCLKout2_ADLY_PD != this->DCLKout2_ADLY_PD){
			addrs.insert(0x10E);
		}
		if(_state->CLKout2_3_PD != this->CLKout2_3_PD){
			addrs.insert(0x10E);
		}
		if(_state->SDCLKout3_DIS_MODE != this->SDCLKout3_DIS_MODE){
			addrs.insert(0x10E);
		}
		if(_state->SDCLKout3_PD != this->SDCLKout3_PD){
			addrs.insert(0x10E);
		}
		if(_state->SDCLKout3_POL != this->SDCLKout3_POL){
			addrs.insert(0x10F);
		}
		if(_state->CLKout3_FMT != this->CLKout3_FMT){
			addrs.insert(0x10F);
		}
		if(_state->DCLKout2_POL != this->DCLKout2_POL){
			addrs.insert(0x10F);
		}
		if(_state->CLKout2_FMT != this->CLKout2_FMT){
			addrs.insert(0x10F);
		}
		if(_state->CLKout4_5_ODL != this->CLKout4_5_ODL){
			addrs.insert(0x110);
		}
		if(_state->CLKout4_5_IDL != this->CLKout4_5_IDL){
			addrs.insert(0x110);
		}
		if(_state->DCLKout4_DIV != this->DCLKout4_DIV){
			addrs.insert(0x110);
		}
		if(_state->DCLKout4_DDLY_CNTH != this->DCLKout4_DDLY_CNTH){
			addrs.insert(0x111);
		}
		if(_state->DCLKout4_DDLY_CNTL != this->DCLKout4_DDLY_CNTL){
			addrs.insert(0x111);
		}
		if(_state->DCLKout4_ADLY != this->DCLKout4_ADLY){
			addrs.insert(0x113);
		}
		if(_state->DCLKout4_ADLY_MUX != this->DCLKout4_ADLY_MUX){
			addrs.insert(0x113);
		}
		if(_state->DCLKout4_MUX != this->DCLKout4_MUX){
			addrs.insert(0x113);
		}
		if(_state->DCLKout4_HS != this->DCLKout4_HS){
			addrs.insert(0x114);
		}
		if(_state->SDCLKout5_MUX != this->SDCLKout5_MUX){
			addrs.insert(0x114);
		}
		if(_state->SDCLKout5_DDLY != this->SDCLKout5_DDLY){
			addrs.insert(0x114);
		}
		if(_state->SDCLKout5_HS != this->SDCLKout5_HS){
			addrs.insert(0x114);
		}
		if(_state->SDCLKout5_ADLY_EN != this->SDCLKout5_ADLY_EN){
			addrs.insert(0x115);
		}
		if(_state->SDCLKout5_ADLY != this->SDCLKout5_ADLY){
			addrs.insert(0x115);
		}
		if(_state->DCLKout4_DDLY_PD != this->DCLKout4_DDLY_PD){
			addrs.insert(0x116);
		}
		if(_state->DCLKout4_HSg_PD != this->DCLKout4_HSg_PD){
			addrs.insert(0x116);
		}
		if(_state->DCLKout4_ADLYg_PD != this->DCLKout4_ADLYg_PD){
			addrs.insert(0x116);
		}
		if(_state->DCLKout4_ADLY_PD != this->DCLKout4_ADLY_PD){
			addrs.insert(0x116);
		}
		if(_state->CLKout4_5_PD != this->CLKout4_5_PD){
			addrs.insert(0x116);
		}
		if(_state->SDCLKout5_DIS_MODE != this->SDCLKout5_DIS_MODE){
			addrs.insert(0x116);
		}
		if(_state->SDCLKout5_PD != this->SDCLKout5_PD){
			addrs.insert(0x116);
		}
		if(_state->SDCLKout5_POL != this->SDCLKout5_POL){
			addrs.insert(0x117);
		}
		if(_state->CLKout5_FMT != this->CLKout5_FMT){
			addrs.insert(0x117);
		}
		if(_state->DCLKout4_POL != this->DCLKout4_POL){
			addrs.insert(0x117);
		}
		if(_state->CLKout4_FMT != this->CLKout4_FMT){
			addrs.insert(0x117);
		}
		if(_state->CLKout6_7_ODL != this->CLKout6_7_ODL){
			addrs.insert(0x118);
		}
		if(_state->CLKout6_8_IDL != this->CLKout6_8_IDL){
			addrs.insert(0x118);
		}
		if(_state->DCLKout6_DIV != this->DCLKout6_DIV){
			addrs.insert(0x118);
		}
		if(_state->DCLKout6_DDLY_CNTH != this->DCLKout6_DDLY_CNTH){
			addrs.insert(0x119);
		}
		if(_state->DCLKout6_DDLY_CNTL != this->DCLKout6_DDLY_CNTL){
			addrs.insert(0x119);
		}
		if(_state->DCLKout6_ADLY != this->DCLKout6_ADLY){
			addrs.insert(0x11B);
		}
		if(_state->DCLKout6_ADLY_MUX != this->DCLKout6_ADLY_MUX){
			addrs.insert(0x11B);
		}
		if(_state->DCLKout6_MUX != this->DCLKout6_MUX){
			addrs.insert(0x11B);
		}
		if(_state->DCLKout6_HS != this->DCLKout6_HS){
			addrs.insert(0x11C);
		}
		if(_state->SDCLKout7_MUX != this->SDCLKout7_MUX){
			addrs.insert(0x11C);
		}
		if(_state->SDCLKout7_DDLY != this->SDCLKout7_DDLY){
			addrs.insert(0x11C);
		}
		if(_state->SDCLKout7_HS != this->SDCLKout7_HS){
			addrs.insert(0x11C);
		}
		if(_state->SDCLKout7_ADLY_EN != this->SDCLKout7_ADLY_EN){
			addrs.insert(0x11D);
		}
		if(_state->SDCLKout7_ADLY != this->SDCLKout7_ADLY){
			addrs.insert(0x11D);
		}
		if(_state->DCLKout6_DDLY_PD != this->DCLKout6_DDLY_PD){
			addrs.insert(0x11E);
		}
		if(_state->DCLKout6_HSg_PD != this->DCLKout6_HSg_PD){
			addrs.insert(0x11E);
		}
		if(_state->DCLKout6_ADLYg_PD != this->DCLKout6_ADLYg_PD){
			addrs.insert(0x11E);
		}
		if(_state->DCLKout6_ADLY_PD != this->DCLKout6_ADLY_PD){
			addrs.insert(0x11E);
		}
		if(_state->CLKout6_7_PD != this->CLKout6_7_PD){
			addrs.insert(0x11E);
		}
		if(_state->SDCLKout7_DIS_MODE != this->SDCLKout7_DIS_MODE){
			addrs.insert(0x11E);
		}
		if(_state->SDCLKout7_PD != this->SDCLKout7_PD){
			addrs.insert(0x11E);
		}
		if(_state->SDCLKout7_POL != this->SDCLKout7_POL){
			addrs.insert(0x11F);
		}
		if(_state->CLKout7_FMT != this->CLKout7_FMT){
			addrs.insert(0x11F);
		}
		if(_state->DCLKout6_POL != this->DCLKout6_POL){
			addrs.insert(0x11F);
		}
		if(_state->CLKout6_FMT != this->CLKout6_FMT){
			addrs.insert(0x11F);
		}
		if(_state->CLKout8_9_ODL != this->CLKout8_9_ODL){
			addrs.insert(0x120);
		}
		if(_state->CLKout8_9_IDL != this->CLKout8_9_IDL){
			addrs.insert(0x120);
		}
		if(_state->DCLKout8_DIV != this->DCLKout8_DIV){
			addrs.insert(0x120);
		}
		if(_state->DCLKout8_DDLY_CNTH != this->DCLKout8_DDLY_CNTH){
			addrs.insert(0x121);
		}
		if(_state->DCLKout8_DDLY_CNTL != this->DCLKout8_DDLY_CNTL){
			addrs.insert(0x121);
		}
		if(_state->DCLKout8_ADLY != this->DCLKout8_ADLY){
			addrs.insert(0x123);
		}
		if(_state->DCLKout8_ADLY_MUX != this->DCLKout8_ADLY_MUX){
			addrs.insert(0x123);
		}
		if(_state->DCLKout8_MUX != this->DCLKout8_MUX){
			addrs.insert(0x123);
		}
		if(_state->DCLKout8_HS != this->DCLKout8_HS){
			addrs.insert(0x124);
		}
		if(_state->SDCLKout9_MUX != this->SDCLKout9_MUX){
			addrs.insert(0x124);
		}
		if(_state->SDCLKout9_DDLY != this->SDCLKout9_DDLY){
			addrs.insert(0x124);
		}
		if(_state->SDCLKout9_HS != this->SDCLKout9_HS){
			addrs.insert(0x124);
		}
		if(_state->SDCLKout9_ADLY_EN != this->SDCLKout9_ADLY_EN){
			addrs.insert(0x125);
		}
		if(_state->SDCLKout9_ADLY != this->SDCLKout9_ADLY){
			addrs.insert(0x125);
		}
		if(_state->DCLKout8_DDLY_PD != this->DCLKout8_DDLY_PD){
			addrs.insert(0x126);
		}
		if(_state->DCLKout8_HSg_PD != this->DCLKout8_HSg_PD){
			addrs.insert(0x126);
		}
		if(_state->DCLKout8_ADLYg_PD != this->DCLKout8_ADLYg_PD){
			addrs.insert(0x126);
		}
		if(_state->DCLKout8_ADLY_PD != this->DCLKout8_ADLY_PD){
			addrs.insert(0x126);
		}
		if(_state->CLKout8_9_PD != this->CLKout8_9_PD){
			addrs.insert(0x126);
		}
		if(_state->SDCLKout9_DIS_MODE != this->SDCLKout9_DIS_MODE){
			addrs.insert(0x126);
		}
		if(_state->SDCLKout9_PD != this->SDCLKout9_PD){
			addrs.insert(0x126);
		}
		if(_state->SDCLKout9_POL != this->SDCLKout9_POL){
			addrs.insert(0x127);
		}
		if(_state->CLKout9_FMT != this->CLKout9_FMT){
			addrs.insert(0x127);
		}
		if(_state->DCLKout8_POL != this->DCLKout8_POL){
			addrs.insert(0x127);
		}
		if(_state->CLKout8_FMT != this->CLKout8_FMT){
			addrs.insert(0x127);
		}
		if(_state->CLKout10_11_ODL != this->CLKout10_11_ODL){
			addrs.insert(0x128);
		}
		if(_state->CLKout10_11_IDL != this->CLKout10_11_IDL){
			addrs.insert(0x128);
		}
		if(_state->DCLKout10_DIV != this->DCLKout10_DIV){
			addrs.insert(0x128);
		}
		if(_state->DCLKout10_DDLY_CNTH != this->DCLKout10_DDLY_CNTH){
			addrs.insert(0x129);
		}
		if(_state->DCLKout10_DDLY_CNTL != this->DCLKout10_DDLY_CNTL){
			addrs.insert(0x129);
		}
		if(_state->DCLKout10_ADLY != this->DCLKout10_ADLY){
			addrs.insert(0x12B);
		}
		if(_state->DCLKout10_ADLY_MUX != this->DCLKout10_ADLY_MUX){
			addrs.insert(0x12B);
		}
		if(_state->DCLKout10_MUX != this->DCLKout10_MUX){
			addrs.insert(0x12B);
		}
		if(_state->DCLKout10_HS != this->DCLKout10_HS){
			addrs.insert(0x12C);
		}
		if(_state->SDCLKout11_MUX != this->SDCLKout11_MUX){
			addrs.insert(0x12C);
		}
		if(_state->SDCLKout11_DDLY != this->SDCLKout11_DDLY){
			addrs.insert(0x12C);
		}
		if(_state->SDCLKout11_HS != this->SDCLKout11_HS){
			addrs.insert(0x12C);
		}
		if(_state->SDCKLout11_ADLY_EN != this->SDCKLout11_ADLY_EN){
			addrs.insert(0x12D);
		}
		if(_state->SDCLKout11_ADLY != this->SDCLKout11_ADLY){
			addrs.insert(0x12D);
		}
		if(_state->DCLKout10_DDLY_PD != this->DCLKout10_DDLY_PD){
			addrs.insert(0x12E);
		}
		if(_state->DCLKout10_HSg_PD != this->DCLKout10_HSg_PD){
			addrs.insert(0x12E);
		}
		if(_state->DLCLKout10_ADLYg_PD != this->DLCLKout10_ADLYg_PD){
			addrs.insert(0x12E);
		}
		if(_state->DCLKout10_ADLY_PD != this->DCLKout10_ADLY_PD){
			addrs.insert(0x12E);
		}
		if(_state->CLKout10_11_PD != this->CLKout10_11_PD){
			addrs.insert(0x12E);
		}
		if(_state->SDCLKout11_DIS_MODE != this->SDCLKout11_DIS_MODE){
			addrs.insert(0x12E);
		}
		if(_state->SDCLKout11_PD != this->SDCLKout11_PD){
			addrs.insert(0x12E);
		}
		if(_state->SDCLKout11_POL != this->SDCLKout11_POL){
			addrs.insert(0x12F);
		}
		if(_state->CLKout11_FMT != this->CLKout11_FMT){
			addrs.insert(0x12F);
		}
		if(_state->DCLKout10_POL != this->DCLKout10_POL){
			addrs.insert(0x12F);
		}
		if(_state->CLKout10_FMT != this->CLKout10_FMT){
			addrs.insert(0x12F);
		}
		if(_state->CLKout12_13_ODL != this->CLKout12_13_ODL){
			addrs.insert(0x130);
		}
		if(_state->CLKout12_13_IDL != this->CLKout12_13_IDL){
			addrs.insert(0x130);
		}
		if(_state->DCLKout12_DIV != this->DCLKout12_DIV){
			addrs.insert(0x130);
		}
		if(_state->DCLKout12_DDLY_CNTH != this->DCLKout12_DDLY_CNTH){
			addrs.insert(0x131);
		}
		if(_state->DCLKout12_DDLY_CNTL != this->DCLKout12_DDLY_CNTL){
			addrs.insert(0x131);
		}
		if(_state->DCLKout12_ADLY != this->DCLKout12_ADLY){
			addrs.insert(0x133);
		}
		if(_state->DCLKout12_ADLY_MUX != this->DCLKout12_ADLY_MUX){
			addrs.insert(0x133);
		}
		if(_state->DCLKout12_MUX != this->DCLKout12_MUX){
			addrs.insert(0x133);
		}
		if(_state->DCLKout12_HS != this->DCLKout12_HS){
			addrs.insert(0x134);
		}
		if(_state->SDCLKout13_MUX != this->SDCLKout13_MUX){
			addrs.insert(0x134);
		}
		if(_state->SDCLKout13_DDLY != this->SDCLKout13_DDLY){
			addrs.insert(0x134);
		}
		if(_state->SDCLKout13_HS != this->SDCLKout13_HS){
			addrs.insert(0x134);
		}
		if(_state->SDCLKout13_ADLY_EN != this->SDCLKout13_ADLY_EN){
			addrs.insert(0x135);
		}
		if(_state->SDCLKout13_ADLY != this->SDCLKout13_ADLY){
			addrs.insert(0x135);
		}
		if(_state->DCLKout12_DDLY_PD != this->DCLKout12_DDLY_PD){
			addrs.insert(0x136);
		}
		if(_state->DCLKout12_HSg_PD != this->DCLKout12_HSg_PD){
			addrs.insert(0x136);
		}
		if(_state->DCLKout12_ADLYg_PD != this->DCLKout12_ADLYg_PD){
			addrs.insert(0x136);
		}
		if(_state->DCLKout12_ADLY_PD != this->DCLKout12_ADLY_PD){
			addrs.insert(0x136);
		}
		if(_state->CLKout12_13_PD != this->CLKout12_13_PD){
			addrs.insert(0x136);
		}
		if(_state->SDCLKout13_DIS_MODE != this->SDCLKout13_DIS_MODE){
			addrs.insert(0x136);
		}
		if(_state->SDCLKout13_PD != this->SDCLKout13_PD){
			addrs.insert(0x136);
		}
		if(_state->SDCLKout13_POL != this->SDCLKout13_POL){
			addrs.insert(0x137);
		}
		if(_state->CLKout13_FMT != this->CLKout13_FMT){
			addrs.insert(0x137);
		}
		if(_state->DCLKout12_POL != this->DCLKout12_POL){
			addrs.insert(0x137);
		}
		if(_state->CLKout12_FMT != this->CLKout12_FMT){
			addrs.insert(0x137);
		}
		if(_state->VCO_MUX != this->VCO_MUX){
			addrs.insert(0x138);
		}
		if(_state->OSCout_MUX != this->OSCout_MUX){
			addrs.insert(0x138);
		}
		if(_state->OSCout_FMT != this->OSCout_FMT){
			addrs.insert(0x138);
		}
		if(_state->SYSREF_CLKin0_MUX != this->SYSREF_CLKin0_MUX){
			addrs.insert(0x139);
		}
		if(_state->SYSREF_MUX != this->SYSREF_MUX){
			addrs.insert(0x139);
		}
		if(_state->SYSREF_DIV != this->SYSREF_DIV){
			addrs.insert(0x13A);
			addrs.insert(0x13B);
		}
		if(_state->SYSREF_DDLY != this->SYSREF_DDLY){
			addrs.insert(0x13C);
			addrs.insert(0x13D);
		}
		if(_state->SYSREF_PULSE_CNT != this->SYSREF_PULSE_CNT){
			addrs.insert(0x13E);
		}
		if(_state->PLL2_NCLK_MUX != this->PLL2_NCLK_MUX){
			addrs.insert(0x13F);
		}
		if(_state->PLL1_NCLK_MUX != this->PLL1_NCLK_MUX){
			addrs.insert(0x13F);
		}
		if(_state->FB_MUX != this->FB_MUX){
			addrs.insert(0x13F);
		}
		if(_state->FB_MUX_EN != this->FB_MUX_EN){
			addrs.insert(0x13F);
		}
		if(_state->PLL1_PD != this->PLL1_PD){
			addrs.insert(0x140);
		}
		if(_state->VCO_LDO_PD != this->VCO_LDO_PD){
			addrs.insert(0x140);
		}
		if(_state->VCO_PD != this->VCO_PD){
			addrs.insert(0x140);
		}
		if(_state->OSCin_PD != this->OSCin_PD){
			addrs.insert(0x140);
		}
		if(_state->SYSREF_GBL_PD != this->SYSREF_GBL_PD){
			addrs.insert(0x140);
		}
		if(_state->SYSREF_PD != this->SYSREF_PD){
			addrs.insert(0x140);
		}
		if(_state->SYSREF_DDLY_PD != this->SYSREF_DDLY_PD){
			addrs.insert(0x140);
		}
		if(_state->SYSREF_PLSR_PD != this->SYSREF_PLSR_PD){
			addrs.insert(0x140);
		}
		if(_state->DDLYd_SYSREF_EN != this->DDLYd_SYSREF_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd12_EN != this->DDLYd12_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd10_EN != this->DDLYd10_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd7_EN != this->DDLYd7_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd6_EN != this->DDLYd6_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd4_EN != this->DDLYd4_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd2_EN != this->DDLYd2_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd0_EN != this->DDLYd0_EN){
			addrs.insert(0x141);
		}
		if(_state->DDLYd_STEP_CNT != this->DDLYd_STEP_CNT){
			addrs.insert(0x142);
		}
		if(_state->SYSREF_DDLY_CLR != this->SYSREF_DDLY_CLR){
			addrs.insert(0x143);
		}
		if(_state->SYNC_1SHOT_EN != this->SYNC_1SHOT_EN){
			addrs.insert(0x143);
		}
		if(_state->SYNC_POL != this->SYNC_POL){
			addrs.insert(0x143);
		}
		if(_state->SYNC_EN != this->SYNC_EN){
			addrs.insert(0x143);
		}
		if(_state->SYNC_PLL2_DLD != this->SYNC_PLL2_DLD){
			addrs.insert(0x143);
		}
		if(_state->SYNC_PLL1_DLD != this->SYNC_PLL1_DLD){
			addrs.insert(0x143);
		}
		if(_state->SYNC_MODE != this->SYNC_MODE){
			addrs.insert(0x143);
		}
		if(_state->SYNC_DISSYSREF != this->SYNC_DISSYSREF){
			addrs.insert(0x144);
		}
		if(_state->SYNC_DIS12 != this->SYNC_DIS12){
			addrs.insert(0x144);
		}
		if(_state->SYNC_DIS10 != this->SYNC_DIS10){
			addrs.insert(0x144);
		}
		if(_state->SYNC_DIS8 != this->SYNC_DIS8){
			addrs.insert(0x144);
		}
		if(_state->SYNC_DIS6 != this->SYNC_DIS6){
			addrs.insert(0x144);
		}
		if(_state->SYNC_DIS4 != this->SYNC_DIS4){
			addrs.insert(0x144);
		}
		if(_state->SYNC_DIS2 != this->SYNC_DIS2){
			addrs.insert(0x144);
		}
		if(_state->SYNC_DIS0 != this->SYNC_DIS0){
			addrs.insert(0x144);
		}
		if(_state->CLKin2_EN != this->CLKin2_EN){
			addrs.insert(0x146);
		}
		if(_state->CLKin1_EN != this->CLKin1_EN){
			addrs.insert(0x146);
		}
		if(_state->CLKin0_EN != this->CLKin0_EN){
			addrs.insert(0x146);
		}
		if(_state->CLKin2_TYPE != this->CLKin2_TYPE){
			addrs.insert(0x146);
		}
		if(_state->CLKin1_TYPE != this->CLKin1_TYPE){
			addrs.insert(0x146);
		}
		if(_state->CLKin0_TYPE != this->CLKin0_TYPE){
			addrs.insert(0x146);
		}
		if(_state->CLKin_SEL_POL != this->CLKin_SEL_POL){
			addrs.insert(0x147);
		}
		if(_state->CLKin_SEL_MODE != this->CLKin_SEL_MODE){
			addrs.insert(0x147);
		}
		if(_state->CLKin1_OUT_MUX != this->CLKin1_OUT_MUX){
			addrs.insert(0x147);
		}
		if(_state->CLKin0_OUT_MUX != this->CLKin0_OUT_MUX){
			addrs.insert(0x147);
		}
		if(_state->CLKin_SEL0_MUX != this->CLKin_SEL0_MUX){
			addrs.insert(0x148);
		}
		if(_state->CLKin_SEL0_TYPE != this->CLKin_SEL0_TYPE){
			addrs.insert(0x148);
		}
		if(_state->SDIO_RDBK_TYPE != this->SDIO_RDBK_TYPE){
			addrs.insert(0x149);
		}
		if(_state->CLKin_SEL1_MUX != this->CLKin_SEL1_MUX){
			addrs.insert(0x149);
		}
		if(_state->CLKin_SEL1_TYPE != this->CLKin_SEL1_TYPE){
			addrs.insert(0x149);
		}
		if(_state->RESET_MUX != this->RESET_MUX){
			addrs.insert(0x14A);
		}
		if(_state->RESET_TYPE != this->RESET_TYPE){
			addrs.insert(0x14A);
		}
		if(_state->LOS_TIMEOUT != this->LOS_TIMEOUT){
			addrs.insert(0x14B);
		}
		if(_state->LOS_EN != this->LOS_EN){
			addrs.insert(0x14B);
		}
		if(_state->TRACK_EN != this->TRACK_EN){
			addrs.insert(0x14B);
		}
		if(_state->HOLDOVER_FORCE != this->HOLDOVER_FORCE){
			addrs.insert(0x14B);
		}
		if(_state->MAN_DAC_EN != this->MAN_DAC_EN){
			addrs.insert(0x14B);
		}
		if(_state->MAN_DAC != this->MAN_DAC){
			addrs.insert(0x14B);
			addrs.insert(0x14C);
		}
		if(_state->DAC_TRIP_LOW != this->DAC_TRIP_LOW){
			addrs.insert(0x14D);
		}
		if(_state->DAC_CLK_MULT != this->DAC_CLK_MULT){
			addrs.insert(0x14E);
		}
		if(_state->DAC_TRIP_HIGH != this->DAC_TRIP_HIGH){
			addrs.insert(0x14E);
		}
		if(_state->DAC_CLK_CNTR != this->DAC_CLK_CNTR){
			addrs.insert(0x14F);
		}
		if(_state->CLKin_OVERRIDE != this->CLKin_OVERRIDE){
			addrs.insert(0x150);
		}
		if(_state->HOLDOVER_PLL1_DET != this->HOLDOVER_PLL1_DET){
			addrs.insert(0x150);
		}
		if(_state->HOLDOVER_LOS_DET != this->HOLDOVER_LOS_DET){
			addrs.insert(0x150);
		}
		if(_state->HOLDOVER_VTUNE_DET != this->HOLDOVER_VTUNE_DET){
			addrs.insert(0x150);
		}
		if(_state->HOLDOVER_HITLESS_SWITCH != this->HOLDOVER_HITLESS_SWITCH){
			addrs.insert(0x150);
		}
		if(_state->HOLDOVER_EN != this->HOLDOVER_EN){
			addrs.insert(0x150);
		}
		if(_state->HOLDOVER_DLD_CNT != this->HOLDOVER_DLD_CNT){
			addrs.insert(0x151);
			addrs.insert(0x152);
		}
		if(_state->CLKin0_R != this->CLKin0_R){
			addrs.insert(0x153);
			addrs.insert(0x154);
		}
		if(_state->CLKin1_R != this->CLKin1_R){
			addrs.insert(0x155);
			addrs.insert(0x156);
		}
		if(_state->CLKin2_R != this->CLKin2_R){
			addrs.insert(0x157);
			addrs.insert(0x158);
		}
		if(_state->PLL1_N != this->PLL1_N){
			addrs.insert(0x159);
			addrs.insert(0x15A);
		}
		if(_state->PLL1_WND_SIZE != this->PLL1_WND_SIZE){
			addrs.insert(0x15B);
		}
		if(_state->PLL1_CP_TRI != this->PLL1_CP_TRI){
			addrs.insert(0x15B);
		}
		if(_state->PLL1_CP_POL != this->PLL1_CP_POL){
			addrs.insert(0x15B);
		}
		if(_state->PLL1_CP_GAIN != this->PLL1_CP_GAIN){
			addrs.insert(0x15B);
		}
		if(_state->PLL1_DLD_CNT != this->PLL1_DLD_CNT){
			addrs.insert(0x15C);
			addrs.insert(0x15D);
		}
		if(_state->PLL1_R_DLY != this->PLL1_R_DLY){
			addrs.insert(0x15E);
		}
		if(_state->PLL1_N_DLY != this->PLL1_N_DLY){
			addrs.insert(0x15E);
		}
		if(_state->PLL1_LD_MUX != this->PLL1_LD_MUX){
			addrs.insert(0x15F);
		}
		if(_state->PLL1_LD_TYPE != this->PLL1_LD_TYPE){
			addrs.insert(0x15F);
		}
		if(_state->PLL2_R != this->PLL2_R){
			addrs.insert(0x160);
			addrs.insert(0x161);
		}
		if(_state->PLL2_P != this->PLL2_P){
			addrs.insert(0x162);
		}
		if(_state->OSCin_FREQ != this->OSCin_FREQ){
			addrs.insert(0x162);
		}
		if(_state->PLL2_XTAL_EN != this->PLL2_XTAL_EN){
			addrs.insert(0x162);
		}
		if(_state->PLL2_REF_2X_EN != this->PLL2_REF_2X_EN){
			addrs.insert(0x162);
		}
		if(_state->PLL2_N_CAL != this->PLL2_N_CAL){
			addrs.insert(0x163);
			addrs.insert(0x164);
			addrs.insert(0x165);
		}
		if(_state->PLL2_FCAL_DIS != this->PLL2_FCAL_DIS){
			addrs.insert(0x166);
		}
		if(_state->PLL2_N != this->PLL2_N){
			addrs.insert(0x166);
			addrs.insert(0x167);
			addrs.insert(0x168);
		}
		if(_state->PLL2_WND_SIZE != this->PLL2_WND_SIZE){
			addrs.insert(0x169);
		}
		if(_state->PLL2_CP_GAIN != this->PLL2_CP_GAIN){
			addrs.insert(0x169);
		}
		if(_state->PLL2_CP_POL != this->PLL2_CP_POL){
			addrs.insert(0x169);
		}
		if(_state->SYSREF_REQ_EN != this->SYSREF_REQ_EN){
			addrs.insert(0x16A);
		}
		if(_state->PLL2_DLD_CNT != this->PLL2_DLD_CNT){
			addrs.insert(0x16A);
			addrs.insert(0x16B);
		}
		if(_state->PLL2_LF_R4 != this->PLL2_LF_R4){
			addrs.insert(0x16C);
		}
		if(_state->PLL2_LF_R3 != this->PLL2_LF_R3){
			addrs.insert(0x16C);
		}
		if(_state->PLL2_LF_C4 != this->PLL2_LF_C4){
			addrs.insert(0x16D);
		}
		if(_state->PLL2_LF_C3 != this->PLL2_LF_C3){
			addrs.insert(0x16D);
		}
		if(_state->PLL2_LD_MUX != this->PLL2_LD_MUX){
			addrs.insert(0x16E);
		}
		if(_state->PLL2_LD_TYPE != this->PLL2_LD_TYPE){
			addrs.insert(0x16E);
		}
		if(_state->PLL2_PRE_PD != this->PLL2_PRE_PD){
			addrs.insert(0x173);
		}
		if(_state->PLL2_PD != this->PLL2_PD){
			addrs.insert(0x173);
		}
		if(_state->VCO1_DIV != this->VCO1_DIV){
			addrs.insert(0x174);
		}
		if(_state->OPT_REG_1 != this->OPT_REG_1){
			addrs.insert(0x17C);
		}
		if(_state->OPT_REG_2 != this->OPT_REG_2){
			addrs.insert(0x17D);
		}
		if(_state->RB_PLL1_LD_LOST != this->RB_PLL1_LD_LOST){
			addrs.insert(0x182);
		}
		if(_state->RB_PLL1_LD != this->RB_PLL1_LD){
			addrs.insert(0x182);
		}
		if(_state->CLR_PLL1_LD_LOST != this->CLR_PLL1_LD_LOST){
			addrs.insert(0x182);
		}
		if(_state->RB_PLL2_LD_LOST != this->RB_PLL2_LD_LOST){
			addrs.insert(0x183);
		}
		if(_state->RB_PLL2_LD != this->RB_PLL2_LD){
			addrs.insert(0x183);
		}
		if(_state->CLR_PLL2_LD_LOST != this->CLR_PLL2_LD_LOST){
			addrs.insert(0x183);
		}
		if(_state->RB_DAC_VALUE != this->RB_DAC_VALUE){
			addrs.insert(0x184);
			addrs.insert(0x185);
		}
		if(_state->RB_CLKin2_SEL != this->RB_CLKin2_SEL){
			addrs.insert(0x184);
		}
		if(_state->RB_CLKin1_SEL != this->RB_CLKin1_SEL){
			addrs.insert(0x184);
		}
		if(_state->RB_CLKin0_SEL != this->RB_CLKin0_SEL){
			addrs.insert(0x184);
		}
		if(_state->RB_CLKin1_LOS != this->RB_CLKin1_LOS){
			addrs.insert(0x184);
		}
		if(_state->RB_CLKin0_LOS != this->RB_CLKin0_LOS){
			addrs.insert(0x184);
		}
		if(_state->RB_HOLDOVER != this->RB_HOLDOVER){
			addrs.insert(0x188);
		}
		if(_state->SPI_LOCK != this->SPI_LOCK){
			addrs.insert(0x1FFD);
			addrs.insert(0x1FFE);
			addrs.insert(0x1FFF);
		}
	
		std::vector<T> outVec;
		for ( auto & it : addrs ){
			outVec.push_back(it);
		}
		return outVec;
	}
	
	template<typename T> std::vector<T> get_POR_addrs(void){
		if(_state == nullptr){
			; // some error detection here
		}
		std::vector<T> addrs;
		addrs.push_back(0x000);
		addrs.push_back(0x002);
		addrs.push_back(0x003);
		addrs.push_back(0x004);
		addrs.push_back(0x005);
		addrs.push_back(0x006);
		addrs.push_back(0x00C);
		addrs.push_back(0x00D);
		addrs.push_back(0x100);
		addrs.push_back(0x101);
		addrs.push_back(0x103);
		addrs.push_back(0x104);
		addrs.push_back(0x105);
		addrs.push_back(0x106);
		addrs.push_back(0x107);
		addrs.push_back(0x108);
		addrs.push_back(0x109);
		addrs.push_back(0x10B);
		addrs.push_back(0x10C);
		addrs.push_back(0x10D);
		addrs.push_back(0x10E);
		addrs.push_back(0x10F);
		addrs.push_back(0x110);
		addrs.push_back(0x111);
		addrs.push_back(0x113);
		addrs.push_back(0x114);
		addrs.push_back(0x115);
		addrs.push_back(0x116);
		addrs.push_back(0x117);
		addrs.push_back(0x118);
		addrs.push_back(0x119);
		addrs.push_back(0x11B);
		addrs.push_back(0x11C);
		addrs.push_back(0x11D);
		addrs.push_back(0x11E);
		addrs.push_back(0x11F);
		addrs.push_back(0x120);
		addrs.push_back(0x121);
		addrs.push_back(0x123);
		addrs.push_back(0x124);
		addrs.push_back(0x125);
		addrs.push_back(0x126);
		addrs.push_back(0x127);
		addrs.push_back(0x128);
		addrs.push_back(0x129);
		addrs.push_back(0x12B);
		addrs.push_back(0x12C);
		addrs.push_back(0x12D);
		addrs.push_back(0x12E);
		addrs.push_back(0x12F);
		addrs.push_back(0x130);
		addrs.push_back(0x131);
		addrs.push_back(0x133);
		addrs.push_back(0x134);
		addrs.push_back(0x135);
		addrs.push_back(0x136);
		addrs.push_back(0x137);
		addrs.push_back(0x138);
		addrs.push_back(0x139);
		addrs.push_back(0x13A);
		addrs.push_back(0x13B);
		addrs.push_back(0x13C);
		addrs.push_back(0x13D);
		addrs.push_back(0x13E);
		addrs.push_back(0x13F);
		addrs.push_back(0x140);
		addrs.push_back(0x141);
		addrs.push_back(0x142);
		addrs.push_back(0x143);
		addrs.push_back(0x144);
		addrs.push_back(0x145);
		addrs.push_back(0x146);
		addrs.push_back(0x147);
		addrs.push_back(0x148);
		addrs.push_back(0x149);
		addrs.push_back(0x14A);
		addrs.push_back(0x14B);
		addrs.push_back(0x14C);
		addrs.push_back(0x14D);
		addrs.push_back(0x14E);
		addrs.push_back(0x14F);
		addrs.push_back(0x150);
		addrs.push_back(0x151);
		addrs.push_back(0x152);
		addrs.push_back(0x153);
		addrs.push_back(0x154);
		addrs.push_back(0x155);
		addrs.push_back(0x156);
		addrs.push_back(0x157);
		addrs.push_back(0x158);
		addrs.push_back(0x159);
		addrs.push_back(0x15A);
		addrs.push_back(0x15B);
		addrs.push_back(0x15C);
		addrs.push_back(0x15D);
		addrs.push_back(0x15E);
		addrs.push_back(0x15F);
		addrs.push_back(0x160);
		addrs.push_back(0x161);
		addrs.push_back(0x162);
		addrs.push_back(0x163);
		addrs.push_back(0x164);
		addrs.push_back(0x165);
		addrs.push_back(0x171);
		addrs.push_back(0x172);
		addrs.push_back(0x17C);
		addrs.push_back(0x17D);
		addrs.push_back(0x166);
		addrs.push_back(0x167);
		addrs.push_back(0x168);
		addrs.push_back(0x169);
		addrs.push_back(0x16A);
		addrs.push_back(0x16B);
		addrs.push_back(0x16C);
		addrs.push_back(0x16D);
		addrs.push_back(0x16E);
		addrs.push_back(0x173);
		addrs.push_back(0x174);
		addrs.push_back(0x182);
		addrs.push_back(0x183);
		addrs.push_back(0x184);
		addrs.push_back(0x185);
		addrs.push_back(0x188);
		addrs.push_back(0x1FFD);
		addrs.push_back(0x1FFE);
		addrs.push_back(0x1FFF);
		return addrs;
	}
// private: 
	// private members section
	std::unique_ptr<lmk04828_regs_t> _state;
};
#endif //__LMK04828_REGS_HPP__
