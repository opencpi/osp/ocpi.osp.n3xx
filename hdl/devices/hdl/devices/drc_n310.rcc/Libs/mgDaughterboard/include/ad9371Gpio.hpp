#include <functional>


class N310_AD9371_GPIO{

public:

	using read_fn_t = std::function<std::uint8_t(void)>;  //callback functor definition

	read_fn_t _read_fn;  // callback declaration

	std::uint8_t readGpio(void);  // GPIO write API

	N310_AD9371_GPIO(read_fn_t read_fn); //constructor requireing callback


};

