#ifndef __MG_DAUGHTERBOARD_HPP__
#define __MG_DAUGHTERBOARD_HPP__

#include <array>
#include <algorithm>

#include <cpldController.hpp>
#include <lmk04828Controller.hpp>
#include <dsa.hpp>
#include <adf435x.hpp>
#include <ad9371Gpio.hpp>
#include <transceiver.h>
#include <cpldGpio.hpp>
// #include <i2c.hpp>  // i2c integration
// #include <i2c_n310.hpp> // i2c integration


// abstract routing tuple

// chip enumeration

// chip index

// db index

enum class systemChip {
	c_LMK04828,
	c_AD9371,
	c_CPLD_SPI,
	c_CPLD_GPIO,
	c_DSA,
	c_ADF4351
};


// these should only be created from the lambda wrappers in the dboard constructor
struct abstractRoutingTuple {
	abstractRoutingTuple(systemChip _chipName, std::uint16_t _chipIndex, std::uint16_t _dbIndex);
	systemChip get_chipName(void) const;
	std::uint16_t get_chipIndex(void) const;
	std::uint16_t get_dbIndex(void) const;
	void setTuple(struct abstractRoutingTuple &_refTuple);
private:
	systemChip chipName;
	std::uint16_t chipIndex;
	std::uint16_t dbIndex; // pass through
};

struct chipRouter { // only defined in middleware??
	std::uint16_t get_chipSelect(const struct abstractRoutingTuple _tupleRequest) const;
	chipRouter(const std::vector<std::vector<std::uint16_t>> _systemMap);
private:
	const std::vector<std::vector<std::uint16_t>> systemMap;
};



#if 0
	struct routingTuple { // extra 'encoding' for communication
	    void setTuple(struct routingTuple &_ref);
	    std::uint16_t getDB(void) const;
	    std::uint16_t getCS(void) const;
	    routingTuple(std::uint16_t _chip_select_indexor, std::uint16_t _db_select);
	private:
	    std::uint16_t chip_select_indexor;
	    std::uint16_t db_select;
	    std::mutex _set_mutex;
	    void setDB(std::uint16_t _value);
	    void setCS(std::uint16_t _value);
	};
#endif

using cpld_write_spi_t = std::function<void(uint32_t)>;
//! SPI read functor: Return SPI
using cpld_read_spi_t = std::function<uint32_t(uint32_t)>;

// CPLD GPIO Callback
using cpldGpio_write_t = std::function<void(bool)>;

// DSA Callback
using dsa_write_t = std::function<void((unsigned char))>;

//New Transceiver SPI
using ad9371_write_spi_trans_t = std::function<void(std::uint8_t *)>; // frame pointer
using ad9371_read_spi_trans_t = std::function<std::uint8_t(std::uint8_t *)>; // frame pointer

using requestSysref_t = std::function<void(std::uint16_t)>;
using mykGpioReset_t = std::function<void(void)>;

using ad9371Gpio_read_t = std::function<std::uint8_t(void)>;

// this memory space is controlled by DRC explicitly
using jesdInitMemSpace_t = std::function<std::array<std::uint32_t,8>&(void)>; // initialize Transceiver pageArray std::array
using jesdMemTranslator_t = std::function<std::uint32_t(std::uint32_t, std::uint32_t, bool)>;

#if 0
	middleware: Map controller IO to hardware
	How? -- heres a rundown of all the interactions at the current DRC implementation level
		// Daughterboard controls DB select
		// where does ALL the chip select information come from?
			// n310, IMPLEMENTATION of motherboard
		// need to protect all access to CS or DB routing information

	cpld_read_spi -- Chip select, DB select
		std::uint_least16_t cpld_read_spi(std::uint_least32_t frame) {
			slaves.n310_cpld_spi.set_CPLD_DB_SEL(daughterboard::db);
			slaves.n310_cpld_spi.set_CPLD_ADDR_CS(daughterboard::cs); // CHIP SELECT CHANGES DURING RUNTIME
			slaves.n310_cpld_spi.set_CPLD_FRAME(frame);
			slaves.n310_cpld_spi.set_CPLD_RENABLE(true); // set on
			while(!slaves.n310_cpld_spi.get_CPLD_DONE()){
				; // blocking read
			}
			slaves.n310_cpld_spi.set_CPLD_RENABLE(false); // set off - must be controlled via rcc
			return slaves.n310_cpld_spi.get_CPLD_RDATA(); // where do I place this
		}

    cpld_write_spi -- Chip select, DB select
		void cpld_write_spi(std::uint_least32_t frame) {
			slaves.n310_cpld_spi.set_CPLD_DB_SEL(daughterboard::db); // Daughterboard routing
			slaves.n310_cpld_spi.set_CPLD_ADDR_CS(daughterboard::cs); // CHIP SELECT CHANGES DURING RUNTIME
			slaves.n310_cpld_spi.set_CPLD_FRAME(frame);
			slaves.n310_cpld_spi.set_CPLD_WENABLE(true); // set on
			while(!slaves.n310_cpld_spi.get_CPLD_DONE()){
				; // blocking read
			}
			slaves.n310_cpld_spi.set_CPLD_WENABLE(false); // set off - must be controlled via rcc
			return;
		}

	writeCpldGpio -- ? -- Chip select, DB select ** chip select logic different from other chips **
		void writeCpldGpio(bool set) {
			slaves.n310_gpio.set_DB_Select(daughterboard::db); <-- depending on daughterboard
			slaves.n310_gpio.set_Chip_Select(daughterboard::cs); <-- Chip select based on rf_port_num
			slaves.n310_gpio.set_Set(set);
		}

	readAd9371Gpio -- No chip select, DB select
		std::uint8_t readAd9371Gpio(void){
			slaves.n310_adrv9371_gpio.set_DB_Select(daughterboard::db);
			return slaves.n310_adrv9371_gpio.get_regValue();
		}

	writeDsaIo -- Chip select, DB select
		void writeDsaIo(unsigned char value) {
			slaves.n310_dsa.set_DB_Select(daughterboard::db);
			slaves.n310_dsa.set_Chip_Select(daughterboard::cs); // 4 POSSIBILITIES DURING RUNTIME
			slaves.n310_dsa.set_Attenuation(value);
		}

	sysref_wrapper -- NO ADDITIONAL ROUTING

	mykReset_wrapper -- NO ADDITIONAL ROUTING

	ad9371_write_spi -- Chip select, DB select ** -- NO ACTUAL CHIP SELECT RN, SEPARATE HARDWARE
		void ad9371_write_spi(std::uint8_t * bufIn) {
			std::uint_least32_t spiFrame = (bufIn[0] << 16) | (bufIn[1] << 8) | (bufIn[2]);
			slaves.n310_ad9371_spi.set_MK_DB_SEL(daughterboard::db);
			slaves.n310_ad9371_spi.set_MK_FRAME(spiFrame);
			slaves.n310_ad9371_spi.set_MK_WENABLE(true); // write enable
			while(!slaves.n310_ad9371_spi.get_MK_DONE()) {
				;
			}
			slaves.n310_ad9371_spi.set_MK_WENABLE(false);
			return;
		}

	ad9371_read_spi -- Same as above**

	jesdInitMemSpace_t -- No chip select, DB select

	jesdMemTranslator_t -- No chip select, DB select

	mcrConfigure_wrapper -- NOT USING THIS
#endif

// DRC will inherit this

// typenmae T --> Implementation of DRC middleware

// #define __USING_SINGLETON

// https://stackoverflow.com/questions/34519073/inherit-singleton
// CRTP --> Curiously Recurring Template Pattern
template<typename T> // based on the DRC implementation (motherboard specific?)
class drcMiddleware { // singleton -- SHOULD REALLY BE A SINGLETON, but for now can't figure that out
protected: // only friends/derived classes can see this
	#ifdef __USING_SINGLETON
		drcMiddleware() noexcept = default; // function is non-throwing, default constructor
		drcMiddleware(const drcMiddleware&) = delete; // any use of this constructor is an error
		drcMiddleware& operator=(const drcMiddleware&) = delete; // any use of this constructor is an error
		virtual ~drcMiddleware() = default; // silence warning "base class drcMiddleware<T> has a non-virtual destructor [-Weffc++]"
		// shared object by all of the daughterboards, only ONE drc, therefore only ONE middleware object
	#endif

	// all the information needed to cross scope domains between this library and DRC (OpenCPI scope)

	// static std::mutex _set_mutex; // protects cross boundary interaction between dboards and middleware
	// set of purely virtual methods that the SINGLETON must define
		// singleton == single static object

		// dboard will write to this
		// drcMiddleware will write/read from this

	const chipRouter _chipRouter;
	drcMiddleware(std::vector<std::vector<std::uint16_t>> systemMap)
		:
			_chipRouter(systemMap),
			_routingTuple(systemChip::c_LMK04828,0,0)
		{ }
public:
	abstractRoutingTuple _routingTuple;
	#if __USING_SINGLETON
		static T& get_instance() noexcept(std::is_nothrow_constructible<T>::value) // ?
		{ // return only ONE possible instance for singleton
			// Guaranteed to be destroyed.
			// Instantiated on first use.
			// Thread safe in C++11
			std::cout << "HELP\n";
			static T instance{}; // one static instance of DERIVED class, constructs ONCE
			instance.throwaway();

			return instance;
		} // can use drcMiddleware<T?>::get_instance().DEFINED_METHOD() --> can now use this member
	#endif

	virtual void throwaway(void) { return; } // virtual

	// routingTuple _routingTuple; // chip and db select

	// static std::mutex _set_mutex; // protects access to this _routingTuple (singleton could be shared resource)

	// pure virtual functions to be defined BY DRC implementation --> Available for V-Table routing

	// cpld_read_spi
	virtual std::uint32_t cpld_read_spi(std::uint32_t frame) { return frame;} // virtual
	// cpld_write_spi
	virtual void cpld_write_spi(std::uint_least32_t frame) { return;} // virtual
	// writeCpldGpio
	virtual void writeCpldGpio(bool set) { return; } // virtual
	// readAd9371Gpio
	virtual std::uint8_t readAd9371Gpio(void) { std::uint8_t val = 0; return val;} // virtual
	// writeDsaIo
	virtual void writeDsaIo(unsigned char value) { return;} // virtual

	// NEXT TWO MIGHT BE REDUNDANT

	// sysref_wrapper
	virtual void sysref_wrapper(std::uint16_t the_sysref) { return;} // virtual // LMK controller redirect
	// mykReset_wrapper
	virtual void mykReset_wrapper(void) { return; } // virtual // CPLD controller redirect

	// ad9371_write_spi
	virtual void ad9371_write_spi(std::uint8_t * bufIn) { return; } // virtual
	// ad9371_read_spi
	virtual std::uint8_t ad9371_read_spi(std::uint8_t * bufIn) { std::uint8_t val = 0; return val; } // virtual
	// jesdInitMemSpace_t
	virtual std::array<std::uint32_t,8>& jesd_mem_manager(void) { std::array<std::uint32_t, 8> myVal; return myVal; } // virtual
	// jesdMemTranslator_t
	virtual std::uint32_t jesd_mem_mapper(std::uint32_t addressIn, std::uint32_t value, bool _wr) { std::uint32_t temp = 0; return temp;} // virtual

};

// typenmae T --> Implementation of DRC middleware

template<typename T>
struct daughterboard {
	friend class drcMiddleware<T>;
private:
	static std::uint16_t _globalDaughterboardCount;
	static std::mutex _set_mutex; // use this for all daughterboard shared R/W access, even to middleware routing tupple
public:
	drcMiddleware<T> *referenceMiddleware;
	const std::uint16_t dbOrdinal;

	magnesium_cpld_ctrl cpldController; // one per db
	lmk04828_ctrl lmkController; // one per db
	std::array<adf435x_iface::sptr,2> adfController; // two per db
	std::array<N310_DSA,4> dsaController; // four per db
	N310_CPLD_GPIO cpldGpioController; // one per db
	Transceiver transceiverController; // one per db
	N310_AD9371_GPIO ad9371GpioController; // one per db

	std::uint_least8_t POR; // bit pattern for each controller POR status:
	std::string whichDB;
	const bool active;
	// bit vs controller
	// 0x0 = unset
	// 0x1 = cpld (bit: 0)
	// 0x2 = lmk  (bit: 1)
	// 0x4 = adf  (bit: 2)
	// 0x8 = dsa  (bit: 3)
	// 0x10 = cpld gpio (bit: 4)
	// 0x20 = ad9371 GPIO (bit: 5)
	// 0x40 = transceiver object (bit: 6)
	// 0x80 = ad9371GpioController (bit: 7) -- no need to reset?

	daughterboard & operator = (const daughterboard &other) = default; // assignment operator
	daughterboard(const daughterboard &other) = default; // copy constructor
	#if 1
		daughterboard(std::string _whichDB)
			:
				POR(0x00),
				whichDB(_whichDB)
			{ } // log
	#endif

	// daughterboard(cpld_write_spi_t cpld_write, cpld_read_spi_t cpld_read, dsa_write_t writedsa, cpldGpio_write_t writegpio, ad9371Gpio_read_t ad9371ReadGpio, 
	// 				ad9371_write_spi_trans_t writetrans, ad9371_read_spi_trans_t readtrans, jesdInitMemSpace_t memtrans, requestSysref_t requestSysref_fn,
	// 				mykGpioReset_t mykGpioReset_fn, jesdMemTranslator_t jesdMemTranslator_fn, std::string _whichDB);
	daughterboard(drcMiddleware<T> *_drcMiddlewareSingleton, std::string _whichDB, bool _active = true)
		:
			referenceMiddleware(_drcMiddlewareSingleton),
			dbOrdinal(daughterboard::_globalDaughterboardCount),
			cpldController(
				[this](std::uint32_t frame)->void {
					// cpld internal memory checking
					std::uint16_t chip_index;
					if(((frame >> 16) & 0x7f) < 0x14) { // PS Memory -- 0x05
						chip_index = 0;
					}
					else { // PL Memory -- 0x00
						chip_index = 1;
					}
					abstractRoutingTuple _routingTuple(systemChip::c_CPLD_SPI,chip_index,dbOrdinal);
					// lock the shared reference middleware access calls here
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_write_spi(frame);
				},
				[this](std::uint32_t frame)->std::uint16_t {
					// cpld internal memory checking
					std::uint16_t chip_index;
					if(((frame >> 16) & 0x7f) < 0x14) { // PS Memory -- 0x05
						chip_index = 0;
					}
					else { // PL Memory -- 0x00
						chip_index = 1;
					}
					abstractRoutingTuple _routingTuple(systemChip::c_CPLD_SPI,chip_index,dbOrdinal);
					// set the reference routing tuple to this routing tuple
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_read_spi(frame);
				}
			),
			lmkController(
				[this](std::uint32_t frame)->void {
					abstractRoutingTuple _routingTuple(systemChip::c_LMK04828,0,dbOrdinal); // -- 0x1
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_write_spi(frame);
				},
				[this](std::uint32_t frame)->std::uint32_t {
					abstractRoutingTuple _routingTuple(systemChip::c_LMK04828,0,dbOrdinal); // -- 0x1
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					return this->referenceMiddleware->cpld_read_spi(frame);
				}
			),
			dsaController{{
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,0,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				},
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,1,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				},
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,2,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				},
				{
					[this](std::uint8_t gainSel)->void {
						abstractRoutingTuple _routingTuple(systemChip::c_DSA,3,dbOrdinal); // -- channel
						std::lock_guard<std::mutex>	l(this->_set_mutex);
						this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // R/W to shared resource
						return this->referenceMiddleware->writeDsaIo(gainSel);
					}
				}
			}},
			cpldGpioController(
				[this](bool _set, std::bitset<4> _channels)->void{ // API now expects bitset
					// Note: This routing is controlled outside of the library API?? Uses 'chip select' even though it should not
					// regardless, DON'T lock mutex here
					for(std::uint16_t bit = 0; bit < 4; bit++){ // every bit position that's set
						if(_channels.test(bit)){
							abstractRoutingTuple _routingTuple(systemChip::c_CPLD_GPIO,bit,dbOrdinal); // -- channel
							std::lock_guard<std::mutex> l(this->_set_mutex);
							this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
							this->referenceMiddleware->writeCpldGpio(_set);
						}
					}
				}
			),
			transceiverController( // some of these require dbIndexing information, but no chip select information
				[this](std::uint8_t * _bufIn)->void{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					this->referenceMiddleware->ad9371_write_spi(_bufIn);
				},
				[this](std::uint8_t * _bufIn)->std::uint8_t{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->ad9371_read_spi(_bufIn);
				},
				[this](void)->std::array<std::uint32_t,8>&{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					// the systemChip ^^ and 0 are just fillers.. only need to relay daughterboard indexing information
					// better way?
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->jesd_mem_manager(); // how should get this? Do we need DB select anymore??
				},
				// accesses lmk and cpldGpioController, so those MUST be constructed first in the daughterboard list initialization sequence
				[this](std::uint16_t _pattern)->void{
					this->lmkController.requestSysref(_pattern);
				},
				[this](void)->void{
					this->cpldController.mykonosReset();
				},
				[this](std::uint32_t _addressIn, std::uint32_t _value, bool _wr)->std::uint32_t{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->jesd_mem_mapper(_addressIn,_value,_wr); // how should get this? Do we need DB select anymore??
				} 
			),
			ad9371GpioController(
				[this](void)->std::uint8_t{
					// requires DB mapping to be done here
					abstractRoutingTuple _routingTuple(systemChip::c_AD9371,0,dbOrdinal); // only really needed for dbOrdinal
					// c_AD9371 and 0 are both filler information, NOT USED HERE
					std::lock_guard<std::mutex> l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple); // only for dbOrdinal
					return this->referenceMiddleware->readAd9371Gpio();
				}
			),
			POR(0x7F),
			whichDB(_whichDB),
			active(_active)
	{
		// can't use list constructor for the member objects
		std::cout << "In daughterboard ctor body\n";
		{ // thread safety
			// really drcMiddleware
			std::lock_guard<std::mutex>	l(this->_set_mutex); // R/W to shared resource
			// this->_routingTuple.db_select = daughterboard::_globalDaughterboardCount;
			daughterboard::_globalDaughterboardCount++;
		}
		std::array<std::uint16_t,2> idx = {0,1}; // chip select indexors
		// std::array<std::uint16_t,2> adfCS = {4,3}; // rx, tx actual chip select artifacts from DRC level
		for(auto &x : idx){
			adfController[x] = adf435x_iface::make_adf4351(
				[this,x](std::vector<uint32_t> vecIn)->void{
					abstractRoutingTuple _routingTuple(systemChip::c_ADF4351,x,dbOrdinal); // RX (?) VS TX (?) LO chip indexor
					std::lock_guard<std::mutex>	l(this->_set_mutex);
					this->referenceMiddleware->_routingTuple.setTuple(_routingTuple);
					for(auto &it : vecIn){
						this->referenceMiddleware->cpld_write_spi(it);
					}
					return;
				}
			);
		}
	}
	// ~daughterboard() = default;
	daughterboard() = delete;
	~daughterboard(void){
		;
	}
	std::string resetDaughterboard(void)
	{
		std::stringstream reset_logOut;
		if(active){
			reset_logOut << "\nDaughterboard--" + this->whichDB + ":";
			reset_logOut << "\nPower Off Reset Control";
			// Make a table CHIP | POR
			reset_logOut << "\n-----------------------";
			reset_logOut << "\nCHIP            | POR";
			reset_logOut << "\n-----------------------";
			reset_logOut << "\nCPLD Pathing    |";
			if(this->POR & 0x1){
				reset_logOut << " x";
				this->cpldController.reset(); // call reset routine
				this->POR &= ~(0x1);
			}
			reset_logOut << "\nLMK04828        |";
			if(this->POR & 0x2){
				reset_logOut << " x";
				this->lmkController.reset(); // call reset routine
				this->POR &= ~(0x2);
			}
			reset_logOut << "\nADF4351         |";
			if(this->POR & 0x4){
				reset_logOut << " x";
				for(auto &LO : this->adfController){
					LO->reset();
					// LO->_lo_enable(0.0,0.0,false); // @TODO: Does this reset the lock detect?
					// LO->_lo_disable();
				}
				this->POR &= ~(0x4);
			}
			reset_logOut << "\nDSA             |";
			if(this->POR & 0x8){
				reset_logOut << " x";
				for (int i = 0; i < 4; i++){
					this->dsaController[i].double_to_hex(static_cast<double>(0.0));
				}
				this->POR &= ~(0x8);
			}
			reset_logOut << "\nCPLD GPIO POR   |";
			if(this->POR & 0x10){
				reset_logOut << " x";
				this->cpldGpioController.writeGpio(false,0xF); // set all channels off
				this->POR &= ~(0x10);
			}
			// this reset vector MUST be supplied to the transceiver object, thus this POR bit is deprecated
			#if 0
				reset_logOut << "\nAD9371 GPIO     |";
				if(this->POR & 0x20){
					// Set to nullptr here to not affect constructor sequence
					// @TODO: need to have transceiver.reset functionality that interfaces with the GPIO
					reset_logOut << " x";
					this->cpldController.mykonosReset();
					this->POR &= ~(0x20);
				}
			#endif
			reset_logOut << "\nTransceiver Obj |";
			if(this->POR & 0x40){
				reset_logOut << " x";
				this->transceiverController.reset();
				this->POR &= ~(0x40);
			}
			reset_logOut << "\n-----------------------";
			// log(3,reset_logOut.str().c_str());
		} else {
			reset_logOut << "\nDaughterboard--" + this->whichDB + ": INACTIVE";
			reset_logOut << "\nFailed reset operation";
		}
		return reset_logOut.str();
	}
};

// since we essentiall #pragma once this header file, this static initialization should ONLY be taken care of once
template<typename T>
std::uint16_t daughterboard<T>::_globalDaughterboardCount = 0; // starts off at 0

template<typename T> std::mutex daughterboard<T>::_set_mutex;

#endif // __MG_DAUGHTERBOARD_HPP__