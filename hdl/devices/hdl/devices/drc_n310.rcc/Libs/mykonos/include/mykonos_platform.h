#ifndef _MYKONOS_PLATFORM_H_
#define _MYKONOS_PLATFORM_H_

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
// This structure emulates a C++ structure (eh)
// Method members that MUST BE INITIALIZED, will behave as callback method into driver api
typedef struct {
  uint8_t (*spiRead)(uint8_t * buf);
  void (*spiWrite)(uint8_t * buf);
}  MykonosOpenCPI;

extern MykonosOpenCPI mykonos_callback;

#ifdef __cplusplus
}
#endif

#endif//_MYKONOS_PLATFORM_H_