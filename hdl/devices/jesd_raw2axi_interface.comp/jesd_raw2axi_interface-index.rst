.. jesd_raw2axi_interface documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _jesd_raw2axi_interface:


JESD RAW-to-AXI Interface (``jesd_raw2axi_interface``)
======================================================
RAW properties interface for AXI-based register map programming of the Analog Devices JES204B Framework cores.


Implementations
---------------
.. ocpi_documentation_implementations:: ../jesd_raw2axi_interface.hdl


Dependencies
------------
There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``jesd_raw2axi_interface`` are:

 * Analog Deivces JESD204B Framework - Register Map Addresses

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
