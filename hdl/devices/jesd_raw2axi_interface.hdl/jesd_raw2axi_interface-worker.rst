.. jesd_raw2axi_interface HDL worker


.. _jesd_raw2axi_interface-HDL-worker:


``jesd_raw2axi_interface`` HDL Worker
=====================================

Detail
------
HDL implementation of ``jesd_raw2axi_interface`` device worker in support of the ``n310`` platform.  This device worker maps RAW properties to AXI registers for the Analog Device JESD204B Framework cores implemented within the ``adrv9371_jesd_2tx_2rx`` device worker.

.. ocpi_documentation_worker::

   max_addr: Max address in the register map for raw properties
   tpl_tx_raw: Raw property interface for the JESD Tx Transport Layer
   link_tx_raw: Raw property interface for the JESD Tx Link Layer
   phy_tx_raw: Raw property interface for the JESD Tx Physical Layer
   clk_tx_raw: Raw property interface for the JESD Tx Clock Generator

   tpl_rx_raw: Raw property interface for the JESD Rx Transport Layer
   link_rx_raw: Raw property interface for the JESD Rx Link Layer
   phy_rx_raw: Raw property interface for the JESD Rx Physical Layer
   clk_rx_raw: Raw property interface for the JESD Rx Clock Generator

   tx_event: Transmitter Event Trigger

   tpl_tx_axi: AXI-Lite Interface for JESD Tx Transport Layer
   link_tx_axi: AXI-Lite Interface for JESD Tx Link Layer
   phy_tx_axi: AXI-Lite Interface for JESD Tx Physical Layer
   clk_tx_axi: AXI-Lite Interface for JESD Tx Clock Generator

   tpl_rx_axi: AXI-Lite Interface for JESD Rx Transport Layer
   link_rx_axi: AXI-Lite Interface for JESD Rx Link Layer
   phy_rx_axi: AXI-Lite Interface for JESD Rx Physical Layer 
   clk_rx_axi: AXI-Lite Interface for JESD Rx Clock Generator  


.. Utilization
   -----------
   .. ocpi_documentation_utilization::
