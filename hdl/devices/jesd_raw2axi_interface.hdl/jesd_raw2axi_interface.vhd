library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library axi;

architecture rtl of worker is

    constant addr_split : natural := to_integer(unsigned(MAX_ADDR));

    signal raw_tpl_tx_in  : ocpi.wci.raw_in_t;
    signal raw_tpl_tx_out : ocpi.wci.raw_out_t;

    signal raw_tpl_rx_in  : ocpi.wci.raw_in_t;
    signal raw_tpl_rx_out : ocpi.wci.raw_out_t;

    signal raw_link_tx_in  : ocpi.wci.raw_in_t;
    signal raw_link_tx_out : ocpi.wci.raw_out_t;

    signal raw_link_rx_in  : ocpi.wci.raw_in_t;
    signal raw_link_rx_out : ocpi.wci.raw_out_t;

    signal raw_clk_rx_in  : ocpi.wci.raw_in_t;
    signal raw_clk_rx_out : ocpi.wci.raw_out_t;

    signal raw_clk_tx_in  : ocpi.wci.raw_in_t;
    signal raw_clk_tx_out : ocpi.wci.raw_out_t;

    signal raw_phy_rx_in  : ocpi.wci.raw_in_t;
    signal raw_phy_rx_out : ocpi.wci.raw_out_t;

    signal raw_phy_tx_in  : ocpi.wci.raw_in_t;
    signal raw_phy_tx_out : ocpi.wci.raw_out_t;

    signal tpl_tx_axi_m2s : axi.lite32.axi_m2s_t;
    signal tpl_tx_axi_s2m : axi.lite32.axi_s2m_t;

    signal tpl_rx_axi_m2s : axi.lite32.axi_m2s_t;
    signal tpl_rx_axi_s2m : axi.lite32.axi_s2m_t;

    signal link_tx_axi_m2s : axi.lite32.axi_m2s_t;
    signal link_tx_axi_s2m : axi.lite32.axi_s2m_t;

    signal link_rx_axi_m2s : axi.lite32.axi_m2s_t;
    signal link_rx_axi_s2m : axi.lite32.axi_s2m_t;

    signal clk_rx_axi_m2s : axi.lite32.axi_m2s_t;
    signal clk_rx_axi_s2m : axi.lite32.axi_s2m_t;

    signal clk_tx_axi_m2s : axi.lite32.axi_m2s_t;
    signal clk_tx_axi_s2m : axi.lite32.axi_s2m_t;

    signal phy_rx_axi_m2s : axi.lite32.axi_m2s_t;
    signal phy_rx_axi_s2m : axi.lite32.axi_s2m_t;

    signal phy_tx_axi_m2s : axi.lite32.axi_m2s_t;
    signal phy_tx_axi_s2m : axi.lite32.axi_s2m_t;

    signal which_raw : std_logic_vector(2 downto 0);

begin

    tpl_axi_tx_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_tpl_tx_in,
        raw_out => raw_tpl_tx_out,
        axi_in  => tpl_tx_axi_s2m,
        axi_out => tpl_tx_axi_m2s);

    link_tx_axi_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_link_tx_in,
        raw_out => raw_link_tx_out,
        axi_in  => link_tx_axi_s2m,
        axi_out => link_tx_axi_m2s);

    tpl_axi_rx_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_tpl_rx_in,
        raw_out => raw_tpl_rx_out,
        axi_in  => tpl_rx_axi_s2m,
        axi_out => tpl_rx_axi_m2s);

    link_rx_axi_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_link_rx_in,
        raw_out => raw_link_rx_out,
        axi_in  => link_rx_axi_s2m,
        axi_out => link_rx_axi_m2s);

    clk_rx_axi_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_clk_rx_in,
        raw_out => raw_clk_rx_out,
        axi_in  => clk_rx_axi_s2m,
        axi_out => clk_rx_axi_m2s);

    clk_tx_axi_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_clk_tx_in,
        raw_out => raw_clk_tx_out,
        axi_in  => clk_tx_axi_s2m,
        axi_out => clk_tx_axi_m2s);

    phy_rx_axi_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_phy_rx_in,
        raw_out => raw_phy_rx_out,
        axi_in  => phy_rx_axi_s2m,
        axi_out => phy_rx_axi_m2s);

    phy_tx_axi_raw : axi.lite32.raw2axi_lite32
    port map(
        clk     => ctl_in.clk,
        reset   => ctl_in.reset,
        raw_in  => raw_phy_tx_in,
        raw_out => raw_phy_tx_out,
        axi_in  => phy_tx_axi_s2m,
        axi_out => phy_tx_axi_m2s);

    which_raw <=    "011" when (to_integer(props_in.raw.address) < addr_split*4)   else
                    "010" when (to_integer(props_in.raw.address) < addr_split*4*2) else
                    "001" when (to_integer(props_in.raw.address) < addr_split*4*3) else
                    "000" when (to_integer(props_in.raw.address) < addr_split*4*4) else
                    "100" when (to_integer(props_in.raw.address) < addr_split*4*5) else
                    "101" when (to_integer(props_in.raw.address) < addr_split*4*6) else
                    "110" when (to_integer(props_in.raw.address) < addr_split*4*7) else
                    "111";

    props_out.raw <=    raw_tpl_tx_out  when which_raw = "011" else 
                        raw_link_tx_out when which_raw = "010" else 
                        raw_tpl_rx_out  when which_raw = "001" else 
                        raw_link_rx_out when which_raw = "000" else
                        raw_clk_rx_out  when which_raw = "100" else
                        raw_clk_tx_out  when which_raw = "101" else
                        raw_phy_rx_out  when which_raw = "110" else
                        raw_phy_tx_out;-- else ocpi.wci.raw_out_t;

    raw_tpl_tx_in.address      <= props_in.raw.address     when which_raw = "011" else (others => '0');
    raw_tpl_tx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "011" else (others => '0');
    raw_tpl_tx_in.is_read      <= props_in.raw.is_read     when which_raw = "011" else '0';
    raw_tpl_tx_in.is_write     <= props_in.raw.is_write    when which_raw = "011" else '0';
    raw_tpl_tx_in.data         <= props_in.raw.data        when which_raw = "011" else (others => '0');

    raw_link_tx_in.address      <= unsigned(to_unsigned(to_integer(props_in.raw.address), raw_link_tx_in.address'length) - to_unsigned(addr_split*4, raw_link_tx_in.address'length)) when which_raw = "010" else (others => '0');
    raw_link_tx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "010" else (others => '0');
    raw_link_tx_in.is_read      <= props_in.raw.is_read     when which_raw = "010" else '0';
    raw_link_tx_in.is_write     <= props_in.raw.is_write    when which_raw = "010" else '0';
    raw_link_tx_in.data         <= props_in.raw.data        when which_raw = "010" else (others => '0');

    raw_tpl_rx_in.address      <= unsigned(to_unsigned(to_integer(props_in.raw.address), raw_tpl_rx_in.address'length) - to_unsigned(addr_split*4*2, raw_tpl_rx_in.address'length)) when which_raw = "001" else (others => '0');
    raw_tpl_rx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "001" else (others => '0');
    raw_tpl_rx_in.is_read      <= props_in.raw.is_read     when which_raw = "001" else '0';
    raw_tpl_rx_in.is_write     <= props_in.raw.is_write    when which_raw = "001" else '0';
    raw_tpl_rx_in.data         <= props_in.raw.data        when which_raw = "001" else (others => '0');

    raw_link_rx_in.address      <= unsigned(to_unsigned(to_integer(props_in.raw.address), raw_link_rx_in.address'length) - to_unsigned(addr_split*4*3, raw_link_rx_in.address'length)) when which_raw = "000" else (others => '0');
    raw_link_rx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "000" else (others => '0');
    raw_link_rx_in.is_read      <= props_in.raw.is_read     when which_raw = "000" else '0';
    raw_link_rx_in.is_write     <= props_in.raw.is_write    when which_raw = "000" else '0';
    raw_link_rx_in.data         <= props_in.raw.data        when which_raw = "000" else (others => '0');

    raw_clk_rx_in.address      <= unsigned(to_unsigned(to_integer(props_in.raw.address), raw_clk_rx_in.address'length) - to_unsigned(addr_split*4*4, raw_clk_rx_in.address'length)) when which_raw = "100" else (others => '0');
    raw_clk_rx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "100" else (others => '0');
    raw_clk_rx_in.is_read      <= props_in.raw.is_read     when which_raw = "100" else '0';
    raw_clk_rx_in.is_write     <= props_in.raw.is_write    when which_raw = "100" else '0';
    raw_clk_rx_in.data         <= props_in.raw.data        when which_raw = "100" else (others => '0');

    raw_clk_tx_in.address      <= unsigned(to_unsigned(to_integer(props_in.raw.address), raw_clk_tx_in.address'length) - to_unsigned(addr_split*4*5, raw_clk_tx_in.address'length)) when which_raw = "101" else (others => '0');
    raw_clk_tx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "101" else (others => '0');
    raw_clk_tx_in.is_read      <= props_in.raw.is_read     when which_raw = "101" else '0';
    raw_clk_tx_in.is_write     <= props_in.raw.is_write    when which_raw = "101" else '0';
    raw_clk_tx_in.data         <= props_in.raw.data        when which_raw = "101" else (others => '0');

    raw_phy_rx_in.address      <= unsigned(to_unsigned(to_integer(props_in.raw.address), raw_phy_rx_in.address'length) - to_unsigned(addr_split*4*6, raw_phy_rx_in.address'length)) when which_raw = "110" else (others => '0');
    raw_phy_rx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "110" else (others => '0');
    raw_phy_rx_in.is_read      <= props_in.raw.is_read     when which_raw = "110" else '0';
    raw_phy_rx_in.is_write     <= props_in.raw.is_write    when which_raw = "110" else '0';
    raw_phy_rx_in.data         <= props_in.raw.data        when which_raw = "110" else (others => '0');

    raw_phy_tx_in.address      <= unsigned(to_unsigned(to_integer(props_in.raw.address), raw_clk_tx_in.address'length) - to_unsigned(addr_split*4*7, raw_clk_tx_in.address'length)) when which_raw = "111" else (others => '0');
    raw_phy_tx_in.byte_enable  <= props_in.raw.byte_enable when which_raw = "111" else (others => '0');
    raw_phy_tx_in.is_read      <= props_in.raw.is_read     when which_raw = "111" else '0';
    raw_phy_tx_in.is_write     <= props_in.raw.is_write    when which_raw = "111" else '0';
    raw_phy_tx_in.data         <= props_in.raw.data        when which_raw = "111" else (others => '0');

    link_tx_axi_out.araddr           <= link_tx_axi_m2s.AR.ADDR(31 downto 0);
    link_tx_axi_out.arprot           <= link_tx_axi_m2s.AR.PROT;
    link_tx_axi_out.arvalid          <= link_tx_axi_m2s.AR.VALID;
    link_tx_axi_out.awaddr           <= link_tx_axi_m2s.AW.ADDR(31 downto 0);
    link_tx_axi_out.awprot           <= link_tx_axi_m2s.AW.PROT;
    link_tx_axi_out.awvalid          <= link_tx_axi_m2s.AW.VALID;
    link_tx_axi_out.bready           <= link_tx_axi_m2s.B.READY;
    link_tx_axi_out.rready           <= link_tx_axi_m2s.R.READY;
    link_tx_axi_out.wdata            <= link_tx_axi_m2s.W.DATA;
    link_tx_axi_out.wstrb            <= link_tx_axi_m2s.W.STRB;
    link_tx_axi_out.wvalid           <= link_tx_axi_m2s.W.VALID;
    link_tx_axi_out.s_axi_aclk       <= link_tx_axi_m2s.A.CLK;
    link_tx_axi_out.s_axi_aresetn    <= link_tx_axi_m2s.A.RESETn;

    link_tx_axi_s2m.aw.READY <= link_tx_axi_in.awready;
    link_tx_axi_s2m.w.READY  <= link_tx_axi_in.wready;
    link_tx_axi_s2m.b.RESP   <= link_tx_axi_in.bresp;
    link_tx_axi_s2m.b.VALID  <= link_tx_axi_in.bvalid;
    link_tx_axi_s2m.ar.READY <= link_tx_axi_in.arready;
    link_tx_axi_s2m.r.DATA   <= link_tx_axi_in.rdata;
    link_tx_axi_s2m.r.RESP   <= link_tx_axi_in.rresp;
    link_tx_axi_s2m.r.VALID  <= link_tx_axi_in.rvalid;

    link_rx_axi_out.araddr           <= link_rx_axi_m2s.AR.ADDR(31 downto 0);
    link_rx_axi_out.arprot           <= link_rx_axi_m2s.AR.PROT;
    link_rx_axi_out.arvalid          <= link_rx_axi_m2s.AR.VALID;
    link_rx_axi_out.awaddr           <= link_rx_axi_m2s.AW.ADDR(31 downto 0);
    link_rx_axi_out.awprot           <= link_rx_axi_m2s.AW.PROT;
    link_rx_axi_out.awvalid          <= link_rx_axi_m2s.AW.VALID;
    link_rx_axi_out.bready           <= link_rx_axi_m2s.B.READY;
    link_rx_axi_out.rready           <= link_rx_axi_m2s.R.READY;
    link_rx_axi_out.wdata            <= link_rx_axi_m2s.W.DATA;
    link_rx_axi_out.wstrb            <= link_rx_axi_m2s.W.STRB;
    link_rx_axi_out.wvalid           <= link_rx_axi_m2s.W.VALID;
    link_rx_axi_out.s_axi_aclk       <= link_rx_axi_m2s.A.CLK;
    link_rx_axi_out.s_axi_aresetn    <= link_rx_axi_m2s.A.RESETn;

    link_rx_axi_s2m.aw.READY <= link_rx_axi_in.awready;
    link_rx_axi_s2m.w.READY  <= link_rx_axi_in.wready;
    link_rx_axi_s2m.b.RESP   <= link_rx_axi_in.bresp;
    link_rx_axi_s2m.b.VALID  <= link_rx_axi_in.bvalid;
    link_rx_axi_s2m.ar.READY <= link_rx_axi_in.arready;
    link_rx_axi_s2m.r.DATA   <= link_rx_axi_in.rdata;
    link_rx_axi_s2m.r.RESP   <= link_rx_axi_in.rresp;
    link_rx_axi_s2m.r.VALID  <= link_rx_axi_in.rvalid;

    tpl_tx_axi_out.araddr           <= tpl_tx_axi_m2s.AR.ADDR(31 downto 0);
    tpl_tx_axi_out.arprot           <= tpl_tx_axi_m2s.AR.PROT;
    tpl_tx_axi_out.arvalid          <= tpl_tx_axi_m2s.AR.VALID;
    tpl_tx_axi_out.awaddr           <= tpl_tx_axi_m2s.AW.ADDR(31 downto 0);
    tpl_tx_axi_out.awprot           <= tpl_tx_axi_m2s.AW.PROT;
    tpl_tx_axi_out.awvalid          <= tpl_tx_axi_m2s.AW.VALID;
    tpl_tx_axi_out.bready           <= tpl_tx_axi_m2s.B.READY;
    tpl_tx_axi_out.rready           <= tpl_tx_axi_m2s.R.READY;
    tpl_tx_axi_out.wdata            <= tpl_tx_axi_m2s.W.DATA;
    tpl_tx_axi_out.wstrb            <= tpl_tx_axi_m2s.W.STRB;
    tpl_tx_axi_out.wvalid           <= tpl_tx_axi_m2s.W.VALID;
    tpl_tx_axi_out.s_axi_aclk       <= tpl_tx_axi_m2s.A.CLK;
    tpl_tx_axi_out.s_axi_aresetn    <= tpl_tx_axi_m2s.A.RESETn;

    tpl_tx_axi_s2m.aw.READY <= tpl_tx_axi_in.awready;
    tpl_tx_axi_s2m.w.READY  <= tpl_tx_axi_in.wready;
    tpl_tx_axi_s2m.b.RESP   <= tpl_tx_axi_in.bresp;
    tpl_tx_axi_s2m.b.VALID  <= tpl_tx_axi_in.bvalid;
    tpl_tx_axi_s2m.ar.READY <= tpl_tx_axi_in.arready;
    tpl_tx_axi_s2m.r.DATA   <= tpl_tx_axi_in.rdata;
    tpl_tx_axi_s2m.r.RESP   <= tpl_tx_axi_in.rresp;
    tpl_tx_axi_s2m.r.VALID  <= tpl_tx_axi_in.rvalid;

    tpl_rx_axi_out.araddr           <= tpl_rx_axi_m2s.AR.ADDR(31 downto 0);
    tpl_rx_axi_out.arprot           <= tpl_rx_axi_m2s.AR.PROT;
    tpl_rx_axi_out.arvalid          <= tpl_rx_axi_m2s.AR.VALID;
    tpl_rx_axi_out.awaddr           <= tpl_rx_axi_m2s.AW.ADDR(31 downto 0);
    tpl_rx_axi_out.awprot           <= tpl_rx_axi_m2s.AW.PROT;
    tpl_rx_axi_out.awvalid          <= tpl_rx_axi_m2s.AW.VALID;
    tpl_rx_axi_out.bready           <= tpl_rx_axi_m2s.B.READY;
    tpl_rx_axi_out.rready           <= tpl_rx_axi_m2s.R.READY;
    tpl_rx_axi_out.wdata            <= tpl_rx_axi_m2s.W.DATA;
    tpl_rx_axi_out.wstrb            <= tpl_rx_axi_m2s.W.STRB;
    tpl_rx_axi_out.wvalid           <= tpl_rx_axi_m2s.W.VALID;
    tpl_rx_axi_out.s_axi_aclk       <= tpl_rx_axi_m2s.A.CLK;
    tpl_rx_axi_out.s_axi_aresetn    <= tpl_rx_axi_m2s.A.RESETn;

    tpl_rx_axi_s2m.aw.READY <= tpl_rx_axi_in.awready;
    tpl_rx_axi_s2m.w.READY  <= tpl_rx_axi_in.wready;
    tpl_rx_axi_s2m.b.RESP   <= tpl_rx_axi_in.bresp;
    tpl_rx_axi_s2m.b.VALID  <= tpl_rx_axi_in.bvalid;
    tpl_rx_axi_s2m.ar.READY <= tpl_rx_axi_in.arready;
    tpl_rx_axi_s2m.r.DATA   <= tpl_rx_axi_in.rdata;
    tpl_rx_axi_s2m.r.RESP   <= tpl_rx_axi_in.rresp;
    tpl_rx_axi_s2m.r.VALID  <= tpl_rx_axi_in.rvalid;

    clk_rx_axi_out.araddr           <= clk_rx_axi_m2s.AR.ADDR(31 downto 0);
    clk_rx_axi_out.arprot           <= clk_rx_axi_m2s.AR.PROT;
    clk_rx_axi_out.arvalid          <= clk_rx_axi_m2s.AR.VALID;
    clk_rx_axi_out.awaddr           <= clk_rx_axi_m2s.AW.ADDR(31 downto 0);
    clk_rx_axi_out.awprot           <= clk_rx_axi_m2s.AW.PROT;
    clk_rx_axi_out.awvalid          <= clk_rx_axi_m2s.AW.VALID;
    clk_rx_axi_out.bready           <= clk_rx_axi_m2s.B.READY;
    clk_rx_axi_out.rready           <= clk_rx_axi_m2s.R.READY;
    clk_rx_axi_out.wdata            <= clk_rx_axi_m2s.W.DATA;
    clk_rx_axi_out.wstrb            <= clk_rx_axi_m2s.W.STRB;
    clk_rx_axi_out.wvalid           <= clk_rx_axi_m2s.W.VALID;
    clk_rx_axi_out.s_axi_aclk       <= clk_rx_axi_m2s.A.CLK;
    clk_rx_axi_out.s_axi_aresetn    <= clk_rx_axi_m2s.A.RESETn;

    clk_rx_axi_s2m.aw.READY <= clk_rx_axi_in.awready;
    clk_rx_axi_s2m.w.READY  <= clk_rx_axi_in.wready;
    clk_rx_axi_s2m.b.RESP   <= clk_rx_axi_in.bresp;
    clk_rx_axi_s2m.b.VALID  <= clk_rx_axi_in.bvalid;
    clk_rx_axi_s2m.ar.READY <= clk_rx_axi_in.arready;
    clk_rx_axi_s2m.r.DATA   <= clk_rx_axi_in.rdata;
    clk_rx_axi_s2m.r.RESP   <= clk_rx_axi_in.rresp;
    clk_rx_axi_s2m.r.VALID  <= clk_rx_axi_in.rvalid;

    clk_tx_axi_out.araddr           <= clk_tx_axi_m2s.AR.ADDR(31 downto 0);
    clk_tx_axi_out.arprot           <= clk_tx_axi_m2s.AR.PROT;
    clk_tx_axi_out.arvalid          <= clk_tx_axi_m2s.AR.VALID;
    clk_tx_axi_out.awaddr           <= clk_tx_axi_m2s.AW.ADDR(31 downto 0);
    clk_tx_axi_out.awprot           <= clk_tx_axi_m2s.AW.PROT;
    clk_tx_axi_out.awvalid          <= clk_tx_axi_m2s.AW.VALID;
    clk_tx_axi_out.bready           <= clk_tx_axi_m2s.B.READY;
    clk_tx_axi_out.rready           <= clk_tx_axi_m2s.R.READY;
    clk_tx_axi_out.wdata            <= clk_tx_axi_m2s.W.DATA;
    clk_tx_axi_out.wstrb            <= clk_tx_axi_m2s.W.STRB;
    clk_tx_axi_out.wvalid           <= clk_tx_axi_m2s.W.VALID;
    clk_tx_axi_out.s_axi_aclk       <= clk_tx_axi_m2s.A.CLK;
    clk_tx_axi_out.s_axi_aresetn    <= clk_tx_axi_m2s.A.RESETn;

    clk_tx_axi_s2m.aw.READY <= clk_tx_axi_in.awready;
    clk_tx_axi_s2m.w.READY  <= clk_tx_axi_in.wready;
    clk_tx_axi_s2m.b.RESP   <= clk_tx_axi_in.bresp;
    clk_tx_axi_s2m.b.VALID  <= clk_tx_axi_in.bvalid;
    clk_tx_axi_s2m.ar.READY <= clk_tx_axi_in.arready;
    clk_tx_axi_s2m.r.DATA   <= clk_tx_axi_in.rdata;
    clk_tx_axi_s2m.r.RESP   <= clk_tx_axi_in.rresp;
    clk_tx_axi_s2m.r.VALID  <= clk_tx_axi_in.rvalid;

    phy_rx_axi_out.araddr           <= phy_rx_axi_m2s.AR.ADDR(31 downto 0);
    phy_rx_axi_out.arprot           <= phy_rx_axi_m2s.AR.PROT;
    phy_rx_axi_out.arvalid          <= phy_rx_axi_m2s.AR.VALID;
    phy_rx_axi_out.awaddr           <= phy_rx_axi_m2s.AW.ADDR(31 downto 0);
    phy_rx_axi_out.awprot           <= phy_rx_axi_m2s.AW.PROT;
    phy_rx_axi_out.awvalid          <= phy_rx_axi_m2s.AW.VALID;
    phy_rx_axi_out.bready           <= phy_rx_axi_m2s.B.READY;
    phy_rx_axi_out.rready           <= phy_rx_axi_m2s.R.READY;
    phy_rx_axi_out.wdata            <= phy_rx_axi_m2s.W.DATA;
    phy_rx_axi_out.wstrb            <= phy_rx_axi_m2s.W.STRB;
    phy_rx_axi_out.wvalid           <= phy_rx_axi_m2s.W.VALID;
    phy_rx_axi_out.s_axi_aclk       <= phy_rx_axi_m2s.A.CLK;
    phy_rx_axi_out.s_axi_aresetn    <= phy_rx_axi_m2s.A.RESETn;

    phy_rx_axi_s2m.aw.READY <= phy_rx_axi_in.awready;
    phy_rx_axi_s2m.w.READY  <= phy_rx_axi_in.wready;
    phy_rx_axi_s2m.b.RESP   <= phy_rx_axi_in.bresp;
    phy_rx_axi_s2m.b.VALID  <= phy_rx_axi_in.bvalid;
    phy_rx_axi_s2m.ar.READY <= phy_rx_axi_in.arready;
    phy_rx_axi_s2m.r.DATA   <= phy_rx_axi_in.rdata;
    phy_rx_axi_s2m.r.RESP   <= phy_rx_axi_in.rresp;
    phy_rx_axi_s2m.r.VALID  <= phy_rx_axi_in.rvalid;

    phy_tx_axi_out.araddr           <= phy_tx_axi_m2s.AR.ADDR(31 downto 0);
    phy_tx_axi_out.arprot           <= phy_tx_axi_m2s.AR.PROT;
    phy_tx_axi_out.arvalid          <= phy_tx_axi_m2s.AR.VALID;
    phy_tx_axi_out.awaddr           <= phy_tx_axi_m2s.AW.ADDR(31 downto 0);
    phy_tx_axi_out.awprot           <= phy_tx_axi_m2s.AW.PROT;
    phy_tx_axi_out.awvalid          <= phy_tx_axi_m2s.AW.VALID;
    phy_tx_axi_out.bready           <= phy_tx_axi_m2s.B.READY;
    phy_tx_axi_out.rready           <= phy_tx_axi_m2s.R.READY;
    phy_tx_axi_out.wdata            <= phy_tx_axi_m2s.W.DATA;
    phy_tx_axi_out.wstrb            <= phy_tx_axi_m2s.W.STRB;
    phy_tx_axi_out.wvalid           <= phy_tx_axi_m2s.W.VALID;
    phy_tx_axi_out.s_axi_aclk       <= phy_tx_axi_m2s.A.CLK;
    phy_tx_axi_out.s_axi_aresetn    <= phy_tx_axi_m2s.A.RESETn;

    phy_tx_axi_s2m.aw.READY <= phy_tx_axi_in.awready;
    phy_tx_axi_s2m.w.READY  <= phy_tx_axi_in.wready;
    phy_tx_axi_s2m.b.RESP   <= phy_tx_axi_in.bresp;
    phy_tx_axi_s2m.b.VALID  <= phy_tx_axi_in.bvalid;
    phy_tx_axi_s2m.ar.READY <= phy_tx_axi_in.arready;
    phy_tx_axi_s2m.r.DATA   <= phy_tx_axi_in.rdata;
    phy_tx_axi_s2m.r.RESP   <= phy_tx_axi_in.rresp;
    phy_tx_axi_s2m.r.VALID  <= phy_tx_axi_in.rvalid;

end rtl;
