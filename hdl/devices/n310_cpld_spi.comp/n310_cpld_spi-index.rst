.. n310_cpld_spi documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _n310_cpld_spi:


N310 CPLD SPI Interface (``n310_cpld_spi``)
===========================================
SPI communication interface for the Master CPLD Controller on each daughterboard.  This device worker supports register map programming of the following hardware chips: CPLD, Clock Source, and TX/RX LO Synthesizers.  Each hardware chip has a unique SPI frame structure.  

Design
------

Interface
---------
.. literalinclude:: ../specs/n310_cpld_spi-spec.xml
   :language: xml
   :lines: 2-9,12


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Implementations
---------------
.. ocpi_documentation_implementations:: ../n310_cpld_spi.hdl


Dependencies
------------

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``n310_cpld_spi`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
