.. n310_cpld_spi HDL worker


.. _n310_cpld_spi-HDL-worker:


``n310_cpld_spi`` HDL Worker
============================

Detail
------
HDL implementation of ``n310_cpld_spi`` device worker in support of the ``n310`` platform. 

.. ocpi_documentation_worker::

	VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)


Signal Ports
~~~~~~~~~~~~
.. literalinclude:: ./n310_cpld_spi.xml
   :language: xml
   :lines: 3-33


.. Utilization
   -----------
   .. ocpi_documentation_utilization::



