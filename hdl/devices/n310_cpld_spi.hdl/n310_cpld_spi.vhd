-- THIS FILE WAS ORIGINALLY GENERATED ON Thu Mar 18 12:54:21 2021 EDT
-- BASED ON THE FILE: cpld_spi.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all;
library work; use work.cpld_spi_ila_pkg.all;
use ocpi.wci.all;

architecture rtl of worker is

  constant caddr_width_c  : integer := 2;
  
  type   state_t is (idle_e, starting_e, busy_e, flush_e, done_e);
  signal state_r      : state_t := idle_e;
  
  -- Internal signals
  signal caddr            : unsigned(caddr_width_c downto 0);
  signal fdata            : ulong_t := (others => '0');
  signal rdata            : std_logic_vector(15 downto 0) := (others => '0');
  signal preamble         : integer := 0;
  signal addr_width       : integer := 0;
  signal data_width       : integer := 0;
  signal frame_length     : integer := 0;
  signal clock_divisor    : integer := 0;
  

  --Component Spec Property Signals
  signal CPLD_DONE      : bool_t   := bfalse;
  signal CPLD_RDATA     : ushort_t  := (others => '0');
  signal CPLD_DB_SEL    : bool_t   := bfalse;
  signal CPLD_RENABLE   : bool_t;
  signal CPLD_WENABLE   : bool_t;

  --Primitive
  signal clk                : std_logic;
  signal reset              : std_logic := '0';
  signal done           : bool_t := bfalse;
  
  -- The SPI signals - a 4 wire interface
  signal sdi                 : std_logic := '0';
  signal sclk                : std_logic := '0';
  signal sen                 : std_logic := '0';
  signal sdo                 : std_logic := '0';

  signal bit_count           : integer := 0;
  signal clk_count           : integer := 0; 

  signal dba_ps_sclk         : std_logic_vector(0 downto 0);
  signal dba_ps_sdo          : std_logic_vector(0 downto 0);
  signal dba_ps_sdi          : std_logic_vector(0 downto 0);
  signal dba_cpld_pscsb      : std_logic_vector(0 downto 0);
  signal dba_lmk_csb         : std_logic_vector(0 downto 0);
  signal dba_dac_csb         : std_logic_vector(0 downto 0);
  signal dba_pl_sclk         : std_logic_vector(0 downto 0);
  signal dba_pl_sdo          : std_logic_vector(0 downto 0);
  signal dba_pl_sdi          : std_logic_vector(0 downto 0);
  signal dba_cpld_plcsb      : std_logic_vector(0 downto 0);
  signal dba_rxlo_csb        : std_logic_vector(0 downto 0);
  signal dba_txlo_csb        : std_logic_vector(0 downto 0);
  signal dbb_ps_sclk         : std_logic_vector(0 downto 0);
  signal dbb_ps_sdo          : std_logic_vector(0 downto 0);
  signal dbb_ps_sdi          : std_logic_vector(0 downto 0);
  signal dbb_cpld_pscsb      : std_logic_vector(0 downto 0);
  signal dbb_lmk_csb         : std_logic_vector(0 downto 0);
  signal dbb_dac_csb         : std_logic_vector(0 downto 0);
  signal dbb_pl_sclk         : std_logic_vector(0 downto 0);
  signal dbb_pl_sdo          : std_logic_vector(0 downto 0);
  signal dbb_pl_sdi          : std_logic_vector(0 downto 0);
  signal dbb_cpld_plcsb      : std_logic_vector(0 downto 0);
  signal dbb_rxlo_csb        : std_logic_vector(0 downto 0);
  signal dbb_txlo_csb        : std_logic_vector(0 downto 0);



begin

  clk                  <= ctl_in.clk;
  reset                <= ctl_in.reset;
    
  CPLD_DB_SEL           <= props_in.CPLD_DB_SEL;
  CPLD_RENABLE          <= props_in.CPLD_RENABLE and ctl_in.is_operating;
  CPLD_WENABLE          <= props_in.CPLD_WENABLE and ctl_in.is_operating; 
  caddr                 <= props_in.CPLD_ADDR_CS(caddr_width_c downto 0);

  fdata                 <= props_in.CPLD_FRAME; --Full Frame
  props_out.CPLD_RDATA  <= unsigned(rdata);
  props_out.CPLD_DONE   <= done;
  done                  <= to_bool(state_r = done_e);

  clock_divisor <= 100;

  --Possible Future Clock Divisor
  --clock_divisor <= to_integer(to_unsigned((from_float(CLK_FREQ_p)*2)/9000000,10)) when caddr = "000" else -- 9 MHz
  --                 to_integer(to_unsigned((from_float(CLK_FREQ_p)*2)/20000000,10)) when caddr = "010" else  -- 20 MHz
  --                 to_integer(to_unsigned(from_float(CLK_FREQ_p)/from_float(SPI_CLK_FREQ_p),16));


--Frame Configurations
  preamble     <=  1 when caddr = "000" else --CPLD Condition
                   3 when caddr = "001" else --LMK Condition
                   0 when caddr = "011" else --ADF Condition
                   0 when caddr = "100" else --ADF Condition
                   1; --default
  
  addr_width   <=  7 when caddr = "000" else --CPLD Condition
                  13 when caddr = "001" else --LMK Condition
                   3 when caddr = "011" else -- TX ADF Condition
                   3 when caddr = "100" else -- RX ADF Condition
                   7; --default

  data_width   <= 16 when caddr = "000" else --CPLD Condition
                   8 when caddr = "001" else --LMK Condition
                  29 when caddr = "011" else --TX ADF Condition
                  29 when caddr = "100" else --RX ADF Condition
                  16; --default
  
  --Full Frame Length Needed
  frame_length <= 24 when caddr = "000" else --CPLD Condition
                  24 when caddr = "001" else --LMK Condition
                  32 when caddr = "011" else --TX ADF Condition
                  32 when caddr = "100" else --RX ADF Condition
                  24; --default

  
  sen <=  '0' when ((state_r = starting_e) or (state_r = busy_e) or (state_r = flush_e)) else '1'; -- enable asserted low
  
  sdo <=  fdata(frame_length - bit_count - 1) when (CPLD_WENABLE and (state_r = busy_e)) else --Writing
          fdata(frame_length - bit_count - 1) when (CPLD_RENABLE and (bit_count <  (preamble + addr_width)) and (state_r = busy_e)) else --Reading
          '0'; 


  spiRead: process(sclk) is
  begin
    if (rising_edge(sclk)) then
      if (CPLD_RENABLE = '1' and (bit_count > (preamble + addr_width - 1))) then
            rdata(data_width - (bit_count - (preamble + addr_width - 1))) <= sdi;
      end if;
    end if;
  end process;


  spiSclk: process(clk) is 
  begin
      if (rising_edge(clk)) then
        if (reset = '1') then
          sclk <= '0';
        else
          case (state_r) is
            when idle_e =>
              if (CPLD_RENABLE or CPLD_WENABLE) then
                state_r <= starting_e;
              else
                state_r <= idle_e;
              end if;
            when starting_e =>
              if (clk_count = clock_divisor/2 -1) then
                state_r     <= busy_e;
                bit_count   <= 0;
                clk_count   <= 0;
              else
                state_r <= starting_e;
                clk_count <= clk_count + 1;
              end if; 
            when busy_e =>
              if (clk_count = clock_divisor - 1) then -- end of bit cycle
                clk_count <= 0;
                if (bit_count = frame_length - 1) then -- end of access
                    sclk <= '0';
                    state_r <= flush_e;
                else
                  sclk <=  not sclk;
                  bit_count   <= bit_count + 1;
                end if;
              else
                clk_count <= clk_count + 1;
                if (clk_count = clock_divisor/2 - 1) then -- mid-cycle, read data is captured
                  sclk <= not sclk;
                else
                  sclk <= sclk;
                end if;
              end if;
            when flush_e =>
              if (clk_count = clock_divisor/2 - 1) then
                state_r <= flush_e;
                clk_count <= clk_count + 1;
              elsif (clk_count = clock_divisor - 1) then
                state_r <= done_e;
                clk_count <= 0;
              else
                state_r <= flush_e;
                clk_count <= clk_count + 1;
              end if;
            when done_e =>
              sclk <= '0';
              if (CPLD_RENABLE or CPLD_WENABLE) then
                state_r <= done_e;
              else
                state_r <= idle_e;
              end if;
          end case;
        end if;
      end if;
  end process;


  spiInterface: process(clk) is 
  begin
    if (rising_edge(clk)) then
      if (CPLD_DB_SEL = '0') then
    
    
          --Daughter Board A Signals
              dbb_ps_sclk(0) <= '0';
              dbb_ps_sdo(0) <= '0';
              dbb_ps_sdi(0) <= '0';
              dbb_cpld_pscsb(0) <= '1';
              dbb_lmk_csb(0) <= '1';
              dbb_dac_csb(0) <= '1';
    
              dbb_pl_sclk(0) <= '0';
              dbb_pl_sdo(0) <= '0';
              dbb_pl_sdi(0) <= '0';
              dbb_cpld_plcsb(0) <= '1';
              dbb_rxlo_csb(0) <= '1'; 
              dbb_txlo_csb(0) <= '1';
    
          case (caddr) is 
            -- 000: CPLD(PL), 001: LMK(PS), 010: DAC(PS/PL), 011: TXLO(PL), 100: RXLO(PL), 101:CPLD(PS)
            when "000" =>
              dba_ps_sclk(0) <= '0';
              dba_ps_sdo(0) <= '0';
              dba_ps_sdi(0) <= '0';
              dba_cpld_pscsb(0) <= '1';
              dba_lmk_csb(0) <= '1';
              dba_dac_csb(0) <= '1';
    
              dba_pl_sclk(0) <= sclk;
              dba_pl_sdo(0) <= sdo;
              dba_pl_sdi(0) <= DBA_CPLD_PL_SDI;
              dba_cpld_plcsb(0) <= sen;
              dba_rxlo_csb(0) <= '1'; 
              dba_txlo_csb(0) <= '1';
    
              sdi <= DBA_CPLD_PL_SDI;
    
            when "001" =>
              dba_ps_sclk(0) <= sclk;
              dba_ps_sdo(0) <= sdo;
              dba_ps_sdi(0) <= DBA_CPLD_PS_SDI;
              dba_cpld_pscsb(0) <= '1';
              dba_lmk_csb(0) <= sen;
              dba_dac_csb(0) <= '1';
    
              dba_pl_sclk(0) <= '0';
              dba_pl_sdo(0) <= '0';
              dba_cpld_plcsb(0) <= '1';
              dba_rxlo_csb(0) <= '1'; 
              dba_txlo_csb(0) <= '1'; 
    
              sdi <= DBA_CPLD_PS_SDI;
        
            when "010" =>
              dba_ps_sclk(0) <= sclk;
              dba_ps_sdo(0) <= sdo;
              dba_ps_sdi(0) <= DBA_CPLD_PS_SDI;
              dba_cpld_pscsb(0) <= '1';
              dba_lmk_csb(0) <= '1';
              dba_dac_csb(0) <= sen;
    
              dba_pl_sclk(0) <= '0';
              dba_pl_sdo(0) <= '0';
              dba_cpld_plcsb(0) <= '1';
              dba_rxlo_csb(0) <= '1'; 
              dba_txlo_csb(0) <= '1';
    
              sdi <= DBA_CPLD_PS_SDI;
        
            when "011" =>
              dba_ps_sclk(0) <= '0';
              dba_ps_sdo(0) <= '0';
              dba_ps_sdi(0) <= '0';
              dba_cpld_pscsb(0) <= '1';
              dba_lmk_csb(0) <= '1';
              dba_dac_csb(0) <= '1';
    
              dba_pl_sclk(0) <= sclk;
              dba_pl_sdo(0) <= sdo;
              dba_cpld_plcsb(0) <= '1';
              dba_rxlo_csb(0) <= sen; 
              dba_txlo_csb(0) <= '1';
    
              sdi <= '0';
      
            when "100" =>
              dba_ps_sclk(0) <= '0';
              dba_ps_sdo(0) <= '0';
              dba_ps_sdi(0) <= '0';
              dba_cpld_pscsb(0) <= '1';
              dba_lmk_csb(0) <= '1';
              dba_dac_csb(0) <= '1';
    
              dba_pl_sclk(0) <= sclk;
              dba_pl_sdo(0) <= sdo;
              dba_cpld_plcsb(0) <= '1';
              dba_rxlo_csb(0) <= '1'; 
              dba_txlo_csb(0) <= sen;
    
              sdi <= '0';
    
            when "101" =>
              dba_ps_sclk(0) <= sclk;
              dba_ps_sdo(0) <= sdo;
              dba_ps_sdi(0) <= DBA_CPLD_PS_SDI;
              dba_cpld_pscsb(0) <= sen;
              dba_lmk_csb(0) <= '1';
              dba_dac_csb(0) <= '1';
    
              dba_pl_sclk(0) <= '0';
              dba_pl_sdo(0) <= '0';
              dba_cpld_plcsb(0) <= '1';
              dba_rxlo_csb(0) <= '1'; 
              dba_txlo_csb(0) <= '1'; 
    
              sdi <= DBA_CPLD_PS_SDI;
                  
            when others =>
              dba_ps_sclk(0) <= '0';
              dba_ps_sdo(0) <= '0';
              dba_ps_sdi(0) <= '0';
              dba_cpld_pscsb(0) <= '1';
              dba_lmk_csb(0) <= '1';
              dba_dac_csb(0) <= '1';
    
              dba_pl_sclk(0) <= '0';
              dba_pl_sdo(0) <= '0';
              dba_cpld_plcsb(0) <= '1';
              dba_rxlo_csb(0) <= '1'; 
              dba_txlo_csb(0) <= '1';
    
              sdi <= '0';
              
          end case;
      else
    
         --Daughter Board B Signals
              dba_ps_sclk(0) <= '0';
              dba_ps_sdo(0) <= '0';
              dba_ps_sdi(0) <= '0';
              dba_cpld_pscsb(0) <= '1';
              dba_lmk_csb(0) <= '1';
              dba_dac_csb(0) <= '1';
    
              dba_pl_sclk(0) <= '0';
              dba_pl_sdo(0) <= '0';
              dba_pl_sdi(0) <= '0';
              dba_cpld_plcsb(0) <= '1';
              dba_rxlo_csb(0) <= '1'; 
              dba_txlo_csb(0) <= '1';
    
          case (caddr) is 
            -- 000: CPLD(PL), 001: LMK(PS), 010: DAC(PS/PL), 011: TXLO(PL), 100: RXLO(PL), 101:CPLD(PS)
            when "000" =>
              dbb_ps_sclk(0) <= '0';
              dbb_ps_sdo(0) <= '0';
              dbb_ps_sdi(0) <= '0';
              dbb_cpld_pscsb(0) <= '1';
              dbb_lmk_csb(0) <= '1';
              dbb_dac_csb(0) <= '1';
    
              dbb_pl_sclk(0) <= sclk;
              dbb_pl_sdo(0) <= sdo;
              dbb_pl_sdi(0) <= DBB_CPLD_PL_SDI;
              dbb_cpld_plcsb(0) <= sen;
              dbb_rxlo_csb(0) <= '1'; 
              dbb_txlo_csb(0) <= '1';
    
              sdi <= DBB_CPLD_PL_SDI;
    
            when "001" =>
              dbb_ps_sclk(0) <= sclk;
              dbb_ps_sdo(0) <= sdo;
              dbb_ps_sdi(0) <= DBB_CPLD_PS_SDI;
              dbb_cpld_pscsb(0) <= '1';
              dbb_lmk_csb(0) <= sen;
              dbb_dac_csb(0) <= '1';
    
              dbb_pl_sclk(0) <= '0';
              dbb_pl_sdo(0) <= '0';
              dbb_cpld_plcsb(0) <= '1';
              dbb_rxlo_csb(0) <= '1'; 
              dbb_txlo_csb(0) <= '1'; 
    
              sdi <= DBB_CPLD_PS_SDI;
        
            when "010" =>
              dbb_ps_sclk(0) <= sclk;
              dbb_ps_sdo(0) <= sdo;
              dbb_ps_sdi(0) <= DBB_CPLD_PS_SDI;
              dbb_cpld_pscsb(0) <= '1';
              dbb_lmk_csb(0) <= '1';
              dbb_dac_csb(0) <= sen;
    
              dbb_pl_sclk(0) <= '0';
              dbb_pl_sdo(0) <= '0';
              dbb_cpld_plcsb(0) <= '1';
              dbb_rxlo_csb(0) <= '1'; 
              dbb_txlo_csb(0) <= '1';
    
              sdi <= DBB_CPLD_PS_SDI;
        
            when "011" =>
              dbb_ps_sclk(0) <= '0';
              dbb_ps_sdo(0) <= '0';
              dbb_ps_sdi(0) <= '0';
              dbb_cpld_pscsb(0) <= '1';
              dbb_lmk_csb(0) <= '1';
              dbb_dac_csb(0) <= '1';
    
              dbb_pl_sclk(0) <= sclk;
              dbb_pl_sdo(0) <= sdo;
              dbb_cpld_plcsb(0) <= '1';
              dbb_rxlo_csb(0) <= sen; 
              dbb_txlo_csb(0) <= '1';
    
              sdi <= '0';
      
            when "100" =>
              dbb_ps_sclk(0) <= '0';
              dbb_ps_sdo(0) <= '0';
              dbb_ps_sdi(0) <= '0';
              dbb_cpld_pscsb(0) <= '1';
              dbb_lmk_csb(0) <= '1';
              dbb_dac_csb(0) <= '1';
    
              dbb_pl_sclk(0) <= sclk;
              dbb_pl_sdo(0) <= sdo;
              dbb_cpld_plcsb(0) <= '1';
              dbb_rxlo_csb(0) <= '1'; 
              dbb_txlo_csb(0) <= sen;
    
              sdi <= '0';
    
            when "101" =>
              dbb_ps_sclk(0) <= sclk;
              dbb_ps_sdo(0) <= sdo;
              dbb_ps_sdi(0) <= DBB_CPLD_PS_SDI;
              dbb_cpld_pscsb(0) <= sen;
              dbb_lmk_csb(0) <= '1';
              dbb_dac_csb(0) <= '1';
    
              dbb_pl_sclk(0) <= '0';
              dbb_pl_sdo(0) <= '0';
              dbb_cpld_plcsb(0) <= '1';
              dbb_rxlo_csb(0) <= '1'; 
              dbb_txlo_csb(0) <= '1'; 
    
              sdi <= DBB_CPLD_PS_SDI;
              
            when others =>
              dbb_ps_sclk(0) <= sclk;
              dbb_ps_sdo(0) <= sdo;
              dbb_ps_sdi(0) <= DBB_CPLD_PS_SDI;
              dbb_cpld_pscsb(0) <= '1';
              dbb_lmk_csb(0) <= '1';
              dbb_dac_csb(0) <= '1';
    
              dbb_pl_sclk(0) <= '0';
              dbb_pl_sdo(0) <= '0';
              dbb_cpld_plcsb(0) <= '1';
              dbb_rxlo_csb(0) <= '1'; 
              dbb_txlo_csb(0) <= '1';
    
              sdi <= '0';
          end case;
      end if;
    end if;
  end process;

  --Port Definitions
  DBA_CPLD_PS_SCLK <= dba_ps_sclk(0);
  DBA_CPLD_PS_SDO <= dba_ps_sdo(0);
  DBA_CPLD_PS_CSB <= dba_cpld_pscsb(0);
  DBA_LMK_PS_CSB <= dba_lmk_csb(0);
  DBA_DAC_PS_CSB <= dba_dac_csb(0);
  DBA_CPLD_PL_SCLK <= dba_pl_sclk(0);
  DBA_CPLD_PL_SDO <= dba_pl_sdo(0);
  DBA_CPLD_Pl_CSB <= dba_cpld_plcsb(0);
  DBA_RXLO_PL_CSB <= dba_rxlo_csb(0);
  DBA_TXLO_PL_CSB <= dba_txlo_csb(0);
  DBB_CPLD_PS_SCLK <= dbb_ps_sclk(0);
  DBB_CPLD_PS_SDO <= dbb_ps_sdo(0);
  DBB_CPLD_PS_CSB <= dbb_cpld_pscsb(0);
  DBB_LMK_PS_CSB <= dbb_lmk_csb(0);
  DBB_DAC_PS_CSB <= dbb_dac_csb(0);
  DBB_CPLD_PL_SCLK <= dbb_pl_sclk(0);
  DBB_CPLD_PL_SDO <= dbb_pl_sdo(0);
  DBB_CPLD_PL_CSB <= dbb_cpld_plcsb(0);
  DBB_RXLO_PL_CSB <= dbb_rxlo_csb(0);
  DBB_TXLO_PL_CSB <= dbb_txlo_csb(0);


  -- Vivadio ILA Intergation
  DEBUG: if its(VIVADO_ILA_p) generate  
  begin
    
    cpld_spi_ila: ila_0
      port map (
        clk => ctl_in.clk,
        probe0 => dba_ps_sclk,
        probe1 => dba_ps_sdo,
        probe2 => dba_ps_sdi,
        probe3 => dba_cpld_pscsb,
        probe4 => dba_lmk_csb,
        probe5 => dba_dac_csb,
        probe6 => dba_pl_sclk,
        probe7 => dba_pl_sdo,
        probe8 => dba_pl_sdi,
        probe9 => dba_cpld_plcsb,
        probe10 => dba_txlo_csb,
        probe11 => dba_rxlo_csb,
        probe12 => from_bool(CPLD_WENABLE),
        probe13 => from_bool(CPLD_RENABLE),
        probe14 => from_bool(done),
        probe15 => rdata );  
  
  end generate;
end rtl;
