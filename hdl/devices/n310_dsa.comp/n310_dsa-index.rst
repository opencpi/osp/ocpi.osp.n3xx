.. n310_dsa documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _n310_dsa:


N310 Digital Step Attenuators (``n310_dsa``)
============================================
GPIO interface for controlling the Digital Step Attenuators (DSA) within the RF Front-End (FE) sub-system.  This device worker supports both daughterboards.  Each daughterboard has 2 Tx DSA and 2 RX DSA.  

Design
------

Interface
---------
.. literalinclude:: ../specs/n310_dsa-spec.xml
   :language: xml

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Implementations
---------------
.. ocpi_documentation_implementations:: ../n310_dsa.hdl


Dependencies
------------

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``



Limitations
-----------
Limitations of ``n310_dsa`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
