.. n310_dsa HDL worker


.. _n310_dsa-HDL-worker:


``n310_dsa`` HDL Worker
=======================

Detail
------
HDL implementation of ``n310_dsa`` device worker in support of the ``n310`` platform. 

.. ocpi_documentation_worker::

Signal Ports
~~~~~~~~~~~~
.. literalinclude:: ./n310_dsa.xml
   :language: xml
   :lines: 5-82


.. Utilization
   -----------
   .. ocpi_documentation_utilization::




