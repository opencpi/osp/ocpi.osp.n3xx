-- THIS FILE WAS ORIGINALLY GENERATED ON Fri Jun 11 13:18:56 2021 EDT
-- BASED ON THE FILE: n310_dsa.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of worker is


	signal DB_Select_Stl_Vec : STD_LOGIC_VECTOR(0 downto 0);
	signal Chip_Select_Stl_Vec : STD_LOGIC_VECTOR(7 downto 0);
	signal Attenuation_Stl_Vec : STD_LOGIC_VECTOR(7 downto 0); 
	signal DBA_TX1_DSA0 : std_logic;
	signal DBA_TX1_DSA1 : std_logic;
	signal DBA_TX1_DSA2 : std_logic;
	signal DBA_TX1_DSA3 : std_logic;
	signal DBA_TX1_DSA4 : std_logic;
	signal DBA_TX1_DSA5 : std_logic;
	signal DBA_TX2_DSA0 : std_logic;
	signal DBA_TX2_DSA1 : std_logic;
	signal DBA_TX2_DSA2 : std_logic;
	signal DBA_TX2_DSA3 : std_logic;
	signal DBA_TX2_DSA4 : std_logic;
	signal DBA_TX2_DSA5 : std_logic;
	signal DBA_RX1_DSA0 : std_logic;
	signal DBA_RX1_DSA1 : std_logic;
	signal DBA_RX1_DSA2 : std_logic;
	signal DBA_RX1_DSA3 : std_logic;
	signal DBA_RX1_DSA4 : std_logic;
	signal DBA_RX1_DSA5 : std_logic;
	signal DBA_RX2_DSA0 : std_logic;
	signal DBA_RX2_DSA1 : std_logic;
	signal DBA_RX2_DSA2 : std_logic;
	signal DBA_RX2_DSA3 : std_logic;
	signal DBA_RX2_DSA4 : std_logic;
	signal DBA_RX2_DSA5 : std_logic;
	signal DBB_TX1_DSA0 : std_logic;
	signal DBB_TX1_DSA1 : std_logic;
	signal DBB_TX1_DSA2 : std_logic;
	signal DBB_TX1_DSA3 : std_logic;
	signal DBB_TX1_DSA4 : std_logic;
	signal DBB_TX1_DSA5 : std_logic;
	signal DBB_TX2_DSA0 : std_logic;
	signal DBB_TX2_DSA1 : std_logic;
	signal DBB_TX2_DSA2 : std_logic;
	signal DBB_TX2_DSA3 : std_logic;
	signal DBB_TX2_DSA4 : std_logic;
	signal DBB_TX2_DSA5 : std_logic;
	signal DBB_RX1_DSA0 : std_logic;
	signal DBB_RX1_DSA1 : std_logic;
	signal DBB_RX1_DSA2 : std_logic;
	signal DBB_RX1_DSA3 : std_logic;
	signal DBB_RX1_DSA4 : std_logic;
	signal DBB_RX1_DSA5 : std_logic;
	signal DBB_RX2_DSA0 : std_logic;
	signal DBB_RX2_DSA1 : std_logic;
	signal DBB_RX2_DSA2 : std_logic;
	signal DBB_RX2_DSA3 : std_logic;
	signal DBB_RX2_DSA4 : std_logic;
	signal DBB_RX2_DSA5 : std_logic;


  

begin

	--Assigning values in OCS properties to signals
	DB_Select_Stl_Vec <= from_bool(props_in.DB_Select); 
	Chip_Select_Stl_Vec <= from_uchar(props_in.Chip_Select); 
	Attenuation_Stl_Vec <= from_uchar(props_in.Attenuation); 
   

process(ctl_in.clk)


--variable needed insided process for concatenation
variable concat_signal : STD_LOGIC_VECTOR (2 downto 0):= (others=>'0');

begin

--concatenation of DB_Select with Chip_select 
concat_signal := DB_Select_Stl_Vec & Chip_Select_Stl_Vec(1 downto 0); 

   if rising_edge(ctl_in.clk) then
       if (ctl_in.reset = '1') then
           DBA_TX1_DSA0  <= '0';
           DBA_TX1_DSA1  <= '0';
           DBA_TX1_DSA2  <= '0';
           DBA_TX1_DSA3  <= '0';
           DBA_TX1_DSA4  <= '0';
           DBA_TX1_DSA5  <= '0';
           DBA_TX2_DSA0  <= '0';
           DBA_TX2_DSA1  <= '0';
           DBA_TX2_DSA2  <= '0';
           DBA_TX2_DSA3  <= '0';
           DBA_TX2_DSA4  <= '0';
           DBA_TX2_DSA5  <= '0';
           DBA_RX1_DSA0  <= '0';
           DBA_RX1_DSA1  <= '0';
           DBA_RX1_DSA2  <= '0';
           DBA_RX1_DSA3  <= '0';
           DBA_RX1_DSA4  <= '0';
           DBA_RX1_DSA5  <= '0';
           DBA_RX2_DSA0  <= '0';
           DBA_RX2_DSA1  <= '0';
           DBA_RX2_DSA2  <= '0';
           DBA_RX2_DSA3  <= '0';
           DBA_RX2_DSA4  <= '0';
           DBA_RX2_DSA5  <= '0';
           DBB_TX1_DSA0  <= '0';
           DBB_TX1_DSA1  <= '0';
           DBB_TX1_DSA2  <= '0';
           DBB_TX1_DSA3  <= '0';
           DBB_TX1_DSA4  <= '0';
           DBB_TX1_DSA5  <= '0';
           DBB_TX2_DSA0  <= '0';
           DBB_TX2_DSA1  <= '0';
           DBB_TX2_DSA2  <= '0';
           DBB_TX2_DSA3  <= '0';
           DBB_TX2_DSA4  <= '0';
           DBB_TX2_DSA5  <= '0'; 
           DBB_RX1_DSA0  <= '0';
           DBB_RX1_DSA1  <= '0';
           DBB_RX1_DSA2  <= '0';
           DBB_RX1_DSA3  <= '0';
           DBB_RX1_DSA4  <= '0';
           DBB_RX1_DSA5  <= '0';
           DBB_RX2_DSA0  <= '0';
           DBB_RX2_DSA1  <= '0';
           DBB_RX2_DSA2  <= '0';
           DBB_RX2_DSA3  <= '0';
           DBB_RX2_DSA4  <= '0';
           DBB_RX2_DSA5  <= '0';

    --Attenuation values being set for DB-A. Channels 1-4 
       elsif (props_in.Attenuation_written = btrue) then
           case concat_signal is
               when "000"=>  
                DBA_TX1_DSA0  <= Attenuation_Stl_Vec(0);
                DBA_TX1_DSA1  <= Attenuation_Stl_Vec(1);
                DBA_TX1_DSA2  <= Attenuation_Stl_Vec(2);
                DBA_TX1_DSA3  <= Attenuation_Stl_Vec(3);
                DBA_TX1_DSA4  <= Attenuation_Stl_Vec(4);
                DBA_TX1_DSA5  <= Attenuation_Stl_Vec(5);
   
            when "001"=>
                DBA_TX2_DSA0   <= Attenuation_Stl_Vec(0);
                DBA_TX2_DSA1   <= Attenuation_Stl_Vec(1);
                DBA_TX2_DSA2   <= Attenuation_Stl_Vec(2);
                DBA_TX2_DSA3   <= Attenuation_Stl_Vec(3);
                DBA_TX2_DSA4   <= Attenuation_Stl_Vec(4);
                DBA_TX2_DSA5   <= Attenuation_Stl_Vec(5);
                
            when "010"=>
                DBA_RX1_DSA0   <= Attenuation_Stl_Vec(0);
                DBA_RX1_DSA1   <= Attenuation_Stl_Vec(1);
                DBA_RX1_DSA2   <= Attenuation_Stl_Vec(2);
                DBA_RX1_DSA3   <= Attenuation_Stl_Vec(3);
                DBA_RX1_DSA4   <= Attenuation_Stl_Vec(4);
                DBA_RX1_DSA5   <= Attenuation_Stl_Vec(5);
   
            when "011"=>
                DBA_RX2_DSA0  <= Attenuation_Stl_Vec(0);
                DBA_RX2_DSA1  <= Attenuation_Stl_Vec(1);
                DBA_RX2_DSA2  <= Attenuation_Stl_Vec(2);
                DBA_RX2_DSA3  <= Attenuation_Stl_Vec(3);
                DBA_RX2_DSA4  <= Attenuation_Stl_Vec(4);
                DBA_RX2_DSA5  <= Attenuation_Stl_Vec(5);
       
            -- Attenuation values being set for daughterboard b  
           when "100"=>
               DBB_TX1_DSA0  <= Attenuation_Stl_Vec(0);
               DBB_TX1_DSA1  <= Attenuation_Stl_Vec(1);
               DBB_TX1_DSA2  <= Attenuation_Stl_Vec(2);
               DBB_TX1_DSA3  <= Attenuation_Stl_Vec(3);
               DBB_TX1_DSA4  <= Attenuation_Stl_Vec(4);
               DBB_TX1_DSA5  <= Attenuation_Stl_Vec(5);
       
           when "101" =>
               DBB_TX2_DSA0   <= Attenuation_Stl_Vec(0);
               DBB_TX2_DSA1   <= Attenuation_Stl_Vec(1);
               DBB_TX2_DSA2   <= Attenuation_Stl_Vec(2);
               DBB_TX2_DSA3   <= Attenuation_Stl_Vec(3);
               DBB_TX2_DSA4   <= Attenuation_Stl_Vec(4);
               DBB_TX2_DSA5   <= Attenuation_Stl_Vec(5); 
   
           when "110"=> 
               DBB_RX1_DSA0   <= Attenuation_Stl_Vec(0);
               DBB_RX1_DSA1   <= Attenuation_Stl_Vec(1);
               DBB_RX1_DSA2   <= Attenuation_Stl_Vec(2);
               DBB_RX1_DSA3   <= Attenuation_Stl_Vec(3);
               DBB_RX1_DSA4   <= Attenuation_Stl_Vec(4);
               DBB_RX1_DSA5   <= Attenuation_Stl_Vec(5);
       
           when others=>
               DBB_RX2_DSA0   <= Attenuation_Stl_Vec(0);
               DBB_RX2_DSA1   <= Attenuation_Stl_Vec(1);
               DBB_RX2_DSA2   <= Attenuation_Stl_Vec(2);
               DBB_RX2_DSA3   <= Attenuation_Stl_Vec(3);
               DBB_RX2_DSA4   <= Attenuation_Stl_Vec(4);
               DBB_RX2_DSA5   <= Attenuation_Stl_Vec(5);
            
           end case;   
       end if;
    end if; 
end process;

	DBA_TX1_DSA_0 <= DBA_TX1_DSA0;
	DBA_TX1_DSA_1 <= DBA_TX1_DSA1;
	DBA_TX1_DSA_2 <= DBA_TX1_DSA2;
	DBA_TX1_DSA_3 <= DBA_TX1_DSA3;
	DBA_TX1_DSA_4 <= DBA_TX1_DSA4;
	DBA_TX1_DSA_5 <= DBA_TX1_DSA5;
	DBA_TX2_DSA_0 <= DBA_TX2_DSA0;
	DBA_TX2_DSA_1 <= DBA_TX2_DSA1;
	DBA_TX2_DSA_2 <= DBA_TX2_DSA2;
	DBA_TX2_DSA_3 <= DBA_TX2_DSA3;
	DBA_TX2_DSA_4 <= DBA_TX2_DSA4;
	DBA_TX2_DSA_5 <= DBA_TX2_DSA5;
	DBA_RX1_DSA_0 <= DBA_RX1_DSA0;
	DBA_RX1_DSA_1 <= DBA_RX1_DSA1;
	DBA_RX1_DSA_2 <= DBA_RX1_DSA2;
	DBA_RX1_DSA_3 <= DBA_RX1_DSA3;
	DBA_RX1_DSA_4 <= DBA_RX1_DSA4;
	DBA_RX1_DSA_5 <= DBA_RX1_DSA5;
	DBA_RX2_DSA_0 <= DBA_RX2_DSA0;
	DBA_RX2_DSA_1 <= DBA_RX2_DSA1;
	DBA_RX2_DSA_2 <= DBA_RX2_DSA2;
	DBA_RX2_DSA_3 <= DBA_RX2_DSA3;
	DBA_RX2_DSA_4 <= DBA_RX2_DSA4;
	DBA_RX2_DSA_5 <= DBA_RX2_DSA5;
	DBB_TX1_DSA_0 <= DBB_TX1_DSA0;
	DBB_TX1_DSA_1 <= DBB_TX1_DSA1;
	DBB_TX1_DSA_2 <= DBB_TX1_DSA2;
	DBB_TX1_DSA_3 <= DBB_TX1_DSA3;
	DBB_TX1_DSA_4 <= DBB_TX1_DSA4;
	DBB_TX1_DSA_5 <= DBB_TX1_DSA5;
	DBB_TX2_DSA_0 <= DBB_TX2_DSA0;
	DBB_TX2_DSA_1 <= DBB_TX2_DSA1;
	DBB_TX2_DSA_2 <= DBB_TX2_DSA2;
	DBB_TX2_DSA_3 <= DBB_TX2_DSA3;
	DBB_TX2_DSA_4 <= DBB_TX2_DSA4;
	DBB_TX2_DSA_5 <= DBB_TX2_DSA5;
	DBB_RX1_DSA_0 <= DBB_RX1_DSA0;
	DBB_RX1_DSA_1 <= DBB_RX1_DSA1;
	DBB_RX1_DSA_2 <= DBB_RX1_DSA2;
	DBB_RX1_DSA_3 <= DBB_RX1_DSA3;
	DBB_RX1_DSA_4 <= DBB_RX1_DSA4;
	DBB_RX1_DSA_5 <= DBB_RX1_DSA5;
	DBB_RX2_DSA_0 <= DBB_RX2_DSA0;
	DBB_RX2_DSA_1 <= DBB_RX2_DSA1;
	DBB_RX2_DSA_2 <= DBB_RX2_DSA2;
	DBB_RX2_DSA_3 <= DBB_RX2_DSA3;
	DBB_RX2_DSA_4 <= DBB_RX2_DSA4;
	DBB_RX2_DSA_5 <= DBB_RX2_DSA5;

end rtl;
