.. n310_gpio documentation

.. Skeleton comment (to be deleted): Alternative names should be listed as
   keywords. If none are to be included delete the meta directive.

.. meta::
   :keywords: skeleton example


.. _n310_gpio:


N310 GPIO Interface (``n310_gpio``)
===================================
GPIO interface to the Master CPLD Controller for setting the ATR Registers.  The ATR Registers are used to enable or disable the AD9371 RF Channels and other platform configuration settings.  Individual control lines are implemented for the respective channels: ``CPLD-ATR-TX0``, ``CPLD-ATR-TX1``, ``CPLD-ATR-RX0``, and ``CPLD-ATR-RX1``.

Design
------

Interface
---------
.. literalinclude:: ../specs/n310_gpio-spec.xml
   :language: xml


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Implementations
---------------
.. ocpi_documentation_implementations:: ../n310_gpio.hdl



Dependencies
------------

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``



Limitations
-----------
Limitations of ``n310_gpio`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
