.. n310_gpio HDL worker


.. _n310_gpio-HDL-worker:


``n310_gpio`` HDL Worker
========================
    

Detail
------
HDL implementation of ``n310_gpio`` device worker in support of the ``n310`` platform.  Individual control lines are implemented for the respective channels: ``CPLD-ATR-TX0``, ``CPLD-ATR-TX1``, ``CPLD-ATR-RX0``, and ``CPLD-ATR-RX1``.  Additional, the AD9371 TX channels are conditioned by the status of the ``data_sink_qdac_ad9371_sub`` device sub-worker, while the AD9371 RX channels are conditioned by the status of the ``data_src_qadc_ad9371_sub device`` sub-worker.

.. ocpi_documentation_worker::

  tx_ctl: Control interface between the ``n310_gpio`` and ``data_sink_qdac_ad9371_sub`` device workers.
  rx_ctl: Control interface between the ``n310_gpio`` and ``data_src_qadc_ad9371_sub`` device workers.


Signal Ports
~~~~~~~~~~~~
.. literalinclude:: ./n310_gpio.xml
   :language: xml
   :lines: 2-5


.. Utilization
   -----------
   .. ocpi_documentation_utilization::



