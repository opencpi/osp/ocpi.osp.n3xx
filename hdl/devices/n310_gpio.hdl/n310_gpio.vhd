-- THIS FILE WAS ORIGINALLY GENERATED ON Thu Jun 17 16:59:00 2021 EDT
-- BASED ON THE FILE: n310_gpio.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions

architecture rtl of worker is

signal DB_Select_Stl_Vec : STD_LOGIC_VECTOR(0 downto 0);
signal Chip_Select_Stl_Vec : STD_LOGIC_VECTOR(7 downto 0);


begin

--Assigning values in OCS properties to signals
DB_Select_Stl_Vec <= from_bool(props_in.DB_Select); 
Chip_Select_Stl_Vec <= from_uchar(props_in.Chip_Select); 
   

process(ctl_in.clk)
begin

    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        ATR_TX0  <= '0';
        ATR_TX1  <= '0';
        ATR_RX0  <= '0';
        ATR_RX1  <= '0';
        tx_ctl_out.qdac0 <= '0';
        tx_ctl_out.qdac1 <= '0';
        rx_ctl_out.qadc0 <= '0';
        rx_ctl_out.qadc1 <= '0';

     --values being set for DB-A. 
      elsif (props_in.Set_written = btrue) then
          case (Chip_Select_Stl_Vec) is
            when "00" =>
              ATR_TX0  <= props_in.Set;
              tx_ctl_out.qdac0 <= props_in.Set;
            when "01" =>
              ATR_TX1  <= props_in.Set;
              tx_ctl_out.qdac1 <= props_in.Set;
            when "10" =>
              ATR_RX0  <= props_in.Set;
              rx_ctl_out.qadc0 <= props_in.Set;
            when "11" =>
              ATR_RX1  <= props_in.Set;
              rx_ctl_out.qadc1 <= props_in.Set;
            when others =>
              NULL; 
          end case;
      end if;
    end if;
end process;

end rtl;


