.. n310_qadc_csts documentation

.. _n310_qadc_csts:


N310 qADC Class Worker (``n310_qadc_csts``)
===========================================
OpenCPI qDAC Class Worker for the ``n310`` platform.

Design
------

Interface
---------
.. literalinclude:: ../specs/n310_qadc_csts-spec.xml
   :language: xml
   :lines: 1-40,45


Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Ports
~~~~~
.. ocpi_documentation_ports::

   out: Output Data Port
   
Implementations
---------------
.. ocpi_documentation_implementations:: ../n310_qadc_csts.hdl


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ``ocpi.platforms.adc_csts`` primitive
 * ``ocpi.core.util``, ``ocpi.core.bsv``, ``ocpi.core.timed_sample_prot`` primitives

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``n310_qadc_csts`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
