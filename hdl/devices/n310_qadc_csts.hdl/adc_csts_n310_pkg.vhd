library ieee; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library timed_sample_prot, adc_csts;
library adc_csts;


package adc_csts_n310 is

--constant DATA_BIT_WIDTH : positive := 12;
constant DATA_BIT_WIDTH : integer := to_integer(work.n310_qadc_csts_constants.ADC_WIDTH_BITS);
constant SAMP_COUNT_BIT_WIDTH : positive := 32;
constant DROPPED_SAMPS_BIT_WIDTH : positive := 32;

type data_complex_t is record
  real      : std_logic_vector(DATA_BIT_WIDTH-1 downto 0);
  imaginary : std_logic_vector(DATA_BIT_WIDTH-1 downto 0);
end record data_complex_t;


component samp_drop_detector is
  port(
    -- CTRL
    clk       : in  std_logic;
    rst       : in  std_logic;
    status    : out adc_csts.adc_csts.samp_drop_detector_status_t;
    -- INPUT
    idata     : in  adc_csts_n310.data_complex_t;
    ivld      : in  std_logic;
    -- OUTPUT
    odata     : out adc_csts_n310.data_complex_t;
    osamp_drop: out std_logic;
    ovld      : out std_logic;
    ordy      : in  std_logic);
end component;

component data_widener is
  generic(
    BITS_PACKED_INTO_MSBS : boolean := true);
  port(
    -- CTRL
    clk        : in  std_logic;
    rst        : in  std_logic;
    -- INPUT
    idata      : in  adc_csts_n310.data_complex_t;
    isamp_drop : in  std_logic;
    ivld       : in  std_logic;
    irdy       : out std_logic;
    -- OUTPUT
    oprotocol  : out timed_sample_prot.complex_short_timed_sample.protocol_t;
    ordy       : in  std_logic);
end component;

component ila_0 is
  port (
    clk      : in  std_logic;
    probe0   : in  std_logic_vector(15 downto 0);
    probe1   : in  std_logic_vector(15 downto 0);
    probe2   : in  std_logic_vector(15 downto 0);
    probe3   : in  std_logic_vector(15 downto 0);
    probe4   : in  std_logic_vector(31 downto 0);
    probe5   : in  std_logic_vector(0 downto 0);
    probe6   : in  std_logic_vector(0 downto 0);
    probe7   : in  std_logic_vector(0 downto 0);
    probe8   : in  std_logic_vector(0 downto 0);
    probe9   : in  std_logic_vector(0 downto 0) );
end component;

end package adc_csts_n310;
