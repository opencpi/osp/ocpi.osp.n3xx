.. n310_qadc_csts HDL worker


.. _n310_qadc_csts-HDL-worker:


``n310_qadc_csts`` HDL Worker
=============================

Detail
------
HDL implementation of ``n310_qadc_csts`` class worker for the ``n310`` platform.  This worker is equivalent to the OpenCPI ``data_src_csts.hdl`` class worker within ``ocpi.platform``.  It has been modified to support scalable data widths up to 16 bits.  

.. ocpi_documentation_worker::

   VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)
   OUT_PORT_DATA_WIDTH: Build-time parameter for input port data width
   send_eof: ``true`` = Send an eof after num_samples_before_eof samples have been sent
   num_samples_before_eof: Number of samples to issue an eof after num_samples_before_eof samples have been sent

   out: Output Data Port
   dev: Devsignal Interface from ``data_src_qadc_ad9371_sub`` subdevice worker 

   ocpi_buffer_size_out: OpenCPI Framework Property
   ocpi_blocked_out: OpenCPI Framework Property
   ocpi_max_latency_out: OpenCPI Framework Property
   ocpi_latency_out: OpenCPI Framework Property
   ocpi_messages_out: OpenCPI Framework Property
   ocpi_bytes_out: OpenCPI Framework Property


.. Utilization
   -----------
   .. ocpi_documentation_utilization::
