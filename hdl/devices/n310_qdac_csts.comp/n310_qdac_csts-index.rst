.. n310_qdac_csts documentation

.. _n310_qdac_csts:


N310 qDAC Class Worker (``n310_qdac_csts``)
===========================================
OpenCPI qDAC Class Worker for the ``n310`` platform.

Design
------

Interface
---------
.. literalinclude:: ../specs/n310_qdac_csts-spec.xml
   :language: xml
   :lines: 1-28,36

.. Opcode handling
   ~~~~~~~~~~~~~~~
   Skeleton outline: Description of how the non-stream opcodes are handled.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::


Ports
~~~~~
.. ocpi_documentation_ports::

   in: Input Data Port
   on_off: Tx Event Port


Implementations
---------------
.. ocpi_documentation_implementations:: ../n310_qdac_csts.hdl



Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ``ocpi.platforms.dac_csts`` primitive
 * ``ocpi.core.timed_sample_prot`` primitive

There is also a dependency on:

 * ``ieee.std_logic_1164``
 * ``ieee.numeric_std``



Limitations
-----------
Limitations of ``n310_qdac_csts`` are:

 * NONE

.. Testing
   -------
   .. ocpi_documentation_test_platforms::

   .. ocpi_documentation_test_result_summary::
