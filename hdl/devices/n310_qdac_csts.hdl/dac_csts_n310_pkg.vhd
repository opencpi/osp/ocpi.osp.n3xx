library ieee; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library dac_csts;
library timed_sample_prot; use timed_sample_prot.complex_short_timed_sample.all;
library work;

package dac_csts_n310 is

-- represents the width of each of I and Q
--constant DATA_BIT_WIDTH : positive := 16;
constant DATA_BIT_WIDTH : integer := to_integer(work.n310_qdac_csts_constants.DAC_WIDTH_BITS);
constant SAMP_COUNT_BIT_WIDTH : positive := 32;
constant NUM_UNDERRUNS_BIT_WIDTH : positive := 32;

type data_complex_t is record
  real      : std_logic_vector(DATA_BIT_WIDTH-1 downto 0);
  imaginary : std_logic_vector(DATA_BIT_WIDTH-1 downto 0);
end record data_complex_t;


component data_narrower is
  generic(
    BITS_PACKED_INTO_LSBS : boolean := false);
  port(
    -- INPUT
    clk           : in  std_logic;
    rst           : in  std_logic;
    iprotocol     : in  timed_sample_prot.complex_short_timed_sample.protocol_t;
    imetadata     : in  dac_csts.dac_csts.metadata_t;
    imetadata_vld : in  std_logic;
    irdy          : out std_logic;
    -- OUTPUT
    odata         : out dac_csts_n310.data_complex_t;
    odata_vld     : out std_logic;
    ometadata     : out dac_csts.dac_csts.metadata_t;
    ometadata_vld : out std_logic;
    ordy          : in  std_logic);
end component;

component ila_0 is
  port (
    clk      : in  std_logic;
    probe0   : in  std_logic_vector(31 downto 0);
    probe1   : in  std_logic_vector(0 downto 0);
    probe2   : in  std_logic_vector(0 downto 0);
    probe3   : in  std_logic_vector(0 downto 0);
    probe4   : in  std_logic_vector(0 downto 0);
    probe5   : in  std_logic_vector(0 downto 0);
    probe6   : in  std_logic_vector(15 downto 0);
    probe7   : in  std_logic_vector(15 downto 0);
    probe8   : in  std_logic_vector(0 downto 0) );
end component;


end package dac_csts_n310;
