.. n310_qdac_csts HDL worker


.. _n310_qdac_csts-HDL-worker:


``n310_qdac_csts`` HDL Worker
=============================

Detail
------
HDL implementation of ``n310_qdac_csts`` class worker for the ``n310`` platform.  This worker is equivalent to the OpenCPI ``data_sink_csts.hdl`` class worker within ``ocpi.platform``.  It has been modified to support scalable data widths up to 16 bits.  

.. ocpi_documentation_worker::

   VIVADO_ILA_p: Build-time parameter for Xilinx Integrated Logic Analyzer (ILA)
   IN_PORT_DATA_WIDTH: Build-time parameter for input port data width 
   unused_opcode_detected_sticky: Unused OpCode Sticky Flag
   clr_unused_opcode_detected_sticky: Clear Unused Opcode Sticky Flag
   dac_clk_freq_hz: DAC Clock Frequency(`Simulaton Only`)
   status: Status Register (`Debugging Only`)
   ctl_count: Control Plane Counter (`Debugging Only`)
   dac_count: DAC Plane Counter (`Debugging Only`)

   in: Input Data Port
   on_off: Tx Event Port

   dev: Devsignal Interface to ``data_sink_qdac_ad9371_sub`` subdevice worker 

   ocpi_buffer_size_on_off: OpenCPI Framework Property
   ocpi_blocked_on_off: OpenCPI Framework Property
   ocpi_max_latency_on_off: OpenCPI Framework Property
   ocpi_latency_on_off: OpenCPI Framework Property
   ocpi_messages_on_off: OpenCPI Framework Property
   ocpi_bytes_on_off: OpenCPI Framework Property


.. Utilization
   -----------
   .. ocpi_documentation_utilization::
