.. n310 Platform worker

.. _n310-platform-worker:


``n310`` Platform Worker
========================
The Ettus Research N310 USRP platform is a high-performance Software-Defined Radio (SDR) platform for wireless applications.  The architecture consists of a Xilinx Zynq-7100 System-on-Chip (SoC) motherboard with dual AD9371 transceiver daughterboards to provide multiple-channel deployment.   The Processing System (PS) is a dual-core ARM Cortex-A9 800-MHZ CPU running OpenEmbedded Linux, and the Programmable Logic (PL) is a Kintex Series-7 FPGA.  
 
 * Max Channels: 4 Tx, 4 Rx Channels
 * Frequency Range: 10 MHz to 6 GHz
 * Channel Bandwidth: up to 70 MHz @ 10 MHz steps (100 MHz - future release)
 * Gain Control: up to 30 dB @ 0.5 dB steps
 * OpenCPI Control Plane Clock: 100 MHz (166.67 MHz - future release)
 * Master Clock Rates (MCR): 90 and 96 MSps (122.88, 125, and 153.6 MSps - future release)
 * Operational Modes: Embedded (Networked - future release)

.. _n310_osp_architecture:

.. figure:: ./figures/N310_OSP_Architecture.svg
   :alt: Single Daugtherboard Block Diagram
   :align: center
   :height: 1200px
   :width: 1200px

   Single Daugtherboard Architecture Diagram

.. _n310_osp_full_architecture:

.. figure:: ./figures/N310_OSP_Architecture-FULL.svg
   :alt: Single Daugtherboard Block Diagram
   :align: center
   :height: 2000px
   :width: 1200px

   Dual Daugtherboard Architecture Diagram

Control Plane Device Workers
-----------------------------
 * :doc:`drc_n310 <../../devices/drc_n310.comp/drc_n310-index>`: OpenCPI Digital Radio Controller (DRC)
 * :doc:`n310_cpld_spi <../../devices/n310_cpld_spi.comp/n310_cpld_spi-index>`: SPI interface to the Master CPLD Controller
 * :doc:`n310_gpio <../../devices/n310_gpio.comp/n310_gpio-index>`: Control interface for the Master CPLD Controller 
 * :doc:`n310_dsa <../../devices/n310_dsa.comp/n310_dsa-index>`: Control interface for the N310 Digital Step Attenuators (DSA)
 * :doc:`ad9371_spi <../../devices/ad9371_spi.comp/ad9371_spi-index>`: SPI interface Analog Devices (ADI) AD9371 Transceiver
 * :doc:`ad9371_gpio <../../devices/ad9371_gpio.comp/ad9371_gpio-index>`: GPIO debugging interface for the Analog Devices (ADI) AD9371 Transceiver
 * :doc:`jesd_raw2axi_interface <../../devices/jesd_raw2axi_interface.comp/jesd_raw2axi_interface-index>`: OpenCPI RAW properties to AXI registers for the Analog Devices (ADI) JESD204B Framework cores

Data Plane Device Workers
--------------------------
  * :doc:`adrv9371_jesd_2tx_2rx <../../devices/adrv9371_jesd_2tx_2rx.comp/adrv9371_jesd_2tx_2rx-index>`: JESD204B Interface
  * :doc:`data_sink_qdac_ad9371_sub <../../devices/data_sink_qdac_ad9371_sub.comp/data_sink_qdac_ad9371_sub-index>`: JESD204B qDAC Subdevice Worker 
  * :doc:`data_src_qadc_ad9371_sub <../../devices/data_src_qadc_ad9371_sub.comp/data_src_qadc_ad9371_sub-index>`: JESD204B qADC Subdevice Worker 
  * :doc:`n310_qdac_csts <../../devices/n310_qdac_csts.comp/n310_qdac_csts-index>`: N310 qDAC Class Worker 
  * :doc:`n310_qadc_csts <../../devices/n310_qadc_csts.comp/n310_qadc_csts-index>`: N310 qADC Class Worker 
  * :doc:`cic_dec_csts <../../devices/cic_dec_csts.comp/cic_dec_csts-index>`: Cascaded Integrator-Comb (CIC) Filter Worker - Decimation 
  * :doc:`cic_int_csts <../../devices/cic_int_csts.comp/cic_int_csts-index>`: Cascaded Integrator-Comb (CIC) Filter Worker - Interpolation 

Detail
------

.. ocpi_documentation_worker::

   sdp_channels: Number of OpenCPI Scalable Data Plane (SDP) Interfaces connected to the Xilinx Zynq High-Performance AXI Ports.
   sdpDropCount: Number of SDP Packet Dropped.
   useFCLK: Build-time parameter to select the Xilinx Zynq FCLK (0-3) to used for OpenCPI Control Plane Clock.


.. Utilization
   -----------
   .. ocpi_documentation_utilization::