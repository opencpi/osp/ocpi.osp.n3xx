-- THIS FILE WAS ORIGINALLY GENERATED ON Wed Jun  2 10:35:59 2021 PDT
-- BASED ON THE FILE: n310.xml
library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; 
library UNISIM; use UNISIM.vcomponents.all;
library work; use work.zynq_ps_n310_pkg.all;


architecture Structural of n310_worker is

  constant whichGP        : natural := 0;
  constant whichFCLK      : natural := to_integer(unsigned(useFCLK));
  constant sdpWidth       : natural := to_integer(unsigned(sdp_width));

  signal ps_m_axi_gp_in   : axi.zynq_7000_m_gp.axi_s2m_array_t(0 to C_M_AXI_GP_COUNT-1); 
  signal ps_m_axi_gp_out  : axi.zynq_7000_m_gp.axi_m2s_array_t(0 to C_M_AXI_GP_COUNT-1);
  signal ps_m_axi_ap_in   : axilite_ap_s2m_array_t(0 to C_M_AXI_AP_COUNT-1);
  signal ps_m_axi_ap_out  : axilite_ap_m2s_array_t(0 to C_M_AXI_AP_COUNT-1); 
  signal ps_s_axi_hp_in   : axi.zynq_7000_s_hp.axi_m2s_array_t(0 to C_S_AXI_HP_COUNT-1); 
  signal ps_s_axi_hp_out  : axi.zynq_7000_s_hp.axi_s2m_array_t(0 to C_S_AXI_HP_COUNT-1);
  signal ps_mio           : std_logic_vector (53 downto 0);
  signal ps_gpio_in       : gpioIn_t;
  signal ps_gpio_out      : gpioOut_t;
  signal fclk             : std_logic_vector(3 downto 0);
  signal fclkreset_n      : std_logic_vector(3 downto 0);

  type sdp_data_t is array(0 to C_S_AXI_HP_COUNT) of dword_array_t(0 to sdpWidth-1);
  signal sdp_in_data_1d   : sdp_data_t;
  signal sdp_out_data_1d  : sdp_data_t;
  signal sdp_in           : sdp.sdp.s2m_array_t(0 to C_S_AXI_HP_COUNT-1);
  signal sdp_in_data      : sdp.sdp.data_array_t(0 to C_S_AXI_HP_COUNT-1, 0 to sdpWidth-1);
  signal sdp_out          : sdp.sdp.m2s_array_t(0 to C_S_AXI_HP_COUNT-1);
  signal sdp_out_data     : sdp.sdp.data_array_t(0 to C_S_AXI_HP_COUNT-1, 0 to sdpWidth-1);
  signal axi_error        : bool_array_t(0 to C_S_AXI_HP_COUNT-1);
  signal dbg_state        : ulonglong_array_t(0 to C_S_AXI_HP_COUNT-1);
  signal dbg_state1       : ulonglong_array_t(0 to C_S_AXI_HP_COUNT-1);
  signal dbg_state2       : ulonglong_array_t(0 to C_S_AXI_HP_COUNT-1);

  signal clk              : std_logic;
  signal rst              : std_logic;

  signal fpga_pudc_b_buf  : std_logic;
  attribute DONT_TOUCH    : string;
  attribute DONT_TOUCH of fpga_pudc_b_buf : signal is "true";


begin

-- Metadata Interface 
  metadata_out.clk     <= clk;
  metadata_out.romAddr <= props_in.romAddr;
  metadata_out.romEn   <= props_in.romData_read;


-- Timekeeping Interface
  timebase_out.clk      <= clk;
  timebase_out.PPS      <= '0';
  timebase_out.usingPPS <= '0'; 


-- Global clock buffer for Control Plane
  clkbuf : BUFG   port map( I => fclk(whichFCLK),
                            O => clk);


-- The FCLKRESET signals from the PS are documented as asynchronous with the
-- associated FCLK for whatever reason.  Here we make a synchronized reset from it.
sr : cdc.cdc.reset
  generic map(SRC_RST_VALUE => '0',
  RST_DELAY => 17)
  port map   (src_rst   => fclkreset_n(whichFCLK),
              dst_clk   => fclk(whichFCLK),
              dst_rst   => rst,
              dst_rst_n => open );


-- Control Plane Interface
cp : axi.zynq_7000_m_gp.axi2cp_zynq_7000_m_gp
  port map(
      clk     => clk,
      reset   => rst,
      axi_in  => ps_m_axi_gp_out(whichGP),
      axi_out => ps_m_axi_gp_in(whichGP),
      cp_in   => cp_in,
      cp_out  => cp_out );


-- Data Plane Interface
sdp_in <= sdp.sdp.s2m_array_t(zynq_in);
zynq_out <= zynq_out_array_t(sdp_out);

-- convert between 2d array and array of arrays (VHDL does not allow 1d slices of 2d)
sd0 : for i in 0 to C_S_AXI_HP_COUNT-1 generate
  sd1: for j in 0 to sdpWidth-1 generate
        sdp_in_data(i,j) <= zynq_in_data(i)(j);
        zynq_out_data(i)(j) <= sdp_out_data(i,j);
    end generate;
  end generate;

-- convert 2d data paths into 1d (VHDL does not allow 1d slices of 2d)
sd2 : for i in 0 to C_S_AXI_HP_COUNT-1 generate
  sd3: for j in 0 to sdpWidth-1 generate
          sdp_in_data_1d(i)(j) <= sdp_in_data(i, j);
          sdp_out_data(i, j) <= sdp_out_data_1d(i)(j);
  end generate;
end generate;

g0 : for i in 0 to C_S_AXI_HP_COUNT-1 generate
  dp : axi.zynq_7000_s_hp.sdp2axi_zynq_7000_s_hp
    generic map(ocpi_debug => true,
                sdp_width  => sdpWidth)
    port map(   clk          => clk,
                reset        => rst,
                sdp_in       => sdp_in(i),
                sdp_in_data  => sdp_in_data_1d(i),
                sdp_out      => sdp_out(i),
                sdp_out_data => sdp_out_data_1d(i),
                axi_in       => ps_s_axi_hp_out(i),
                axi_out      => ps_s_axi_hp_in(i),
                axi_error    => axi_error(i),
                dbg_state    => dbg_state(i),
                dbg_state1   => dbg_state1(i),
                dbg_state2   => dbg_state2(i));
end generate;


-- Zynq PS Instantiation
ps : zynq_ps_n310
  port map(
    ps_in.debug         => (31 => ocpi.util.slvn(whichGP,1)(0), others => '0'),
    ps_out.FCLK         => fclk,
    ps_out.FCLKRESET_N  => fclkreset_n,    
    m_axi_gp_in         => ps_m_axi_gp_in,
    m_axi_gp_out        => ps_m_axi_gp_out,
    m_axi_ap_in         => ps_m_axi_ap_in,
    m_axi_ap_out        => ps_m_axi_ap_out,
    s_axi_hp_in         => ps_s_axi_hp_in,
    s_axi_hp_out        => ps_s_axi_hp_out,
    mio                 => ps_mio,
    gpio_in             => ps_gpio_in,
    gpio_out            => ps_gpio_out );


  ----------------------------------------------------------------------------
  --
  -- PUDC Workaround
  --
  ----------------------------------------------------------------------------
  -- This is a workaround for a silicon bug in Series 7 FPGA where a
  -- race condition with the reading of PUDC during the erase of the FPGA
  -- image cause glitches on output IO pins.
  --
  -- Workaround:
  -- - Define the PUDC pin in the XDC file with a pullup.
  -- - Implements an IBUF on the PUDC input and make sure that it does
  --   not get optimized out.
  pudc_ibuf_i: IBUF
      port map (
        I => fpga_pudc_b,
        O => fpga_pudc_b_buf );


  -- Platform Properties
  props_out.UUID <= metadata_in.UUID;
  props_out.oldtime <= to_ulonglong(std_logic_vector'(x"0000000000000000"));
  props_out.romData <= metadata_in.romData;
  props_out.nSwitches <= to_ulong(0);
  props_out.nLEDs <= to_ulong(0);
  props_out.memories_length <= to_ulong(0);
  props_out.memories <= (to_ulong(0),to_ulong(0),to_ulong(0),to_ulong(0));
  props_out.dna <= to_ulonglong(std_logic_vector'(x"0000000000000000"));
  props_out.switches <= to_ulong(0);
  props_out.slotCardIsPresent <= (bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse,bfalse);
  props_out.sdpDropCount    <= zynq_in(0).dropCount;

end Structural;
