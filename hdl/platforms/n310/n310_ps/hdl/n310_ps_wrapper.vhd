--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Tue Sep 27 10:36:05 2022
--Host        : localhost.localdomain running 64-bit CentOS Linux release 7.9.2009 (Core)
--Command     : generate_target n310_ps_wrapper.bd
--Design      : n310_ps_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity n310_ps_wrapper is
  port (
    DDR_VRN : inout STD_LOGIC;
    DDR_VRP : inout STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FCLK_CLK0 : out STD_LOGIC;
    FCLK_CLK1 : out STD_LOGIC;
    FCLK_CLK2 : out STD_LOGIC;
    FCLK_CLK3 : out STD_LOGIC;
    FCLK_RESET0_N : out STD_LOGIC;
    FCLK_RESET1_N : out STD_LOGIC;
    FCLK_RESET2_N : out STD_LOGIC;
    FCLK_RESET3_N : out STD_LOGIC;
    GPIO_I : in STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_O : out STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_T : out STD_LOGIC_VECTOR ( 63 downto 0 );
    MIO : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    M_AXI_GP0_ACLK : in STD_LOGIC;
    M_AXI_GP0_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arready : in STD_LOGIC;
    M_AXI_GP0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_arvalid : out STD_LOGIC;
    M_AXI_GP0_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awready : in STD_LOGIC;
    M_AXI_GP0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_awvalid : out STD_LOGIC;
    M_AXI_GP0_bid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_bready : out STD_LOGIC;
    M_AXI_GP0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_bvalid : in STD_LOGIC;
    M_AXI_GP0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_rid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_rlast : in STD_LOGIC;
    M_AXI_GP0_rready : out STD_LOGIC;
    M_AXI_GP0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_rvalid : in STD_LOGIC;
    M_AXI_GP0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_wid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_wlast : out STD_LOGIC;
    M_AXI_GP0_wready : in STD_LOGIC;
    M_AXI_GP0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_wvalid : out STD_LOGIC;
    PS_CLK : inout STD_LOGIC;
    PS_PORB : inout STD_LOGIC;
    PS_SRSTB : inout STD_LOGIC;
    S_AXI_HP0_ACLK : in STD_LOGIC;
    S_AXI_HP0_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arready : out STD_LOGIC;
    S_AXI_HP0_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_arvalid : in STD_LOGIC;
    S_AXI_HP0_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awready : out STD_LOGIC;
    S_AXI_HP0_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_awvalid : in STD_LOGIC;
    S_AXI_HP0_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_bready : in STD_LOGIC;
    S_AXI_HP0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_bvalid : out STD_LOGIC;
    S_AXI_HP0_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_rlast : out STD_LOGIC;
    S_AXI_HP0_rready : in STD_LOGIC;
    S_AXI_HP0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_rvalid : out STD_LOGIC;
    S_AXI_HP0_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_wlast : in STD_LOGIC;
    S_AXI_HP0_wready : out STD_LOGIC;
    S_AXI_HP0_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP0_wvalid : in STD_LOGIC;
    S_AXI_HP1_ACLK : in STD_LOGIC;
    S_AXI_HP1_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arready : out STD_LOGIC;
    S_AXI_HP1_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_arvalid : in STD_LOGIC;
    S_AXI_HP1_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awready : out STD_LOGIC;
    S_AXI_HP1_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_awvalid : in STD_LOGIC;
    S_AXI_HP1_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_bready : in STD_LOGIC;
    S_AXI_HP1_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_bvalid : out STD_LOGIC;
    S_AXI_HP1_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_rlast : out STD_LOGIC;
    S_AXI_HP1_rready : in STD_LOGIC;
    S_AXI_HP1_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_rvalid : out STD_LOGIC;
    S_AXI_HP1_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_wlast : in STD_LOGIC;
    S_AXI_HP1_wready : out STD_LOGIC;
    S_AXI_HP1_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP1_wvalid : in STD_LOGIC;
    S_AXI_HP2_ACLK : in STD_LOGIC;
    S_AXI_HP2_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arready : out STD_LOGIC;
    S_AXI_HP2_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_arvalid : in STD_LOGIC;
    S_AXI_HP2_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awready : out STD_LOGIC;
    S_AXI_HP2_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_awvalid : in STD_LOGIC;
    S_AXI_HP2_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_bready : in STD_LOGIC;
    S_AXI_HP2_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_bvalid : out STD_LOGIC;
    S_AXI_HP2_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_rlast : out STD_LOGIC;
    S_AXI_HP2_rready : in STD_LOGIC;
    S_AXI_HP2_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_rvalid : out STD_LOGIC;
    S_AXI_HP2_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_wlast : in STD_LOGIC;
    S_AXI_HP2_wready : out STD_LOGIC;
    S_AXI_HP2_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP2_wvalid : in STD_LOGIC;
    S_AXI_HP3_ACLK : in STD_LOGIC;
    S_AXI_HP3_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arready : out STD_LOGIC;
    S_AXI_HP3_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_arvalid : in STD_LOGIC;
    S_AXI_HP3_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awready : out STD_LOGIC;
    S_AXI_HP3_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_awvalid : in STD_LOGIC;
    S_AXI_HP3_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_bready : in STD_LOGIC;
    S_AXI_HP3_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_bvalid : out STD_LOGIC;
    S_AXI_HP3_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_rlast : out STD_LOGIC;
    S_AXI_HP3_rready : in STD_LOGIC;
    S_AXI_HP3_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_rvalid : out STD_LOGIC;
    S_AXI_HP3_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_wlast : in STD_LOGIC;
    S_AXI_HP3_wready : out STD_LOGIC;
    S_AXI_HP3_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP3_wvalid : in STD_LOGIC;
    USBIND_port_indctl : out STD_LOGIC_VECTOR ( 1 downto 0 );
    USBIND_vbus_pwrfault : in STD_LOGIC;
    USBIND_vbus_pwrselect : out STD_LOGIC
  );
end n310_ps_wrapper;

architecture STRUCTURE of n310_ps_wrapper is
  component n310_ps is
  port (
    FCLK_CLK0 : out STD_LOGIC;
    FCLK_CLK1 : out STD_LOGIC;
    FCLK_CLK2 : out STD_LOGIC;
    FCLK_CLK3 : out STD_LOGIC;
    FCLK_RESET0_N : out STD_LOGIC;
    FCLK_RESET1_N : out STD_LOGIC;
    FCLK_RESET2_N : out STD_LOGIC;
    FCLK_RESET3_N : out STD_LOGIC;
    S_AXI_HP0_ACLK : in STD_LOGIC;
    S_AXI_HP1_ACLK : in STD_LOGIC;
    S_AXI_HP2_ACLK : in STD_LOGIC;
    S_AXI_HP3_ACLK : in STD_LOGIC;
    GPIO_I : in STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_O : out STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_T : out STD_LOGIC_VECTOR ( 63 downto 0 );
    MIO : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    DDR_VRN : inout STD_LOGIC;
    DDR_VRP : inout STD_LOGIC;
    PS_SRSTB : inout STD_LOGIC;
    PS_CLK : inout STD_LOGIC;
    PS_PORB : inout STD_LOGIC;
    M_AXI_GP0_arvalid : out STD_LOGIC;
    M_AXI_GP0_awvalid : out STD_LOGIC;
    M_AXI_GP0_bready : out STD_LOGIC;
    M_AXI_GP0_rready : out STD_LOGIC;
    M_AXI_GP0_wlast : out STD_LOGIC;
    M_AXI_GP0_wvalid : out STD_LOGIC;
    M_AXI_GP0_arid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_awid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_wid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_arlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_awlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arready : in STD_LOGIC;
    M_AXI_GP0_awready : in STD_LOGIC;
    M_AXI_GP0_bvalid : in STD_LOGIC;
    M_AXI_GP0_rlast : in STD_LOGIC;
    M_AXI_GP0_rvalid : in STD_LOGIC;
    M_AXI_GP0_wready : in STD_LOGIC;
    M_AXI_GP0_bid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_rid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_arready : out STD_LOGIC;
    S_AXI_HP0_awready : out STD_LOGIC;
    S_AXI_HP0_bvalid : out STD_LOGIC;
    S_AXI_HP0_rlast : out STD_LOGIC;
    S_AXI_HP0_rvalid : out STD_LOGIC;
    S_AXI_HP0_wready : out STD_LOGIC;
    S_AXI_HP0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_arvalid : in STD_LOGIC;
    S_AXI_HP0_awvalid : in STD_LOGIC;
    S_AXI_HP0_bready : in STD_LOGIC;
    S_AXI_HP0_rready : in STD_LOGIC;
    S_AXI_HP0_wlast : in STD_LOGIC;
    S_AXI_HP0_wvalid : in STD_LOGIC;
    S_AXI_HP0_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    USBIND_port_indctl : out STD_LOGIC_VECTOR ( 1 downto 0 );
    USBIND_vbus_pwrselect : out STD_LOGIC;
    USBIND_vbus_pwrfault : in STD_LOGIC;
    S_AXI_HP2_arready : out STD_LOGIC;
    S_AXI_HP2_awready : out STD_LOGIC;
    S_AXI_HP2_bvalid : out STD_LOGIC;
    S_AXI_HP2_rlast : out STD_LOGIC;
    S_AXI_HP2_rvalid : out STD_LOGIC;
    S_AXI_HP2_wready : out STD_LOGIC;
    S_AXI_HP2_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_arvalid : in STD_LOGIC;
    S_AXI_HP2_awvalid : in STD_LOGIC;
    S_AXI_HP2_bready : in STD_LOGIC;
    S_AXI_HP2_rready : in STD_LOGIC;
    S_AXI_HP2_wlast : in STD_LOGIC;
    S_AXI_HP2_wvalid : in STD_LOGIC;
    S_AXI_HP2_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP1_arready : out STD_LOGIC;
    S_AXI_HP1_awready : out STD_LOGIC;
    S_AXI_HP1_bvalid : out STD_LOGIC;
    S_AXI_HP1_rlast : out STD_LOGIC;
    S_AXI_HP1_rvalid : out STD_LOGIC;
    S_AXI_HP1_wready : out STD_LOGIC;
    S_AXI_HP1_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_arvalid : in STD_LOGIC;
    S_AXI_HP1_awvalid : in STD_LOGIC;
    S_AXI_HP1_bready : in STD_LOGIC;
    S_AXI_HP1_rready : in STD_LOGIC;
    S_AXI_HP1_wlast : in STD_LOGIC;
    S_AXI_HP1_wvalid : in STD_LOGIC;
    S_AXI_HP1_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP3_arready : out STD_LOGIC;
    S_AXI_HP3_awready : out STD_LOGIC;
    S_AXI_HP3_bvalid : out STD_LOGIC;
    S_AXI_HP3_rlast : out STD_LOGIC;
    S_AXI_HP3_rvalid : out STD_LOGIC;
    S_AXI_HP3_wready : out STD_LOGIC;
    S_AXI_HP3_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_arvalid : in STD_LOGIC;
    S_AXI_HP3_awvalid : in STD_LOGIC;
    S_AXI_HP3_bready : in STD_LOGIC;
    S_AXI_HP3_rready : in STD_LOGIC;
    S_AXI_HP3_wlast : in STD_LOGIC;
    S_AXI_HP3_wvalid : in STD_LOGIC;
    S_AXI_HP3_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    M_AXI_GP0_ACLK : in STD_LOGIC
  );
  end component n310_ps;
begin
n310_ps_i: component n310_ps
     port map (
      DDR_VRN => DDR_VRN,
      DDR_VRP => DDR_VRP,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FCLK_CLK0 => FCLK_CLK0,
      FCLK_CLK1 => FCLK_CLK1,
      FCLK_CLK2 => FCLK_CLK2,
      FCLK_CLK3 => FCLK_CLK3,
      FCLK_RESET0_N => FCLK_RESET0_N,
      FCLK_RESET1_N => FCLK_RESET1_N,
      FCLK_RESET2_N => FCLK_RESET2_N,
      FCLK_RESET3_N => FCLK_RESET3_N,
      GPIO_I(63 downto 0) => GPIO_I(63 downto 0),
      GPIO_O(63 downto 0) => GPIO_O(63 downto 0),
      GPIO_T(63 downto 0) => GPIO_T(63 downto 0),
      MIO(53 downto 0) => MIO(53 downto 0),
      M_AXI_GP0_ACLK => M_AXI_GP0_ACLK,
      M_AXI_GP0_araddr(31 downto 0) => M_AXI_GP0_araddr(31 downto 0),
      M_AXI_GP0_arburst(1 downto 0) => M_AXI_GP0_arburst(1 downto 0),
      M_AXI_GP0_arcache(3 downto 0) => M_AXI_GP0_arcache(3 downto 0),
      M_AXI_GP0_arid(11 downto 0) => M_AXI_GP0_arid(11 downto 0),
      M_AXI_GP0_arlen(3 downto 0) => M_AXI_GP0_arlen(3 downto 0),
      M_AXI_GP0_arlock(1 downto 0) => M_AXI_GP0_arlock(1 downto 0),
      M_AXI_GP0_arprot(2 downto 0) => M_AXI_GP0_arprot(2 downto 0),
      M_AXI_GP0_arqos(3 downto 0) => M_AXI_GP0_arqos(3 downto 0),
      M_AXI_GP0_arready => M_AXI_GP0_arready,
      M_AXI_GP0_arsize(2 downto 0) => M_AXI_GP0_arsize(2 downto 0),
      M_AXI_GP0_arvalid => M_AXI_GP0_arvalid,
      M_AXI_GP0_awaddr(31 downto 0) => M_AXI_GP0_awaddr(31 downto 0),
      M_AXI_GP0_awburst(1 downto 0) => M_AXI_GP0_awburst(1 downto 0),
      M_AXI_GP0_awcache(3 downto 0) => M_AXI_GP0_awcache(3 downto 0),
      M_AXI_GP0_awid(11 downto 0) => M_AXI_GP0_awid(11 downto 0),
      M_AXI_GP0_awlen(3 downto 0) => M_AXI_GP0_awlen(3 downto 0),
      M_AXI_GP0_awlock(1 downto 0) => M_AXI_GP0_awlock(1 downto 0),
      M_AXI_GP0_awprot(2 downto 0) => M_AXI_GP0_awprot(2 downto 0),
      M_AXI_GP0_awqos(3 downto 0) => M_AXI_GP0_awqos(3 downto 0),
      M_AXI_GP0_awready => M_AXI_GP0_awready,
      M_AXI_GP0_awsize(2 downto 0) => M_AXI_GP0_awsize(2 downto 0),
      M_AXI_GP0_awvalid => M_AXI_GP0_awvalid,
      M_AXI_GP0_bid(11 downto 0) => M_AXI_GP0_bid(11 downto 0),
      M_AXI_GP0_bready => M_AXI_GP0_bready,
      M_AXI_GP0_bresp(1 downto 0) => M_AXI_GP0_bresp(1 downto 0),
      M_AXI_GP0_bvalid => M_AXI_GP0_bvalid,
      M_AXI_GP0_rdata(31 downto 0) => M_AXI_GP0_rdata(31 downto 0),
      M_AXI_GP0_rid(11 downto 0) => M_AXI_GP0_rid(11 downto 0),
      M_AXI_GP0_rlast => M_AXI_GP0_rlast,
      M_AXI_GP0_rready => M_AXI_GP0_rready,
      M_AXI_GP0_rresp(1 downto 0) => M_AXI_GP0_rresp(1 downto 0),
      M_AXI_GP0_rvalid => M_AXI_GP0_rvalid,
      M_AXI_GP0_wdata(31 downto 0) => M_AXI_GP0_wdata(31 downto 0),
      M_AXI_GP0_wid(11 downto 0) => M_AXI_GP0_wid(11 downto 0),
      M_AXI_GP0_wlast => M_AXI_GP0_wlast,
      M_AXI_GP0_wready => M_AXI_GP0_wready,
      M_AXI_GP0_wstrb(3 downto 0) => M_AXI_GP0_wstrb(3 downto 0),
      M_AXI_GP0_wvalid => M_AXI_GP0_wvalid,
      PS_CLK => PS_CLK,
      PS_PORB => PS_PORB,
      PS_SRSTB => PS_SRSTB,
      S_AXI_HP0_ACLK => S_AXI_HP0_ACLK,
      S_AXI_HP0_araddr(31 downto 0) => S_AXI_HP0_araddr(31 downto 0),
      S_AXI_HP0_arburst(1 downto 0) => S_AXI_HP0_arburst(1 downto 0),
      S_AXI_HP0_arcache(3 downto 0) => S_AXI_HP0_arcache(3 downto 0),
      S_AXI_HP0_arid(5 downto 0) => S_AXI_HP0_arid(5 downto 0),
      S_AXI_HP0_arlen(3 downto 0) => S_AXI_HP0_arlen(3 downto 0),
      S_AXI_HP0_arlock(1 downto 0) => S_AXI_HP0_arlock(1 downto 0),
      S_AXI_HP0_arprot(2 downto 0) => S_AXI_HP0_arprot(2 downto 0),
      S_AXI_HP0_arqos(3 downto 0) => S_AXI_HP0_arqos(3 downto 0),
      S_AXI_HP0_arready => S_AXI_HP0_arready,
      S_AXI_HP0_arsize(2 downto 0) => S_AXI_HP0_arsize(2 downto 0),
      S_AXI_HP0_arvalid => S_AXI_HP0_arvalid,
      S_AXI_HP0_awaddr(31 downto 0) => S_AXI_HP0_awaddr(31 downto 0),
      S_AXI_HP0_awburst(1 downto 0) => S_AXI_HP0_awburst(1 downto 0),
      S_AXI_HP0_awcache(3 downto 0) => S_AXI_HP0_awcache(3 downto 0),
      S_AXI_HP0_awid(5 downto 0) => S_AXI_HP0_awid(5 downto 0),
      S_AXI_HP0_awlen(3 downto 0) => S_AXI_HP0_awlen(3 downto 0),
      S_AXI_HP0_awlock(1 downto 0) => S_AXI_HP0_awlock(1 downto 0),
      S_AXI_HP0_awprot(2 downto 0) => S_AXI_HP0_awprot(2 downto 0),
      S_AXI_HP0_awqos(3 downto 0) => S_AXI_HP0_awqos(3 downto 0),
      S_AXI_HP0_awready => S_AXI_HP0_awready,
      S_AXI_HP0_awsize(2 downto 0) => S_AXI_HP0_awsize(2 downto 0),
      S_AXI_HP0_awvalid => S_AXI_HP0_awvalid,
      S_AXI_HP0_bid(5 downto 0) => S_AXI_HP0_bid(5 downto 0),
      S_AXI_HP0_bready => S_AXI_HP0_bready,
      S_AXI_HP0_bresp(1 downto 0) => S_AXI_HP0_bresp(1 downto 0),
      S_AXI_HP0_bvalid => S_AXI_HP0_bvalid,
      S_AXI_HP0_rdata(63 downto 0) => S_AXI_HP0_rdata(63 downto 0),
      S_AXI_HP0_rid(5 downto 0) => S_AXI_HP0_rid(5 downto 0),
      S_AXI_HP0_rlast => S_AXI_HP0_rlast,
      S_AXI_HP0_rready => S_AXI_HP0_rready,
      S_AXI_HP0_rresp(1 downto 0) => S_AXI_HP0_rresp(1 downto 0),
      S_AXI_HP0_rvalid => S_AXI_HP0_rvalid,
      S_AXI_HP0_wdata(63 downto 0) => S_AXI_HP0_wdata(63 downto 0),
      S_AXI_HP0_wid(5 downto 0) => S_AXI_HP0_wid(5 downto 0),
      S_AXI_HP0_wlast => S_AXI_HP0_wlast,
      S_AXI_HP0_wready => S_AXI_HP0_wready,
      S_AXI_HP0_wstrb(7 downto 0) => S_AXI_HP0_wstrb(7 downto 0),
      S_AXI_HP0_wvalid => S_AXI_HP0_wvalid,
      S_AXI_HP1_ACLK => S_AXI_HP1_ACLK,
      S_AXI_HP1_araddr(31 downto 0) => S_AXI_HP1_araddr(31 downto 0),
      S_AXI_HP1_arburst(1 downto 0) => S_AXI_HP1_arburst(1 downto 0),
      S_AXI_HP1_arcache(3 downto 0) => S_AXI_HP1_arcache(3 downto 0),
      S_AXI_HP1_arid(5 downto 0) => S_AXI_HP1_arid(5 downto 0),
      S_AXI_HP1_arlen(3 downto 0) => S_AXI_HP1_arlen(3 downto 0),
      S_AXI_HP1_arlock(1 downto 0) => S_AXI_HP1_arlock(1 downto 0),
      S_AXI_HP1_arprot(2 downto 0) => S_AXI_HP1_arprot(2 downto 0),
      S_AXI_HP1_arqos(3 downto 0) => S_AXI_HP1_arqos(3 downto 0),
      S_AXI_HP1_arready => S_AXI_HP1_arready,
      S_AXI_HP1_arsize(2 downto 0) => S_AXI_HP1_arsize(2 downto 0),
      S_AXI_HP1_arvalid => S_AXI_HP1_arvalid,
      S_AXI_HP1_awaddr(31 downto 0) => S_AXI_HP1_awaddr(31 downto 0),
      S_AXI_HP1_awburst(1 downto 0) => S_AXI_HP1_awburst(1 downto 0),
      S_AXI_HP1_awcache(3 downto 0) => S_AXI_HP1_awcache(3 downto 0),
      S_AXI_HP1_awid(5 downto 0) => S_AXI_HP1_awid(5 downto 0),
      S_AXI_HP1_awlen(3 downto 0) => S_AXI_HP1_awlen(3 downto 0),
      S_AXI_HP1_awlock(1 downto 0) => S_AXI_HP1_awlock(1 downto 0),
      S_AXI_HP1_awprot(2 downto 0) => S_AXI_HP1_awprot(2 downto 0),
      S_AXI_HP1_awqos(3 downto 0) => S_AXI_HP1_awqos(3 downto 0),
      S_AXI_HP1_awready => S_AXI_HP1_awready,
      S_AXI_HP1_awsize(2 downto 0) => S_AXI_HP1_awsize(2 downto 0),
      S_AXI_HP1_awvalid => S_AXI_HP1_awvalid,
      S_AXI_HP1_bid(5 downto 0) => S_AXI_HP1_bid(5 downto 0),
      S_AXI_HP1_bready => S_AXI_HP1_bready,
      S_AXI_HP1_bresp(1 downto 0) => S_AXI_HP1_bresp(1 downto 0),
      S_AXI_HP1_bvalid => S_AXI_HP1_bvalid,
      S_AXI_HP1_rdata(63 downto 0) => S_AXI_HP1_rdata(63 downto 0),
      S_AXI_HP1_rid(5 downto 0) => S_AXI_HP1_rid(5 downto 0),
      S_AXI_HP1_rlast => S_AXI_HP1_rlast,
      S_AXI_HP1_rready => S_AXI_HP1_rready,
      S_AXI_HP1_rresp(1 downto 0) => S_AXI_HP1_rresp(1 downto 0),
      S_AXI_HP1_rvalid => S_AXI_HP1_rvalid,
      S_AXI_HP1_wdata(63 downto 0) => S_AXI_HP1_wdata(63 downto 0),
      S_AXI_HP1_wid(5 downto 0) => S_AXI_HP1_wid(5 downto 0),
      S_AXI_HP1_wlast => S_AXI_HP1_wlast,
      S_AXI_HP1_wready => S_AXI_HP1_wready,
      S_AXI_HP1_wstrb(7 downto 0) => S_AXI_HP1_wstrb(7 downto 0),
      S_AXI_HP1_wvalid => S_AXI_HP1_wvalid,
      S_AXI_HP2_ACLK => S_AXI_HP2_ACLK,
      S_AXI_HP2_araddr(31 downto 0) => S_AXI_HP2_araddr(31 downto 0),
      S_AXI_HP2_arburst(1 downto 0) => S_AXI_HP2_arburst(1 downto 0),
      S_AXI_HP2_arcache(3 downto 0) => S_AXI_HP2_arcache(3 downto 0),
      S_AXI_HP2_arid(5 downto 0) => S_AXI_HP2_arid(5 downto 0),
      S_AXI_HP2_arlen(3 downto 0) => S_AXI_HP2_arlen(3 downto 0),
      S_AXI_HP2_arlock(1 downto 0) => S_AXI_HP2_arlock(1 downto 0),
      S_AXI_HP2_arprot(2 downto 0) => S_AXI_HP2_arprot(2 downto 0),
      S_AXI_HP2_arqos(3 downto 0) => S_AXI_HP2_arqos(3 downto 0),
      S_AXI_HP2_arready => S_AXI_HP2_arready,
      S_AXI_HP2_arsize(2 downto 0) => S_AXI_HP2_arsize(2 downto 0),
      S_AXI_HP2_arvalid => S_AXI_HP2_arvalid,
      S_AXI_HP2_awaddr(31 downto 0) => S_AXI_HP2_awaddr(31 downto 0),
      S_AXI_HP2_awburst(1 downto 0) => S_AXI_HP2_awburst(1 downto 0),
      S_AXI_HP2_awcache(3 downto 0) => S_AXI_HP2_awcache(3 downto 0),
      S_AXI_HP2_awid(5 downto 0) => S_AXI_HP2_awid(5 downto 0),
      S_AXI_HP2_awlen(3 downto 0) => S_AXI_HP2_awlen(3 downto 0),
      S_AXI_HP2_awlock(1 downto 0) => S_AXI_HP2_awlock(1 downto 0),
      S_AXI_HP2_awprot(2 downto 0) => S_AXI_HP2_awprot(2 downto 0),
      S_AXI_HP2_awqos(3 downto 0) => S_AXI_HP2_awqos(3 downto 0),
      S_AXI_HP2_awready => S_AXI_HP2_awready,
      S_AXI_HP2_awsize(2 downto 0) => S_AXI_HP2_awsize(2 downto 0),
      S_AXI_HP2_awvalid => S_AXI_HP2_awvalid,
      S_AXI_HP2_bid(5 downto 0) => S_AXI_HP2_bid(5 downto 0),
      S_AXI_HP2_bready => S_AXI_HP2_bready,
      S_AXI_HP2_bresp(1 downto 0) => S_AXI_HP2_bresp(1 downto 0),
      S_AXI_HP2_bvalid => S_AXI_HP2_bvalid,
      S_AXI_HP2_rdata(63 downto 0) => S_AXI_HP2_rdata(63 downto 0),
      S_AXI_HP2_rid(5 downto 0) => S_AXI_HP2_rid(5 downto 0),
      S_AXI_HP2_rlast => S_AXI_HP2_rlast,
      S_AXI_HP2_rready => S_AXI_HP2_rready,
      S_AXI_HP2_rresp(1 downto 0) => S_AXI_HP2_rresp(1 downto 0),
      S_AXI_HP2_rvalid => S_AXI_HP2_rvalid,
      S_AXI_HP2_wdata(63 downto 0) => S_AXI_HP2_wdata(63 downto 0),
      S_AXI_HP2_wid(5 downto 0) => S_AXI_HP2_wid(5 downto 0),
      S_AXI_HP2_wlast => S_AXI_HP2_wlast,
      S_AXI_HP2_wready => S_AXI_HP2_wready,
      S_AXI_HP2_wstrb(7 downto 0) => S_AXI_HP2_wstrb(7 downto 0),
      S_AXI_HP2_wvalid => S_AXI_HP2_wvalid,
      S_AXI_HP3_ACLK => S_AXI_HP3_ACLK,
      S_AXI_HP3_araddr(31 downto 0) => S_AXI_HP3_araddr(31 downto 0),
      S_AXI_HP3_arburst(1 downto 0) => S_AXI_HP3_arburst(1 downto 0),
      S_AXI_HP3_arcache(3 downto 0) => S_AXI_HP3_arcache(3 downto 0),
      S_AXI_HP3_arid(5 downto 0) => S_AXI_HP3_arid(5 downto 0),
      S_AXI_HP3_arlen(3 downto 0) => S_AXI_HP3_arlen(3 downto 0),
      S_AXI_HP3_arlock(1 downto 0) => S_AXI_HP3_arlock(1 downto 0),
      S_AXI_HP3_arprot(2 downto 0) => S_AXI_HP3_arprot(2 downto 0),
      S_AXI_HP3_arqos(3 downto 0) => S_AXI_HP3_arqos(3 downto 0),
      S_AXI_HP3_arready => S_AXI_HP3_arready,
      S_AXI_HP3_arsize(2 downto 0) => S_AXI_HP3_arsize(2 downto 0),
      S_AXI_HP3_arvalid => S_AXI_HP3_arvalid,
      S_AXI_HP3_awaddr(31 downto 0) => S_AXI_HP3_awaddr(31 downto 0),
      S_AXI_HP3_awburst(1 downto 0) => S_AXI_HP3_awburst(1 downto 0),
      S_AXI_HP3_awcache(3 downto 0) => S_AXI_HP3_awcache(3 downto 0),
      S_AXI_HP3_awid(5 downto 0) => S_AXI_HP3_awid(5 downto 0),
      S_AXI_HP3_awlen(3 downto 0) => S_AXI_HP3_awlen(3 downto 0),
      S_AXI_HP3_awlock(1 downto 0) => S_AXI_HP3_awlock(1 downto 0),
      S_AXI_HP3_awprot(2 downto 0) => S_AXI_HP3_awprot(2 downto 0),
      S_AXI_HP3_awqos(3 downto 0) => S_AXI_HP3_awqos(3 downto 0),
      S_AXI_HP3_awready => S_AXI_HP3_awready,
      S_AXI_HP3_awsize(2 downto 0) => S_AXI_HP3_awsize(2 downto 0),
      S_AXI_HP3_awvalid => S_AXI_HP3_awvalid,
      S_AXI_HP3_bid(5 downto 0) => S_AXI_HP3_bid(5 downto 0),
      S_AXI_HP3_bready => S_AXI_HP3_bready,
      S_AXI_HP3_bresp(1 downto 0) => S_AXI_HP3_bresp(1 downto 0),
      S_AXI_HP3_bvalid => S_AXI_HP3_bvalid,
      S_AXI_HP3_rdata(63 downto 0) => S_AXI_HP3_rdata(63 downto 0),
      S_AXI_HP3_rid(5 downto 0) => S_AXI_HP3_rid(5 downto 0),
      S_AXI_HP3_rlast => S_AXI_HP3_rlast,
      S_AXI_HP3_rready => S_AXI_HP3_rready,
      S_AXI_HP3_rresp(1 downto 0) => S_AXI_HP3_rresp(1 downto 0),
      S_AXI_HP3_rvalid => S_AXI_HP3_rvalid,
      S_AXI_HP3_wdata(63 downto 0) => S_AXI_HP3_wdata(63 downto 0),
      S_AXI_HP3_wid(5 downto 0) => S_AXI_HP3_wid(5 downto 0),
      S_AXI_HP3_wlast => S_AXI_HP3_wlast,
      S_AXI_HP3_wready => S_AXI_HP3_wready,
      S_AXI_HP3_wstrb(7 downto 0) => S_AXI_HP3_wstrb(7 downto 0),
      S_AXI_HP3_wvalid => S_AXI_HP3_wvalid,
      USBIND_port_indctl(1 downto 0) => USBIND_port_indctl(1 downto 0),
      USBIND_vbus_pwrfault => USBIND_vbus_pwrfault,
      USBIND_vbus_pwrselect => USBIND_vbus_pwrselect
    );
end STRUCTURE;
