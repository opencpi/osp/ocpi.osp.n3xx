// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Tue Sep 27 10:43:12 2022
// Host        : localhost.localdomain running 64-bit CentOS Linux release 7.9.2009 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/opencpi/Xilinx/ocpi.osp.n310_ps/ocpi.osp.n310_ps.srcs/sources_1/bd/n310_ps/n310_ps_stub.v
// Design      : n310_ps
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z100ffg900-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module n310_ps(DDR_VRN, DDR_VRP, DDR_addr, DDR_ba, DDR_cas_n, 
  DDR_ck_n, DDR_ck_p, DDR_cke, DDR_cs_n, DDR_dm, DDR_dq, DDR_dqs_n, DDR_dqs_p, DDR_odt, DDR_ras_n, 
  DDR_reset_n, DDR_we_n, FCLK_CLK0, FCLK_CLK1, FCLK_CLK2, FCLK_CLK3, FCLK_RESET0_N, 
  FCLK_RESET1_N, FCLK_RESET2_N, FCLK_RESET3_N, GPIO_I, GPIO_O, GPIO_T, MIO, M_AXI_GP0_ACLK, 
  M_AXI_GP0_araddr, M_AXI_GP0_arburst, M_AXI_GP0_arcache, M_AXI_GP0_arid, M_AXI_GP0_arlen, 
  M_AXI_GP0_arlock, M_AXI_GP0_arprot, M_AXI_GP0_arqos, M_AXI_GP0_arready, M_AXI_GP0_arsize, 
  M_AXI_GP0_arvalid, M_AXI_GP0_awaddr, M_AXI_GP0_awburst, M_AXI_GP0_awcache, 
  M_AXI_GP0_awid, M_AXI_GP0_awlen, M_AXI_GP0_awlock, M_AXI_GP0_awprot, M_AXI_GP0_awqos, 
  M_AXI_GP0_awready, M_AXI_GP0_awsize, M_AXI_GP0_awvalid, M_AXI_GP0_bid, M_AXI_GP0_bready, 
  M_AXI_GP0_bresp, M_AXI_GP0_bvalid, M_AXI_GP0_rdata, M_AXI_GP0_rid, M_AXI_GP0_rlast, 
  M_AXI_GP0_rready, M_AXI_GP0_rresp, M_AXI_GP0_rvalid, M_AXI_GP0_wdata, M_AXI_GP0_wid, 
  M_AXI_GP0_wlast, M_AXI_GP0_wready, M_AXI_GP0_wstrb, M_AXI_GP0_wvalid, PS_CLK, PS_PORB, 
  PS_SRSTB, S_AXI_HP0_ACLK, S_AXI_HP0_araddr, S_AXI_HP0_arburst, S_AXI_HP0_arcache, 
  S_AXI_HP0_arid, S_AXI_HP0_arlen, S_AXI_HP0_arlock, S_AXI_HP0_arprot, S_AXI_HP0_arqos, 
  S_AXI_HP0_arready, S_AXI_HP0_arsize, S_AXI_HP0_arvalid, S_AXI_HP0_awaddr, 
  S_AXI_HP0_awburst, S_AXI_HP0_awcache, S_AXI_HP0_awid, S_AXI_HP0_awlen, S_AXI_HP0_awlock, 
  S_AXI_HP0_awprot, S_AXI_HP0_awqos, S_AXI_HP0_awready, S_AXI_HP0_awsize, 
  S_AXI_HP0_awvalid, S_AXI_HP0_bid, S_AXI_HP0_bready, S_AXI_HP0_bresp, S_AXI_HP0_bvalid, 
  S_AXI_HP0_rdata, S_AXI_HP0_rid, S_AXI_HP0_rlast, S_AXI_HP0_rready, S_AXI_HP0_rresp, 
  S_AXI_HP0_rvalid, S_AXI_HP0_wdata, S_AXI_HP0_wid, S_AXI_HP0_wlast, S_AXI_HP0_wready, 
  S_AXI_HP0_wstrb, S_AXI_HP0_wvalid, S_AXI_HP1_ACLK, S_AXI_HP1_araddr, S_AXI_HP1_arburst, 
  S_AXI_HP1_arcache, S_AXI_HP1_arid, S_AXI_HP1_arlen, S_AXI_HP1_arlock, S_AXI_HP1_arprot, 
  S_AXI_HP1_arqos, S_AXI_HP1_arready, S_AXI_HP1_arsize, S_AXI_HP1_arvalid, 
  S_AXI_HP1_awaddr, S_AXI_HP1_awburst, S_AXI_HP1_awcache, S_AXI_HP1_awid, S_AXI_HP1_awlen, 
  S_AXI_HP1_awlock, S_AXI_HP1_awprot, S_AXI_HP1_awqos, S_AXI_HP1_awready, S_AXI_HP1_awsize, 
  S_AXI_HP1_awvalid, S_AXI_HP1_bid, S_AXI_HP1_bready, S_AXI_HP1_bresp, S_AXI_HP1_bvalid, 
  S_AXI_HP1_rdata, S_AXI_HP1_rid, S_AXI_HP1_rlast, S_AXI_HP1_rready, S_AXI_HP1_rresp, 
  S_AXI_HP1_rvalid, S_AXI_HP1_wdata, S_AXI_HP1_wid, S_AXI_HP1_wlast, S_AXI_HP1_wready, 
  S_AXI_HP1_wstrb, S_AXI_HP1_wvalid, S_AXI_HP2_ACLK, S_AXI_HP2_araddr, S_AXI_HP2_arburst, 
  S_AXI_HP2_arcache, S_AXI_HP2_arid, S_AXI_HP2_arlen, S_AXI_HP2_arlock, S_AXI_HP2_arprot, 
  S_AXI_HP2_arqos, S_AXI_HP2_arready, S_AXI_HP2_arsize, S_AXI_HP2_arvalid, 
  S_AXI_HP2_awaddr, S_AXI_HP2_awburst, S_AXI_HP2_awcache, S_AXI_HP2_awid, S_AXI_HP2_awlen, 
  S_AXI_HP2_awlock, S_AXI_HP2_awprot, S_AXI_HP2_awqos, S_AXI_HP2_awready, S_AXI_HP2_awsize, 
  S_AXI_HP2_awvalid, S_AXI_HP2_bid, S_AXI_HP2_bready, S_AXI_HP2_bresp, S_AXI_HP2_bvalid, 
  S_AXI_HP2_rdata, S_AXI_HP2_rid, S_AXI_HP2_rlast, S_AXI_HP2_rready, S_AXI_HP2_rresp, 
  S_AXI_HP2_rvalid, S_AXI_HP2_wdata, S_AXI_HP2_wid, S_AXI_HP2_wlast, S_AXI_HP2_wready, 
  S_AXI_HP2_wstrb, S_AXI_HP2_wvalid, S_AXI_HP3_ACLK, S_AXI_HP3_araddr, S_AXI_HP3_arburst, 
  S_AXI_HP3_arcache, S_AXI_HP3_arid, S_AXI_HP3_arlen, S_AXI_HP3_arlock, S_AXI_HP3_arprot, 
  S_AXI_HP3_arqos, S_AXI_HP3_arready, S_AXI_HP3_arsize, S_AXI_HP3_arvalid, 
  S_AXI_HP3_awaddr, S_AXI_HP3_awburst, S_AXI_HP3_awcache, S_AXI_HP3_awid, S_AXI_HP3_awlen, 
  S_AXI_HP3_awlock, S_AXI_HP3_awprot, S_AXI_HP3_awqos, S_AXI_HP3_awready, S_AXI_HP3_awsize, 
  S_AXI_HP3_awvalid, S_AXI_HP3_bid, S_AXI_HP3_bready, S_AXI_HP3_bresp, S_AXI_HP3_bvalid, 
  S_AXI_HP3_rdata, S_AXI_HP3_rid, S_AXI_HP3_rlast, S_AXI_HP3_rready, S_AXI_HP3_rresp, 
  S_AXI_HP3_rvalid, S_AXI_HP3_wdata, S_AXI_HP3_wid, S_AXI_HP3_wlast, S_AXI_HP3_wready, 
  S_AXI_HP3_wstrb, S_AXI_HP3_wvalid, USBIND_port_indctl, USBIND_vbus_pwrfault, 
  USBIND_vbus_pwrselect)
/* synthesis syn_black_box black_box_pad_pin="DDR_VRN,DDR_VRP,DDR_addr[14:0],DDR_ba[2:0],DDR_cas_n,DDR_ck_n,DDR_ck_p,DDR_cke,DDR_cs_n,DDR_dm[3:0],DDR_dq[31:0],DDR_dqs_n[3:0],DDR_dqs_p[3:0],DDR_odt,DDR_ras_n,DDR_reset_n,DDR_we_n,FCLK_CLK0,FCLK_CLK1,FCLK_CLK2,FCLK_CLK3,FCLK_RESET0_N,FCLK_RESET1_N,FCLK_RESET2_N,FCLK_RESET3_N,GPIO_I[63:0],GPIO_O[63:0],GPIO_T[63:0],MIO[53:0],M_AXI_GP0_ACLK,M_AXI_GP0_araddr[31:0],M_AXI_GP0_arburst[1:0],M_AXI_GP0_arcache[3:0],M_AXI_GP0_arid[11:0],M_AXI_GP0_arlen[3:0],M_AXI_GP0_arlock[1:0],M_AXI_GP0_arprot[2:0],M_AXI_GP0_arqos[3:0],M_AXI_GP0_arready,M_AXI_GP0_arsize[2:0],M_AXI_GP0_arvalid,M_AXI_GP0_awaddr[31:0],M_AXI_GP0_awburst[1:0],M_AXI_GP0_awcache[3:0],M_AXI_GP0_awid[11:0],M_AXI_GP0_awlen[3:0],M_AXI_GP0_awlock[1:0],M_AXI_GP0_awprot[2:0],M_AXI_GP0_awqos[3:0],M_AXI_GP0_awready,M_AXI_GP0_awsize[2:0],M_AXI_GP0_awvalid,M_AXI_GP0_bid[11:0],M_AXI_GP0_bready,M_AXI_GP0_bresp[1:0],M_AXI_GP0_bvalid,M_AXI_GP0_rdata[31:0],M_AXI_GP0_rid[11:0],M_AXI_GP0_rlast,M_AXI_GP0_rready,M_AXI_GP0_rresp[1:0],M_AXI_GP0_rvalid,M_AXI_GP0_wdata[31:0],M_AXI_GP0_wid[11:0],M_AXI_GP0_wlast,M_AXI_GP0_wready,M_AXI_GP0_wstrb[3:0],M_AXI_GP0_wvalid,PS_CLK,PS_PORB,PS_SRSTB,S_AXI_HP0_ACLK,S_AXI_HP0_araddr[31:0],S_AXI_HP0_arburst[1:0],S_AXI_HP0_arcache[3:0],S_AXI_HP0_arid[5:0],S_AXI_HP0_arlen[3:0],S_AXI_HP0_arlock[1:0],S_AXI_HP0_arprot[2:0],S_AXI_HP0_arqos[3:0],S_AXI_HP0_arready,S_AXI_HP0_arsize[2:0],S_AXI_HP0_arvalid,S_AXI_HP0_awaddr[31:0],S_AXI_HP0_awburst[1:0],S_AXI_HP0_awcache[3:0],S_AXI_HP0_awid[5:0],S_AXI_HP0_awlen[3:0],S_AXI_HP0_awlock[1:0],S_AXI_HP0_awprot[2:0],S_AXI_HP0_awqos[3:0],S_AXI_HP0_awready,S_AXI_HP0_awsize[2:0],S_AXI_HP0_awvalid,S_AXI_HP0_bid[5:0],S_AXI_HP0_bready,S_AXI_HP0_bresp[1:0],S_AXI_HP0_bvalid,S_AXI_HP0_rdata[63:0],S_AXI_HP0_rid[5:0],S_AXI_HP0_rlast,S_AXI_HP0_rready,S_AXI_HP0_rresp[1:0],S_AXI_HP0_rvalid,S_AXI_HP0_wdata[63:0],S_AXI_HP0_wid[5:0],S_AXI_HP0_wlast,S_AXI_HP0_wready,S_AXI_HP0_wstrb[7:0],S_AXI_HP0_wvalid,S_AXI_HP1_ACLK,S_AXI_HP1_araddr[31:0],S_AXI_HP1_arburst[1:0],S_AXI_HP1_arcache[3:0],S_AXI_HP1_arid[5:0],S_AXI_HP1_arlen[3:0],S_AXI_HP1_arlock[1:0],S_AXI_HP1_arprot[2:0],S_AXI_HP1_arqos[3:0],S_AXI_HP1_arready,S_AXI_HP1_arsize[2:0],S_AXI_HP1_arvalid,S_AXI_HP1_awaddr[31:0],S_AXI_HP1_awburst[1:0],S_AXI_HP1_awcache[3:0],S_AXI_HP1_awid[5:0],S_AXI_HP1_awlen[3:0],S_AXI_HP1_awlock[1:0],S_AXI_HP1_awprot[2:0],S_AXI_HP1_awqos[3:0],S_AXI_HP1_awready,S_AXI_HP1_awsize[2:0],S_AXI_HP1_awvalid,S_AXI_HP1_bid[5:0],S_AXI_HP1_bready,S_AXI_HP1_bresp[1:0],S_AXI_HP1_bvalid,S_AXI_HP1_rdata[63:0],S_AXI_HP1_rid[5:0],S_AXI_HP1_rlast,S_AXI_HP1_rready,S_AXI_HP1_rresp[1:0],S_AXI_HP1_rvalid,S_AXI_HP1_wdata[63:0],S_AXI_HP1_wid[5:0],S_AXI_HP1_wlast,S_AXI_HP1_wready,S_AXI_HP1_wstrb[7:0],S_AXI_HP1_wvalid,S_AXI_HP2_ACLK,S_AXI_HP2_araddr[31:0],S_AXI_HP2_arburst[1:0],S_AXI_HP2_arcache[3:0],S_AXI_HP2_arid[5:0],S_AXI_HP2_arlen[3:0],S_AXI_HP2_arlock[1:0],S_AXI_HP2_arprot[2:0],S_AXI_HP2_arqos[3:0],S_AXI_HP2_arready,S_AXI_HP2_arsize[2:0],S_AXI_HP2_arvalid,S_AXI_HP2_awaddr[31:0],S_AXI_HP2_awburst[1:0],S_AXI_HP2_awcache[3:0],S_AXI_HP2_awid[5:0],S_AXI_HP2_awlen[3:0],S_AXI_HP2_awlock[1:0],S_AXI_HP2_awprot[2:0],S_AXI_HP2_awqos[3:0],S_AXI_HP2_awready,S_AXI_HP2_awsize[2:0],S_AXI_HP2_awvalid,S_AXI_HP2_bid[5:0],S_AXI_HP2_bready,S_AXI_HP2_bresp[1:0],S_AXI_HP2_bvalid,S_AXI_HP2_rdata[63:0],S_AXI_HP2_rid[5:0],S_AXI_HP2_rlast,S_AXI_HP2_rready,S_AXI_HP2_rresp[1:0],S_AXI_HP2_rvalid,S_AXI_HP2_wdata[63:0],S_AXI_HP2_wid[5:0],S_AXI_HP2_wlast,S_AXI_HP2_wready,S_AXI_HP2_wstrb[7:0],S_AXI_HP2_wvalid,S_AXI_HP3_ACLK,S_AXI_HP3_araddr[31:0],S_AXI_HP3_arburst[1:0],S_AXI_HP3_arcache[3:0],S_AXI_HP3_arid[5:0],S_AXI_HP3_arlen[3:0],S_AXI_HP3_arlock[1:0],S_AXI_HP3_arprot[2:0],S_AXI_HP3_arqos[3:0],S_AXI_HP3_arready,S_AXI_HP3_arsize[2:0],S_AXI_HP3_arvalid,S_AXI_HP3_awaddr[31:0],S_AXI_HP3_awburst[1:0],S_AXI_HP3_awcache[3:0],S_AXI_HP3_awid[5:0],S_AXI_HP3_awlen[3:0],S_AXI_HP3_awlock[1:0],S_AXI_HP3_awprot[2:0],S_AXI_HP3_awqos[3:0],S_AXI_HP3_awready,S_AXI_HP3_awsize[2:0],S_AXI_HP3_awvalid,S_AXI_HP3_bid[5:0],S_AXI_HP3_bready,S_AXI_HP3_bresp[1:0],S_AXI_HP3_bvalid,S_AXI_HP3_rdata[63:0],S_AXI_HP3_rid[5:0],S_AXI_HP3_rlast,S_AXI_HP3_rready,S_AXI_HP3_rresp[1:0],S_AXI_HP3_rvalid,S_AXI_HP3_wdata[63:0],S_AXI_HP3_wid[5:0],S_AXI_HP3_wlast,S_AXI_HP3_wready,S_AXI_HP3_wstrb[7:0],S_AXI_HP3_wvalid,USBIND_port_indctl[1:0],USBIND_vbus_pwrfault,USBIND_vbus_pwrselect" */;
  inout DDR_VRN;
  inout DDR_VRP;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  output FCLK_CLK0;
  output FCLK_CLK1;
  output FCLK_CLK2;
  output FCLK_CLK3;
  output FCLK_RESET0_N;
  output FCLK_RESET1_N;
  output FCLK_RESET2_N;
  output FCLK_RESET3_N;
  input [63:0]GPIO_I;
  output [63:0]GPIO_O;
  output [63:0]GPIO_T;
  inout [53:0]MIO;
  input M_AXI_GP0_ACLK;
  output [31:0]M_AXI_GP0_araddr;
  output [1:0]M_AXI_GP0_arburst;
  output [3:0]M_AXI_GP0_arcache;
  output [11:0]M_AXI_GP0_arid;
  output [3:0]M_AXI_GP0_arlen;
  output [1:0]M_AXI_GP0_arlock;
  output [2:0]M_AXI_GP0_arprot;
  output [3:0]M_AXI_GP0_arqos;
  input M_AXI_GP0_arready;
  output [2:0]M_AXI_GP0_arsize;
  output M_AXI_GP0_arvalid;
  output [31:0]M_AXI_GP0_awaddr;
  output [1:0]M_AXI_GP0_awburst;
  output [3:0]M_AXI_GP0_awcache;
  output [11:0]M_AXI_GP0_awid;
  output [3:0]M_AXI_GP0_awlen;
  output [1:0]M_AXI_GP0_awlock;
  output [2:0]M_AXI_GP0_awprot;
  output [3:0]M_AXI_GP0_awqos;
  input M_AXI_GP0_awready;
  output [2:0]M_AXI_GP0_awsize;
  output M_AXI_GP0_awvalid;
  input [11:0]M_AXI_GP0_bid;
  output M_AXI_GP0_bready;
  input [1:0]M_AXI_GP0_bresp;
  input M_AXI_GP0_bvalid;
  input [31:0]M_AXI_GP0_rdata;
  input [11:0]M_AXI_GP0_rid;
  input M_AXI_GP0_rlast;
  output M_AXI_GP0_rready;
  input [1:0]M_AXI_GP0_rresp;
  input M_AXI_GP0_rvalid;
  output [31:0]M_AXI_GP0_wdata;
  output [11:0]M_AXI_GP0_wid;
  output M_AXI_GP0_wlast;
  input M_AXI_GP0_wready;
  output [3:0]M_AXI_GP0_wstrb;
  output M_AXI_GP0_wvalid;
  inout PS_CLK;
  inout PS_PORB;
  inout PS_SRSTB;
  input S_AXI_HP0_ACLK;
  input [31:0]S_AXI_HP0_araddr;
  input [1:0]S_AXI_HP0_arburst;
  input [3:0]S_AXI_HP0_arcache;
  input [5:0]S_AXI_HP0_arid;
  input [3:0]S_AXI_HP0_arlen;
  input [1:0]S_AXI_HP0_arlock;
  input [2:0]S_AXI_HP0_arprot;
  input [3:0]S_AXI_HP0_arqos;
  output S_AXI_HP0_arready;
  input [2:0]S_AXI_HP0_arsize;
  input S_AXI_HP0_arvalid;
  input [31:0]S_AXI_HP0_awaddr;
  input [1:0]S_AXI_HP0_awburst;
  input [3:0]S_AXI_HP0_awcache;
  input [5:0]S_AXI_HP0_awid;
  input [3:0]S_AXI_HP0_awlen;
  input [1:0]S_AXI_HP0_awlock;
  input [2:0]S_AXI_HP0_awprot;
  input [3:0]S_AXI_HP0_awqos;
  output S_AXI_HP0_awready;
  input [2:0]S_AXI_HP0_awsize;
  input S_AXI_HP0_awvalid;
  output [5:0]S_AXI_HP0_bid;
  input S_AXI_HP0_bready;
  output [1:0]S_AXI_HP0_bresp;
  output S_AXI_HP0_bvalid;
  output [63:0]S_AXI_HP0_rdata;
  output [5:0]S_AXI_HP0_rid;
  output S_AXI_HP0_rlast;
  input S_AXI_HP0_rready;
  output [1:0]S_AXI_HP0_rresp;
  output S_AXI_HP0_rvalid;
  input [63:0]S_AXI_HP0_wdata;
  input [5:0]S_AXI_HP0_wid;
  input S_AXI_HP0_wlast;
  output S_AXI_HP0_wready;
  input [7:0]S_AXI_HP0_wstrb;
  input S_AXI_HP0_wvalid;
  input S_AXI_HP1_ACLK;
  input [31:0]S_AXI_HP1_araddr;
  input [1:0]S_AXI_HP1_arburst;
  input [3:0]S_AXI_HP1_arcache;
  input [5:0]S_AXI_HP1_arid;
  input [3:0]S_AXI_HP1_arlen;
  input [1:0]S_AXI_HP1_arlock;
  input [2:0]S_AXI_HP1_arprot;
  input [3:0]S_AXI_HP1_arqos;
  output S_AXI_HP1_arready;
  input [2:0]S_AXI_HP1_arsize;
  input S_AXI_HP1_arvalid;
  input [31:0]S_AXI_HP1_awaddr;
  input [1:0]S_AXI_HP1_awburst;
  input [3:0]S_AXI_HP1_awcache;
  input [5:0]S_AXI_HP1_awid;
  input [3:0]S_AXI_HP1_awlen;
  input [1:0]S_AXI_HP1_awlock;
  input [2:0]S_AXI_HP1_awprot;
  input [3:0]S_AXI_HP1_awqos;
  output S_AXI_HP1_awready;
  input [2:0]S_AXI_HP1_awsize;
  input S_AXI_HP1_awvalid;
  output [5:0]S_AXI_HP1_bid;
  input S_AXI_HP1_bready;
  output [1:0]S_AXI_HP1_bresp;
  output S_AXI_HP1_bvalid;
  output [63:0]S_AXI_HP1_rdata;
  output [5:0]S_AXI_HP1_rid;
  output S_AXI_HP1_rlast;
  input S_AXI_HP1_rready;
  output [1:0]S_AXI_HP1_rresp;
  output S_AXI_HP1_rvalid;
  input [63:0]S_AXI_HP1_wdata;
  input [5:0]S_AXI_HP1_wid;
  input S_AXI_HP1_wlast;
  output S_AXI_HP1_wready;
  input [7:0]S_AXI_HP1_wstrb;
  input S_AXI_HP1_wvalid;
  input S_AXI_HP2_ACLK;
  input [31:0]S_AXI_HP2_araddr;
  input [1:0]S_AXI_HP2_arburst;
  input [3:0]S_AXI_HP2_arcache;
  input [5:0]S_AXI_HP2_arid;
  input [3:0]S_AXI_HP2_arlen;
  input [1:0]S_AXI_HP2_arlock;
  input [2:0]S_AXI_HP2_arprot;
  input [3:0]S_AXI_HP2_arqos;
  output S_AXI_HP2_arready;
  input [2:0]S_AXI_HP2_arsize;
  input S_AXI_HP2_arvalid;
  input [31:0]S_AXI_HP2_awaddr;
  input [1:0]S_AXI_HP2_awburst;
  input [3:0]S_AXI_HP2_awcache;
  input [5:0]S_AXI_HP2_awid;
  input [3:0]S_AXI_HP2_awlen;
  input [1:0]S_AXI_HP2_awlock;
  input [2:0]S_AXI_HP2_awprot;
  input [3:0]S_AXI_HP2_awqos;
  output S_AXI_HP2_awready;
  input [2:0]S_AXI_HP2_awsize;
  input S_AXI_HP2_awvalid;
  output [5:0]S_AXI_HP2_bid;
  input S_AXI_HP2_bready;
  output [1:0]S_AXI_HP2_bresp;
  output S_AXI_HP2_bvalid;
  output [63:0]S_AXI_HP2_rdata;
  output [5:0]S_AXI_HP2_rid;
  output S_AXI_HP2_rlast;
  input S_AXI_HP2_rready;
  output [1:0]S_AXI_HP2_rresp;
  output S_AXI_HP2_rvalid;
  input [63:0]S_AXI_HP2_wdata;
  input [5:0]S_AXI_HP2_wid;
  input S_AXI_HP2_wlast;
  output S_AXI_HP2_wready;
  input [7:0]S_AXI_HP2_wstrb;
  input S_AXI_HP2_wvalid;
  input S_AXI_HP3_ACLK;
  input [31:0]S_AXI_HP3_araddr;
  input [1:0]S_AXI_HP3_arburst;
  input [3:0]S_AXI_HP3_arcache;
  input [5:0]S_AXI_HP3_arid;
  input [3:0]S_AXI_HP3_arlen;
  input [1:0]S_AXI_HP3_arlock;
  input [2:0]S_AXI_HP3_arprot;
  input [3:0]S_AXI_HP3_arqos;
  output S_AXI_HP3_arready;
  input [2:0]S_AXI_HP3_arsize;
  input S_AXI_HP3_arvalid;
  input [31:0]S_AXI_HP3_awaddr;
  input [1:0]S_AXI_HP3_awburst;
  input [3:0]S_AXI_HP3_awcache;
  input [5:0]S_AXI_HP3_awid;
  input [3:0]S_AXI_HP3_awlen;
  input [1:0]S_AXI_HP3_awlock;
  input [2:0]S_AXI_HP3_awprot;
  input [3:0]S_AXI_HP3_awqos;
  output S_AXI_HP3_awready;
  input [2:0]S_AXI_HP3_awsize;
  input S_AXI_HP3_awvalid;
  output [5:0]S_AXI_HP3_bid;
  input S_AXI_HP3_bready;
  output [1:0]S_AXI_HP3_bresp;
  output S_AXI_HP3_bvalid;
  output [63:0]S_AXI_HP3_rdata;
  output [5:0]S_AXI_HP3_rid;
  output S_AXI_HP3_rlast;
  input S_AXI_HP3_rready;
  output [1:0]S_AXI_HP3_rresp;
  output S_AXI_HP3_rvalid;
  input [63:0]S_AXI_HP3_wdata;
  input [5:0]S_AXI_HP3_wid;
  input S_AXI_HP3_wlast;
  output S_AXI_HP3_wready;
  input [7:0]S_AXI_HP3_wstrb;
  input S_AXI_HP3_wvalid;
  output [1:0]USBIND_port_indctl;
  input USBIND_vbus_pwrfault;
  output USBIND_vbus_pwrselect;
endmodule
