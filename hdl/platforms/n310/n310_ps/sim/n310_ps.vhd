--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Tue Sep 27 10:36:04 2022
--Host        : localhost.localdomain running 64-bit CentOS Linux release 7.9.2009 (Core)
--Command     : generate_target n310_ps.bd
--Design      : n310_ps
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity n310_ps is
  port (
    DDR_VRN : inout STD_LOGIC;
    DDR_VRP : inout STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FCLK_CLK0 : out STD_LOGIC;
    FCLK_CLK1 : out STD_LOGIC;
    FCLK_CLK2 : out STD_LOGIC;
    FCLK_CLK3 : out STD_LOGIC;
    FCLK_RESET0_N : out STD_LOGIC;
    FCLK_RESET1_N : out STD_LOGIC;
    FCLK_RESET2_N : out STD_LOGIC;
    FCLK_RESET3_N : out STD_LOGIC;
    GPIO_I : in STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_O : out STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_T : out STD_LOGIC_VECTOR ( 63 downto 0 );
    MIO : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    M_AXI_GP0_ACLK : in STD_LOGIC;
    M_AXI_GP0_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_arready : in STD_LOGIC;
    M_AXI_GP0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_arvalid : out STD_LOGIC;
    M_AXI_GP0_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_awready : in STD_LOGIC;
    M_AXI_GP0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_awvalid : out STD_LOGIC;
    M_AXI_GP0_bid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_bready : out STD_LOGIC;
    M_AXI_GP0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_bvalid : in STD_LOGIC;
    M_AXI_GP0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_rid : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_rlast : in STD_LOGIC;
    M_AXI_GP0_rready : out STD_LOGIC;
    M_AXI_GP0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_rvalid : in STD_LOGIC;
    M_AXI_GP0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_wid : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_wlast : out STD_LOGIC;
    M_AXI_GP0_wready : in STD_LOGIC;
    M_AXI_GP0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_wvalid : out STD_LOGIC;
    PS_CLK : inout STD_LOGIC;
    PS_PORB : inout STD_LOGIC;
    PS_SRSTB : inout STD_LOGIC;
    S_AXI_HP0_ACLK : in STD_LOGIC;
    S_AXI_HP0_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_arready : out STD_LOGIC;
    S_AXI_HP0_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_arvalid : in STD_LOGIC;
    S_AXI_HP0_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_awready : out STD_LOGIC;
    S_AXI_HP0_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_awvalid : in STD_LOGIC;
    S_AXI_HP0_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_bready : in STD_LOGIC;
    S_AXI_HP0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_bvalid : out STD_LOGIC;
    S_AXI_HP0_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_rlast : out STD_LOGIC;
    S_AXI_HP0_rready : in STD_LOGIC;
    S_AXI_HP0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_rvalid : out STD_LOGIC;
    S_AXI_HP0_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_wlast : in STD_LOGIC;
    S_AXI_HP0_wready : out STD_LOGIC;
    S_AXI_HP0_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP0_wvalid : in STD_LOGIC;
    S_AXI_HP1_ACLK : in STD_LOGIC;
    S_AXI_HP1_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_arready : out STD_LOGIC;
    S_AXI_HP1_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_arvalid : in STD_LOGIC;
    S_AXI_HP1_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_awready : out STD_LOGIC;
    S_AXI_HP1_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_awvalid : in STD_LOGIC;
    S_AXI_HP1_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_bready : in STD_LOGIC;
    S_AXI_HP1_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_bvalid : out STD_LOGIC;
    S_AXI_HP1_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_rlast : out STD_LOGIC;
    S_AXI_HP1_rready : in STD_LOGIC;
    S_AXI_HP1_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_rvalid : out STD_LOGIC;
    S_AXI_HP1_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_wlast : in STD_LOGIC;
    S_AXI_HP1_wready : out STD_LOGIC;
    S_AXI_HP1_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP1_wvalid : in STD_LOGIC;
    S_AXI_HP2_ACLK : in STD_LOGIC;
    S_AXI_HP2_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_arready : out STD_LOGIC;
    S_AXI_HP2_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_arvalid : in STD_LOGIC;
    S_AXI_HP2_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_awready : out STD_LOGIC;
    S_AXI_HP2_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_awvalid : in STD_LOGIC;
    S_AXI_HP2_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_bready : in STD_LOGIC;
    S_AXI_HP2_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_bvalid : out STD_LOGIC;
    S_AXI_HP2_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_rlast : out STD_LOGIC;
    S_AXI_HP2_rready : in STD_LOGIC;
    S_AXI_HP2_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_rvalid : out STD_LOGIC;
    S_AXI_HP2_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_wlast : in STD_LOGIC;
    S_AXI_HP2_wready : out STD_LOGIC;
    S_AXI_HP2_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP2_wvalid : in STD_LOGIC;
    S_AXI_HP3_ACLK : in STD_LOGIC;
    S_AXI_HP3_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_arready : out STD_LOGIC;
    S_AXI_HP3_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_arvalid : in STD_LOGIC;
    S_AXI_HP3_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_awready : out STD_LOGIC;
    S_AXI_HP3_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_awvalid : in STD_LOGIC;
    S_AXI_HP3_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_bready : in STD_LOGIC;
    S_AXI_HP3_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_bvalid : out STD_LOGIC;
    S_AXI_HP3_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_rlast : out STD_LOGIC;
    S_AXI_HP3_rready : in STD_LOGIC;
    S_AXI_HP3_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_rvalid : out STD_LOGIC;
    S_AXI_HP3_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_wlast : in STD_LOGIC;
    S_AXI_HP3_wready : out STD_LOGIC;
    S_AXI_HP3_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP3_wvalid : in STD_LOGIC;
    USBIND_port_indctl : out STD_LOGIC_VECTOR ( 1 downto 0 );
    USBIND_vbus_pwrfault : in STD_LOGIC;
    USBIND_vbus_pwrselect : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of n310_ps : entity is "n310_ps,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=n310_ps,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=1,numReposBlks=1,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_clkrst_cnt=12,da_ps7_cnt=1,synth_mode=OOC_per_BD}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of n310_ps : entity is "n310_ps.hwdef";
end n310_ps;

architecture STRUCTURE of n310_ps is
  component n310_ps_processing_system7_0_0 is
  port (
    ENET0_MDIO_MDC : out STD_LOGIC;
    ENET0_MDIO_O : out STD_LOGIC;
    ENET0_MDIO_T : out STD_LOGIC;
    ENET0_MDIO_I : in STD_LOGIC;
    GPIO_I : in STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_O : out STD_LOGIC_VECTOR ( 63 downto 0 );
    GPIO_T : out STD_LOGIC_VECTOR ( 63 downto 0 );
    USB0_PORT_INDCTL : out STD_LOGIC_VECTOR ( 1 downto 0 );
    USB0_VBUS_PWRSELECT : out STD_LOGIC;
    USB0_VBUS_PWRFAULT : in STD_LOGIC;
    M_AXI_GP0_ARVALID : out STD_LOGIC;
    M_AXI_GP0_AWVALID : out STD_LOGIC;
    M_AXI_GP0_BREADY : out STD_LOGIC;
    M_AXI_GP0_RREADY : out STD_LOGIC;
    M_AXI_GP0_WLAST : out STD_LOGIC;
    M_AXI_GP0_WVALID : out STD_LOGIC;
    M_AXI_GP0_ARID : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_AWID : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_WID : out STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_ARBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_ARLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_ARSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_AWBURST : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_AWLOCK : out STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_AWSIZE : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_ARPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_AWPROT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    M_AXI_GP0_ARADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_AWADDR : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_WDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    M_AXI_GP0_ARCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_ARLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_ARQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_AWCACHE : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_AWLEN : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_AWQOS : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_WSTRB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    M_AXI_GP0_ACLK : in STD_LOGIC;
    M_AXI_GP0_ARREADY : in STD_LOGIC;
    M_AXI_GP0_AWREADY : in STD_LOGIC;
    M_AXI_GP0_BVALID : in STD_LOGIC;
    M_AXI_GP0_RLAST : in STD_LOGIC;
    M_AXI_GP0_RVALID : in STD_LOGIC;
    M_AXI_GP0_WREADY : in STD_LOGIC;
    M_AXI_GP0_BID : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_RID : in STD_LOGIC_VECTOR ( 11 downto 0 );
    M_AXI_GP0_BRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_RRESP : in STD_LOGIC_VECTOR ( 1 downto 0 );
    M_AXI_GP0_RDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_ARREADY : out STD_LOGIC;
    S_AXI_HP0_AWREADY : out STD_LOGIC;
    S_AXI_HP0_BVALID : out STD_LOGIC;
    S_AXI_HP0_RLAST : out STD_LOGIC;
    S_AXI_HP0_RVALID : out STD_LOGIC;
    S_AXI_HP0_WREADY : out STD_LOGIC;
    S_AXI_HP0_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_BID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_RID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_RDATA : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_RCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP0_WCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP0_RACOUNT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_WACOUNT : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_ACLK : in STD_LOGIC;
    S_AXI_HP0_ARVALID : in STD_LOGIC;
    S_AXI_HP0_AWVALID : in STD_LOGIC;
    S_AXI_HP0_BREADY : in STD_LOGIC;
    S_AXI_HP0_RDISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP0_RREADY : in STD_LOGIC;
    S_AXI_HP0_WLAST : in STD_LOGIC;
    S_AXI_HP0_WRISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP0_WVALID : in STD_LOGIC;
    S_AXI_HP0_ARBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_ARLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_ARSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_AWBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_AWLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP0_AWSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_ARPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_AWPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP0_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP0_ARCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_ARLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_ARQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_AWCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_AWLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_AWQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP0_ARID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_AWID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_WID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP0_WDATA : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP0_WSTRB : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP1_ARREADY : out STD_LOGIC;
    S_AXI_HP1_AWREADY : out STD_LOGIC;
    S_AXI_HP1_BVALID : out STD_LOGIC;
    S_AXI_HP1_RLAST : out STD_LOGIC;
    S_AXI_HP1_RVALID : out STD_LOGIC;
    S_AXI_HP1_WREADY : out STD_LOGIC;
    S_AXI_HP1_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_BID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_RID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_RDATA : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_RCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP1_WCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP1_RACOUNT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_WACOUNT : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_ACLK : in STD_LOGIC;
    S_AXI_HP1_ARVALID : in STD_LOGIC;
    S_AXI_HP1_AWVALID : in STD_LOGIC;
    S_AXI_HP1_BREADY : in STD_LOGIC;
    S_AXI_HP1_RDISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP1_RREADY : in STD_LOGIC;
    S_AXI_HP1_WLAST : in STD_LOGIC;
    S_AXI_HP1_WRISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP1_WVALID : in STD_LOGIC;
    S_AXI_HP1_ARBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_ARLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_ARSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_AWBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_AWLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP1_AWSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_ARPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_AWPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP1_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP1_ARCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_ARLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_ARQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_AWCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_AWLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_AWQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP1_ARID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_AWID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_WID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP1_WDATA : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP1_WSTRB : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP2_ARREADY : out STD_LOGIC;
    S_AXI_HP2_AWREADY : out STD_LOGIC;
    S_AXI_HP2_BVALID : out STD_LOGIC;
    S_AXI_HP2_RLAST : out STD_LOGIC;
    S_AXI_HP2_RVALID : out STD_LOGIC;
    S_AXI_HP2_WREADY : out STD_LOGIC;
    S_AXI_HP2_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_BID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_RID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_RDATA : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_RCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP2_WCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP2_RACOUNT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_WACOUNT : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_ACLK : in STD_LOGIC;
    S_AXI_HP2_ARVALID : in STD_LOGIC;
    S_AXI_HP2_AWVALID : in STD_LOGIC;
    S_AXI_HP2_BREADY : in STD_LOGIC;
    S_AXI_HP2_RDISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP2_RREADY : in STD_LOGIC;
    S_AXI_HP2_WLAST : in STD_LOGIC;
    S_AXI_HP2_WRISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP2_WVALID : in STD_LOGIC;
    S_AXI_HP2_ARBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_ARLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_ARSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_AWBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_AWLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP2_AWSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_ARPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_AWPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP2_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP2_ARCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_ARLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_ARQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_AWCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_AWLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_AWQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP2_ARID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_AWID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_WID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP2_WDATA : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP2_WSTRB : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP3_ARREADY : out STD_LOGIC;
    S_AXI_HP3_AWREADY : out STD_LOGIC;
    S_AXI_HP3_BVALID : out STD_LOGIC;
    S_AXI_HP3_RLAST : out STD_LOGIC;
    S_AXI_HP3_RVALID : out STD_LOGIC;
    S_AXI_HP3_WREADY : out STD_LOGIC;
    S_AXI_HP3_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_BID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_RID : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_RDATA : out STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_RCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP3_WCOUNT : out STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXI_HP3_RACOUNT : out STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_WACOUNT : out STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_ACLK : in STD_LOGIC;
    S_AXI_HP3_ARVALID : in STD_LOGIC;
    S_AXI_HP3_AWVALID : in STD_LOGIC;
    S_AXI_HP3_BREADY : in STD_LOGIC;
    S_AXI_HP3_RDISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP3_RREADY : in STD_LOGIC;
    S_AXI_HP3_WLAST : in STD_LOGIC;
    S_AXI_HP3_WRISSUECAP1_EN : in STD_LOGIC;
    S_AXI_HP3_WVALID : in STD_LOGIC;
    S_AXI_HP3_ARBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_ARLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_ARSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_AWBURST : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_AWLOCK : in STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_HP3_AWSIZE : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_ARPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_AWPROT : in STD_LOGIC_VECTOR ( 2 downto 0 );
    S_AXI_HP3_ARADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_AWADDR : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_HP3_ARCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_ARLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_ARQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_AWCACHE : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_AWLEN : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_AWQOS : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_HP3_ARID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_AWID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_WID : in STD_LOGIC_VECTOR ( 5 downto 0 );
    S_AXI_HP3_WDATA : in STD_LOGIC_VECTOR ( 63 downto 0 );
    S_AXI_HP3_WSTRB : in STD_LOGIC_VECTOR ( 7 downto 0 );
    FCLK_CLK0 : out STD_LOGIC;
    FCLK_CLK1 : out STD_LOGIC;
    FCLK_CLK2 : out STD_LOGIC;
    FCLK_CLK3 : out STD_LOGIC;
    FCLK_RESET0_N : out STD_LOGIC;
    FCLK_RESET1_N : out STD_LOGIC;
    FCLK_RESET2_N : out STD_LOGIC;
    FCLK_RESET3_N : out STD_LOGIC;
    MIO : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    DDR_CAS_n : inout STD_LOGIC;
    DDR_CKE : inout STD_LOGIC;
    DDR_Clk_n : inout STD_LOGIC;
    DDR_Clk : inout STD_LOGIC;
    DDR_CS_n : inout STD_LOGIC;
    DDR_DRSTB : inout STD_LOGIC;
    DDR_ODT : inout STD_LOGIC;
    DDR_RAS_n : inout STD_LOGIC;
    DDR_WEB : inout STD_LOGIC;
    DDR_BankAddr : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_Addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_VRN : inout STD_LOGIC;
    DDR_VRP : inout STD_LOGIC;
    DDR_DM : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_DQ : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_DQS_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_DQS : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    PS_SRSTB : inout STD_LOGIC;
    PS_CLK : inout STD_LOGIC;
    PS_PORB : inout STD_LOGIC
  );
  end component n310_ps_processing_system7_0_0;
  signal AXI_GP0_ACLK_1 : STD_LOGIC;
  signal GPIO_I_0_1 : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal Net : STD_LOGIC_VECTOR ( 53 downto 0 );
  signal Net1 : STD_LOGIC;
  signal Net2 : STD_LOGIC;
  signal Net3 : STD_LOGIC;
  signal Net4 : STD_LOGIC;
  signal Net5 : STD_LOGIC;
  signal S_AXI_HP0_0_1_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP0_0_1_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP0_0_1_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP0_0_1_ARID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP0_0_1_ARLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP0_0_1_ARLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP0_0_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP0_0_1_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP0_0_1_ARREADY : STD_LOGIC;
  signal S_AXI_HP0_0_1_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP0_0_1_ARVALID : STD_LOGIC;
  signal S_AXI_HP0_0_1_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP0_0_1_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP0_0_1_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP0_0_1_AWID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP0_0_1_AWLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP0_0_1_AWLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP0_0_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP0_0_1_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP0_0_1_AWREADY : STD_LOGIC;
  signal S_AXI_HP0_0_1_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP0_0_1_AWVALID : STD_LOGIC;
  signal S_AXI_HP0_0_1_BID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP0_0_1_BREADY : STD_LOGIC;
  signal S_AXI_HP0_0_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP0_0_1_BVALID : STD_LOGIC;
  signal S_AXI_HP0_0_1_RDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP0_0_1_RID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP0_0_1_RLAST : STD_LOGIC;
  signal S_AXI_HP0_0_1_RREADY : STD_LOGIC;
  signal S_AXI_HP0_0_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP0_0_1_RVALID : STD_LOGIC;
  signal S_AXI_HP0_0_1_WDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP0_0_1_WID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP0_0_1_WLAST : STD_LOGIC;
  signal S_AXI_HP0_0_1_WREADY : STD_LOGIC;
  signal S_AXI_HP0_0_1_WSTRB : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_HP0_0_1_WVALID : STD_LOGIC;
  signal S_AXI_HP0_ACLK_0_1 : STD_LOGIC;
  signal S_AXI_HP1_0_1_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP1_0_1_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP1_0_1_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP1_0_1_ARID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP1_0_1_ARLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP1_0_1_ARLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP1_0_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP1_0_1_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP1_0_1_ARREADY : STD_LOGIC;
  signal S_AXI_HP1_0_1_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP1_0_1_ARVALID : STD_LOGIC;
  signal S_AXI_HP1_0_1_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP1_0_1_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP1_0_1_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP1_0_1_AWID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP1_0_1_AWLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP1_0_1_AWLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP1_0_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP1_0_1_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP1_0_1_AWREADY : STD_LOGIC;
  signal S_AXI_HP1_0_1_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP1_0_1_AWVALID : STD_LOGIC;
  signal S_AXI_HP1_0_1_BID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP1_0_1_BREADY : STD_LOGIC;
  signal S_AXI_HP1_0_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP1_0_1_BVALID : STD_LOGIC;
  signal S_AXI_HP1_0_1_RDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP1_0_1_RID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP1_0_1_RLAST : STD_LOGIC;
  signal S_AXI_HP1_0_1_RREADY : STD_LOGIC;
  signal S_AXI_HP1_0_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP1_0_1_RVALID : STD_LOGIC;
  signal S_AXI_HP1_0_1_WDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP1_0_1_WID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP1_0_1_WLAST : STD_LOGIC;
  signal S_AXI_HP1_0_1_WREADY : STD_LOGIC;
  signal S_AXI_HP1_0_1_WSTRB : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_HP1_0_1_WVALID : STD_LOGIC;
  signal S_AXI_HP1_ACLK_0_1 : STD_LOGIC;
  signal S_AXI_HP2_0_1_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP2_0_1_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP2_0_1_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP2_0_1_ARID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP2_0_1_ARLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP2_0_1_ARLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP2_0_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP2_0_1_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP2_0_1_ARREADY : STD_LOGIC;
  signal S_AXI_HP2_0_1_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP2_0_1_ARVALID : STD_LOGIC;
  signal S_AXI_HP2_0_1_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP2_0_1_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP2_0_1_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP2_0_1_AWID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP2_0_1_AWLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP2_0_1_AWLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP2_0_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP2_0_1_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP2_0_1_AWREADY : STD_LOGIC;
  signal S_AXI_HP2_0_1_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP2_0_1_AWVALID : STD_LOGIC;
  signal S_AXI_HP2_0_1_BID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP2_0_1_BREADY : STD_LOGIC;
  signal S_AXI_HP2_0_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP2_0_1_BVALID : STD_LOGIC;
  signal S_AXI_HP2_0_1_RDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP2_0_1_RID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP2_0_1_RLAST : STD_LOGIC;
  signal S_AXI_HP2_0_1_RREADY : STD_LOGIC;
  signal S_AXI_HP2_0_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP2_0_1_RVALID : STD_LOGIC;
  signal S_AXI_HP2_0_1_WDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP2_0_1_WID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP2_0_1_WLAST : STD_LOGIC;
  signal S_AXI_HP2_0_1_WREADY : STD_LOGIC;
  signal S_AXI_HP2_0_1_WSTRB : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_HP2_0_1_WVALID : STD_LOGIC;
  signal S_AXI_HP2_ACLK_0_1 : STD_LOGIC;
  signal S_AXI_HP3_0_1_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP3_0_1_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP3_0_1_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP3_0_1_ARID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP3_0_1_ARLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP3_0_1_ARLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP3_0_1_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP3_0_1_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP3_0_1_ARREADY : STD_LOGIC;
  signal S_AXI_HP3_0_1_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP3_0_1_ARVALID : STD_LOGIC;
  signal S_AXI_HP3_0_1_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal S_AXI_HP3_0_1_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP3_0_1_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP3_0_1_AWID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP3_0_1_AWLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP3_0_1_AWLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP3_0_1_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP3_0_1_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal S_AXI_HP3_0_1_AWREADY : STD_LOGIC;
  signal S_AXI_HP3_0_1_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal S_AXI_HP3_0_1_AWVALID : STD_LOGIC;
  signal S_AXI_HP3_0_1_BID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP3_0_1_BREADY : STD_LOGIC;
  signal S_AXI_HP3_0_1_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP3_0_1_BVALID : STD_LOGIC;
  signal S_AXI_HP3_0_1_RDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP3_0_1_RID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP3_0_1_RLAST : STD_LOGIC;
  signal S_AXI_HP3_0_1_RREADY : STD_LOGIC;
  signal S_AXI_HP3_0_1_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_HP3_0_1_RVALID : STD_LOGIC;
  signal S_AXI_HP3_0_1_WDATA : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal S_AXI_HP3_0_1_WID : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_HP3_0_1_WLAST : STD_LOGIC;
  signal S_AXI_HP3_0_1_WREADY : STD_LOGIC;
  signal S_AXI_HP3_0_1_WSTRB : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_HP3_0_1_WVALID : STD_LOGIC;
  signal S_AXI_HP3_ACLK_0_1 : STD_LOGIC;
  signal processing_system7_0_DDR_ADDR : STD_LOGIC_VECTOR ( 14 downto 0 );
  signal processing_system7_0_DDR_BA : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal processing_system7_0_DDR_CAS_N : STD_LOGIC;
  signal processing_system7_0_DDR_CKE : STD_LOGIC;
  signal processing_system7_0_DDR_CK_N : STD_LOGIC;
  signal processing_system7_0_DDR_CK_P : STD_LOGIC;
  signal processing_system7_0_DDR_CS_N : STD_LOGIC;
  signal processing_system7_0_DDR_DM : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_DDR_DQ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal processing_system7_0_DDR_DQS_N : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_DDR_DQS_P : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_DDR_ODT : STD_LOGIC;
  signal processing_system7_0_DDR_RAS_N : STD_LOGIC;
  signal processing_system7_0_DDR_RESET_N : STD_LOGIC;
  signal processing_system7_0_DDR_WE_N : STD_LOGIC;
  signal processing_system7_0_FCLK_CLK0 : STD_LOGIC;
  signal processing_system7_0_FCLK_CLK1 : STD_LOGIC;
  signal processing_system7_0_FCLK_CLK2 : STD_LOGIC;
  signal processing_system7_0_FCLK_CLK3 : STD_LOGIC;
  signal processing_system7_0_FCLK_RESET0_N : STD_LOGIC;
  signal processing_system7_0_FCLK_RESET1_N : STD_LOGIC;
  signal processing_system7_0_FCLK_RESET2_N : STD_LOGIC;
  signal processing_system7_0_FCLK_RESET3_N : STD_LOGIC;
  signal processing_system7_0_GPIO_O : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal processing_system7_0_GPIO_T : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARID : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARREADY : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_ARSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal processing_system7_0_M_AXI_GP0_ARVALID : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_AWADDR : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWBURST : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWCACHE : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWID : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWLEN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWLOCK : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWPROT : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWQOS : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWREADY : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_AWSIZE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal processing_system7_0_M_AXI_GP0_AWVALID : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_BID : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal processing_system7_0_M_AXI_GP0_BREADY : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_BRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal processing_system7_0_M_AXI_GP0_BVALID : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_RDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal processing_system7_0_M_AXI_GP0_RID : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal processing_system7_0_M_AXI_GP0_RLAST : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_RREADY : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_RRESP : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal processing_system7_0_M_AXI_GP0_RVALID : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_WDATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal processing_system7_0_M_AXI_GP0_WID : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal processing_system7_0_M_AXI_GP0_WLAST : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_WREADY : STD_LOGIC;
  signal processing_system7_0_M_AXI_GP0_WSTRB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal processing_system7_0_M_AXI_GP0_WVALID : STD_LOGIC;
  signal processing_system7_0_USBIND_0_PORT_INDCTL : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal processing_system7_0_USBIND_0_VBUS_PWRFAULT : STD_LOGIC;
  signal processing_system7_0_USBIND_0_VBUS_PWRSELECT : STD_LOGIC;
  signal NLW_processing_system7_0_ENET0_MDIO_MDC_UNCONNECTED : STD_LOGIC;
  signal NLW_processing_system7_0_ENET0_MDIO_O_UNCONNECTED : STD_LOGIC;
  signal NLW_processing_system7_0_ENET0_MDIO_T_UNCONNECTED : STD_LOGIC;
  signal NLW_processing_system7_0_S_AXI_HP0_RACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP0_RCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP0_WACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP0_WCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP1_RACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP1_RCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP1_WACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP1_WCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP2_RACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP2_RCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP2_WACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP2_WCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP3_RACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP3_RCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP3_WACOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_processing_system7_0_S_AXI_HP3_WCOUNT_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of DDR_cas_n : signal is "xilinx.com:interface:ddrx:1.0 DDR CAS_N";
  attribute X_INTERFACE_INFO of DDR_ck_n : signal is "xilinx.com:interface:ddrx:1.0 DDR CK_N";
  attribute X_INTERFACE_INFO of DDR_ck_p : signal is "xilinx.com:interface:ddrx:1.0 DDR CK_P";
  attribute X_INTERFACE_INFO of DDR_cke : signal is "xilinx.com:interface:ddrx:1.0 DDR CKE";
  attribute X_INTERFACE_INFO of DDR_cs_n : signal is "xilinx.com:interface:ddrx:1.0 DDR CS_N";
  attribute X_INTERFACE_INFO of DDR_odt : signal is "xilinx.com:interface:ddrx:1.0 DDR ODT";
  attribute X_INTERFACE_INFO of DDR_ras_n : signal is "xilinx.com:interface:ddrx:1.0 DDR RAS_N";
  attribute X_INTERFACE_INFO of DDR_reset_n : signal is "xilinx.com:interface:ddrx:1.0 DDR RESET_N";
  attribute X_INTERFACE_INFO of DDR_we_n : signal is "xilinx.com:interface:ddrx:1.0 DDR WE_N";
  attribute X_INTERFACE_INFO of FCLK_CLK0 : signal is "xilinx.com:signal:clock:1.0 CLK.FCLK_CLK0 CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of FCLK_CLK0 : signal is "XIL_INTERFACENAME CLK.FCLK_CLK0, ASSOCIATED_BUSIF M_AXI_GP0:M_AXI_AP0:M_AXI_AP1:M_AXI_AP2:M_AXI_AP3:M_AXI_AP4:M_AXI_AP5:M_AXI_AP6:M_AXI_AP7:M_AXI_AP8:M_AXI_AP9:M_AXI_AP10:M_AXI_AP11:M_AXI_AP12:M_AXI_AP13:M_AXI_AP14:M_AXI_AP15, ASSOCIATED_RESET S_AXI_ARESETN, CLK_DOMAIN n310_ps_processing_system7_0_0_FCLK_CLK0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of FCLK_CLK1 : signal is "xilinx.com:signal:clock:1.0 CLK.FCLK_CLK1 CLK";
  attribute X_INTERFACE_PARAMETER of FCLK_CLK1 : signal is "XIL_INTERFACENAME CLK.FCLK_CLK1, ASSOCIATED_RESET FCLK_RESET1_N, CLK_DOMAIN n310_ps_processing_system7_0_0_FCLK_CLK1, FREQ_HZ 250000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of FCLK_CLK2 : signal is "xilinx.com:signal:clock:1.0 CLK.FCLK_CLK2 CLK";
  attribute X_INTERFACE_PARAMETER of FCLK_CLK2 : signal is "XIL_INTERFACENAME CLK.FCLK_CLK2, ASSOCIATED_RESET FCLK_RESET2_N, CLK_DOMAIN n310_ps_processing_system7_0_0_FCLK_CLK2, FREQ_HZ 166666672, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of FCLK_CLK3 : signal is "xilinx.com:signal:clock:1.0 CLK.FCLK_CLK3 CLK";
  attribute X_INTERFACE_PARAMETER of FCLK_CLK3 : signal is "XIL_INTERFACENAME CLK.FCLK_CLK3, ASSOCIATED_RESET FCLK_RESET3_N, CLK_DOMAIN n310_ps_processing_system7_0_0_FCLK_CLK3, FREQ_HZ 200000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of FCLK_RESET0_N : signal is "xilinx.com:signal:reset:1.0 RST.FCLK_RESET0_N RST";
  attribute X_INTERFACE_PARAMETER of FCLK_RESET0_N : signal is "XIL_INTERFACENAME RST.FCLK_RESET0_N, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of FCLK_RESET1_N : signal is "xilinx.com:signal:reset:1.0 RST.FCLK_RESET1_N RST";
  attribute X_INTERFACE_PARAMETER of FCLK_RESET1_N : signal is "XIL_INTERFACENAME RST.FCLK_RESET1_N, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of FCLK_RESET2_N : signal is "xilinx.com:signal:reset:1.0 RST.FCLK_RESET2_N RST";
  attribute X_INTERFACE_PARAMETER of FCLK_RESET2_N : signal is "XIL_INTERFACENAME RST.FCLK_RESET2_N, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of FCLK_RESET3_N : signal is "xilinx.com:signal:reset:1.0 RST.FCLK_RESET3_N RST";
  attribute X_INTERFACE_PARAMETER of FCLK_RESET3_N : signal is "XIL_INTERFACENAME RST.FCLK_RESET3_N, INSERT_VIP 0, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arready : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARREADY";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arvalid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARVALID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awready : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWREADY";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awvalid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWVALID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_bready : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 BREADY";
  attribute X_INTERFACE_INFO of M_AXI_GP0_bvalid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 BVALID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_rlast : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 RLAST";
  attribute X_INTERFACE_INFO of M_AXI_GP0_rready : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 RREADY";
  attribute X_INTERFACE_INFO of M_AXI_GP0_rvalid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 RVALID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_wlast : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 WLAST";
  attribute X_INTERFACE_INFO of M_AXI_GP0_wready : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 WREADY";
  attribute X_INTERFACE_INFO of M_AXI_GP0_wvalid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 WVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_ACLK : signal is "xilinx.com:signal:clock:1.0 CLK.S_AXI_HP0_ACLK CLK";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP0_ACLK : signal is "XIL_INTERFACENAME CLK.S_AXI_HP0_ACLK, ASSOCIATED_BUSIF S_AXI_HP0, CLK_DOMAIN n310_ps_S_AXI_HP0_ACLK_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_bready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 BREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP0_bvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 BVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_rlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 RLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP0_rready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 RREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP0_rvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 RVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_wlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 WLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP0_wready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 WREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP0_wvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 WVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_ACLK : signal is "xilinx.com:signal:clock:1.0 CLK.S_AXI_HP1_ACLK CLK";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP1_ACLK : signal is "XIL_INTERFACENAME CLK.S_AXI_HP1_ACLK, ASSOCIATED_BUSIF S_AXI_HP1, CLK_DOMAIN n310_ps_S_AXI_HP1_ACLK_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_bready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 BREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP1_bvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 BVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_rlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 RLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP1_rready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 RREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP1_rvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 RVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_wlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 WLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP1_wready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 WREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP1_wvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 WVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_ACLK : signal is "xilinx.com:signal:clock:1.0 CLK.S_AXI_HP2_ACLK CLK";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP2_ACLK : signal is "XIL_INTERFACENAME CLK.S_AXI_HP2_ACLK, ASSOCIATED_BUSIF S_AXI_HP2, CLK_DOMAIN n310_ps_S_AXI_HP2_ACLK_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_bready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 BREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP2_bvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 BVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_rlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 RLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP2_rready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 RREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP2_rvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 RVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_wlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 WLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP2_wready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 WREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP2_wvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 WVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_ACLK : signal is "xilinx.com:signal:clock:1.0 CLK.S_AXI_HP3_ACLK CLK";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP3_ACLK : signal is "XIL_INTERFACENAME CLK.S_AXI_HP3_ACLK, ASSOCIATED_BUSIF S_AXI_HP3, CLK_DOMAIN n310_ps_S_AXI_HP3_ACLK_0, FREQ_HZ 100000000, INSERT_VIP 0, PHASE 0.000";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_bready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 BREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP3_bvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 BVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_rlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 RLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP3_rready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 RREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP3_rvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 RVALID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_wlast : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 WLAST";
  attribute X_INTERFACE_INFO of S_AXI_HP3_wready : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 WREADY";
  attribute X_INTERFACE_INFO of S_AXI_HP3_wvalid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 WVALID";
  attribute X_INTERFACE_INFO of USBIND_vbus_pwrfault : signal is "xilinx.com:display_processing_system7:usbctrl:1.0 USBIND VBUS_PWRFAULT";
  attribute X_INTERFACE_INFO of USBIND_vbus_pwrselect : signal is "xilinx.com:display_processing_system7:usbctrl:1.0 USBIND VBUS_PWRSELECT";
  attribute X_INTERFACE_INFO of DDR_addr : signal is "xilinx.com:interface:ddrx:1.0 DDR ADDR";
  attribute X_INTERFACE_PARAMETER of DDR_addr : signal is "XIL_INTERFACENAME DDR, AXI_ARBITRATION_SCHEME TDM, BURST_LENGTH 8, CAN_DEBUG false, CAS_LATENCY 11, CAS_WRITE_LATENCY 11, CS_ENABLED true, DATA_MASK_ENABLED true, DATA_WIDTH 8, MEMORY_TYPE COMPONENTS, MEM_ADDR_MAP ROW_COLUMN_BANK, SLOT Single, TIMEPERIOD_PS 1250";
  attribute X_INTERFACE_INFO of DDR_ba : signal is "xilinx.com:interface:ddrx:1.0 DDR BA";
  attribute X_INTERFACE_INFO of DDR_dm : signal is "xilinx.com:interface:ddrx:1.0 DDR DM";
  attribute X_INTERFACE_INFO of DDR_dq : signal is "xilinx.com:interface:ddrx:1.0 DDR DQ";
  attribute X_INTERFACE_INFO of DDR_dqs_n : signal is "xilinx.com:interface:ddrx:1.0 DDR DQS_N";
  attribute X_INTERFACE_INFO of DDR_dqs_p : signal is "xilinx.com:interface:ddrx:1.0 DDR DQS_P";
  attribute X_INTERFACE_INFO of M_AXI_GP0_araddr : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARADDR";
  attribute X_INTERFACE_PARAMETER of M_AXI_GP0_araddr : signal is "XIL_INTERFACENAME M_AXI_GP0, ADDR_WIDTH 32, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN n310_ps_processing_system7_0_0_FCLK_CLK0, DATA_WIDTH 32, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 1, HAS_LOCK 1, HAS_PROT 1, HAS_QOS 1, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 12, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 8, NUM_READ_THREADS 4, NUM_WRITE_OUTSTANDING 8, NUM_WRITE_THREADS 4, PHASE 0.000, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 0, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arburst : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARBURST";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arcache : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARCACHE";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arlen : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARLEN";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arlock : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARLOCK";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arprot : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARPROT";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arqos : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARQOS";
  attribute X_INTERFACE_INFO of M_AXI_GP0_arsize : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 ARSIZE";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awaddr : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWADDR";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awburst : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWBURST";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awcache : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWCACHE";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awlen : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWLEN";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awlock : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWLOCK";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awprot : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWPROT";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awqos : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWQOS";
  attribute X_INTERFACE_INFO of M_AXI_GP0_awsize : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 AWSIZE";
  attribute X_INTERFACE_INFO of M_AXI_GP0_bid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 BID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_bresp : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 BRESP";
  attribute X_INTERFACE_INFO of M_AXI_GP0_rdata : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 RDATA";
  attribute X_INTERFACE_INFO of M_AXI_GP0_rid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 RID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_rresp : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 RRESP";
  attribute X_INTERFACE_INFO of M_AXI_GP0_wdata : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 WDATA";
  attribute X_INTERFACE_INFO of M_AXI_GP0_wid : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 WID";
  attribute X_INTERFACE_INFO of M_AXI_GP0_wstrb : signal is "xilinx.com:interface:aximm:1.0 M_AXI_GP0 WSTRB";
  attribute X_INTERFACE_INFO of S_AXI_HP0_araddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARADDR";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP0_araddr : signal is "XIL_INTERFACENAME S_AXI_HP0, ADDR_WIDTH 32, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN n310_ps_S_AXI_HP0_ACLK_0, DATA_WIDTH 64, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 1, HAS_LOCK 1, HAS_PROT 1, HAS_QOS 1, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 8, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 8, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP0_arsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 ARSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awaddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWADDR";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP0_awsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 AWSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP0_bid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 BID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_bresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 BRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP0_rdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 RDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP0_rid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 RID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_rresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 RRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP0_wdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 WDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP0_wid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 WID";
  attribute X_INTERFACE_INFO of S_AXI_HP0_wstrb : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP0 WSTRB";
  attribute X_INTERFACE_INFO of S_AXI_HP1_araddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARADDR";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP1_araddr : signal is "XIL_INTERFACENAME S_AXI_HP1, ADDR_WIDTH 32, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN n310_ps_S_AXI_HP1_ACLK_0, DATA_WIDTH 64, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 1, HAS_LOCK 1, HAS_PROT 1, HAS_QOS 1, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 8, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 8, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP1_arsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 ARSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awaddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWADDR";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP1_awsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 AWSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP1_bid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 BID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_bresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 BRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP1_rdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 RDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP1_rid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 RID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_rresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 RRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP1_wdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 WDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP1_wid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 WID";
  attribute X_INTERFACE_INFO of S_AXI_HP1_wstrb : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP1 WSTRB";
  attribute X_INTERFACE_INFO of S_AXI_HP2_araddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARADDR";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP2_araddr : signal is "XIL_INTERFACENAME S_AXI_HP2, ADDR_WIDTH 32, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN n310_ps_S_AXI_HP2_ACLK_0, DATA_WIDTH 64, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 1, HAS_LOCK 1, HAS_PROT 1, HAS_QOS 1, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 8, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 8, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP2_arsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 ARSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awaddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWADDR";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP2_awsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 AWSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP2_bid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 BID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_bresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 BRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP2_rdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 RDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP2_rid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 RID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_rresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 RRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP2_wdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 WDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP2_wid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 WID";
  attribute X_INTERFACE_INFO of S_AXI_HP2_wstrb : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP2 WSTRB";
  attribute X_INTERFACE_INFO of S_AXI_HP3_araddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARADDR";
  attribute X_INTERFACE_PARAMETER of S_AXI_HP3_araddr : signal is "XIL_INTERFACENAME S_AXI_HP3, ADDR_WIDTH 32, ARUSER_WIDTH 0, AWUSER_WIDTH 0, BUSER_WIDTH 0, CLK_DOMAIN n310_ps_S_AXI_HP3_ACLK_0, DATA_WIDTH 64, FREQ_HZ 100000000, HAS_BRESP 1, HAS_BURST 1, HAS_CACHE 1, HAS_LOCK 1, HAS_PROT 1, HAS_QOS 1, HAS_REGION 0, HAS_RRESP 1, HAS_WSTRB 1, ID_WIDTH 6, INSERT_VIP 0, MAX_BURST_LENGTH 16, NUM_READ_OUTSTANDING 8, NUM_READ_THREADS 1, NUM_WRITE_OUTSTANDING 8, NUM_WRITE_THREADS 1, PHASE 0.000, PROTOCOL AXI3, READ_WRITE_MODE READ_WRITE, RUSER_BITS_PER_BYTE 0, RUSER_WIDTH 0, SUPPORTS_NARROW_BURST 1, WUSER_BITS_PER_BYTE 0, WUSER_WIDTH 0";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP3_arsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 ARSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awaddr : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWADDR";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awburst : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWBURST";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awcache : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWCACHE";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awlen : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWLEN";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awlock : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWLOCK";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awprot : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWPROT";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awqos : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWQOS";
  attribute X_INTERFACE_INFO of S_AXI_HP3_awsize : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 AWSIZE";
  attribute X_INTERFACE_INFO of S_AXI_HP3_bid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 BID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_bresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 BRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP3_rdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 RDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP3_rid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 RID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_rresp : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 RRESP";
  attribute X_INTERFACE_INFO of S_AXI_HP3_wdata : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 WDATA";
  attribute X_INTERFACE_INFO of S_AXI_HP3_wid : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 WID";
  attribute X_INTERFACE_INFO of S_AXI_HP3_wstrb : signal is "xilinx.com:interface:aximm:1.0 S_AXI_HP3 WSTRB";
  attribute X_INTERFACE_INFO of USBIND_port_indctl : signal is "xilinx.com:display_processing_system7:usbctrl:1.0 USBIND PORT_INDCTL";
begin
  AXI_GP0_ACLK_1 <= M_AXI_GP0_ACLK;
  FCLK_CLK0 <= processing_system7_0_FCLK_CLK0;
  FCLK_CLK1 <= processing_system7_0_FCLK_CLK1;
  FCLK_CLK2 <= processing_system7_0_FCLK_CLK2;
  FCLK_CLK3 <= processing_system7_0_FCLK_CLK3;
  FCLK_RESET0_N <= processing_system7_0_FCLK_RESET0_N;
  FCLK_RESET1_N <= processing_system7_0_FCLK_RESET1_N;
  FCLK_RESET2_N <= processing_system7_0_FCLK_RESET2_N;
  FCLK_RESET3_N <= processing_system7_0_FCLK_RESET3_N;
  GPIO_I_0_1(63 downto 0) <= GPIO_I(63 downto 0);
  GPIO_O(63 downto 0) <= processing_system7_0_GPIO_O(63 downto 0);
  GPIO_T(63 downto 0) <= processing_system7_0_GPIO_T(63 downto 0);
  M_AXI_GP0_araddr(31 downto 0) <= processing_system7_0_M_AXI_GP0_ARADDR(31 downto 0);
  M_AXI_GP0_arburst(1 downto 0) <= processing_system7_0_M_AXI_GP0_ARBURST(1 downto 0);
  M_AXI_GP0_arcache(3 downto 0) <= processing_system7_0_M_AXI_GP0_ARCACHE(3 downto 0);
  M_AXI_GP0_arid(11 downto 0) <= processing_system7_0_M_AXI_GP0_ARID(11 downto 0);
  M_AXI_GP0_arlen(3 downto 0) <= processing_system7_0_M_AXI_GP0_ARLEN(3 downto 0);
  M_AXI_GP0_arlock(1 downto 0) <= processing_system7_0_M_AXI_GP0_ARLOCK(1 downto 0);
  M_AXI_GP0_arprot(2 downto 0) <= processing_system7_0_M_AXI_GP0_ARPROT(2 downto 0);
  M_AXI_GP0_arqos(3 downto 0) <= processing_system7_0_M_AXI_GP0_ARQOS(3 downto 0);
  M_AXI_GP0_arsize(2 downto 0) <= processing_system7_0_M_AXI_GP0_ARSIZE(2 downto 0);
  M_AXI_GP0_arvalid <= processing_system7_0_M_AXI_GP0_ARVALID;
  M_AXI_GP0_awaddr(31 downto 0) <= processing_system7_0_M_AXI_GP0_AWADDR(31 downto 0);
  M_AXI_GP0_awburst(1 downto 0) <= processing_system7_0_M_AXI_GP0_AWBURST(1 downto 0);
  M_AXI_GP0_awcache(3 downto 0) <= processing_system7_0_M_AXI_GP0_AWCACHE(3 downto 0);
  M_AXI_GP0_awid(11 downto 0) <= processing_system7_0_M_AXI_GP0_AWID(11 downto 0);
  M_AXI_GP0_awlen(3 downto 0) <= processing_system7_0_M_AXI_GP0_AWLEN(3 downto 0);
  M_AXI_GP0_awlock(1 downto 0) <= processing_system7_0_M_AXI_GP0_AWLOCK(1 downto 0);
  M_AXI_GP0_awprot(2 downto 0) <= processing_system7_0_M_AXI_GP0_AWPROT(2 downto 0);
  M_AXI_GP0_awqos(3 downto 0) <= processing_system7_0_M_AXI_GP0_AWQOS(3 downto 0);
  M_AXI_GP0_awsize(2 downto 0) <= processing_system7_0_M_AXI_GP0_AWSIZE(2 downto 0);
  M_AXI_GP0_awvalid <= processing_system7_0_M_AXI_GP0_AWVALID;
  M_AXI_GP0_bready <= processing_system7_0_M_AXI_GP0_BREADY;
  M_AXI_GP0_rready <= processing_system7_0_M_AXI_GP0_RREADY;
  M_AXI_GP0_wdata(31 downto 0) <= processing_system7_0_M_AXI_GP0_WDATA(31 downto 0);
  M_AXI_GP0_wid(11 downto 0) <= processing_system7_0_M_AXI_GP0_WID(11 downto 0);
  M_AXI_GP0_wlast <= processing_system7_0_M_AXI_GP0_WLAST;
  M_AXI_GP0_wstrb(3 downto 0) <= processing_system7_0_M_AXI_GP0_WSTRB(3 downto 0);
  M_AXI_GP0_wvalid <= processing_system7_0_M_AXI_GP0_WVALID;
  S_AXI_HP0_0_1_ARADDR(31 downto 0) <= S_AXI_HP0_araddr(31 downto 0);
  S_AXI_HP0_0_1_ARBURST(1 downto 0) <= S_AXI_HP0_arburst(1 downto 0);
  S_AXI_HP0_0_1_ARCACHE(3 downto 0) <= S_AXI_HP0_arcache(3 downto 0);
  S_AXI_HP0_0_1_ARID(5 downto 0) <= S_AXI_HP0_arid(5 downto 0);
  S_AXI_HP0_0_1_ARLEN(3 downto 0) <= S_AXI_HP0_arlen(3 downto 0);
  S_AXI_HP0_0_1_ARLOCK(1 downto 0) <= S_AXI_HP0_arlock(1 downto 0);
  S_AXI_HP0_0_1_ARPROT(2 downto 0) <= S_AXI_HP0_arprot(2 downto 0);
  S_AXI_HP0_0_1_ARQOS(3 downto 0) <= S_AXI_HP0_arqos(3 downto 0);
  S_AXI_HP0_0_1_ARSIZE(2 downto 0) <= S_AXI_HP0_arsize(2 downto 0);
  S_AXI_HP0_0_1_ARVALID <= S_AXI_HP0_arvalid;
  S_AXI_HP0_0_1_AWADDR(31 downto 0) <= S_AXI_HP0_awaddr(31 downto 0);
  S_AXI_HP0_0_1_AWBURST(1 downto 0) <= S_AXI_HP0_awburst(1 downto 0);
  S_AXI_HP0_0_1_AWCACHE(3 downto 0) <= S_AXI_HP0_awcache(3 downto 0);
  S_AXI_HP0_0_1_AWID(5 downto 0) <= S_AXI_HP0_awid(5 downto 0);
  S_AXI_HP0_0_1_AWLEN(3 downto 0) <= S_AXI_HP0_awlen(3 downto 0);
  S_AXI_HP0_0_1_AWLOCK(1 downto 0) <= S_AXI_HP0_awlock(1 downto 0);
  S_AXI_HP0_0_1_AWPROT(2 downto 0) <= S_AXI_HP0_awprot(2 downto 0);
  S_AXI_HP0_0_1_AWQOS(3 downto 0) <= S_AXI_HP0_awqos(3 downto 0);
  S_AXI_HP0_0_1_AWSIZE(2 downto 0) <= S_AXI_HP0_awsize(2 downto 0);
  S_AXI_HP0_0_1_AWVALID <= S_AXI_HP0_awvalid;
  S_AXI_HP0_0_1_BREADY <= S_AXI_HP0_bready;
  S_AXI_HP0_0_1_RREADY <= S_AXI_HP0_rready;
  S_AXI_HP0_0_1_WDATA(63 downto 0) <= S_AXI_HP0_wdata(63 downto 0);
  S_AXI_HP0_0_1_WID(5 downto 0) <= S_AXI_HP0_wid(5 downto 0);
  S_AXI_HP0_0_1_WLAST <= S_AXI_HP0_wlast;
  S_AXI_HP0_0_1_WSTRB(7 downto 0) <= S_AXI_HP0_wstrb(7 downto 0);
  S_AXI_HP0_0_1_WVALID <= S_AXI_HP0_wvalid;
  S_AXI_HP0_ACLK_0_1 <= S_AXI_HP0_ACLK;
  S_AXI_HP0_arready <= S_AXI_HP0_0_1_ARREADY;
  S_AXI_HP0_awready <= S_AXI_HP0_0_1_AWREADY;
  S_AXI_HP0_bid(5 downto 0) <= S_AXI_HP0_0_1_BID(5 downto 0);
  S_AXI_HP0_bresp(1 downto 0) <= S_AXI_HP0_0_1_BRESP(1 downto 0);
  S_AXI_HP0_bvalid <= S_AXI_HP0_0_1_BVALID;
  S_AXI_HP0_rdata(63 downto 0) <= S_AXI_HP0_0_1_RDATA(63 downto 0);
  S_AXI_HP0_rid(5 downto 0) <= S_AXI_HP0_0_1_RID(5 downto 0);
  S_AXI_HP0_rlast <= S_AXI_HP0_0_1_RLAST;
  S_AXI_HP0_rresp(1 downto 0) <= S_AXI_HP0_0_1_RRESP(1 downto 0);
  S_AXI_HP0_rvalid <= S_AXI_HP0_0_1_RVALID;
  S_AXI_HP0_wready <= S_AXI_HP0_0_1_WREADY;
  S_AXI_HP1_0_1_ARADDR(31 downto 0) <= S_AXI_HP1_araddr(31 downto 0);
  S_AXI_HP1_0_1_ARBURST(1 downto 0) <= S_AXI_HP1_arburst(1 downto 0);
  S_AXI_HP1_0_1_ARCACHE(3 downto 0) <= S_AXI_HP1_arcache(3 downto 0);
  S_AXI_HP1_0_1_ARID(5 downto 0) <= S_AXI_HP1_arid(5 downto 0);
  S_AXI_HP1_0_1_ARLEN(3 downto 0) <= S_AXI_HP1_arlen(3 downto 0);
  S_AXI_HP1_0_1_ARLOCK(1 downto 0) <= S_AXI_HP1_arlock(1 downto 0);
  S_AXI_HP1_0_1_ARPROT(2 downto 0) <= S_AXI_HP1_arprot(2 downto 0);
  S_AXI_HP1_0_1_ARQOS(3 downto 0) <= S_AXI_HP1_arqos(3 downto 0);
  S_AXI_HP1_0_1_ARSIZE(2 downto 0) <= S_AXI_HP1_arsize(2 downto 0);
  S_AXI_HP1_0_1_ARVALID <= S_AXI_HP1_arvalid;
  S_AXI_HP1_0_1_AWADDR(31 downto 0) <= S_AXI_HP1_awaddr(31 downto 0);
  S_AXI_HP1_0_1_AWBURST(1 downto 0) <= S_AXI_HP1_awburst(1 downto 0);
  S_AXI_HP1_0_1_AWCACHE(3 downto 0) <= S_AXI_HP1_awcache(3 downto 0);
  S_AXI_HP1_0_1_AWID(5 downto 0) <= S_AXI_HP1_awid(5 downto 0);
  S_AXI_HP1_0_1_AWLEN(3 downto 0) <= S_AXI_HP1_awlen(3 downto 0);
  S_AXI_HP1_0_1_AWLOCK(1 downto 0) <= S_AXI_HP1_awlock(1 downto 0);
  S_AXI_HP1_0_1_AWPROT(2 downto 0) <= S_AXI_HP1_awprot(2 downto 0);
  S_AXI_HP1_0_1_AWQOS(3 downto 0) <= S_AXI_HP1_awqos(3 downto 0);
  S_AXI_HP1_0_1_AWSIZE(2 downto 0) <= S_AXI_HP1_awsize(2 downto 0);
  S_AXI_HP1_0_1_AWVALID <= S_AXI_HP1_awvalid;
  S_AXI_HP1_0_1_BREADY <= S_AXI_HP1_bready;
  S_AXI_HP1_0_1_RREADY <= S_AXI_HP1_rready;
  S_AXI_HP1_0_1_WDATA(63 downto 0) <= S_AXI_HP1_wdata(63 downto 0);
  S_AXI_HP1_0_1_WID(5 downto 0) <= S_AXI_HP1_wid(5 downto 0);
  S_AXI_HP1_0_1_WLAST <= S_AXI_HP1_wlast;
  S_AXI_HP1_0_1_WSTRB(7 downto 0) <= S_AXI_HP1_wstrb(7 downto 0);
  S_AXI_HP1_0_1_WVALID <= S_AXI_HP1_wvalid;
  S_AXI_HP1_ACLK_0_1 <= S_AXI_HP1_ACLK;
  S_AXI_HP1_arready <= S_AXI_HP1_0_1_ARREADY;
  S_AXI_HP1_awready <= S_AXI_HP1_0_1_AWREADY;
  S_AXI_HP1_bid(5 downto 0) <= S_AXI_HP1_0_1_BID(5 downto 0);
  S_AXI_HP1_bresp(1 downto 0) <= S_AXI_HP1_0_1_BRESP(1 downto 0);
  S_AXI_HP1_bvalid <= S_AXI_HP1_0_1_BVALID;
  S_AXI_HP1_rdata(63 downto 0) <= S_AXI_HP1_0_1_RDATA(63 downto 0);
  S_AXI_HP1_rid(5 downto 0) <= S_AXI_HP1_0_1_RID(5 downto 0);
  S_AXI_HP1_rlast <= S_AXI_HP1_0_1_RLAST;
  S_AXI_HP1_rresp(1 downto 0) <= S_AXI_HP1_0_1_RRESP(1 downto 0);
  S_AXI_HP1_rvalid <= S_AXI_HP1_0_1_RVALID;
  S_AXI_HP1_wready <= S_AXI_HP1_0_1_WREADY;
  S_AXI_HP2_0_1_ARADDR(31 downto 0) <= S_AXI_HP2_araddr(31 downto 0);
  S_AXI_HP2_0_1_ARBURST(1 downto 0) <= S_AXI_HP2_arburst(1 downto 0);
  S_AXI_HP2_0_1_ARCACHE(3 downto 0) <= S_AXI_HP2_arcache(3 downto 0);
  S_AXI_HP2_0_1_ARID(5 downto 0) <= S_AXI_HP2_arid(5 downto 0);
  S_AXI_HP2_0_1_ARLEN(3 downto 0) <= S_AXI_HP2_arlen(3 downto 0);
  S_AXI_HP2_0_1_ARLOCK(1 downto 0) <= S_AXI_HP2_arlock(1 downto 0);
  S_AXI_HP2_0_1_ARPROT(2 downto 0) <= S_AXI_HP2_arprot(2 downto 0);
  S_AXI_HP2_0_1_ARQOS(3 downto 0) <= S_AXI_HP2_arqos(3 downto 0);
  S_AXI_HP2_0_1_ARSIZE(2 downto 0) <= S_AXI_HP2_arsize(2 downto 0);
  S_AXI_HP2_0_1_ARVALID <= S_AXI_HP2_arvalid;
  S_AXI_HP2_0_1_AWADDR(31 downto 0) <= S_AXI_HP2_awaddr(31 downto 0);
  S_AXI_HP2_0_1_AWBURST(1 downto 0) <= S_AXI_HP2_awburst(1 downto 0);
  S_AXI_HP2_0_1_AWCACHE(3 downto 0) <= S_AXI_HP2_awcache(3 downto 0);
  S_AXI_HP2_0_1_AWID(5 downto 0) <= S_AXI_HP2_awid(5 downto 0);
  S_AXI_HP2_0_1_AWLEN(3 downto 0) <= S_AXI_HP2_awlen(3 downto 0);
  S_AXI_HP2_0_1_AWLOCK(1 downto 0) <= S_AXI_HP2_awlock(1 downto 0);
  S_AXI_HP2_0_1_AWPROT(2 downto 0) <= S_AXI_HP2_awprot(2 downto 0);
  S_AXI_HP2_0_1_AWQOS(3 downto 0) <= S_AXI_HP2_awqos(3 downto 0);
  S_AXI_HP2_0_1_AWSIZE(2 downto 0) <= S_AXI_HP2_awsize(2 downto 0);
  S_AXI_HP2_0_1_AWVALID <= S_AXI_HP2_awvalid;
  S_AXI_HP2_0_1_BREADY <= S_AXI_HP2_bready;
  S_AXI_HP2_0_1_RREADY <= S_AXI_HP2_rready;
  S_AXI_HP2_0_1_WDATA(63 downto 0) <= S_AXI_HP2_wdata(63 downto 0);
  S_AXI_HP2_0_1_WID(5 downto 0) <= S_AXI_HP2_wid(5 downto 0);
  S_AXI_HP2_0_1_WLAST <= S_AXI_HP2_wlast;
  S_AXI_HP2_0_1_WSTRB(7 downto 0) <= S_AXI_HP2_wstrb(7 downto 0);
  S_AXI_HP2_0_1_WVALID <= S_AXI_HP2_wvalid;
  S_AXI_HP2_ACLK_0_1 <= S_AXI_HP2_ACLK;
  S_AXI_HP2_arready <= S_AXI_HP2_0_1_ARREADY;
  S_AXI_HP2_awready <= S_AXI_HP2_0_1_AWREADY;
  S_AXI_HP2_bid(5 downto 0) <= S_AXI_HP2_0_1_BID(5 downto 0);
  S_AXI_HP2_bresp(1 downto 0) <= S_AXI_HP2_0_1_BRESP(1 downto 0);
  S_AXI_HP2_bvalid <= S_AXI_HP2_0_1_BVALID;
  S_AXI_HP2_rdata(63 downto 0) <= S_AXI_HP2_0_1_RDATA(63 downto 0);
  S_AXI_HP2_rid(5 downto 0) <= S_AXI_HP2_0_1_RID(5 downto 0);
  S_AXI_HP2_rlast <= S_AXI_HP2_0_1_RLAST;
  S_AXI_HP2_rresp(1 downto 0) <= S_AXI_HP2_0_1_RRESP(1 downto 0);
  S_AXI_HP2_rvalid <= S_AXI_HP2_0_1_RVALID;
  S_AXI_HP2_wready <= S_AXI_HP2_0_1_WREADY;
  S_AXI_HP3_0_1_ARADDR(31 downto 0) <= S_AXI_HP3_araddr(31 downto 0);
  S_AXI_HP3_0_1_ARBURST(1 downto 0) <= S_AXI_HP3_arburst(1 downto 0);
  S_AXI_HP3_0_1_ARCACHE(3 downto 0) <= S_AXI_HP3_arcache(3 downto 0);
  S_AXI_HP3_0_1_ARID(5 downto 0) <= S_AXI_HP3_arid(5 downto 0);
  S_AXI_HP3_0_1_ARLEN(3 downto 0) <= S_AXI_HP3_arlen(3 downto 0);
  S_AXI_HP3_0_1_ARLOCK(1 downto 0) <= S_AXI_HP3_arlock(1 downto 0);
  S_AXI_HP3_0_1_ARPROT(2 downto 0) <= S_AXI_HP3_arprot(2 downto 0);
  S_AXI_HP3_0_1_ARQOS(3 downto 0) <= S_AXI_HP3_arqos(3 downto 0);
  S_AXI_HP3_0_1_ARSIZE(2 downto 0) <= S_AXI_HP3_arsize(2 downto 0);
  S_AXI_HP3_0_1_ARVALID <= S_AXI_HP3_arvalid;
  S_AXI_HP3_0_1_AWADDR(31 downto 0) <= S_AXI_HP3_awaddr(31 downto 0);
  S_AXI_HP3_0_1_AWBURST(1 downto 0) <= S_AXI_HP3_awburst(1 downto 0);
  S_AXI_HP3_0_1_AWCACHE(3 downto 0) <= S_AXI_HP3_awcache(3 downto 0);
  S_AXI_HP3_0_1_AWID(5 downto 0) <= S_AXI_HP3_awid(5 downto 0);
  S_AXI_HP3_0_1_AWLEN(3 downto 0) <= S_AXI_HP3_awlen(3 downto 0);
  S_AXI_HP3_0_1_AWLOCK(1 downto 0) <= S_AXI_HP3_awlock(1 downto 0);
  S_AXI_HP3_0_1_AWPROT(2 downto 0) <= S_AXI_HP3_awprot(2 downto 0);
  S_AXI_HP3_0_1_AWQOS(3 downto 0) <= S_AXI_HP3_awqos(3 downto 0);
  S_AXI_HP3_0_1_AWSIZE(2 downto 0) <= S_AXI_HP3_awsize(2 downto 0);
  S_AXI_HP3_0_1_AWVALID <= S_AXI_HP3_awvalid;
  S_AXI_HP3_0_1_BREADY <= S_AXI_HP3_bready;
  S_AXI_HP3_0_1_RREADY <= S_AXI_HP3_rready;
  S_AXI_HP3_0_1_WDATA(63 downto 0) <= S_AXI_HP3_wdata(63 downto 0);
  S_AXI_HP3_0_1_WID(5 downto 0) <= S_AXI_HP3_wid(5 downto 0);
  S_AXI_HP3_0_1_WLAST <= S_AXI_HP3_wlast;
  S_AXI_HP3_0_1_WSTRB(7 downto 0) <= S_AXI_HP3_wstrb(7 downto 0);
  S_AXI_HP3_0_1_WVALID <= S_AXI_HP3_wvalid;
  S_AXI_HP3_ACLK_0_1 <= S_AXI_HP3_ACLK;
  S_AXI_HP3_arready <= S_AXI_HP3_0_1_ARREADY;
  S_AXI_HP3_awready <= S_AXI_HP3_0_1_AWREADY;
  S_AXI_HP3_bid(5 downto 0) <= S_AXI_HP3_0_1_BID(5 downto 0);
  S_AXI_HP3_bresp(1 downto 0) <= S_AXI_HP3_0_1_BRESP(1 downto 0);
  S_AXI_HP3_bvalid <= S_AXI_HP3_0_1_BVALID;
  S_AXI_HP3_rdata(63 downto 0) <= S_AXI_HP3_0_1_RDATA(63 downto 0);
  S_AXI_HP3_rid(5 downto 0) <= S_AXI_HP3_0_1_RID(5 downto 0);
  S_AXI_HP3_rlast <= S_AXI_HP3_0_1_RLAST;
  S_AXI_HP3_rresp(1 downto 0) <= S_AXI_HP3_0_1_RRESP(1 downto 0);
  S_AXI_HP3_rvalid <= S_AXI_HP3_0_1_RVALID;
  S_AXI_HP3_wready <= S_AXI_HP3_0_1_WREADY;
  USBIND_port_indctl(1 downto 0) <= processing_system7_0_USBIND_0_PORT_INDCTL(1 downto 0);
  USBIND_vbus_pwrselect <= processing_system7_0_USBIND_0_VBUS_PWRSELECT;
  processing_system7_0_M_AXI_GP0_ARREADY <= M_AXI_GP0_arready;
  processing_system7_0_M_AXI_GP0_AWREADY <= M_AXI_GP0_awready;
  processing_system7_0_M_AXI_GP0_BID(11 downto 0) <= M_AXI_GP0_bid(11 downto 0);
  processing_system7_0_M_AXI_GP0_BRESP(1 downto 0) <= M_AXI_GP0_bresp(1 downto 0);
  processing_system7_0_M_AXI_GP0_BVALID <= M_AXI_GP0_bvalid;
  processing_system7_0_M_AXI_GP0_RDATA(31 downto 0) <= M_AXI_GP0_rdata(31 downto 0);
  processing_system7_0_M_AXI_GP0_RID(11 downto 0) <= M_AXI_GP0_rid(11 downto 0);
  processing_system7_0_M_AXI_GP0_RLAST <= M_AXI_GP0_rlast;
  processing_system7_0_M_AXI_GP0_RRESP(1 downto 0) <= M_AXI_GP0_rresp(1 downto 0);
  processing_system7_0_M_AXI_GP0_RVALID <= M_AXI_GP0_rvalid;
  processing_system7_0_M_AXI_GP0_WREADY <= M_AXI_GP0_wready;
  processing_system7_0_USBIND_0_VBUS_PWRFAULT <= USBIND_vbus_pwrfault;
processing_system7_0: component n310_ps_processing_system7_0_0
     port map (
      DDR_Addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_BankAddr(2 downto 0) => DDR_ba(2 downto 0),
      DDR_CAS_n => DDR_cas_n,
      DDR_CKE => DDR_cke,
      DDR_CS_n => DDR_cs_n,
      DDR_Clk => DDR_ck_p,
      DDR_Clk_n => DDR_ck_n,
      DDR_DM(3 downto 0) => DDR_dm(3 downto 0),
      DDR_DQ(31 downto 0) => DDR_dq(31 downto 0),
      DDR_DQS(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_DQS_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_DRSTB => DDR_reset_n,
      DDR_ODT => DDR_odt,
      DDR_RAS_n => DDR_ras_n,
      DDR_VRN => DDR_VRN,
      DDR_VRP => DDR_VRP,
      DDR_WEB => DDR_we_n,
      ENET0_MDIO_I => '0',
      ENET0_MDIO_MDC => NLW_processing_system7_0_ENET0_MDIO_MDC_UNCONNECTED,
      ENET0_MDIO_O => NLW_processing_system7_0_ENET0_MDIO_O_UNCONNECTED,
      ENET0_MDIO_T => NLW_processing_system7_0_ENET0_MDIO_T_UNCONNECTED,
      FCLK_CLK0 => processing_system7_0_FCLK_CLK0,
      FCLK_CLK1 => processing_system7_0_FCLK_CLK1,
      FCLK_CLK2 => processing_system7_0_FCLK_CLK2,
      FCLK_CLK3 => processing_system7_0_FCLK_CLK3,
      FCLK_RESET0_N => processing_system7_0_FCLK_RESET0_N,
      FCLK_RESET1_N => processing_system7_0_FCLK_RESET1_N,
      FCLK_RESET2_N => processing_system7_0_FCLK_RESET2_N,
      FCLK_RESET3_N => processing_system7_0_FCLK_RESET3_N,
      GPIO_I(63 downto 0) => GPIO_I_0_1(63 downto 0),
      GPIO_O(63 downto 0) => processing_system7_0_GPIO_O(63 downto 0),
      GPIO_T(63 downto 0) => processing_system7_0_GPIO_T(63 downto 0),
      MIO(53 downto 0) => MIO(53 downto 0),
      M_AXI_GP0_ACLK => AXI_GP0_ACLK_1,
      M_AXI_GP0_ARADDR(31 downto 0) => processing_system7_0_M_AXI_GP0_ARADDR(31 downto 0),
      M_AXI_GP0_ARBURST(1 downto 0) => processing_system7_0_M_AXI_GP0_ARBURST(1 downto 0),
      M_AXI_GP0_ARCACHE(3 downto 0) => processing_system7_0_M_AXI_GP0_ARCACHE(3 downto 0),
      M_AXI_GP0_ARID(11 downto 0) => processing_system7_0_M_AXI_GP0_ARID(11 downto 0),
      M_AXI_GP0_ARLEN(3 downto 0) => processing_system7_0_M_AXI_GP0_ARLEN(3 downto 0),
      M_AXI_GP0_ARLOCK(1 downto 0) => processing_system7_0_M_AXI_GP0_ARLOCK(1 downto 0),
      M_AXI_GP0_ARPROT(2 downto 0) => processing_system7_0_M_AXI_GP0_ARPROT(2 downto 0),
      M_AXI_GP0_ARQOS(3 downto 0) => processing_system7_0_M_AXI_GP0_ARQOS(3 downto 0),
      M_AXI_GP0_ARREADY => processing_system7_0_M_AXI_GP0_ARREADY,
      M_AXI_GP0_ARSIZE(2 downto 0) => processing_system7_0_M_AXI_GP0_ARSIZE(2 downto 0),
      M_AXI_GP0_ARVALID => processing_system7_0_M_AXI_GP0_ARVALID,
      M_AXI_GP0_AWADDR(31 downto 0) => processing_system7_0_M_AXI_GP0_AWADDR(31 downto 0),
      M_AXI_GP0_AWBURST(1 downto 0) => processing_system7_0_M_AXI_GP0_AWBURST(1 downto 0),
      M_AXI_GP0_AWCACHE(3 downto 0) => processing_system7_0_M_AXI_GP0_AWCACHE(3 downto 0),
      M_AXI_GP0_AWID(11 downto 0) => processing_system7_0_M_AXI_GP0_AWID(11 downto 0),
      M_AXI_GP0_AWLEN(3 downto 0) => processing_system7_0_M_AXI_GP0_AWLEN(3 downto 0),
      M_AXI_GP0_AWLOCK(1 downto 0) => processing_system7_0_M_AXI_GP0_AWLOCK(1 downto 0),
      M_AXI_GP0_AWPROT(2 downto 0) => processing_system7_0_M_AXI_GP0_AWPROT(2 downto 0),
      M_AXI_GP0_AWQOS(3 downto 0) => processing_system7_0_M_AXI_GP0_AWQOS(3 downto 0),
      M_AXI_GP0_AWREADY => processing_system7_0_M_AXI_GP0_AWREADY,
      M_AXI_GP0_AWSIZE(2 downto 0) => processing_system7_0_M_AXI_GP0_AWSIZE(2 downto 0),
      M_AXI_GP0_AWVALID => processing_system7_0_M_AXI_GP0_AWVALID,
      M_AXI_GP0_BID(11 downto 0) => processing_system7_0_M_AXI_GP0_BID(11 downto 0),
      M_AXI_GP0_BREADY => processing_system7_0_M_AXI_GP0_BREADY,
      M_AXI_GP0_BRESP(1 downto 0) => processing_system7_0_M_AXI_GP0_BRESP(1 downto 0),
      M_AXI_GP0_BVALID => processing_system7_0_M_AXI_GP0_BVALID,
      M_AXI_GP0_RDATA(31 downto 0) => processing_system7_0_M_AXI_GP0_RDATA(31 downto 0),
      M_AXI_GP0_RID(11 downto 0) => processing_system7_0_M_AXI_GP0_RID(11 downto 0),
      M_AXI_GP0_RLAST => processing_system7_0_M_AXI_GP0_RLAST,
      M_AXI_GP0_RREADY => processing_system7_0_M_AXI_GP0_RREADY,
      M_AXI_GP0_RRESP(1 downto 0) => processing_system7_0_M_AXI_GP0_RRESP(1 downto 0),
      M_AXI_GP0_RVALID => processing_system7_0_M_AXI_GP0_RVALID,
      M_AXI_GP0_WDATA(31 downto 0) => processing_system7_0_M_AXI_GP0_WDATA(31 downto 0),
      M_AXI_GP0_WID(11 downto 0) => processing_system7_0_M_AXI_GP0_WID(11 downto 0),
      M_AXI_GP0_WLAST => processing_system7_0_M_AXI_GP0_WLAST,
      M_AXI_GP0_WREADY => processing_system7_0_M_AXI_GP0_WREADY,
      M_AXI_GP0_WSTRB(3 downto 0) => processing_system7_0_M_AXI_GP0_WSTRB(3 downto 0),
      M_AXI_GP0_WVALID => processing_system7_0_M_AXI_GP0_WVALID,
      PS_CLK => PS_CLK,
      PS_PORB => PS_PORB,
      PS_SRSTB => PS_SRSTB,
      S_AXI_HP0_ACLK => S_AXI_HP0_ACLK_0_1,
      S_AXI_HP0_ARADDR(31 downto 0) => S_AXI_HP0_0_1_ARADDR(31 downto 0),
      S_AXI_HP0_ARBURST(1 downto 0) => S_AXI_HP0_0_1_ARBURST(1 downto 0),
      S_AXI_HP0_ARCACHE(3 downto 0) => S_AXI_HP0_0_1_ARCACHE(3 downto 0),
      S_AXI_HP0_ARID(5 downto 0) => S_AXI_HP0_0_1_ARID(5 downto 0),
      S_AXI_HP0_ARLEN(3 downto 0) => S_AXI_HP0_0_1_ARLEN(3 downto 0),
      S_AXI_HP0_ARLOCK(1 downto 0) => S_AXI_HP0_0_1_ARLOCK(1 downto 0),
      S_AXI_HP0_ARPROT(2 downto 0) => S_AXI_HP0_0_1_ARPROT(2 downto 0),
      S_AXI_HP0_ARQOS(3 downto 0) => S_AXI_HP0_0_1_ARQOS(3 downto 0),
      S_AXI_HP0_ARREADY => S_AXI_HP0_0_1_ARREADY,
      S_AXI_HP0_ARSIZE(2 downto 0) => S_AXI_HP0_0_1_ARSIZE(2 downto 0),
      S_AXI_HP0_ARVALID => S_AXI_HP0_0_1_ARVALID,
      S_AXI_HP0_AWADDR(31 downto 0) => S_AXI_HP0_0_1_AWADDR(31 downto 0),
      S_AXI_HP0_AWBURST(1 downto 0) => S_AXI_HP0_0_1_AWBURST(1 downto 0),
      S_AXI_HP0_AWCACHE(3 downto 0) => S_AXI_HP0_0_1_AWCACHE(3 downto 0),
      S_AXI_HP0_AWID(5 downto 0) => S_AXI_HP0_0_1_AWID(5 downto 0),
      S_AXI_HP0_AWLEN(3 downto 0) => S_AXI_HP0_0_1_AWLEN(3 downto 0),
      S_AXI_HP0_AWLOCK(1 downto 0) => S_AXI_HP0_0_1_AWLOCK(1 downto 0),
      S_AXI_HP0_AWPROT(2 downto 0) => S_AXI_HP0_0_1_AWPROT(2 downto 0),
      S_AXI_HP0_AWQOS(3 downto 0) => S_AXI_HP0_0_1_AWQOS(3 downto 0),
      S_AXI_HP0_AWREADY => S_AXI_HP0_0_1_AWREADY,
      S_AXI_HP0_AWSIZE(2 downto 0) => S_AXI_HP0_0_1_AWSIZE(2 downto 0),
      S_AXI_HP0_AWVALID => S_AXI_HP0_0_1_AWVALID,
      S_AXI_HP0_BID(5 downto 0) => S_AXI_HP0_0_1_BID(5 downto 0),
      S_AXI_HP0_BREADY => S_AXI_HP0_0_1_BREADY,
      S_AXI_HP0_BRESP(1 downto 0) => S_AXI_HP0_0_1_BRESP(1 downto 0),
      S_AXI_HP0_BVALID => S_AXI_HP0_0_1_BVALID,
      S_AXI_HP0_RACOUNT(2 downto 0) => NLW_processing_system7_0_S_AXI_HP0_RACOUNT_UNCONNECTED(2 downto 0),
      S_AXI_HP0_RCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP0_RCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP0_RDATA(63 downto 0) => S_AXI_HP0_0_1_RDATA(63 downto 0),
      S_AXI_HP0_RDISSUECAP1_EN => '0',
      S_AXI_HP0_RID(5 downto 0) => S_AXI_HP0_0_1_RID(5 downto 0),
      S_AXI_HP0_RLAST => S_AXI_HP0_0_1_RLAST,
      S_AXI_HP0_RREADY => S_AXI_HP0_0_1_RREADY,
      S_AXI_HP0_RRESP(1 downto 0) => S_AXI_HP0_0_1_RRESP(1 downto 0),
      S_AXI_HP0_RVALID => S_AXI_HP0_0_1_RVALID,
      S_AXI_HP0_WACOUNT(5 downto 0) => NLW_processing_system7_0_S_AXI_HP0_WACOUNT_UNCONNECTED(5 downto 0),
      S_AXI_HP0_WCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP0_WCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP0_WDATA(63 downto 0) => S_AXI_HP0_0_1_WDATA(63 downto 0),
      S_AXI_HP0_WID(5 downto 0) => S_AXI_HP0_0_1_WID(5 downto 0),
      S_AXI_HP0_WLAST => S_AXI_HP0_0_1_WLAST,
      S_AXI_HP0_WREADY => S_AXI_HP0_0_1_WREADY,
      S_AXI_HP0_WRISSUECAP1_EN => '0',
      S_AXI_HP0_WSTRB(7 downto 0) => S_AXI_HP0_0_1_WSTRB(7 downto 0),
      S_AXI_HP0_WVALID => S_AXI_HP0_0_1_WVALID,
      S_AXI_HP1_ACLK => S_AXI_HP1_ACLK_0_1,
      S_AXI_HP1_ARADDR(31 downto 0) => S_AXI_HP1_0_1_ARADDR(31 downto 0),
      S_AXI_HP1_ARBURST(1 downto 0) => S_AXI_HP1_0_1_ARBURST(1 downto 0),
      S_AXI_HP1_ARCACHE(3 downto 0) => S_AXI_HP1_0_1_ARCACHE(3 downto 0),
      S_AXI_HP1_ARID(5 downto 0) => S_AXI_HP1_0_1_ARID(5 downto 0),
      S_AXI_HP1_ARLEN(3 downto 0) => S_AXI_HP1_0_1_ARLEN(3 downto 0),
      S_AXI_HP1_ARLOCK(1 downto 0) => S_AXI_HP1_0_1_ARLOCK(1 downto 0),
      S_AXI_HP1_ARPROT(2 downto 0) => S_AXI_HP1_0_1_ARPROT(2 downto 0),
      S_AXI_HP1_ARQOS(3 downto 0) => S_AXI_HP1_0_1_ARQOS(3 downto 0),
      S_AXI_HP1_ARREADY => S_AXI_HP1_0_1_ARREADY,
      S_AXI_HP1_ARSIZE(2 downto 0) => S_AXI_HP1_0_1_ARSIZE(2 downto 0),
      S_AXI_HP1_ARVALID => S_AXI_HP1_0_1_ARVALID,
      S_AXI_HP1_AWADDR(31 downto 0) => S_AXI_HP1_0_1_AWADDR(31 downto 0),
      S_AXI_HP1_AWBURST(1 downto 0) => S_AXI_HP1_0_1_AWBURST(1 downto 0),
      S_AXI_HP1_AWCACHE(3 downto 0) => S_AXI_HP1_0_1_AWCACHE(3 downto 0),
      S_AXI_HP1_AWID(5 downto 0) => S_AXI_HP1_0_1_AWID(5 downto 0),
      S_AXI_HP1_AWLEN(3 downto 0) => S_AXI_HP1_0_1_AWLEN(3 downto 0),
      S_AXI_HP1_AWLOCK(1 downto 0) => S_AXI_HP1_0_1_AWLOCK(1 downto 0),
      S_AXI_HP1_AWPROT(2 downto 0) => S_AXI_HP1_0_1_AWPROT(2 downto 0),
      S_AXI_HP1_AWQOS(3 downto 0) => S_AXI_HP1_0_1_AWQOS(3 downto 0),
      S_AXI_HP1_AWREADY => S_AXI_HP1_0_1_AWREADY,
      S_AXI_HP1_AWSIZE(2 downto 0) => S_AXI_HP1_0_1_AWSIZE(2 downto 0),
      S_AXI_HP1_AWVALID => S_AXI_HP1_0_1_AWVALID,
      S_AXI_HP1_BID(5 downto 0) => S_AXI_HP1_0_1_BID(5 downto 0),
      S_AXI_HP1_BREADY => S_AXI_HP1_0_1_BREADY,
      S_AXI_HP1_BRESP(1 downto 0) => S_AXI_HP1_0_1_BRESP(1 downto 0),
      S_AXI_HP1_BVALID => S_AXI_HP1_0_1_BVALID,
      S_AXI_HP1_RACOUNT(2 downto 0) => NLW_processing_system7_0_S_AXI_HP1_RACOUNT_UNCONNECTED(2 downto 0),
      S_AXI_HP1_RCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP1_RCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP1_RDATA(63 downto 0) => S_AXI_HP1_0_1_RDATA(63 downto 0),
      S_AXI_HP1_RDISSUECAP1_EN => '0',
      S_AXI_HP1_RID(5 downto 0) => S_AXI_HP1_0_1_RID(5 downto 0),
      S_AXI_HP1_RLAST => S_AXI_HP1_0_1_RLAST,
      S_AXI_HP1_RREADY => S_AXI_HP1_0_1_RREADY,
      S_AXI_HP1_RRESP(1 downto 0) => S_AXI_HP1_0_1_RRESP(1 downto 0),
      S_AXI_HP1_RVALID => S_AXI_HP1_0_1_RVALID,
      S_AXI_HP1_WACOUNT(5 downto 0) => NLW_processing_system7_0_S_AXI_HP1_WACOUNT_UNCONNECTED(5 downto 0),
      S_AXI_HP1_WCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP1_WCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP1_WDATA(63 downto 0) => S_AXI_HP1_0_1_WDATA(63 downto 0),
      S_AXI_HP1_WID(5 downto 0) => S_AXI_HP1_0_1_WID(5 downto 0),
      S_AXI_HP1_WLAST => S_AXI_HP1_0_1_WLAST,
      S_AXI_HP1_WREADY => S_AXI_HP1_0_1_WREADY,
      S_AXI_HP1_WRISSUECAP1_EN => '0',
      S_AXI_HP1_WSTRB(7 downto 0) => S_AXI_HP1_0_1_WSTRB(7 downto 0),
      S_AXI_HP1_WVALID => S_AXI_HP1_0_1_WVALID,
      S_AXI_HP2_ACLK => S_AXI_HP2_ACLK_0_1,
      S_AXI_HP2_ARADDR(31 downto 0) => S_AXI_HP2_0_1_ARADDR(31 downto 0),
      S_AXI_HP2_ARBURST(1 downto 0) => S_AXI_HP2_0_1_ARBURST(1 downto 0),
      S_AXI_HP2_ARCACHE(3 downto 0) => S_AXI_HP2_0_1_ARCACHE(3 downto 0),
      S_AXI_HP2_ARID(5 downto 0) => S_AXI_HP2_0_1_ARID(5 downto 0),
      S_AXI_HP2_ARLEN(3 downto 0) => S_AXI_HP2_0_1_ARLEN(3 downto 0),
      S_AXI_HP2_ARLOCK(1 downto 0) => S_AXI_HP2_0_1_ARLOCK(1 downto 0),
      S_AXI_HP2_ARPROT(2 downto 0) => S_AXI_HP2_0_1_ARPROT(2 downto 0),
      S_AXI_HP2_ARQOS(3 downto 0) => S_AXI_HP2_0_1_ARQOS(3 downto 0),
      S_AXI_HP2_ARREADY => S_AXI_HP2_0_1_ARREADY,
      S_AXI_HP2_ARSIZE(2 downto 0) => S_AXI_HP2_0_1_ARSIZE(2 downto 0),
      S_AXI_HP2_ARVALID => S_AXI_HP2_0_1_ARVALID,
      S_AXI_HP2_AWADDR(31 downto 0) => S_AXI_HP2_0_1_AWADDR(31 downto 0),
      S_AXI_HP2_AWBURST(1 downto 0) => S_AXI_HP2_0_1_AWBURST(1 downto 0),
      S_AXI_HP2_AWCACHE(3 downto 0) => S_AXI_HP2_0_1_AWCACHE(3 downto 0),
      S_AXI_HP2_AWID(5 downto 0) => S_AXI_HP2_0_1_AWID(5 downto 0),
      S_AXI_HP2_AWLEN(3 downto 0) => S_AXI_HP2_0_1_AWLEN(3 downto 0),
      S_AXI_HP2_AWLOCK(1 downto 0) => S_AXI_HP2_0_1_AWLOCK(1 downto 0),
      S_AXI_HP2_AWPROT(2 downto 0) => S_AXI_HP2_0_1_AWPROT(2 downto 0),
      S_AXI_HP2_AWQOS(3 downto 0) => S_AXI_HP2_0_1_AWQOS(3 downto 0),
      S_AXI_HP2_AWREADY => S_AXI_HP2_0_1_AWREADY,
      S_AXI_HP2_AWSIZE(2 downto 0) => S_AXI_HP2_0_1_AWSIZE(2 downto 0),
      S_AXI_HP2_AWVALID => S_AXI_HP2_0_1_AWVALID,
      S_AXI_HP2_BID(5 downto 0) => S_AXI_HP2_0_1_BID(5 downto 0),
      S_AXI_HP2_BREADY => S_AXI_HP2_0_1_BREADY,
      S_AXI_HP2_BRESP(1 downto 0) => S_AXI_HP2_0_1_BRESP(1 downto 0),
      S_AXI_HP2_BVALID => S_AXI_HP2_0_1_BVALID,
      S_AXI_HP2_RACOUNT(2 downto 0) => NLW_processing_system7_0_S_AXI_HP2_RACOUNT_UNCONNECTED(2 downto 0),
      S_AXI_HP2_RCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP2_RCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP2_RDATA(63 downto 0) => S_AXI_HP2_0_1_RDATA(63 downto 0),
      S_AXI_HP2_RDISSUECAP1_EN => '0',
      S_AXI_HP2_RID(5 downto 0) => S_AXI_HP2_0_1_RID(5 downto 0),
      S_AXI_HP2_RLAST => S_AXI_HP2_0_1_RLAST,
      S_AXI_HP2_RREADY => S_AXI_HP2_0_1_RREADY,
      S_AXI_HP2_RRESP(1 downto 0) => S_AXI_HP2_0_1_RRESP(1 downto 0),
      S_AXI_HP2_RVALID => S_AXI_HP2_0_1_RVALID,
      S_AXI_HP2_WACOUNT(5 downto 0) => NLW_processing_system7_0_S_AXI_HP2_WACOUNT_UNCONNECTED(5 downto 0),
      S_AXI_HP2_WCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP2_WCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP2_WDATA(63 downto 0) => S_AXI_HP2_0_1_WDATA(63 downto 0),
      S_AXI_HP2_WID(5 downto 0) => S_AXI_HP2_0_1_WID(5 downto 0),
      S_AXI_HP2_WLAST => S_AXI_HP2_0_1_WLAST,
      S_AXI_HP2_WREADY => S_AXI_HP2_0_1_WREADY,
      S_AXI_HP2_WRISSUECAP1_EN => '0',
      S_AXI_HP2_WSTRB(7 downto 0) => S_AXI_HP2_0_1_WSTRB(7 downto 0),
      S_AXI_HP2_WVALID => S_AXI_HP2_0_1_WVALID,
      S_AXI_HP3_ACLK => S_AXI_HP3_ACLK_0_1,
      S_AXI_HP3_ARADDR(31 downto 0) => S_AXI_HP3_0_1_ARADDR(31 downto 0),
      S_AXI_HP3_ARBURST(1 downto 0) => S_AXI_HP3_0_1_ARBURST(1 downto 0),
      S_AXI_HP3_ARCACHE(3 downto 0) => S_AXI_HP3_0_1_ARCACHE(3 downto 0),
      S_AXI_HP3_ARID(5 downto 0) => S_AXI_HP3_0_1_ARID(5 downto 0),
      S_AXI_HP3_ARLEN(3 downto 0) => S_AXI_HP3_0_1_ARLEN(3 downto 0),
      S_AXI_HP3_ARLOCK(1 downto 0) => S_AXI_HP3_0_1_ARLOCK(1 downto 0),
      S_AXI_HP3_ARPROT(2 downto 0) => S_AXI_HP3_0_1_ARPROT(2 downto 0),
      S_AXI_HP3_ARQOS(3 downto 0) => S_AXI_HP3_0_1_ARQOS(3 downto 0),
      S_AXI_HP3_ARREADY => S_AXI_HP3_0_1_ARREADY,
      S_AXI_HP3_ARSIZE(2 downto 0) => S_AXI_HP3_0_1_ARSIZE(2 downto 0),
      S_AXI_HP3_ARVALID => S_AXI_HP3_0_1_ARVALID,
      S_AXI_HP3_AWADDR(31 downto 0) => S_AXI_HP3_0_1_AWADDR(31 downto 0),
      S_AXI_HP3_AWBURST(1 downto 0) => S_AXI_HP3_0_1_AWBURST(1 downto 0),
      S_AXI_HP3_AWCACHE(3 downto 0) => S_AXI_HP3_0_1_AWCACHE(3 downto 0),
      S_AXI_HP3_AWID(5 downto 0) => S_AXI_HP3_0_1_AWID(5 downto 0),
      S_AXI_HP3_AWLEN(3 downto 0) => S_AXI_HP3_0_1_AWLEN(3 downto 0),
      S_AXI_HP3_AWLOCK(1 downto 0) => S_AXI_HP3_0_1_AWLOCK(1 downto 0),
      S_AXI_HP3_AWPROT(2 downto 0) => S_AXI_HP3_0_1_AWPROT(2 downto 0),
      S_AXI_HP3_AWQOS(3 downto 0) => S_AXI_HP3_0_1_AWQOS(3 downto 0),
      S_AXI_HP3_AWREADY => S_AXI_HP3_0_1_AWREADY,
      S_AXI_HP3_AWSIZE(2 downto 0) => S_AXI_HP3_0_1_AWSIZE(2 downto 0),
      S_AXI_HP3_AWVALID => S_AXI_HP3_0_1_AWVALID,
      S_AXI_HP3_BID(5 downto 0) => S_AXI_HP3_0_1_BID(5 downto 0),
      S_AXI_HP3_BREADY => S_AXI_HP3_0_1_BREADY,
      S_AXI_HP3_BRESP(1 downto 0) => S_AXI_HP3_0_1_BRESP(1 downto 0),
      S_AXI_HP3_BVALID => S_AXI_HP3_0_1_BVALID,
      S_AXI_HP3_RACOUNT(2 downto 0) => NLW_processing_system7_0_S_AXI_HP3_RACOUNT_UNCONNECTED(2 downto 0),
      S_AXI_HP3_RCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP3_RCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP3_RDATA(63 downto 0) => S_AXI_HP3_0_1_RDATA(63 downto 0),
      S_AXI_HP3_RDISSUECAP1_EN => '0',
      S_AXI_HP3_RID(5 downto 0) => S_AXI_HP3_0_1_RID(5 downto 0),
      S_AXI_HP3_RLAST => S_AXI_HP3_0_1_RLAST,
      S_AXI_HP3_RREADY => S_AXI_HP3_0_1_RREADY,
      S_AXI_HP3_RRESP(1 downto 0) => S_AXI_HP3_0_1_RRESP(1 downto 0),
      S_AXI_HP3_RVALID => S_AXI_HP3_0_1_RVALID,
      S_AXI_HP3_WACOUNT(5 downto 0) => NLW_processing_system7_0_S_AXI_HP3_WACOUNT_UNCONNECTED(5 downto 0),
      S_AXI_HP3_WCOUNT(7 downto 0) => NLW_processing_system7_0_S_AXI_HP3_WCOUNT_UNCONNECTED(7 downto 0),
      S_AXI_HP3_WDATA(63 downto 0) => S_AXI_HP3_0_1_WDATA(63 downto 0),
      S_AXI_HP3_WID(5 downto 0) => S_AXI_HP3_0_1_WID(5 downto 0),
      S_AXI_HP3_WLAST => S_AXI_HP3_0_1_WLAST,
      S_AXI_HP3_WREADY => S_AXI_HP3_0_1_WREADY,
      S_AXI_HP3_WRISSUECAP1_EN => '0',
      S_AXI_HP3_WSTRB(7 downto 0) => S_AXI_HP3_0_1_WSTRB(7 downto 0),
      S_AXI_HP3_WVALID => S_AXI_HP3_0_1_WVALID,
      USB0_PORT_INDCTL(1 downto 0) => processing_system7_0_USBIND_0_PORT_INDCTL(1 downto 0),
      USB0_VBUS_PWRFAULT => processing_system7_0_USBIND_0_VBUS_PWRFAULT,
      USB0_VBUS_PWRSELECT => processing_system7_0_USBIND_0_VBUS_PWRSELECT
    );
end STRUCTURE;
