File Descriptions

boot_f100M.bin - boot.bin for setting PS FCLK0 to 100MHz
boot_f125M.bin - boot.bin for setting PS FCLK0 to 125MHz (Default for N310 OSP)
boot_f166M.bin - boot.bin for setting PS FCLK0 to 166MHz