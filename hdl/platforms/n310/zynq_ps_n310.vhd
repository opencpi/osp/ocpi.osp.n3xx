library IEEE; use IEEE.std_logic_1164.all, ieee.numeric_std.all;
library work; use work.zynq_ps_n310_pkg.all;
library axi;

entity zynq_ps_n310 is
  	port ( ps_in : in pl2ps_t;
             ps_out : out ps2pl_t;
             m_axi_gp_in : in axi.zynq_7000_m_gp.axi_s2m_array_t(0 to C_M_AXI_GP_COUNT-1);
           	 m_axi_gp_out : out axi.zynq_7000_m_gp.axi_m2s_array_t(0 to C_M_AXI_GP_COUNT-1);
             m_axi_ap_in : in axilite_ap_s2m_array_t(0 to C_M_AXI_AP_COUNT-1);
             m_axi_ap_out : out axilite_ap_m2s_array_t(0 to C_M_AXI_AP_COUNT-1);
           	 s_axi_hp_in : in axi.zynq_7000_s_hp.axi_m2s_array_t(0 to C_S_AXI_HP_COUNT-1);
             s_axi_hp_out : out axi.zynq_7000_s_hp.axi_s2m_array_t(0 to C_S_AXI_HP_COUNT-1);
             mio : inout std_logic_vector(53 downto 0);
		        gpio_in : in gpioIn_t;
             gpio_out : out gpioOut_t );
end entity zynq_ps_n310;

architecture Structural of zynq_ps_n310 is

component n310_ps_wrapper is
  port (
      DDR_VRN : inout STD_LOGIC;
      DDR_VRP : inout STD_LOGIC;
      DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
      DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
      DDR_cas_n : inout STD_LOGIC;
      DDR_ck_n : inout STD_LOGIC;
      DDR_ck_p : inout STD_LOGIC;
      DDR_cke : inout STD_LOGIC;
      DDR_cs_n : inout STD_LOGIC;
      DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
      DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
      DDR_odt : inout STD_LOGIC;
      DDR_ras_n : inout STD_LOGIC;
      DDR_reset_n : inout STD_LOGIC;
      DDR_we_n : inout STD_LOGIC;
      FCLK_CLK0 : out STD_LOGIC;
      FCLK_CLK1 : out STD_LOGIC;
      FCLK_CLK2 : out STD_LOGIC;
      FCLK_CLK3 : out STD_LOGIC;
      FCLK_RESET0_N : out STD_LOGIC;
      FCLK_RESET1_N : out STD_LOGIC;
      FCLK_RESET2_N : out STD_LOGIC;
      FCLK_RESET3_N : out STD_LOGIC;
      GPIO_I : in STD_LOGIC_VECTOR ( 63 downto 0 );
      GPIO_O : out STD_LOGIC_VECTOR ( 63 downto 0 );
      GPIO_T : out STD_LOGIC_VECTOR ( 63 downto 0 );
      MIO : inout STD_LOGIC_VECTOR ( 53 downto 0 );
      M_AXI_GP0_ACLK : in STD_LOGIC;
      M_AXI_GP0_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_GP0_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_GP0_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_GP0_arid : out STD_LOGIC_VECTOR ( 11 downto 0 );
      M_AXI_GP0_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_GP0_arlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_GP0_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_GP0_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_GP0_arready : in STD_LOGIC;
      M_AXI_GP0_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_GP0_arvalid : out STD_LOGIC;
      M_AXI_GP0_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_GP0_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_GP0_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_GP0_awid : out STD_LOGIC_VECTOR ( 11 downto 0 );
      M_AXI_GP0_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_GP0_awlock : out STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_GP0_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_GP0_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_GP0_awready : in STD_LOGIC;
      M_AXI_GP0_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_GP0_awvalid : out STD_LOGIC;
      M_AXI_GP0_bid : in STD_LOGIC_VECTOR ( 11 downto 0 );
      M_AXI_GP0_bready : out STD_LOGIC;
      M_AXI_GP0_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_GP0_bvalid : in STD_LOGIC;
      M_AXI_GP0_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_GP0_rid : in STD_LOGIC_VECTOR ( 11 downto 0 );
      M_AXI_GP0_rlast : in STD_LOGIC;
      M_AXI_GP0_rready : out STD_LOGIC;
      M_AXI_GP0_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_GP0_rvalid : in STD_LOGIC;
      M_AXI_GP0_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_GP0_wid : out STD_LOGIC_VECTOR ( 11 downto 0 );
      M_AXI_GP0_wlast : out STD_LOGIC;
      M_AXI_GP0_wready : in STD_LOGIC;
      M_AXI_GP0_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_GP0_wvalid : out STD_LOGIC;
      PS_CLK : inout STD_LOGIC;
      PS_PORB : inout STD_LOGIC;
      PS_SRSTB : inout STD_LOGIC;
      S_AXI_HP0_ACLK : in STD_LOGIC;
      S_AXI_HP0_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP0_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP0_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP0_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP0_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP0_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP0_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP0_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP0_arready : out STD_LOGIC;
      S_AXI_HP0_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP0_arvalid : in STD_LOGIC;
      S_AXI_HP0_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP0_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP0_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP0_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP0_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP0_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP0_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP0_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP0_awready : out STD_LOGIC;
      S_AXI_HP0_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP0_awvalid : in STD_LOGIC;
      S_AXI_HP0_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP0_bready : in STD_LOGIC;
      S_AXI_HP0_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP0_bvalid : out STD_LOGIC;
      S_AXI_HP0_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP0_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP0_rlast : out STD_LOGIC;
      S_AXI_HP0_rready : in STD_LOGIC;
      S_AXI_HP0_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP0_rvalid : out STD_LOGIC;
      S_AXI_HP0_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP0_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP0_wlast : in STD_LOGIC;
      S_AXI_HP0_wready : out STD_LOGIC;
      S_AXI_HP0_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_HP0_wvalid : in STD_LOGIC;
      S_AXI_HP1_ACLK : in STD_LOGIC;
      S_AXI_HP1_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP1_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP1_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP1_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP1_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP1_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP1_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP1_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP1_arready : out STD_LOGIC;
      S_AXI_HP1_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP1_arvalid : in STD_LOGIC;
      S_AXI_HP1_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP1_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP1_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP1_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP1_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP1_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP1_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP1_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP1_awready : out STD_LOGIC;
      S_AXI_HP1_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP1_awvalid : in STD_LOGIC;
      S_AXI_HP1_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP1_bready : in STD_LOGIC;
      S_AXI_HP1_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP1_bvalid : out STD_LOGIC;
      S_AXI_HP1_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP1_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP1_rlast : out STD_LOGIC;
      S_AXI_HP1_rready : in STD_LOGIC;
      S_AXI_HP1_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP1_rvalid : out STD_LOGIC;
      S_AXI_HP1_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP1_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP1_wlast : in STD_LOGIC;
      S_AXI_HP1_wready : out STD_LOGIC;
      S_AXI_HP1_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_HP1_wvalid : in STD_LOGIC;
      S_AXI_HP2_ACLK : in STD_LOGIC;
      S_AXI_HP2_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP2_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP2_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP2_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP2_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP2_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP2_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP2_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP2_arready : out STD_LOGIC;
      S_AXI_HP2_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP2_arvalid : in STD_LOGIC;
      S_AXI_HP2_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP2_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP2_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP2_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP2_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP2_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP2_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP2_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP2_awready : out STD_LOGIC;
      S_AXI_HP2_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP2_awvalid : in STD_LOGIC;
      S_AXI_HP2_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP2_bready : in STD_LOGIC;
      S_AXI_HP2_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP2_bvalid : out STD_LOGIC;
      S_AXI_HP2_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP2_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP2_rlast : out STD_LOGIC;
      S_AXI_HP2_rready : in STD_LOGIC;
      S_AXI_HP2_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP2_rvalid : out STD_LOGIC;
      S_AXI_HP2_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP2_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP2_wlast : in STD_LOGIC;
      S_AXI_HP2_wready : out STD_LOGIC;
      S_AXI_HP2_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_HP2_wvalid : in STD_LOGIC;
      S_AXI_HP3_ACLK : in STD_LOGIC;
      S_AXI_HP3_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP3_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP3_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP3_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP3_arlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP3_arlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP3_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP3_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP3_arready : out STD_LOGIC;
      S_AXI_HP3_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP3_arvalid : in STD_LOGIC;
      S_AXI_HP3_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_HP3_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP3_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP3_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP3_awlen : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP3_awlock : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP3_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP3_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_HP3_awready : out STD_LOGIC;
      S_AXI_HP3_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_HP3_awvalid : in STD_LOGIC;
      S_AXI_HP3_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP3_bready : in STD_LOGIC;
      S_AXI_HP3_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP3_bvalid : out STD_LOGIC;
      S_AXI_HP3_rdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP3_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP3_rlast : out STD_LOGIC;
      S_AXI_HP3_rready : in STD_LOGIC;
      S_AXI_HP3_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_HP3_rvalid : out STD_LOGIC;
      S_AXI_HP3_wdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
      S_AXI_HP3_wid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_HP3_wlast : in STD_LOGIC;
      S_AXI_HP3_wready : out STD_LOGIC;
      S_AXI_HP3_wstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_HP3_wvalid : in STD_LOGIC;
      USBIND_port_indctl : out STD_LOGIC_VECTOR ( 1 downto 0 );
      USBIND_vbus_pwrfault : in STD_LOGIC;
      USBIND_vbus_pwrselect : out STD_LOGIC );
end component n310_ps_wrapper;

begin

ps : n310_ps_wrapper
  port map (
      DDR_VRN => open,
      DDR_VRP => open,
      DDR_addr => open,
      DDR_ba => open,
      DDR_cas_n => open,
      DDR_ck_n => open,
      DDR_ck_p => open,
      DDR_cke => open,
      DDR_cs_n => open,
      DDR_dm => open,
      DDR_dq => open,
      DDR_dqs_n => open,
      DDR_dqs_p => open,
      DDR_odt => open,
      DDR_ras_n => open,
      DDR_reset_n => open,
      DDR_we_n => open,
      FCLK_CLK0 => ps_out.FCLK(0),
      FCLK_CLK1 => ps_out.FCLK(1),
      FCLK_CLK2 => ps_out.FCLK(2),
      FCLK_CLK3 => ps_out.FCLK(3),
      FCLK_RESET0_N => ps_out.FCLKRESET_N(0),
      FCLK_RESET1_N => ps_out.FCLKRESET_N(1),
      FCLK_RESET2_N => ps_out.FCLKRESET_N(2),
      FCLK_RESET3_N => ps_out.FCLKRESET_N(3),
      GPIO_I => gpio_in.I,
      GPIO_O => gpio_out.O,
      GPIO_T => gpio_out.T,
      MIO => mio,
      M_AXI_GP0_ACLK => m_axi_gp_in(0).A.CLK,
      M_AXI_GP0_araddr => m_axi_gp_out(0).AR.ADDR,
      M_AXI_GP0_arburst => m_axi_gp_out(0).AR.BURST,
      M_AXI_GP0_arcache => m_axi_gp_out(0).AR.CACHE,
      M_AXI_GP0_arid => m_axi_gp_out(0).AR.ID,
      M_AXI_GP0_arlen => m_axi_gp_out(0).AR.LEN,
      M_AXI_GP0_arlock => m_axi_gp_out(0).AR.LOCK,
      M_AXI_GP0_arprot => m_axi_gp_out(0).AR.PROT,
      M_AXI_GP0_arqos => open,
      M_AXI_GP0_arready => m_axi_gp_in(0).AR.READY,
      M_AXI_GP0_arsize => m_axi_gp_out(0).AR.SIZE,
      M_AXI_GP0_arvalid => m_axi_gp_out(0).AR.VALID,
      M_AXI_GP0_awaddr => m_axi_gp_out(0).AW.ADDR,
      M_AXI_GP0_awburst => m_axi_gp_out(0).AW.BURST,
      M_AXI_GP0_awcache => m_axi_gp_out(0).AW.CACHE,
      M_AXI_GP0_awid => m_axi_gp_out(0).AW.ID,
      M_AXI_GP0_awlen => m_axi_gp_out(0).AW.LEN,
      M_AXI_GP0_awlock => m_axi_gp_out(0).AW.LOCK,
      M_AXI_GP0_awprot => m_axi_gp_out(0).AW.PROT,
      M_AXI_GP0_awqos => open,
      M_AXI_GP0_awready => m_axi_gp_in(0).AW.READY,
      M_AXI_GP0_awsize => m_axi_gp_out(0).AW.SIZE,
      M_AXI_GP0_awvalid => m_axi_gp_out(0).AW.VALID,
      M_AXI_GP0_bid => m_axi_gp_in(0).B.ID,
      M_AXI_GP0_bready => m_axi_gp_out(0).B.READY,
      M_AXI_GP0_bresp => m_axi_gp_in(0).B.RESP,
      M_AXI_GP0_bvalid => m_axi_gp_in(0).B.VALID,
      M_AXI_GP0_rdata => m_axi_gp_in(0).R.DATA,
      M_AXI_GP0_rid => m_axi_gp_in(0).R.ID,
      M_AXI_GP0_rlast => m_axi_gp_in(0).R.LAST,
      M_AXI_GP0_rready => m_axi_gp_out(0).R.READY,
      M_AXI_GP0_rresp => m_axi_gp_in(0).R.RESP,
      M_AXI_GP0_rvalid => m_axi_gp_in(0).R.VALID,
      M_AXI_GP0_wdata => m_axi_gp_out(0).W.DATA,
      M_AXI_GP0_wid => m_axi_gp_out(0).W.ID,
      M_AXI_GP0_wlast => m_axi_gp_out(0).W.LAST,
      M_AXI_GP0_wready => m_axi_gp_in(0).W.READY,
      M_AXI_GP0_wstrb => m_axi_gp_out(0).W.STRB,
      M_AXI_GP0_wvalid => m_axi_gp_out(0).W.VALID,
      PS_CLK => open,
      PS_PORB => open,
      PS_SRSTB => open,
      S_AXI_HP0_ACLK => s_axi_hp_in(0).A.CLK,
      S_AXI_HP0_araddr => s_axi_hp_in(0).AR.ADDR,
      S_AXI_HP0_arburst => s_axi_hp_in(0).AR.BURST,
      S_AXI_HP0_arcache => s_axi_hp_in(0).AR.CACHE,
      S_AXI_HP0_arid => s_axi_hp_in(0).AR.ID,
      S_AXI_HP0_arlen => s_axi_hp_in(0).AR.LEN,
      S_AXI_HP0_arlock => s_axi_hp_in(0).AR.LOCK,
      S_AXI_HP0_arprot => s_axi_hp_in(0).AR.PROT,
      S_AXI_HP0_arqos => (others => '0'),
      S_AXI_HP0_arready => s_axi_hp_out(0).AR.READY,
      S_AXI_HP0_arsize => s_axi_hp_in(0).AR.SIZE,
      S_AXI_HP0_arvalid => s_axi_hp_in(0).AR.VALID,
      S_AXI_HP0_awaddr  => s_axi_hp_in(0).AW.ADDR,
      S_AXI_HP0_awburst => s_axi_hp_in(0).AW.BURST,
      S_AXI_HP0_awcache => s_axi_hp_in(0).AW.CACHE,
      S_AXI_HP0_awid => s_axi_hp_in(0).AW.ID,
      S_AXI_HP0_awlen => s_axi_hp_in(0).AW.LEN,
      S_AXI_HP0_awlock  => s_axi_hp_in(0).AW.LOCK,
      S_AXI_HP0_awprot => s_axi_hp_in(0).AW.PROT,
      S_AXI_HP0_awqos => (others => '0'),
      S_AXI_HP0_awready => s_axi_hp_out(0).AW.READY,
      S_AXI_HP0_awsize => s_axi_hp_in(0).AW.SIZE,
      S_AXI_HP0_awvalid => s_axi_hp_in(0).AW.VALID,
      S_AXI_HP0_bid  => s_axi_hp_out(0).B.ID,
      S_AXI_HP0_bready => s_axi_hp_in(0).B.READY,
      S_AXI_HP0_bresp => s_axi_hp_out(0).B.RESP,
      S_AXI_HP0_bvalid => s_axi_hp_out(0).B.VALID,
      S_AXI_HP0_rdata => s_axi_hp_out(0).R.DATA,
      S_AXI_HP0_rid => s_axi_hp_out(0).R.ID,
      S_AXI_HP0_rlast => s_axi_hp_out(0).R.LAST,
      S_AXI_HP0_rready => s_axi_hp_in(0).R.READY,
      S_AXI_HP0_rresp => s_axi_hp_out(0).R.RESP,
      S_AXI_HP0_rvalid => s_axi_hp_out(0).R.VALID,
      S_AXI_HP0_wdata => s_axi_hp_in(0).W.DATA,
      S_AXI_HP0_wid => s_axi_hp_in(0).W.ID,
      S_AXI_HP0_wlast => s_axi_hp_in(0).W.LAST,
      S_AXI_HP0_wready => s_axi_hp_out(0).W.READY,
      S_AXI_HP0_wstrb => s_axi_hp_in(0).W.STRB,
      S_AXI_HP0_wvalid  => s_axi_hp_in(0).W.VALID,
      S_AXI_HP1_ACLK => s_axi_hp_in(1).A.CLK,
      S_AXI_HP1_araddr => s_axi_hp_in(1).AR.ADDR,
      S_AXI_HP1_arburst => s_axi_hp_in(1).AR.BURST,
      S_AXI_HP1_arcache => s_axi_hp_in(1).AR.CACHE,
      S_AXI_HP1_arid => s_axi_hp_in(1).AR.ID,
      S_AXI_HP1_arlen => s_axi_hp_in(1).AR.LEN,
      S_AXI_HP1_arlock => s_axi_hp_in(1).AR.LOCK,
      S_AXI_HP1_arprot => s_axi_hp_in(1).AR.PROT,
      S_AXI_HP1_arqos => (others => '0'),
      S_AXI_HP1_arready => s_axi_hp_out(1).AR.READY,
      S_AXI_HP1_arsize => s_axi_hp_in(1).AR.SIZE,
      S_AXI_HP1_arvalid => s_axi_hp_in(1).AR.VALID,
      S_AXI_HP1_awaddr  => s_axi_hp_in(1).AW.ADDR,
      S_AXI_HP1_awburst => s_axi_hp_in(1).AW.BURST,
      S_AXI_HP1_awcache => s_axi_hp_in(1).AW.CACHE,
      S_AXI_HP1_awid  => s_axi_hp_in(1).AW.ID,
      S_AXI_HP1_awlen => s_axi_hp_in(1).AW.LEN,
      S_AXI_HP1_awlock => s_axi_hp_in(1).AW.LOCK,
      S_AXI_HP1_awprot => s_axi_hp_in(1).AW.PROT,
      S_AXI_HP1_awqos => (others => '0'),
      S_AXI_HP1_awready => s_axi_hp_out(1).AW.READY,
      S_AXI_HP1_awsize => s_axi_hp_in(1).AW.SIZE,
      S_AXI_HP1_awvalid => s_axi_hp_in(1).AW.VALID,
      S_AXI_HP1_bid  => s_axi_hp_out(1).B.ID,
      S_AXI_HP1_bready => s_axi_hp_in(1).B.READY,
      S_AXI_HP1_bresp => s_axi_hp_out(1).B.RESP,
      S_AXI_HP1_bvalid => s_axi_hp_out(1).B.VALID,
      S_AXI_HP1_rdata => s_axi_hp_out(1).R.DATA,
      S_AXI_HP1_rid => s_axi_hp_out(1).R.ID,
      S_AXI_HP1_rlast => s_axi_hp_out(1).R.LAST,
      S_AXI_HP1_rready => s_axi_hp_in(1).R.READY,
      S_AXI_HP1_rresp => s_axi_hp_out(1).R.RESP,
      S_AXI_HP1_rvalid => s_axi_hp_out(1).R.VALID,
      S_AXI_HP1_wdata => s_axi_hp_in(1).W.DATA,
      S_AXI_HP1_wid => s_axi_hp_in(1).W.ID,
      S_AXI_HP1_wlast => s_axi_hp_in(1).W.LAST,
      S_AXI_HP1_wready => s_axi_hp_out(1).W.READY,
      S_AXI_HP1_wstrb => s_axi_hp_in(1).W.STRB,
      S_AXI_HP1_wvalid => s_axi_hp_in(1).W.VALID,
      S_AXI_HP2_ACLK => s_axi_hp_in(2).A.CLK,
      S_AXI_HP2_araddr => s_axi_hp_in(2).AR.ADDR,
      S_AXI_HP2_arburst => s_axi_hp_in(2).AR.BURST,
      S_AXI_HP2_arcache => s_axi_hp_in(2).AR.CACHE,
      S_AXI_HP2_arid => s_axi_hp_in(2).AR.ID,
      S_AXI_HP2_arlen => s_axi_hp_in(2).AR.LEN,
      S_AXI_HP2_arlock => s_axi_hp_in(2).AR.LOCK,
      S_AXI_HP2_arprot => s_axi_hp_in(2).AR.PROT,
      S_AXI_HP2_arqos => (others => '0'),
      S_AXI_HP2_arready => s_axi_hp_out(2).AR.READY,
      S_AXI_HP2_arsize => s_axi_hp_in(2).AR.SIZE,
      S_AXI_HP2_arvalid => s_axi_hp_in(2).AR.VALID,
      S_AXI_HP2_awaddr => s_axi_hp_in(2).AW.ADDR,
      S_AXI_HP2_awburst => s_axi_hp_in(2).AW.BURST,
      S_AXI_HP2_awcache => s_axi_hp_in(2).AW.CACHE,
      S_AXI_HP2_awid  => s_axi_hp_in(2).AW.ID,
      S_AXI_HP2_awlen => s_axi_hp_in(2).AW.LEN,
      S_AXI_HP2_awlock  => s_axi_hp_in(2).AW.LOCK,
      S_AXI_HP2_awprot => s_axi_hp_in(2).AW.PROT,
      S_AXI_HP2_awqos => (others => '0'),
      S_AXI_HP2_awready => s_axi_hp_out(2).AW.READY,
      S_AXI_HP2_awsize => s_axi_hp_in(2).AW.SIZE,
      S_AXI_HP2_awvalid => s_axi_hp_in(2).AW.VALID,
      S_AXI_HP2_bid => s_axi_hp_out(2).B.ID,
      S_AXI_HP2_bready => s_axi_hp_in(2).B.READY,
      S_AXI_HP2_bresp => s_axi_hp_out(2).B.RESP,
      S_AXI_HP2_bvalid => s_axi_hp_out(2).B.VALID,
      S_AXI_HP2_rdata => s_axi_hp_out(2).R.DATA,
      S_AXI_HP2_rid => s_axi_hp_out(2).R.ID,
      S_AXI_HP2_rlast => s_axi_hp_out(2).R.LAST,
      S_AXI_HP2_rready => s_axi_hp_in(2).R.READY,
      S_AXI_HP2_rresp => s_axi_hp_out(2).R.RESP,
      S_AXI_HP2_rvalid => s_axi_hp_out (2).R.VALID,
      S_AXI_HP2_wdata => s_axi_hp_in(2).W.DATA,
      S_AXI_HP2_wid => s_axi_hp_in(2).W.ID,
      S_AXI_HP2_wlast => s_axi_hp_in(2).W.LAST,
      S_AXI_HP2_wready => s_axi_hp_out(2).W.READY,
      S_AXI_HP2_wstrb => s_axi_hp_in(2).W.STRB,
      S_AXI_HP2_wvalid => s_axi_hp_in(2).W.VALID,
      S_AXI_HP3_ACLK => s_axi_hp_in(3).A.CLK,
      S_AXI_HP3_araddr => s_axi_hp_in(3).AR.ADDR,
      S_AXI_HP3_arburst => s_axi_hp_in(3).AR.BURST,
      S_AXI_HP3_arcache => s_axi_hp_in(3).AR.CACHE,
      S_AXI_HP3_arid => s_axi_hp_in(3).AR.ID,
      S_AXI_HP3_arlen => s_axi_hp_in(3).AR.LEN,
      S_AXI_HP3_arlock => s_axi_hp_in(3).AR.LOCK,
      S_AXI_HP3_arprot => s_axi_hp_in(3).AR.PROT,
      S_AXI_HP3_arqos => (others => '0'),
      S_AXI_HP3_arready => s_axi_hp_out(3).AR.READY,
      S_AXI_HP3_arsize => s_axi_hp_in(3).AR.SIZE,
      S_AXI_HP3_arvalid => s_axi_hp_in(3).AR.VALID,
      S_AXI_HP3_awaddr => s_axi_hp_in(3).AW.ADDR,
      S_AXI_HP3_awburst => s_axi_hp_in(3).AW.BURST,
      S_AXI_HP3_awcache => s_axi_hp_in(3).AW.CACHE,
      S_AXI_HP3_awid  => s_axi_hp_in(3).AW.ID,
      S_AXI_HP3_awlen => s_axi_hp_in(3).AW.LEN,
      S_AXI_HP3_awlock => s_axi_hp_in(3).AW.LOCK,
      S_AXI_HP3_awprot => s_axi_hp_in(3).AW.PROT,
      S_AXI_HP3_awqos => (others => '0'),
      S_AXI_HP3_awready => s_axi_hp_out(3).AW.READY,
      S_AXI_HP3_awsize => s_axi_hp_in(3).AW.SIZE,
      S_AXI_HP3_awvalid => s_axi_hp_in(3).AW.VALID,
      S_AXI_HP3_bid => s_axi_hp_out(3).B.ID,
      S_AXI_HP3_bready => s_axi_hp_in(3).B.READY,
      S_AXI_HP3_bresp => s_axi_hp_out(3).B.RESP,
      S_AXI_HP3_bvalid => s_axi_hp_out(3).B.VALID,
      S_AXI_HP3_rdata => s_axi_hp_out(3).R.DATA,
      S_AXI_HP3_rid => s_axi_hp_out(3).R.ID,
      S_AXI_HP3_rlast => s_axi_hp_out(3).R.LAST,
      S_AXI_HP3_rready => s_axi_hp_in(3).R.READY,
      S_AXI_HP3_rresp => s_axi_hp_out(3).R.RESP,
      S_AXI_HP3_rvalid => s_axi_hp_out(3).R.VALID,
      S_AXI_HP3_wdata => s_axi_hp_in(3).W.DATA,
      S_AXI_HP3_wid => s_axi_hp_in(3).W.ID,
      S_AXI_HP3_wlast => s_axi_hp_in(3).W.LAST,
      S_AXI_HP3_wready => s_axi_hp_out(3).W.READY,
      S_AXI_HP3_wstrb => s_axi_hp_in(3).W.STRB,
      S_AXI_HP3_wvalid => s_axi_hp_in(3).W.VALID,
      USBIND_port_indctl => open,
      USBIND_vbus_pwrfault => '0',
      USBIND_vbus_pwrselect => open
  );

end Structural;