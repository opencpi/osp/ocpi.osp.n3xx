library IEEE; use IEEE.std_logic_1164.all, IEEE.numeric_std.all;
library unisim; use unisim.vcomponents.all;
library axi, sdp;

package zynq_ps_n310_pkg is

  	constant C_M_AXI_GP_COUNT 	: natural := 1;
  	constant C_S_AXI_HP_COUNT 	: natural := 4;
  	constant C_M_AXI_AP_COUNT	: natural := 16;	
  	constant C_SPI_COUNT		: natural := 0;
  	constant C_JESD_COUNT	 	: natural := 0;
	
--	---------------------------------------------------------
  	-- The global signals, which from a prefix point of view is called the A channel
  	-- Which of CLK and RESETn is driven by which side varies
  	type axilite_ap_a_m2s_t is record
  	  RESETn : std_logic;
  	end record a_m2s_t;
  	type axilite_ap_a_s2m_t is record
  	  CLK : std_logic;
  	end record a_s2m_t;
--	----------------------------------------------------------
  	-- The write address channel, a.k.a. the AW channel
  	type axilite_ap_aw_m2s_t is record
  	  ADDR : std_logic_vector(32 -1 downto 0);
  	  PROT : std_logic_vector(2 downto 0);
  	  VALID : std_logic;
  	end record axilite_ap_aw_m2s_t;
	
  	type axilite_ap_aw_s2m_t is record
  	  READY : std_logic;
  	end record axilite_ap_aw_s2m_t;
	
	
  	-- The write data channel, a.k.a. the W channel
  	type axilite_ap_w_m2s_t is record
  	  DATA : std_logic_vector(32 -1 downto 0);
  	  STRB : std_logic_vector((32/8)-1 downto 0);
  	  VALID : std_logic;
  	end record axilite_ap_w_m2s_t; 
	
  	type axilite_ap_w_s2m_t is record
  	  READY : std_logic;
  	end record axilite_ap_w_s2m_t;
	
  	-- The write response channel, a.k.a. the B channel
  	type axilite_ap_b_m2s_t is record
  	  READY : std_logic;
  	end record axilite_ap_b_m2s_t;
  	
  	type axilite_ap_b_s2m_t is record
  	  RESP : std_logic_vector(1 downto 0);
  	  VALID : std_logic;
  	end record axilite_ap_b_s2m_t;
 	
  	------------------------------------------------------------
  	-- The read address channel, a.k.a. the AR channel
  	type axilite_ap_ar_m2s_t is record
  	  ADDR : std_logic_vector(32 -1 downto 0);
  	  PROT : std_logic_vector(2 downto 0);
  	  VALID : std_logic;
  	end record axilite_ap_ar_m2s_t;
	
  	type axilite_ap_ar_s2m_t is record
  	  READY : std_logic;
  	end record axilite_ap_ar_s2m_t;
  	
	
  	-- The read data channel, a.k.a. the R channel
  	type axilite_ap_r_m2s_t is record
  	  READY : std_logic;
  	end record axilite_ap_r_m2s_t;
	
  	type axilite_ap_r_s2m_t is record
  	  DATA : std_logic_vector(32 -1 downto 0);
  	  RESP : std_logic_vector(1 downto 0);
  	  VALID : std_logic;
  	end record axilite_ap_r_s2m_t;
	
	
  	-- The bundles of signals m2s, and s2m
  	type axilite_ap_m2s_t is record
  	  a   : axilite_ap_a_m2s_t;
  	  aw  : axilite_ap_aw_m2s_t;
  	  ar  : axilite_ap_ar_m2s_t;
  	  w   : axilite_ap_w_m2s_t;
  	  r   : axilite_ap_r_m2s_t;
  	  b   : axilite_ap_b_m2s_t;
  	end record axilite_ap_m2s_t;
  	
  	type axilite_ap_s2m_t is record
  	  a   : axilite_ap_a_s2m_t;
  	  aw  : axilite_ap_aw_s2m_t;
  	  ar  : axilite_ap_ar_s2m_t;
  	  w   : axilite_ap_w_s2m_t;
  	  r   : axilite_ap_r_s2m_t;
  	  b   : axilite_ap_b_s2m_t;
  	end record axilite_ap_s2m_t;
	
  	type axilite_ap_m2s_array_t is array (natural range <>) of axilite_ap_m2s_t;
  	type axilite_ap_s2m_array_t is array (natural range <>) of axilite_ap_s2m_t;


	type ps2pl_t is record
	  	FCLK         : std_logic_vector(3 downto 0);
	  	FCLKRESET_N  : std_logic_vector(3 downto 0);
	end record ps2pl_t;
	
	type pl2ps_t is record
	  	DEBUG	: std_logic_vector(31 downto 0); --     FTMT_F2P_DEBUG
	end record pl2ps_t;
	
	type psInPort_t is record
		I	: std_logic;
	end record psInPort_t;
	
	type psOutPort_t is record
		O	: std_logic;
		T 	: std_logic;
	end record psOutPort_t;
	
	type spiInInterface_t is record
		SCLK 	: psInPort_t;
		MOSI	: psInPort_t;
		MISO	: psInPort_t;
	end record spiInInterface_t;
	
	type spiOutInterface_t is record
		SCLK	: psOutPort_t;
		MOSI	: psOutPort_t;
		MISO	: psOutPort_t;
	end record spiOutInterface_t;
	
	type spiSSInInterface_t is record
		SS		: psInPort_t;
	end record spSSInInterface_t;
	
	type spiSSOutInterface_t is record
		SS	: psOutPort_t;
		SS1	: psOutPort_t;
		SS2	: psOutPort_t;
	end record spSSOutInterface_t;
	
	type spiInInterface_array_t is array (natural range <>) of spiInInterface_t;
	type spiOutInterface_array_t is array (natural range <>) of spiOutInterface_t;
	type spiSSInInterface_array_t is array (natural range <>) of spiSSInInterface_t;
	type spiSSOutInterface_array_t is array (natural range <>) of spiSSOutInterface_t;
	
	type gpioIn_t is record
		I		: std_logic_vector(63 downto 0);
	end record gpioIn_t;
	
	type gpioOut_t is record
		O		: std_logic_vector(63 downto 0);
		T		: std_logic_vector(63 downto 0);
	end record gpioOut_t;


	component zynq_ps_n310 is
  		port (  ps_in : in pl2ps_t;
           		ps_out : out ps2pl_t;
           		m_axi_gp_in : in axi.zynq_7000_m_gp.axi_s2m_array_t(0 to C_M_AXI_GP_COUNT-1);
           		m_axi_gp_out : out axi.zynq_7000_m_gp.axi_m2s_array_t(0 to C_M_AXI_GP_COUNT-1);
           		m_axi_ap_in : in axilite_ap_s2m_array_t(0 to C_M_AXI_AP_COUNT-1);
           		m_axi_ap_out : out axilite_ap_m2s_array_t(0 to C_M_AXI_AP_COUNT-1);
           		s_axi_hp_in : in axi.zynq_7000_s_hp.axi_m2s_array_t(0 to C_S_AXI_HP_COUNT-1);
           		s_axi_hp_out : out axi.zynq_7000_s_hp.axi_s2m_array_t(0 to C_S_AXI_HP_COUNT-1);
			        mio : inout std_logic_vector(53 downto 0);
					    gpio_in	: in gpioIn_t;
					    gpio_out : out gpioOut_t );
	end component zynq_ps_n310;


end package zynq_ps_n310_pkg;