.. Platform directory index page


Platforms
=========
Available Platforms:

.. toctree::
   :maxdepth: 1
   :glob:

   */*-worker
   */*-gsg