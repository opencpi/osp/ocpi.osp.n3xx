-------------------------------------------------------------------------------
-- CMACC Module for FIRs - Symmetric Systolic Architecture
-------------------------------------------------------------------------------
--
-- File: cmacc_systolic_sym.vhd
--
-- Description:
--
-- This Complex-Multiply-Accumulate module is the basic building block for
-- complex systolic symmetric FIRs.
--
-- Block Diagram:
--
--    DLY_IN_F ----R---R---*--------- DLY_OUT
--                         |
--    DLY_IN_R ----R------(+)
--                         |
--                         R
--                         |
--    COEFF    -----------(X)
--                         |
--                         R
--                         |
--    ACC_IN   -----------(+)---R---- ACC_OUT
--
--     R   : Register
--    (X)  : Complex Multiplier
--    (+)  : Adder
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library util_prims;

entity cmacc_systolic_sym is

generic (
    DATA_WIDTH   : positive := 16;
    COEFF_WIDTH  : positive := 16;
    ACC_PREC     : positive := 37);
port (
    CLK          : in std_logic;
    RST          : in std_logic;
    DIN_VLD      : in std_logic;
    DLY_IN_F_RE  : in std_logic_vector(DATA_WIDTH-1 downto 0);
    DLY_IN_R_RE  : in std_logic_vector(DATA_WIDTH-1 downto 0);
    DLY_OUT_RE   : out std_logic_vector(DATA_WIDTH-1 downto 0);
    DLY_IN_F_IM  : in std_logic_vector(DATA_WIDTH-1 downto 0);
    DLY_IN_R_IM  : in std_logic_vector(DATA_WIDTH-1 downto 0);
    DLY_OUT_IM   : out std_logic_vector(DATA_WIDTH-1 downto 0);
    COEFF_RE     : in std_logic_vector(COEFF_WIDTH-1 downto 0);
    COEFF_IM     : in std_logic_vector(COEFF_WIDTH-1 downto 0);
    ACC_IN_RE    : in std_logic_vector(ACC_PREC-1 downto 0);
    ACC_IN_IM    : in std_logic_vector(ACC_PREC-1 downto 0);
    ACC_OUT_RE   : out std_logic_vector(ACC_PREC-1 downto 0) := (others => '0');
    ACC_OUT_IM   : out std_logic_vector(ACC_PREC-1 downto 0) := (others => '0'));

end cmacc_systolic_sym;

architecture rtl of cmacc_systolic_sym is

    signal dly_in_f_z1_re   : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal dly_in_f_z1_im   : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal dly_in_f_z2_re   : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal dly_in_f_z2_im   : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal dly_in_r_z1_re   : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal dly_in_r_z1_im   : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal pre_add_a_re     : std_logic_vector(DATA_WIDTH downto 0);
    signal pre_add_a_im     : std_logic_vector(DATA_WIDTH downto 0);
    signal pre_add_b_re     : std_logic_vector(DATA_WIDTH downto 0);
    signal pre_add_b_im     : std_logic_vector(DATA_WIDTH downto 0);
    signal pre_add_y_re     : std_logic_vector(DATA_WIDTH downto 0);
    signal pre_add_y_im     : std_logic_vector(DATA_WIDTH downto 0);
    signal pre_add_reg_re   : std_logic_vector(DATA_WIDTH downto 0) := (others => '0');
    signal pre_add_reg_im   : std_logic_vector(DATA_WIDTH downto 0) := (others => '0');
    signal mult_out_full_re : std_logic_vector(DATA_WIDTH downto 0);
    signal mult_out_full_im : std_logic_vector(DATA_WIDTH downto 0);
    signal mult_out_reg_re  : std_logic_vector(DATA_WIDTH downto 0) := (others => '0');
    signal mult_out_reg_im  : std_logic_vector(DATA_WIDTH downto 0) := (others => '0');
    signal acc_out_pre_re   : std_logic_vector(ACC_PREC-1 downto 0);
    signal acc_out_pre_im   : std_logic_vector(ACC_PREC-1 downto 0);

    signal cm_out_vld : std_logic := '0';

begin

    -- Delay Chain Registers

    process (CLK)
    begin
        if rising_edge(CLK) then
            if (DIN_VLD = '1') then
                dly_in_f_z1_re <= DLY_IN_F_RE;
                dly_in_f_z2_re <= dly_in_f_z1_re;
                dly_in_r_z1_re <= DLY_IN_R_RE;

                dly_in_f_z1_im <= DLY_IN_F_IM;
                dly_in_f_z2_im <= dly_in_f_z1_im;
                dly_in_r_z1_im <= DLY_IN_R_IM;
            end if;
        end if;
    end process;

    DLY_OUT_RE <= dly_in_f_z2_re;
    DLY_OUT_IM <= dly_in_f_z2_im;

    -- Pre-Adder

    pre_add_a_re <= std_logic_vector(resize(signed(dly_in_f_z2_re), DATA_WIDTH+1));
    pre_add_b_re <= std_logic_vector(resize(signed(dly_in_r_z1_re), DATA_WIDTH+1));
    pre_add_y_re <= std_logic_vector(signed(pre_add_a_re) + signed(pre_add_b_re));

    pre_add_a_im <= std_logic_vector(resize(signed(dly_in_f_z2_im), DATA_WIDTH+1));
    pre_add_b_im <= std_logic_vector(resize(signed(dly_in_r_z1_im), DATA_WIDTH+1));
    pre_add_y_im <= std_logic_vector(signed(pre_add_a_im) + signed(pre_add_b_im));

    process (CLK)
    begin
        if rising_edge(CLK) then
            if (DIN_VLD = '1') then
                pre_add_reg_re <= pre_add_y_re;
                pre_add_reg_im <= pre_add_y_im;
            end if;
        end if;
    end process;

    -- Complex Multiplier : Full width is DATA_WIDTH + 1
    complex_multiplier : util_prims.util_prims.complex_mult
    generic map (
        A_WIDTH  => DATA_WIDTH+1,
        B_WIDTH  => COEFF_WIDTH,
        PROD_WIDTH => DATA_WIDTH+1)
    port map (
        CLK      => CLK,
        RST      => RST,
        DIN_A_RE => pre_add_reg_re,
        DIN_A_IM => pre_add_reg_im,
        DIN_B_RE => COEFF_RE,
        DIN_B_IM => COEFF_IM,
        DIN_VLD  => DIN_VLD,
        DOUT_RE  => mult_out_full_re,
        DOUT_IM  => mult_out_full_im,
        DOUT_VLD => cm_out_vld);

    -- Accumulator
    acc_out_pre_re <= std_logic_vector(signed(ACC_IN_RE) + signed(mult_out_full_re));
    acc_out_pre_im <= std_logic_vector(signed(ACC_IN_IM) + signed(mult_out_full_im));

    process(CLK)
    begin
        if rising_edge(CLK) then
            if (cm_out_vld = '1') then
                ACC_OUT_RE <= acc_out_pre_re;
                ACC_OUT_IM <= acc_out_pre_im;
            end if;
        end if;
    end process;

end;
