-------------------------------------------------------------------------------
-- Programmable FIR Filter : Systolic Structure for Symmetric Taps (Even #)
-------------------------------------------------------------------------------
--
-- File: firfilt_ccc.vhd
--
-- Description:
--
-- This FIR Filter implementation makes use of tap symmetry and can 
-- handle both even and odd tap configurations. Taps in this design are
-- complex valued.
--
-- The following generic parameters define the construction of the FIR:
--
--    NUM_SECTIONS : The number of filter sections. For a number of taps
--                 : equal to N, ths would be set to N/2.
--    DATA_WIDTH   : Width of the filter input and final output data
--    COEFF_WIDTH  : Width of the programmable filter coefficients
--    ACC_PREC     : Accumulator Precision. This should be set to at least
--                 : DATA_WIDTH + COEFF_WIDTH + 1 + log2(NUM_SECTIONS)
--                 : to account for bit growth in each successive stage.
--    EVEN_SYM     : Tells whether there are an even or odd number of taps.
--                 : TRUE = even , FALSE = odd
--
-- Coefficient Access:
--
-- ADDR input is used to index each coefficient with an independent address.
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity firfilt_ccc is

generic(
    NUM_SECTIONS  : positive;
    DATA_WIDTH    : positive;
    COEFF_WIDTH   : positive;
    ACC_PREC      : positive;
    NUM_COEFF     : positive;
    EVEN_SYM      : boolean := true);

port(
    CLK        : in std_logic;
    RST        : in std_logic;
    DIN_RE     : in std_logic_vector(DATA_WIDTH-1 downto 0);
    DIN_IM     : in std_logic_vector(DATA_WIDTH-1 downto 0);
    DIN_VLD    : in std_logic;
    DOUT_RE    : out std_logic_vector(DATA_WIDTH-1 downto 0);
    DOUT_IM    : out std_logic_vector(DATA_WIDTH-1 downto 0);
    DOUT_VLD   : out std_logic;
    ADDR       : in std_logic_vector(15 downto 0);
    WREN       : in std_logic;
    RDATA_RE   : out std_logic_vector(COEFF_WIDTH-1 downto 0);
    RDATA_IM   : out std_logic_vector(COEFF_WIDTH-1 downto 0);
    WDATA_RE   : in std_logic_vector(COEFF_WIDTH-1 downto 0);
    WDATA_IM   : in std_logic_vector(COEFF_WIDTH-1 downto 0));

end firfilt_ccc;

architecture rtl of firfilt_ccc is

    constant MAX_POS : signed(DATA_WIDTH-1 downto 0) := (DATA_WIDTH-1 => '0', others => '1');
    constant MAX_NEG : signed(DATA_WIDTH-1 downto 0) := (DATA_WIDTH-1 => '1', others => '0');

    component cmacc_systolic_sym
    generic (
        DATA_WIDTH   : positive;
        COEFF_WIDTH  : positive;
        ACC_PREC     : positive);
    port (
        CLK          : in std_logic;
        RST          : in std_logic;
        DIN_VLD      : in std_logic;
        DLY_IN_F_RE  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_IN_R_RE  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_OUT_RE   : out std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_IN_F_IM  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_IN_R_IM  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_OUT_IM   : out std_logic_vector(DATA_WIDTH-1 downto 0);
        COEFF_RE     : in std_logic_vector(COEFF_WIDTH-1 downto 0);
        COEFF_IM     : in std_logic_vector(COEFF_WIDTH-1 downto 0);
        ACC_IN_RE    : in std_logic_vector(ACC_PREC-1 downto 0);
        ACC_IN_IM    : in std_logic_vector(ACC_PREC-1 downto 0);
        ACC_OUT_RE   : out std_logic_vector(ACC_PREC-1 downto 0) := (others => '0');
        ACC_OUT_IM   : out std_logic_vector(ACC_PREC-1 downto 0) := (others => '0'));
    end component;

    type coeff_array_t is array (natural range <>) of std_logic_vector(COEFF_WIDTH-1 downto 0);
    type data_array_t  is array (natural range <>) of std_logic_vector(DATA_WIDTH-1 downto 0);
    type accum_array_t is array (natural range <>) of std_logic_vector(ACC_PREC-1 downto 0);

    signal coeff_ptr     : std_logic_vector(15 downto 0);
    signal coeff_wren    : std_logic_vector(NUM_SECTIONS-1 downto 0);
    signal coeff_reg_re  : coeff_array_t(0 to NUM_SECTIONS-1);
    signal coeff_reg_im  : coeff_array_t(0 to NUM_SECTIONS-1);
    signal rev_dly_re    : data_array_t(0 to NUM_COEFF) := (others => (others => '0'));
    signal rev_dly_im    : data_array_t(0 to NUM_COEFF) := (others => (others => '0'));
    signal dly_chain_re  : data_array_t(0 to NUM_SECTIONS) := (others => (others => '0'));
    signal dly_chain_im  : data_array_t(0 to NUM_SECTIONS) := (others => (others => '0'));
    signal acc_chain_re  : accum_array_t(0 to NUM_SECTIONS) := (others => (others => '0'));
    signal acc_chain_im  : accum_array_t(0 to NUM_SECTIONS) := (others => (others => '0'));

begin

    ----------------------------------------------------------------------------
    -- Coefficient Registers
    ----------------------------------------------------------------------------

    coeffreg_i : for i in 0 to NUM_SECTIONS-1 generate

        coeff_wren(i) <= '1' when ( to_integer(unsigned(ADDR)) = i ) and (WREN = '1') else '0';

        coeffreg : process (CLK)
        begin
            if rising_edge(CLK) then
                if (RST = '1') then
                    coeff_reg_re(i) <= (others => '0');
                    coeff_reg_im(i) <= (others => '0');
                elsif (coeff_wren(i) = '1') then
                    coeff_reg_re(i) <= WDATA_RE;
                    coeff_reg_im(i) <= WDATA_IM;
                end if;
            end if;
        end process coeffreg;

    end generate coeffreg_i;

    addr_rdata_reg: process (CLK)
    begin
        if rising_edge(CLK) then
            if (RST = '1') then
                RDATA_RE <= (others => '0');
                RDATA_IM <= (others => '0');
            else
                RDATA_RE <= coeff_reg_re( to_integer( unsigned(ADDR) ) );
                RDATA_IM <= coeff_reg_im( to_integer( unsigned(ADDR) ) );
            end if;
        end if;
    end process addr_rdata_reg;

    ----------------------------------------------------------------------------
    -- Delay incoming data by N for reverse path
    ----------------------------------------------------------------------------

    rev_dly_re(0) <= DIN_RE;
    rev_dly_im(0) <= DIN_IM;

    revdly_i: for i in 1 to NUM_COEFF generate
        revdly: process (CLK)
        begin
            if rising_edge(CLK) then
                if (DIN_VLD = '1') then
                    rev_dly_re(i) <= rev_dly_re(i-1);
                    rev_dly_im(i) <= rev_dly_im(i-1);
                end if;
            end if;
        end process revdly;
    end generate revdly_i;

    ----------------------------------------------------------------------------
    -- Filter
    ----------------------------------------------------------------------------

    dly_chain_re(0) <= DIN_RE;
    dly_chain_im(0) <= DIN_IM;
    acc_chain_re(0) <= (others => '0');
    acc_chain_im(0) <= (others => '0');

    cmacc_i: for i in 0 to NUM_SECTIONS-1 generate

        even_cmacc : if (i < NUM_SECTIONS-1) or (EVEN_SYM = true) generate
            cmacc: cmacc_systolic_sym
            generic map (
                DATA_WIDTH   => DATA_WIDTH,
                COEFF_WIDTH  => COEFF_WIDTH,
                ACC_PREC     => ACC_PREC)
            port map (
                CLK         => CLK,
                RST         => RST,
                DIN_VLD     => DIN_VLD,
                DLY_IN_F_RE => dly_chain_re(i),
                DLY_IN_F_IM => dly_chain_im(i),
                DLY_IN_R_RE => rev_dly_re(NUM_COEFF),
                DLY_IN_R_IM => rev_dly_im(NUM_COEFF),
                DLY_OUT_RE  => dly_chain_re(i+1),
                DLY_OUT_IM  => dly_chain_im(i+1),
                COEFF_RE    => coeff_reg_re(i),
                COEFF_IM    => coeff_reg_im(i),
                ACC_IN_RE   => acc_chain_re(i),
                ACC_IN_IM   => acc_chain_im(i),
                ACC_OUT_RE  => acc_chain_re(i+1),
                ACC_OUT_IM  => acc_chain_im(i+1));
        end generate even_cmacc;

        odd_cmacc : if (i = NUM_SECTIONS-1) and (EVEN_SYM = false) generate
            cmacc: cmacc_systolic_sym
            generic map (
                DATA_WIDTH   => DATA_WIDTH,
                COEFF_WIDTH  => COEFF_WIDTH,
                ACC_PREC     => ACC_PREC)
            port map (
                CLK         => CLK,
                RST         => RST,
                DIN_VLD     => DIN_VLD,
                DLY_IN_F_RE => dly_chain_re(i),
                DLY_IN_F_IM => dly_chain_im(i),
                DLY_IN_R_RE => (others => '0'),
                DLY_IN_R_IM => (others => '0'),
                DLY_OUT_RE  => dly_chain_re(i+1),
                DLY_OUT_IM  => dly_chain_im(i+1),
                COEFF_RE    => coeff_reg_re(i),
                COEFF_IM    => coeff_reg_im(i),
                ACC_IN_RE   => acc_chain_re(i),
                ACC_IN_IM   => acc_chain_im(i),
                ACC_OUT_RE  => acc_chain_re(i+1),
                ACC_OUT_IM  => acc_chain_im(i+1));
        end generate odd_cmacc;

    end generate cmacc_i;

    ----------------------------------------------------------------------------
    -- Final Output
    ----------------------------------------------------------------------------
    -- Take bottom DATA_WIDTH bits from accum for output, clips it if the value is too large

    DOUT_RE <=  std_logic_vector(MAX_POS) when signed(acc_chain_re(NUM_SECTIONS)) > MAX_POS else
                std_logic_vector(MAX_NEG) when signed(acc_chain_re(NUM_SECTIONS)) < MAX_NEG else
                acc_chain_re(NUM_SECTIONS)(DATA_WIDTH-1 downto 0);

    DOUT_IM <=  std_logic_vector(MAX_POS) when signed(acc_chain_im(NUM_SECTIONS)) > MAX_POS else
                std_logic_vector(MAX_NEG) when signed(acc_chain_im(NUM_SECTIONS)) < MAX_NEG else
                acc_chain_im(NUM_SECTIONS)(DATA_WIDTH-1 downto 0);

    process (CLK)
    begin
        if rising_edge(CLK) then
            DOUT_VLD <= DIN_VLD;
        end if;
    end process;

end rtl;
