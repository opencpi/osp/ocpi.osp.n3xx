library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;
library util_prims;

entity frequency_translator is
    generic (
        DATA_WIDTH : positive;
        period     : natural;
        phs_inc_re : integer;
        phs_inc_im : integer);
    port (
        clk                  : in std_logic;
        rst                  : in std_logic;
        in_vld               : in std_logic;
        sample_in_real       : in std_logic_vector(DATA_WIDTH-1 downto 0);
        sample_in_imag       : in std_logic_vector(DATA_WIDTH-1 downto 0);
        translated_samp_real : out std_logic_vector(DATA_WIDTH-1 downto 0);
        translated_samp_imag : out std_logic_vector(DATA_WIDTH-1 downto 0);
        out_vld              : out std_logic);
end entity frequency_translator;

architecture rtl of frequency_translator is

    constant MAX_POS : signed(DATA_WIDTH-1 downto 0) := (DATA_WIDTH-1 => '0', others => '1');
    constant MAX_NEG : signed(DATA_WIDTH-1 downto 0) := (DATA_WIDTH-1 => '1', others => '0');

    constant phase_inc : complex := complex'(real(phs_inc_re)/32767.0, real(phs_inc_im)/32767.0);

    -- 0 is real, 1 is imag
    type complex_int is record RE, IM: integer; end record;

    type phase_int_array_t is array (0 to period) of complex_int;

    function gen_cosine(period    : natural; 
                        phase_inc : complex) return phase_int_array_t is
        variable phase : complex := (RE => 1.0, IM => 0.0);
        variable wave  : phase_int_array_t := (others => (RE => 0, IM => 0));

        variable tmp_real : integer := 0;
        variable tmp_imag : integer := 0;

    begin

        for i in 0 to period loop

            tmp_real := integer(phase.RE * 32767.0);
            tmp_imag := integer(phase.IM * 32767.0);

            if tmp_real > MAX_POS then
                tmp_real := to_integer(MAX_POS);
            elsif tmp_real < MAX_NEG then
                tmp_real := to_integer(MAX_NEG);
            end if;

            if tmp_imag > MAX_POS then
                tmp_imag := to_integer(MAX_POS);
            elsif tmp_imag < MAX_NEG then
                tmp_imag := to_integer(MAX_NEG);
            end if;

            wave(i) := (RE => tmp_real, IM => tmp_imag);
            phase := phase * phase_inc;
        end loop;

        return wave;
    end function;

    constant phases : phase_int_array_t := gen_cosine(period, phase_inc);

    signal phase_re : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');
    signal phase_im : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');

begin

    process(clk)
        variable phase_counter : integer := 0;
    begin
        if rising_edge(clk) then
            if in_vld = '1' then
                if phase_counter >= period then
                    phase_counter := 0;
                end if;
                phase_re <= std_logic_vector(to_signed(phases(phase_counter).RE, DATA_WIDTH));
                phase_im <= std_logic_vector(to_signed(phases(phase_counter).IM, DATA_WIDTH));
                phase_counter := phase_counter + 1;

            end if;
        end if;
    end process;

    -- Multiplier : Full width is COEFF_WIDTH + DATA_WIDTH + 1
    complex_multiplier : util_prims.util_prims.complex_mult
    generic map (
        A_WIDTH  => 16,--DATA_WIDTH,
        B_WIDTH  => 16,--COEFF_WIDTH,
        PROD_WIDTH => 16)--DATA_WIDTH+COEFF_WIDTH
    port map (
        CLK      => clk,
        RST      => rst,
        DIN_A_RE => sample_in_real,
        DIN_A_IM => sample_in_imag,
        DIN_B_RE => phase_re,
        DIN_B_IM => phase_im,
        DIN_VLD  => in_vld,
        DOUT_RE  => translated_samp_real,
        DOUT_IM  => translated_samp_imag,
        DOUT_VLD => open);

    delay_valid : process(clk)
    begin
        if rising_edge(clk) then
            out_vld <= in_vld;
        end if;
    end process delay_valid;

end architecture rtl;