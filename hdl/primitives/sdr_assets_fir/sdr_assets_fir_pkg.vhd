-- This package enables VHDL code to instantiate all entities and modules in this library
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use ieee.math_complex.all;
package fir is

component firfilt_ccc
    generic(
        NUM_SECTIONS  : positive;
        DATA_WIDTH    : positive;
        COEFF_WIDTH   : positive;
        ACC_PREC      : positive;
        NUM_COEFF     : positive;
        EVEN_SYM      : boolean := true);
    port(
        CLK        : in std_logic;
        RST        : in std_logic;
        DIN_RE     : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DIN_IM     : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DIN_VLD    : in std_logic;
        DOUT_RE    : out std_logic_vector(DATA_WIDTH-1 downto 0);
        DOUT_IM    : out std_logic_vector(DATA_WIDTH-1 downto 0);
        DOUT_VLD   : out std_logic;
        ADDR       : in std_logic_vector(15 downto 0);
        WREN       : in std_logic;
        RDATA_RE   : out std_logic_vector(COEFF_WIDTH-1 downto 0);
        RDATA_IM   : out std_logic_vector(COEFF_WIDTH-1 downto 0);
        WDATA_RE   : in std_logic_vector(COEFF_WIDTH-1 downto 0);
        WDATA_IM   : in std_logic_vector(COEFF_WIDTH-1 downto 0));
end component;

component cmacc_systolic_sym
    generic (
        DATA_WIDTH   : positive;
        COEFF_WIDTH  : positive;
        ACC_PREC     : positive);
    port (
        CLK          : in std_logic;
        RST          : in std_logic;
        DIN_VLD      : in std_logic;
        DLY_IN_F_RE  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_IN_R_RE  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_OUT_RE   : out std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_IN_F_IM  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_IN_R_IM  : in std_logic_vector(DATA_WIDTH-1 downto 0);
        DLY_OUT_IM   : out std_logic_vector(DATA_WIDTH-1 downto 0);
        COEFF_RE     : in std_logic_vector(COEFF_WIDTH-1 downto 0);
        COEFF_IM     : in std_logic_vector(COEFF_WIDTH-1 downto 0);
        ACC_IN_RE    : in std_logic_vector(ACC_PREC-1 downto 0);
        ACC_IN_IM    : in std_logic_vector(ACC_PREC-1 downto 0);
        ACC_OUT_RE   : out std_logic_vector(ACC_PREC-1 downto 0);
        ACC_OUT_IM   : out std_logic_vector(ACC_PREC-1 downto 0));
end component;

component frequency_translator
    generic (
        DATA_WIDTH : positive;
        period     : natural;
        phs_inc_re : integer;
        phs_inc_im : integer);
    port (
        clk                  : in std_logic;
        rst                  : in std_logic;
        in_vld               : in std_logic;
        sample_in_real       : in std_logic_vector(DATA_WIDTH-1 downto 0);
        sample_in_imag       : in std_logic_vector(DATA_WIDTH-1 downto 0);
        translated_samp_real : out std_logic_vector(DATA_WIDTH-1 downto 0);
        translated_samp_imag : out std_logic_vector(DATA_WIDTH-1 downto 0);
        out_vld              : out std_logic);
end component;

component interpolator_taps
    generic (
        USE_CLOCK : boolean := false);
    port (
        clk   : in std_logic;
        addr  : in integer;-- address of which tap to use
        tap_0 : out integer;
        tap_1 : out integer;
        tap_2 : out integer;
        tap_3 : out integer;
        tap_4 : out integer;
        tap_5 : out integer;
        tap_6 : out integer;
        tap_7 : out integer);
end component;

end package fir;

