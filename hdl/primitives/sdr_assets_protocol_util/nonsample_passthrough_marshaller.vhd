-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.
library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all;
library ocpi;
use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library sdr_assets_protocol_util; use sdr_assets_protocol_util.all;

entity nonsample_passthrough_marshaller is
    generic(
        INPUT_DATA_WIDTH  : positive := 32; -- default to 32
        OUTPUT_DATA_WIDTH : positive := 32; -- default to 32
        WSI_MBYTEEN_WIDTH : positive);
    port(
        clk       : in  std_logic;
        rst       : in  std_logic;
        -- INPUT
        in_data         : in  std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
        in_valid        : in  std_logic;
        in_ready        : in  std_logic;
        in_som          : in  std_logic;
        in_eom          : in  std_logic;
        in_opcode       : in  natural;
        in_eof          : in  std_logic;
        in_take         : out std_logic;
        -- OUTPUT TO WORKER FROM INPUT SIDE
        samp_reset      : out std_logic;
        samp_data_in    : out std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
        samp_in_vld     : out std_logic;
        samp_som_in     : out std_logic;
        samp_eom_in     : out std_logic;
        oeof            : out std_logic;
        take            : in  std_logic;
        -- INPUT FROM WORKER TO OUTPUT SIDE
        samp_data_out   : in  std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0); -- Separate port for samples data
        samp_out_vld    : in  std_logic;
        samp_eom_out    : in  std_logic;
        ieof            : in  std_logic;
        enable          : out std_logic;
        -- OUTPUT
        out_data        : out std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0);
        out_valid       : out std_logic;
        out_byte_en     : out std_logic_vector(WSI_MBYTEEN_WIDTH-1 downto 0);
        out_give        : out std_logic;
        out_eom         : out std_logic;
        out_opcode      : out natural;
        out_eof         : out std_logic;
        out_ready       : in  std_logic;
        -- DEBUG
        passthrough     : in std_logic := '0' -- Bypass worker
    );
end nonsample_passthrough_marshaller;

architecture rtl of nonsample_passthrough_marshaller is
    
    constant big_small_count    : integer := INPUT_DATA_WIDTH / OUTPUT_DATA_WIDTH - 1;
    constant small_big_count    : integer := OUTPUT_DATA_WIDTH / INPUT_DATA_WIDTH;
    signal samps_op             : std_logic;
    signal do_work              : std_logic;
    signal valid_eom            : std_logic;
    signal valid_data           : std_logic;
    signal data_op              : std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0);  
    signal op_take              : std_logic;
    signal op_valid             : std_logic;
    signal byte_en              : std_logic_vector(WSI_MBYTEEN_WIDTH-1 downto 0);
    signal counter              : integer := 0;   
    signal count_reset          : std_logic;
    signal temp_counter         : integer := 0;

    signal in_opData            : std_logic_vector(OUTPUT_DATA_WIDTH-1 downto 0);
    signal in_opValid           : std_logic;
    signal op_eom               : std_logic;
    signal op_eof               : std_logic;

begin 
    --------------------------------
    -- OPCODE HANDLING
    --------------------------------
    samps_op <= '1' when in_opcode = 0 else '0';
    do_work <=  out_ready and in_valid;


    out_valid <=    in_opValid when samps_op = '0' else 
                    '0' when (in_valid = '0' and in_som = '1' and in_eom = '1') else -- Handle zero-length messages 
                    (samp_out_vld) or (passthrough and in_valid and in_ready);

    out_give <= in_ready when (samps_op = '0' and in_som = '1' and in_eom = '1' and in_valid = '0') and in_opcode /= 0 else '0';

    out_eom <=  in_eom when (in_som = '1' and in_eom = '1' and in_valid = '0' and samps_op = '0') else 
                op_eom when samps_op = '0' else 
                (samp_eom_out) or (passthrough and in_eom);

    out_eof <=  in_eof when (in_som = '1' and in_eom = '1' and in_valid = '0' and samps_op = '0') else 
                op_eof when samps_op = '0' else 
                (ieof) or (passthrough and in_eof);  

    in_take <=  (in_som and in_eom and out_ready) or (op_take and in_ready and out_ready) when samps_op = '0' else -- nonsamps operations
                '1' when (in_valid = '0' and in_som = '1' and in_eom = '1') else -- Handle zero-length messages
                (take and in_ready) or (passthrough and out_ready); -- sample operations

    samp_reset <= rst;
    out_opcode <= in_opcode;
    enable <= out_ready;
    samp_data_in <= in_data;
    samp_in_vld <= (in_valid and in_ready and samps_op);
    samp_som_in <= in_som;
    samp_eom_in <= in_eom;
    oeof <= in_eof;
    out_byte_en <= byte_en;


    -- For bypass, if same size pass in data out, otherwise all zeros
    out_data_val_eq : if INPUT_DATA_WIDTH = OUTPUT_DATA_WIDTH generate
        out_data <= in_opData when samps_op = '0' else 
                    samp_data_out when passthrough = '0' else
                    in_data;
    end generate out_data_val_eq;

    out_data_val_not_eq : if INPUT_DATA_WIDTH /= OUTPUT_DATA_WIDTH generate
        out_data <= in_opData when samps_op = '0' else 
                    samp_data_out when passthrough = '0' else
                    (others=>'0');
    end generate out_data_val_not_eq;
    --------------------------------
    -- Handle message boundaries
    --------------------------------
    process(clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                valid_data <= '0';
                valid_eom <= '0';
            elsif (do_work = '1') then
                valid_data <= '1';
                valid_eom <= in_eom;
            elsif (out_ready = '1' and valid_data = '1') then
                valid_data <= '0';
                valid_eom <= '0';
            end if;
        end if;
    end process;

    --------------------------------
    -- Same size ports
    --------------------------------
    same_port_size : if INPUT_DATA_WIDTH = OUTPUT_DATA_WIDTH generate
        
        op_take <= (out_ready and in_ready);
        in_opData <= in_data;
        in_opValid <= in_valid;
        op_eom <= in_eom;
        op_eof <= in_eof;
        byte_en <= (others => '1'); -- TODO make better default value
        
    end generate same_port_size;

    --------------------------------
    -- Large input to small output
    --------------------------------
    large_to_small : if INPUT_DATA_WIDTH > OUTPUT_DATA_WIDTH generate

        in_opData <= data_op;
        in_opValid <= op_valid;
        op_eom <= in_eom and op_take;
        op_eof <= in_eof and op_take;
        byte_en <= (others => '1'); -- TODO make better default value

        process(clk)
            variable word_counter : integer;
            variable data_in      : std_logic_vector(INPUT_DATA_WIDTH-1 downto 0);
        begin
            if rising_edge(clk) then
                if (rst = '1') then
                    word_counter := 0;
                    data_in := (others => '0');
                    op_take <= '0';
                    op_valid <= '0';
                    data_op <= (others => '0');
                else 
                    if (samps_op = '0' and do_work = '1' and op_take = '0') then
                        if (word_counter = 0) then
                            data_in := in_data;
                        end if;

                        op_valid <= '1';
                        data_op <= data_in(OUTPUT_DATA_WIDTH-1 downto 0);
                        data_in := std_logic_vector(shift_right(unsigned(data_in), OUTPUT_DATA_WIDTH));

                        if (word_counter /= big_small_count) then
                            word_counter := word_counter + 1;
                        else
                            data_in := (others => '0');
                            word_counter := 0;
                            op_take <= '1';
                        end if;
                    elsif (out_ready = '1') then
                        op_valid <= '0';
                        op_take <= '0';
                        word_counter := 0;
                    end if;
                end if;
            end if;
            temp_counter <= word_counter;
        end process;
    end generate large_to_small;

    --------------------------------
    -- Small input to large output
    --------------------------------
    small_to_large : if INPUT_DATA_WIDTH < OUTPUT_DATA_WIDTH generate

        in_opData <= data_op;
        in_opValid <= op_valid;
        op_eom <= in_eom;
        op_eof <= in_eof;

        op_take <=  do_work when (in_eom = '0' and samps_op = '0') else
                    (do_work and valid_eom);

        op_valid <= '1' when (do_work and counter = small_big_count and samps_op = '0') else '0';
        count_reset <= '1' when (valid_eom = '1' and do_work = '1' and samps_op = '0') else '0'; 

        byte_en <= (others => '1'); -- TODO make better use

        process(clk)
        begin
            if rising_edge(clk) then
                if (rst = '1') then
                    counter <= 0;
                    data_op <= (others => '0');
                elsif (samps_op = '1') then
                    counter <= 0;
                    data_op <= (others => '0');
                else
                    if (counter = 0) then
                        if (samps_op = '0' and in_valid = '1') then
                            data_op <= in_data & data_op(OUTPUT_DATA_WIDTH-1 downto INPUT_DATA_WIDTH);
                            counter <= counter + 1;
                        else
                            data_op <= (others => '0');
                        end if;
                    elsif (counter = 1) then
                        if (samps_op = '0' and do_work = '1') then
                            data_op <= in_data & data_op(OUTPUT_DATA_WIDTH-1 downto INPUT_DATA_WIDTH);
                            counter <= counter + 1;
                        -- elsif () check G condition
                            
                        end if;
                    elsif (counter = small_big_count) then          
                        if (samps_op = '0' and do_work = '1') then   
                            data_op <= in_data & data_op(OUTPUT_DATA_WIDTH-1 downto INPUT_DATA_WIDTH);            

                            if (count_reset = '1') then
                                counter <= 0;
                            elsif (counter > 2) then
                                counter <= 1;
                            else
                                counter <= counter - 1;
                            end if;
                        end if;
                    else --- More than 3 count states
                        if (samps_op = '0' and do_work = '1') then
                            data_op <= in_data & data_op(OUTPUT_DATA_WIDTH-1 downto INPUT_DATA_WIDTH);
                            counter <= counter + 1;
                        end if;
                    end if;
                end if;
            end if;
        end process;
    end generate small_to_large;
    
end architecture rtl;
