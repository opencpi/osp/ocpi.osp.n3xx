-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.
library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all;
library ocpi;
library sdr_assets_timed_sample_prot; use sdr_assets_timed_sample_prot.timed_sample.all;

-- TODO this can handle width factors of 96, but a width of 64 would need different handling

entity timed_sample_demarshaller is
generic(
    WSI_DATA_WIDTH : positive := 32); -- default to 32
port(
    clk       : in  std_logic;
    rst       : in  std_logic;
    -- INPUT
    idata     : in  std_logic_vector(WSI_DATA_WIDTH-1 downto 0);
    ivalid    : in  ocpi.types.Bool_t;
    iready    : in  ocpi.types.Bool_t;
    isom      : in  ocpi.types.Bool_t;
    ieom      : in  ocpi.types.Bool_t;
    iopcode   : in  sdr_assets_timed_sample_prot.timed_sample.opcode_t;
    ieof      : in  ocpi.types.Bool_t;
    itake     : out ocpi.types.Bool_t;
    -- OUTPUT
    samp_data : out std_logic_vector(WSI_DATA_WIDTH-1 downto 0); -- Separate port for samples data
    oprotocol : out sdr_assets_timed_sample_prot.timed_sample.protocol_t;
    oeof      : out ocpi.types.Bool_t;
    ordy      : in  std_logic);
end entity;

architecture rtl of timed_sample_demarshaller is

    constant op_data_width : positive := 96;
    constant width_factor  : positive := (op_data_width/WSI_DATA_WIDTH) - 1;

    type data_t is array (natural range <>) of std_logic_vector(WSI_DATA_WIDTH-1 downto 0);

    -- When a take is occuring for these opcodes
    signal take_time_arr        : std_logic_vector(width_factor-1 downto 0) := (others => '0');
    signal take_samp_period_arr : std_logic_vector(width_factor-1 downto 0) := (others => '0');
    signal take_metadata_arr    : std_logic_vector(width_factor-1 downto 0) := (others => '0');

    -- When a take is on a samples opcode
    signal data_arr : data_t(width_factor-1 downto 0) := (others => (others => '0'));

    signal eozlm   : std_logic := '0';
    signal iinfo   : std_logic := '0';
    signal ixfer   : std_logic := '0';
    signal take    : std_logic := '0';
    signal take_r  : std_logic := '0';
    signal itake_s : std_logic := '0';

    -- Intermediate sample that is passed through
    signal int_sample : std_logic_vector(WSI_DATA_WIDTH-1 downto 0) := (others => '0');

    signal timed_sample_prot_s :    sdr_assets_timed_sample_prot.timed_sample.protocol_t :=
                                    sdr_assets_timed_sample_prot.timed_sample.PROTOCOL_ZERO;

    -- Array for the 96 bits of time, interval and metadata opcodes
    signal arg_95_0 : std_logic_vector(op_data_width-1 downto 0) := (others => '0');

begin

    eozlm_gen : ocpi.util.zlm_detector
    port map(
        clk         => clk,
        reset       => rst,
        som         => isom,
        valid       => ivalid,
        eom         => ieom,
        ready       => iready,
        take        => itake_s,
        eozlm_pulse => open,
        eozlm       => eozlm);

    iinfo <= '1' when ((iready = '1') and ((ivalid = '1') or (eozlm = '1'))) else '0';

    take  <= iinfo and ordy;
    ixfer <= iinfo and itake_s;

    data_reg : process(clk)
    begin
        if(rising_edge(clk)) then
            if(rst = '1') then
                data_arr <= (others => (others => '0'));
            elsif(take = '1') then
                data_arr(0) <= idata;
                for I in 1 to width_factor-1 loop
                    data_arr(I) <= data_arr(I-1);
                end loop;
            end if;
        end if;
    end process data_reg;

    take_final_regs : process(clk)
    begin
        if(rising_edge(clk)) then
            if(rst = '1') then
                take_time_arr        <= (others => '0');
                take_samp_period_arr <= (others => '0');
                take_metadata_arr    <= (others => '0');
            elsif(take = '1') then
                if(iopcode = sdr_assets_timed_sample_prot.timed_sample.TIME_TIME) then
                    if take_time_arr(width_factor-1) = '1' then
                        take_time_arr <= (others => '0');
                    else
                        take_time_arr(0) <= not take_time_arr(0);
                        for I in 1 to width_factor-1 loop
                            take_time_arr(I) <= take_time_arr(I-1);
                        end loop;
                    end if;
                else
                    take_time_arr <= (others => '0');
                end if;

                if(iopcode = sdr_assets_timed_sample_prot.timed_sample.SAMPLE_INTERVAL) then
                    if take_samp_period_arr(width_factor-1) = '1' then
                        take_samp_period_arr <= (others => '0');
                    else
                        take_samp_period_arr(0) <= not take_samp_period_arr(0);
                        for I in 1 to width_factor-1 loop
                            take_samp_period_arr(I) <= take_samp_period_arr(I-1);
                        end loop;
                    end if;
                else
                    take_samp_period_arr <= (others => '0');
                end if;

                if(iopcode = sdr_assets_timed_sample_prot.timed_sample.METADATA) then
                    if take_metadata_arr(width_factor-1) = '1' then
                        take_metadata_arr <= (others => '0');
                    else
                        take_metadata_arr(0) <= not take_metadata_arr(0);
                        for I in 1 to width_factor-1 loop
                            take_metadata_arr(I) <= take_metadata_arr(I-1);
                        end loop;
                    end if;
                else
                    take_metadata_arr <= (others => '0');
                end if;
            end if;
        end if;
    end process take_final_regs;

    -- reference https://opencpi.github.io/OpenCPI_HDL_Development.pdf section
    -- 3.8.1 Message Payloads vs. Physical Data Width on Data Interfaces

    arg_95_0(op_data_width-1 downto op_data_width-WSI_DATA_WIDTH) <= idata;
    op_data_gen : for I in 0 to width_factor-1 generate
        arg_95_0(((I+1)*WSI_DATA_WIDTH)-1 downto I*WSI_DATA_WIDTH) <= data_arr(width_factor-I-1);
    end generate op_data_gen;

    -- this is the heart of the demarshalling functionality

    int_sample <= idata;

    timed_sample_prot_s.sample_vld <= '1' when (iopcode = sdr_assets_timed_sample_prot.timed_sample.SAMPLE) and (ixfer = '1') else '0';

    timed_sample_prot_s.time.fraction(63 downto 32) <= arg_95_0(63 downto 32); -- Fraction is type ulonglong
    timed_sample_prot_s.time.fraction(31 downto 0)  <= arg_95_0(31 downto 0); 
    timed_sample_prot_s.time.seconds                <= arg_95_0(95 downto 64); -- Seconds is only 32 bits

    timed_sample_prot_s.time_vld <= '1' when (take_time_arr(width_factor-1) = '1') and (ixfer = '1') else '0';

    timed_sample_prot_s.sample_interval.fraction(63 downto 32) <= arg_95_0(63 downto 32); -- Fraction is type ulonglong
    timed_sample_prot_s.sample_interval.fraction(31 downto 0)  <= arg_95_0(31 downto 0); 
    timed_sample_prot_s.sample_interval.seconds                <= arg_95_0(95 downto 64); -- Seconds is only 32 bits

    timed_sample_prot_s.sample_interval_vld <= '1' when (take_samp_period_arr(width_factor-1) = '1') and (ixfer = '1') else '0';

    timed_sample_prot_s.flush <= '1' when (iopcode = sdr_assets_timed_sample_prot.timed_sample.FLUSH) and (ixfer = '1') else '0';

    timed_sample_prot_s.discontinuity <= '1' when (iopcode = sdr_assets_timed_sample_prot.timed_sample.DISCONTINUITY) and (ixfer = '1') else '0';

    timed_sample_prot_s.metadata.value(63 downto 32) <= arg_95_0(63 downto 32); -- Metadata mimics what is done by the time and sample_interval opcodes
    timed_sample_prot_s.metadata.value(31 downto 0)  <= arg_95_0(31 downto 0); 
    timed_sample_prot_s.metadata.id                  <= arg_95_0(95 downto 64);

    timed_sample_prot_s.metadata_vld <= '1' when (take_metadata_arr(width_factor-1) = '1') and (ixfer = '1') else '0';

    -- necessary to prevent combinatorial loop, depending an what's connected to ordy
    pipeline : process(clk)
    begin
        if(rising_edge(clk)) then
            if(rst = '1') then
                samp_data <= (others => '0');
                oprotocol <= PROTOCOL_ZERO;
                oeof      <= '0';
            else
                if(ordy = '1') then
                    samp_data <= int_sample;
                    oprotocol <= timed_sample_prot_s;
                    oeof      <= ieof;
                end if;
            end if;
        end if;
    end process pipeline;

    itake_s <= take;
    itake <= itake_s;

end rtl;
