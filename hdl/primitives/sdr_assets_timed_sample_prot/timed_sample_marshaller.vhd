-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.
library ieee; use ieee.std_logic_1164.all, ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi; use ocpi.types.all;
library util;
library sdr_assets_timed_sample_prot; use sdr_assets_timed_sample_prot.timed_sample.all;

-- TODO this can handle width factors of 96, but a width of 64 would need different handling

entity timed_sample_marshaller is
    generic(
        WSI_DATA_WIDTH    : positive := 32;
        WSI_MBYTEEN_WIDTH : positive);
    port(
        clk          : in  std_logic;
        rst          : in  std_logic;
        -- INPUT
        samp_data    : in  std_logic_vector(WSI_DATA_WIDTH-1 downto 0); -- Separate port for samples data
        iprotocol    : in  sdr_assets_timed_sample_prot.timed_sample.protocol_t;
        ieof         : in  ocpi.types.Bool_t;
        irdy         : out std_logic;
        -- OUTPUT
        odata        : out std_logic_vector(WSI_DATA_WIDTH-1 downto 0);
        ovalid       : out ocpi.types.Bool_t;
        obyte_enable : out std_logic_vector(WSI_MBYTEEN_WIDTH-1 downto 0);
        ogive        : out ocpi.types.Bool_t;
        osom         : out ocpi.types.Bool_t;
        oeom         : out ocpi.types.Bool_t;
        oopcode      : out sdr_assets_timed_sample_prot.timed_sample.opcode_t;
        oeof         : out ocpi.types.Bool_t;
        oready       : in  ocpi.types.Bool_t);
end entity;
architecture rtl of timed_sample_marshaller is

    constant op_data_width : positive := 96;
    constant width_factor  : positive := (op_data_width/WSI_DATA_WIDTH);
    constant index_size : positive := positive(ceil(log2(real(op_data_width))));

    -- output states - what we are offering to give
    type state_t is (SAMPLE, TIME_T, INTERVAL, FLUSH, DISCONTINUITY, METADATA, EOF, IDLE);

    -- The mux end for these opcodes
    signal mux_time     : std_logic := '0';
    signal mux_interval : std_logic := '0';
    signal mux_metadata : std_logic := '0';

    -- Tell when the last of these opcodes have come through
    signal last_time     : std_logic := '0';
    signal last_interval : std_logic := '0';
    signal last_metadata : std_logic := '0';

    signal time_idx     : unsigned(index_size-1 downto 0) := to_unsigned(WSI_DATA_WIDTH, index_size);
    signal interval_idx : unsigned(index_size-1 downto 0) := to_unsigned(WSI_DATA_WIDTH, index_size);
    signal metadata_idx : unsigned(index_size-1 downto 0) := to_unsigned(WSI_DATA_WIDTH, index_size);

    signal ivld        : bool_t; -- the inputs are valid (should be an input but is not)
    signal iprotocol_r :    sdr_assets_timed_sample_prot.timed_sample.protocol_t :=
                            sdr_assets_timed_sample_prot.timed_sample.PROTOCOL_ZERO;
    signal ieof_r      : bool_t; -- the EOF indication associated with the pipeline register
    signal irdy_s      : bool_t; -- are we ready to load the pipeline?
    signal ifull_r     : bool_t; -- is the pipeline register occupied?
    signal ifirst_r    : bool_t; -- are we doing the first thing indicated by what is in the pipeline reg?
    signal mux_start   : bool_t; -- a copy of ifirst_r;
    signal mux_end     : bool_t; -- we are offering the last thing indicated in the pipeline reg
    signal state       : state_t; -- what is being offered to the output port
    signal state_r     : state_t; -- what was last offered

    signal int_sample : std_logic_vector(WSI_DATA_WIDTH-1 downto 0);

    signal valid        : bool_t;
    signal give         : bool_t;
    signal som          : bool_t;
    signal eom          : bool_t;
    signal opcode : sdr_assets_timed_sample_prot.timed_sample.opcode_t :=
                    sdr_assets_timed_sample_prot.timed_sample.SAMPLE;
begin
    -- FIXME bring in the overall valid signal and remove qualifications in the complex_xxx fifo
    ivld <= iprotocol.sample_vld or
            iprotocol.time_vld or
            iprotocol.sample_interval_vld or
            iprotocol.flush or
            iprotocol.discontinuity or
            iprotocol.metadata_vld or
            ieof;

    -- We can accept input if the pipeline register is empty or we are emptying it in this cycle,
    -- but not if asserting EOF
    irdy_s  <= not ifull_r or (mux_end and oready and not ieof_r);
    irdy    <= irdy_s;

    -- We offer output if the pipelne register is full
    -- And if we are sending samples, we ALSO need input valid, to know whether we need EOM
    give    <= ifull_r and to_bool(state /= SAMPLE or ivld);
    valid   <= give and to_bool(state /= DISCONTINUITY and state /= FLUSH and state /= EOF); -- assert valid if not ZLM
    pipeline: process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                int_sample  <= (others => '0');
                iprotocol_r <= sdr_assets_timed_sample_prot.timed_sample.PROTOCOL_ZERO;
                ieof_r      <= '0';
                ifull_r     <= '0';
                ifirst_r    <= '0';
                state_r     <= IDLE;

                time_idx     <= to_unsigned(WSI_DATA_WIDTH, index_size);
                interval_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);
                metadata_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);

            else
                if ifull_r and state = IDLE then
                    report "No outputs when pipeline register is full" severity failure;
                end if;
                -- input side: load the pipeline if there is input and we can accept it
                if ivld = '1' and irdy_s = '1' then
                    int_sample  <= samp_data;
                    iprotocol_r <= iprotocol;
                    ieof_r      <= ieof;
                    ifull_r     <= '1';
                    ifirst_r    <= '1';
                elsif give and oready then
                    -- we're not loading but we are transferring
                    ifirst_r    <= '0';
                    if mux_end = '1' and not its(ieof_r) then
                        ifull_r   <= '0';
                    end if;
                end if;
                -- we are transferring so advance the state
                if give and oready then
                    state_r <= state;

                    if state = TIME_T then
                        if time_idx >= op_data_width then
                            time_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);
                        else
                            time_idx <= time_idx + WSI_DATA_WIDTH;
                        end if;
                    else
                        time_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);
                    end if;

                    if state = INTERVAL then
                        if interval_idx >= op_data_width then
                            interval_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);
                        else
                            interval_idx <= interval_idx + WSI_DATA_WIDTH;
                        end if;
                    else
                        interval_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);
                    end if;

                    if state = METADATA then
                        if metadata_idx >= op_data_width then
                            metadata_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);
                        else
                            metadata_idx <= metadata_idx + WSI_DATA_WIDTH;
                        end if;
                    else
                        metadata_idx <= to_unsigned(WSI_DATA_WIDTH, index_size);
                    end if;

                end if;
            end if;
        end if;
    end process pipeline;

    mux_start <= ifirst_r;

    mux_time     <= '1' when state = TIME_T   and time_idx     = op_data_width else '0';
    mux_interval <= '1' when state = INTERVAL and interval_idx = op_data_width else '0';
    mux_metadata <= '1' when state = METADATA and metadata_idx = op_data_width else '0';

    last_time     <= '1' when state_r = TIME_T   and time_idx     = WSI_DATA_WIDTH else '0';
    last_interval <= '1' when state_r = INTERVAL and interval_idx = WSI_DATA_WIDTH else '0';
    last_metadata <= '1' when state_r = METADATA and metadata_idx = WSI_DATA_WIDTH else '0';

    -- FIXME state should not be IDLE if the pipeline register is full
    -- sets the priority of multiplexing of output messages
    -- priority is: DISCONTINUITY, TIME, INTERVAL, FLUSH, SAMPLES, EOF
    mux_end   <= to_bool(state = SAMPLE or state = EOF or state = IDLE 
                        or
                        (state = DISCONTINUITY and iprotocol_r.time_vld = '0' and iprotocol_r.sample_interval_vld = '0' and
                        iprotocol_r.flush = '0' and iprotocol_r.metadata_vld = '0' and iprotocol_r.sample_vld = '0') 
                        or
                        (mux_time = '1' and iprotocol_r.sample_interval_vld = '0' and iprotocol_r.flush = '0' and
                        iprotocol_r.metadata_vld = '0' and iprotocol_r.sample_vld = '0') 
                        or
                        (mux_interval = '1' and iprotocol_r.flush = '0' and iprotocol_r.metadata_vld = '0' and iprotocol_r.sample_vld = '0') 
                        or
                        (state = FLUSH and iprotocol_r.sample_vld = '0' and iprotocol_r.metadata_vld = '0') 
                        or
                        (mux_metadata = '1' and iprotocol_r.sample_vld = '0'));

    state <=    IDLE when not ifull_r else

                DISCONTINUITY when iprotocol_r.discontinuity = '1' and mux_start else

                TIME_T when iprotocol_r.time_vld = '1' and (mux_start or state_r = DISCONTINUITY) else

                TIME_T when state_r = TIME_T and time_idx > WSI_DATA_WIDTH else

                INTERVAL when iprotocol_r.sample_interval_vld = '1' and
                                        (mux_start or state_r = DISCONTINUITY or last_time = '1') else

                INTERVAL when state_r = INTERVAL and interval_idx > WSI_DATA_WIDTH else

                FLUSH when  iprotocol_r.flush = '1' and 
                            (mux_start or state_r = DISCONTINUITY or last_time = '1' 
                            or last_interval = '1') else

                SAMPLE when iprotocol_r.sample_vld = '1' and
                            (mux_start or state_r = DISCONTINUITY or last_time = '1' 
                            or last_interval = '1' or state_r = FLUSH) else

                METADATA when  iprotocol_r.metadata_vld = '1' and 
                                    (mux_start or state_r = DISCONTINUITY or last_time = '1' or 
                                    last_interval = '1' or state_r = FLUSH or state_r = SAMPLE) else

                METADATA when state_r = METADATA and metadata_idx > WSI_DATA_WIDTH else

                EOF when state_r = EOF or (ieof_r and 
                        (mux_start or state_r = DISCONTINUITY or last_time = '1' or
                        last_interval = '1' or state_r = FLUSH or state_r = SAMPLE or
                        last_metadata = '1')) else

                IDLE;

    ogen : process(state, iprotocol_r, iprotocol, time_idx, interval_idx, metadata_idx, int_sample)
        variable slv_time_data     : std_logic_vector(op_data_width-1 downto 0) := (others => '0');
        variable slv_interval_data : std_logic_vector(op_data_width-1 downto 0) := (others => '0');
        variable slv_metadata_data : std_logic_vector(op_data_width-1 downto 0) := (others => '0');
    begin
        slv_time_data     := sdr_assets_timed_sample_prot.timed_sample.to_slv(iprotocol_r.time);
        slv_interval_data := sdr_assets_timed_sample_prot.timed_sample.to_slv(iprotocol_r.sample_interval);
        slv_metadata_data := sdr_assets_timed_sample_prot.timed_sample.to_slv(iprotocol_r.metadata);

        case state is
        when SAMPLE =>
            opcode <= sdr_assets_timed_sample_prot.timed_sample.SAMPLE;
            odata  <= int_sample;
            som    <= '0'; -- inserteom deals with this
            -- force EOM if next thing is higher priority, but inserteom takes care of normal eom
            eom    <= iprotocol.discontinuity or iprotocol.time_vld or iprotocol.sample_interval_vld or iprotocol.flush;
        when TIME_T =>
            opcode <= sdr_assets_timed_sample_prot.timed_sample.TIME_TIME; -- fraction is split into two sections. This is secion 1.
            odata <= slv_time_data(to_integer(time_idx)-1 downto to_integer(time_idx)-WSI_DATA_WIDTH);
            som    <= '0';
            eom    <= '0';
            if time_idx = WSI_DATA_WIDTH then
                som    <= '1';
            elsif time_idx = op_data_width then
                eom    <= '1';
            end if;
        when INTERVAL =>
            opcode <= sdr_assets_timed_sample_prot.timed_sample.SAMPLE_INTERVAL;
            odata <= slv_interval_data(to_integer(interval_idx)-1 downto to_integer(interval_idx)-WSI_DATA_WIDTH);
            som    <= '0';
            eom    <= '0';
            if interval_idx = WSI_DATA_WIDTH then
                som    <= '1';
            elsif interval_idx = op_data_width then
                eom    <= '1';
            end if;
        when DISCONTINUITY =>
            opcode <= sdr_assets_timed_sample_prot.timed_sample.DISCONTINUITY;
            odata  <= (others => '0');
            som    <= '1';
            eom    <= '1';
        when FLUSH =>
            opcode <= sdr_assets_timed_sample_prot.timed_sample.FLUSH;
            odata  <= (others => '0');
            som    <= '1';
            eom    <= '1';
        when METADATA =>
            opcode <= sdr_assets_timed_sample_prot.timed_sample.METADATA;
            odata <= slv_metadata_data(to_integer(metadata_idx)-1 downto to_integer(metadata_idx)-WSI_DATA_WIDTH);
            som    <= '0';
            eom    <= '0';
            if metadata_idx = WSI_DATA_WIDTH then
                som    <= '1';
            elsif metadata_idx = op_data_width then
                eom    <= '1';
            end if;
        when others => -- IDLE or EOF
            odata  <= (others => '0');
            som    <= '0';
            eom    <= '0';
        end case;
    end process ogen;

    oopcode      <= opcode;
    ovalid       <= valid;
    ogive        <= give;
    osom         <= som;
    oeom         <= eom;
    obyte_enable <= (others => '1');
    oeof         <= to_bool(state = EOF);

end rtl;
