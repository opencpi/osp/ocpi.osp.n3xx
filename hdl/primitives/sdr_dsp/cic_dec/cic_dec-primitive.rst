.. cic_dec documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _cic_dec-primitive:


CIC decimator (``cic_dec``)
===========================
CIC (cascading integrating comb) filter combined with decimator.

Design
------
CIC decimators are an efficient method of reducing the sample rate of an input signal by an integer factor. CIC decimators do not require any multiplication operations making them particularly suitable for compact FPGA designs.

A block diagram representation of the implementation:

.. _cic_dec-diagram:

.. figure:: cic_dec.svg
   :alt: Block diagram of CIC filter with decimator implementation
   :align: center

   Block diagram of CIC decimator implementation.

Further details on CIC decimators can be found in :ref:`cic_dec_csts`.

Implementation
--------------
Inputs are read into the primitive on the rising edge of ``clk``. Outputs are set on rising edge of ``clk``. ``rst`` provides a synchronous, active high, reset signal to the module.

The primitive is pipelined so that a new sample can be inserted into the primitive on every rising edge of ``clk``. The ``data_valid_in`` signal allows each input sample to be marked as valid or not. The pipeline advances every rising edge of ``clk`` regardless of if valid data is passed to the primitive or not. The ``data_valid_out`` signal is a delayed version of ``valid_in`` that indicates which outputs were calculated based on valid inputs.

From ``data_valid_in`` to ``data_valid_out`` there is a latency of ``comb_stages_g`` + ``int_stages_g`` + 1. .

The ``enable`` signal provides a clock enable for the primitive. When ``enable`` is set low no data will enter or leave the primitive and all calculations are stopped. When ``enable`` is high the module will operate normally.

Interface
---------

Generics
~~~~~~~~

 * ``int_stages_g`` (``integer``): Number of integrator stages. Should be equal to ``comb_stages_g`` in most situations.

 * ``comb_stages_g`` (``integer``): Number of comb stages. Should be equal to ``int_stages_g`` in most situations.

 * ``diff_delay_g`` (``integer``): Differential delay of the comb stages. Typically 1 or 2.

 * ``input_word_size_g`` (``integer``): Bit width of input signal.

 * ``output_word_size_g`` (``integer``): Bit width of output signal and internal registers. The minimum value allowable value of ``output_word_size_g`` is :math:`\texttt{input_word_size_g} + \left \lceil \texttt{int_stages_g}*log_{2}\left ( \texttt{down_sample_factor}*\texttt{diff_delay_g} \right ) \right \rceil`.

 * ``dec_factor_size_g`` (``integer``): Bit width of the down_sample_factor signal.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_en`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``data_valid_in`` (``std_logic``), in: ``data_in`` is valid when ``data_valid_in`` is high.

 * ``data_in`` (``signed``, ``input_word_size`` bits), in: Input data to the CIC decimator

 * ``down_sample_factor`` (``unsigned``, ``dec_factor_size_g`` bits), in: The factor by which the data is down sampled. E.g. if ``down_sample_factor`` is set to 2 the data rate of ``data_out`` will be half that of ``data_in``. Must not be set to 0.

 * ``data_valid_out`` (``std_logic``), out: ``data_out`` is valid when ``data_valid_out`` is high.

 * ``data_out`` (``signed``, ``output_word_size`` bits), out: Output data from the CIC decimator.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cic_dec`` are:

 * None.
