-- HDL Implementation of a CIC interpolator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Description : Cascaded integrator-comb filter (CIC) based interpolator
-- ----------------------------------------------------------------------------
-- Usage :
-- Interpolates an input signal using a CIC filter
-- See http://dspguru.com/sites/dspguru/files/cic.pdf for more information
-- on CIC filters.
-- Number of integrator and comb stages can be set with a generic
-- For typical usage int_stages_g == comb_stages_g
-- The gain of the filter is G = ((R*M)^N)/R where:
-- R is the interpolation factor which can be set at runtime.
-- M is differential delay
-- N is the number of integrator and comb sections
-- The size of the output and intermediate calculation registers is given by:
-- bits_out = ceil[N*(log2(R*M))+bits_in]
-- E.g. for a max 36 bit output size and 16 bit input size, the largest
-- interpolation factor would be R = floor[(2^((32-12)/3))/2] = 50
-- (when N=3,M=2).
-- -----------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity cic_int is
  generic (
    int_stages_g       : integer := 3;
    comb_stages_g      : integer := 3;
    diff_delay_g       : integer := 2;
    input_word_size_g  : integer := 16;
    output_word_size_g : integer := 36;  -- ceil([N*log2(RM)]+input_bits)
    int_factor_size_g  : integer := 16
    );

  port (
    clk              : in  std_logic;
    reset            : in  std_logic;
    clk_en           : in  std_logic;
    data_valid_in    : in  std_logic;
    data_in          : in  signed(input_word_size_g - 1 downto 0);
    up_sample_factor : in  unsigned(int_factor_size_g - 1 downto 0);
    input_hold       : out std_logic;
    data_valid_out   : out std_logic;
    data_out         : out signed(output_word_size_g - 1 downto 0)
    );
end cic_int;

architecture rtl of cic_int is

  -- Define array types
  type integrate_array_t is array(0 to int_stages_g - 1) of
    signed(output_word_size_g - 1 downto 0);
  type comb_array_t is array(0 to int_stages_g - 1) of
    signed(output_word_size_g - 1 downto 0);
  type diff_delay_t is array(0 to diff_delay_g - 1) of comb_array_t;

  -- Integrator pipeline register
  signal int_array : integrate_array_t;

  -- Integrator valid register
  signal int_delay : std_logic_vector(int_stages_g - 1 downto 0);

  -- Comb valid registers
  signal comb_delay     : std_logic_vector(comb_stages_g - 1 downto 0);
  signal comb_out_valid : std_logic;

  -- Comb pipeline registers
  signal comb_array  : comb_array_t;
  signal delay_array : diff_delay_t;
  signal comb_valid  : std_logic_vector(comb_stages_g downto 0);

  -- Comb data out registers
  signal comb_data : signed(output_word_size_g - 1 downto 0);

  -- Upsampler signals
  signal counter               : unsigned(int_factor_size_g - 1 downto 0);
  signal up_sampled_data       : signed(output_word_size_g - 1 downto 0);
  signal up_sampled_data_valid : std_logic;
  signal input_freeze          : std_logic;

  type state_t is (go_s, up_sample_s);
  signal current_state : state_t;

begin

  -- Check CIC settings are not set to 0
  assert diff_delay_g /= 0 report "diff_delay_g must be greater than 0" severity failure;
  assert int_stages_g /= 0 report "int_stages_g must be greater than 0" severity failure;
  assert comb_stages_g /= 0 report "comb_stages_g must be greater than 0" severity failure;

  -----------------------------------------------------------------------------
  -- Comb stage
  -----------------------------------------------------------------------------
  comb_stage_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        comb_array <= (others => (others => '0'));
        comb_delay <= (others => '0');
      elsif (clk_en = '1' and input_freeze = '0') then
        -- comb_delay marks which stages have valid inputs
        comb_delay <= comb_delay(comb_stages_g - 2 downto 0) & data_valid_in;
        -- Update first comb stage
        if (data_valid_in = '1') then
          comb_array(0) <= resize(data_in, output_word_size_g) - delay_array(diff_delay_g - 1)(0);
        end if;
        -- Update the remaining comb stages
        for gen_var in 1 to comb_stages_g - 1 loop
          if comb_delay(gen_var - 1) = '1' then
            comb_array(gen_var) <= comb_array(gen_var - 1) - delay_array(diff_delay_g - 1)(gen_var);
          end if;
        end loop;

      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Comb delay register
  -----------------------------------------------------------------------------
  -- Creates a delayed version of the input to each comb stage
  delay_gen : if diff_delay_g = 1 generate
    comb_delay_reg_p : process(clk)
    begin
      if rising_edge(clk) then
        if (reset = '1') then
          delay_array(0) <= (others => (others => '0'));
        elsif (clk_en = '1' and input_freeze = '0') then
          -- If input to first comb is valid, then register the input
          if data_valid_in = '1' then
            delay_array(0)(0) <= resize(data_in, output_word_size_g);
          end if;
          -- For each comb stage, if input to current stage is valid,
          -- then register the last input to the current stage.
          for gen_var in 1 to comb_stages_g - 1 loop
            if comb_delay(gen_var - 1) = '1' then
              delay_array(0)(gen_var) <= comb_array(gen_var - 1);
            end if;
          end loop;
        end if;
      end if;
    end process;
  end generate;

  -----------------------------------------------------------------------------
  -- Comb delay pipeline (when differential delay is > 1)
  -----------------------------------------------------------------------------
  -- If the differential delay is greater than 1 then add additional delay
  -- stages for the inputs to each CIC comb stage

  -- comb_valid vector stores which of the inputs to each of the comb stages
  -- are valid. e.g. comb_valid.
  comb_valid <= comb_delay & data_valid_in;

  delay_pipe_gen : if diff_delay_g > 1 generate
    comb_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if (reset = '1') then
          delay_array <= (others => (others => (others => '0')));
        elsif (clk_en = '1' and input_freeze = '0') then
          -- If input to first comb is valid, then register the input
          if data_valid_in = '1' then
            delay_array(0)(0) <= resize(data_in, output_word_size_g);
          end if;
          -- For each comb stage, if input to current stage is valid,
          -- then register the last input to the current stage.
          for gen_var in 1 to comb_stages_g - 1 loop
            if comb_delay(gen_var - 1) = '1' then
              delay_array(0)(gen_var) <= comb_array(gen_var - 1);
            end if;
          end loop;
          -- For each extra delay register, register the previous comb stage
          -- inputs one more time
          for delay_gen_var in 1 to diff_delay_g - 1 loop
            -- For each comb stage, if input to current stage is valid,
            -- then register the last input to the current stage.
            for gen_var in 0 to comb_stages_g - 1 loop
              if comb_valid(gen_var) = '1' then
                delay_array(delay_gen_var)(gen_var) <= delay_array(delay_gen_var - 1)(gen_var);
              end if;
            end loop;
          end loop;
        end if;
      end if;
    end process;
  end generate;

  comb_out_valid <= comb_delay(comb_stages_g - 1);
  comb_data      <= comb_array(comb_stages_g - 1);

  -----------------------------------------------------------------------------
  -- Up sample stage
  -----------------------------------------------------------------------------
  interpolate_stage_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        current_state <= go_s;
      elsif (clk_en = '1') then
        case current_state is
          when go_s =>
            if comb_out_valid = '1' and up_sample_factor /= 1 then
              current_state <= up_sample_s;
              counter       <= up_sample_factor - 2;
            end if;
          when up_sample_s =>
            counter <= counter - 1;
            if counter = 0 then
              current_state <= go_s;
            end if;
        end case;
      end if;
    end if;
  end process;

  -- Zero pad samples
  up_sampled_data       <= comb_data when current_state = go_s                                else (others => '0');
  up_sampled_data_valid <= '1'       when comb_out_valid = '1' or current_state = up_sample_s else '0';
  input_freeze          <= '1'       when current_state = up_sample_s                         else '0';
  input_hold            <= input_freeze;

  -----------------------------------------------------------------------------
  -- Integrator stage
  -----------------------------------------------------------------------------
  integrate_stage_p : process(clk)
  begin
    if rising_edge(clk) then
      if (reset = '1') then
        int_array <= (others => (others => '0'));
        int_delay <= (others => '0');
      elsif (clk_en = '1') then
        -- Track which samples are valid in the integration pipeline
        int_delay <= int_delay(int_stages_g - 2 downto 0) & up_sampled_data_valid;
        -- Shift new samples into the integration pipeline
        if (up_sampled_data_valid = '1') then
          int_array(0) <= up_sampled_data + int_array(0);
        end if;
        for gen_var in 1 to int_stages_g - 1 loop
          -- Advance the pipeline if the previous integration stage is done
          if int_delay(gen_var - 1) = '1' then
            int_array(gen_var) <= int_array(gen_var - 1) + int_array(gen_var);
          end if;
        end loop;
      end if;
    end if;
  end process;

  data_valid_out <= int_delay(int_stages_g - 1);
  data_out       <= int_array(int_stages_g - 1);

end rtl;
