-- HDL Implementation of a CORDIC based carrier tone generator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- Local libraries
library work;
use work.sdr_dsp.cordic_sin_cos;

entity cordic_dds is

  generic (
    -- Output bit size (Max. 32 bits - Unless lookup table changed)
    output_size_g      : integer := 16;
    -- Phase Accumulator Size (Min. 32 bits - Unless lookup table changed)
    phase_accum_size_g : integer := 32
    );

  port (
    clk        : in  std_logic;
    rst        : in  std_logic;
    clk_en     : in  std_logic;
    step_size  : in  std_logic_vector(phase_accum_size_g - 1 downto 0);
    gain_in    : in  std_logic_vector(output_size_g - 1 downto 0);
    sine_out   : out std_logic_vector(output_size_g - 1 downto 0);
    cosine_out : out std_logic_vector(output_size_g - 1 downto 0)
    );
end cordic_dds;


architecture behavioral of cordic_dds is

  signal phase_accum : std_logic_vector(phase_accum_size_g - 1 downto 0);
  signal gain        : std_logic_vector(output_size_g - 1 downto 0);

begin

-- The top 32 bits of the phase accumulator are used as the angle to the
-- cordic_sin_cos primative's angle_in input.
  cordic_module : cordic_sin_cos
    generic map (
      output_size_g => output_size_g
      )
    port map (
      clk        => clk,
      clk_en     => clk_en,
      rst        => rst,
      angle_in   => phase_accum(phase_accum_size_g - 1 downto phase_accum_size_g - 32),
      gain_in    => gain,
      sine_out   => sine_out,
      cosine_out => cosine_out
      );


-- Adds dds_step_size to the previous phase accumulator value. Phase
-- accumulator register wraps on overflow.
  phase_calc : process(clk, rst)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        phase_accum <= (others => '0');
        gain        <= (others => '0');
      elsif (clk_en = '1') then
        gain        <= gain_in;
        phase_accum <= std_logic_vector(signed(phase_accum) + signed(step_size));
      end if;
    end if;

  end process phase_calc;

end behavioral;
