-- HDL Implementation of a CORDIC based rectangular to polar converter.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- Outputs ATAN2(Y/X) and sqrt(x^2+y^2) of the input Y and X values.
entity cordic_rec_to_pol is

  generic (
    input_size_g  : integer := 16;      -- Input bit width
    output_size_g : integer := 16   -- Maximum = 32 bits. (Or LUT depth + 1)
    );

  port (
    clk           : in  std_logic;
    rst           : in  std_logic;
    enable        : in  std_logic;
    valid_in      : in  std_logic;
    -- X input to ATAN2(Y/X) function (Signed)
    x_in          : in  signed(input_size_g - 1 downto 0);
    -- Y input to ATAN2(Y/X) function (Signed)
    y_in          : in  signed(input_size_g - 1 downto 0);
    valid_out     : out std_logic;
    -- angle_out = ATAN2(y_in/x_in) * ((2^(output_size_g-1) - 1)/pi)
    angle_out     : out signed(output_size_g - 1 downto 0);
    -- magnitude_out = cordic_gain * SQRT(x_in^2 + y_in^2)
    magnitude_out : out signed(input_size_g + 1 downto 0)
    );
end cordic_rec_to_pol;

architecture rtl of cordic_rec_to_pol is

  -----------------------------------------------------------------------------
  -- Define Cordic look up table (LUT)
  -----------------------------------------------------------------------------
  -- Total LUT size = 31*32 = 992bits = 124 bytes;
  constant LUT_depth_c : integer := 31;
  constant LUT_width_c : integer := 32;

  type LUT_t is array (0 to LUT_depth_c - 1) of signed(LUT_width_c - 1 downto 0);

  -- CORDIC Look up table. Generated using:
  -- round(ATAN(2^(-i)) * ((2^(31))-1)/180), i = 0,1,...,(LUT_depth_c - 1)
  constant cordic_lut_c : LUT_t := (
    X"20000000", X"12E4051D", X"09FB385B", X"051111D4", X"028B0D43", X"0145D7E1",
    X"00A2F61E", X"00517C55", X"0028BE53", X"00145F2F", X"000A2F98", X"000517CC",
    X"00028BE6", X"000145F3", X"0000A2FA", X"0000517D", X"000028BE", X"0000145F",
    X"00000A30", X"00000518", X"0000028C", X"00000146", X"000000A3", X"00000051",
    X"00000029", X"00000014", X"0000000A", X"00000005", X"00000003", X"00000001",
    X"00000000"
    );

  -- PI Constant
  -- Positive PI is set to be +(2^(output_size_g - 1)) - 1
  -- Negative PI is set to be -(2^(output_size_g - 1)) - 1
  constant pi_const : signed := to_signed(2**(output_size_g - 1) - 1, output_size_g);

  -- Input signals
  signal valid_reg : std_logic_vector(output_size_g - 1 downto 0);

  -- Quadrant signals
  -- Contains the quadrant setting for the input IQ sample.
  signal quadrant_input : std_logic_vector(1 downto 0);
  type quadrant_array_t is array (0 to output_size_g - 1) of std_logic_vector(1 downto 0);
  -- Register to store quadrant settings for all stages of the pipeline.
  signal quadrant       : quadrant_array_t;

  -- Cordic pipeline signals
  -- Initial values for x, y, and z inputs to the cordic algorithm
  signal initial_x : signed(input_size_g + 1 downto 0);
  signal initial_y : signed(input_size_g + 1 downto 0);
  signal initial_z : signed(LUT_width_c - 1 downto 0);

  -- Stores x and y values for each pipeline stage of the cordic algorithm
  -- X and Y must be 2 bits wider than input_size_g
  type pipeline_array_t is array (0 to output_size_g - 1)
    of signed(input_size_g + 1 downto 0);

  -- Stores pipeline state for CORDIC variable X and Y
  signal X : pipeline_array_t := (others => (others => '0'));
  signal Y : pipeline_array_t := (others => (others => '0'));

  -- Stores z values for each pipeline stage of the cordic algorithm.
  -- must be the same width as the lookup table
  type pipeline_array_z_t is array (0 to output_size_g - 1)
    of signed(LUT_width_c - 1 downto 0);

  -- Stores pipeline state for CORDIC variable Z
  signal Z : pipeline_array_z_t := (others => (others => '0'));

begin

  -----------------------------------------------------------------------------
  -- Input shift register
  -- Shift register stores whether the input was valid for each
  -- stage of the pipeline. This is used to set the output valid signal.
  ----------------------------------------------------------------------------
  valid_in_shift_reg_p : process(clk)
  begin

    if rising_edge(clk) then
      if (rst = '1') then
        valid_reg <= (others => '0');
      else
        if (enable = '1') then
          valid_reg <= valid_reg(valid_reg'length - 2 downto 0) & valid_in;
        end if;
      end if;
    end if;

  end process;

  -----------------------------------------------------------------------------
  -- Input quadrant calculator.
  -- Calculate which quadrant the input is in so that the output of
  -- the atan calculation can be converted to atan2.
  -- quadrant_input = 00 when x=0, y=0 (error state)
  -- quadrant_input = 01 when x>=0 (atan2 = atan)
  -- quadrant_input = 10 when x<0, y>=0 (atan2 = -atan + pi)
  -- quadrant_input = 11 when x<0, y<0  (atan2 = -atan - pi)
  -----------------------------------------------------------------------------
  input_quadrant_p : process(x_in, y_in)
  begin
    if (x_in = 0 and y_in = 0) then
      quadrant_input <= "00";
    elsif(x_in >= 0) then
      quadrant_input <= "01";
    elsif (x_in < 0 and y_in >= 0) then
      quadrant_input <= "10";
    elsif (x_in < 0 and y_in < 0) then
      quadrant_input <= "11";
    else                                -- This will never happen
      quadrant_input <= "00";
    end if;
  end process;

  ----------------------------------------------------------------------------
  -- Input quadrant shift register.
  -- Shift register stores the quadrant value for each stage of the
  -- pipeline.
  ----------------------------------------------------------------------------
  quadrant_shift_reg_p : process(clk)
  begin

    if rising_edge(clk) then
      if (rst = '1') then
        quadrant <= (others => (others => '0'));
      else
        if (enable = '1') then
          -- Connect input to beginning of shift register
          quadrant(0) <= quadrant_input;
          -- Advance the shift register
          generate_pipeline : for quad_gen_var in 0 to output_size_g - 2 loop
            quadrant(quad_gen_var + 1) <= quadrant(quad_gen_var);
          end loop generate_pipeline;
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Input polarity adjustment.
  -- The CORDIC algorithm can only output atan values between
  -- -90 and +90 degrees. This means that the x input to the algorithm must
  -- be a +ve number as when x>=0, 90<=atan(y/x)<=90. The offset is
  -- corrected at the end of the calculation using the quadrant information
  -- saved above.
  -----------------------------------------------------------------------------
  set_inital_cordic_values_p : process(x_in, y_in)
  begin

    -- Max size of X is cordic_gain * sqrt(x^2 + y^2)
    -- Assuming X and Y are full scale then worst case gain
    -- is sqrt(2) * cordic_gain.
    -- cordic_gain tends to ~1.64676 if the number of iterations is >15
    -- Giving a total worst case gain of ~2.39. Expanding x by 2 bits
    -- prevents overflows due to this gain.
    if (x_in < 0) then
      initial_x <= resize(-x_in, input_size_g + 2);
    else
      initial_x <= resize(x_in, input_size_g + 2);
    end if;

    -- Resize Y to same width as X
    initial_y <= resize(y_in, input_size_g + 2);
    initial_z <= (others => '0');

  end process;

  -----------------------------------------------------------------------------
  -- Pipelined CORDIC algorithm
  -- This process generates the pipelined stages of the cordic algorithm
  -- in vectoring mode. See the Microchip Application note AN1061 for
  -- more details on the cordic algorithm.
  -----------------------------------------------------------------------------
  pipelined_cordic_p : process(clk)
  begin

    if rising_edge(clk) then

      if (rst = '1') then
        -- Clear the whole pipeline on reset
        for gen_var in 0 to (output_size_g - 1) loop
          X(gen_var) <= (others => '0');
          Y(gen_var) <= (others => '0');
          Z(gen_var) <= (others => '0');
        end loop;
      else
        if enable = '1' then
          -- Register initial values
          X(0) <= initial_x;
          Y(0) <= initial_y;
          Z(0) <= initial_z;

          -- Generate the pipeline stages for the cordic algorithm in
          -- vectoring mode
          generate_pipeline : for gen_var in 0 to output_size_g - 2 loop
            if (Y(gen_var) < 0) then
              X(gen_var + 1) <= X(gen_var) - (shift_right((Y(gen_var)), gen_var));
              Y(gen_var + 1) <= Y(gen_var) + (shift_right((X(gen_var)), gen_var));
              Z(gen_var + 1) <= Z(gen_var) - cordic_lut_c(gen_var);
            else
              X(gen_var + 1) <= X(gen_var) + (shift_right((Y(gen_var)), gen_var));
              Y(gen_var + 1) <= Y(gen_var) - (shift_right((X(gen_var)), gen_var));
              Z(gen_var + 1) <= Z(gen_var) + cordic_lut_c(gen_var);
            end if;
          end loop generate_pipeline;
        end if;
      end if;
    end if;

  end process;

  -----------------------------------------------------------------------------
  -- Output quadrant correction
  -- Adjusts the output of the CORDIC ATAN result to give ATAN2
  -- quadrant_input = 00 when x=0, y=0 (error state)
  -- quadrant_input = 01 when x>=0 (atan2 = atan)
  -- quadrant_input = 10 when x<0, y>=0 (atan2 = -atan + pi)
  -- quadrant_input = 11 when x<0, y<0  (atan2 = -atan - pi)
  -----------------------------------------------------------------------------
  output_quadrant_correction_p : process(clk)
  begin

    if rising_edge(clk) then
      if (rst = '1') then
        angle_out     <= (others => '0');
        magnitude_out <= (others => '0');
        valid_out     <= '0';
      else
        if enable = '1' then

          if quadrant(output_size_g - 1) = "00" then
            -- This is an error state. Output 0
            angle_out <= (others => '0');
          elsif quadrant(output_size_g - 1) = "01" then
            angle_out <= Z(output_size_g - 1)(LUT_width_c - 1 downto LUT_width_c - output_size_g);
          elsif quadrant(output_size_g - 1) = "10" then
            angle_out <= -(Z(output_size_g - 1)(LUT_width_c - 1 downto LUT_width_c - output_size_g)) + pi_const;
          elsif quadrant(output_size_g - 1) = "11" then
            angle_out <= -(Z(output_size_g - 1)(LUT_width_c - 1 downto LUT_width_c - output_size_g)) - pi_const;
          else
            -- Set output to 0 if quadrant == "UU", "XX" etc.
            angle_out <= (others => '0');
          end if;

          magnitude_out <= X(output_size_g - 1);
          valid_out     <= valid_reg(output_size_g - 1);
        end if;

      end if;
    end if;

  end process;

end rtl;
