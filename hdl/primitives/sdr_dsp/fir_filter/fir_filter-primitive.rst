.. fir_filter documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fir_filter-primitive:


Finite impulse response (FIR) filter (``fir_filter``)
=====================================================
FIR filter primitive with flexible resource usage.

Design
------
Implements an FIR filter with a compile time definable number of taps.

The mathematical representation of the implementation is given in :eq:`fir_filter-equation`.

.. math::
   :label: fir_filter-equation

   y[n] = \sum_{i=0}^{T-1} b_i * x[n-i]

In :eq:`fir_filter-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`b_i` is the taps values, where there are :math:`T` taps index from :math:`0` to :math:`T - 1`.

A block diagram representation of the implementation:

.. _fir_filter-diagram:

.. figure:: fir_filter.svg
   :alt: Block diagram of finite impulse response (FIR) filter implementation
   :align: center

   FIR filter implementation. :math:`b_i` is the taps values. :math:`x[n]` is the input data. :math:`y[n]` is the output data.

Implementation
--------------
The primitive is designed to work with 16 bit taps. The input data width is customisable at compile / build time. Output data width is automatically set set based on the input width and the number of taps.

The number of multiply accumulate (MAC) units used to implement the filter is definable using a VHDL generic allowing the resource usage to be customised depending on the number of DSPs available and the required data rate. The minimum number of clock cycles required between each input is given by equation :eq:`fir_filter_primitive_stages-equation`. This FIR filter design is most suited for when ``num_multipliers_g`` is small compared to ``num_taps_g``, however it will operate with any combination  of ``num_multipliers_g`` and ``num_taps_g``.

.. math::
   :label: fir_filter_primitive_stages-equation

   \text{clock_cycles_between_inputs} = \left \lceil \frac{\texttt{num_taps_g}}{\texttt{num_multipliers_g}} \right \rceil

The maximum data rate the FIR filter can operate at is given by equation :eq:`fir_filter_primitive_clock_rate-equation`.

.. math::
   :label: fir_filter_primitive_clock_rate-equation

   \text{filter_data_rate} = \frac{\text{fpga_clock_rate}}{\left \lceil \frac{\texttt{num_taps_g}}{\texttt{num_multipliers_g}} \right \rceil}

Each MAC unit is used to perform part the FIR filter operation. The results of each of those MACs then need to be added together to get the final filter output. If :math:`\text{clock_cycles_between_inputs}` found in :eq:`fir_filter_primitive_stages-equation` is greater than one, then part of this summation operation can be time shared over multiple clock cycles. In order to take advantage of this an array of serial adders are instantiated to perform the summation. The number of serial adders required is given by equation :eq:`fir_filter_primitive_serial_adders-equation`.

.. math::
   :label: fir_filter_primitive_serial_adders-equation

   \text{number_of_serial_adders} = \left \lceil \frac{\texttt{num_multipliers_g}}{\left \lceil \frac{\texttt{num_taps_g}}{\texttt{num_multipliers_g}} \right \rceil} \right \rceil

If :math:`\text{number_of_serial_adders}` from :eq:`fir_filter_primitive_serial_adders-equation` is greater than one, then the results of the serial adders also need to be summed to get the final filter result. This final operation is done using a pipelined adder tree. A pipelined adder tree is typically resource intensive requiring instantiation of :math:`\text{number_of_serial_adders} - 1` adders. If equation :eq:`fir_filter_primitive_serial_adders-equation` is large, then other HDL FIR implementations such as a systolic FIR filter may be more suitable.

Interface
---------

Generics
~~~~~~~~

 * ``data_in_width_g`` (``integer``): Bit width of input signal.

 * ``num_multipliers_g`` (``integer``): Sets the number of multipliers to split the FIR filter calculations over. Must be less than or equal to ``num_taps_g``.

 * ``num_taps_g`` (``integer``): Sets the number of taps the FIR filter has.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Clock enable for module. Module is enabled when ``enable`` is high.

 * ``data_valid_in`` (``std_logic``), in: ``data_in`` is valid when ``data_valid_in`` is high.

 * ``take`` (``std_logic``), out: High when the FIR filter is able to take data from its input.

 * ``data_in`` (``signed``, ``data_in_width_g`` bits), in: Input data to the FIR filter.

 * ``taps_in`` (``signed``, ``array: 16 bit x num_taps_g``), in : The array of taps used by the FIR filter.

 * ``data_out`` (``signed``, ``data_in_width_g + 16 + ceil(log2(num_taps_g))`` bits), out: Output data from the FIR filter.

 * ``data_valid_out`` (``std_logic``), out: ``data_out`` is valid when ``data_valid_out`` is high.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``fir_filter`` are:

 * Does not take advantage of the FPGA resource savings possible when the FIR taps are symmetrical.

 * The primitive uses a pipelined adder tree when :math:`\text{number_of_serial_adders}` from :eq:`fir_filter_primitive_serial_adders-equation` is greater than one. If :math:`\text{number_of_serial_adders} > 32` then other FIR filter implementations are more suitable as the adder tree will use significant FPGA resources.
