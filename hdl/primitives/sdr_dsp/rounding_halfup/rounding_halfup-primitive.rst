.. rounding_halfup documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _rounding_halfup-primitive:


Half-up rounder (``rounding_halfup``)
=====================================
Half-up rounding and truncation for a variable width signed input.

Design
------
In the half-up rounder the input stream is taken in and depending on the value of the binary point (most-significant bit to be truncated) value:

 * If binary point is zero, the input is just truncated to the output width.

 * If binary point is one, the input is right shifted by the number of places to be truncated less one, one is added, and then the input is right shifted by one. The number is then truncated to the output width. The result of this operation is that any value :math:`\geq x.5` will round up to :math:`x+1` and any value :math:`< x.5` will round down to :math:`x`.

The mathematical representation of the implementation is given in :eq:`rounding_halfup-equation`.

.. math::
   :label: rounding_halfup-equation

   y[n] = \left \lfloor \frac{\left \lfloor \frac{x[n]}{2^{binary\_point-1}} \right \rfloor + 1.0}{2} \right \rfloor

In :eq:`rounding_halfup-equation`:

 * :math:`y[n]` is the output values.

 * :math:`x[n]` is the input values.

Implementation
--------------
The primitive is pipelined so that a new sample can be inserted into the primitive on every rising edge of ``clk``. The ``data_valid_in`` signal allows each input sample to be marked as valid or not. The pipeline advances every rising edge of ``clk`` regardless of if valid data is passed to the primitive or not. The ``data_valid_out`` signal is a delayed version of ``data_valid_in`` that indicates which outputs were calculated based on valid inputs.

The ``enable`` signal provides a clock enable for the primitive. When ``enable`` is set low no data will enter or leave the primitive and all calculations are stopped. When ``enable`` is high the module will operate normally.

Interface
---------

Generics
~~~~~~~~

 * ``input_width_g`` (``integer``): Sets width of ``data_in`` signal.

 * ``output_width_g`` (``integer``): Sets width of ``data_out`` signal.


Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_enable`` (``std_logic``), in: Enable. If ``enable`` is low the module will not operate.

 * ``data_in`` (``signed``, ``input_width_g`` bits), in: Input value for primitive.

 * ``data_valid_in`` (``std_logic``), in: ``data_in`` is valid when ``data_valid_in`` is high.

 * ``binary_point`` (``integer``), in: Sets number of times the input data is right shifted before being output.

 * ``data_out`` (``signed``, ``output_width_g`` bits), out: Output value for primitive.

 * ``data_valid_out`` (``std_logic``), out: ``data_out``  is valid when ``data_valid_out`` is high.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``rounding_halfup`` are:

 * A small positive bias will be introduced when using this primitive. If this causes an issue use convergent rounding instead.
