-- DSP Primitive Package
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This package enables VHDL code to instantiate all entities and modules
-- in this library.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.fir_signed_array_pkg.all;

package sdr_dsp is

  component cic_dec
    generic (
      int_stages_g       : integer := 3;
      comb_stages_g      : integer := 3;
      diff_delay_g       : integer := 2;
      input_word_size_g  : integer := 16;
      output_word_size_g : integer := 36;
      dec_factor_size_g  : integer := 16
      );
    port (
      clk                : in  std_logic;
      reset              : in  std_logic;
      clk_en             : in  std_logic;
      data_valid_in      : in  std_logic;
      data_in            : in  signed(input_word_size_g - 1 downto 0);
      down_sample_factor : in  unsigned(dec_factor_size_g - 1 downto 0);
      data_valid_out     : out std_logic;
      data_out           : out signed(output_word_size_g - 1 downto 0)
      );
  end component;

  component cic_int
    generic (
      int_stages_g       : integer := 3;
      comb_stages_g      : integer := 3;
      diff_delay_g       : integer := 2;
      input_word_size_g  : integer := 16;
      output_word_size_g : integer := 36;
      int_factor_size_g  : integer := 16
      );
    port (
      clk              : in  std_logic;
      reset            : in  std_logic;
      clk_en           : in  std_logic;
      data_valid_in    : in  std_logic;
      data_in          : in  signed(input_word_size_g - 1 downto 0);
      up_sample_factor : in  unsigned(int_factor_size_g - 1 downto 0);
      input_hold       : out std_logic;
      data_valid_out   : out std_logic;
      data_out         : out signed(output_word_size_g - 1 downto 0)
      );
  end component;

  component cordic_dds
    generic (
      output_size_g      : integer := 16;
      phase_accum_size_g : integer := 32
      );
    port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      clk_en     : in  std_logic;
      step_size  : in  std_logic_vector(phase_accum_size_g - 1 downto 0);
      gain_in    : in  std_logic_vector(output_size_g - 1 downto 0);
      sine_out   : out std_logic_vector(output_size_g - 1 downto 0);
      cosine_out : out std_logic_vector(output_size_g - 1 downto 0)
      );
  end component;

  component cordic_rec_to_pol

    generic (
      input_size_g  : integer := 16;
      output_size_g : integer := 16
      );
    port (
      clk           : in  std_logic;
      rst           : in  std_logic;
      enable        : in  std_logic;
      valid_in      : in  std_logic;
      x_in          : in  signed(input_size_g - 1 downto 0);
      y_in          : in  signed(input_size_g - 1 downto 0);
      valid_out     : out std_logic;
      angle_out     : out signed(output_size_g - 1 downto 0);
      magnitude_out : out signed(input_size_g + 1 downto 0)
      );
  end component;

  component cordic_sin_cos
    generic (
      input_size_g  : integer := 32;
      output_size_g : integer := 16
      );
    port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      clk_en     : in  std_logic;
      angle_in   : in  std_logic_vector(input_size_g - 1 downto 0);
      gain_in    : in  std_logic_vector(output_size_g - 1 downto 0);
      sine_out   : out std_logic_vector(output_size_g - 1 downto 0);
      cosine_out : out std_logic_vector(output_size_g - 1 downto 0)
      );
  end component;

  component multiply_accumulate
    generic (
      a_in_size        : integer := 16;
      b_in_size        : integer := 16;
      accumulator_size : integer := 48
      );
    port (
      clk        : in  std_logic;
      rst        : in  std_logic;
      en_in      : in  std_logic;
      a_in       : in  signed(a_in_size - 1 downto 0);
      b_in       : in  signed(b_in_size - 1 downto 0);
      load_accum : in  std_logic;
      accum_in   : in  signed(accumulator_size - 1 downto 0);
      accum_out  : out signed(accumulator_size - 1 downto 0)
      );
  end component;

  component rounding_halfup
    generic (
      input_width_g  : integer := 32;
      output_width_g : integer := 16
      );
    port (
      clk            : in  std_logic;
      reset          : in  std_logic;
      clk_en         : in  std_logic;
      data_in        : in  signed(input_width_g - 1 downto 0);
      data_valid_in  : in  std_logic;
      binary_point   : in  integer range 0 to input_width_g - 1;
      data_out       : out signed(output_width_g - 1 downto 0);
      data_valid_out : out std_logic
      );
  end component;

  component fir_filter
    generic (
      data_in_width_g   : integer := 16;
      num_multipliers_g : integer := 16;
      num_taps_g        : integer := 2
      );
    port (
      clk            : in  std_logic;
      rst            : in  std_logic;
      enable         : in  std_logic;
      data_in_valid  : in  std_logic;
      take           : out std_logic;
      data_in        : in  signed(data_in_width_g - 1 downto 0);
      taps_in        : in  fir_signed_array(0 to num_taps_g - 1);
      data_out       : out signed(data_in_width_g + pkg_coefficient_width + integer(ceil(log2(real(num_taps_g)))) - 1 downto 0);
      data_valid_out : out std_logic
      );
  end component;

  component windower
    generic (
      data_width_g  : integer := 16;
      coeff_width_g : integer := 33
      );
    port (
      clk                : in  std_logic;
      reset              : in  std_logic;
      enable             : in  std_logic;
      coeff_in           : in  signed(coeff_width_g-1 downto 0);
      data_in            : in  signed(data_width_g-1 downto 0);
      data_in_valid_in   : in  std_logic;
      data_out           : out signed(data_width_g-1 downto 0);
      data_out_valid_out : out std_logic
      );
  end component;

end package sdr_dsp;
