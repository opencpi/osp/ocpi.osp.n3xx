-- Logarithm package.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package log_pkg is
  -- array of real log table values
  type log_table_array_t is array (natural range <>) of real;
  -- initialise log table using base value and bit size
  function log_table_init(base_val : in real; bit_size : in integer)
    return log_table_array_t;
end package log_pkg;

package body log_pkg is
  -- initialise log table using base value and bit size
  function log_table_init(base_val : in real; bit_size : in integer)
    return log_table_array_t is
    variable i     : integer;
    variable j     : real;
    variable table : log_table_array_t(0 to integer(log2(real(bit_size))) + 8);
  begin
    for i in 1 to 7 loop
      table(table'length - i) := log2(1.0+2.0**(i-8)) / log2(real(base_val)) *
                                 2.0**16;
    end loop;
    i := table'length - 8;
    j := 2.0;
    while i > 0 loop
      table(i) := log2(j) / log2(real(base_val)) * 2.0**16;
      i        := i - 1;
      j        := j**2;
    end loop;
    table(0) := real(bit_size - 1) / log2(real(base_val)) * 2.0**16;
    return table;
  end function;
end package body log_pkg;
