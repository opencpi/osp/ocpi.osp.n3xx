-- HDL Implementation of a windower function
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library sdr_dsp;

entity windower is
  generic (
    data_width_g  : integer := 16;
    coeff_width_g : integer := 33
    );
  port (
    clk                : in  std_logic;
    reset              : in  std_logic;
    enable             : in  std_logic;
    coeff_in           : in  signed(coeff_width_g-1 downto 0);
    data_in            : in  signed(data_width_g-1 downto 0);
    data_in_valid_in   : in  std_logic;
    data_out           : out signed(data_width_g-1 downto 0);
    data_out_valid_out : out std_logic
    );
end windower;

architecture rtl of windower is

  signal data       : signed(data_width_g+coeff_width_g-1 downto 0);
  signal data_valid : std_logic;

begin

  ------------------------------------------------------------------------------
  -- multiplier Process
  ------------------------------------------------------------------------------
  -- Process that multiplies the window coefficient with input data.
  ------------------------------------------------------------------------------
  multiplier : process (clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        data       <= (others => '0');
        data_valid <= '0';
      elsif enable = '1' then
        data       <= data_in * coeff_in;
        data_valid <= data_in_valid_in;
      end if;
    end if;
  end process multiplier;

  ------------------------------------------------------------------------------
  -- Round half up primitive
  ------------------------------------------------------------------------------
  round_half_up_i : sdr_dsp.sdr_dsp.rounding_halfup
    generic map (
      input_width_g  => data_width_g+coeff_width_g,
      output_width_g => data_width_g
      )
    port map (
      clk            => clk,
      reset          => reset,
      clk_en         => enable,
      data_in        => data,
      data_valid_in  => data_valid,
      binary_point   => coeff_width_g-1,
      data_out       => data_out,
      data_valid_out => data_out_valid_out
      );

end rtl;
