.. flush_inserter_v2 documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _flush_inserter_v2-primitive:


Flush inserter for V2 HDL interface (``flush_inserter_v2``)
===========================================================
Inserts a number of zero data samples when a flush message is received.
``flush_inserter_v2`` is the same as ``flush_inserter``, but has been upgraded to support the OpenCPI version 2 interface standard.

Design
------
Inserts a configurable number of data samples, with the value zero, when a message with the flush opcode is received. The message with a flush opcode is then forwarded on.

Implementation
--------------
When a message with a flush opcode is received, back pressure is applied to the input interface. A state machine is used to clock out a data message with ``flush_length`` samples with a value of zero. If ``flush_length`` is greater than ``max_message_length_g`` then the flush message is split into messages with a maximum length of ``max_message_length_g``. Input back pressure is then removed and the message with a flush opcode passes onwards.

The component adds no delay to normal interface messages.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g`` (``integer``): Sets the width of the interface data signal.

 * ``opcode_width_g`` (``integer``): Sets the width of the interface opcode signal.

 * ``byte_enable_width_g`` (``integer``): Sets the width of the interface byte enable signal.

 * ``max_message_length_g`` (``integer``): Sets the maximum length of a data message. If ``flush_length`` is greater than this then the message is split into messages with a maximum length of ``max_message_length_g``.

 * ``flush_opcode_g`` (``std_logic_vector``): Sets the value of the flush opcode.

 * ``data_opcode_g`` (``std_logic_vector``): Sets the value of the data opcode.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``flush_length`` (``unsigned``), in: Number of zeros that should be inserted when a flush is received.

 * ``take_in`` (``std_logic``), in: High when data is taken from the input interface.

 * ``take_out`` (``std_logic``), out: Signal should be connected to input interface take output.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_eof`` (``std_logic``), in: EOF signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``data_width_g`` bits), in: Data signal from OpenCPI interface.

 * ``input_ready`` (``std_logic``), in: Ready signal from OpenCPI interface.

 * ``output_som`` (``std_logic``), out: Output of SOM signal from input interface with flush inserted.

 * ``output_eom`` (``std_logic``), out: Output of EOM signal from input interface with flush inserted.

 * ``output_eof`` (``std_logic``), out: Output of EOF signal from input interface with flush inserted.

 * ``output_valid`` (``std_logic``), out: Output of valid signal from input interface with flush inserted.

 * ``output_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), out: Output of byte enable signal from input interface with flush inserted.

 * ``output_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), out: Output of opcode signal from input interface with flush inserted.

 * ``output_data`` (``std_logic_vector``, ``data_width_g`` bits), out: Output of data signal from input interface with flush inserted.

 * ``output_ready`` (``std_logic``), out: Output of ready signal from input interface with flush inserted.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``flush_inserter_v2`` are:

 * None.
