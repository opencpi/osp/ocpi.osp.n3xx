.. message_length_limiter documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _message_length_limiter-primitive:


Message length limiter (``message_length_limiter``)
===================================================
Splits OpenCPI interface messages that have a sequence length longer than a configurable value.

Design
------
When a sequence is specified in an OpenCPI protocol, the maximum length of the sequence is also specified. An HDL worker should not output a message longer than than this maximum length. This primitive is designed to split messages that exceed this maximum length. It is typically used in components that upsample or interpolate an input message.

Implementation
--------------
A counter is used to track the length of each output message. If a message with opcode value ``LIMIT_OPCODE_G`` exceeds a length of ``MAX_MESSAGE_LENGTH_G`` words, then an end of message signal is inserted so that the message length is exactly ``MAX_MESSAGE_LENGTH_G`` words long. A start of message signal is then inserted and the message length counter is reset. This process repeats until the original end of message signal is detected. The message is then ended and the counter is reset. For example, if an output message is 5120 words long and ``MAX_MESSAGE_LENGTH_G`` is 2048, then two messages of length 2048 will be output, followed by one message of length 1024.

The primitive causes a single clock cycle delay between the unlimited and limited output interfaces.

Interface
---------

Generics
~~~~~~~~

 * ``DATA_WIDTH_G`` (``integer``): Sets the width of the interface data signal.

 * ``OPCODE_WIDTH_G`` (``integer``): Sets the width of the interface opcode signal.

 * ``BYTE_ENABLE_WIDTH_G`` (``integer``): Sets the width of the interface byte enable signal.

 * ``MAX_MESSAGE_LENGTH_G`` (``integer``): Sets the maximum length of a message before it should be split.

 * ``LIMIT_OPCODE_G`` (``std_logic_vector``): Sets the value of the opcode for which the message length limiting should be applied.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``output_som`` (``std_logic``), in: SOM signal from OpenCPI interface that requires length limiting.

 * ``output_eom`` (``std_logic``), in: EOM signal from OpenCPI interface that requires length limiting.

 * ``output_valid`` (``std_logic``), in: Valid signal from OpenCPI interface that requires length limiting.

 * ``output_give`` (``std_logic``), in: Give signal from OpenCPI interface that requires length limiting.

 * ``output_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), in: Byte enable signal from OpenCPI interface that requires length limiting.

 * ``output_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), in: Opcode signal from OpenCPI interface that requires length limiting.

 * ``output_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: Data signal from OpenCPI interface that requires length limiting.

 * ``output_limited_som`` (``std_logic``), out: SOM signal of OpenCPI interface with length limiting applied.

 * ``output_limited_eom`` (``std_logic``), out: EOM signal of OpenCPI interface with length limiting applied.

 * ``output_limited_valid`` (``std_logic``), out: Valid signal of OpenCPI interface with length limiting applied.

 * ``output_limited_give`` (``std_logic``), out: Give signal of OpenCPI interface with length limiting applied.

 * ``output_limited_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), out: Byte enable signal of OpenCPI interface with length limiting applied.

 * ``output_limited_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), out: Opcode signal of OpenCPI interface with length limiting applied.

 * ``output_limited_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), out: Data signal of OpenCPI interface with length limiting applied.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``message_length_limiter`` are:

 * None.
