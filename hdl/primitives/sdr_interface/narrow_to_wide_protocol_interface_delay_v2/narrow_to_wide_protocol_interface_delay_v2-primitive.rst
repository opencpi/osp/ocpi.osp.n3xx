.. narrow_to_wide_protocol_interface_delay_v2 documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _narrow_to_wide_protocol_interface_delay_v2-primitive:


Protocol interface delay (Narrow to wide) for V2 HDL interface (``narrow_to_wide_protocol_interface_delay_v2``)
===============================================================================================================
Delays OpenCPI interface signals to align them with a processed data signal. Used when the input interface width is smaller than the output interface width.

Design
------
When data processing is performed on a stream of input data it typically results in a valid output one or more clock cycles after the input. This primitive delays OpenCPI interface signals in order to align them with a processed data stream. When the input opcode is equal to ``processed_data_opcode_g`` the processed data stream is output instead of the the delayed input data.

This primitive differs from the :ref:`protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>` as it also adapts the width of the interface from a narrower input interface to a wider output interface.

Implementation
--------------
A shift register of length ``delay_g`` is constructed for each of the interface signals (``som`` (start of message), ``eom`` (end of message), ``valid``, ``byte_enable``, ``opcode``, and ``data``). A shift register of length ``delay_g`` is also constructed for the interface ``take`` signal to keep track of when data was actually taken from the input interface.

The ``delay_g`` value is set to the same length as the delay present between the input signal and the processed data. As such, at the end of the shift registers, the interface signals are correctly aligned with the processed data. A multiplexer is used to select either the processed data signal or the delayed input signal. Processed data is selected when the delayed ``opcode`` signal is equal to ``processed_data_opcode_g`` and the delayed ``valid`` signal is ``1``. The output ``give`` signal is driven whenever the delayed version of the interface ``take`` signal is ``1`` and the output interface is ready to accept data.

If the output interface is not ready to accept data then the delay shift registers are not advanced. This means for every input signal an output signal is generated after a delay of ``delay_g`` clock cycles where the output is ready to accept data.

Interface
---------

Generics
~~~~~~~~

 * ``delay_g`` (``natural``): Sets the length of the interface delay (in clock cycles) to be added.

 * ``data_in_width_g`` (``positive``): Sets the width of the input interface data signal.

 * ``data_out_width_g`` (``positive``): Sets the width of the output interface data signal.

 * ``opcode_width_g`` (``positive``): Sets the width of the interface opcode signal.

 * ``byte_enable_width_g`` (``positive``): Sets the width of the interface byte enable signal.

 * ``processed_data_opcode_g`` (``std_logic_vector``): Sets the value of the opcode for which the ``processed_stream_in`` signal should be used for the output data signal instead of the delayed input data signal.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. When high, the delay pipeline is advanced. Therefore, data is absorbed on ``input`` and ``output`` changes when ``enable`` is high.

 * ``take_in`` (``std_logic``), in: Qualifies ``input_valid`` and ``input_ready``. Optional - can be left unconnected or driven high when not used. This only needs to be used when ``enable`` can be high while the input is not changing.

 * ``processed_stream_in`` (``std_logic_vector``, ``data_out_width_g`` bits), in: The processed data stream. I.e. the data this primitive is aligning the interface signals with.

 * ``input_som`` (``std_logic``), in: SOM signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_eom`` (``std_logic``), in: EOM signal from OpenCPI interface.

 * ``input_eof`` (``std_logic``), in: EOF signal from OpenCPI interface. Optional - can be left unconnected if ``output_eof`` is not required.

 * ``input_valid`` (``std_logic``), in: Valid signal from OpenCPI interface.

 * ``input_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), in: Byte enable signal from OpenCPI interface.

 * ``input_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), in: Opcode signal from OpenCPI interface.

 * ``input_data`` (``std_logic_vector``, ``data_in_width_g`` bits), in: Data signal from OpenCPI interface.

 * ``output_som`` (``std_logic``), out: SOM signal on output OpenCPI interface.

 * ``output_eom`` (``std_logic``), out: EOM signal on output OpenCPI interface.

 * ``output_eof`` (``std_logic``), out: EOF signal on output OpenCPI interface.

 * ``output_valid`` (``std_logic``), out: Valid signal on output OpenCPI interface.

 * ``output_give`` (``std_logic``), out: Give signal on output OpenCPI interface.

 * ``output_byte_enable`` (``std_logic_vector``, ``byte_enable_width_g`` bits), out: Byte enable signal on output OpenCPI interface.

 * ``output_opcode`` (``std_logic_vector``, ``opcode_width_g`` bits), out: Opcode signal on output OpenCPI interface.

 * ``output_data`` (``std_logic_vector``, ``data_out_width_g`` bits), out: Data signal on output OpenCPI interface.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``narrow_to_wide_protocol_interface_delay_v2`` are:

 * ``data_out_width_g`` must be an integer multiple of ``data_in_width_g``.

 * ``data_out_width_g`` / ``data_in_width_g`` must only be in the range 2 to 8 (inclusive).
