-- Protocol interface delay primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity protocol_interface_delay is
  generic (
    DELAY_G                 : integer          := 1;  -- Must not be 0
    DATA_WIDTH_G            : integer          := 8;
    OPCODE_WIDTH_G          : integer          := 3;
    BYTE_ENABLE_WIDTH_G     : integer          := 1;
    PROCESSED_DATA_MUX_G    : std_logic        := '1';
    PROCESSED_DATA_OPCODE_G : std_logic_vector := "000"
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;  -- High when output is ready
    take_in             : in  std_logic;  -- High when data is taken from input
    processed_stream_in : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
    -- Input interface signals
    input_som           : in  std_logic;
    input_eom           : in  std_logic;
    input_valid         : in  std_logic;
    input_byte_enable   : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
    input_opcode        : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
    input_data          : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
    -- Output interface signals
    output_som          : out std_logic;
    output_eom          : out std_logic;
    output_valid        : out std_logic;
    output_give         : out std_logic;
    output_byte_enable  : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
    output_opcode       : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
    output_data         : out std_logic_vector(DATA_WIDTH_G - 1 downto 0)
    );
end protocol_interface_delay;

architecture rtl of protocol_interface_delay is

  type byte_enable_array_t is array (DELAY_G - 1 downto 0) of std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
  type opcode_array_t is array (DELAY_G - 1 downto 0) of std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
  type data_array_t is array (DELAY_G - 1 downto 0) of std_logic_vector(DATA_WIDTH_G - 1 downto 0);

  -- Interface delay registers
  signal input_register_take        : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_som         : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_eom         : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_valid       : std_logic_vector(DELAY_G - 1 downto 0);
  signal input_register_byte_enable : byte_enable_array_t;
  signal input_register_opcode      : opcode_array_t;
  signal input_register_data        : data_array_t;

begin

  -- Add delay to align data with respective flow control signals
  delay_pipeline_1_gen : if DELAY_G = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take <= (others => '0');
        -- other registers don't need to be reset as gated by
        -- input_register_take.
        elsif(enable = '1') then
          input_register_take(0)        <= take_in;
          input_register_som(0)         <= input_som;
          input_register_eom(0)         <= input_eom;
          input_register_valid(0)       <= input_valid;
          input_register_byte_enable(0) <= input_byte_enable;
          input_register_opcode(0)      <= input_opcode;
          input_register_data(0)        <= input_data;
        end if;
      end if;
    end process;
  end generate;

  delay_pipeline_2_plus_gen : if DELAY_G > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          input_register_take <= (others => '0');
        -- other registers don't need to be reset as gated by
        -- input_register_take..
        elsif(enable = '1') then
          input_register_take        <= input_register_take(DELAY_G - 2 downto 0) & take_in;
          input_register_som         <= input_register_som(DELAY_G - 2 downto 0) & input_som;
          input_register_eom         <= input_register_eom(DELAY_G - 2 downto 0) & input_eom;
          input_register_valid       <= input_register_valid(DELAY_G - 2 downto 0) & input_valid;
          input_register_byte_enable <= input_register_byte_enable(DELAY_G - 2 downto 0) & input_byte_enable;
          input_register_opcode      <= input_register_opcode(DELAY_G - 2 downto 0) & input_opcode;
          input_register_data        <= input_register_data(DELAY_G - 2 downto 0) & input_data;
        end if;
      end if;
    end process;
  end generate;

  -- When the delayed interface opcode is the sample opcode and the data is
  -- valid, output the processed sample data rather than the delayed data.
  output_data <= processed_stream_in when
                 input_register_valid(input_register_valid'high) = '1' and
                 input_register_opcode(input_register_opcode'high) = PROCESSED_DATA_OPCODE_G and
                 PROCESSED_DATA_MUX_G = '1'
                 else input_register_data(input_register_data'high);

  -- Streaming interface output
  output_give        <= input_register_take(input_register_take'high) and enable;
  output_valid       <= input_register_valid(input_register_valid'high);
  output_som         <= input_register_som(input_register_som'high);
  output_eom         <= input_register_eom(input_register_eom'high);
  output_byte_enable <= input_register_byte_enable(input_register_byte_enable'high);
  output_opcode      <= input_register_opcode(input_register_opcode'high);

end rtl;
