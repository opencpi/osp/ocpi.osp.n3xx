.. protocol_interface_generator_v2 documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _protocol_interface_generator_v2-primitive:


Protocol interface generator HDL Version 2 (``protocol_interface_generator_v2``)
================================================================================
Generates OpenCPI interface signals to align them with a generated data signal.

Design
------
Components that generate data can take one or more clock cycles after a reset before they output valid data. This primitive generates OpenCPI output interface signals aligned with the output of logic that is generating data. The ``DELAY_G`` value sets the number of clock cycles after a reset when the generator logic will produce the first valid output.

A generator may feature a runtime configurable enable signal. When the generator is disabled, the ``generator_enable`` signal should be set to ``low``. This will terminate any on-going output messages and prevent any interface signals being generated until the generator is enabled.

A generator may feature a runtime configurable reset signal. When the generator is reset, the ``generator_reset`` signal should be set to ``high``. This will terminate any on-going output messages and output a discontinuity message to indicate a break in the samples. Once ``generator_reset`` is ``low``, the primitive will generate output interface signals after a delay of ``DELAY_G`` clock cycles.

A generator may need to output a discontinuity message at exactly the point that a change (as a result of a runtime change of a generator property) occurs at the output of a generator. When ``discontinuity_trigger`` is set ``high`` a discontinuity is output after a delay of ``DELAY_G`` clock cycles.

Implementation
--------------
The data source generator is connected before this primitive and passes the data into this primitive which then aligns that data with the OpenCPI interface signals.

Interface
---------

Generics
~~~~~~~~

 * ``delay_g`` (``positive``): Sets the length of the interface delay (in clock cycles) to be added.

 * ``data_width_g`` (``positive``): Sets the width of the interface data signal.

 * ``opcode_width_g`` (``positive``): Sets the width of the interface opcode signal.

 * ``byte_enable_width_g`` (``positive``): Sets the width of the interface byte enable signal.

 * ``max_message_size_g`` (``positive``): Sets the maximum possible message size (in bytes) which can be used.

 * ``processed_data_opcode_g`` (``std_logic_vector``): Sets the value of the opcode for when sample data is being generated.

 * ``discontinuity_opcode_g`` (``std_logic_vector``): Sets the value of the opcode for when a discontinuity is generated.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: System reset. Active high, synchronous with rising edge of clock.

 * ``output_ready`` (``std_logic``), in: High when output is ready.

 * ``discontinuity_trigger`` (``std_logic``), in: High when an event occurs which should introduce a discontinuity. After this goes high a discontinuity is inserted after ``DELAY_G`` clock cycles.

 * ``generator_enable`` (``std_logic``), in: High when generator is enabled. Typically set via a runtime modifiable property. While disabled it is assumed that the generator will not output any samples, but will retain its current state. After being enabled it is assumed that the module can immediately output samples, unless it has also been reset.

 * ``generator_reset`` (``std_logic``), in: High when generator is reset. Typically set via a runtime modifiable property. This signal should be set when the generating module is reset. After a reset event a discontinuity message is generated. It is assumed that the generator will take ``DELAY_G`` cycles after the reset event before a valid output is generated again.

 * ``message_length`` (``unsigned``), in: Specifies the length that the generated output message should be.

 * ``output_hold`` (``std_logic``), in: High when the generator module should be disabled. This occurs when a discontinuity message is being output, and therefore the generator must not output a sample.

 * ``processed_stream_in`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), in: The generated data stream. i.e. the data this primitive is aligning the output interface signals with.

 * ``output_som`` (``std_logic``), out: SOM signal on output OpenCPI interface.

 * ``output_eom`` (``std_logic``), out: EOM signal on output OpenCPI interface.

 * ``output_eof`` (``std_logic``), out: EOF signal on output OpenCPI interface.

 * ``output_valid`` (``std_logic``), out: Valid signal on output OpenCPI interface.

 * ``output_give`` (``std_logic``), out: Give signal on output OpenCPI interface.

 * ``output_byte_enable`` (``std_logic_vector``, ``BYTE_ENABLE_WIDTH_G`` bits), out: Byte enable signal on output OpenCPI interface.

 * ``output_opcode`` (``std_logic_vector``, ``OPCODE_WIDTH_G`` bits), out: Opcode signal on output OpenCPI interface.

 * ``output_data`` (``std_logic_vector``, ``DATA_WIDTH_G`` bits), out: Data signal on output OpenCPI interface.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``protocol_interface_generator_v2`` are:

 * None
