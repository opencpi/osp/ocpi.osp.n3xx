.. timestamp_recovery documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _timestamp_recovery-primitive:


Timestamp recovery (``timestamp_recovery``)
===========================================
Recovers the timestamp of each incoming sample by taking a periodic timestamp update message and incrementing it by the sample interval between updates.

Design
------
Recovers the timestamp of each incoming sample by taking a timestamp update message and incrementing it by the sample interval for each new sample received.

``timestamp_[seconds/fraction]_out`` and ``sample_interval_[seconds/fraction]_out`` are valid at the point where ``data_valid_in`` is high. This means timestamp and sample rate can be registered at the same time as the input data to a component.

Implementation
--------------
The timestamp calculation is split over multiple clock cycles to avoid timing issues cause by the high resolution of the timestamp. The primitive is fully pipelined and asserts no backpressure. Once a timestamp update or sample interval update has occurred, a new timestamp can be generated on every rising edge of the module's clock.

When a new timestamp is received the next data sample will have this timestamp. When a new sample interval is received the next data sample timestamp will be the last timestamp calculated using the old sample interval. Subsequent timestamps are advanced using the new sample rate. Users are advised that after a sample interval change a timestamp opcode should be sent for best results. This is due to the ambiguity of exactly when between the two samples the sample rate change occurred.

Interface
---------

Generics
~~~~~~~~

 * ``DATA_IN_WIDTH_G`` (``integer``): Sets the width of the data interface input.

 * ``ADDER_WIDTH_G`` (``integer``): Sets the size of the adders used to calculate to overall timestamp. 32 bits is recommended in most applications.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``timestamp_valid_in`` (``std_logic``), in: ``data_in`` is valid and contains timestamp data when ``timestamp_valid_in`` is high.

 * ``sample_interval_valid_in`` (``std_logic``), in: ``data_in`` is valid and contains sample interval data when ``sample_interval_valid_in`` is high.

 * ``data_valid_in`` (``std_logic``), in: ``data_in`` is valid and contains sample data when ``data_valid_in`` is high.

 * ``data_in`` (``std_logic_vector``, ``DATA_IN_WIDTH_G`` bits), in: Input data to the timestamp recovery primitive.

 * ``timestamp_units_out`` (``std_logic_vector``, 32 bits), out: Contains the integer part of the timestamp. Valid when ``data_valid_in`` is high.

 * ``timestamp_fraction_out`` (``std_logic_vector``, 64 bits), out: Contains the fractional part of the timestamp. Valid when ``data_valid_in`` is high.

 * ``sample_interval_units_out`` (``std_logic_vector``, 32 bits), out: Contains the integer part of the sample interval. Valid when ``data_valid_in`` is high.

 * ``sample_interval_fraction_out`` (``std_logic_vector``, 64 bits), out: Contains the fractional part of the sample interval. Valid when ``data_valid_in`` is high.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``timestamp_recovery`` are:

 * ``ADDER_WIDTH_G`` must be a factor of the full width of the timestamp (96 bits).

 * ``DATA_IN_WIDTH_G`` must be a factor of ADDER_WIDTH_G.
