-- Delay module for Upsampled sample data relative to incoming metadata
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes sample data.
-- Delay_G should be set to the delay in clock cycles of the module that
-- processes sample data.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity upsample_protocol_interface_delay_v2 is
  generic (
    stage1_delay_g          : positive         := 1;
    stage2_delay_g          : positive         := 1;
    data_width_g            : positive         := 8;
    opcode_width_g          : positive         := 3;
    byte_enable_width_g     : positive         := 1;
    processed_data_mux_g    : std_logic        := '1';
    processed_data_opcode_g : std_logic_vector := "000"
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;         -- Advances the delay line
    take_in             : in  std_logic := '1';  -- Qualifies _valid and _ready
    up_sample_factor    : in  unsigned(15 downto 0);
    processed_stream_in : in  std_logic_vector(data_width_g - 1 downto 0);
    -- Input interface signals
    input_som           : in  std_logic := '0';
    input_eom           : in  std_logic;
    input_eof           : in  std_logic := '0';
    input_valid         : in  std_logic;
    input_ready         : in  std_logic;
    input_byte_enable   : in  std_logic_vector(byte_enable_width_g - 1 downto 0);
    input_opcode        : in  std_logic_vector(opcode_width_g - 1 downto 0);
    input_data          : in  std_logic_vector(data_width_g - 1 downto 0);
    -- Output interface signals
    output_som          : out std_logic;
    output_eom          : out std_logic;
    output_eof          : out std_logic;
    output_valid        : out std_logic;
    output_give         : out std_logic;
    output_byte_enable  : out std_logic_vector(byte_enable_width_g - 1 downto 0);
    output_opcode       : out std_logic_vector(opcode_width_g - 1 downto 0);
    output_data         : out std_logic_vector(data_width_g - 1 downto 0)
    );
end upsample_protocol_interface_delay_v2;

architecture rtl of upsample_protocol_interface_delay_v2 is

  -- Interface types
  type stage1_byte_enable_array_t is array (stage1_delay_g - 1 downto 0) of std_logic_vector(byte_enable_width_g - 1 downto 0);
  type stage1_opcode_array_t is array (stage1_delay_g - 1 downto 0) of std_logic_vector(opcode_width_g - 1 downto 0);
  type stage1_data_array_t is array (stage1_delay_g - 1 downto 0) of std_logic_vector(data_width_g - 1 downto 0);
  type stage2_byte_enable_array_t is array (stage2_delay_g - 1 downto 0) of std_logic_vector(byte_enable_width_g - 1 downto 0);
  type stage2_opcode_array_t is array (stage2_delay_g - 1 downto 0) of std_logic_vector(opcode_width_g - 1 downto 0);
  type stage2_data_array_t is array (stage2_delay_g - 1 downto 0) of std_logic_vector(data_width_g - 1 downto 0);

  -- Interface delay registers
  signal stage1_input_register_take        : std_logic_vector(stage1_delay_g - 1 downto 0);
  signal stage1_input_register_som         : std_logic_vector(stage1_delay_g - 1 downto 0);
  signal stage1_input_register_eom         : std_logic_vector(stage1_delay_g - 1 downto 0);
  signal stage1_input_register_eof         : std_logic_vector(stage1_delay_g - 1 downto 0);
  signal stage1_input_register_valid       : std_logic_vector(stage1_delay_g - 1 downto 0);
  signal stage1_input_register_byte_enable : stage1_byte_enable_array_t;
  signal stage1_input_register_opcode      : stage1_opcode_array_t;
  signal stage1_input_register_data        : stage1_data_array_t;
  signal stage1_output_take                : std_logic;
  signal stage1_output_som                 : std_logic;
  signal stage1_output_eom                 : std_logic;
  signal stage1_output_eof                 : std_logic;
  signal stage1_output_valid               : std_logic;
  signal stage1_output_byte_enable         : std_logic_vector(byte_enable_width_g - 1 downto 0);
  signal stage1_output_opcode              : std_logic_vector(opcode_width_g - 1 downto 0);
  signal stage1_output_data                : std_logic_vector(data_width_g - 1 downto 0);
  signal stage2_input_take                 : std_logic;
  signal stage2_input_som                  : std_logic;
  signal stage2_input_eom                  : std_logic;
  signal stage2_input_eof                  : std_logic;
  signal stage2_input_valid                : std_logic;
  signal stage2_input_byte_enable          : std_logic_vector(byte_enable_width_g - 1 downto 0);
  signal stage2_input_opcode               : std_logic_vector(opcode_width_g - 1 downto 0);
  signal stage2_input_data                 : std_logic_vector(data_width_g - 1 downto 0);
  signal stage2_input_register_take        : std_logic_vector(stage2_delay_g - 1 downto 0);
  signal stage2_input_register_som         : std_logic_vector(stage2_delay_g - 1 downto 0);
  signal stage2_input_register_eom         : std_logic_vector(stage2_delay_g - 1 downto 0);
  signal stage2_input_register_eof         : std_logic_vector(stage2_delay_g - 1 downto 0);
  signal stage2_input_register_valid       : std_logic_vector(stage2_delay_g - 1 downto 0);
  signal stage2_input_register_byte_enable : stage2_byte_enable_array_t;
  signal stage2_input_register_opcode      : stage2_opcode_array_t;
  signal stage2_input_register_data        : stage2_data_array_t;

  -- Up-sample state machine signal
  signal input_freeze  : std_logic;
  type state_t is (idle_s, up_sample_s);
  signal current_state : state_t;
  signal counter       : unsigned(15 downto 0);
  signal eom_r         : std_logic;

begin

  ------------------------------------------------------------------------------
  -- Stage 1 Interface delay
  ------------------------------------------------------------------------------
  -- Add delay to align data with respective flow control signals
  stage1_delay_pipeline_1_gen : if stage1_delay_g = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage1_input_register_take  <= (others => '0');
          stage1_input_register_valid <= (others => '0');
        -- Other registers don't need to be reset as take and valid are the
        -- qualifying signals
        elsif enable = '1' and input_freeze = '0' then
          stage1_input_register_take(0)        <= input_ready and take_in;
          stage1_input_register_som(0)         <= input_som;
          stage1_input_register_eom(0)         <= input_eom;
          stage1_input_register_eof(0)         <= input_eof;
          stage1_input_register_valid(0)       <= input_valid and take_in;
          stage1_input_register_byte_enable(0) <= input_byte_enable;
          stage1_input_register_opcode(0)      <= input_opcode;
          stage1_input_register_data(0)        <= input_data;
        end if;
      end if;
    end process;
  end generate;

  stage1_delay_pipeline_2_plus_gen : if stage1_delay_g > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage1_input_register_take  <= (others => '0');
          stage1_input_register_valid <= (others => '0');
        -- Other registers don't need to be reset as take and valid are the
        -- qualifying signals
        elsif enable = '1' and input_freeze = '0' then
          stage1_input_register_take        <= stage1_input_register_take(stage1_input_register_take'length - 2 downto 0) & (input_ready and take_in);
          stage1_input_register_som         <= stage1_input_register_som(stage1_input_register_som'length - 2 downto 0) & input_som;
          stage1_input_register_eom         <= stage1_input_register_eom(stage1_input_register_eom'length - 2 downto 0) & input_eom;
          stage1_input_register_eof         <= stage1_input_register_eof(stage1_input_register_eof'length - 2 downto 0) & input_eof;
          stage1_input_register_valid       <= stage1_input_register_valid(stage1_input_register_valid'length - 2 downto 0) & (input_valid and take_in);
          stage1_input_register_byte_enable <= stage1_input_register_byte_enable(stage1_input_register_byte_enable'length - 2 downto 0) & input_byte_enable;
          stage1_input_register_opcode      <= stage1_input_register_opcode(stage1_input_register_opcode'length - 2 downto 0) & input_opcode;
          stage1_input_register_data        <= stage1_input_register_data(stage1_input_register_data'length - 2 downto 0) & input_data;
        end if;
      end if;
    end process;
  end generate;

  stage1_output_take        <= stage1_input_register_take(stage1_input_register_take'high);
  stage1_output_som         <= stage1_input_register_som(stage1_input_register_som'high);
  stage1_output_eom         <= stage1_input_register_eom(stage1_input_register_eom'high);
  stage1_output_eof         <= stage1_input_register_eof(stage1_input_register_eof'high);
  stage1_output_valid       <= stage1_input_register_valid(stage1_input_register_valid'high);
  stage1_output_byte_enable <= stage1_input_register_byte_enable(stage1_input_register_byte_enable'high);
  stage1_output_opcode      <= stage1_input_register_opcode(stage1_input_register_opcode'high);
  stage1_output_data        <= stage1_input_register_data(stage1_input_register_data'high);

  ------------------------------------------------------------------------------
  -- Up-sample stage
  ------------------------------------------------------------------------------
  -- Pauses the stage 1 input register while up-sampled samples are being
  -- generated.
  interpolate_stage_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        current_state <= idle_s;
      elsif enable = '1' then
        case current_state is
          when idle_s =>
            if stage1_output_valid = '1' and up_sample_factor /= 1 and
              stage1_output_opcode = processed_data_opcode_g then
              current_state <= up_sample_s;
              counter       <= up_sample_factor - 2;
              eom_r         <= stage1_output_eom;
            end if;
          when up_sample_s =>
            counter <= counter - 1;
            if counter = 0 then
              current_state <= idle_s;
            end if;
        end case;
      end if;
    end if;
  end process;

  input_freeze <= '1' when current_state = up_sample_s else '0';

  -- Inject the interface control signals for the up sampled data
  stage1_output_p : process (current_state, counter, stage1_output_take, stage1_output_som, stage1_output_eom,
                             stage1_output_eof, stage1_output_valid, stage1_output_byte_enable, stage1_output_opcode,
                             stage1_output_data, up_sample_factor, eom_r)
  begin
    -- Data is always passed through as we only care about the contents when
    -- the opcode is not a sample opcode.
    stage2_input_data <= stage1_output_data;
    stage2_input_eof  <= stage1_output_eof;
    if current_state = idle_s then
      stage2_input_take        <= stage1_output_take;
      stage2_input_valid       <= stage1_output_valid;
      stage2_input_byte_enable <= stage1_output_byte_enable;
      stage2_input_opcode      <= stage1_output_opcode;
      -- Suppress EOM flag when interpolating as it will be output at the
      -- end of the interpolated samples.
      if stage1_output_take = '1' and stage1_output_valid = '1' and up_sample_factor /= 1 and
        stage1_output_opcode = processed_data_opcode_g then
        stage2_input_eom <= '0';
      else
        stage2_input_eom <= stage1_output_eom;
      end if;
    else
      -- During interpolation most of the interface control signals are fixed
      stage2_input_take        <= '1';
      stage2_input_valid       <= '1';
      stage2_input_byte_enable <= (others => '1');
      stage2_input_opcode      <= processed_data_opcode_g;
      -- On the last sample of the interpolation output the EOM flag if the
      -- original up sampled sample had it set.
      if counter = 0 then
        stage2_input_eom <= eom_r;
      else
        stage2_input_eom <= '0';
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Stage 2 interface delay
  ------------------------------------------------------------------------------
  -- Add further delay to align data with respective flow control signals
  stage2_delay_pipeline_1_gen : if stage2_delay_g = 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage2_input_register_take  <= (others => '0');
          stage2_input_register_valid <= (others => '0');
        -- Other registers don't need to be reset as take and valid are the
        -- qualifying signals
        elsif enable = '1' then
          stage2_input_register_take(0)        <= stage2_input_take;
          stage2_input_register_som(0)         <= stage2_input_som;
          stage2_input_register_eom(0)         <= stage2_input_eom;
          stage2_input_register_eof(0)         <= stage2_input_eof;
          stage2_input_register_valid(0)       <= stage2_input_valid;
          stage2_input_register_byte_enable(0) <= stage2_input_byte_enable;
          stage2_input_register_opcode(0)      <= stage2_input_opcode;
          stage2_input_register_data(0)        <= stage2_input_data;
        end if;
      end if;
    end process;
  end generate;

  stage2_delay_pipeline_2_plus_gen : if stage2_delay_g > 1 generate
    interface_delay_pipeline_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          stage2_input_register_take  <= (others => '0');
          stage2_input_register_valid <= (others => '0');
        -- Other registers don't need to be reset as take and valid are the
        -- qualifying signals
        elsif enable = '1' then
          stage2_input_register_take        <= stage2_input_register_take(stage2_input_register_take'length - 2 downto 0) & stage2_input_take;
          stage2_input_register_som         <= stage2_input_register_som(stage2_input_register_som'length - 2 downto 0) & stage2_input_som;
          stage2_input_register_eom         <= stage2_input_register_eom(stage2_input_register_eom'length - 2 downto 0) & stage2_input_eom;
          stage2_input_register_eof         <= stage2_input_register_eof(stage2_input_register_eof'length - 2 downto 0) & stage2_input_eof;
          stage2_input_register_valid       <= stage2_input_register_valid(stage2_input_register_valid'length - 2 downto 0) & stage2_input_valid;
          stage2_input_register_byte_enable <= stage2_input_register_byte_enable(stage2_input_register_byte_enable'length - 2 downto 0) & stage2_input_byte_enable;
          stage2_input_register_opcode      <= stage2_input_register_opcode(stage2_input_register_opcode'length - 2 downto 0) & stage2_input_opcode;
          stage2_input_register_data        <= stage2_input_register_data(stage2_input_register_data'length - 2 downto 0) & stage2_input_data;
        end if;
      end if;
    end process;
  end generate;

  -- When the delayed interface opcode is the sample opcode and the data is
  -- valid, output the processed sample data rather than the delayed data.
  output_data <= std_logic_vector(processed_stream_in) when
                 stage2_input_register_valid(stage2_input_register_valid'high) = '1'
                 and stage2_input_register_opcode(stage2_input_register_opcode'high) = processed_data_opcode_g
                 else stage2_input_register_data(stage2_input_register_data'high);

  -- Streaming interface output
  output_som         <= stage2_input_register_som(stage2_input_register_som'high);
  output_eom         <= stage2_input_register_eom(stage2_input_register_eom'high);
  output_eof         <= stage2_input_register_eof(stage2_input_register_eof'high);
  output_valid       <= stage2_input_register_valid(stage2_input_register_valid'high);
  output_give        <= stage2_input_register_take(stage2_input_register_take'high);
  output_byte_enable <= stage2_input_register_byte_enable(stage2_input_register_byte_enable'high);
  output_opcode      <= stage2_input_register_opcode(stage2_input_register_opcode'high);

end rtl;
