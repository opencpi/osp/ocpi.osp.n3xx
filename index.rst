.. Top level project documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Ettus Research N3xx OpenCPI System Support Project 
==================================================
OpenCPI System Support Project (OSP) for the following:

   * :doc:`Ettus USRP N310 Platform <./hdl/platforms/n310/n310-worker>` (:doc:`Getting Started Guide <./hdl/platforms/n310/n310-gsg>`)

Repository Contents
-------------------
.. toctree::
   :maxdepth: 2
   :glob:

   applications/applications
   components/components
   hdl/devices/devices
   hdl/primitives/sdr_dsp/*-library
   hdl/primitives/sdr_interface/*-library
   hdl/platforms/platforms





